<?php

namespace App\Interfaces;

interface ClaimsInterface
{
    const DEFAULT_LIMIT = 25;
    const DEFAULT_SORTING = 'id';
    const DEFAULT_ASCENDING = 'desc';

    /**
     * fetch all claims
     *
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function all(array $columns = ['*'], $sorting = ClaimsInterface::DEFAULT_SORTING, $ascending = ClaimsInterface::DEFAULT_ASCENDING);

    /**
     * list claims
     *
     * @param array $conditions
     * @param array $columns
     * @param int $offset
     * @param int $limit
     *
     * @return \Illuminate\Support\Collection
     */
    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * pagination claims
     *
     * @param array $conditions
     * @param array $columns
     * @param int $page
     * @param int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $perPage = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * find claims by id
     *
     * @param $id
     * @param array $columns
     *
     * @return object|null
     */
    public function findById($id, array $columns = ['*']);

    /**
     * get claims dropdown list
     *
     * @return dropdown
     */
    public function dropdownList();

    /**
     * create new claims
     *
     * @param array $data
     *
     * @return object|null
     */
    public function store(array $data);

    /**
     * approve claims by id
     *
     * @param $id
     * @param array $data
     *
     * @return object
     */
    public function approveById($id, array $data);

    /**
     * Cancel claims by id
     *
     * @param $id
     * @param array $data
     *
     * @return object
     */
    public function cancelById($id, array $data);
}
