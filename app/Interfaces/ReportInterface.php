<?php

namespace App\Interfaces;

interface ReportInterface
{
    const DEFAULT_LIMIT = 25;
    const DEFAULT_SORTING = 'id';
    const DEFAULT_ASCENDING = 'desc';

    /**
     * list report user
     *
     * @param array $conditions
     * @param array $columns
     * @param int $offset
     * @param int $limit
     *
     * @return \Illuminate\Support\Collection
     */
    public function fetchUserByFields(array $conditions = []);

    /**
     * list report tracking
     *
     * @param array $data
     *
     * @return object
     */
    public function tracking(array $conditions);

    /**
     * list report bill
     *
     * @param array $conditions
     *
     * @return \Illuminate\Support\Collection
     */
    public function fetchBillList(array $conditions = []);

       /**
     * list report truck
     *
     * @param array $conditions
     *
     * @return \Illuminate\Support\Collection
     */
    public function fetchTruckList(array $conditions = []);

    /**
     * list report topup
     *
     * @param array $data
     *
     * @return object
     */
    public function topup(array $conditions);
    /**
     * list report withdraw
     *
     * @param array $conditions
     * @param array $columns
     * @param int $offset
     * @param int $limit
     *
     * @return \Illuminate\Support\Collection
     */
    public function fetchWithdrawListByFields(array $conditions = []);

    /**
     * pagination nocode
     *
     * @param array $conditions
     * @param int $page
     * @param int $perPage
     *
     * @return object
     */
    public function paginateActiveUserByFields(array $conditions = [], $offset = 1, $limit = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);
}
