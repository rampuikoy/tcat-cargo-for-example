<?php

namespace App\Imports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithBatchInserts;

class UsersImport implements ToModel, WithHeadingRow, WithValidation, WithChunkReading, WithBatchInserts
{
  use Importable;

  public function model(array $row)
  {
    return new User([
      'code' => $row['code'],
      'name' => $row['name'],
      'email' => $row['email'],
      // 'password' => Hash::make($row['password']),
      'password' => $row['password'],
      'email_verified_at' => $row['email_verified_at'],
      'remember_token' => $row['remember_token'],
      'tel1' => $row['tel1'],
      'tel2' => $row['tel2'],
      'image' => $row['image'],
      'status' => $row['status'],
      'create_bill' => $row['create_bill'],
      'withholding' => $row['withholding'],
      'tax_id' => $row['tax_id'],
      'sms_notification' => $row['sms_notification'],
      'alert_box' => $row['alert_box'],
      'alert_message' => $row['alert_message'],
      'fcm_token' => $row['fcm_token'],
      'user_remark' => $row['user_remark'],
      'admin_remark' => $row['admin_remark'],
      'system_remark' => $row['system_remark'],
      'created_at' => $row['created_at'],
      'updated_at' => $row['updated_at'],
      'created_admin_id' => $row['created_admin_id'],
      'updated_admin_id' => $row['updated_admin_id'],
      'created_admin' => $row['created_admin'],
      'updated_admin' => $row['updated_admin'],
      'deleted_at' => $row['deleted_at'],
      'province_id' => $row['province_id'],
      'admin_ref_id' => $row['admin_ref_id'],
      'default_rate_id' => $row['default_rate_id'],
      'thai_shipping_method_id' => $row['thai_shipping_method_id'],
    ]);
  }

  public function rules(): array
  {
    return  [
      'email' => 'unique:users,email',
      'code' => 'unique:users,code'
    ];
  }

  public function chunkSize(): int
  {
    return 1000;
  }

  public function batchSize(): int
  {
    return 1000;
  }
}
