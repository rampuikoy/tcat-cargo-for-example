<?php

namespace App\Interfaces;

interface TrackingInterface
{
    const DEFAULT_LIMIT = 25;
    const DEFAULT_SORTING = 'id';
    const DEFAULT_ASCENDING = 'desc';

    /**
     * fetch all tracking
     *
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function all(array $columns = ['*'], $sorting = TrackingInterface::DEFAULT_SORTING, $ascending = TrackingInterface::DEFAULT_ASCENDING);

    /**
     * list tracking
     *
     * @param array $conditions
     * @param array $columns
     * @param int $offset
     * @param int $limit
     *
     * @return \Illuminate\Support\Collection
     */
    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * pagination tracking
     *
     * @param array $conditions
     * @param array $columns
     * @param int $page
     * @param int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $perPage = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * find tracking by id
     *
     * @param $id
     * @param array $columns
     *
     * @return object|null
     */
    public function findById($id, array $columns = ['*']);

    /**
     * find tracking by user code
     *
     * @param $user code
     * @param array $columns
     *
     * @return object|null
     */
    public function findByUserCode($userCode, array $columns = ['*']);

    /**
     * find tracking by user track
     *
     * @param $code track
     * @param array $columns
     *
     * @return object|null
     */
    public function findByCode($code, array $columns = ['*']);

    /**
     * find tracking by user track
     *
     * @param $code track
     * @param array $columns
     *
     * @return object|null
     */
    public function findExternalByCode($code, array $columns = ['*']);


    /**
     * find tracking by user track
     *
     * @param $code track
     * @param array $columns
     *
     * @return object|null
     */
    public function findByUserCodeExtend($code);

    /**
     * get tracking dropdown list
     *
     * @return dropdown
     */
    public function dropdownList();

    /**
     * create new tracking
     *
     * @param array $data
     *
     * @return object|null
     */
    public function store(array $data);

    /**
     * update tracking by id
     *
     * @param $id
     * @param array $data
     *
     * @return object
     */
    public function updateById($id, array $data);

    /**
     * create or update tracking detail by id
     *
     * @param $id
     * @param array $data
     *
     * @return object
     */
    public function trackingDetail($id, array $data);

    /**
     * delete tracking by id
     *
     * @param $id
     *
     * @return object|null
     */

    // public function deleteById($id);

    /**
     * soft delete tracking by id
     *
     * @param $id
     *
     * @return object|null
     */
    public function softDeleteById($id);

    /**
     * delete many permission by id
     *
     * @param $id2
     *
     * @return object|null
     */
    public function deleteMany(array $id);

    public function searchManyByCode($id, array $columns = ['*']);

    /**
     * putZoneIn
     *
     * @param $data
     * @param array $columns
     *
     * @return object|null
     */

    public function putZoneIn(array $data);
    /**
     * putZoneInModal
     *
     * @param $data
     * @param array $columns
     *
     * @return object|null
     */

    public function putZoneInModal(array $data);

    /**
     * postZoneIn
     *
     * @param $data
     * @param array $columns
     *
     * @return object|null
     */

    public function postZoneIn(array $data);
}
