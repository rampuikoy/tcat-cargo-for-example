<?php

namespace App\Exports;

use App\Exports\Commission\CommissionNormal;
use App\Exports\Commission\CommissionSpecial;
use App\Exports\Commission\CommissionSummary;
use App\Helpers\Util;
use App\Models\Admin;
use App\Models\Rate;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class CommissionExport implements WithMultipleSheets
{
    use Exportable;

    protected $bills, $bill_custom, $sale, $rate, $date_start, $date_end;

    public static $minPrice = 100;

    public function setBill(Collection $bills)
    {
        $this->bills = $bills;
    }

    public function setBillCustom(Collection $bills)
    {
        $this->bill_custom = $bills;
    }

    public function setSale(Admin $sale)
    {
        $this->sale = $sale;
    }

    public function setRate(Rate $rate)
    {
        $this->rate = $rate;
    }

    public function setDateStart($date_start)
    {
        $this->date_start = $date_start;
    }

    public function setDateEnd($date_end)
    {
        $this->date_end = $date_end;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $bill_normal            = $this->getComissionNormalData();
        $total_normal           = $this->getComissionTotal($bill_normal);
        $commission_normal      = collect([
                                    'sale'          => $this->sale,
                                    'rate'          => $this->rate,
                                    'date_start'    => $this->date_start,
                                    'date_end'      => $this->date_end,
                                    'bill'          => $bill_normal
                                ])
                                ->merge($total_normal)
                                ->toArray();

        $bill_special           = $this->getComissionSpecialData();
        $total_normal           = $this->getComissionTotal($bill_special);
        $commission_special     = collect([
                                    'sale'          => $this->sale,
                                    'rate'          => $this->rate,
                                    'date_start'    => $this->date_start,
                                    'date_end'      => $this->date_end,
                                    'bill'          => $bill_special
                                ])
                                ->merge($total_normal)
                                ->toArray();
                                
        $commission_summary     = [
                                    'normal_increase'   => collect($bill_normal)->sum('sale_revenue'),
                                    'special_increase'  => collect($bill_special)->sum('sale_revenue'),
                                 ];

        return [
            new CommissionNormal($commission_normal),
            new CommissionSpecial($commission_special),
            new CommissionSummary($commission_summary),
        ];
    }

    public function getComissionNormalData()
    {
        $rate           = $this->rate;
        $collection     = collect($this->bills)->sortBy('user_code')->values();
        $results        = [];

        if ($rate && $collection->count() > 0) {
            $results = $collection->map(function ($bill) use ($rate) {
                $shipping_rate              = json_decode($bill->shipping_rate);
                $user_shipping_rate         = $this->getUserShippingRate($shipping_rate);
                $company_shipping_rate      = $this->getCompanyShippingRate($shipping_rate, $rate);

                $company_cost_total         = $company_shipping_rate['bill_total'] ?? 0;
                $user_cost_total            = $user_shipping_rate['bill_total'] ?? 0;
                $brand_revenue              = $this->calculateBrand($shipping_rate->sum_price);
                list($company_revenue, $sale_revenue) = $this->calculateSaleRevenue($company_cost_total, $user_cost_total, $brand_revenue, $bill->shipping_price);

                return (object) [
                    'id'                    => $bill->id,
                    'no'                    => $bill->no,
                    'user_code'             => $bill->user_code,
                    'user_expense'          => $bill->shipping_price,
                    'company_revenue'       => $company_revenue,
                    'sum_price_brand'       => $brand_revenue,
                    'sale_revenue'          => $sale_revenue,
                    'user_shipping_rate'    => $user_shipping_rate,
                    'company_shipping_rate' => $company_shipping_rate
                ];
            })->toArray();
        }

        return $results;
    }

    public function getComissionSpecialData()
    {
        $collection     = collect($this->bill_custom)->sortBy('user_code')->values();
        $results        = [];

        if ($collection->count() > 0) {
            $results = $collection->map(function ($bill) {
                $rate                       = $bill->commissionRate;
                $shipping_rate              = json_decode($bill->shipping_rate);
                $user_shipping_rate         = $this->getUserShippingRate($shipping_rate);
                $company_shipping_rate      = $this->getCompanyShippingRate($shipping_rate, $rate);

                $company_cost_total         = $company_shipping_rate['bill_total'] ?? 0;
                $user_cost_total            = $user_shipping_rate['bill_total'] ?? 0;
                $brand_revenue              = $this->calculateBrand($shipping_rate->sum_price);
                list($company_revenue, $sale_revenue) = $this->calculateSaleRevenue($company_cost_total, $user_cost_total, $brand_revenue, $bill->shipping_price);


                return (object) [
                    'id'                    => $bill->id,
                    'no'                    => $bill->no,
                    'user_code'             => $bill->user_code,
                    'user_expense'          => $bill->shipping_price,
                    'company_revenue'       => $company_revenue,
                    'sum_price_brand'       => $brand_revenue,
                    'sale_revenue'          => $sale_revenue,
                    'user_shipping_rate'    => $user_shipping_rate,
                    'company_shipping_rate' => $company_shipping_rate,
                    'rate'                  => $rate
                ];
            })->toArray();
        }
        return $results;
    }

    private function getComissionTotal($bills)
    {
        return collect($bills)->reduce(function ($sum, $bill) {
            $sum['total_user_expense']      += $bill->user_expense;
            $sum['total_company_revenue']   += $bill->company_revenue;
            $sum['total_sum_price_brand']   += $bill->sum_price_brand;
            $sum['total_sale_revenue']      += $bill->sale_revenue;
            return $sum;
        }, [
            'total_user_expense'        => 0,
            'total_company_revenue'     => 0,
            'total_sum_price_brand'     => 0,
            'total_sale_revenue'        => 0,
        ]);
    }

    private function getUserShippingRate($shipping_rate)
    {
        $car_total      = ($shipping_rate->sum_price->kg_car_genaral +
                            $shipping_rate->sum_price->cubic_car_genaral +
                            $shipping_rate->sum_price->kg_car_iso +
                            $shipping_rate->sum_price->cubic_car_iso);

        $ship_total     = ($shipping_rate->sum_price->kg_ship_genaral +
                            $shipping_rate->sum_price->cubic_ship_genaral +
                            $shipping_rate->sum_price->kg_ship_iso +
                            $shipping_rate->sum_price->cubic_ship_iso);

        $bill_total     = ($car_total + $ship_total);

        return [
            // car
            'sum_unit_kg_car_genaral'       => $shipping_rate->sum_unit->kg_car_genaral,        // สินค้าทั่วไป -> KG -> จำนวน
            'rate_kg_car_genaral'           => Util::setIfExist($shipping_rate->sum_unit->kg_car_genaral, $shipping_rate->kg_car_genaral),       // สินค้าทั่วไป -> KG -> เรท
            'sum_price_kg_car_genaral'      => $shipping_rate->sum_price->kg_car_genaral,       // สินค้าทั่วไป -> KG -> ยอด

            'sum_unit_cubic_car_genaral'    => $shipping_rate->sum_unit->cubic_car_genaral,     // สินค้าทั่วไป -> CBM -> จำนวน
            'rate_cubic_car_genaral'        => Util::setIfExist($shipping_rate->sum_unit->cubic_car_genaral, $shipping_rate->cubic_car_genaral), // สินค้าทั่วไป -> CBM -> เรท
            'sum_price_cubic_car_genaral'   => $shipping_rate->sum_price->cubic_car_genaral,    // สินค้าทั่วไป -> CBM -> ยอด

            'sum_unit_kg_car_iso'           => $shipping_rate->sum_unit->kg_car_iso,            // อย. มอก. -> KG -> จำนวน
            'rate_kg_car_iso'               => Util::setIfExist($shipping_rate->sum_unit->kg_car_iso, $shipping_rate->kg_car_iso),               // อย. มอก. -> KG -> เรท
            'sum_price_kg_car_iso'          => $shipping_rate->sum_price->kg_car_iso,           // อย. มอก. -> KG -> ยอด
            
            'sum_unit_cubic_car_iso'        => $shipping_rate->sum_unit->cubic_car_iso,         // อย. มอก. -> CBM -> จำนวน
            'rate_cubic_car_iso'            => Util::setIfExist($shipping_rate->sum_unit->cubic_car_iso, $shipping_rate->cubic_car_iso),         // อย. มอก. -> CBM -> เรท
            'sum_price_cubic_car_iso'       => $shipping_rate->sum_price->cubic_car_iso,        // อย. มอก. -> CBM -> ยอด
            'car_total'                     => $car_total,                                      //ยอดรวม

            // ship             
            'sum_unit_kg_ship_genaral'      => $shipping_rate->sum_unit->kg_ship_genaral,        // สินค้าทั่วไป -> KG -> จำนวน
            'rate_kg_ship_genaral'          => Util::setIfExist($shipping_rate->sum_unit->kg_ship_genaral, $shipping_rate->kg_ship_genaral),        // สินค้าทั่วไป -> KG -> เรท  
            'sum_price_kg_ship_genaral'     => $shipping_rate->sum_price->kg_ship_genaral,       // สินค้าทั่วไป -> KG -> ยอด

            'sum_unit_cubic_ship_genaral'   => $shipping_rate->sum_unit->cubic_ship_genaral,     // สินค้าทั่วไป -> CBM -> จำนวน
            'rate_cubic_ship_genaral'       => Util::setIfExist($shipping_rate->sum_unit->cubic_ship_genaral, $shipping_rate->cubic_ship_genaral),  // สินค้าทั่วไป -> CBM -> เรท
            'sum_price_cubic_ship_genaral'  => $shipping_rate->sum_price->cubic_ship_genaral,    // สินค้าทั่วไป -> CBM -> ยอด

            'sum_unit_kg_ship_iso'          => $shipping_rate->sum_unit->kg_ship_iso,            // อย. มอก. -> KG -> จำนวน
            'rate_kg_ship_iso'              => Util::setIfExist($shipping_rate->sum_unit->kg_ship_iso, $shipping_rate->kg_ship_iso),               // อย. มอก. -> KG -> เรท
            'sum_price_kg_ship_iso'         => $shipping_rate->sum_price->kg_ship_iso,           // อย. มอก. -> KG -> ยอด

            'sum_unit_cubic_ship_iso'       => $shipping_rate->sum_unit->cubic_ship_iso,         // อย. มอก. -> CBM -> จำนวน
            'rate_cubic_ship_iso'           => Util::setIfExist($shipping_rate->sum_unit->cubic_ship_iso, $shipping_rate->cubic_ship_iso),         // อย. มอก. -> CBM -> เรท
            'sum_price_cubic_ship_iso'      => $shipping_rate->sum_price->cubic_ship_iso,        // อย. มอก. -> CBM -> ยอด
            'ship_total'                    => $ship_total,                                      //ยอดรวม
            
            'bill_total'                    => $bill_total                                      //ยอดบิล
        ];
    }

    private function getCompanyShippingRate($shipping_rate, $rate)
    {
        $sum_price_kg_car_genaral           = ($shipping_rate->sum_unit->kg_car_genaral * $rate->kg_car_genaral);
        $sum_price_cubic_car_genaral        = ($shipping_rate->sum_unit->cubic_car_genaral * $rate->cubic_car_genaral);
        $sum_price_kg_car_iso               = ($shipping_rate->sum_unit->kg_car_iso * $rate->kg_car_iso);
        $sum_unit_cubic_car_iso             = ($shipping_rate->sum_unit->cubic_car_iso * $rate->cubic_car_iso);

        $car_total                          = ($sum_price_kg_car_genaral +
                                                $sum_price_cubic_car_genaral +
                                                $sum_price_kg_car_iso +
                                                $sum_unit_cubic_car_iso);

        $sum_price_kg_ship_genaral          = ($shipping_rate->sum_unit->kg_ship_genaral * $rate->kg_ship_genaral);
        $sum_price_cubic_ship_genaral       = ($shipping_rate->sum_unit->cubic_ship_genaral * $rate->cubic_ship_genaral);
        $sum_price_kg_ship_iso              = ($shipping_rate->sum_unit->kg_ship_iso * $rate->kg_ship_iso);
        $sum_unit_cubic_ship_iso            = ($shipping_rate->sum_unit->cubic_ship_iso * $rate->cubic_ship_iso);
                                      
        $ship_total                         = ($sum_price_kg_ship_genaral +
                                                $sum_price_cubic_ship_genaral +
                                                $sum_price_kg_ship_iso +
                                                $sum_unit_cubic_ship_iso);
        
        $bill_total                         = ($car_total + $ship_total);

        return [
            // car
            'sum_unit_kg_car_genaral'       => $shipping_rate->sum_unit->kg_car_genaral,                                                // สินค้าทั่วไป -> KG -> จำนวน
            'rate_kg_car_genaral'           => Util::setIfExist($shipping_rate->sum_unit->kg_car_genaral, $rate->kg_car_genaral),       // สินค้าทั่วไป -> KG -> เรท
            'sum_price_kg_car_genaral'      => $sum_price_kg_car_genaral,                                                               // สินค้าทั่วไป -> KG -> ยอด

            'sum_unit_cubic_car_genaral'    => $shipping_rate->sum_unit->cubic_car_genaral,                                             // สินค้าทั่วไป -> CBM -> จำนวน
            'rate_cubic_car_genaral'        => Util::setIfExist($shipping_rate->sum_unit->cubic_car_genaral, $rate->cubic_car_genaral), // สินค้าทั่วไป -> CBM -> เรท
            'sum_price_cubic_car_genaral'   => $sum_price_cubic_car_genaral,                                                            // สินค้าทั่วไป -> CBM -> ยอด

            'sum_unit_kg_car_iso'           => $shipping_rate->sum_unit->kg_car_iso,                                                    // อย. มอก. -> KG -> จำนวน
            'rate_kg_car_iso'               => Util::setIfExist($shipping_rate->sum_unit->kg_car_iso, $rate->kg_car_iso),               // อย. มอก. -> KG -> เรท
            'sum_price_kg_car_iso'          => $sum_price_kg_car_iso,                                                                   // อย. มอก. -> KG -> ยอด
            
            'sum_unit_cubic_car_iso'        => $shipping_rate->sum_unit->cubic_car_iso,                                                 // อย. มอก. -> CBM -> จำนวน
            'rate_cubic_car_iso'            => Util::setIfExist($shipping_rate->sum_unit->cubic_car_iso, $rate->cubic_car_iso),         // อย. มอก. -> CBM -> เรท
            'sum_price_cubic_car_iso'       => $sum_unit_cubic_car_iso,                                                                 // อย. มอก. -> CBM -> ยอด
            'car_total'                     => $car_total,                                      //ยอดรวม

            // ship             
            'sum_unit_kg_ship_genaral'      => $shipping_rate->sum_unit->kg_ship_genaral,                                                // สินค้าทั่วไป -> KG -> จำนวน
            'rate_kg_ship_genaral'          => Util::setIfExist($shipping_rate->sum_unit->kg_ship_genaral, $rate->kg_ship_genaral),       // สินค้าทั่วไป -> KG -> เรท
            'sum_price_kg_ship_genaral'     => $sum_price_kg_ship_genaral,                                                               // สินค้าทั่วไป -> KG -> ยอด

            'sum_unit_cubic_ship_genaral'   => $shipping_rate->sum_unit->cubic_ship_genaral,                                             // สินค้าทั่วไป -> CBM -> จำนวน
            'rate_cubic_ship_genaral'       => Util::setIfExist($shipping_rate->sum_unit->cubic_ship_genaral, $rate->cubic_ship_genaral), // สินค้าทั่วไป -> CBM -> เรท
            'sum_price_cubic_ship_genaral'  => $sum_price_cubic_ship_genaral,                                                            // สินค้าทั่วไป -> CBM -> ยอด

            'sum_unit_kg_ship_iso'          => $shipping_rate->sum_unit->kg_ship_iso,                                                    // อย. มอก. -> KG -> จำนวน
            'rate_kg_ship_iso'              => Util::setIfExist($shipping_rate->sum_unit->kg_ship_iso, $rate->kg_ship_iso),               // อย. มอก. -> KG -> เรท
            'sum_price_kg_ship_iso'         => $sum_price_kg_ship_iso,                                                                   // อย. มอก. -> KG -> ยอด
            
            'sum_unit_cubic_ship_iso'       => $shipping_rate->sum_unit->cubic_ship_iso,                                                 // อย. มอก. -> CBM -> จำนวน
            'rate_cubic_ship_iso'           => Util::setIfExist($shipping_rate->sum_unit->cubic_ship_iso, $rate->cubic_ship_iso),         // อย. มอก. -> CBM -> เรท
            'sum_price_cubic_ship_iso'      => $sum_unit_cubic_ship_iso,                                                                 // อย. มอก. -> CBM -> ยอด
            'ship_total'                    => $ship_total,  
            
            'bill_total'                    => $bill_total                                      //ยอดบิล
        ];
    }

    private function calculateBrand($sum_price)
    {
        return ($sum_price->kg_car_brand +
            $sum_price->cubic_car_brand +
            $sum_price->kg_ship_brand +
            $sum_price->cubic_ship_brand);
    }

    private function calculateSaleRevenue($company_cost_total, $user_cost_total, $brand_revenue, $shipping_price)
    {
        $min = self::$minPrice;
        if ($user_cost_total < $min) {
            $sale_revenue = ($user_cost_total - $company_cost_total - $brand_revenue);
            $company_revenue = max(($min - $sale_revenue), 0);
        } else {

            $sale_revenue = ($shipping_price - $company_cost_total - $brand_revenue);
            $company_revenue = $company_cost_total;
        }
        return [$company_revenue, $sale_revenue];
    }
}
