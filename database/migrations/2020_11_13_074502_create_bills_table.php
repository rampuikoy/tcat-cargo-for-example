<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->id()->unsigned();
            $table->timestamps();
            $table->smallInteger('rate_id')->unsigned();
            $table->string('rate_title');
            $table->smallInteger('shipping_weight_counter')->unsigned();
            $table->smallInteger('shipping_cubic_counter')->unsigned();
            $table->double('shipping_weight_price', 8, 2);
            $table->double('shipping_cubic_price', 8, 2);
            $table->integer('shipping_min_rate');
            $table->double('shipping_price', 8, 2);
            $table->text('shipping_rate');
            $table->smallInteger('counter')->unsigned();
            $table->integer('thai_shipping_method_id')->unsigned();
            $table->smallInteger('thai_shipping_method_sub_id')->unsigned()->nullable();
            $table->string('thai_shipping_method_sub_title')->nullable();
            $table->double('thai_shipping_method_charge', 8, 2);
            $table->double('thai_shipping_method_price', 8, 2);
            $table->string('thai_shipping_method_note')->nullable();
            $table->double('thai_shipping_raw_price', 8, 2)->nullable();
            $table->double('thai_shipping_raw_charge', 8, 2)->nullable();
            $table->datetime('thai_shipping_date')->nullable();
            $table->string('thai_shipping_time', 10)->nullable();
            $table->string('thai_shipping_code', 50)->nullable();
            $table->text('thai_shipping_detail')->nullable();
            $table->smallInteger('thai_shipping_admin_id')->unsigned()->nullable();
            $table->string('thai_shipping_admin')->nullable();
            $table->datetime('thai_shipped_at')->nullable();
            $table->string('other_charge_title1', 100)->nullable();
            $table->double('other_charge_price1', 8, 2)->nullable();
            $table->string('other_charge_title2', 100)->nullable();
            $table->double('other_charge_price2', 8, 2)->nullable();
            $table->string('discount_title1', 100)->nullable();
            $table->double('discount_price1', 8, 2)->nullable();
            $table->string('discount_title2', 100)->nullable();
            $table->double('discount_price2', 8, 2)->nullable();
            $table->string('addon_refund_title', 100)->nullable();
            $table->double('addon_refund_credit', 8, 2)->nullable();
            $table->double('total_price', 8, 2)->nullable();
            $table->double('sub_total', 8, 2);
            $table->double('total_weight', 8, 2);
            $table->double('total_cubic', 10, 4);
            $table->integer('status')->default(1);
            $table->text('user_remark')->nullable();
            $table->text('admin_remark')->nullable();
            $table->text('admin_thai_shipping_remark')->nullable();
            $table->text('admin_packing_remark')->nullable();
            $table->text('system_remark')->nullable();
            $table->string('user_code')->nullable();
            $table->integer('user_address_id')->unsigned()->nullable();
            $table->string('coupon_code')->nullable();
            $table->smallInteger('coupon_id')->unsigned()->nullable();
            $table->double('coupon_price', 8, 2)->nullable();
            $table->string('shipping_name')->nullable();
            $table->string('shipping_tel', 20)->nullable();
            $table->string('shipping_address')->nullable();
            $table->string('shipping_district_code')->nullable();
            $table->smallInteger('shipping_amphur_id')->unsigned()->nullable();
            $table->smallInteger('shipping_province_id')->unsigned()->nullable();
            $table->string('shipping_zipcode')->nullable();
            $table->smallInteger('withholding')->unsigned()->default(0);
            $table->double('withholding_amount', 8, 2)->default(0.00);
            $table->string('created_admin')->nullable();
            $table->string('updated_admin')->nullable();
            $table->integer('created_admin_id')->nullable();
            $table->integer('updated_admin_id')->nullable();
            $table->datetime('approved_at')->nullable();
            $table->string('approved_admin')->nullable();
            $table->integer('approved_admin_id')->unsigned()->nullable();
            $table->string('printed_admin')->nullable();
            $table->datetime('printed_at')->nullable();
            $table->smallInteger('printed_admin_id')->unsigned()->nullable();
            $table->smallInteger('droppoint_id')->unsigned()->nullable();
            $table->datetime('droppoint_at')->nullable();
            $table->text('droppoint_remark')->nullable();
            $table->string('droppoint_admin')->nullable();
            $table->integer('droppoint_admin_id')->unsigned()->nullable();
            $table->smallInteger('droppoint_noti')->default(0);
            $table->double('exchange_rate', 8, 2)->default(0.00);
            $table->double('cost_box_china', 8, 2)->default(0.00);
            $table->double('cost_shipping_china', 8, 2)->default(0.00);
            $table->double('cost_box_thai', 8, 2)->default(0.00);
            $table->double('cost_shipping_thai', 8, 2)->default(0.00);
            $table->smallInteger('printed_thai_shipping')->nullable()->comment('ทำเครื่องหมายว่าพริ้นใบคุม');
            $table->string('sign_image')->nullable();
            $table->string('driver_admin')->nullable();
            $table->smallInteger('driver_admin_id')->unsigned()->nullable();
            $table->smallInteger('mark')->nullable();
            $table->datetime('marked_at')->nullable();
            $table->string('marked_user')->nullable();
            $table->integer('marked_user_id')->unsigned()->nullable();
            $table->integer('commission_rate_id')->unsigned()->nullable();
            $table->double('latitude', 11, 8)->nullable();
            $table->double('longitude', 11, 8)->nullable();
            $table->smallInteger('truck_id')->unsigned()->nullable();
            $table->smallInteger('driver2_admin_id')->unsigned()->nullable();
            $table->string('driver2_admin')->nullable();
            $table->smallInteger('tcatexpress_address_api')->default(0);
            $table->integer('commission_admin_id')->nullable();
            $table->datetime('succeed_at')->nullable();
            $table->integer('commission_id')->unsigned()->nullable();
            $table->double('commission_amount', 8, 2)->nullable();
            $table->datetime('paid_at')->nullable();
            $table->timestamp('peak_at')->nullable();
            $table->string('peak_code')->nullable();
            $table->enum('peak_status', ['wait', 'active', 'void'])->default('wait');
            $table->double('peak_amount', 8, 2)->default(0.00);
            $table->string('peak_remark')->nullable();

            $table->foreign('thai_shipping_method_id')->references('id')->on('thai_shipping_methods')->onDelete('RESTRICT')->onUpdate('RESTRICT');
            $table->foreign('user_code')->references('code')->on('users')->onDelete('RESTRICT')->onUpdate('cascade');
            $table->foreign('user_address_id')->references('id')->on('user_addresses')->onDelete('RESTRICT')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bills');
    }
}
