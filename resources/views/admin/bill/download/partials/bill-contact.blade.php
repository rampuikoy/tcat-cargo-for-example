<table class="contact" aria-describedby="bill-contact"style="border: 1px solid #bbb;" >
	<thead>
		<tr>
			<th colspan="6" scope="col" class="text-center">
				<div style="padding-top: 3px;">ติดต่อฝ่ายบริการลูกค้า</div>
			</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="text-right" width="12%"><div>เว็บไซต์&nbsp;:</div></td>
			<td width="10%"><div>www.tcatcargo.com</div></td>
			<td class="text-right" ><div>Line&nbsp;:</div></td>
			<td ><div>@tcatcargo</div></td>
			<td class="text-right" ><div>facebook&nbsp;:</div></td>
			<td ><div>tcatcargo</div></td>		
		</tr>
		<tr>	
			<td class="text-right"><div>ฝ่ายบริการลูกค้า&nbsp;:</div></td>
			<td ><div>080-003-2233</div></td>
			<td class="text-right"><div>โกดังหลักสินค้า&nbsp;:</div></td>
			<td><div>080-002-3636</div></td>		
			<td class="text-right"><div>โอนเงินไปจีน&nbsp;:</div></td>
			<td><div>080-003-2828</div></td>		
		</tr>
		<tr>
			<td class="text-right" style="vertical-align: top;"><div >ฝ่ายไกด์-ทริป&nbsp;:</div></td>
			<td style="vertical-align: top;"><div>088-333-7979</div></td>
			<td class="text-right" style="vertical-align: top;"><div>ฝ่ายลูกค้าปิดตู้&nbsp;:</div></td>
			<td ><div>085-977-6543,&nbsp;<br>085-722-7222</div></td>
			<td class="text-right" style="vertical-align: top;"><div>เบอร์ร้องเรียน&nbsp;:</div></td>
			<td style="vertical-align: top;"><div>092-254-8000</div></td>
		</tr>
		<tr>
			<td colspan="6" style="height: 5px;"></td>
		</tr>
	</tbody>
</table>