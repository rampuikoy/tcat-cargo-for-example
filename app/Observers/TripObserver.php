<?php

namespace App\Observers;

use App\Models\Trip;
use App\Jobs\JobSendFcm;
use App\Jobs\JobSendSms;
use App\Jobs\JobSendEmail;
use App\Observers\BaseObserver;

class TripObserver extends BaseObserver
{
    /**
     * Handle the trip "creating" event.
     *
     * @param  \App\Models\Trip  $trip
     * @return void
     */
    public function creating(Trip $trip)
    {
        //
        $this->stampCreated($trip);
    }

    /**
     * Handle the trip "updating" event.
     *
     * @param  \App\Models\Trip  $trip
     * @return void
     */
    public function updating(Trip $trip)
    {
        //
        $this->stampUpdated($trip);

        if ($trip->isDirty('status')) {
            $user       = $trip->user;
            $total      = number_format($trip->total, 2);

            if ($trip->status == 2) {
                $email      = $trip->emails()->create([
                    'mailable_class'    => 'App\Mail\Trip\TripCreated',
                    'status'            => 'queue',
                    'to'                => $user->email,
                    'created_admin'     => $trip->created_admin,
                    'created_admin_id'  => $trip->created_admin_id,
                ]);
                JobSendEmail::dispatch($email->id)->onQueue(env('QUEUE_EMAIL_PREFIX'));

                $sms        = $trip->sms()->create([
                    'status'            => 'queue',
                    'user_code'         => $user->code,
                    'msisdn'            => $user->tel1,
                    'message'           => __('sms.trip_created', [
                        'no' => $trip->no,
                        'total' => $total,
                    ]),
                    'created_admin'     => $trip->created_admin,
                    'created_admin_id'  => $trip->created_admin_id,
                ]);
                JobSendSms::dispatch($sms->id)->onQueue(env('QUEUE_SMS_PREFIX'));
            } elseif ($trip->status == 3) {
                $email      = $trip->emails()->create([
                    'mailable_class'    => 'App\Mail\Trip\TripPaid',
                    'status'            => 'queue',
                    'to'                => $user->email,
                    'created_admin'     => $trip->created_admin,
                    'created_admin_id'  => $trip->created_admin_id,
                ]);
                JobSendEmail::dispatch($email->id)->onQueue(env('QUEUE_EMAIL_PREFIX'));

                $sms        = $trip->sms()->create([
                    'status'            => 'queue',
                    'user_code'         => $user->code,
                    'msisdn'            => $user->tel1,
                    'message'           => __('sms.trip_paid', [
                        'no' => $trip->no,
                        'total' => $total,
                    ]),
                    'created_admin'     => $trip->created_admin,
                    'created_admin_id'  => $trip->created_admin_id,
                ]);
                JobSendSms::dispatch($sms->id)->onQueue(env('QUEUE_SMS_PREFIX'));
            } elseif ($trip->status == 5) {
                $email      = $trip->emails()->create([
                    'mailable_class'    => 'App\Mail\Trip\TripCancel',
                    'status'            => 'queue',
                    'to'                => $user->email,
                    'created_admin'     => $trip->created_admin,
                    'created_admin_id'  => $trip->created_admin_id,
                ]);
                JobSendEmail::dispatch($email->id)->onQueue(env('QUEUE_EMAIL_PREFIX'));

                $sms        = $trip->sms()->create([
                    'status'            => 'queue',
                    'user_code'         => $user->code,
                    'msisdn'            => $user->tel1,
                    'message'           => __('sms.trip_cancel', [
                        'no' => $trip->no,
                        'total' => $total,
                    ]),
                    'created_admin'     => $trip->created_admin,
                    'created_admin_id'  => $trip->created_admin_id,
                ]);
                JobSendSms::dispatch($sms->id)->onQueue(env('QUEUE_SMS_PREFIX'));
            }
        }
    }
}
