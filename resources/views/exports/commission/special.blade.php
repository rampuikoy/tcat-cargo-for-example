<table cellspacing="0" border="1">
  <caption>export comission rate special</caption>
  <thead>
    <tr>
      <th scope="col" style="border:1px solid #000000;background-color:#afd095; font-weight: bold;" colspan="71">
        ใบรายงานค่าคอมเซลล์
      </th>
    </tr>
  </thead>
  <tbody>

    <!-- Sale header -->
    <tr>
      <td colspan="2" style="border:1px solid #000000;text-align:center;background-color:#f4b183; font-weight: bold;">ชื่อเซลล์</td>
      <td rowspan="4" style="border:1px solid #000000;"></td>
      <td colspan="4" style="border:1px solid #000000;text-align:center;background-color:#f4b183; font-weight: bold;">ยอดรวมสุทธิ</td>
      <td colspan="2" style="border:1px solid #000000;text-align:center;background-color:#f4b183; font-weight: bold;">ทางรถ/ทั่วไป</td>
      <td colspan="2" style="border:1px solid #000000;text-align:center;background-color:#f4b183; font-weight: bold;">ทางรถ/อย.มอก.</td>
      <td colspan="2" style="border:1px solid #000000;text-align:center;background-color:#f4b183; font-weight: bold;">ทางเรือ/ทั่วไป</td>
      <td colspan="2" style="border:1px solid #000000;text-align:center;background-color:#f4b183; font-weight: bold;">ทางเรือ/อย.มอก.</td>
      <td rowspan="4" style="border:1px solid #000000;"></td>
      <td colspan="6" style="border:1px solid #000000;text-align:center;background-color:#f4b183; font-weight: bold;">วันที่</td>
      <td rowspan="4" colspan="49" style="border:1px solid #000000;"></td>
    </tr>

    <!-- Sale sub header -->
    <tr>
      <td colspan="2" style="border:1px solid #000000;background-color:#f2f2f2; text-align: center; font-weight: bold;">
        {{ $commissions['sale']['username'] }}
      </td>
      <td style="border:1px solid #000000;text-align:center;background-color:#f8cbad; font-weight: bold;">รายจ่ายลูกค้า</td>
      <td style="border:1px solid #000000;text-align:center;background-color:#f8cbad; font-weight: bold;">รายรับบริษัท</td>
      <td style="border:1px solid #000000;text-align:center;background-color:#f8cbad; font-weight: bold;">สินค้าแบรนด์</td>
      <td style="border:1px solid #000000;text-align:center;background-color:#f8cbad; font-weight: bold;">รายรับเซลล์</td>
      <td style="border:1px solid #000000;text-align:center;background-color:#f8cbad; font-weight: bold;">KG</td>
      <td style="border:1px solid #000000;text-align:center;background-color:#f8cbad; font-weight: bold;">Q</td>
      <td style="border:1px solid #000000;text-align:center;background-color:#f8cbad; font-weight: bold;">KG</td>
      <td style="border:1px solid #000000;text-align:center;background-color:#f8cbad; font-weight: bold;">Q</td>
      <td style="border:1px solid #000000;text-align:center;background-color:#f8cbad; font-weight: bold;">KG</td>
      <td style="border:1px solid #000000;text-align:center;background-color:#f8cbad; font-weight: bold;">Q</td>
      <td style="border:1px solid #000000;text-align:center;background-color:#f8cbad; font-weight: bold;">KG</td>
      <td style="border:1px solid #000000;text-align:center;background-color:#f8cbad; font-weight: bold;">Q</td>
      <td style="border:1px solid #000000;text-align:center;background-color:#f8cbad; font-weight: bold;" colspan="3">วันที่เริ่มต้น</td>
      <td style="border:1px solid #000000;text-align:center;background-color:#f8cbad; font-weight: bold;" colspan="3">วันที่สิ้นสุด</td>
    </tr>

    <!-- Sale detail -->
    <tr>
      <td colspan="2" style="border:1px solid #000000;background-color:#f2f2f2; text-align: center; font-weight: bold;">
        {{ $commissions['sale']['name'] }}
      </td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">
        {{ $commissions['total_user_expense'] }}
      </td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">
        {{ $commissions['total_company_revenue'] }}
      </td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">
        {{ $commissions['total_sum_price_brand'] }}
      </td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">
        {{ $commissions['total_sale_revenue'] }}
      </td>
      <td style="border:1px solid #000000;">
        {{ $commissions['rate']['kg_car_genaral'] }}
      </td>
      <td style="border:1px solid #000000;">
        {{ $commissions['rate']['cubic_car_genaral'] }}
      </td>
      <td style="border:1px solid #000000;">
        {{ $commissions['rate']['kg_car_iso'] }}
      </td>
      <td style="border:1px solid #000000;">
        {{ $commissions['rate']['cubic_car_iso'] }}
      </td>
      <td style="border:1px solid #000000;">
        {{ $commissions['rate']['kg_ship_genaral'] }}
      </td>
      <td style="border:1px solid #000000;">
        {{ $commissions['rate']['cubic_ship_genaral'] }}
      </td>
      <td style="border:1px solid #000000;">
        {{ $commissions['rate']['kg_ship_iso'] }}
      </td>
      <td style="border:1px solid #000000;">
        {{ $commissions['rate']['cubic_ship_iso'] }}
      </td>
      <td colspan="3" style="border:1px solid #000000; text-align: center; font-weight: bold;">
        {{ $commissions['date_start'] }}
      </td>
      <td colspan="3" style="border:1px solid #000000; text-align: center; font-weight: bold;">
        {{ $commissions['date_end'] }}
      </td>
    </tr>

    <!-- blank row -->
    <tr style="height:20px;">
      <td colspan="2" style="border:1px solid #000000;"></td>
      <td colspan="12" style="border:1px solid #000000;"></td>
      <td colspan="6" style="border:1px solid #000000;"></td>
    </tr>

    <!-- Bill header -->
    <tr>
      <td rowspan="5" style="border:1px solid #000000;width:15px;background-color:#f4b183;text-align:center; font-weight: bold;">ลำดับที่
      </td>
      <td rowspan="5" style="border:1px solid #000000;width:15px;background-color:#f4b183;text-align:center; font-weight: bold;">รหัสลูกค้า
      </td>
      <td rowspan="5" style="border:1px solid #000000;width:15px;background-color:#f4b183;text-align:center; font-weight: bold;">เลขที่บิล
      </td>
      <td rowspan="5" style="border:1px solid #000000;width:15px;background-color:#f4b183;text-align:center; font-weight: bold;">
        รายจ่ายลูกค้า</td>
      <td rowspan="5" style="border:1px solid #000000;width:15px;background-color:#f4b183;text-align:center; font-weight: bold;">
        รายรับบริษัท</td>
      <td rowspan="5" style="border:1px solid #000000;width:15px;background-color:#f4b183;text-align:center; font-weight: bold;">
        สินค้าแบรนด์</td>
      <td rowspan="5" style="border:1px solid #000000;width:15px;background-color:#f4b183;text-align:center; font-weight: bold;">
        รายรับเซลล์</td>
      <td colspan="27" style="border:1px solid #000000;width:15px;background-color:#f4b183;text-align:center; font-weight: bold;">
        รายจ่ายลูกค้า</td>
      <td rowspan="5" style="border:1px solid #000000;"></td>
      <td colspan="27" style="border:1px solid #000000;width:15px;background-color:#f4b183;text-align:center; font-weight: bold;">
        รายรับบริษัท</td>
      <td rowspan="5" style="border:1px solid #000000;"></td>
      <td colspan="8" style="border:1px solid #000000;"></td>
    </tr>

    <!-- Bill sub header 1 -->
    <tr>
      <td colspan="13" style="border:1px solid #000000;background-color:#f4b183;text-align:center; font-weight: bold;">ทางรถ</td>
      <td colspan="13" style="border:1px solid #000000;background-color:#f4b183;text-align:center; font-weight: bold;">ทางเรือ</td>
      <td rowspan="4" style="border:1px solid #000000;background-color:#f4b183;text-align:center; font-weight: bold;">ยอดบิล</td>
      <td colspan="13" style="border:1px solid #000000;background-color:#f4b183;text-align:center; font-weight: bold;">ทางรถ</td>
      <td colspan="13" style="border:1px solid #000000;background-color:#f4b183;text-align:center; font-weight: bold;">ทางเรือ</td>
      <td rowspan="4" style="border:1px solid #000000;background-color:#f4b183;text-align:center; font-weight: bold;">ยอดรวม</td>
      <td colspan="8" style="border:1px solid #000000;background-color:#f4b183;text-align:center; font-weight: bold;">เรทค่าคอมฯ</td>
    </tr>

    <!-- Bill sub header 2 -->
    <tr>
      <td colspan="6" style="border:1px solid #000000;background-color:#f4b183;text-align:center; font-weight: bold;">สินค้าทั่วไป</td>
      <td colspan="6" style="border:1px solid #000000;background-color:#f4b183;text-align:center; font-weight: bold;">อย. มอก.</td>
      <td rowspan="3" style="border:1px solid #000000;background-color:#f4b183;text-align:center; font-weight: bold;">ยอดรวม</td>
      <td colspan="6" style="border:1px solid #000000;background-color:#f4b183;text-align:center; font-weight: bold;">สินค้าทั่วไป</td>
      <td colspan="6" style="border:1px solid #000000;background-color:#f4b183;text-align:center; font-weight: bold;">อย. มอก.</td>
      <td rowspan="3" style="border:1px solid #000000;background-color:#f4b183;text-align:center; font-weight: bold;">ยอดรวม</td>
      <td colspan="6" style="border:1px solid #000000;background-color:#f4b183;text-align:center; font-weight: bold;">สินค้าทั่วไป</td>
      <td colspan="6" style="border:1px solid #000000;background-color:#f4b183;text-align:center; font-weight: bold;">อย. มอก.</td>
      <td rowspan="3" style="border:1px solid #000000;background-color:#f4b183;text-align:center; font-weight: bold;">ยอดรวม</td>
      <td colspan="6" style="border:1px solid #000000;background-color:#f4b183;text-align:center; font-weight: bold;">สินค้าทั่วไป</td>
      <td colspan="6" style="border:1px solid #000000;background-color:#f4b183;text-align:center; font-weight: bold;">อย. มอก.</td>
      <td rowspan="3" style="border:1px solid #000000;background-color:#f4b183;text-align:center; font-weight: bold;">ยอดรวม</td>
      <td colspan="4" style="border:1px solid #000000;background-color:#f4b183;text-align:center; font-weight: bold;">ทางรถ</td>
      <td colspan="4" style="border:1px solid #000000;background-color:#f4b183;text-align:center; font-weight: bold;">ทางเรือ</td>
    </tr>

    <!-- Bill sub header 3 -->
    <tr>
      <td colspan="3" style="border:1px solid #000000;background-color:#f8cbad;text-align:center; font-weight: bold;">น้ำหนัก(KG)</td>
      <td colspan="3" style="border:1px solid #000000;background-color:#f8cbad;text-align:center; font-weight: bold;">ปริมาตร (Q)</td>
      <td colspan="3" style="border:1px solid #000000;background-color:#f8cbad;text-align:center; font-weight: bold;">น้ำหนัก(KG)</td>
      <td colspan="3" style="border:1px solid #000000;background-color:#f8cbad;text-align:center; font-weight: bold;">ปริมาตร (Q)</td>
      <td colspan="3" style="border:1px solid #000000;background-color:#f8cbad;text-align:center; font-weight: bold;">น้ำหนัก(KG)</td>
      <td colspan="3" style="border:1px solid #000000;background-color:#f8cbad;text-align:center; font-weight: bold;">ปริมาตร (Q)</td>
      <td colspan="3" style="border:1px solid #000000;background-color:#f8cbad;text-align:center; font-weight: bold;">น้ำหนัก(KG)</td>
      <td colspan="3" style="border:1px solid #000000;background-color:#f8cbad;text-align:center; font-weight: bold;">ปริมาตร (Q)</td>
      <td colspan="3" style="border:1px solid #000000;background-color:#f8cbad;text-align:center; font-weight: bold;">น้ำหนัก(KG)</td>
      <td colspan="3" style="border:1px solid #000000;background-color:#f8cbad;text-align:center; font-weight: bold;">ปริมาตร (Q)</td>
      <td colspan="3" style="border:1px solid #000000;background-color:#f8cbad;text-align:center; font-weight: bold;">น้ำหนัก(KG)</td>
      <td colspan="3" style="border:1px solid #000000;background-color:#f8cbad;text-align:center; font-weight: bold;">ปริมาตร (Q)</td>
      <td colspan="3" style="border:1px solid #000000;background-color:#f8cbad;text-align:center; font-weight: bold;">น้ำหนัก(KG)</td>
      <td colspan="3" style="border:1px solid #000000;background-color:#f8cbad;text-align:center; font-weight: bold;">ปริมาตร (Q)</td>
      <td colspan="3" style="border:1px solid #000000;background-color:#f8cbad;text-align:center; font-weight: bold;">น้ำหนัก(KG)</td>
      <td colspan="3" style="border:1px solid #000000;background-color:#f8cbad;text-align:center; font-weight: bold;">ปริมาตร (Q)</td>
      <td colspan="2" style="border:1px solid #000000;background-color:#f8cbad;text-align:center; font-weight: bold;">สินค้าทั่วไป</td>
      <td colspan="2" style="border:1px solid #000000;background-color:#f8cbad;text-align:center; font-weight: bold;">อย. มอก.</td>
      <td colspan="2" style="border:1px solid #000000;background-color:#f8cbad;text-align:center; font-weight: bold;">สินค้าทั่วไป</td>
      <td colspan="2" style="border:1px solid #000000;background-color:#f8cbad;text-align:center; font-weight: bold;">อย. มอก.</td>
    </tr>

    <!-- Bill sub header 4 -->
    <tr>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">จำนวน</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">เรท</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">ยอด</td>

      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">จำนวน</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">เรท</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">ยอด</td>

      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">จำนวน</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">เรท</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">ยอด</td>

      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">จำนวน</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">เรท</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">ยอด</td>

      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">จำนวน</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">เรท</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">ยอด</td>

      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">จำนวน</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">เรท</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">ยอด</td>

      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">จำนวน</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">เรท</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">ยอด</td>

      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">จำนวน</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">เรท</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">ยอด</td>

      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">จำนวน</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">เรท</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">ยอด</td>

      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">จำนวน</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">เรท</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">ยอด</td>

      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">จำนวน</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">เรท</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">ยอด</td>

      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">จำนวน</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">เรท</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">ยอด</td>

      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">จำนวน</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">เรท</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">ยอด</td>

      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">จำนวน</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">เรท</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">ยอด</td>

      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">จำนวน</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">เรท</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">ยอด</td>

      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">จำนวน</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">เรท</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">ยอด</td>

      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">Q</td>
      <td style="border:1px solid #000000;text-align:right;background-color:#d9d9d9; font-weight: bold;">KG</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">Q</td>
      <td style="border:1px solid #000000;text-align:right;background-color:#d9d9d9; font-weight: bold;">KG</td>

      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">Q</td>
      <td style="border:1px solid #000000;text-align:right;background-color:#d9d9d9; font-weight: bold;">KG</td>
      <td style="border:1px solid #000000;text-align:right; font-weight: bold;">Q</td>
      <td style="border:1px solid #000000;text-align:right;background-color:#d9d9d9; font-weight: bold;">KG</td>
    </tr>

    <!-- Bill detail -->
    @foreach($commissions['bill'] as $index => $bill)
      <tr>
        <td style="border:1px solid #000000;background-color: #ffe699;text-align: center;">{{ $index + 1 }}</td>
        <td style="border:1px solid #000000;background-color: #ffe699;text-align: center;font-weight: bold;">
          {{ $bill->user_code }}
        </td>
        <td style="border:1px solid #000000;background-color: #ffe699;text-align: center;font-weight: bold;">
          {{ $bill->no }}
        </td>
        <td style="border:1px solid #000000;font-weight: bold;">
          {{ $bill->user_expense > 0 ? $bill->user_expense : null }}
        </td>
        <td style="border:1px solid #000000;font-weight: bold;">
          {{ $bill->company_revenue > 0 ? $bill->company_revenue : null }}
        </td>
        <td style="border:1px solid #000000;font-weight: bold;">
          {{ $bill->sum_price_brand > 0 ? $bill->sum_price_brand : null }}
        </td>
        <td style="border:1px solid #000000;font-weight: bold;">
          {{ $bill->sale_revenue > 0 ? $bill->sale_revenue : null }}
        </td>
        <!-- Car Genaral KG -->
        <td style="border:1px solid #000000;">
          {{ @$bill->user_shipping_rate['sum_unit_kg_car_genaral'] > 0 ? $bill->user_shipping_rate['sum_unit_kg_car_genaral'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->user_shipping_rate['rate_kg_car_genaral'] > 0 ? $bill->user_shipping_rate['rate_kg_car_genaral'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->user_shipping_rate['sum_price_kg_car_genaral'] > 0 ? $bill->user_shipping_rate['sum_price_kg_car_genaral'] : null }}
        </td>
        <!-- Car Genaral CBM -->
        <td style="border:1px solid #000000;">
          {{ @$bill->user_shipping_rate['sum_unit_cubic_car_genaral'] > 0 ? $bill->user_shipping_rate['sum_unit_cubic_car_genaral'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->user_shipping_rate['rate_cubic_car_genaral'] > 0 ? $bill->user_shipping_rate['rate_cubic_car_genaral'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->user_shipping_rate['sum_price_cubic_car_genaral'] > 0 ? $bill->user_shipping_rate['sum_price_cubic_car_genaral'] : null }}
        </td>
        <!-- Car Iso KG -->
        <td style="border:1px solid #000000;">
          {{ @$bill->user_shipping_rate['sum_unit_kg_car_iso'] > 0 ? $bill->user_shipping_rate['sum_unit_kg_car_iso'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->user_shipping_rate['rate_kg_car_iso'] > 0 ? $bill->user_shipping_rate['rate_kg_car_iso'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->user_shipping_rate['sum_price_kg_car_iso'] > 0 ? $bill->user_shipping_rate['sum_price_kg_car_iso'] : null }}
        </td>
        <!-- Car Iso CBM -->
        <td style="border:1px solid #000000;">
          {{ @$bill->user_shipping_rate['sum_unit_cubic_car_iso'] > 0 ? $bill->user_shipping_rate['sum_unit_cubic_car_iso'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->user_shipping_rate['rate_cubic_car_iso'] > 0 ? $bill->user_shipping_rate['rate_cubic_car_iso'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->user_shipping_rate['sum_price_cubic_car_iso'] > 0 ? $bill->user_shipping_rate['sum_price_cubic_car_iso'] : null }}
        </td>
        <!-- Car Total -->
        <td style="border:1px solid #000000; background-color: #c6e0b4; font-weight: bold;">
          {{ @$bill->user_shipping_rate['car_total'] > 0 ? $bill->user_shipping_rate['car_total'] : null }}
        </td>
        <!-- Ship Genaral KG -->
        <td style="border:1px solid #000000;">
          {{ @$bill->user_shipping_rate['sum_unit_kg_ship_genaral'] > 0 ? $bill->user_shipping_rate['sum_unit_kg_ship_genaral'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->user_shipping_rate['rate_kg_ship_genaral'] > 0 ? $bill->user_shipping_rate['rate_kg_ship_genaral'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->user_shipping_rate['sum_price_kg_ship_genaral'] > 0 ? $bill->user_shipping_rate['sum_price_kg_ship_genaral'] : null }}
        </td>
        <!-- Ship Genaral Cubic -->
        <td style="border:1px solid #000000;">
          {{ @$bill->user_shipping_rate['sum_unit_cubic_ship_genaral'] > 0 ? $bill->user_shipping_rate['sum_unit_cubic_ship_genaral'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->user_shipping_rate['rate_cubic_ship_genaral'] > 0 ? $bill->user_shipping_rate['rate_cubic_ship_genaral'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->user_shipping_rate['sum_price_cubic_ship_genaral'] > 0 ? $bill->user_shipping_rate['sum_price_cubic_ship_genaral'] : null }}
        </td>
        <!-- Ship Iso KG -->
        <td style="border:1px solid #000000;">
          {{ @$bill->user_shipping_rate['sum_unit_kg_ship_iso'] > 0 ? $bill->user_shipping_rate['sum_unit_kg_ship_iso'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->user_shipping_rate['rate_kg_ship_iso'] > 0 ? $bill->user_shipping_rate['rate_kg_ship_iso'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->user_shipping_rate['sum_price_kg_ship_iso'] > 0 ? $bill->user_shipping_rate['sum_price_kg_ship_iso'] : null }}
        </td>
        <!-- Ship Iso Cubic -->
        <td style="border:1px solid #000000;">
          {{ @$bill->user_shipping_rate['sum_unit_cubic_ship_iso'] > 0 ? $bill->user_shipping_rate['sum_unit_cubic_ship_iso'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->user_shipping_rate['rate_cubic_ship_iso'] > 0 ? $bill->user_shipping_rate['rate_cubic_ship_iso'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->user_shipping_rate['sum_price_cubic_ship_iso'] > 0 ? $bill->user_shipping_rate['sum_price_cubic_ship_iso'] : null }}
        </td>
        <!-- Ship Total -->
        <td style="border:1px solid #000000; background-color: #c6e0b4; font-weight: bold;">
          {{ @$bill->user_shipping_rate['ship_total'] > 0 ? $bill->user_shipping_rate['ship_total'] : null }}
        </td>
        <!-- Bill Total -->
        <td style="border:1px solid #000000; background-color: #92d050; font-weight: bold;">
          {{ @$bill->user_shipping_rate['bill_total'] > 0 ? $bill->user_shipping_rate['bill_total'] : null }}
        </td>
        <td style="border:1px solid #000000;"></td>
        <!-- Car Genaral KG -->
        <td style="border:1px solid #000000;">
          {{ @$bill->company_shipping_rate['sum_unit_kg_car_genaral'] > 0 ? $bill->company_shipping_rate['sum_unit_kg_car_genaral'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->company_shipping_rate['rate_kg_car_genaral'] > 0 ? $bill->company_shipping_rate['rate_kg_car_genaral'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->company_shipping_rate['sum_price_kg_car_genaral'] > 0 ? $bill->company_shipping_rate['sum_price_kg_car_genaral'] : null }}
        </td>
        <!-- Car Genaral CBM -->
        <td style="border:1px solid #000000;">
          {{ @$bill->company_shipping_rate['sum_unit_cubic_car_genaral'] > 0 ? $bill->company_shipping_rate['sum_unit_cubic_car_genaral'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->company_shipping_rate['rate_cubic_car_genaral'] > 0 ? $bill->company_shipping_rate['rate_cubic_car_genaral'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->company_shipping_rate['sum_price_cubic_car_genaral'] > 0 ? $bill->company_shipping_rate['sum_price_cubic_car_genaral'] : null }}
        </td>
        <!-- Car Iso KG -->
        <td style="border:1px solid #000000;">
          {{ @$bill->company_shipping_rate['sum_unit_kg_car_iso'] > 0 ? $bill->company_shipping_rate['sum_unit_kg_car_iso'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->company_shipping_rate['rate_kg_car_iso'] > 0 ? $bill->company_shipping_rate['rate_kg_car_iso'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->company_shipping_rate['sum_price_kg_car_iso'] > 0 ? $bill->company_shipping_rate['sum_price_kg_car_iso'] : null }}
        </td>
        <!-- Car Iso CBM -->
        <td style="border:1px solid #000000;">
          {{ @$bill->company_shipping_rate['sum_unit_cubic_car_iso'] > 0 ? $bill->company_shipping_rate['sum_unit_cubic_car_iso'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->company_shipping_rate['rate_cubic_car_iso'] > 0 ? $bill->company_shipping_rate['rate_cubic_car_iso'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->company_shipping_rate['sum_price_cubic_car_iso'] > 0 ? $bill->company_shipping_rate['sum_price_cubic_car_iso'] : null }}
        </td>
        <!-- Car Total -->
        <td style="border:1px solid #000000; background-color: #c6e0b4; font-weight: bold;">
          {{ @$bill->company_shipping_rate['car_total'] > 0 ? $bill->company_shipping_rate['car_total'] : null }}
        </td>
        <!-- Ship Genaral KG -->
        <td style="border:1px solid #000000;">
          {{ @$bill->company_shipping_rate['sum_unit_kg_ship_genaral'] > 0 ? $bill->company_shipping_rate['sum_unit_kg_ship_genaral'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->company_shipping_rate['rate_kg_ship_genaral'] > 0 ? $bill->company_shipping_rate['rate_kg_ship_genaral'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->company_shipping_rate['sum_price_kg_ship_genaral'] > 0 ? $bill->company_shipping_rate['sum_price_kg_ship_genaral'] : null }}
        </td>
        <!-- Ship Genaral Cubic -->
        <td style="border:1px solid #000000;">
          {{ @$bill->company_shipping_rate['sum_unit_cubic_ship_genaral'] > 0 ? $bill->company_shipping_rate['sum_unit_cubic_ship_genaral'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->company_shipping_rate['rate_cubic_ship_genaral'] > 0 ? $bill->company_shipping_rate['rate_cubic_ship_genaral'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->company_shipping_rate['sum_price_cubic_ship_genaral'] > 0 ? $bill->company_shipping_rate['sum_price_cubic_ship_genaral'] : null }}
        </td>
        <!-- Ship Iso KG -->
        <td style="border:1px solid #000000;">
          {{ @$bill->company_shipping_rate['sum_unit_kg_ship_iso'] > 0 ? $bill->company_shipping_rate['sum_unit_kg_ship_iso'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->company_shipping_rate['rate_kg_ship_iso'] > 0 ? $bill->company_shipping_rate['rate_kg_ship_iso'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->company_shipping_rate['sum_price_kg_ship_iso'] > 0 ? $bill->company_shipping_rate['sum_price_kg_ship_iso'] : null }}
        </td>
        <!-- Ship Iso Cubic -->
        <td style="border:1px solid #000000;">
          {{ @$bill->company_shipping_rate['sum_unit_cubic_ship_iso'] > 0 ? $bill->company_shipping_rate['sum_unit_cubic_ship_iso'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->company_shipping_rate['rate_cubic_ship_iso'] > 0 ? $bill->company_shipping_rate['rate_cubic_ship_iso'] : null }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->company_shipping_rate['sum_price_cubic_ship_iso'] > 0 ? $bill->company_shipping_rate['sum_price_cubic_ship_iso'] : null }}
        </td>
        <!-- Ship Total -->
        <td style="border:1px solid #000000; background-color: #c6e0b4; font-weight: bold;">
          {{ @$bill->company_shipping_rate['ship_total'] > 0 ? $bill->company_shipping_rate['ship_total'] : null }}
        </td>
        <!-- Bill total -->
        <td style="border:1px solid #000000; background-color: #92d050; font-weight: bold;">
          {{ @$bill->company_shipping_rate['bill_total'] > 0 ? $bill->company_shipping_rate['bill_total'] : null }}
        </td>
        <td style="border:1px solid #000000;"></td>
        <td style="border:1px solid #000000;">
          {{ @$bill->rate->kg_car_genaral }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->rate->cubic_car_genaral }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->rate->kg_car_iso }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->rate->cubic_car_iso }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->rate->kg_ship_genaral }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->rate->cubic_ship_genaral }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->rate->kg_ship_iso }}
        </td>
        <td style="border:1px solid #000000;">
          {{ @$bill->rate->cubic_ship_iso }}
        </td>
      </tr>
    @endforeach
  </tbody>
</table>
