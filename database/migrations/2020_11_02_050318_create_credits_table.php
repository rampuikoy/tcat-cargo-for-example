<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credits', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->timestamp('approved_at')->nullable();
            $table->string('creditable_type', 50)->nullable(false);
            $table->integer('creditable_id')->unsigned();
            $table->string('user_code', 20)->nullable();
            $table->double('amount', 10, 2)->unsigned();
            $table->double('before', 12, 2)->nullable();
            $table->double('after', 12, 2)->nullable();
            $table->enum('type', ['topup', 'withdraw'])->default('topup');
            $table->string('created_admin', 50)->nullable();
            $table->string('updated_admin', 50)->nullable();
            $table->string('approved_admin', 50)->nullable();
            $table->integer('created_admin_id')->unsigned()->nullable();
            $table->integer('updated_admin_id')->unsigned()->nullable();
            $table->integer('approved_admin_id')->unsigned()->nullable();

            $table->foreign('user_code')->references('code')->on('users')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('created_admin_id')->references('id')->on('admins')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('updated_admin_id')->references('id')->on('admins')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('approved_admin_id')->references('id')->on('admins')->onDelete('set null')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credits');
    }
}
