<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\UserAddress;
use App\Models\Admin;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserAddressTest extends TestCase
{
  use WithFaker;
  protected $user;
  protected $userAddress;
  protected $url = '/api/backend/user-address';
  /**
   * A basic feature test example.
   *
   * @return void
   */
  protected function authenticate(string $guard = 'admins')
  {
    $this->user = factory(Admin::class)->create();
    auth()->shouldUse($guard);
    $this->userAddress = factory(UserAddress::class)->create();
    $token = JWTAuth::fromUser($this->user);
    return 'Bearer ' . $token;
  }

  protected function deleteUser()
  {
    $this->userAddress->forceDelete();
    $this->user->forceDelete();
  }

  /** @test */
  public function GetUserAddressSearchSuccessTest()
  {
    $id = random_int(1, 200);
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', "{$this->url}/search", [
      "id" => $id,
      "user_code" => "",
      "name" => "",
      "tel" => "",
      "address" => "",
      "province_id" => "",
      "zipcode" => "",
      "status" => "",
      "user_remark" => "",
      "admin_remark" => "",
    ]);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function GetUserAddressSearchFailTest()
  {
    $response = $this->json('GET', "{$this->url}/search");
    $response->assertStatus(401);
  }

  /** @test */
  public function GetUserAddressDropdownSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', "{$this->url}/dropdown");
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function GetUserAddressDropdownFailTest()
  {
    $response = $this->json('GET', "{$this->url}/dropdown");
    $response->assertStatus(401);
  }

  /** @test */
  public function GetUserAddressAllSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', $this->url);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function GetUserAddressAllFailTest()
  {
    $response = $this->json('GET', $this->url);
    $response->assertStatus(401);
  }

  /** @test */
  public function UserAddressPostSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('POST', $this->url, [
      'user_code'     => 'SF0002',
      'name'          => 'test',
      'address'       => 'test',
      'province_id'   => '4',
      'amphur_id'     => '66',
      'district_code' => '130101',
      'zipcode'       => '12000',
    ]);
    $response->assertStatus(200);
    UserAddress::where('name', 'test')->forceDelete();
    $this->deleteUser();
  }

  /** @test */
  public function UserAddressPostFailAuthTest()
  {
    $response = $this->json('POST', $this->url);
    $response->assertStatus(401);
  }

  /** @test */
  public function UserAddressPostFailTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('POST', $this->url);
    $response->assertStatus(422);
    $this->deleteUser();
  }

  /** @test */
  public function UserAddressUpdateByIdSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('PUT', "{$this->url}/{$this->userAddress->id}", [
      'name'          => 'test',
      'address'       => 'test-test',
    ]);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function UserAddressUpdateByIdFailAuthTest()
  {
    $response = $this->json('PUT', "{$this->url}/10");
    $response->assertStatus(401);
  }

  /** @test */
  public function UserAddressUpdateByIdFailTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' =>  $token,
    ])->json('PUT', "{$this->url}/{$this->userAddress->id}");
    $response->assertStatus(422);
    $this->deleteUser();
  }

  /** @test */
  public function UserAddressDeleteSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('DELETE', "{$this->url}/{$this->userAddress->id}");
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function UserAddressDeleteFailTest()
  {
    $response = $this->json('DELETE', "{$this->url}/5");
    $response->assertStatus(401);
  }
}
