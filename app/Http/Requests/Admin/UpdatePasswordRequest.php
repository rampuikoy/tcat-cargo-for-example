<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\FormRequest;

class UpdatePasswordRequest extends FormRequest
{
    public function authorize()
    {
        return $this->user('admins')->can('backend.admin.update');
    }

    public function rules()
    {
        return [
            'password' => 'required|confirmed|min:4|max:20|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9]).*$/',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'password'      => __('form.admin.password'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
