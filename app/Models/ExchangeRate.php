<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExchangeRate extends Model
{
  use SoftDeletes;
  
  protected $table    = 'exchange_rates';

  protected $fillable = [
    'rate',
    'raw_rate',
    'created_admin_id',
    'created_admin'
  ];

  // query sql
  public function scopeSearch($query, $conditions)
  {
    return $query->where(function ($query) use ($conditions) {
      if (@$conditions['date']) {
        $date = date_create($conditions['date']);
        $query->whereDate('created_at', '=', date_format($date, 'Y-m-d H:i:s'));
      }
    });
  }
}
