<?php

namespace App\Repositories;

use App\Interfaces\ProvinceInterface;
use App\Models\Province;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use Illuminate\Support\Facades\Cache;

class ProvinceRepository implements ProvinceInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const MINUTE_CACHE = 1;

    public function __construct(Province $province)
    {
        $this->province = $province;
    }

    public function all(array $columns = ['*'], $sorting = ProvinceInterface::DEFAULT_SORTING, $ascending = ProvinceInterface::DEFAULT_ASCENDING)
    {
        $key = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time = $this->getTime(self::MINUTE_CACHE);

        $allProvince = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
            return $this->province::select($columns)->orderBy($sorting, $ascending)->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.province')]), $allProvince);
    }

    public function findById($id, array $columns = ['*'])
    {
        $key = $this->getCacheKey(null, $id);
        $time = $this->getTime(self::MINUTE_CACHE);

        $province = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($id)
        ])->remember($key, $time, function () use ($id, $columns) {
            return $this->province::select($columns)->whereId($id)->first();
        });

        if (!$province) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.province'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.province')]), $province);
    }
}
