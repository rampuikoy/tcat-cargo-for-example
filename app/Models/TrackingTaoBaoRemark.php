<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrackingTaoBaoRemark extends Model
{
    use HasFactory;
    protected $connection = 'taobaoconew';
    protected $table = 'tracking_remark';
    protected $fillable = [
                'tracking_id',
                'tracking_code',
                'remark_duplicate',
                'remark',
            ];
    public $timestamps = false;

    public function scopeSearch($query, $code)
    {
        return $query->where('tracking_code', $code);
    }
}
