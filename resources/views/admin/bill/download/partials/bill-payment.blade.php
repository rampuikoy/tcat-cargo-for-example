@php $payments = $bill->credits; @endphp
<table class="bordered" aria-describedby="bill-payment">
	<thead style="background: #dfdfdf;">
		<tr>
			<th scope="col" colspan="5">
				<div>การชำระเงิน</div>
			</th>
		</tr>
		<tr>
			<th scope="col" width="100">
				<div>วันที่ชำระบิล</div>
			</th>
			<th scope="col">
				<div>รายการ</div>
			</th>
			<th scope="col" class="text-right" width="90">
				<div>เครดิตก่อนชำระ(@lang('admin.thb'))</div>
			</th>
			<th scope="col" class="text-right" width="90">
				<div>เครดิตที่ใช้(@lang('admin.thb'))</div>
			</th>
			<th scope="col" class="text-right" width="90">
				<div>เครดิตคงเหลือ(@lang('admin.thb'))</div>
			</th>
		</tr>
	</thead>
	<tbody>
		@foreach($payments as $payment)
		<tr>
			<td class="text-center">
				<div>{!!str_replace(' ', '&nbsp;',$payment->created_at)!!}</div>
			</td>
			<td>
				<div>
					@if($payment->type == 'topup' && $payment->amount == $bill->addon_refund_credit)
					{!!str_replace(' ', '&nbsp;', $bill->addon_refund_title)!!} /
					@endif
					{!!str_replace(' ', '&nbsp;',$payment->description)!!}
				</div>
			</td>
			<td class="text-right">
				<div>{{number_format($payment->before, 2)}}</div>
			</td>
			<td class="text-right">
				<div>
					{{ ($payment->type=='topup')?'+' : '-' }}
					{{number_format($payment->amount, 2)}}
				</div>
			</td>
			<td class="text-right">
				<div>{{number_format($payment->after, 2)}}</div>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>