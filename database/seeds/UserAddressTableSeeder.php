<?php

use App\Helpers\MoveSqlData;
use App\Models\UserAddress;
use Illuminate\Database\Seeder;

class UserAddressTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $move_geo_data    = collect(MoveSqlData::insertUserAddress());
        $message          = $move_geo_data->get('message');
        $error            = $move_geo_data->get('error');

        if($error){
            $this->command->error($message);
            $this->command->warning('Please insert fresh again');
        }else{
            $this->command->info($message);
        }
        
        factory(UserAddress::class, 5)->create();
    }
}
