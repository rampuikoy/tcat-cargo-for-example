<?php

return [
    'user_registed'         => 'TCAT-CARGO บิลขนส่ง ยินดีต้อนรับค่ะ',
    'topup_approved'        => 'TCAT-CARGO เติมเครดิต แจ้งเติมสำเร็จ',
    'withdraw_approved'     => 'TCAT-CARGO บิลถอนเครดิต: :no แจ้งถอนสำเร็จ',
    'bill_payment'          => 'TCAT-CARGO บิลขนส่ง แจ้งชำระเงินแล้ว :no',
    'bill_created'          => 'TCAT-CARGO บิลขนส่ง แจ้งบิลใหม่ :no',
    'bill_shipped'          => 'TCAT-CARGO บิลขนส่ง แจ้งจัดส่งแล้ว :no',
    'bill_success'          => 'TCAT-CARGO บิลขนส่ง สรุปบิล :no',
    'bill_cancel'           => 'TCAT-CARGO บิลขนส่ง แจ้งยกเลิก :no',
    'bill_dropped'          => 'TCAT-CARGO บิลขนส่ง แจ้งรับสินค้า :no',

    'trip_created'          => 'China Trip Tour แจ้งบิลใหม่',
    'trip_paid'             => 'China Trip Tour แจ้งชำระเงินแล้ว',
    'trip_cancel'           => 'China Trip Tour แจ้งยกเลิกบิล',
    'trip_success'          => 'China Trip Tour แจ้งรายละเอียด',

    'point_order_success'   => 'TCAT-CARGO บิลแลกสินค้า :no สำเร็จแล้ว',
    'point_order_cancel'    => 'บิลแลกสินค้า :no ถูกยกเลิก',
];
