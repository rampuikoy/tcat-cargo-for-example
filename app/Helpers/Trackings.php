<?php

namespace App\Helpers;

use App\Models\TrackingTaoBao;
use App\Models\TrackingTmall;
use App\Models\TrackingTaoBaoRemark;
use App\Models\TrackingTaoBaoHasBill;
use App\Models\Tracking;
use \App\Helpers\Util;

class Trackings
{

    public static function checkTmallDuplicate(Tracking $tracking, $remark = true)
    {

        // if duplicated not do it again
        if ($tracking->duplicate_order) {
            return $tracking;
        }

        $tmall = TrackingTmall::search($tracking->code)->first();
        if (isset($tmall->id)) {
            if ($tracking->user_code && $remark) {
                // write tmall remark

                $tmall->remark = Util::concatField($tmall->remark, $tracking->user_code);
                $tmall->save();
            }
            // write system_remark

            $message = ($tmall->po) ? 'ซ้ำ ' . $tmall->po : '';
            $tracking->system_remark = Util::concatField($tracking->system_remark, $message);
            $tracking->duplicate_order = $tmall->po;
        }
        return $tracking;
    }

    public static function checkTaoBaoDuplicate(Tracking $tracking, $remark = true)
    {
        // if duplicated not do it again
        if ($tracking->duplicate_order) {
            return $tracking;
        }
        $taoBao = TrackingTaoBao::search($tracking->code)->first();
        if (isset($taoBao->id)) {

            if ($tracking->user_code && $remark) {
                // write taoBao remark

                $tracking_remark = TrackingTaoBaoRemark::firstOrNew([
                    'tracking_id' => $taoBao->id,
                    'tracking_code' => $taoBao->tracking,
                ]);

                $tracking_remark->remark_duplicate = Util::concatField($tracking_remark->remark_duplicate, $tracking->user_code);
                $tracking_remark->save();
            }

            $tracking_has_bill = TrackingTaoBaoHasBill::search($tracking->code)->first();
            if (isset($tracking_has_bill->tracking_code)) {
                // $tracking_has_bill->duplicate = 2; // ?
                $message = ($tracking_has_bill->bill_code) ? 'ซ้ำ ' . $tracking_has_bill->bill_code : '';
                $tracking->system_remark = Util::concatField($tracking->system_remark, $message);
                $tracking->duplicate_order = $tracking_has_bill->bill_code;
            }
        }
        return $tracking;
    }
}
