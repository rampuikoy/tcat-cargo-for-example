<?php

namespace App\Helpers;

use App\Models\Admin;
use App\Models\Role;
use App\Models\Tracking;
use App\Models\User;
use App\Models\UserAddress;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class MoveSqlData
{
    private static function getJsonByFile($path)
    {
        $fullPath = base_path($path);
        if (!file_exists($fullPath)) {
            return null;
        }

        $jsonString = file_get_contents($fullPath);
        $data = json_decode($jsonString, true);
        return @$data[2]['data'] ? $data[2]['data'] : [];
    }

    private static function getResourceName($key)
    {
        switch ($key) {
            case 'find':
                return 'index';
            case 'view':
                return 'show';
            case 'create':
                return 'store';
            case 'update':
                return 'update';
            case 'delete':
                return 'destroy';
            case 'approve':
                return 'approve';
            default:
                return;
        }
    }

    public static function insertAdminTable()
    {
        $admins = collect(self::getJsonByFile('database/sql/admins.json'));
        $results = collect(['error' => false, 'message' => null]);
        $success_rows = 0;
        $total_rows = $admins->count();

        foreach ($admins as $admin) {
            try {

                $duplicateEmail = DB::table('admins')->where(['email' => $admin['email']])->count();
                if ($duplicateEmail > 0 || $admin['email'] == null) {
                    $admin['email'] = 'example' . $admin['id'] . '@tcatcargo.com';
                }

                $create = DB::table('admins')->insert([
                    'id' => $admin['id'],
                    'username' => $admin['username'],
                    'email' => $admin['email'],
                    'email_verified_at' => $admin['created_at'],
                    'tel' => $admin['tel'],
                    'name' => $admin['title'],
                    'department' => $admin['department'],
                    'droppoint_id' => $admin['droppoint_id'],
                    'password' => $admin['password'],
                    'status' => $admin['status'],
                    'regex_user' => $admin['regex_user'],
                    'pattern_user' => ($admin['pattern_user'] != '') ? $admin['pattern_user'] : null,
                    'type' => $admin['type'],
                    'remark' => $admin['remark'],
                    'updated_admin_id' => $admin['updated_admin_id'],
                    'created_admin_id' => $admin['created_admin_id'],
                    'created_admin' => $admin['created_admin'],
                    'updated_admin' => $admin['updated_admin'],
                    'created_at' => $admin['created_at'],
                    'updated_at' => $admin['updated_at'],
                    'deleted_at' => $admin['status'] == 'inactive' ? $admin['updated_at'] : null,
                    'remember_token' => null,
                    'branch' => $admin['branch'],
                ]);

                if ($create) {
                    $success_rows += 1;
                }
            } catch (\Throwable $th) {
                return ['error' => true, 'message' => $th->getMessage()];
            }
        }

        if ($success_rows < $total_rows) {
            $results['error'] = true;
            $results['message'] = 'Not all data insert. accept: ' . $success_rows . ' | total: ' . $total_rows;
        } else {
            $results['error'] = false;
            $results['message'] = 'Insert all data success. accept: ' . $success_rows . ' | total: ' . $total_rows;
        }

        return $results;
    }

    public static function insertRoleTable()
    {
        try {
            $permissions = collect(self::getJsonByFile('database/sql/permissions.json'));
            $permission_role = collect(self::getJsonByFile('database/sql/permission_role.json'));
            $collection = collect(self::getJsonByFile('database/sql/roles.json'));
            $total_rows = $collection->count();
            $success_rows = 0;

            $roles = $collection->map(function ($role) use ($permission_role, $permissions) {
                $permission = $permission_role->where('role_id', $role['id'])
                    ->map(function ($item) use ($permissions) {
                        $permission = $permissions->firstWhere('id', $item['permission_id']);
                        if ($permission) {
                            $slugs = json_decode($permission['slug'], true);
                            $methods = [];
                            foreach ($slugs as $key => $slug) {
                                if ($permission['name'] == 'extends') {
                                    $newKey = 'extend.' . $key;
                                } else {
                                    $newKey = str_replace('office', 'warehouse', $permission['name']) . '.' . self::getResourceName($key);
                                }
                                if ($slug) {
                                    $newKey = 'backend.' . str_replace('_', '-', $newKey);
                                    $newKey = str_replace('backend.extend.', 'extend.', $newKey);
                                    array_push($methods, $newKey);
                                }
                            }
                            return $methods;
                        }
                    })
                    ->toArray();

                $permission = collect($permission)->reduce(function ($carry, $item) {
                    $carry = ($carry) ? $carry : [];
                    return array_merge($carry, $item);
                });

                $newItem = [
                    'name' => $role['name'],
                    'guard_name' => 'admins',
                    'remark' => 'อ้างอิงจาก Role (เก่า): ' . $role['name'] . ', (slug): ' . $role['slug'],
                    'permissions' => $permission,
                ];

                return $newItem;
            });

            foreach ($roles as $role) {
                $createRole = Role::create([
                    'name' => $role['name'],
                    'guard_name' => $role['guard_name'],
                    'remark' => $role['remark'],
                ]);

                $createRole->syncPermissions($role['permissions']);
                $success_rows += 1;
            }

            if ($success_rows < $total_rows) {
                return ['error' => true, 'message' => 'Not all role insert. accept: ' . $success_rows . ' | total: ' . $total_rows];
            } else {
                return ['error' => false, 'message' => 'Insert all role success. accept: ' . $success_rows . ' | total: ' . $total_rows];
            }
        } catch (\Throwable $th) {
            return ['error' => true, 'message' => $th->getMessage()];
        }
    }
    public static function insertModelHasRoleTable()
    {
        try {
            $roles = collect(self::getJsonByFile('database/sql/roles.json'));
            $collection = collect(self::getJsonByFile('database/sql/role_admins.json'));
            $total_rows = $collection->count();
            $success_rows = 0;

            $hasAdmins = $collection->map(function ($item) use ($roles) {
                $role = $roles->where('id', $item['role_id'])->first();
                $item['role_name'] = ($role) ? $role['name'] : null;
                return $item;
            });

            foreach ($hasAdmins as $hasAdmin) {
                $admin = Admin::where('id', $hasAdmin['admin_id'])->first();

                if (!$admin) {
                    $success_rows += 1;
                    continue;
                }
                if ($admin->update(['role' => $hasAdmin['role_name']])) {
                    $success_rows += 1;
                } else {
                    break;
                }
            }
            if ($success_rows < $total_rows) {
                return ['error' => true, 'message' => 'Not all role insert. accept: ' . $success_rows . ' | total: ' . $total_rows];
            } else {
                return ['error' => false, 'message' => 'Insert all role success. accept: ' . $success_rows . ' | total: ' . $total_rows];
            }
        } catch (\Throwable $th) {
            return ['error' => true, 'message' => $th->getMessage()];
        }
    }

    public static function insertLocation()
    {
        try {
            $provinces = collect(self::getJsonByFile('database/sql/provinces.json'));
            $amphurs = collect(self::getJsonByFile('database/sql/amphurs.json'));
            $districts = collect(self::getJsonByFile('database/sql/districts.json'));
            $zipcodes = collect(self::getJsonByFile('database/sql/zipcodes.json'));

            if ($provinces->count() === 0) {
                return ['error' => true, 'message' => 'provinces empty'];
            }
            if ($amphurs->count() === 0) {
                return ['error' => true, 'message' => 'amphurs empty'];
            }
            if ($districts->count() === 0) {
                return ['error' => true, 'message' => 'districts empty'];
            }
            if ($zipcodes->count() === 0) {
                return ['error' => true, 'message' => 'zipcodes empty'];
            }

            $province_status = (DB::table('provinces')->insert($provinces->toArray())) ? 'pass' : 'fail';
            $amphur_status = (DB::table('amphurs')->insert($amphurs->toArray())) ? 'pass' : 'fail';
            $district_status = (DB::table('districts')->insert($districts->toArray())) ? 'pass' : 'fail';
            $zipcode_status = (DB::table('zipcodes')->insert($zipcodes->toArray())) ? 'pass' : 'fail';

            return ['error' => false, 'message' => 'Insert province: ' . $province_status . ' | amphur: ' . $amphur_status . ' | district:' . $district_status . ' | zipcode:' . $zipcode_status];
        } catch (\Throwable $th) {
            return ['error' => true, 'message' => $th->getMessage()];
        }
    }

    public static function insertThaiShippingMethod()
    {
        try {
            $methods = collect(self::getJsonByFile('database/sql/thai_shipping_methods.json'));

            if ($methods->count() === 0) {
                return ['error' => true, 'message' => 'thai shipping methods empty'];
            }

            $methods = $methods->map(function ($method) {

                if ($method['latitude'] == '13°48\'47.5"N' && $method['longitude'] == '100°04\'10.7"E') {
                    $method['latitude'] = 13.8131996;
                    $method['longitude'] = 100.0674502;
                }

                return [
                    'id' => $method['id'],
                    'created_at' => $method['created_at'],
                    'updated_at' => $method['updated_at'],
                    'type' => $method['type'],
                    'status' => $method['status'],
                    'access' => $method['access'],
                    'title_th' => $method['title_th'],
                    'title_en' => $method['title_en'],
                    'title_cn' => $method['title_cn'],
                    'address' => $method['address'],
                    'detail' => $method['detail'],
                    'tel' => $method['tel'],
                    'province_id' => $method['province_id'],
                    'amphur_id' => $method['amphur_id'],
                    'district_code' => $method['district_code'],
                    'zipcode' => $method['zipcode'],
                    'latitude' => $method['latitude'] ? $method['latitude'] : null,
                    'longitude' => $method['longitude'] ? $method['longitude'] : null,
                    'calculate_type' => $method['calculate_type'],
                    'refund' => $method['refund'] ? $method['refund'] : null,
                    'shipping_rate_weight' => $method['shipping_rate_weight'] ? $method['shipping_rate_weight'] : 0,
                    'shipping_rate_cubic' => $method['shipping_rate_cubic'] ? $method['shipping_rate_cubic'] : 0,
                    'charge' => $method['charge'] ? $method['charge'] : 0,
                    'max_weight' => $method['max_weight'] ? $method['max_weight'] : 0,
                    'delivery' => $method['delivery'],
                    'min_price' => $method['min_price'],
                    'max_distance' => $method['max_distance'] ? $method['max_distance'] : null,
                    'delivery_rate_km' => $method['delivery_rate_km'] ? $method['delivery_rate_km'] : 0,
                    'delivery_rate_kg' => $method['delivery_rate_kg'] ? $method['delivery_rate_kg'] : 0,
                    'delivery_rate_cubic' => $method['delivery_rate_cubic'] ? $method['delivery_rate_cubic'] : 0,
                    'remark' => $method['remark'],
                    'api' => $method['api'],
                    'remark' => $method['remark'],
                    'created_admin_id' => $method['created_admin_id'],
                    'updated_admin_id' => $method['updated_admin_id'],
                    'created_admin' => $method['created_admin'],
                    'updated_admin' => $method['updated_admin'],
                    'deleted_at' => $method['status'] == 'inactive' ? $method['updated_at'] : null,
                ];
            });

            $method_status = (DB::table('thai_shipping_methods')->insert($methods->toArray())) ? 'pass' : 'fail';

            return ['error' => false, 'message' => 'Insert thai shipping methods: ' . $method_status];
        } catch (\Throwable $th) {
            return ['error' => true, 'message' => $th->getMessage()];
        }
    }

    public static function insertShippingMethod()
    {
        try {
            $shipping_methods = collect(self::getJsonByFile('database/sql/shipping_methods.json'));

            if ($shipping_methods->count() === 0) {
                return ['error' => true, 'message' => 'shipping method empty'];
            }

            $shipping_methods = $shipping_methods->map(function ($item) {
                return [
                    'id'                      => $item['id'],
                    'title_th'                => $item['title_th'],
                    'title_en'                => $item['title_en'],
                    'title_cn'                => $item['title_cn'],
                    'remark'                  => $item['remark'],
                    'status'                  => $item['status'],
                    'created_admin_id'        => $item['created_admin_id'],
                    'updated_admin_id'        => $item['updated_admin_id'],
                    'created_admin'           => $item['created_admin'],
                    'updated_admin'           => $item['updated_admin'],
                    'created_at'              => $item['created_at'],
                    'updated_at'              => $item['updated_at'],
                ];
            });

            $method_status = (DB::table('shipping_methods')->insert($shipping_methods->toArray())) ? 'pass' : 'fail';

            return ['error' => false, 'message' => 'Insert shipping methods: ' . $method_status];
        } catch (\Throwable $th) {
            return ['error' => true, 'message' => $th->getMessage()];
        }
    }

    public static function insertProductType()
    {
        try {
            $product_type = collect(self::getJsonByFile('database/sql/product_types.json'));

            if ($product_type->count() === 0) {
                return ['error' => true, 'message' => 'product type empty'];
            }

            $product_type = $product_type->map(function ($item) {
                return [
                    'id'                      => $item['id'],
                    'title_th'                => $item['title_th'],
                    'title_en'                => $item['title_en'],
                    'title_cn'                => $item['title_cn'],
                    'remark'                  => $item['remark'],
                    'status'                  => $item['status'],
                    'created_admin_id'        => $item['created_admin_id'],
                    'updated_admin_id'        => $item['updated_admin_id'],
                    'created_admin'           => $item['created_admin'],
                    'updated_admin'           => $item['updated_admin'],
                    'created_at'              => $item['created_at'],
                    'updated_at'              => $item['updated_at'],
                ];
            });

            $method_status = (DB::table('product_types')->insert($product_type->toArray())) ? 'pass' : 'fail';

            return ['error' => false, 'message' => 'Insert product types: ' . $method_status];
        } catch (\Throwable $th) {
            return ['error' => true, 'message' => $th->getMessage()];
        }
    }
    public static function insertWarehouse()
    {
        try {
            $product_type = collect(self::getJsonByFile('database/sql/warehouses.json'));

            if ($product_type->count() === 0) {
                return ['error' => true, 'message' => 'warehouses empty'];
            }

            $product_type = $product_type->map(function ($item) {
                return [
                    'id'                      => $item['id'],
                    'title'                   => $item['title'],
                    'title_customer'          => $item['title_customer'],
                    'status'                  => $item['status'],
                    'created_admin_id'        => $item['created_admin_id'],
                    'updated_admin_id'        => $item['updated_admin_id'],
                    'created_at'              => $item['created_at'],
                    'updated_at'              => $item['updated_at'],
                ];
            });

            $method_status = (DB::table('warehouses')->insert($product_type->toArray())) ? 'pass' : 'fail';

            return ['error' => false, 'message' => 'Insert warehouses: ' . $method_status];
        } catch (\Throwable $th) {
            return ['error' => true, 'message' => $th->getMessage()];
        }
    }

    public static function insertBanks()
    {
        try {
            $banks = collect(self::getJsonByFile('database/sql/banks.json'));

            if ($banks->count() === 0) {
                return ['error' => true, 'message' => 'banks empty'];
            }

            $banks = $banks->map(function ($item) {
                return [
                    'id'                      => $item['id'],
                    'created_at'              => $item['created_at'],
                    'updated_at'              => $item['updated_at'],
                    'title_th'                => $item['title_th'],
                    'title_en'                => $item['title_en'],
                    'title_cn'                => $item['title_cn'],
                    'code'                    => $item['code'],
                    'status'                  => $item['status'],
                    'created_admin_id'        => $item['created_admin_id'],
                    'updated_admin_id'        => $item['updated_admin_id'],
                ];
            });

            $method_status = (DB::table('banks')->insert($banks->toArray())) ? 'pass' : 'fail';

            return ['error' => false, 'message' => 'Insert bank: ' . $method_status];
        } catch (\Throwable $th) {
            return ['error' => true, 'message' => $th->getMessage()];
        }
    }

    public static function insertBankMap()
    {
        try {
            $bank_map = collect(self::getJsonByFile('database/sql/bank_map.json'));

            if ($bank_map->count() === 0) {
                return ['error' => true, 'message' => 'banks empty'];
            }

            $bank_map = $bank_map->map(function ($item) {
                return [
                    'id'                   => $item['id'],
                    'bank_id'              => $item['bank_id'],
                    'external_bank_id'     => $item['external_bank_id'],
                    'remark'               => $item['remark'],
                ];
            });

            $method_status = (DB::table('bank_map')->insert($bank_map->toArray())) ? 'pass' : 'fail';

            return ['error' => false, 'message' => 'Insert bank: ' . $method_status];
        } catch (\Throwable $th) {
            return ['error' => true, 'message' => $th->getMessage()];
        }
    }

    public static function insertThaiShippingAreaPrice()
    {
        try {
            $thaiShippingAreaPrice = collect(self::getJsonByFile('database/sql/thai_shipping_area_prices.json'));

            if ($thaiShippingAreaPrice->count() === 0) {
                return ['error' => true, 'message' => 'thaiShippingAreaPrice empty'];
            }

            $thaiShippingAreaPrice = $thaiShippingAreaPrice->map(function ($item) {
                return [
                    'id'                            => $item['id'],
                    'created_at'                    => $item['created_at'],
                    'updated_at'                    => $item['updated_at'],
                    'price_a'                       => $item['price_a'],
                    'price_b'                       => $item['price_b'],
                    'price_c'                       => $item['price_c'],
                    'price_d'                       => $item['price_d'],
                    'price_e'                       => $item['price_e'],
                    'province_id'                   => $item['province_id'],
                    'amphur_id'                     => $item['amphur_id'],
                    'created_admin'                 => $item['created_admin'],
                    'updated_admin'                 => $item['updated_admin'],
                    'thai_shipping_method_id'       => $item['thai_shipping_method_id'],
                    'created_admin_id'              => $item['created_admin_id'],
                    'updated_admin_id'              => $item['updated_admin_id'],
                ];
            });

            $method_status = (DB::table('thai_shipping_area_prices')->insert($thaiShippingAreaPrice->toArray())) ? 'pass' : 'fail';

            return ['error' => false, 'message' => 'Insert thai_shipping_area_prices: ' . $method_status];
        } catch (\Throwable $th) {
            return ['error' => true, 'message' => $th->getMessage()];
        }
    }
    //

    public static function insertThaiShippingWeightPrice()
    {
        try {
            $thaiShippingWeightPrice = collect(self::getJsonByFile('database/sql/thai_shipping_weight_prices.json'));

            if ($thaiShippingWeightPrice->count() === 0) {
                return ['error' => true, 'message' => 'thaiShippingWeightPrice empty'];
            }

            $thaiShippingWeightPrice = $thaiShippingWeightPrice->map(function ($item) {
                return [
                    'id'                                => $item['id'],
                    'title'                             => $item['title'],
                    'min_weight'                        => $item['min_weight'],
                    'max_weight'                        => $item['max_weight'],
                    'price'                             => $item['price'],
                    'created_admin'                     => $item['created_admin'],
                    'updated_admin'                     => $item['updated_admin'],
                    'thai_shipping_method_id'           => $item['thai_shipping_method_id'],
                    'created_admin_id'                  => $item['created_admin_id'],
                    'updated_admin_id'                  => $item['updated_admin_id'],
                    'thai_shipping_method_id'           => $item['thai_shipping_method_id'],
                    'created_admin_id'                  => $item['created_admin_id'],
                    'updated_admin_id'                  => $item['updated_admin_id'],
                ];
            });

            $method_status = (DB::table('thai_shipping_weight_prices')->insert($thaiShippingWeightPrice->toArray())) ? 'pass' : 'fail';

            return ['error' => false, 'message' => 'Insert thaiShippingWeightPrice: ' . $method_status];
        } catch (\Throwable $th) {
            return ['error' => true, 'message' => $th->getMessage()];
        }
    }

    public static function insertThaiShippingWeighSizetPrice()
    {
        try {
            $thaiShippingWeighSizetPrice = collect(self::getJsonByFile('database/sql/thai_shipping_weight_size_prices.json'));

            if ($thaiShippingWeighSizetPrice->count() === 0) {
                return ['error' => true, 'message' => 'thaiShippingWeighSizetPrice empty'];
            }

            $thaiShippingWeighSizetPrice = $thaiShippingWeighSizetPrice->map(function ($item) {
                return [
                    'id'                                => $item['id'],
                    'title'                             => $item['title'],
                    'min_weight'                        => $item['min_weight'],
                    'max_weight'                        => $item['max_weight'],
                    'max_size'                          => $item['max_size'],
                    'price'                             => $item['price'],
                    'created_admin'                     => $item['created_admin'],
                    'updated_admin'                     => $item['updated_admin'],
                    'thai_shipping_method_id'           => $item['thai_shipping_method_id'],
                    'created_admin_id'                  => $item['created_admin_id'],
                    'updated_admin_id'                  => $item['updated_admin_id'],
                    'thai_shipping_method_id'           => $item['thai_shipping_method_id'],
                    'created_admin_id'                  => $item['created_admin_id'],
                    'updated_admin_id'                  => $item['updated_admin_id'],
                ];
            });

            $method_status = (DB::table('thai_shipping_weight_size_prices')->insert($thaiShippingWeighSizetPrice->toArray())) ? 'pass' : 'fail';

            return ['error' => false, 'message' => 'Insert thaiShippingWeighSizetPrice: ' . $method_status];
        } catch (\Throwable $th) {
            return ['error' => true, 'message' => $th->getMessage()];
        }
    }

    public static function insertRate()
    {
        try {
            $rates = collect(self::getJsonByFile('database/sql/rates.json'));

            if ($rates->count() === 0) {
                return ['error' => true, 'message' => 'rates empty'];
            }

            $rates = $rates->map(function ($item) {
                return [
                    'id'                    => $item['id'],
                    'created_at'            => $item['created_at'],
                    'updated_at'            => $item['updated_at'],
                    'title'                 => $item['title'],
                    'status'                => $item['status'],
                    'admin_remark'          => $item['admin_remark'],
                    'user_code'             => $item['user_code'],
                    'kg_car_genaral'        => $item['kg_car_genaral'],
                    'kg_car_iso'            => $item['kg_car_iso'],
                    'kg_car_brand'          => $item['kg_car_brand'],
                    'kg_ship_genaral'       => $item['kg_ship_genaral'],
                    'kg_ship_iso'           => $item['kg_ship_iso'],
                    'kg_ship_brand'         => $item['kg_ship_brand'],
                    'kg_plane_genaral'      => $item['kg_plane_genaral'],
                    'kg_plane_iso'          => $item['kg_plane_iso'],
                    'kg_plane_brand'        => $item['kg_plane_brand'],
                    'cubic_car_genaral'     => $item['cubic_car_genaral'],
                    'cubic_car_iso'         => $item['cubic_car_iso'],
                    'cubic_car_brand'       => $item['cubic_car_brand'],
                    'cubic_ship_genaral'    => $item['cubic_ship_genaral'],
                    'cubic_ship_iso'        => $item['cubic_ship_iso'],
                    'cubic_ship_brand'      => $item['cubic_ship_brand'],
                    'cubic_plane_genaral'   => $item['cubic_plane_genaral'],
                    'cubic_plane_iso'       => $item['cubic_plane_iso'],
                    'cubic_plane_brand'     => $item['cubic_plane_brand'],
                    'created_admin'         => $item['created_admin'],
                    'updated_admin'         => $item['updated_admin'],
                    'created_admin_id'      => $item['created_admin_id'],
                    'updated_admin_id'      => $item['updated_admin_id'],
                    'default_rate'          => $item['default_rate'],
                    'type'                  => $item['type'],
                    'admin_id'              => $item['admin_id'],
                ];
            })->toArray();

            $create = DB::table('rates')->insert($rates);
            $message = ($create) ? 'pass' : 'fail';

            return ['error' => false, 'message' => 'Insert rates: ' . $message];
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return ['error' => true, 'message' => $th->getMessage()];
        }
    }

    public static function insertExchangeRate()
    {
        try {
            $exchange_rates = collect(self::getJsonByFile('database/sql/exchange_rates.json'));

            if ($exchange_rates->count() === 0) {
                return ['error' => true, 'message' => 'exchange rates empty'];
            }

            $exchange_rates = $exchange_rates->map(function ($item) {
                return [
                    'id'                => $item['id'],
                    'rate'              => $item['rate'],
                    'raw_rate'          => $item['raw_rate'],
                    'created_admin_id'  => $item['created_admin_id'],
                    'created_at'        => $item['created_at'],
                    'updated_at'        => $item['updated_at'],
                    'created_admin'     => $item['created_admin'],
                ];
            })->toArray();

            $create = DB::table('exchange_rates')->insert($exchange_rates);
            $message = ($create) ? 'pass' : 'fail';

            return ['error' => false, 'message' => 'Insert exchange rates: ' . $message];
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return ['error' => true, 'message' => $th->getMessage()];
        }
    }

    public static function insertUser()
    {
        try {
            $users          = collect(self::getJsonByFile('database/sql/users.json'))->unique('id');
            $total_rows     = $users->count();

            if ($total_rows === 0) {
                throw new \Exception('users rows empty');
            }

            $insert_data = $users->map(function ($item) {
                                return [
                                    'id'                        => $item['id'],
                                    'code'                      => $item['code'],
                                    'name'                      => $item['name'],
                                    'password'                  => $item['password'] ?? Hash::make($item['code'] . '.' . $item['tel1']),
                                    'tel1'                      => $item['tel1'],
                                    'tel2'                      => $item['tel2'],
                                    'email'                     => $item['email'],
                                    'province_id'               => $item['province_id'],
                                    'user_remark'               => $item['user_remark'],
                                    'admin_remark'              => $item['admin_remark'],
                                    'system_remark'             => $item['system_remark'],
                                    'status'                    => $item['status'],
                                    // 'provider'                  => $item['provider'],
                                    // 'provider_id'               => $item['provider_id'],
                                    'remember_token'            => $item['remember_token'],
                                    'created_at'                => $item['created_at'],
                                    'updated_at'                => $item['updated_at'],
                                    'default_rate_id'           => $item['default_rate_id'],
                                    'created_admin_id'          => $item['created_admin_id'],
                                    'updated_admin_id'          => $item['updated_admin_id'],
                                    'created_admin'             => $item['created_admin'],
                                    'updated_admin'             => $item['updated_admin'],
                                    'withholding'               => $item['withholding'],
                                    'tax_id'                    => $item['tax_id'],
                                    'fcm_token'                 => $item['fcm_token'],
                                    'image'                     => $item['image'] ?? 'default.png',
                                    'alert_box'                 => $item['alert_box'],
                                    'alert_message'             => $item['alert_message'],
                                    'create_bill'               => $item['create_bill'],
                                    'sms_notification'          => $item['sms_notification'],
                                    'thai_shipping_method_id'   => $item['thai_shipping_method_id'] > 0 ? $item['thai_shipping_method_id'] : NULL,
                                    'admin_ref_id'              => $item['admin_ref_id'] > 0 ? $item['admin_ref_id'] : NULL,
                                ];
                            })
                            ->chunk(1000);

           $success_rows = $insert_data->reduceWithKeys(function ($count, $item, $key) {
                                $insert_data = $item->toArray();
                                $create = DB::table('users')->insert($insert_data);
                                if (!$create) {
                                    throw new \Exception('Insert users: failed. chunk(1000) loop:' . $key);
                                }
                                return $count += count($insert_data);
                            }, 0);
            
            return ['error' => false, 'message' => 'Insert users success. accept: ' . $success_rows . ' | total: ' . $total_rows];
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return ['error' => true, 'message' => $th->getMessage()];
        }
    }

    public static function insertUserAddress()
    {
        try {
            $address        = collect(self::getJsonByFile('database/sql/user_addresses.json'))->whereNotIn('user_code', ['SD6781', 'SD9729', 'SE2272']);
            $total_rows     = $address->count();

            if ($total_rows === 0) {
                throw new \Exception('user_addresses rows empty');
            }
            $insert_data    = $address->map(function ($item) {
                                return [
                                    'id'                => $item['id'],
                                    'created_at'        => $item['created_at'],
                                    'updated_at'        => $item['updated_at'],
                                    'address'           => $item['address'],
                                    'name'              => $item['name'],
                                    'tel'               => $item['tel'],
                                    'province_id'       => $item['province_id'],
                                    'amphur_id'         => $item['amphur_id'],
                                    'district_code'     => $item['district_code'],
                                    'zipcode'           => $item['zipcode'],
                                    'user_code'         => $item['user_code'],
                                    'user_remark'       => $item['user_remark'],
                                    'admin_remark'      => $item['admin_remark'],
                                    'created_admin'     => $item['created_admin'],
                                    'updated_admin'     => $item['updated_admin'],
                                    'created_admin_id'  => $item['created_admin_id'],
                                    'updated_admin_id'  => $item['updated_admin_id'],
                                    'status'            => $item['status'],
                                    'latitude'          => $item['latitude'],
                                    'longitude'         => $item['longitude'],
                                ];
                            })
                            ->chunk(1000);

            $success_rows = $insert_data->reduceWithKeys(function ($count, $item, $key) {
                $insert_data = $item->toArray();
                $create = DB::table('user_addresses')->insert($insert_data);
                if (!$create) {
                    throw new \Exception('Insert user_addresses: failed. chunk(1000) loop:' . $key);
                }
                return $count += count($insert_data);
            }, 0);
            
            return ['error' => false, 'message' => 'Insert user_addresses success. accept: ' . $success_rows . ' | total: ' . $total_rows];
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return ['error' => true, 'message' => $th->getMessage()];
        }
    }

    public static function insertCoupon()
    {
        try {
            $coupons          = collect(self::getJsonByFile('database/sql/coupons.json'));
            $total_rows     = $coupons->count();

            if ($total_rows === 0) {
                throw new \Exception('coupons rows empty');
            }
            $insert_data = $coupons->map(function ($item) {
                                return [
                                    'id'                        => $item['id'],
                                    'created_at'                => $item['created_at'],
                                    'updated_at'                => $item['updated_at'],
                                    'title'                     => $item['title'],
                                    'code'                      => $item['code'],
                                    'user_code'                 => $item['user_code'],
                                    'shipping_method_id'        => $item['shipping_method_id'],
                                    'limit_user_code'           => $item['limit_user_code'],
                                    'limit_shipping_method'     => $item['limit_shipping_method'],
                                    'type'                      => $item['type'],
                                    'price'                     => $item['price'],
                                    'bill'                      => $item['bill'],
                                    'bill_min_price'            => $item['bill_min_price'],
                                    'start_date'                => $item['start_date'],
                                    'end_date'                  => $item['end_date'],
                                    'limit'                     => $item['limit'],
                                    'counter'                   => $item['counter'],
                                    'remark'                    => $item['remark'],
                                    'status'                    => $item['status'],
                                    'created_admin_id'          => $item['created_admin_id'],
                                    'updated_admin_id'          => $item['updated_admin_id'],
                                    'created_admin'             => $item['created_admin'],
                                    'updated_admin'             => $item['updated_admin'],
                                ];
                            })
                            ->toArray();

            $create = DB::table('coupons')->insert($insert_data);
            
            if (!$create) {
                throw new \Exception('Insert coupons fail | ' . 'total:' . $total_rows);
            }
            
            return ['error' => false, 'message' => 'Insert coupons pass | ' . 'total:' . $total_rows];
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return ['error' => true, 'message' => $th->getMessage()];
        }
    }

    public static function insertBills()
    {
        try {
            $bills          = collect(self::getJsonByFile('database/sql/bills.json'));
            $total_rows     = $bills->count();

            if ($total_rows === 0) {
                return ['error' => true, 'message' => 'bills empty'];
            }

            $insert_data = $bills->map(function ($item) {
                return [
                    'id'                                    => $item['id'],
                    'created_at'                            => $item['created_at'],
                    'updated_at'                            => $item['updated_at'],
                    'rate_id'                               => $item['rate_id'],
                    'rate_title'                            => $item['rate_title'],
                    'shipping_weight_counter'               => $item['shipping_weight_counter'],
                    'shipping_cubic_counter'                => $item['shipping_cubic_counter'],
                    'shipping_weight_price'                 => $item['shipping_weight_price'],
                    'shipping_cubic_price'                  => $item['shipping_cubic_price'],
                    'shipping_min_rate'                     => $item['shipping_min_rate'],
                    'shipping_price'                        => $item['shipping_price'],
                    'shipping_rate'                         => $item['shipping_rate'],
                    'counter'                               => $item['counter'],
                    'thai_shipping_method_id'               => $item['thai_shipping_method_id'],
                    'thai_shipping_method_sub_id'           => $item['thai_shipping_method_sub_id'],
                    'thai_shipping_method_sub_title'        => $item['thai_shipping_method_sub_title'],
                    'thai_shipping_method_charge'           => $item['thai_shipping_method_charge'],
                    'thai_shipping_method_price'            => $item['thai_shipping_method_price'],
                    'thai_shipping_method_note'             => $item['thai_shipping_method_note'],
                    'thai_shipping_raw_price'               => $item['thai_shipping_raw_price'],
                    'thai_shipping_raw_charge'              => $item['thai_shipping_raw_charge'],
                    'thai_shipping_date'                    => $item['thai_shipping_date'],
                    'thai_shipping_time'                    => $item['thai_shipping_time'],
                    'thai_shipping_code'                    => $item['thai_shipping_code'],
                    'thai_shipping_admin_id'                => $item['thai_shipping_admin_id'],
                    'thai_shipping_admin'                   => $item['thai_shipping_admin'],
                    'thai_shipped_at'                       => $item['thai_shipped_at'],
                    'thai_shipping_detail'                  => $item['thai_shipping_detail'],
                    'other_charge_title1'                   => $item['other_charge_title1'],
                    'other_charge_price1'                   => $item['other_charge_price1'],
                    'other_charge_title2'                   => $item['other_charge_title2'],
                    'other_charge_price2'                   => $item['other_charge_price2'],
                    'discount_title1'                       => $item['discount_title1'],
                    'discount_price1'                       => $item['discount_price1'],
                    'discount_title2'                       => $item['discount_title2'],
                    'discount_price2'                       => $item['discount_price2'],
                    'addon_refund_title'                    => $item['addon_refund_title'],
                    'addon_refund_credit'                   => $item['addon_refund_credit'],
                    'coupon_id'                             => $item['coupon_id'],
                    'coupon_code'                           => $item['coupon_code'],
                    'coupon_price'                          => $item['coupon_price'],
                    'total_weight'                          => $item['total_weight'],
                    'total_cubic'                           => $item['total_cubic'],
                    'sub_total'                             => $item['sub_total'],
                    'withholding'                           => $item['withholding'],
                    'withholding_amount'                    => $item['withholding_amount'],
                    'total_price'                           => $item['total_price'],
                    'shipping_name'                         => $item['shipping_name'],
                    'shipping_tel'                          => $item['shipping_tel'],
                    'shipping_address'                      => $item['shipping_address'],
                    'shipping_district_code'                => $item['shipping_district_code'],
                    'shipping_amphur_id'                    => $item['shipping_amphur_id'],
                    'shipping_province_id'                  => $item['shipping_province_id'],
                    'shipping_zipcode'                      => $item['shipping_zipcode'],
                    'status'                                => $item['status'],
                    'user_remark'                           => $item['user_remark'],
                    'system_remark'                         => $item['system_remark'],
                    'admin_remark'                          => $item['admin_remark'],
                    'admin_packing_remark'                  => $item['admin_packing_remark'],
                    'admin_thai_shipping_remark'            => $item['admin_thai_shipping_remark'],
                    'user_code'                             => $item['user_code'],
                    'user_address_id'                       => $item['user_address_id'],
                    'created_admin'                         => $item['created_admin'],
                    'created_admin_id'                      => $item['created_admin_id'],
                    'updated_admin'                         => $item['updated_admin'],
                    'updated_admin_id'                      => $item['updated_admin_id'],
                    'exchange_rate'                         => $item['exchange_rate'],
                    'cost_box_china'                        => $item['cost_box_china'],
                    'cost_shipping_china'                   => $item['cost_shipping_china'],
                    'cost_box_thai'                         => $item['cost_box_thai'],
                    'cost_shipping_thai'                    => $item['cost_shipping_thai'],
                    'printed_thai_shipping'                 => $item['printed_thai_shipping'],
                    'printed_admin_id'                      => $item['printed_admin_id'],
                    'printed_at'                            => $item['printed_at'],
                    'printed_admin'                         => $item['printed_admin'],
                    'droppoint_id'                          => $item['droppoint_id'],
                    'droppoint_at'                          => $item['droppoint_at'],
                    'droppoint_remark'                      => $item['droppoint_remark'],
                    'droppoint_admin'                       => $item['droppoint_admin'],
                    'droppoint_admin_id'                    => $item['droppoint_admin_id'],
                    'droppoint_noti'                        => $item['droppoint_noti'],
                    'sign_image'                            => $item['sign_image'],
                    'truck_id'                              => $item['truck_id'],
                    'driver_admin'                          => $item['driver_admin'],
                    'driver_admin_id'                       => $item['driver_admin_id'],
                    'driver2_admin'                         => $item['driver2_admin'],
                    'driver2_admin_id'                      => $item['driver2_admin_id'],
                    'droppoint_noti'                        => $item['droppoint_noti'],
                    'mark'                                  => $item['mark'],
                    'marked_at'                             => $item['marked_at'],
                    'marked_user_id'                        => $item['marked_user_id'],
                    'marked_user'                           => $item['marked_user'],
                    'commission_rate_id'                    => $item['commission_rate_id'],
                    'latitude'                              => $item['latitude'],
                    'longitude'                             => $item['longitude'],
                    'tcatexpress_address_api'               => $item['tcatexpress_address_api'],
                    'approved_at'                           => $item['approved_at'],
                    'approved_admin'                        => $item['approved_admin'],
                    'approved_admin_id'                     => $item['approved_admin_id'],
                    'commission_admin_id'                   => $item['commission_admin_id'],
                    'succeed_at'                            => $item['succeed_at'],
                    // 'commission_id'                         => $item['commission_id'],
                    // 'commission_amount'                     => $item['commission_amount'],
                    'commission_id'                         => null,
                    'commission_amount'                     => 0,
                    'paid_at'                               => $item['paid_at'],
                    'peak_at'                               => $item['peak_at'],
                    'peak_code'                             => $item['peak_code'],
                    'peak_status'                           => $item['peak_status'],
                    'peak_amount'                           => $item['peak_amount'],
                    'peak_remark'                           => $item['peak_remark']
                ];
            })
            ->toArray();

            $create = DB::table('bills')->insert($insert_data);
            
            if (!$create) {
                throw new \Exception('Insert bills fail | ' . 'total:' . $total_rows);
            }
            
            return ['error' => false, 'message' => 'Insert bills pass | ' . 'total:' . $total_rows];
        } catch (\Throwable $th) {
            return ['error' => true, 'message' => $th->getMessage()];
        }
    }

    public static function insertTrackings()
    {
        try {
            $trackings      = collect(self::getJsonByFile('database/sql/trackings.json'));
            $total_rows     = $trackings->count();

            if ($total_rows === 0) {
                return ['error' => true, 'message' => 'trackings empty'];
            }

            $insert_data    = $trackings->map(function ($item) {
                return  [
                        'id'                    => $item['id'],
                        'user_code'             => $item['user_code'],
                        'code'                  => $item['code'],
                        'reference_code'        => $item['reference_code'],
                        'weight'                => $item['weight'],
                        'width'                 => $item['width'],
                        'height'                => $item['height'],
                        'length'                => $item['length'],
                        'cubic'                 => $item['cubic'],
                        'dimension'             => $item['dimension'],
                        'calculate_type'        => $item['calculate_type'],
                        'calculate_rate'        => $item['calculate_rate'],
                        'price'                 => $item['price'],
                        'cubic_rate'            => $item['cubic_rate'],
                        'weight_rate'           => $item['weight_rate'],
                        'cubic_price'           => $item['cubic_price'],
                        'weight_price'          => $item['weight_price'],
                        'china_in'              => $item['china_in'],
                        'china_out'             => $item['china_out'],
                        'thai_in'               => $item['thai_in'],
                        'thai_out'              => $item['thai_out'],
                        'user_remark'           => $item['user_remark'],
                        'admin_remark'          => $item['admin_remark'],
                        'system_remark'         => $item['system_remark'],
                        'bill_id'               => $item['bill_id'],
                        'shipping_method_id'    => $item['shipping_method_id'],
                        'product_type_id'       => $item['product_type_id'],
                        'warehouse_id'          => $item['warehouse_id'],
                        'warehouse_zone'        => $item['warehouse_zone'],
                        'status'                => $item['status'],
                        'china_cost_box'        => $item['china_cost_box'],
                        'china_cost_shipping'   => $item['china_cost_shipping'],
                        'thai_cost_box'         => $item['thai_cost_box'],
                        'thai_cost_shipping'    => $item['thai_cost_shipping'],
                        'exchange_rate'         => $item['exchange_rate'],
                        'created_admin_id'      => $item['created_admin_id'],
                        'updated_admin_id'      => $item['updated_admin_id'],
                        'created_at'            => $item['created_at'],
                        'updated_at'            => $item['updated_at'],
                        'created_admin'         => $item['created_admin'],
                        'updated_admin'         => $item['updated_admin'],
                        'packed_admin'          => $item['packed_admin'],
                        'packed_admin_id'       => $item['packed_admin_id'],
                        'packed_at'             => $item['packed_at'],
                        'duplicate_order'       => $item['duplicate_order'],
                        'duplicate_check'       => $item['duplicate_check'],
                        'sort'                  => $item['sort'],
                        'deleted_at'            => $item['deleted_at'],
                        'insert_uid'            => $item['insert_uid'],
                        'type'                  => $item['type'],
                        'detail'                => $item['detail'],
                    ];
            })
            ->chunk(1000);

            $success_rows   = $insert_data->reduceWithKeys(function ($count, $item, $key) {
            $insert_data    = $item->toArray();
            $create         = DB::table('trackings')->insert($insert_data);
            if (!$create) {
                throw new \Exception('Insert trackings: failed. chunk(1000) loop:' . $key);
            }
            return $count += count($insert_data);
            }, 0);
            
            return ['error' => false, 'message' => 'Insert trackings success. accept: ' . $success_rows . ' | total: ' . $total_rows];
        } catch (\Throwable $th) {
            return ['error' => true, 'message' => $th->getMessage()];
        }
    }

    public static function insertPivotTrackings()
    {
        try {
            $pivot_trackings    = collect(self::getJsonByFile('database/sql/bill_trackings.json'));
            $total_rows         = $pivot_trackings->count();

            if ($total_rows === 0) {
                return ['error' => true, 'message' => 'pivot trackings empty'];
            }

            $insert_data = $pivot_trackings->map(function ($item) {
                return [
                        'id'            => $item['id'],
                        'created_at'    => $item['created_at'],
                        'updated_at'    => $item['updated_at'],
                        'bill_id'       => $item['bill_id'],
                        'tracking_id'   => $item['tracking_id']
                    ];
            })
            ->toArray();

            $create = DB::table('bill_trackings')->insert($insert_data);
            
            if (!$create) {
                throw new \Exception('Insert pivot trackings fail | ' . 'total:' . $total_rows);
            }
            
            return ['error' => false, 'message' => 'Insert pivot trackings pass | ' . 'total:' . $total_rows];
        } catch (\Throwable $th) {
            return ['error' => true, 'message' => $th->getMessage()];
        }
    }

    public static function insertTrackingDetail()
    {
        try {
            $track_detail       = collect(self::getJsonByFile('database/sql/tracking_details.json'));
            $total_rows         = $track_detail->count();

            if ($total_rows === 0) {
                return ['error' => true, 'message' => 'tracking details empty'];
            }

            $insert_data = $track_detail->map(function ($item) {
                return [
                        'id'                => $item['id'],
                        'product_type'      => $item['product_type'],
                        'product_remark'    => $item['product_remark'],
                        'qty'               => $item['qty'],
                        'tracking_id'       => $item['tracking_id'],
                        'created_admin'     => $item['created_admin'],
                        'updated_admin'     => $item['updated_admin'],
                        'created_admin_id'  => $item['created_admin_id'],
                        'updated_admin_id'  => $item['updated_admin_id'],
                        'created_at'        => $item['created_at'],
                        'updated_at'        => $item['updated_at'],
                    ];
            })
            ->toArray();

            $create = DB::table('tracking_details')->insert($insert_data);
            
            if (!$create) {
                throw new \Exception('Insert tracking_details fail | ' . 'total:' . $total_rows);
            }
            
            return ['error' => false, 'message' => 'Insert tracking_details pass | ' . 'total:' . $total_rows];
        } catch (\Throwable $th) {
            return ['error' => true, 'message' => $th->getMessage()];
        }
    }

    public static function insertCommissionRate()
    {
        try {
            $track_detail       = collect(self::getJsonByFile('database/sql/commission_rates.json'));
            $total_rows         = $track_detail->count();

            if ($total_rows === 0) {
                return ['error' => true, 'message' => 'commission rates empty'];
            }

            $insert_data = $track_detail->map(function ($item) {
                return [
                        'id'                    => $item['id'],
                        'created_at'            => $item['created_at'],
                        'updated_at'            => $item['updated_at'],
                        'kg_car_genaral'        => $item['kg_car_genaral'],
                        'kg_car_iso'            => $item['kg_car_iso'],
                        'kg_car_brand'          => $item['kg_car_brand'],
                        'kg_ship_genaral'       => $item['kg_ship_genaral'],
                        'kg_ship_iso'           => $item['kg_ship_iso'],
                        'kg_ship_brand'         => $item['kg_ship_brand'],
                        'cubic_car_genaral'     => $item['cubic_car_genaral'],
                        'cubic_car_iso'         => $item['cubic_car_iso'],
                        'cubic_car_brand'       => $item['cubic_car_brand'],
                        'cubic_ship_genaral'    => $item['cubic_ship_genaral'],
                        'cubic_ship_iso'        => $item['cubic_ship_iso'],
                        'cubic_ship_brand'      => $item['cubic_ship_brand'],
                        'admin_remark'          => $item['admin_remark'],
                        'created_admin'         => $item['created_admin'],
                        'updated_admin'         => $item['updated_admin'],
                        'created_admin_id'      => $item['created_admin_id'],
                        'updated_admin_id'      => $item['updated_admin_id'],
                        'bill_id'               => $item['bill_id'],
                        'admin_id'              => $item['admin_id']
                    ];
            })
            ->toArray();

            $create = DB::table('commission_rates')->insert($insert_data);
            
            if (!$create) {
                throw new \Exception('Insert commission rates fail | ' . 'total:' . $total_rows);
            }
            
            return ['error' => false, 'message' => 'Insert commission rates pass | ' . 'total:' . $total_rows];
        } catch (\Throwable $th) {
            return ['error' => true, 'message' => $th->getMessage()];
        }
    }
}

