<?php

namespace App\Http\Requests\BankAccount;

use App\Http\Requests\FormRequest;

class UpdateBankAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.bank-account.update');
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'         => 'required',
            'account'       => 'required',
            'branch'        => 'required',
            'status'        => 'required|in:active,inactive',
            'bank'          => 'required|max:10',
            'user_code'     => 'required|exists:users,code',
            'user_remark'   => 'sometimes|nullable|string',
            'admin_remark'  => 'sometimes|nullable|string',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'title'         => __('form.bank_account.title'),
            'account'       => __('form.bank_account.account'),
            'bank'          => __('form.bank_account.bank'),
            'branch'        => __('form.bank_account.branch'),
            'status'        => __('form.bank_account.status'),
            'user_code'     => __('form.bank_account.user_code'),
            'user_remark'   => __('form.bank_account.user_remark'),
            'admin_remark'  => __('form.bank_account.admin_remark'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
