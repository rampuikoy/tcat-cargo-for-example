<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Bill;
use App\Interfaces\BillInterface;
use App\Interfaces\TrackingInterface;
use App\Interfaces\ThaiShippingMethodInterface;
use Barryvdh\DomPDF\Facade as PDF;

class BillDownloadController extends Controller
{
    protected $billInterface, $trackingInterface, $thaiShippingMethodInterface;

    public function __construct(BillInterface $billInterface, TrackingInterface $trackingInterface, ThaiShippingMethodInterface $thaiShippingMethodInterface)
    {
        $this->billInterface = $billInterface;
        $this->trackingInterface = $trackingInterface;
        $this->thaiShippingMethodInterface = $thaiShippingMethodInterface;
    }

    public function getDownloadCover(Request $request, $id)
    {
        $this->authorize('show', Bill::class);
        $bill   = $this->billInterface->findById($id);
        $amount = $request->amount ? $request->amount : 1;
        $pdf = PDF::loadView('admin.bill.download.bill-cover', ['bill' => $bill->results, 'amount' => $amount]);
        return $pdf->stream();
    }

    public function getDownloadSticker(Request $request, $id)
    {
        $this->authorize('show', Bill::class);
        $bill   = $this->billInterface->findById($id);
        $amount = $request->amount ? $request->amount : 1;
        $pdf = PDF::loadView('admin.bill.download.bill-sticker', ['bill' => $bill->results, 'amount' => $amount]);
        return $pdf->setPaper(array(0, 0, 297.64, 500.53), 'landscape')->stream();
    }

    public function getDownloadTracking($id)
    {
        $this->authorize('show', Bill::class);
        $bill = $this->billInterface->findById($id);
        $trackings = [];
        foreach ($bill->results->pivotTrackings as $tracking) {
            $trackingID = $this->trackingInterface->findById($tracking->id);
            $date_thai_in = date_create($trackingID->results->thai_in);
            $trackingID->results->thai_in = date_format($date_thai_in, 'Y-m-d');
            $trackings[] = $trackingID->results;
        }
        $pdf = PDF::loadView('admin.bill.download.tracking-pdf', ['bill' => $bill->results, 'trackings' =>  $trackings]);
        return $pdf->stream();
    }

    public function getDownloadBill($id)
    {
        $dataBill = $this->getDataBillAll($id);
        $pdf = PDF::loadView('admin.bill.download.bill-pdf', $dataBill);
        $filename = 'bill-' . $dataBill['bill']['no'] . '.pdf';
        return $pdf->download($filename);
    }

    public function getDataBillAll($id)
    {
        $bill = $this->billInterface->findById($id);
        $data = ['bill' => $bill->results,];
        return $data;
    }

    public function getDownloadDroppoint(Request $request)
    {
        $data = $request->only([
            "thai_shipping_method_id",
            "bill",
        ]);
        $setData = $this->getDataBillShipping($data);
        $pdf = PDF::loadView('admin.bill.download.bill-shipping-droppoint', $setData);
        return $pdf->stream();
    }

    public function getDownloadOther(Request $request)
    {
        $data = $request->only([
            "thai_shipping_method_id",
            "bill",
        ]);
        $setData = $this->getDataBillShipping($data);
        $pdf = PDF::loadView('admin.bill.download.bill-shipping-other', $setData);
        return $pdf->stream();
    }

    public function getDownloadDriver(Request $request)
    {
        $data = $request->only([
            "thai_shipping_method_id",
            "bill",
        ]);
        $setData = $this->getDataBillShipping($data);
        $pdf = PDF::setPaper('a4', 'landscape')->loadView('admin.bill.download.bill-shipping-driver', $setData);
        return $pdf->stream();
    }

    public function getDownloadBills(Request $request)
    {
        $data = $request->only(["bill"]);
        $setData = $this->getDataBillShipping($data);
        $pdf = PDF::loadView('admin.bill.download.bill-multi-pdf', $setData);
        return $pdf->stream();
    }

    public function getDownloadTrackings(Request $request)
    {
        $data = $request->only(["bill"]);
        $setData = $this->getDataBillShipping($data);
        $pdf = PDF::loadView('admin.bill.download.bill-multi-tracking', $setData);
        return $pdf->stream();
    }

    public function getDownloadCovers(Request $request)
    {
        $data = $request->only(["bill"]);
        $setData = $this->getDataBillShipping($data);
        $pdf = PDF::loadView('admin.bill.download.bill-multi-cover', $setData);
        return $pdf->stream();
    }

    public function getDownloadStickers(Request $request)
    {
        $data = $request->only(["bill"]);
        $setData = $this->getDataBillShipping($data);
        $pdf = PDF::loadView('admin.bill.download.bill-multi-sticker', $setData);
        return $pdf->setPaper(array(0, 0, 297.64, 500.53), 'landscape')->stream();
    }

    public function getDataBillShipping($data)
    {
        $droppoint = !empty($data['thai_shipping_method_id']) ? $this->thaiShippingMethodInterface->findById($data['thai_shipping_method_id']) : null;
        $ids = explode('-', $data['bill']);
        $bills = $this->billInterface->fetchListByIds($ids);
        $sendDate = !empty($data['thai_shipping_method_id']) ?  ['bills' => $bills->results, 'droppoint' => $droppoint->results] : ['bills' => $bills->results];
        return $sendDate;
    }
}
