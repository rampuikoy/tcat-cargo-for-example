<?php

namespace App\Http\Requests\Chat;

use App\Rules\Base64Image;
use App\Http\Requests\FormRequest;

class UpdateBroadcastRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.chat.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message'          => 'sometimes|nullable|string',
            'file'             => ['sometimes', 'nullable', new Base64Image],
            'status'           => 'required|in:active,inactive',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'message'       => __('form.chat.message'),
            'file'          => __('form.chat.file'),
            'status'        => __('form.chat.status'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
