<?php

namespace App\Http\Requests\ThaiShippingMethod;

use App\Http\Requests\FormRequest;
use App\Rules\Base64Image;
class UpdateThaiShippingMethodRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.thai-shipping-method.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'                => 'required',
            'image'                => ['sometimes', 'nullable', new Base64Image],
            'address'              => 'required_if:type,droppoint',
            'province_id'          => 'required_if:type,droppoint',
            'amphur_id'            => 'required_if:type,droppoint',
            'district_code'        => 'required_if:type,droppoint',
            'zipcode'              => 'required_if:type,droppoint',
            'latitude'             => 'required_if:type,delivery',
            'longitude'            => 'required_if:type,delivery',
            'max_weight'           => 'required|numeric',
            'max_distance'         => 'required_if:type,delivery|numeric',
            'charge'               => 'required|numeric',
            'shipping_rate_weight' => 'required_if:calculate_type,fixed|numeric',
            'shipping_rate_cubic'  => 'required_if:calculate_type,fixed|numeric',
            'api'                  => 'required_if:calculate_type,api',
            'calculate_type'       => 'required',
            'type'                 => 'required',
            'status'               => 'required|in:active,inactive',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'title'                     => __('form.thai_shipping_method.title'),
            'image'                     => __('form.thai_shipping_method.image'),
            'address'                   => __('form.thai_shipping_method.address'),
            'province_id'               => __('form.thai_shipping_method.province_id'),
            'address_id'                => __('form.thai_shipping_method.address_id'),
            'amphur_id'                 => __('form.thai_shipping_method.amphur_id'),
            'district_code'             => __('form.thai_shipping_method.district_code'),
            'zipcode'                   => __('form.thai_shipping_method.zipcode'),
            'latitude'                  => __('form.thai_shipping_method.latitude'),
            'longitude'                 => __('form.thai_shipping_method.longitude'),
            'max_weight'                => __('form.thai_shipping_method.max_weight'),
            'max_distance'              => __('form.thai_shipping_method.max_distance'),
            'charge'                    => __('form.thai_shipping_method.charge'),
            'shipping_rate_weight'      => __('form.thai_shipping_method.shipping_rate_weight'),
            'shipping_rate_cubic'       => __('form.thai_shipping_method.shipping_rate_cubic'),
            'api'                       => __('form.thai_shipping_method.api'),
            'calculate_type'            => __('form.thai_shipping_method.calculate_type'),
            'type'                      => __('form.thai_shipping_method.type'),
            'status'                    => __('form.thai_shipping_method.status'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
