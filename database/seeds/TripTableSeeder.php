<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Trip;

class TripTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::whereIn('code', ['SF0002', 'SF0003', 'SF0004', 'SF0005'])->limit(5)->get();
        $users->each(function ($user) {
            Trip::factory()->create([
                'user_code'                 => $user->code,
            ]);
        });
    }
}
