<?php

namespace Database\Factories;

use App\Models\Point;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

class PointFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Point::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $sufType = [
            'topup', 'withdraw'
        ];
        $sufcUser = [
            'SF0002',
            'SF0003',
            'SF0004',
            'SF0005',
            'SF0006',
        ];
        return [
            'pointable_type' => 'App',
            'pointable_id' => random_int(1, 1000),
            'user_code' => Arr::random($sufcUser),
            'amount' => random_int(1, 1000),
            'before' => random_int(1, 1000),
            'after' => random_int(1, 1000),
            'type' => Arr::random($sufType),
        ];
    }
}
