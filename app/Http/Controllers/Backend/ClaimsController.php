<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Claims;
use Illuminate\Http\Request;
use App\Interfaces\ClaimsInterface;
use App\Interfaces\UploadInterface;
use App\Http\Requests\Claim\ClaimsRequest;
use App\Http\Requests\Claim\UploadRequest;
use App\Http\Requests\Claim\UpdateRequest;
use App\Http\Requests\Claim\DestroyRequest;
use Barryvdh\DomPDF\Facade as PDF;


class ClaimsController extends Controller
{
    protected $claimsInterface, $uploadInterface;

    public function __construct(ClaimsInterface $claimsInterface, UploadInterface $uploadInterface)
    {
        $this->claimsInterface = $claimsInterface;
        $this->uploadInterface = $uploadInterface;
    }

    public function index(Request $request)
    {
        $this->authorize('index', Claims::class);
        $columns = $request->input('columns', ['*']);
        $claim = $this->claimsInterface->all($columns);
        return response()->json($claim, $claim->code);
    }

    public function search(Request $request)
    {
        $this->authorize('index', Claims::class);
        $conditions = $request->only([
            'bill_no',
            'user_code',
            'contacted_admin',
            'city',
            'remark',
            'status',
            'shipping',
            'type',
            'china_in'
        ]);
        $columns = $request->input('columns', ['*']);
        $limit = $request->input('limit', $this->claimsInterface::DEFAULT_LIMIT);
        $sorting = $request->input('order_by', $this->claimsInterface::DEFAULT_SORTING);
        $ascending = $request->input('ascending') == 1 ? 'asc' : 'desc';

        if ($request->boolean('pagination')) {
            $page = $request->input('page', 1);
            $claim = $this->claimsInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset = $request->input('page', 0);
            $claim = $this->claimsInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($claim, $claim->code);
    }

    public function show(Request $request, $id)
    {
        $this->authorize('show', Claims::class);
        $columns = $request->input('columns', ['*']);
        $claim = $this->claimsInterface->findById($id, $columns);
        return response()->json($claim, $claim->code);
    }

    public function dropdown()
    {
        $dropdown = $this->claimsInterface->dropdownList();
        return response()->json($dropdown, $dropdown->code);
    }

    public function store(ClaimsRequest $request)
    {
        $data = $request->only([
            'user_code',
            'shipping',
            'contacted_admin',
            'city',
            'china_in',
            'detail',
            'sub_total',
            'total',
            'items'
        ]);
        $claim = $this->claimsInterface->store($data);
        return response()->json($claim, $claim->code);
    }

    public function approveById(UpdateRequest $request, $id)
    {
        $data = $request->only(['type', 'remark']);
        $claim = $this->claimsInterface->approveById($id, $data);
        return response()->json($claim, $claim->code);
    }

    public function cancelById(DestroyRequest $request, $id)
    {
        $data = $request->only(['type', 'remark']);
        $claim = $this->claimsInterface->cancelById($id, $data);
        return response()->json($claim, $claim->code);
    }

    public function upload(UploadRequest $request, $id)
    {
        $data = [
            'type'              => 'image',
            'relatable_type'    => 'App\Models\Claims',
            'relatable_id'      => $id,
            'file_path'         => $request->file_path,
        ];
        $upload = $this->uploadInterface->store($data);
        return response()->json($upload, $upload->code);
    }

    public function getDownload($id)
    {
        $this->authorize('show', Bill::class);
        $claim = $this->claimsInterface->findById($id);
        $pdf = PDF::loadView('admin.claim.pdf', ['bill' => $claim->results]);
        return $pdf->stream();
    }
}
