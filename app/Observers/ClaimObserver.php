<?php

namespace App\Observers;

use App\Models\Claims;
use App\Observers\BaseObserver;

class ClaimObserver extends BaseObserver
{
    /**
     * Handle the Claims "created" event.
     *
     * @param  \App\Models\Claims  $claims
     * @return void
     */
    public function creating(Claims $claims)
    {
        $this->stampCreated($claims);
    }

    /**
     * Handle the Claims "updated" event.
     *
     * @param  \App\Models\Claims  $claims
     * @return void
     */
    public function updating(Claims $claims)
    {
        $this->stampUpdated($claims);
    }
}
