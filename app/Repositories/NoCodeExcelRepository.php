<?php

namespace App\Repositories;

use App\Helpers\Util;
use App\Imports\NoCodeImport;
use App\Interfaces\NoCodeExcelInterface;
use App\Models\NoCode;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use Maatwebsite\Excel\Facades\Excel;

class NoCodeExcelRepository implements NoCodeExcelInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const MINUTE_CACHE  = 0;

    public function __construct(NoCode $nocode)
    {
        $this->nocode   = $nocode;
    }

    public function validateExcelFile($file)
    {
        $start_time     = microtime(true);
        $import         = new NoCodeImport();

        try {
            Excel::import($import, $file);
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $errors     = Util::customExcelFailures($e->failures());
            return $this->unprocessResponse($errors);
        }

        $nocode         = $import->getNoCodes();

        return $this->coreResponse(200, __('messages.import_excel'), [
            'nocode'    => $nocode->toArray(),
            'log'       => [
                'count' => $nocode->count(),
                'execute_time' => round(microtime(true) - $start_time, 2),
            ],
        ]);
    }

    public function createOrUpdate(array $data)
    {
        $nocode    = collect($data);
        //  check empty
        if ($nocode->count() === 0) {
            return $this->coreResponse(500, __('messages.not_create_data', ['data' => __('messages.no_code')]));
        }

        $nocode    = $nocode->map(function ($item) {
                            return $this->nocode::updateOrCreate(
                                [
                                    'code'              => $item['code'],
                                ],
                                [
                                    'reference_code'    => $item['reference_code'],
                                    'order_code'        => $item['order_code'],
                                    'date_in'           => $item['date_in'] ? date('Y-m-d', strtotime($item['date_in'])) : date('Y-m-d'),
                                    'zone'              => $item['zone'],
                                    'remark'            => $item['remark'],
                                ]
                            );
                        });

        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.no_code')]), $nocode);
    }
}
