<?php

namespace App\Observers;

use App\Models\PointWithdraw;

class PointWithdrawObserver extends BaseObserver
{
    /**
     * Handle the point_withdraw "creating" event.
     *
     * @param  \App\Models\PointWithdraw  $point_withdraw
     * @return void
     */
    public function creating(PointWithdraw $point_withdraw)
    {
        $this->stampCreated($point_withdraw);
    }

    /**
     * Handle the point_withdraw "updating" event.
     *
     * @param  \App\Models\PointWithdraw  $point_withdraw
     * @return void
     */
    public function updating(PointWithdraw $point_withdraw)
    {
        $this->stampUpdated($point_withdraw);
    }
}
