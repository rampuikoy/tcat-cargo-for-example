<!DOCTYPE html>
<html lang="th">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>สติ๊กเกอร์ปะหน้ากล่อง</title>
	{!! Html::style('public/css/pdf.css') !!}
	<link rel="stylesheet" href="https://res.imemo.co/tcatcargo_alpha/misc/css/pdf.css" media="all" />
	<style>
		table.cover td {
			border: 1px solid #000;
		}
	</style>
</head>

<body>
	<div style="border: 1px solid #000;font-size: 25px;line-height: 0.75em;padding: 5px;">
		<table class="address" aria-describedby="main-address">
			<tr>
				<th scope="col"></th>
			</tr>
			<tbody>
				<tr>
					<td width="20%">
						<img src="https://res.imemo.co/tcatcargo_alpha/misc/logo/tcatcargo-small.png" alt="logo-tcatcargo" style="width: 100px;">
					</td>
					<td>
						<div><strong>บริษัท&nbsp;TCAT&nbsp;Cargo</strong><br>
							5/77&nbsp;ม.8&nbsp;ต.บางกระสอ&nbsp;อ.เมือง&nbsp;จ.นนทบุรี&nbsp;11000<br>โทร&nbsp;080-003-1122,&nbsp;080-003-2233
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div style="border: 1px solid #000;padding: 5px;font-size: 38px;line-height: 0.80em;height: 225px;">
		<strong>ผู้รับ:</strong> {{$address->name}} ({{$address->user_code}}) / {{$address->tel}} <br>
		<strong>ที่อยู่:</strong> {{$address->address}}
		ต.{{$address->district->title_th}}
		อ.{{$address->amphur->title_th}}
		จ.{{$address->province->title_th}}
		{{$address->zipcode}}
	</div>
</body>

</html>