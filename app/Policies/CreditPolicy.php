<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class CreditPolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.credit.index');
    }

    public function show(Admin $admin)
    {
        return $admin->hasAnyPermission(['backend.credit.show', 'backend.credit.update']);
    }

    public function store(Admin $admin)
    {
        return $admin->can('backend.credit.store');
    }

    public function update(Admin $admin)
    {
        return $admin->can('backend.credit.update');
    }

    public function destroy(Admin $admin)
    {
        return $admin->can('backend.credit.destroy');
    }
}
