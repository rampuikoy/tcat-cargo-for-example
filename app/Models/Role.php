<?php

namespace App\Models;

class Role extends \Spatie\Permission\Models\Role
{
    public function scopeSuperAdmin($query)
    {
        return $query->where('name', 'SuperAdmin');
    }

    public function pivotPermissions()
    {
        return $this->belongsToMany('App\Models\Permission', 'role_has_permissions', 'role_id', 'permission_id');
    }

    public function scopeSearch($query, $conditions)
    {
        return  $query->where(function ($query) use ($conditions) {
            if (@$conditions['id']) {
                $query->where('id', $conditions['id']);
            }
            if (@$conditions['search']) {
                $query->where('name', 'LIKE', "%{$conditions['search']}%");
            }
        });
    }
}
