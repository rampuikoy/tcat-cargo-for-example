<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class PointPolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.point.index');
    }
}
