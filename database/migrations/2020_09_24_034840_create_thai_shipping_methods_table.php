<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateThaiShippingMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('thai_shipping_methods', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            $table->enum('type', ['droppoint', 'delivery', 'other'])->default('droppoint');
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->enum('access', ['all', 'staff_only'])->default('all');

            $table->string('title_th');
            $table->string('title_en')->nullable();
            $table->string('title_cn')->nullable();
            $table->text('address');
            $table->text('detail')->nullable();
            $table->string('tel', 20);

            $table->integer('province_id')->unsigned()->index();
            $table->foreign('province_id')->references('id')->on('provinces');

            $table->integer('amphur_id')->unsigned()->index();
            $table->foreign('amphur_id')->references('id')->on('amphurs');

            $table->string('district_code', 6)->nullable();
            $table->string('zipcode', 6)->nullable();

            $table->string('image')->nullable()->default('default.png');
            $table->decimal('latitude', 11, 8)->nullable();
            $table->decimal('longitude', 11, 8)->nullable();

            $table->enum('calculate_type', ['fixed', 'area', 'weight', 'weight_size', 'api'])->default('fixed');
            $table->tinyInteger('refund')->nullable();
            $table->integer('shipping_rate_weight')->default(0);
            $table->integer('shipping_rate_cubic')->default(0);
            $table->double('charge', 8, 2)->default(0.00);
            $table->double('max_weight', 8, 2)->default(0.00);

            $table->enum('delivery', ['active', 'inactive'])->default('inactive');
            $table->integer('min_price')->default(0);
            $table->float('max_distance', 8, 2)->nullable();
            $table->float('delivery_rate_km', 8, 2)->default(0.00);
            $table->float('delivery_rate_kg', 8, 2)->default(0.00);
            $table->float('delivery_rate_cubic', 8, 2)->default(0.00);

            $table->text('remark')->nullable();
            $table->string('api')->nullable();

            $table->integer('created_admin_id')->unsigned()->nullable();
            $table->integer('updated_admin_id')->unsigned()->nullable();
            $table->string('created_admin', 64)->nullable();
            $table->string('updated_admin', 64)->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('thai_shipping_methods');
    }
}
