<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    //
    public $timestamps = false;
    public static $imageFolder = 'uploads/resource/';
    public static $imageWidth = 760;
    public static $imageHeight = null;

    protected $fillable = [
        'type',
        'file_path',
        'created_admin_id',
        'created_admin',
        'relatable_type',
        'relatable_id',
        'sub_path'
    ];

    protected $appends = [
        'file_url',
    ];

    public function getFileUrlAttribute($handle = null)
    {
        $file = $handle ?? $this->file_path;
        $result = null;
        switch ($this->relatable_type) {
            case 'App\Models\Bill':
                $result = env('APP_MEDIA_URL') . '/' . Bill::$imageFolder . $this->sub_path . $file;
                break;
            case 'App\Models\Tracking':
                $result = env('APP_MEDIA_URL') . '/' . Tracking::$imageFolder . $file;
                break;
            case 'App\Models\Withdraw':
                $result = env('APP_MEDIA_URL') . '/' . Withdraw::$imageFolder . $file;
                break;
            case 'App\Models\NoCode':
                $result = env('APP_MEDIA_URL') . '/' . NoCode::$imageFolder . $file;
                break;
            case 'App\Models\Claims':
                $result = env('APP_MEDIA_URL') . '/' . Claims::$imageFolder . $file;
                break;
            default:
                $result = env('APP_MEDIA_URL') . '/' . self::$imageFolder . $file;
                break;
        }
        return $result;
    }

    public function relatable()
    {
        return $this->morphTo();
    }
}
