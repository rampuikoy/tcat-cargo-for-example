<?php

namespace App\Http\Controllers\Backend\NoRole;

use App\Http\Controllers\Controller;
use App\Interfaces\ThaiShippingAreaPriceInterface;
use App\Interfaces\ThaiShippingMethodInterface;
use App\Interfaces\ThaiShippingWeightPriceInterface;
use App\Interfaces\ThaiShippingWeightSizePriceInterface;
use Illuminate\Http\Request;

class ThaiShippingMethodController extends Controller
{
    //
    protected $thaiShippingWeightPriceInterface, $thaiShippingWeightSizePriceInterface, $thaiShippingAreaPriceInterface, $thaiShippingMethodInterface;

    public function __construct(ThaiShippingWeightPriceInterface $thaiShippingWeightPriceInterface, ThaiShippingWeightSizePriceInterface $thaiShippingWeightSizePriceInterface, ThaiShippingAreaPriceInterface $thaiShippingAreaPriceInterface, ThaiShippingMethodInterface $thaiShippingMethodInterface)
    {
        $this->thaiShippingWeightPriceInterface = $thaiShippingWeightPriceInterface;
        $this->thaiShippingWeightSizePriceInterface = $thaiShippingWeightSizePriceInterface;
        $this->thaiShippingAreaPriceInterface = $thaiShippingAreaPriceInterface;
        $this->thaiShippingMethodInterface = $thaiShippingMethodInterface;
    }

    public function weightPrice(Request $request, $methodId)
    {
        $columns = $request->input('columns', ['*']);
        $findShipping = $this->thaiShippingWeightPriceInterface->findManyByShippingId($methodId, $columns);
        return response()->json($findShipping, $findShipping->code);
    }

    public function weightSizePrice(Request $request, $methodId)
    {
        $columns = $request->input('columns', ['*']);
        $findShipping = $this->thaiShippingWeightSizePriceInterface->findManyByShippingId($methodId, $columns);
        return response()->json($findShipping, $findShipping->code);
    }

    public function areaPrice(Request $request, $methodId)
    {
        $columns = $request->input('columns', ['*']);
        $findShipping = $this->thaiShippingAreaPriceInterface->findManyByShippingId($methodId, $columns);
        return response()->json($findShipping, $findShipping->code);
    }

    public function droppointPrice(Request $request, $methodId)
    {
        $columns = $request->input('columns', ['id', 'title_th', 'type', 'calculate_type', 'shipping_rate_weight', 'shipping_rate_cubic']);
        $findShipping = $this->thaiShippingMethodInterface->findDroppointById($methodId, $columns);
        return response()->json($findShipping, $findShipping->code);
    }
}
