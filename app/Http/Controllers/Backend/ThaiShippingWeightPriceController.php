<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\ThaiShippingWeightPrice\StoreWeightPriceMethodRequest;
use App\Http\Requests\ThaiShippingWeightPrice\UpdateWeightPriceMethodRequest;
use App\Interfaces\ThaiShippingWeightPriceInterface;
use Illuminate\Http\Request;

class ThaiShippingWeightPriceController extends Controller
{
    protected $thaiShippingWeightPriceInterface;

    public function __construct(ThaiShippingWeightPriceInterface $thaiShippingWeightPriceInterface)
    {
        $this->thaiShippingWeightPriceInterface = $thaiShippingWeightPriceInterface;
    }

    public function index(Request $request)
    {
        $this->authorize('index', ThaiShippingWeightPrice::class);
        $columns = $request->input('columns', ['*']);
        $thaiShippingWeightPrice = $this->thaiShippingWeightPriceInterface->all($columns);
        return response()->json($thaiShippingWeightPrice, $thaiShippingWeightPrice->code);
    }

    public function show(Request $request, $id)
    {
        $this->authorize('show', ThaiShippingWeightPrice::class);
        $columns = $request->input('columns', ['*']);
        $thaiShippingWeightPrice = $this->thaiShippingWeightPriceInterface->findById($id, $columns);
        return response()->json($thaiShippingWeightPrice, $thaiShippingWeightPrice->code);
    }

    public function search(Request $request)
    {
        $this->authorize('index', ThaiShippingWeightPrice::class);
        $conditions = $request->only(['thai_shipping_method_id']);
        $columns = $request->input('columns', ['*']);
        $limit = $request->input('limit', $this->thaiShippingWeightPriceInterface::DEFAULT_LIMIT);
        $sorting = $request->input('order_by', $this->thaiShippingWeightPriceInterface::DEFAULT_SORTING);
        $ascending = $request->input('ascending') == 1 ? 'asc' : 'desc';

        if ($request->boolean('pagination')) {
            $page = $request->input('page', 1);
            $thaiShippingWeightPrice = $this->thaiShippingWeightPriceInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset = $request->input('page', 0);
            $thaiShippingWeightPrice = $this->thaiShippingWeightPriceInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($thaiShippingWeightPrice, $thaiShippingWeightPrice->code);
    }

    public function store(StoreWeightPriceMethodRequest $request)
    {
        $data = $request->only(['title', 'min_weight', 'max_weight', 'price', 'thai_shipping_method_id']);
        $thaiShippingWeightPrice = $this->thaiShippingWeightPriceInterface->store($data);
        return response()->json($thaiShippingWeightPrice, $thaiShippingWeightPrice->code);
    }

    public function update(UpdateWeightPriceMethodRequest $request, $id)
    {
        $data = $request->only(['title', 'min_weight', 'max_weight', 'price']);
        $thaiShippingWeightPrice = $this->thaiShippingWeightPriceInterface->updateById($id, $data);
        return response()->json($thaiShippingWeightPrice, $thaiShippingWeightPrice->code);
    }

    public function destroy($id)
    {
        $this->authorize('destroy', ThaiShippingWeightPrice::class);
        $thaiShippingWeightPrice = $this->thaiShippingWeightPriceInterface->softDeleteById($id);
        return response()->json($thaiShippingWeightPrice, $thaiShippingWeightPrice->code);
    }
}
