<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommissionRatePolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('extend.commission-rate');
    }
}
