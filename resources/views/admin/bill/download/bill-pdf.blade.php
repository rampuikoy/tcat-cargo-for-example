<!DOCTYPE html>
<html lang="th">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>{{$bill->no}}</title>
	{!! Html::style('public/css/pdf.css') !!}
	<link rel="stylesheet" href="https://res.imemo.co/tcatcargo_alpha/misc/css/pdf.css" media="all" />
	<style>
		div {
			line-height: 12px;
		}

		html {
			margin: 30px 20px;
		}
	</style>
</head>

<body>
	<div>
		<table aria-describedby="main-bill-pdf">
			<tr>
				<th scope="col"></th>
			</tr>
			@include('admin.bill.download.partials.bill-header')
			<tr>
				<td colspan="2" class="text-center">
					<div class="page-header">ใบแจ้งค่าบริการขนส่งสินค้า</div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					@include('admin.bill.download.partials.bill-user')
				</td>
			</tr>
			<tr>
				<td colspan="2" style="padding:10px 5px 10px 5px;">
					@include('admin.bill.download.partials.bill-oversea-shipping')
				</td>
			</tr>
			<tr>
				<td colspan="2" style="padding-bottom: 10px;">
					@include('admin.bill.download.partials.bill-thai-shipping')
				</td>
			</tr>
			<tr>
				<td style="vertical-align: top;" width="40%">
					<table aria-describedby="bill-pdf">
						<tr>
							<th scope="col"></th>
						</tr>
						<tbody>
							<tr>
								<td class="text-right" width="50%">
									<div>@lang('admin.exchange_rate')&nbsp;:</div>
								</td>
								<td class="">
									<div>{{number_format($bill->exchange_rate, 2)}}</div>
								</td>
							</tr>
							<tr>
								<td class="text-right">
									<div>จำนวนแทร็ค&nbsp;:</div>
								</td>
								<td class="">
									<div>{{$bill->counter}}</div>
								</td>
							</tr>
							@if($bill->cost_box_china>0)
							<tr>
								<td class="text-right">
									<div>@lang('admin.china_cost_box')&nbsp;:</div>
								</td>
								<td class="">
									<div>{{number_format($bill->cost_box_china, 2)}}&nbsp;@lang('admin.cny')</div>
								</td>
							</tr>
							@endif
							@if($bill->cost_shipping_china>0)
							<tr>
								<td class="text-right">
									<div>@lang('admin.china_cost_shipping')&nbsp;:</div>
								</td>
								<td class="">
									<div>{{number_format($bill->cost_shipping_china, 2)}}&nbsp;@lang('admin.cny')</div>
								</td>
							</tr>
							@endif
							<tr>
								<td class="text-right">
									<div>@lang('admin.user_remark')&nbsp;:</div>
								</td>
								<td class="">
									<div>{{ $bill->user_remark}}</div>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
				<td style="vertical-align: top;padding-bottom: 10px;">
					@include('admin.bill.download.partials.bill-summary')
				</td>
			</tr>
			//TODO change data show with status Bill
			@if($bill->status == 7)
			<tr>
				<td colspan="2">
					@include('admin.bill.download.partials.bill-thai-shipping-summary')
				</td>
			</tr>
			@endif

			@if($bill->credits->count() > 0)
			<tr>
				<td colspan="2" style="padding-bottom: 20px;padding-top: 20px;">
					@include('admin.bill.download.partials.bill-payment')
				</td>
			</tr>
			@else
			<tr>
				<td colspan="2" style="padding: 0px 5px 5px 5px;">
					@include('admin.bill.download.partials.bill-bank')
				</td>
			</tr>
			@endif
			// TODO status Bill END
			<tr>
				<td colspan="2" style="padding: 5px 5px 5px 5px;">
					@include('admin.bill.download.partials.bill-contact')
				</td>
			</tr>
			<tr>
				<td class="text-center" style="padding: 10px;">
					{!!'<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($bill->user_code, 'C128',2,30) . '" alt="barcode" />'!!} <br>
					<small>@lang('admin.user_code'):&nbsp;{{$bill->user_code}}</small>
				</td>
				<td class="text-center" style="padding: 10px;">
					{!!'<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($bill->no, 'C128',2,30) . '" alt="barcode" />'!!} <br>
					<small>@lang('admin.bill_no'):&nbsp;{{$bill->no}}</small>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div style="border: 1px solid #333;padding: 3px 5px;">** @lang('admin.admin_remark')&nbsp;: {{ $bill->admin_remark}} </div>
				</td>
			</tr>
		</table>
	</div>
	<div class="page-break"></div>
	<div style="padding: 5px;">
		@include('admin.bill.download.partials.bill-tracking')
	</div>
</body>

</html>