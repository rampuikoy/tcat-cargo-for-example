<?php

namespace App\Repositories;

use App\Interfaces\ProductInterface;
use App\Models\Product;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use App\Traits\UploadTrait;
use Illuminate\Support\Facades\Cache;


class ProductRepository implements ProductInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait, UploadTrait;

    const MINUTE_CACHE          = 1;
    const TABLE_RELETIONS       = [];

    public function __construct(Product $product)
    {
        $this->product        = $product;
    }

    public function all(array $columns = ['*'], $sorting = ProductInterface::DEFAULT_SORTING, $ascending = ProductInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $products   = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])
                        ->remember($key, $time, function () use ($columns, $sorting, $ascending) {
                            return $this->product::select($columns)
                            ->whereStatus('active')
                            ->orderBy($sorting, $ascending)
                            ->get();
                    });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.product')]), $products);
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = productInterface::DEFAULT_LIMIT, $sorting = productInterface::DEFAULT_SORTING, $ascending = productInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $products   = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
                            return $this->product::select($columns)
                                ->search($conditions)
                                ->orderBy($sorting, $ascending)
                                ->skip($offset)
                                ->limit($limit)
                                ->get();
                    });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.product')]), $products);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = productInterface::DEFAULT_LIMIT, $sorting = productInterface::DEFAULT_SORTING, $ascending = productInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $products     = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
                        $model = $this->product::select($columns)
                            ->search($conditions)
                            ->orderBy($sorting, $ascending);

                        if ($page == 1) {
                            return $model->paginate($limit);
                        } else {
                            return $model->skip($limit * ($page - 1))->paginate($limit);
                        }
                    });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.product')]), $products);
    }

    public function findById($id, array $columns = ['*'])
    {
        $product = $this->findByIdMethod($id, $columns);

        if (!$product) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.product'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.product')]), $product);
    }

    public function dropdownList()
    {
        $key        = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $dropdown   = Cache::remember($key, $time, function () {
                            return [
                                'status'    => $this->product::statusList(),
                            ];
                        });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function store(array $data)
    {
        if (@$data['image']) {
            $data['image'] = $this->storeFile($data['image']);
        }
        $product  = $this->product::create([
                                        'title'         => $data['title'],
                                        'code'          => $data['code'],
                                        'image'         => $data['image'],
                                        'point'         => $data['point'],
                                        'stock'         => $data['stock'],
                                        'limit'         => $data['limit'],
                                        'status'        => $data['status'],
                                        'description'   => $data['description'],
                                        'remark'        => $data['remark'],
                                    ]);
        $this->clearCacheTags();
        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.product')]), $product);
    }

    public function updateById($id, array $data)
    {
        $product    = $this->findByIdMethod($id);
        if (!$product) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.product'), 'id' => $id]));
        }

        if (@$data['image']) {
            $data['image'] = $this->storeFile($data['image']);
        }
       $product->update([
                                        'title'         => $data['title'],
                                        'code'          => $data['code'],
                                        'image'         => $data['image'] ?? $product->image,
                                        'point'         => $data['point'],
                                        'stock'         => $data['stock'],
                                        'limit'         => $data['limit'],
                                        'status'        => $data['status'],
                                        'description'   => $data['description'],
                                        'remark'        => $data['remark'],
                                    ]);
        $this->clearCacheTags();
        $this->clearCacheTagById($product->id);
        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.product')]), $product);
    }

    public function softDeleteById($id)
    {
        $product      = $this->findByIdMethod($id);

        if (!$product) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.product'), 'id' => $id]));
        }

        $product->delete();
        $this->clearCacheTags();
        $this->clearCacheTagById($product->id);
        return $this->coreResponse(200, __('messages.delete_data', ['data' => __('messages.product')]), $product);
    }

    public function findByIdMethod($id, array $columns = ['*'])
    {
        $key        = $this->getCacheKey(null, $id);
        $time       = $this->getTime(self::MINUTE_CACHE);

        $product      = Cache::tags([
                            $this->getClassPrefix(),
                            $this->getFindByIdWithClassPrefix(),
                            $this->getIdWithClassPrefix($id)
                        ])
                        ->remember($key, $time, function () use ($id, $columns) {
                            return $this->product::select($columns)->whereId($id)->first();
                        });

        return $product;
    }

    private function storeFile($file)
    {
        if (!$file) {
            return null;
        }
        return $this->uploadBase64($file, $this->product::$imageFolder, '', $this->product::$imageWidth);
    }

    private function clearCacheTags()
    {
        Cache::tags([
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix()
        ])
        ->flush();
    }

    private function clearCacheTagById($id)
    {
        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();
    }
}
