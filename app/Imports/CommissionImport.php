<?php

namespace App\Imports;

use App\Imports\Commission\CommissionNormal;
use App\Imports\Commission\CommissionSpecial;
use App\Imports\Commission\CommissionSummary;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class CommissionImport implements WithMultipleSheets
{
    use Importable;
    
    public $sheet1, $sheet2, $sheet3;

    public function __construct()
    {
        $this->sheet1 = new CommissionNormal();
        $this->sheet2 = new CommissionSpecial();
        $this->sheet3 = new CommissionSummary();
    }

    public function sheets(): array
    {
        return [
            0 => $this->sheet1,
            1 => $this->sheet2,
            2 => $this->sheet3,
        ];
    }
}
