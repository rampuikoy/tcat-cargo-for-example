<?php
namespace App\Helpers;

/**
 * calculate
 */
class Calculator
{
    public static function dimension($width, $height, $lenght)
    {
        $cal = $width * $height * $lenght;
        return $cal;
    }

    public static function dimensionWeight($width, $height, $lenght)
    {
        $cal = $width * $height * $lenght / 5000;
        return $cal;
    }

    public static function cubic($width, $height, $lenght)
    {
        $cal = $width * $height * $lenght / 1000000;
        return $cal;
    }

    public static function calculateType($dimension, $weight)
    {
        return ($dimension > $weight) ? 'cubic' : 'weight';
    }

    public static function isNumeric($value)
    {
        return is_numeric($value) ? $value : null;
    }

}
