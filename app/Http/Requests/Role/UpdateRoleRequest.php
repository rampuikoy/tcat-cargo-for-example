<?php

namespace App\Http\Requests\Role;

use App\Http\Requests\FormRequest;

class UpdateRoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.role.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('role');
        $rules = [
            'name'          => 'required|max:191|unique:roles,name,'.$id,
            'guard_name'    => 'required|max:191',
            'permissions'   => 'sometimes|array'
                
        ];
        return $rules;
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name'          => __('form.role.name'),
            'guard_name'    => __('form.role.guard_name'),
            'permissions'   => __('form.role.permissions'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
