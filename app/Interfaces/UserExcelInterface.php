<?php

namespace App\Interfaces;

interface UserExcelInterface
{
    public function validateExcelFile($file);

    public function createMany(array $data);

}
