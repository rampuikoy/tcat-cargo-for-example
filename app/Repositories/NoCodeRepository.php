<?php

namespace App\Repositories;

use App\Helpers\ScopeQueries;
use App\Helpers\Util;
use App\Interfaces\NoCodeInterface;
use App\Models\NoCode;
use App\Models\Tracking;
use App\Models\TrackingTaoBaoDetail;
use App\Models\TrackingTaoBaoHasBill;
use App\Models\TrackingTmall;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use Illuminate\Support\Facades\Cache;

class NoCodeRepository implements NoCodeInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const MINUTE_CACHE = 1;

    public function __construct(NoCode $nocode, Tracking $tracking, TrackingTaoBaoDetail $trackingTaobao, TrackingTaoBaoHasBill $trackingTaobaoBill, TrackingTmall $trackingTmall)
    {
        $this->nocode               = $nocode;
        $this->tracking             = $tracking;
        $this->trackingTaobao       = $trackingTaobao;
        $this->trackingTaobaoBill   = $trackingTaobaoBill;
        $this->trackingTmall        = $trackingTmall;
    }

    public function all(array $columns = ['*'], $sorting = NoCodeInterface::DEFAULT_SORTING, $ascending = NoCodeInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $nocodes    = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
                        return $this->nocode::with('images')
                                            ->select($columns)
                                            ->orderBy($sorting, $ascending)
                                            ->get();
                    });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.no_code')]), $nocodes);
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = nocodeInterface::DEFAULT_LIMIT, $sorting = nocodeInterface::DEFAULT_SORTING, $ascending = nocodeInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $nocodes    = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
                        return $this->nocode::with('images')
                                            ->select($columns)
                                            ->where(function ($query) use ($conditions) {
                                                ScopeQueries::scopeSearchNoCode($query, $conditions);
                                            })
                                            ->orderBy($sorting, $ascending)
                                            ->skip($offset)
                                            ->limit($limit)
                                            ->get();
                    });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.no_code')]), $nocodes);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = nocodeInterface::DEFAULT_LIMIT, $sorting = nocodeInterface::DEFAULT_SORTING, $ascending = nocodeInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $nocodes    = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
                        $model  = $this->nocode::with('images')
                                                ->select($columns)
                                                ->where(function ($query) use ($conditions) {
                                                    ScopeQueries::scopeSearchNoCode($query, $conditions);
                                                })
                                                ->orderBy($sorting, $ascending);

                        if ($page == 1) {
                            return $model->paginate($limit);
                        } else {
                            return $model->skip($limit * ($page - 1))->paginate($limit);
                        }
                    });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.no_code')]), $nocodes);
    }

    public function paginateGalleryByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = nocodeInterface::DEFAULT_LIMIT, $sorting = nocodeInterface::DEFAULT_SORTING, $ascending = nocodeInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);
    
        $nocodes    = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
                        $model  = $this->nocode::with('images')
                                                ->select($columns)
                                                ->where(function ($query) use ($conditions) {
                                                    ScopeQueries::scopeSearchNoCode($query, $conditions);
                                                    $query->whereIn('id', function($query){
                                                        $query->from('uploads')->select('relatable_id')->where('relatable_type', 'App\\Models\\NoCode');
                                                    });
                                                })
                                                ->orderBy($sorting, $ascending);
                        if ($page == 1) {
                            return $model->paginate($limit);
                        } else {
                            return $model->skip($limit * ($page - 1))->paginate($limit);
                        }
                    });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.no_code')]), $nocodes);
    }

    public function dropdownList()
    {
        $key        = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $dropdown   = Cache::remember($key, $time, function () {
                        return [
                            'status'        => $this->nocode::statusList(),
                            'type'          => $this->nocode::typeList(),
                            'department'    => $this->nocode::departmentList(),
                            'timeFormat'    => $this->nocode::timeFormatList(),
                            'source'        => $this->nocode::sourceList(),
                            'condition'     => $this->nocode::conditionList(),
                        ];
                    });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function findById($id, array $columns = ['*'])
    {
        $nocode     = $this->findByIdMethod($id, $columns);
        if (!$nocode) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.no_code'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.no_code')]), $nocode);
    }

    public function findByCode($code, array $columns = ['*'])
    {
        $nocode     = $this->findByCodeMethod($code, $columns);
        if (!$nocode) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.no_code'), 'id' => $code]));
        }
        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.no_code')]), $nocode);
    }

    public function store($code, array $options)
    {
        $new_nocode     = $this->getNoCodeWithExternal($code, $options);
        $nocode         = $this->nocode::create($new_nocode->toArray());
        $nocode->refresh()->load('images');
        $this->clearCacheTags();
        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.no_code')]), $nocode);
    }

    public function updateById($id, array $data)
    {
        $nocode         = $this->findByIdMethod($id);

        if (!$nocode) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.no_code'), 'id' => $id]));
        }

        if (!empty($data['zone'])) {
            $data['status'] = 2;
        }

        $nocode->update($data);
        Cache::tags([$this->getIdWithClassPrefix($id), $this->getIdWithClassPrefix($nocode->code)])->flush();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.no_code')]), $nocode);
    }

    public function cancelById($id)
    {
        $nocode     = $this->findByIdMethod($id);

        if (!$nocode) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.no_code'), 'id' => $id]));
        }
        if (!in_array($nocode->status, [1, 2])) {
            return $this->coreResponse(500, __('messages.no_data_status', ['data' => __('messages.no_code'), 'status' => $nocode->status]));
        }

        $nocode->update(['status' => 0]);

        Cache::tags([$this->getIdWithClassPrefix($nocode->id), $this->getIdWithClassPrefix($nocode->code)])->flush();
        $this->clearCacheTags();
        return $this->coreResponse(200, __('messages.cancel_data', ['data' => __('messages.no_code')]), $nocode);
    }

    public function zoneInByCode($code, array $data)
    {
        $nocode     = $this->findByCodeMethod($code);
        if (@$nocode->status === 3) {
            return $this->coreResponse(500, __('messages.nocode_out_before', ['code' => $nocode->code, 'date' => $nocode->date_out, 'taken' => $nocode->taken_admin]), $nocode);
        }
        // Create
        if (!$nocode) {
            $options                = collect($data)->only(['tcat', 'tmall', 'taobao'])->toArray();
            $new_nocode             = $this->getNoCodeWithExternal($code, $options);
            $new_nocode->zone       = Util::concatField($new_nocode->zone, $data['zone']);
            $new_nocode->date_in    = date('Y-m-d');
            $new_nocode->status     = !empty($new_nocode->zone) ? 2 : 1;

            $nocode = $this->nocode::create($new_nocode->toArray());
            $nocode->refresh();
        }
        // Update
        else {
            $nocode->zone           = Util::concatField($nocode->zone, $data['zone']);
            $nocode->status         = !empty($nocode->zone) && $nocode->status === 1 ? 2 : $nocode->status;
            $nocode->update();
        }
        Cache::tags([$this->getIdWithClassPrefix($nocode->id), $this->getIdWithClassPrefix($nocode->code)])->flush();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.no_code')]), $nocode);
    }

    public function zoneOutByCode($code, array $data)
    {
        $nocode     = $this->findByCodeMethod($code);
        if (!$nocode) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.no_code'), 'id' => $code]));
        }
        if ($nocode->status === 3) {
            return $this->coreResponse(500, __('messages.nocode_out_before', ['code' => $nocode->code, 'date' => $nocode->date_out, 'taken' => $nocode->taken_admin]), $nocode);
        }

        $nocode->status         = 3;
        $nocode->taken_admin    = Util::concatField($nocode->taken_admin, $data['taken_admin']);
        $nocode->order_code     = Util::concatField($nocode->order_code, $data['order_code']);
        $nocode->date_out       = date('Y-m-d H:i:s');
        $nocode->update();

        Cache::tags([$this->getIdWithClassPrefix($nocode->id), $this->getIdWithClassPrefix($nocode->code)])->flush();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.no_code')]), $nocode);
    }

    public function clearCacheTagById($id)
    {
        $nocode     = $this->findByIdMethod($id);
        Cache::tags([$this->getFindByIdWithClassPrefix($nocode->id), $this->getIdWithClassPrefix($nocode->code)])->flush();
    }

    private function findByIdMethod($id, array $columns = ['*'])
    {
        $key    = $this->getCacheKey(null, $id);
        $time   = $this->getTime(self::MINUTE_CACHE);

        $nocode = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($id),
        ])->remember($key, $time, function () use ($id, $columns) {
            return $this->nocode::with('images')->select($columns)->whereId($id)->first();
        });

        return $nocode;
    }

    private function findByCodeMethod($code, array $columns = ['*'])
    {
        $key    = $this->getCacheKey(null, $code);
        $time   = $this->getTime(self::MINUTE_CACHE);

        $nocode = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($code),
        ])->remember($key, $time, function () use ($code, $columns) {
            return $this->nocode::with('images')->select($columns)->whereCode($code)->first();
        });

        return $nocode;
    }

    private function getNoCodeWithExternal($code, $options)
    {
        $nocode         = new $this->nocode(['code' => $code, 'date_in' => date('Y-m-d')]);

        if ($options['tcat']) {
            $trackings  = $this->tracking::where('code', 'like', $nocode['code'] . '%')->get();
            if ($trackings->count() > 0) {
                $nocode->source = Util::concatField($nocode->source, 'TCAT-CARGO');

                foreach ($trackings as $tracking) {
                    $nocode->source_order   = Util::concatField($nocode->source_order, $tracking->user_code);
                    $nocode->system_remark  = Util::concatField($nocode->system_remark, $tracking->admin_remark);
                    if ($tracking->weight > 0) {
                        $nocode->weight     = $tracking->weight;
                    }
                    if ($tracking->width > 0) {
                        $nocode->width      = $tracking->width;
                    }
                    if ($tracking->length > 0) {
                        $nocode->length     = $tracking->length;
                    }
                    if ($tracking->height > 0) {
                        $nocode->height     = $tracking->height;
                    }
                }
            }
        }
        if ($options['taobao']) {
            $trackings  = $this->trackingTaobao::where('tracking_code', 'like', $nocode->code . '%')->get();
            if ($trackings->count() > 0) {
                $nocode->source     = Util::concatField($nocode->source, 'TAOBAO');

                foreach ($trackings as $tracking) {
                    if ($tracking->weight > 0) {
                        $nocode->weight         = $tracking->weight;
                    }
                    if ($tracking->width > 0) {
                        $nocode->width          = $tracking->width;
                    }
                    if ($tracking->length > 0) {
                        $nocode->length         = $tracking->length;
                    }
                    if ($tracking->height > 0) {
                        $nocode->height         = $tracking->height;
                    }
                    $tmp = $this->trackingTaobaoBill::code($tracking->tracking_code);
                    if (isset($tmp->tracking_code)) {
                        $nocode->source_order   = Util::concatField($nocode->source_order, $tmp->bill_code);
                    }
                }
            }
        }
        if ($options['tmall']) {
            $trackings  = $this->trackingTmall::where('tracking', 'like', $nocode->code . '%')->get();
            if ($trackings->count() > 0) {
                $nocode->source     = Util::concatField($nocode->source, 'TCAT-MALL');

                foreach ($trackings as $tracking) {
                    $nocode->source_order   = Util::concatField($nocode->source_order, $tracking->po);
                    $nocode->system_remark  = Util::concatField($nocode->system_remark, $tracking->remark);
                    if ($tracking->weight > 0) {
                        $nocode->weight     = $tracking->weight;
                    }
                    if ($tracking->width > 0) {
                        $nocode->width      = $tracking->width;
                    }
                    if ($tracking->length > 0) {
                        $nocode->length     = $tracking->length;
                    }
                    if ($tracking->height > 0) {
                        $nocode->height     = $tracking->height;
                    }
                }
            }
        }
        return $nocode;
    }

    private function clearCacheTags()
    {
        Cache::tags([
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix(),
        ])
            ->flush();
    }
}
