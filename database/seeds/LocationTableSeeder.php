<?php

use Illuminate\Database\Seeder;
use App\Helpers\MoveSqlData;

class LocationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $move_geo_data    = collect(MoveSqlData::insertLocation());
        $message          = $move_geo_data->get('message');
        $error            = $move_geo_data->get('error');

        if($error){
            $this->command->error($message);
            $this->command->warning('Please insert fresh again');
            return;
        }else{
            $this->command->info($message);
        }
    }
}
