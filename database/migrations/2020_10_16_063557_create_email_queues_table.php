<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmailQueuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_queues', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mailable_class', 255)->nullable();
            $table->nullableMorphs('mailable');
            $table->string('to', 255);
            $table->enum('status', ['queue', 'success'])->default('queue');
            $table->integer('created_admin_id')->unsigned()->nullable();
            $table->string('created_admin', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_queues');
    }
}
