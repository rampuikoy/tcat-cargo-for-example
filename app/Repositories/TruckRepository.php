<?php

namespace App\Repositories;

use App\Interfaces\TruckInterface;
use App\Models\Truck;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use Illuminate\Support\Facades\Cache;

class TruckRepository implements TruckInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const CACHE_KEY             = 'TRUCK';
    const CACHE_TAG_ALL         = 'TRUCK_ALL';
    const CACHE_TAG_FETCH       = 'TRUCK_FETCH';
    const CACHE_TAG_PAGINATION  = 'TRUCK_PAGINATION';
    const MINUTE_CACHE          = 1;
    const TABLE_RELETIONS       = [];

    public function __construct(Truck $truck)
    {
        $this->truck        = $truck;
    }

    public function all(array $columns = ['*'], $sorting = TruckInterface::DEFAULT_SORTING, $ascending = TruckInterface::DEFAULT_ASCENDING)
    {
        $key = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time = $this->getTime(self::MINUTE_CACHE);

        $trucks     = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
            return $this->truck::select($columns)->whereStatus('active')->orderBy($sorting, $ascending)->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.truck')]), $trucks);
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = TruckInterface::DEFAULT_LIMIT, $sorting = TruckInterface::DEFAULT_SORTING, $ascending = TruckInterface::DEFAULT_ASCENDING)
    {

        $key = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time = $this->getTime(self::MINUTE_CACHE);

        $trucks     = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
            return $this->truck::select($columns)
                ->search($conditions)
                ->orderBy($sorting, $ascending)
                ->skip($offset)
                ->limit($limit)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.truck')]), $trucks);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = TruckInterface::DEFAULT_LIMIT, $sorting = TruckInterface::DEFAULT_SORTING, $ascending = TruckInterface::DEFAULT_ASCENDING)
    {
        $key = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time = $this->getTime(self::MINUTE_CACHE);

        $trucks     = Cache::tags([
            $this->getClassPrefix(),
            $this->getPaginationWithClassPrefix(),
        ])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
            $model = $this->truck::select($columns)
                ->search($conditions)
                ->orderBy($sorting, $ascending);

            if ($page == 1) {
                return $model->paginate($limit);
            } else {
                return $model->skip($limit * ($page - 1))->paginate($limit);
            }
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.truck')]), $trucks);
    }

    public function findById($id, array $columns = ['*'])
    {
        $truck  = $this->findByIdMethod($id);
        if (!$truck) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.truck'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.truck')]), $truck);
    }

    public function dropdownList()
    {
        $key = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time = $this->getTime(self::MINUTE_CACHE);

        $dropdown   = Cache::remember($key, $time, function () {
            return [
                'status' => $this->truck::statusList()
            ];
        });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function store(array $data)
    {
        $truck  = $this->truck::create($data);
        Cache::tags([$this->getIdWithClassPrefix($truck->id)])->flush();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.truck')]), $truck);
    }

    public function updateById($id, array $data)
    {
        $truck      = $this->findByIdMethod($id);
        if (!$truck) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.truck'), 'id' => $id]));
        }

        $truck->update($data);
        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.truck')]), $truck);
    }

    public function softDeleteById($id)
    {
        $truck      = $this->findByIdMethod($id);
        if (!$truck) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.truck'), 'id' => $id]));
        }
        $truck->delete();
        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.delete_data', ['data' => __('messages.truck')]), $truck);
    }

    public function findByIdMethod($id, array $columns = ['*'])
    {
        $key = $this->getCacheKey(null, $id);
        $time = $this->getTime(self::MINUTE_CACHE);
        $truck  = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($id)
        ])->remember($key, $time, function () use ($id, $columns) {
            return $this->truck::select($columns)->find($id);
        });
        return $truck;
    }

    private function clearCacheTags()
    {
        Cache::tags([
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix()
        ])->flush();
    }
}
