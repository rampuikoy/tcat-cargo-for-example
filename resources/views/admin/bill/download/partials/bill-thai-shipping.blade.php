<table class="thai-shipping" aria-describedby="thai-shipping">
	<thead style="background: #dfdfdf;">
		<tr>
			<th scope="col" colspan="2" class="bordered">
				<div>
					@lang('admin.thai_shipping_method')&nbsp;/&nbsp;{!!str_replace(' ', '&nbsp;',$bill->thaiShippingMethod->title)!!}
				</div>
			</th>
		</tr>
	</thead>
	<tbody class="bordered" style="font-size: 19px;">
		<tr>
			<td width="25%" class="text-right">
				<div>ชื่อผู้รับ&nbsp;: </div>
			</td>
			<td>
				<div>{!!str_replace(' ', '&nbsp;',$bill->shipping_name)!!}&nbsp;/&nbsp;โทร&nbsp;:&nbsp;{{$bill->shipping_tel}}
				</div>
			</td>
		</tr>
		<tr>
			<td class="text-right">
				<div>@lang('admin.address')&nbsp;: </div>
			</td>
			<td>
				<div>
					{!!str_replace(' ', '&nbsp;',$bill->shipping_address)!!}&nbsp;ต.{{@$bill->shippingDistrict->title_th}}&nbsp;อ.{{@$bill->shippingAmphur->title_th}}&nbsp;จ.{{@$bill->shippingProvince->title_th}}&nbsp;{{@$bill->shipping_zipcode}}
				</div>
			</td>
		</tr>
		<tr>
			<td class="text-right">
				<div>@lang('admin.thai_shipping_detail')&nbsp;: </div>
			</td>
			<td>
				<div>
					{{$bill->thai_shipping_detail}}
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<div style="height: 3px;"></div>
			</td>
		</tr>
	</tbody>
</table>