<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermissionPolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.permission.index');
    }

    public function show(Admin $admin)
    {
        return $admin->hasAnyPermission(['backend.permission.show', 'backend.permission.update']);
    }

    public function store(Admin $admin)
    {
        return $admin->can('backend.permission.store');
    }

    public function update(Admin $admin)
    {
        return $admin->can('backend.permission.update');
    }

    public function destroy(Admin $admin)
    {
        return $admin->can('backend.permission.destroy');
    }

    public function all(Admin $admin)
    {
        return $admin->hasAnyPermission(['backend.permission.index', 'backend.permission.show', 'backend.admin.store', 'backend.admin.update']);
    }
}
