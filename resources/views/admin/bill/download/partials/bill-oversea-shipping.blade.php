<table class="bordered" aria-describedby="bill-oversea-shipping">
	<thead style="background: #dfdfdf;">
		<tr>
			<th scope="col" colspan="9">
				<div>@lang('admin.oversea_shipping_method_price')</div>
			</th>
		</tr>
		<tr>
			<th scope="col" rowspan="2" width="90">
				<div>@lang('admin.shipping_method')</div>
			</th>
			<th scope="col" colspan="4">
				<div>@lang('admin.weight')</div>
			</th>
			<th scope="col" colspan="4">
				<div>@lang('admin.cubic')</div>
			</th>
		</tr>
		<tr>
			<th scope="col" class="text-right">
				<div>@lang('admin.amount_tracking')</div>
			</th>
			<th scope="col" class="text-right">
				<div>@lang('admin.weight')</div>
			</th>
			<th scope="col" class="text-right">
				<div>@lang('admin.shipping_rate_weight')</div>
			</th>
			<th scope="col" class="text-right">
				<div>@lang('admin.shipping_cost')</div>
			</th>
			<th scope="col" class="text-right">
				<div>@lang('admin.amount_tracking')</div>
			</th>
			<th scope="col" class="text-right">
				<div>@lang('admin.cubic')</div>
			</th>
			<th scope="col" class="text-right">
				<div>@lang('admin.shipping_rate_cubic')</div>
			</th>
			<th scope="col" class="text-right">
				<div>@lang('admin.shipping_cost')</div>
			</th>
		</tr>
	</thead>
	<tbody>
	@php 
		$rates = json_decode($bill->shipping_rate);
	@endphp
    @if(@$rates->track_counter)
		@if($rates->track_counter->kg_car_genaral>0 || $rates->track_counter->cubic_car_genaral>0)
		<tr>
			<td>
				<div>ทางรถ&nbsp;(ทั่วไป)</div>
			</td>
			<td class="text-right">
				<div>{{ $rates->track_counter->kg_car_genaral }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_unit->kg_car_genaral,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->kg_car_genaral,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_price->kg_car_genaral,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ $rates->track_counter->cubic_car_genaral }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_unit->cubic_car_genaral,4) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->cubic_car_genaral,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_price->cubic_car_genaral,2) }}</div>
			</td>
		</tr>
		@endif
		@if($rates->track_counter->kg_car_iso>0 || $rates->track_counter->cubic_car_iso>0)
		<tr>
			<td>
				<div>ทางรถ&nbsp;(มอก.)</div>
			</td>
			<td class="text-right">
				<div>{{ $rates->track_counter->kg_car_iso }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_unit->kg_car_iso,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->kg_car_iso,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_price->kg_car_iso,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ $rates->track_counter->cubic_car_iso }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_unit->cubic_car_iso,4) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->cubic_car_iso,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_price->cubic_car_iso,2) }}</div>
			</td>
		</tr>
		@endif
		@if($rates->track_counter->kg_car_brand>0 || $rates->track_counter->cubic_car_brand>0)
		<tr>
			<td>
				<div>ทางรถ&nbsp;(แบรนด์/ลิขสิทธฺ์)</div>
			</td>
			<td class="text-right">
				<div>{{ $rates->track_counter->kg_car_brand }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_unit->kg_car_brand,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->kg_car_brand,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_price->kg_car_brand,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ $rates->track_counter->cubic_car_brand }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_unit->cubic_car_brand,4) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->cubic_car_brand,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_price->cubic_car_brand,2) }}</div>
			</td>
		</tr>
		@endif
		@if($rates->track_counter->kg_ship_genaral>0 || $rates->track_counter->cubic_ship_genaral>0)
		<tr>
			<td>
				<div>ทางเรือ&nbsp;(ทั่วไป)</div>
			</td>
			<td class="text-right">
				<div>{{ $rates->track_counter->kg_ship_genaral }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_unit->kg_ship_genaral,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->kg_ship_genaral,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_price->kg_ship_genaral,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ $rates->track_counter->cubic_ship_genaral }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_unit->cubic_ship_genaral,4) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->cubic_ship_genaral,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_price->cubic_ship_genaral,2) }}</div>
			</td>
		</tr>
		@endif
		@if($rates->track_counter->kg_ship_iso>0 || $rates->track_counter->cubic_ship_iso>0)
		<tr>
			<td>
				<div>ทางเรือ&nbsp;(มอก.)</div>
			</td>
			<td class="text-right">
				<div>{{ $rates->track_counter->kg_ship_iso }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_unit->kg_ship_iso,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->kg_ship_iso,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_price->kg_ship_iso,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ $rates->track_counter->cubic_ship_iso }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_unit->cubic_ship_iso,4) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->cubic_ship_iso,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_price->cubic_ship_iso,2) }}</div>
			</td>
		</tr>
		@endif
		@if($rates->track_counter->kg_ship_brand>0 || $rates->track_counter->cubic_ship_brand>0)
		<tr>
			<td>
				<div>ทางเรือ&nbsp;(แบรนด์/ลิขสิทธฺ์)</div>
			</td>
			<td class="text-right">
				<div>{{ $rates->track_counter->kg_ship_brand }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_unit->kg_ship_brand,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->kg_ship_brand,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_price->kg_ship_brand,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ $rates->track_counter->cubic_ship_brand }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_unit->cubic_ship_brand,4) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->cubic_ship_brand,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_price->cubic_ship_brand,2) }}</div>
			</td>
		</tr>
		@endif
		@if($rates->track_counter->kg_plane_genaral>0 || $rates->track_counter->cubic_plane_genaral>0)
		<tr>
			<td>
				<div>เครื่องบิน&nbsp;(ทั่วไป)</div>
			</td>
			<td class="text-right">
				<div>{{ $rates->track_counter->kg_plane_genaral }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_unit->kg_plane_genaral,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->kg_plane_genaral,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_price->kg_plane_genaral,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ $rates->track_counter->cubic_plane_genaral }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_unit->cubic_plane_genaral,4) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->cubic_plane_genaral,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_price->cubic_plane_genaral,2) }}</div>
			</td>
		</tr>
		@endif
		@if($rates->track_counter->kg_plane_iso>0 || $rates->track_counter->cubic_plane_iso>0)
		<tr>
			<td>
				<div>เครื่องบิน&nbsp;(มอก.)</div>
			</td>
			<td class="text-right">
				<div>{{ $rates->track_counter->kg_plane_iso }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_unit->kg_plane_iso,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->kg_plane_iso,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_price->kg_plane_iso,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ $rates->track_counter->cubic_plane_iso }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_unit->cubic_plane_iso,4) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->cubic_plane_iso,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_price->cubic_plane_iso,2) }}</div>
			</td>
		</tr>
		@endif
		@if($rates->track_counter->kg_plane_brand>0 || $rates->track_counter->cubic_plane_brand>0)
		<tr>
			<td>
				<div>เครื่องบิน&nbsp;(แบรนด์/ลิขสิทธฺ์)</div>
			</td>
			<td class="text-right">
				<div>{{ $rates->track_counter->kg_plane_brand }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_unit->kg_plane_brand,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->kg_plane_brand,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_price->kg_plane_brand,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ $rates->track_counter->cubic_plane_brand }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_unit->cubic_plane_brand,4) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->cubic_plane_brand,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->sum_price->cubic_plane_brand,2) }}</div>
			</td>
		</tr>
		@endif

    @endif
	</tbody>
	<tfoot>
		<tr>
			<td>
				<div>รวม</div>
			</td>
			<td class="text-right">
				<div>{{ $rates->counter_weight }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->total_weight,2) }}</div>
			</td>
			<td class="text-right">
				<div>-</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->price_weight,2) }}</div>
			</td>
			<td class="text-right">
				<div>{{ $rates->counter_cubic }}</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->total_cubic,4) }}</div>
			</td>
			<td class="text-right">
				<div>-</div>
			</td>
			<td class="text-right">
				<div>{{ number_format($rates->price_cubic,2) }}</div>
			</td>
		</tr>
	</tfoot>
</table>
