<?php

namespace App\Http\Controllers\Backend;

use App\Models\ProductType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\ProductTypeInterface;
use App\Http\Requests\ProductType\StoreProductTypeRequest;
use App\Http\Requests\ProductType\UpdateProductTypeRequest;

class ProductTypeController extends Controller
{
    protected $productTypeInterface;

    public function __construct(ProductTypeInterface $productTypeInterface)
    {
        $this->productTypeInterface = $productTypeInterface;
    }

    public function index(Request $request)
    {
        $this->authorize('index', ProductType::class);
        $columns        = $request->input('columns', ['*']);
        $productTypes   = $this->productTypeInterface->all($columns);
        return response()->json($productTypes, $productTypes->code);
    }

    public function show(Request $request, $id)
    {
        $this->authorize('show', ProductType::class);
        $columns        = $request->input('columns', ['*']);
        $productType    = $this->productTypeInterface->findById($id, $columns);
        return response()->json($productType, $productType->code);
    }

    public function search(Request $request)
    {
        $this->authorize('index', ProductType::class);
        $conditions     = $request->only(['id', 'title_th', 'title_en', 'title_cn', 'status', 'search']);
        $columns        = $request->input('columns', ['*']);
        $limit          = $request->input('limit', $this->productTypeInterface::DEFAULT_LIMIT);
        $sorting        = $request->input('order_by', $this->productTypeInterface::DEFAULT_SORTING);
        $ascending      = $request->input('ascending') == 1 ? 'asc' : 'desc';

        if ($request->boolean('pagination')) {
            $page           = $request->input('page', 1);
            $productTypes   = $this->productTypeInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset         = $request->input('page', 0);
            $productTypes   = $this->productTypeInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($productTypes, $productTypes->code);
    }

    public function dropdown()
    {
        $dropdown = $this->productTypeInterface->dropdownList();
        return response()->json($dropdown, $dropdown->code);
    }

    public function store(StoreProductTypeRequest $request)
    {
        $data           = $request->only([
                            'title_th',
                            'title_en',
                            'title_cn',
                            'status',
                            'remark',
                        ]);
        $productType   = $this->productTypeInterface->store($data);
        return response()->json($productType, $productType->code);
    }

    public function update(UpdateProductTypeRequest $request, $id)
    {
        $data           = $request->only([
                            'title_th',
                            'title_en',
                            'title_cn',
                            'status',
                            'remark',
                        ]);
        $productType   = $this->productTypeInterface->updateById($id, $data);
        return response()->json($productType, $productType->code);
    }
    public function destroy($id)
    {
        $this->authorize('destroy', ProductType::class);
        $productType = $this->productTypeInterface->softDeleteById($id);
        return response()->json($productType, $productType->code);
    }
}
