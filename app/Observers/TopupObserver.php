<?php

namespace App\Observers;

use App\Jobs\JobSendEmail;
use App\Jobs\JobSendFcm;
use App\Jobs\JobSendSms;
use App\Models\Topup;
use App\Observers\BaseObserver;
use App\Events\NotiEevent;
use App\Helpers\Noti;

class TopupObserver extends BaseObserver
{
    //
    public function creating(Topup $topup)
    {
        $this->stampCreated($topup);
    }

    public function updating(Topup $topup)
    {
        $this->stampUpdated($topup);

        if ($topup->isDirty('date') || $topup->isDirty('time')) {
            $topup_duplicate = collect($topup)
                ->only(['date', 'time', 'amount', 'method', 'system_bank_account_id'])
                ->map(function ($value) {
                    return $value;
                });

            $count = Topup::SearchDuplicate($topup_duplicate)->count();
            if ($count > 1) {
                Topup::SearchDuplicate($topup_duplicate)
                    ->update(['duplicate' => 1]);
                $topup->duplicate = 1;
            }
        }
    }

    public function created(Topup $topup)
    {
        $topup_duplicate = collect($topup)
            ->only(['date', 'time', 'amount', 'method', 'system_bank_account_id'])
            ->map(function ($value) {
                return $value;
            });

        $count = Topup::SearchDuplicate($topup_duplicate)->count();
        if ($count > 1) {
            Topup::SearchDuplicate($topup_duplicate)
                ->update(['duplicate' => 1]);
        }
        broadcast(new NotiEevent())->toOthers();
    }

    public function updated(Topup $topup)
    {
        $user       = $topup->user;
        $amount     = number_format($topup->amount, 2);
        $total      = number_format($user->credit->total ?? 0, 2);

        if ($topup->isDirty('status') && $topup->status === 'approved') {
            $email      = $topup->emails()->create([
                'mailable_class'    => 'App\Mail\Topup\TopupApproved',
                'status'            => 'queue',
                'to'                => $user->email,
                'created_admin'     => $topup->created_admin,
                'created_admin_id'  => $topup->created_admin_id,
            ]);
            JobSendEmail::dispatch($email->id)->onQueue(env('QUEUE_EMAIL_PREFIX'));

            $fcm        = $topup->fcm()->create([
                'status'            => 'queue',
                'user_code'         => $user->code,
                'title'             => __('fcm.topup_approved.title', ['no' => $topup->no]),
                'message'           => __('fcm.topup_approved.message', ['amount' => $amount, 'credit' => $total]),
            ]);
            JobSendFcm::dispatch($fcm->id)->onQueue(env('QUEUE_FCM_PREFIX'));

            if ($user->sms_notification === 'active') {
                return;
            }
            $sms        = $topup->sms()->create([
                'status'            => 'queue',
                'user_code'         => $user->code,
                'msisdn'            => $user->tel1,
                'message'           => __('sms.topup_approved', ['no' => $topup->no, 'amount' => $amount, 'total' => $total]),
                'created_admin'     => $topup->created_admin,
                'created_admin_id'  => $topup->created_admin_id,
            ]);
            JobSendSms::dispatch($sms->id)->onQueue(env('QUEUE_SMS_PREFIX'));
        } else if ($topup->isDirty('status') && $topup->status === 'cancel') {
            // $fcm        = $topup->fcm()->create([
            //                 'status'            => 'queue',
            //                 'user_code'         => $user->code,
            //                 'title'             => __('fcm.topup_cancel.title', ['no' => $topup->no]),
            //                 'message'           => __('fcm.topup_cancel.message', ['remark' => $topup->user_remark]),
            //             ]);
            // JobSendFcm::dispatch($fcm->id)->onQueue(env('QUEUE_FCM_PREFIX'));
        }
    }
}
