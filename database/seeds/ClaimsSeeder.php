<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Claims;

class ClaimsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Claims::factory(10)->create();
    }
}
