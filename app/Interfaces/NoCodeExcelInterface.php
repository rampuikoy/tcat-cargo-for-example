<?php

namespace App\Interfaces;

interface NoCodeExcelInterface
{
    public function validateExcelFile($file);

    public function createOrUpdate(array $data);
}
