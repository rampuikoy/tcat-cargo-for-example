<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\ImportExcelRequest;
use App\Http\Requests\User\ResetPasswordUserRequest;
use App\Http\Requests\User\SettingBillRequest;
use App\Http\Requests\User\SettingFcmTokenRequest;
use App\Http\Requests\User\SettingNotificationRequest;
use App\Http\Requests\User\StoreUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Interfaces\AdminInterface;
use App\Interfaces\ProvinceInterface;
use App\Interfaces\BillInterface;
use App\Interfaces\TripInterface;
use App\Interfaces\RateInterface;
use App\Interfaces\ThaiShippingMethodInterface;
use App\Interfaces\TopupInterface;
use App\Interfaces\UserInterface;
use App\Interfaces\TrackingInterface;
use App\Interfaces\WithdrawInterface;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
	protected $userInterface, $adminInterface, $provinceInterface, $thaiShippingMethodInterface, $rateInterface, $trackingInterface, $withdrawInterface, $topupInterface, $billInterface, $tripInterface;

	public function __construct(UserInterface $userInterface, AdminInterface $adminInterface, ProvinceInterface $provinceInterface, ThaiShippingMethodInterface $thaiShippingMethodInterface, RateInterface $rateInterface, TrackingInterface $trackingInterface, WithdrawInterface $withdrawInterface, TopupInterface $topupInterface, BillInterface $billInterface, TripInterface $tripInterface)
	{
		$this->userInterface 				= $userInterface;
		$this->adminInterface 				= $adminInterface;
		$this->provinceInterface 			= $provinceInterface;
		$this->thaiShippingMethodInterface 	= $thaiShippingMethodInterface;
		$this->rateInterface 				= $rateInterface;
		$this->trackingInterface 			= $trackingInterface;
		$this->withdrawInterface 			= $withdrawInterface;
		$this->topupInterface 				= $topupInterface;
		$this->billInterface 				= $billInterface;
		$this->tripInterface 				= $tripInterface;
	}

	public function index(Request $request)
	{
		$this->authorize('index', User::class);
		$columns 		= $request->input('columns', ['*']);
		$users 			= $this->userInterface->all($columns);
		return response()->json($users, $users->code);
	}

	public function show(Request $request, $code)
	{
		$this->authorize('show', User::class);
		$columns 		= $request->input('columns', ['*']);
		$user 			= $this->userInterface->findByCode($code, $columns);
		return response()->json($user, $user->code);
	}

	public function search(Request $request)
	{
		$this->authorize('index', User::class);
		$conditions 	= $request->only(['id', 'code', 'name', 'email', 'tel', 'default_rate_id', 'thai_shipping_method_id', 'province_id', 'admin_ref_id', 'tax_id', 'date_start', 'date_end', 'user_remark', 'admin_remark', 'sms_notification', 'status', 'condition']);
		$columns 		= $request->input('columns', ['*']);
		$limit 			= $request->input('limit', $this->userInterface::DEFAULT_LIMIT);
		$sorting 		= $request->input('order_by', $this->userInterface::DEFAULT_SORTING);
		$ascending 		= $request->input('ascending') == 1 ? 'asc' : 'desc';
		$offset 		= $request->input('page', 0);

		if ($request->boolean('pagination')) {
			$page 		= $request->input('page', 1);
			$users 		= $this->userInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
		} else {
			$offset 	= $request->input('page', 0);
			$users 		= $this->userInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
		}
		return response()->json($users, $users->code);
	}

	public function searchPoint(Request $request)
	{
		$this->authorize('index', User::class);
		$conditions 	= $request->only(['code', 'name', 'email', 'tel', 'default_rate_id', 'thai_shipping_method_id', 'province_id', 'admin_ref_id', 'tax_id', 'date_start', 'date_end', 'user_remark', 'admin_remark', 'sms_notification', 'status', 'condition']);
		$columns 		= $request->input('columns', ['*']);
		$limit 			= $request->input('limit', $this->userInterface::DEFAULT_LIMIT);
		$sorting 		= $request->input('order_by', $this->userInterface::DEFAULT_SORTING_POINT);
		$ascending 		= $request->input('ascending') == 1 ? 'asc' : 'desc';
		$offset 		= $request->input('page', 0);

		if ($request->boolean('pagination')) {
			$page 		= $request->input('page', 1);
			$users 		= $this->userInterface->paginateListByFieldsPoints($conditions, $columns, $page, $limit, $sorting, $ascending);
		} else {
			$offset 	= $request->input('page', 0);
			$users 		= $this->userInterface->fetchListByFieldsPoints($conditions, $columns, $offset, $limit, $sorting, $ascending);
		}
		return response()->json($users, $users->code);
	}

	public function dropdown()
	{
		$dropdown       = $this->userInterface->dropdownList();
		if (!$dropdown->error) {
			$sales      = $this->adminInterface->findManySale(['id', 'name']);
			$province   = $this->provinceInterface->all(['id', 'title_th']);
			$rate       = $this->rateInterface->all(['id', 'title']);
			$shipping   = $this->thaiShippingMethodInterface->all(['id', 'title_th']);

			$dropdown->results = collect($dropdown->results)->merge([
				'sales'                 => (!$sales->error) ? $sales->results : [],
				'province'              => (!$province->error) ? $province->results : [],
				'rate'                  => (!$rate->error) ? $rate->results : [],
				'thai_shipping_method'  => (!$shipping->error) ? $shipping->results : [],
			]);
		}
		return response()->json($dropdown, $dropdown->code);
	}

	public function store(StoreUserRequest $request)
	{
		$data 	= $request->only([
			'name',
			'code',
			'email',
			'password',
			'tel1',
			'tel2',
			'tax_id',

			'status',
			'withholding',
			'alert_box',
			'sms_notification',
			'create_bill',

			'province_id',
			'thai_shipping_method_id',
			'admin_ref_id',

			'alert_message',
			// 'user_remark',
			'admin_remark',
		]);
		$user 	= $this->userInterface->store($data);
		return response()->json($user, $user->code);
	}

	public function update(UpdateUserRequest $request, $id)
	{
		$data 	= $request->only([
			'name',
			'email',
			'tel1',
			'tel2',
			'tax_id',
			'status',
			'province_id',
			'admin_ref_id',
			'admin_remark',
		]);
		$user 	= $this->userInterface->updateById($id, $data);
		return response()->json($user, $user->code);
	}

	public function ResetPassword(ResetPasswordUserRequest $request, $id)
	{
		$password = $request->input('password');
		$user = $this->userInterface->ResetPassword($id, $password);
		return response()->json($user, $user->code);
	}

	public function settingBill(SettingBillRequest $request, $id)
	{
		$data 	= $request->only(['withholding', 'create_bill', 'thai_shipping_method_id']);
		$user 	= $this->userInterface->updateById($id, $data);
		return response()->json($user, $user->code);
	}

	public function settingNotification(SettingNotificationRequest $request, $id)
	{
		$data 	= $request->only(['sms_notification', 'alert_box', 'alert_message']);
		$user 	= $this->userInterface->updateById($id, $data);
		return response()->json($user, $user->code);
	}

	public function settingFcmToken(SettingFcmTokenRequest $request, $id)
	{
		$token 	= $request->token;
		$user 	= $this->userInterface->updateFcmTokenById($id, $token);
		return response()->json($user, $user->code);
	}

	public function destroy($id)
	{
		$this->authorize('destroy', User::class);
		$user 	= $this->userInterface->softDeleteById($id);
		return response()->json($user, $user->code);
	}

	public function ImportExcel(ImportExcelRequest $request)
	{
		$file 	= $request->file('file');
		$user 	= $this->userInterface->ImportExcel($file);
		return response()->json($user, $user->code);
	}

	public function overview($code)
	{
		$tracking  	= $this->trackingInterface->findByUserCode($code);
		$withdraw  	= $this->withdrawInterface->findByUserCode($code);
		$topup  	= $this->topupInterface->findByUserCode($code);
		$bill       = $this->billInterface->findByUserCode($code);
		$trip       = $this->tripInterface->findByUserCode($code);
		$overview 	= [
			'withdraw' 					=> (!$withdraw->error) ? $withdraw->results : [],
			'topup' 					=> (!$topup->error) ? $topup->results : [],
			'tracking' 					=> (!$tracking->error) ? $tracking->results : [],
			'transport_bill' 			=> (!$bill->error) ? $bill->results : [],
			'bill_transfers_to_china' 	=> [],
			'bill_trip_tour' 			=> (!$trip->error) ? $trip->results : [],
		];
		return response()->json($overview, 200);
	}
}
