<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rates', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('title', 50);
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->text('admin_remark')->nullable();
            $table->text('user_code')->nullable();
            $table->double('kg_car_genaral', 8, 2)->default(0.00);
            $table->double('kg_car_iso', 8, 2)->default(0.00);
            $table->double('kg_car_brand', 8, 2)->default(0.00);

            $table->double('kg_ship_genaral', 8, 2)->default(0.00);
            $table->double('kg_ship_iso', 8, 2)->default(0.00);
            $table->double('kg_ship_brand', 8, 2)->default(0.00);

            $table->double('kg_plane_genaral', 8, 2)->default(0.00);
            $table->double('kg_plane_iso', 8, 2)->default(0.00);
            $table->double('kg_plane_brand', 8, 2)->default(0.00);

            $table->double('cubic_car_genaral', 8, 2)->default(0.00);
            $table->double('cubic_car_iso', 8, 2)->default(0.00);
            $table->double('cubic_car_brand', 8, 2)->default(0.00);

            $table->double('cubic_ship_genaral', 8, 2)->default(0.00);
            $table->double('cubic_ship_iso', 8, 2)->default(0.00);
            $table->double('cubic_ship_brand', 8, 2)->default(0.00);

            $table->double('cubic_plane_genaral', 8, 2)->default(0.00);
            $table->double('cubic_plane_iso', 8, 2)->default(0.00);
            $table->double('cubic_plane_brand', 8, 2)->default(0.00);

            $table->string('created_admin', 20);
            $table->string('updated_admin', 20)->nullable();
            $table->integer('created_admin_id')->unsigned()->nullable();
            $table->integer('updated_admin_id')->unsigned()->nullable();

            $table->tinyInteger('default_rate')->nullable()->default(0);

            $table->enum('type', ['general', 'user', 'sale','sale_only_user'])->default('user');
            $table->integer('admin_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rate');
    }
}
