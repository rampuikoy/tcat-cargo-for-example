<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Interfaces\ReportInterface;
use App\Policies\ExtendPolicy;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    protected $reportInterface;

    public function __construct(ReportInterface $reportInterface)
    {
        $this->reportInterface = $reportInterface;
    }

    public function getUser(Request $request)
    {
        $this->authorize('report', ExtendPolicy::class);
        $conditions     = $request->only(['date_start', 'date_end']);
        $reports        = $this->reportInterface->fetchUserByFields($conditions);
        return response()->json($reports, $reports->code);
    }

    public function getActiveUser(Request $request)
    {
        $this->authorize('report', ExtendPolicy::class);
        $conditions     = $request->only(['date_start', 'date_end', 'code', 'name', 'tel', 'email', 'user_remark', 'admin_remark']);
        $limit          = $request->input('limit', $this->reportInterface::DEFAULT_LIMIT);
        $sorting        = $request->input('order_by', $this->reportInterface::DEFAULT_SORTING);
        $ascending      = $request->input('ascending') == 1 ? 'asc' : 'desc';
        $page           = $request->input('page', 1);
        $reports        = $this->reportInterface->paginateActiveUserByFields($conditions, $page, $limit, $sorting, $ascending);
        return response()->json($reports, $reports->code);
    }

    public function getTracking(Request $request)
    {
        $this->authorize('report', ExtendPolicy::class);
        $conditions     = $request->only(['field', 'date_start', 'date_end', 'status']);
        $reports        = $this->reportInterface->tracking($conditions);
        return response()->json($reports, $reports->code);
    }

    public function getBill(Request $request)
    {
        $this->authorize('report', ExtendPolicy::class);
        $conditions     = $request->only(['date_start', 'date_end', 'shipping_method_id', 'province_id', 'truck_id']);
        $reports        = $this->reportInterface->fetchBillList($conditions);
        return response()->json($reports, $reports->code);
    }

    public function getTruck(Request $request)
    {
        $this->authorize('report', ExtendPolicy::class);
        $conditions     = $request->only(['date_start', 'date_end', 'truck_id', 'shipping_method_id', 'driver_id']);
        $reports        = $this->reportInterface->fetchTruckList($conditions);
        return response()->json($reports, $reports->code);
    }

    public function getTopup(Request $request)
    {
        $this->authorize('report', ExtendPolicy::class);
        $conditions     = $request->only(['date_start', 'date_end']);
        $reports        = $this->reportInterface->topup($conditions);
        return response()->json($reports, $reports->code);
    }

    public function getWithdraw(Request $request)
    {
        $this->authorize('report', ExtendPolicy::class);
        $conditions     = $request->only(['date_start', 'date_end']);
        $reports        = $this->reportInterface->fetchWithdrawListByFields($conditions);
        return response()->json($reports, $reports->code);
    }
}
