<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\BankAccount;
use App\Models\Admin;
use Tymon\JWTAuth\Facades\JWTAuth;
class BankAccountTest extends TestCase
{
  use WithFaker;
  protected $user;
  protected $bankAccount;
  protected $url = '/api/backend/bank-account';
  /**
   * A basic feature test example.
   *
   * @return void
   */
  protected function authenticate(string $guard = 'admins')
  {
    $this->user = factory(Admin::class)->create();
    auth()->shouldUse($guard);
    $this->bankAccount = factory(BankAccount::class)->create();
    $token = JWTAuth::fromUser($this->user);
    return 'Bearer ' . $token;
  }

  protected function deleteUser()
  {
    $this->bankAccount->forceDelete();
    $this->user->forceDelete();
  }

  /** @test */
  public function GetBankAccountSearchSuccessTest()
  {
    $id = random_int(1, 200);
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', "{$this->url}/search", [
      "id" => $id,
      "search" => "",
      "title" => "",
      "account" => "",
      "user_code" => "",
      "bank" => "",
      "branch" => "",
      "status" => "",
      "user_remark" => "",
      "admin_remark" => "",
    ]);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function GetBankAccountSearchFailTest()
  {
    $response = $this->json('GET', "{$this->url}/search");
    $response->assertStatus(401);
  }

  /** @test */
  public function GetBankAccountDropdownSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', "{$this->url}/dropdown");
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function GetBankAccountDropdownFailTest()
  {
    $response = $this->json('GET', "{$this->url}/dropdown");
    $response->assertStatus(401);
  }

  /** @test */
  public function GetBankAccountAllSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', $this->url);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function GetBankAccountAllFailTest()
  {
    $response = $this->json('GET', $this->url);
    $response->assertStatus(401);
  }

  /** @test */
  public function BankAccountPostSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('POST', $this->url, [
      'title'          => 'test',
      'account'      => 'test-test',
      'branch' => 'test',
      'bank'    => '1234567890',
      'user_code'    => 'SF0002',
    ]);
    $response->assertStatus(200);
    BankAccount::where('account', 'test-test')->forceDelete();
    $this->deleteUser();
  }

  /** @test */
  public function BankAccountPostFailAuthTest()
  {
    $response = $this->json('POST', $this->url);
    $response->assertStatus(401);
  }

  /** @test */
  public function BankAccountPostFailTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('POST', $this->url);
    $response->assertStatus(422);
    $this->deleteUser();
  }

  /** @test */
  public function BankAccountUpdateByIdSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('PUT', "{$this->url}/{$this->bankAccount->id}", [
      'title'          => 'test',
      'account'      => 'test-test',
      'branch' => 'test',
      'bank'    => '1234567890',
      'user_code'    => 'SF0002',
    ]);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function BankAccountUpdateByIdFailAuthTest()
  {
    $response = $this->json('PUT', "{$this->url}/10");
    $response->assertStatus(401);
  }

  /** @test */
  public function BankAccountUpdateByIdFailTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' =>  $token,
    ])->json('PUT', "{$this->url}/{$this->bankAccount->id}");
    $response->assertStatus(422);
    $this->deleteUser();
  }

  /** @test */
  public function BankAccountDeleteSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('DELETE', "{$this->url}/{$this->bankAccount->id}");
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function BankAccountDeleteFailTest()
  {
    $response = $this->json('DELETE', "{$this->url}/5");
    $response->assertStatus(401);
  }
}
