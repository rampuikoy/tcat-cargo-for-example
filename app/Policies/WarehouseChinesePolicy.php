<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class WarehouseChinesePolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.warehouse-chinese.index');
    }
    public function show(Admin $admin)
    {
        return $admin->hasAnyPermission(['backend.warehouse-chinese.show', 'backend.warehouse-chinese.update']);
    }

    public function store(Admin $admin)
    {
        return $admin->can('backend.warehouse-chinese.store');
    }
    public function update(Admin $admin)
    {
        return $admin->can('backend.warehouse-chinese.update');
    }

    public function destroy(Admin $admin)
    {
        return $admin->can('backend.warehouse-chinese.destroy');
    }
}
