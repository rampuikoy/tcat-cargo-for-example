<?php

namespace App\Http\Controllers\Backend;

use App\Models\Bill;
use Illuminate\Http\Request;
use App\Interfaces\BillInterface;
use App\Interfaces\RateInterface;
use App\Interfaces\AdminInterface;
use App\Interfaces\TruckInterface;
use App\Interfaces\UploadInterface;
use App\Http\Controllers\Controller;
use App\Interfaces\ProvinceInterface;
use App\Interfaces\TrackingInterface;
use App\Interfaces\UserAddressInterface;
use App\Interfaces\ExchangeRateInterface;
use App\Http\Requests\Bill\PackBillRequest;
use App\Http\Requests\Bill\StoreBillRequest;
use App\Http\Requests\Bill\BillCancelRequest;
use App\Http\Requests\Bill\PostDriverRequest;
use App\Http\Requests\Bill\UploadBillRequest;
use App\Http\Requests\Bill\DroppointInRequest;
use App\Interfaces\ThaiShippingMethodInterface;
use App\Http\Requests\Bill\UploadSignatureBillRequest;
use App\Http\Requests\Bill\NotifyThaiShippingBillRequest;

class BillController extends Controller
{
    protected $billInterface, $trackingInterface, $userAddressInterface, $exchangeRateInterface, $rateInterface, $thaiShippingMethodInterface, $uploadInterface, $adminInterface;

    public function __construct(Bill $bill, BillInterface $billInterface, TrackingInterface $trackingInterface, UserAddressInterface $userAddressInterface, ExchangeRateInterface $exchangeRateInterface, RateInterface $rateInterface, ThaiShippingMethodInterface $thaiShippingMethodInterface, UploadInterface $uploadInterface, AdminInterface $adminInterface, ProvinceInterface $provinceInterface, TruckInterface $truckInterface)
    {
        $this->billInterface            = $billInterface;
        $this->trackingInterface        = $trackingInterface;
        $this->userAddressInterface     = $userAddressInterface;
        $this->exchangeRateInterface    = $exchangeRateInterface;
        $this->rateInterface            = $rateInterface;
        $this->thaiShippingMethodInterface  = $thaiShippingMethodInterface;
        $this->uploadInterface          = $uploadInterface;
        $this->adminInterface           = $adminInterface;
        $this->provinceInterface        = $provinceInterface;
        $this->truckInterface           = $truckInterface;
        $this->bill                     = $bill;
    }

    public function index(Request $request)
    {
        $this->authorize('index', Bill::class);
        $columns = $request->input('columns', ['*']);
        $bill = $this->billInterface->all($columns);
        return response()->json($bill, $bill->code);
    }

    public function show(Request $request, $id)
    {
        $this->authorize('show', Bill::class);
        $columns = $request->input('columns', ['*']);
        $bill = $this->billInterface->findById($id, $columns);
        return response()->json($bill, $bill->code);
    }

    public function search(Request $request)
    {
        $this->authorize('index', Bill::class);
        $conditions = $request->only([
            'bill_list',
            'bill_no',
            'user_code',
            'coupon_code',
            'droppoint_in',
            'status',
            'thai_shipping_method_id',
            'shipping_province_id',
            'amphur',
            'time_format',
            'staff',
            'date_start',
            'date_end',
            'user_remark',
            'admin_remark',
            'truck_id',
            'driver_id',
            'condition',
            'rate_id',
            'zone_a',
            'zone_b',
            'zone_c',
            'zone_d',
        ]);
        $columns = $request->input('columns', ['*']);
        $limit = $request->input('limit', $this->billInterface::DEFAULT_LIMIT);
        $sorting = $request->input('order_by', $this->billInterface::DEFAULT_SORTING);
        $ascending = $request->input('ascending') == 1 ? 'asc' : 'desc';

        if ($request->boolean('pagination')) {
            $page = $request->input('page', 1);
            $bill = $this->billInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset = $request->input('page', 0);
            $bill = $this->billInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($bill, $bill->code);
    }

    public function dropdown()
    {
        $dropdown = $this->billInterface->dropdownList();
        if (!$dropdown->error) {
            $province               = $this->provinceInterface->all(['id', 'title_th']);
            $rate                   = $this->rateInterface->all(['id', 'title']);
            $thai_shipping_method   = $this->thaiShippingMethodInterface->all(['id', 'title_th', 'calculate_type']);
            $driver                 = $this->adminInterface->findManyDriver(['id', 'name']);
            $truck                  = $this->truckInterface->all(['id', 'title']);

            $dropdown->results      = collect($dropdown->results)
                ->merge([
                    'province'               => (!$province->error) ? $province->results : [],
                    'rate'                   => (!$rate->error) ? $rate->results : [],
                    'thai_shipping_method'   => (!$thai_shipping_method->error) ? $thai_shipping_method->results : [],
                    'driver'                 => (!$driver->error) ? $driver->results : [],
                    'truck'                  => (!$truck->error) ? $truck->results : [],
                ]);
        }
        return response()->json($dropdown, $dropdown->code);
    }

    public function store(StoreBillRequest $request)
    {
        try {
            $exchange_rate = $this->exchangeRateInterface->getRateExtend($request->user_code);
            if ($exchange_rate->error) {
                throw new \Exception($exchange_rate->message);
            }

            $rate = $this->rateInterface->findById($request->rate_id);
            if ($rate->error) {
                throw new \Exception($rate->message);
            }

            $thai_shipping = $this->thaiShippingMethodInterface->findById($request->thai_shipping_id);
            if ($thai_shipping->error) {
                throw new \Exception($thai_shipping->message);
            }

            $user_address = null;
            if ($thai_shipping->results->type !== 'droppoint') {
                $user_address = $this->userAddressInterface->findById($request->user_address_id);
                if ($user_address->error) {
                    throw new \Exception($user_address->message);
                }
            }

            $data = $request->only([
                "thai_shipping_detail",
                "user_remark",
                "admin_remark",

                "withholding",
                "other_charge1",
                "other_charge2",
                "discount1",
                "discount2",

                "user_code",
                "coupon",
                "trackings",
            ]);

            $bill = $this->billInterface->store($data, $exchange_rate->results, $rate->results, $thai_shipping->results, @$user_address->results);
            return response()->json($bill, $bill->code);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json(['error' => true, 'code' => 500, 'message' => $th->getMessage()], 500);
        }
    }

    public function stepCreate(Request $request)
    {
        $this->authorize('update', Bill::class);
        $data = $request->only(['bill_id']);
        $bill = $this->billInterface->updateStepCreate($data);
        return response()->json($bill, $bill->code);
    }

    public function update(Request $request, $id)
    {
        $data              = $request->only(['admin_remark']);
        $bill              = $this->billInterface->updateById($id, $data);
        return response()->json($bill, $bill->code);
    }

    public function approve(Request $request, $id)
    {
        $this->authorize('approve', Bill::class);
        $data = $request->only([]);
        $bill = $this->billInterface->approveById($id, $data);
        return response()->json($bill, $bill->code);
    }

    public function cancel(Request $request, $id)
    {
        $this->authorize('cancel', Bill::class);
        $data = $request->only([]);
        $bill = $this->billInterface->cancelById($id, $data);
        return response()->json($bill, $bill->code);
    }

    public function optionCreate($code)
    {
        $this->authorize('store', Bill::class);
        $options = $this->billInterface->optionCreate($code);
        if (!$options->error) {
            $trackings = $this->trackingInterface->findByUserCodeExtend($code);
            $addresses = $this->userAddressInterface->findByUserCodeExtend($code);
            $exchange_rate = $this->exchangeRateInterface->getRateExtend($code);
            $rate = $this->rateInterface->getDefaultUser($code);
            $thai_shipping_method = $this->thaiShippingMethodInterface->all();

            $options->results = collect($options->results)->merge([
                'trackings' => (!$trackings->error) ? $trackings->results : [],
                'addresses' => (!$addresses->error) ? $addresses->results : [],
                'exchange_rate' => (!$exchange_rate->error) ? $exchange_rate->results : [],
                'rates' => (!$rate->error) ? $rate->results : [],
                'thai_shipping_method' => (!$thai_shipping_method->error) ? $thai_shipping_method->results : [],
            ]);
        }
        return response()->json($options, $options->code);
    }

    public function packProduct(PackBillRequest $request, $id)
    {
        $this->authorize('update', Bill::class);
        $data              = $request->only(['tracking_id']);
        $bill              = $this->billInterface->packProduct($id, $data);
        return response()->json($bill, $bill->code);
    }

    public function notifyThaiShipping(NotifyThaiShippingBillRequest $request, $id)
    {
        $this->authorize('update', Bill::class);
        $data              = $request->only([
            'thai_shipping_method_id',
            'thai_shipping_date',
            'thai_shipping_time',
            'thai_shipping_raw_price',
            'thai_shipping_raw_charge',
            'addon_refund_title',
            'addon_refund_credit',
            'thai_shipping_code'
        ]);
        $bill              = $this->billInterface->notifyThaiShipping($id, $data);
        return response()->json($bill, $bill->code);
    }

    public function optionEdit($id)
    {
        $this->authorize('update', Bill::class);
        $options = $this->billInterface->optionEdit($id);
        $options->results = collect($options->results)->merge([
            'template' => $this->bill->template() ? $this->bill->template() : [],
        ]);
        return response()->json($options, $options->code);
    }

    public function destroy($id)
    {
        $this->authorize('destroy', Bill::class);
        $bill = $this->billInterface->softDeleteById($id);
        return response()->json($bill, $bill->code);
    }

    public function upload(UploadBillRequest $request, $id)
    {
        $data = [
            'type'              => 'image',
            'relatable_type'    => 'App\Models\Bill',
            'relatable_id'      => $id,
            'file_path'         => $request->file_path,
            'sub_path'          => $request->sub_path,
        ];
        $upload    = $this->uploadInterface->store($data);
        return response()->json($upload, $upload->code);
    }

    public function postDriver(PostDriverRequest $request, $id)
    {
        $this->authorize('update', Bill::class);
        $data = [
            'truck_id' => $request->truck_id,
            'driver_admin_id' => $request->driver_admin_id,
            'driver2_admin_id' => $request->driver2_admin_id,
        ];

        $bill = $this->billInterface->postDriver($id, $data);

        return response()->json($bill, 200);
    }

    public function postBackward($id)
    {
        $this->authorize('update', Bill::class);
        $bill = $this->billInterface->postBackward($id);
        return response()->json($bill, $bill->code);
    }

    public function postCancel(BillCancelRequest $request, $id)
    {
        $data = $request->only(['remark']);
        $bill = $this->billInterface->postCancel($id, $data);
        return response()->json($bill, $bill->code);
    }

    public function notifyShipped(Request $request, $id)
    {
        $this->authorize('update', Bill::class);
        $bill              = $this->billInterface->notifyShipped($id);
        return response()->json($bill, $bill->code);
    }

    public function closeBill(Request $request, $id)
    {
        $this->authorize('update', Bill::class);
        $bill              = $this->billInterface->closeBill($id);
        return response()->json($bill, $bill->code);
    }

    public function notifyCreate(Request $request, $id)
    {
        $data = $request->only([
            'user_code',
            'payment_type_id',
            'bill_id',
            'type'
        ]);
        $this->authorize('store', Bill::class);
        $bill              = $this->billInterface->notifyCreate($id, $data);
        return response()->json($bill, $bill->code);
    }

    public function destroyUploadImage($id)
    {
        $upload = $this->uploadInterface->delete($id);
        return response()->json($upload, $upload->code);
    }

    public function rateCommission()
    {
        $this->authorize('show', Bill::class);
        $rate_sales = $this->adminInterface->findManySale(['*']);
        return response()->json($rate_sales, $rate_sales->code);
    }

    public function signature(UploadSignatureBillRequest $request, $id)
    {
        $data = $request->only([
            'file_path',
            'latitude',
            'longitude',
        ]);
        $bill = $this->billInterface->signature($id, $data);
        return response()->json($bill, $bill->code);
    }
    public function postDroppointOut(Request $request, $id)
    {
        $this->authorize('update', Bill::class);
        $data = $request->only([
            'admin_remark',
            'driver_admin_id',
            'driver2_admin_id',
            'truck_id'
        ]);
        $bill              = $this->billInterface->postDroppointOut($id, $data);
        return response()->json($bill, $bill->code);
    }
    public function updateDroppointOut(Request $request, $id)
    {
        $this->authorize('update', Bill::class);
        $data = $request->only([
            'thai_shipping_code',
            'thai_shipping_raw_price',
        ]);
        $bill              = $this->billInterface->updateDroppointOut($id, $data);
        return response()->json($bill, $bill->code);
    }
    public function postDroppointIn(Request $request, $id)
    {
        $this->authorize('update', Bill::class);
        $data = $request->only([
            'droppoint_id',
        ]);
        $bill              = $this->billInterface->postDroppointIn($id, $data);
        return response()->json($bill, $bill->code);
    }
    public function updateDroppointIn(DroppointInRequest $request)
    {
        $this->authorize('update', Bill::class);
        $data = $request->only([
            'bills',
        ]);
        $bill              = $this->billInterface->updateDroppointIn($data);
        return response()->json($bill, $bill->code);
    }
}
