<?php

namespace App\Models;

use App\Helpers\ScopeQueries;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Commission extends Model
{
    use HasFactory;

    const PREFIX = 'CM-';

    public static $fileFolder = 'excel/commission/';

    protected $table = 'commissions';

    protected $fillable = [
        'code',
        'username',
        'admin_id',
        'start_date',
        'end_date',
        'file',
        'normal_income',
        'normal_commission',
        'normal_profit',
        'custom_income',
        'custom_commission',
        'custom_profit',
        'total_commission',
        'total',
        'remark',
        'created_admin',
        'updated_admin',
        'created_admin_id',
        'updated_admin_id',
    ];

    protected $appends = ['no', 'file_url'];

    /**
     * Get the record no attribute.
     *
     * @return string
     */
    public function getNoAttribute()
    {
        return ($this->id) ? self::PREFIX . str_pad($this->id, 4, "0", STR_PAD_LEFT) : null;
    }

    /**
     * Get the file photo URL attribute.
     *
     * @return string
     */
    public function getFileUrlAttribute($handle = null)
    {
        $file = $handle ?? $this->file;
        return ($file) ? env('APP_MEDIA_URL') . '/' . self::$fileFolder . $file : null;
    }

    public function scopeSearch($query, $conditions)
    {
        return $query->where(function ($query) use ($conditions) {
            if (@$conditions['no']) {
                $trimmed = ltrim($conditions['no'], self::PREFIX . '0');
                $query->where('id', 'like', '%' . $trimmed . '%');
            }
            if (@$conditions['admin_ref_id']) {
                $query->where('admin_id', $conditions['admin_ref_id']);
            }

            $time_format = $conditions['time_format'] ?? 'created_at';
            if (@$conditions['date_start'] && @$conditions['date_end']) {
                ScopeQueries::scopeDateRange($query, $time_format, $conditions['date_start'], $conditions['date_end']);
            } else if (@$conditions['date_start']) {
                ScopeQueries::scopeDateStart($query, $time_format, $conditions['date_start']);
            } else if (@$conditions['date_end']) {
                ScopeQueries::scopeDateEnd($query, $time_format, $conditions['date_end']);
            }
        });
    }

    // reletions
    public function bills()
    {
        return $this->hasMany('App\Models\Bill', 'commission_id', 'id');
    }
}
