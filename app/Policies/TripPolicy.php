<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class TripPolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.trip.index');
    }

    public function show(Admin $admin)
    {
        return $admin->hasAnyPermission(['backend.trip.show', 'backend.trip.update']);
    }

    public function store(Admin $admin)
    {
        return $admin->can('backend.trip.store');
    }

    public function update(Admin $admin)
    {
        return $admin->can('backend.trip.update');
    }


    public function approve(Admin $admin)
    {
        return $admin->can('backend.trip.approve');
    }

    public function destroy(Admin $admin)
    {
        return $admin->can('backend.trip.destroy');
    }
}
