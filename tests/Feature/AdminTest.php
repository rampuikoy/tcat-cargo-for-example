<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Admin;
use Tymon\JWTAuth\Facades\JWTAuth;

class AdminTest extends TestCase
{
  use WithFaker;
  protected $user;
  protected $url = '/api/backend/admin';
  /**
   * A basic feature test example.
   *
   * @return void
   */
  protected function authenticate(string $guard = 'admins')
  {
    $this->user = factory(Admin::class)->create();
    auth()->shouldUse($guard);
    $token = JWTAuth::fromUser($this->user);
    return 'Bearer ' . $token;
  }

  protected function deleteUser()
  {
    return $this->user->forceDelete();
  }


  /** @test */
  public function GetAdminAllSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', $this->url);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function GetAdminAllFailTest()
  {
    $response = $this->json('GET', $this->url);
    $response->assertStatus(401);
  }

  /** @test */
  public function GetAdminSearchSuccessTest()
  {
    $id = random_int(1, 200);
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', "{$this->url}/search", [
      "id" => $id,
      "username" => "",
      "email" => "",
      "tel" => "",
      "remark" => "",
      "department" => "",
      "branch" => "",
      "droppoint_id" => "",
      "type" => "",
      "role" => "",
      "status" => "",
    ]);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function GetAdminSearchFailTest()
  {
    $response = $this->json('GET', "{$this->url}/search");
    $response->assertStatus(401);
  }

  /** @test */
  public function GetAdminDropdownSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET',  "{$this->url}/dropdown");
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function GetAdminDropdownFailTest()
  {
    $response = $this->json('GET',  "{$this->url}/dropdown");
    $response->assertStatus(401);
  }

  /** @test */
  public function GetAdminByIdSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' =>  $token,
    ])->json('GET', "{$this->url}/1");
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function GetAdminByIdFailTest()
  {
    $response = $this->json('GET',  "{$this->url}/1");
    $response->assertStatus(401);
  }

  /** @test */
  public function AdminPostSuccessTest()
  {
    $userTest = 'TestAtestAtest';
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('POST',  $this->url, [
      'name'          => $userTest,
      'lastname'      => $this->faker->lastName(),
      'email'         =>  $this->faker->email(),
      'password'      => 'a12345678',
      'password_confirmation' => 'a12345678',
      'role'          => 'SuperAdmin',
      'username'      =>  $this->faker->firstName(),
    ]);
    $response->assertStatus(200);
    Admin::where('name', $userTest)->forceDelete();
    $this->deleteUser();
  }

  /** @test */
  public function AdminPostFailAuthTest()
  {
    $response = $this->json('POST', $this->url);
    $response->assertStatus(401);
  }

  /** @test */
  public function AdminPostFailTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' =>  $token,
    ])->json('POST',  $this->url);
    $response->assertStatus(422);
    $this->deleteUser();
  }

  /** @test */
  public function AdminUpdatePasswordByIdSuccessTest()
  {
    $token = $this->authenticate();
    // $test = Admin::where('name', '=','TestAtestAtest')->get();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('PUT',  "{$this->url}/{$this->user->id}/password", [
      "password" => "a123456789",
      "password_confirmation" => "a123456789",
    ]);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function AdminUpdatePasswordByIdFailAuthTest()
  {
    $response = $this->json('PUT',  "{$this->url}/1/password");
    $response->assertStatus(401);
  }

  /** @test */
  public function AdminUpdatePasswordByIdFailTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' =>  $token,
    ])->json('PUT',  "{$this->url}/{$this->user->id}/password");
    $response->assertStatus(422);
    $this->deleteUser();
  }

  /** @test */
  public function AdminUpdateByIdSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('PUT', "{$this->url}/{$this->user->id}/password", [
      'name'          => $this->faker->firstName(),
      'lastname'      => $this->faker->lastName(),
      'email'         =>  $this->faker->email(),
      'password'      => 'a12345678',
      'password_confirmation' => 'a12345678',
      'role'          => 'SuperAdmin',
      'username'      =>  $this->faker->firstName(),
    ]);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function AdminUpdateByIdFailAuthTest()
  {
    $response = $this->json('PUT', "{$this->url}/1/password");
    $response->assertStatus(401);
  }

  /** @test */
  public function AdminUpdateByIdFailTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' =>  $token,
    ])->json('PUT',  "{$this->url}/{$this->user->id}/password");
    $response->assertStatus(422);
    $this->deleteUser();
  }

  /** @test */
  public function AdminSoftDeleteSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('DELETE',  "{$this->url}/{$this->user->id}}");
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function AdminSoftDeleteFailTest()
  {
    $response = $this->json('DELETE',  "{$this->url}/2");
    $response->assertStatus(401);
  }
}
