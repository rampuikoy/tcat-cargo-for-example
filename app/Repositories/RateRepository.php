<?php

namespace App\Repositories;

use App\Interfaces\RateInterface;
use App\Models\Rate;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use Illuminate\Support\Facades\Cache;

class RateRepository implements RateInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const MINUTE_CACHE          = 1;

    public function __construct(Rate $rate)
    {
        $this->rate = $rate;
    }

    public function getDefault()
    {
        $rate = $this->getDefault();

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.rate')]), $rate);
    }

    public function getDefaultUser(string $code, array $columns = ['*'])
    {
        $key    = $this->getCacheKey(null, $code);
        $time   = $this->getTime(self::MINUTE_CACHE);

        $rate   = Cache::tags([
                        $this->getClassPrefix(),
                        $this->getUserCodePrefix($code),
                        $this->getDefaultRateUserCodeWithClassPrefix($code),
                    ])
                    ->remember($key, $time, function () use ($code, $columns) {
                        return $this->rate::select($columns)
                            ->where(function ($query) use ($code) {
                                $query->whereNull('user_code')->orWhere('user_code', '=', $code);
                            })
                            ->orderBy('default_rate', 'DESC')
                            ->whereStatus('active')
                            ->whereIn('type', ['general', 'user'])
                            ->get();

                    });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.rate')]), $rate);
    }

    public function all(array $columns = ['*'], $sorting = RateInterface::DEFAULT_SORTING, $ascending = RateInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $rates      = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
                        return $this->rate::select($columns)->whereStatus('active')->orderBy($sorting, $ascending)->get();
                    });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.rate')]), $rates);
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = RateInterface::DEFAULT_LIMIT, $sorting = RateInterface::DEFAULT_SORTING, $ascending = RateInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $rates      = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
                        return $this->rate::select($columns)
                            ->search($conditions)
                            ->orderBy($sorting, $ascending)
                            ->skip($offset)
                            ->limit($limit)
                            ->get();
                    });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.rate')]), $rates);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = RateInterface::DEFAULT_LIMIT, $sorting = RateInterface::DEFAULT_SORTING, $ascending = RateInterface::DEFAULT_ASCENDING)
    {
        $key    = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time   = $this->getTime(self::MINUTE_CACHE);

        $rates  = Cache::tags([$this->getClassPrefix(), $this->getPaginationWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
                    $model = $this->rate::select($columns)
                        ->search($conditions)
                        ->orderBy($sorting, $ascending);

                    if ($page == 1) {
                        return $model->paginate($limit);
                    } else {
                        return $model->skip($limit * ($page - 1))->paginate($limit);
                    }
                });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.rate')]), $rates);
    }

    public function findById($id, array $columns = ['*'])
    {
        $rate = $this->findByIdMethod($id, $columns);

        if (!$rate) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.rate'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.rate')]), $rate);
    }

    public function findByUserCode($code, array $columns = ['*'])
    {
        $rate = $this->findByUserCodeMethod($code, $columns);

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.rate')]), $rate);
    }

    public function findByAdminId($id, array $columns = ['*'])
    {
        $rate = $this->findByAdminIdMethod($id, $columns);

        if (!$rate) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.rate'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.rate')]), $rate);
    }

    public function findManyByIdSales($id, array $columns = ['*'])
    {
        $rate = $this->findManyByIdSalesMethod($id, $columns);

        if ($rate->isEmpty()) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.rate'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.rate')]), $rate);
    }

    public function dropdownList()
    {
        $key        = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $dropdown   = Cache::remember($key, $time, function () {
            return [
                'status'    => $this->rate::statusList(),
                'type'      => $this->rate::typeList(),
                'condition' => $this->rate::conditionList()
            ];
        });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function store(array $data)
    {
        if (!empty($data['default_rate']) && $data['type'] == 'general') {
            $this->rate::where('default_rate', 3)->update(['default_rate' => 0]);
            $data['default_rate'] = 3;
        }

        if ($data['type'] == 'sale_only_user') {
            $duplicate = $this->checkDuplicate($data)->count();
            if ($duplicate > 0) {
                return $this->coreResponse(404, __('messages.data_with_code_sales', ['data' => $data['user_code']]));
            }
        }

        $rate   =  $this->rate::create($data);
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.rate')]), $rate);
    }

    public function updateById($id, array $data)
    {
        $rate   = $this->findByIdMethod($id);

        if (!$rate) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.rate'), 'id' => $id]));
        }

        if ($data['type'] == 'sale_only_user') {
            if ($rate->user_code !== $data['user_code']) {
                $duplicate = $this->checkDuplicate($data)->count();
                if ($duplicate > 0) {
                    return $this->coreResponse(404, __('messages.data_with_code_sales', ['data' => $data['user_code']]));
                }
            }
        }

        if (!empty($data['default_rate']) &&  $data['type'] == 'general') {
            $this->rate::where('default_rate', 3)->update(['default_rate' => 0]);
            $data['default_rate'] = 3;
        }

        $rate->update($data);
        $this->clearCacheTags();
        $this->clearCacheTagById($rate->id);
        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.rate')]), $rate);
    }

    public function updateByUserCode($code, array $data)
    {

        $rate       = $this->rate::updateOrCreate(['user_code' => $code, 'type' => 'user'], $data);
        $user       = $rate->user()->update(['default_rate_id' => $rate->id]);

        $this->clearCacheTags();
        $this->clearCacheTagById($rate->id);
        app('App\Repositories\UserRepository')->clearCacheTagByUserCode($code);
        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.rate')]), $rate);
    }

    public function softDeleteById($id)
    {
        $rate   = $this->findByIdMethod($id);

        if (!$rate) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.rate'), 'id' => $id]));
        }

        $rate->delete();
        $this->clearCacheTags();
        $this->clearCacheTagById($rate->id);
        return $this->coreResponse(200, __('messages.delete_data', ['data' => __('messages.rate')]), $rate);
    }

    private function checkDuplicate($data)
    {
        return $this->rate->where(function ($query) use ($data) {
            if (@$data['user_code'] && @$data['admin_id']) {
                $query->Where('type', 'sale_only_user')->where('user_code', $data['user_code'])->WhereNotNull('admin_id')->get();
            }
        });
    }

    private function findByIdMethod($id, array $columns = ['*'])
    {
        $key    = $this->getCacheKey(null, $id);
        $time   = $this->getTime(self::MINUTE_CACHE);

        $rate   = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($id)
        ])->remember($key, $time, function () use ($id, $columns) {
            return $this->rate::select($columns)->find($id);
        });

        return $rate;
    }

    private function findByAdminIdMethod($id, array $columns = ['*'])
    {
        $key            = $this->getCacheKey(null, 'ADMIN.'.$id);
        $time           = $this->getTime(self::MINUTE_CACHE);

        $rate           = Cache::tags([
            $this->getClassPrefix(),
            $this->getAdminIdPrefix($id),
            $this->getAdminIdWithClassPrefix($id)
        ])->remember($key, $time, function () use ($id, $columns) {
            return $this->rate::select($columns)->whereAdminId($id)->first();
        });

        return $rate;
    }

    private function findByUserCodeMethod($code, array $columns = ['*'])
    {
        $key            = $this->getCacheKey(null, $code);
        $time           = $this->getTime(self::MINUTE_CACHE);
        $rateDefault    = $this->getDefualtMethod();

        $rate           = Cache::tags([
            $this->getClassPrefix(),
            $this->getUserCodePrefix($code),
            $this->getUserCodeWithClassPrefix($code)
        ])->remember($key, $time, function () use ($code, $rateDefault, $columns) {
            $rate    = $this->rate::firstOrNew([
                'type'      => 'user',
                'user_code' => $code,
                'admin_id'  => null,
            ]);

            if (!$rate->id && !$rateDefault->error) {
                $rate->title              = $code;
                $rate->kg_car_genaral     = $rateDefault->results->kg_car_genaral;
                $rate->cubic_car_genaral  = $rateDefault->results->cubic_car_genaral;
                $rate->kg_car_iso         = $rateDefault->results->kg_car_iso;
                $rate->cubic_car_iso      = $rateDefault->results->cubic_car_iso;
                $rate->kg_car_brand       = $rateDefault->results->kg_car_brand;
                $rate->cubic_car_brand    = $rateDefault->results->cubic_car_brand;
                $rate->kg_ship_genaral    = $rateDefault->results->kg_ship_genaral;
                $rate->cubic_ship_genaral = $rateDefault->results->cubic_ship_genaral;
                $rate->kg_ship_iso        = $rateDefault->results->kg_ship_iso;
                $rate->cubic_ship_iso     = $rateDefault->results->cubic_ship_iso;
                $rate->kg_ship_brand      = $rateDefault->results->kg_ship_brand;
                $rate->cubic_ship_brand   = $rateDefault->results->cubic_ship_brand;
            }
            return $rate;
        });

        return $rate;
    }

    private function getDefualtMethod()
    {
        $key    = $this->getCacheKey(null, 'DEFAULT');
        $time   = $this->getTime(self::MINUTE_CACHE);

        $rate   = Cache::tags([$this->getClassPrefix()])->remember($key, $time, function () {
            return $this->rate::select(['*'])->whereId(1)->first();
        });

        return $rate;
    }

    private function findManyByIdSalesMethod($id, array $columns = ['*'])
    {
        $key            = $this->getCacheKey(null, $id);
        $time           = $this->getTime(self::MINUTE_CACHE);
        $rate           = Cache::tags([
                            $this->getClassPrefix(),
                            $this->getSaleIdPrefix($id),
                            $this->getSaleIdWithClassPrefix($id),
                            $this->getUserCodeWithClassPrefix($id),
                        ])->remember($key, $time, function () use ($id, $columns) {
                            return $this->rate::select($columns)->whereAdminId($id)->get();
                        });

        return $rate;
    }
    
    private function clearCacheTags()
    {
        Cache::tags([
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix(),
        ])->flush();
    }

    private function clearCacheTagById($id)
    {
        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();
    }
}
