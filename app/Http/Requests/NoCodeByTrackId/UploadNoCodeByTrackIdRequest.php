<?php

namespace App\Http\Requests\NoCodeByTrackId;

use App\Http\Requests\FormRequest;
use App\Rules\Base64Image;

class UploadNoCodeByTrackIdRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return $this->user('admins')->can('backend.tracking.update');
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'file_path'   =>  ['required', new Base64Image],
    ];
  }

  /**
   * Get custom attributes for validator errors.
   *
   * @return array
   */
  public function attributes()
  {
    return [
      'file_path'    => __('form.tracking.file_path'),
    ];
  }
}
