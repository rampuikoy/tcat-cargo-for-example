<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Admin;
use App\Models\Seo;
use Tymon\JWTAuth\Facades\JWTAuth;

class SeoTest extends TestCase
{
    use WithFaker;
    protected $user;
    protected $settingSeo;
    protected $url = 'api/backend/seo';
    /**
     * A basic feature test example.
     *
     * @return void
     */
    protected function authenticate(string $guard = 'admins')
    {
        $this->user = factory(Admin::class)->create();
        auth()->shouldUse($guard);
        $this->settingSeo = factory(Seo::class)->create();
        $token = JWTAuth::fromUser($this->user);
        return 'Bearer ' . $token;
    }

    protected function deleteUser()
    {
        $this->settingSeo->forceDelete();
        $this->user->forceDelete();
    }

    /** @test */
    public function GetSeoSuccessTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('GET', "{$this->url}");
        $response->assertStatus(200);
        $this->deleteUser();
    }

     /** @test */
    public function GetSeohFailTest()
    {
        $response = $this->json('GET', "{$this->url}");
        $response->assertStatus(401);
    }

    /** @test */
    public function SeoPostSuccessTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('POST', $this->url, [
            'title' => 'test-test',
            'keyword' => 'test-test',
            'description' => 'test-test',
            'code' => 'test-test',
            'footer' => 'test-test',
            'point_rate' => '0.5',
        ]);
        $response->assertStatus(200);
        Seo::where('title', 'test-test')->forceDelete();
        $this->deleteUser();
    }

     /** @test */
    public function SeoPostFailAuthTest()
    {
        $response = $this->json('POST', $this->url);
        $response->assertStatus(401);
    }

    /** @test */
    public function SeoPostFailTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('POST', $this->url);
        $response->assertStatus(422);
        $this->deleteUser();
    }
}
