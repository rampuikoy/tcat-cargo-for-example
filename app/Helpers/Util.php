<?php

namespace App\Helpers;

use App\Models\User;
use Carbon\Carbon;

class Util
{
    // concat remark or other field
    public static function concatField($field, $value)
    {
        if (empty($field) || !$field) {
            return $value;
        } else {
            if (empty($value)) {
                return $field;
            } else {
                return $field . ' / ' . $value;
            }
        }
    }

    public static function queryFilterBranch(&$query, $field = 'thai_shipping_method_id')
    {
        $admin = auth()->user();
        if ($admin->branch == 'branch') {
            $query->where($field, '=', $admin->droppoint_id);
        }
    }

    public static function excelToDate($date, $format = 'Y-m-d')
    {
        if (!$date) {
            return null;
        }

        $tiemStamp = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToTimestamp($date);
        return Carbon::createFromTimestamp($tiemStamp)->format($format);
    }

    public static function customExcelFailures($failures)
    {
        $failures = collect($failures)->map(function ($failure) {
            return [
                'rows' => $failure->row(), // row that went wrong
                'attribute' => $failure->attribute(), // either heading key (if using heading row concern) or column index
                'errors' => $failure->errors(), // Actual error messages from Laravel validator
                'values' => $failure->values(), // The values of the row that has failed.
            ];
        });

        $mapKey = $failures->mapWithKeys(function ($item) {
            return ['rows_' . $item['rows'] => $item];
        });

        $errors = $mapKey->map(function ($item) use ($failures) {
            return $failures->where('rows', $item['rows'])
                ->pluck('errors')
                ->reduce(function ($old = [], $new) {
                    return collect($old)->concat($new);
                });
        })->sort();

        return $errors;
    }

    public static function trimUpperCase($string)
    {
        return ($string) ? strtoupper(str_replace(' ', '', $string)) : null;
    }

    public static function getOrSet($new, $old)
    {
        return (empty($new)) ? $old : $new;
    }

    public static function getOrSetNumeric($new, $old, $decimal = 2)
    {
        if (empty($new) && empty($old)) {
            return 0;
        } elseif (empty($new) && is_numeric($new)) {
            return $old;
        }
        return round($new, $decimal);
    }

    public static function getOrSetDefaultEnum($collection, $value)
    {
        $enum = empty($value)
        ? collect($collection)->first()
        : $value;
        return collect($collection)->search($enum);
    }

    public static function setIfExist($exist, $value)
    {
        return ($exist) ? $value : null;
    }

    public static function generateFilename($name, $ext)
    {
        return $name . '_' . date('YmdHis') . '.' . $ext;
    }
}
