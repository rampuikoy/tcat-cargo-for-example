<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class ShippingMethodPolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.oversea-shipping-method.index');
    }
    public function show(Admin $admin)
    {
        return $admin->hasAnyPermission(['backend.oversea-shipping-method.show', 'backend.oversea-shipping-method.update']);
    }

    public function store(Admin $admin)
    {
        return $admin->can('backend.oversea-shipping-method.store');
    }
    public function update(Admin $admin)
    {
        return $admin->can('backend.oversea-shipping-method.update');
    }

    public function destroy(Admin $admin)
    {
        return $admin->can('backend.oversea-shipping-method.destroy');
    }

}
