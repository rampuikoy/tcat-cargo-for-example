<?php

namespace App\Http\Requests\Topup;

use App\Http\Requests\FormRequest;
use App\Rules\ExistsBankCompany;

class UpdateTopupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.topup.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'method'                    => 'required',
            'system_bank_account_id'    => ['required_if:method,transfer', 'nullable', new ExistsBankCompany],
            'date'                      => 'required|date_format:Y-m-d',
            'time'                      => 'required|date_format:H:i',
            'admin_remark'              => 'required_if:method,credit',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'method'                  => __('form.topup.method'),
            'system_bank_account_id'  => __('form.topup.system_bank_account_id'),
            'admin_remark'            => __('form.topup.admin_remark'),
            'time'                    => __('form.topup.time'),
            'date'                    => __('form.topup.date'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
