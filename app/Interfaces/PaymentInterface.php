<?php

namespace App\Interfaces;

interface PaymentInterface
{
    const DEFAULT_LIMIT = 25;
    const DEFAULT_SORTING = 'id';
    const DEFAULT_ASCENDING = 'desc';

    /**
     * fetch all payment
     *
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function all(array $columns = ['*'], $sorting = PaymentInterface::DEFAULT_SORTING, $ascending = PaymentInterface::DEFAULT_ASCENDING);

    /**
     * list payment
     *
     * @param array $conditions
     * @param array $columns
     * @param int $offset
     * @param int $limit
     *
     * @return \Illuminate\Support\Collection
     */
    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * pagination payment
     *
     * @param array $conditions
     * @param array $columns
     * @param int $page
     * @param int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $perPage = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * find payment by id
     *
     * @param $id
     * @param array $columns
     *
     * @return object|null
     */
    public function findById($id, array $columns = ['*']);

    /**
     * get payment dropdown list
     *
     * @return dropdown
     */
    public function dropdownList();

    /**
     * create new payment
     *
     * @param array $data
     *
     * @return object|null
     */
    public function store(array $data);


    /**
     * update payment by id
     *
     * @param $id
     * @param array $data
     *
     * @return object
     */
    public function updateById($id, array $data);

        /**
     * approve payment by id
     *
     * @param $id
     * @param array $data
     *
     * @return object
     */
    public function approve($id, array $data,$type);

	/**
     * soft delete payment by id
     *
     * @param $id
     *
     * @return object|null
     */
    public function softDeleteById($id);

        /**
     * find payment by id
     *
     * @param $id
     * @param array $columns
     *
     * @return object|null
     */
    public function findPaymentList($id, $type,array $columns = ['*']);
}
