<?php

namespace App\Repositories;

use App\Interfaces\BankCompanyInterface;
use App\Models\BankAccount;
use Illuminate\Support\Facades\Cache;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;

class BankCompanyRepository implements BankCompanyInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const MINUTE_CACHE  = 1;

    public function __construct(BankAccount $bankAccount)
    {
        $this->bankAccount = $bankAccount;
    }

    public function all(array $columns = ['*'], $sorting = BankCompanyInterface::DEFAULT_SORTING, $ascending = BankCompanyInterface::DEFAULT_ASCENDING)
    {
        $key       = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time      = $this->getTime(self::MINUTE_CACHE);

        $banks     = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
            return  $this->bankAccount::select($columns)
                ->whereNull('user_code')
                ->orderBy($sorting, $ascending)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.bank_company')]), $banks);
    }

    public function fetchListByFields(array $conditions, array $columns = ['*'], $offset = 0, $limit = BankCompanyInterface::DEFAULT_LIMIT, $sorting = BankCompanyInterface::DEFAULT_SORTING, $ascending = BankCompanyInterface::DEFAULT_ASCENDING)
    {
        $key      = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time     = $this->getTime(self::MINUTE_CACHE);

        $banks    = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
            return $this->bankAccount::select($columns)
                ->search($conditions)
                ->whereNull('user_code')
                ->orderBy($sorting, $ascending)
                ->skip($offset)
                ->limit($limit)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.bank_company')]), $banks);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = BankCompanyInterface::DEFAULT_LIMIT, $sorting = BankCompanyInterface::DEFAULT_SORTING, $ascending = BankCompanyInterface::DEFAULT_ASCENDING)
    {
        $key      = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time     = $this->getTime(self::MINUTE_CACHE);

        $banks    = Cache::tags([

            $this->getClassPrefix(),

            $this->getPaginationWithClassPrefix(),

        ])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
            $model = $this->bankAccount::select($columns)
                ->search($conditions)
                ->whereNull('user_code')
                ->orderBy($sorting, $ascending);

            if ($page == 1) {
                return $model->paginate($limit);
            } else {
                return $model->skip($limit * ($page - 1))->paginate($limit);
            }
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.bank_company')]), $banks);
    }

    public function findById($id, array $columns = ['*'])
    {
        $bank    = $this->findByIdMethod($id, $columns);
        if (!$bank) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.bank_company'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.bank_company')]), $bank);
    }

    public function dropdownList()
    {
        $key        = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time       = $this->getTime(self::MINUTE_CACHE);


        $dropdown   = Cache::remember($key, $time, function () {
            return [
                'status'        => $this->bankAccount::statusList(),
                'bank'          =>  $this->bankAccount::bankList(),
            ];
        });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function store(array $data)
    {
        $bank       = $this->bankAccount::create($data);

        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.bank_company')]), $bank);
    }

    public function updateById($id, array $data)
    {
        $bank    = $this->findByIdMethod($id);

        if (!$bank) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.bank_company'), 'id' => $id]));
        }

        $bank->update($data);
        Cache::tags([
            $this->getIdWithClassPrefix($id),
        ])->flush();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.bank_company')]), $bank);
    }

    public function softDeleteById($id)
    {
        $bank    = $this->findByIdMethod($id);

        if (!$bank) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.bank_company'), 'id' => $id]));
        }

        $bank->delete();
        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.delete_data', ['data' => __('messages.bank_company')]), $bank);
    }

    private function clearCacheTags()
    {
        Cache::tags([
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix()
        ])->flush();
    }

    private function findByIdMethod($id, array $columns = ['*'])
    {
        $key        = $this->getCacheKey(null, $id);
        $time       = $this->getTime(self::MINUTE_CACHE);

        $bankAccount    = Cache::tags([
            $this->getClassPrefix(),
            $this->getIdWithClassPrefix($id)
        ])->remember($key, $time, function () use ($id, $columns) {
              return $this->bankAccount::select($columns)
                ->whereNull('user_code')
                ->whereId($id)
                ->first();
        });

        return $bankAccount;
    }
}
