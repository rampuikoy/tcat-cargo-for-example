<?php

namespace Database\Seeders;

// use App\Models\TrackingDetail;

use App\Helpers\MoveSqlData;
use Illuminate\Database\Seeder;

class TrackingDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $track_detail    = collect(MoveSqlData::insertTrackingDetail());
        $message         = $track_detail->get('message');
        $error           = $track_detail->get('error');

        if ($error) {
            $this->command->error($message);
            $this->command->warning('Please insert fresh again');
            return;
        } else {
            $this->command->info($message);
        }
    }
}
