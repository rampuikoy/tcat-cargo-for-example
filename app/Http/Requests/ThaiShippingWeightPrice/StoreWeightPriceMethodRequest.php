<?php

namespace App\Http\Requests\ThaiShippingWeightPrice;

use App\Http\Requests\FormRequest;

class StoreWeightPriceMethodRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.thai-shipping-weight-price.store');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'thai_shipping_method_id'   => 'required|exists:thai_shipping_methods,id',
            'title'                     => 'required',
            'min_weight'                => 'required|numeric|min:0',
            'max_weight'                => 'required|numeric|gt:min_weight',
            'price'                     => 'required|numeric|min:0',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'thai_shipping_method_id'   => __('form.thai_shipping_weight_price.thai_shipping_method_id'),
            'title'                     => __('form.thai_shipping_weight_price.title'),
            'min_weight'                => __('form.thai_shipping_weight_price.min_weight'),
            'max_weight'                => __('form.thai_shipping_weight_price.max_weight'),
            'price'                     => __('form.thai_shipping_weight_price.price'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
