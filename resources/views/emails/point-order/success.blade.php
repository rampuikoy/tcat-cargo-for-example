@component('mail::message')
# เรียนคุณ {{$point_order->user->name}}
แจ้งบิลแลกสินค้า <span style="color: #1bc5bd">สำเร็จ</span> <br>
รหัสลูกค้า: {{$point_order->user->code}}

@component('mail::table')
| ข้อมูลบิล      |               |
|:-------------|--------------:|
| เลขที่บิล      | {{ $point_order->code }} |
| จำนวนแต้ม    | {{number_format($point_order->total, 2)}} |
| สถานะบิล      | <span style="color: #1bc5bd">สำเร็จ</span> |
@endcomponent

@component('mail::table')
| สินค้า |||||
|:-------------|--------------|--------------:|--------------:|--------------:|
| # | รายการสินค้า | จำนวนแต้ม | จำนวน | จำนวนแต้มรวม |
@foreach ($point_order->products as $index => $product)
| {{ $index + 1 }} | {{$product->title}} | {{number_format($product->point, 2)}} | {{$product->pivot->amount}} | {{number_format($product->pivot->total, 2)}} |
@endforeach
@endcomponent

@component('mail::table')
| การจัดส่ง |               |
|:------------|--------------:|
| การขนส่ง    | {{ $point_order->shippingMethod->title }} |
| เลขพัสดุ    | {{ $point_order->tracking }} |
| ค่าขนส่ง    | <span style="color: red">{{ number_format($point_order->shipping_cost, 2) }}</span> |
| ชื่อผู้รับ    | {{ $point_order->title }} |
| โทร      	| {{ $point_order->tel }} |
| ที่อยู่จัดส่ง   | {{ $point_order->full_address }} |
@endcomponent
@endcomponent
