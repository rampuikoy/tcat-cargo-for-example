<?php

namespace App\Observers;

use App\Models\ThaiShippingMethod;
use App\Observers\BaseObserver;

class ThaiShippingMethodObserver extends BaseObserver
{
    /**
     * Handle the thai_shipping_method "creating" event.
     *
     * @param  \App\Models\ThaiShippingMethod  $thai_shipping_method
     * @return void
     */
    public function creating(ThaiShippingMethod $thai_shipping_method)
    {
        //
        $this->stampCreated($thai_shipping_method);
    }

    /**
     * Handle the thai_shipping_method "updating" event.
     *
   * @param  \App\Models\ThaiShippingMethod  $thai_shipping_method
     * @return void
     */
    public function updating(ThaiShippingMethod $thai_shipping_method)
    {
        //
        $thai_shipping_method->title_th = $thai_shipping_method->title;
        $this->stampUpdated($thai_shipping_method);
    }

}
