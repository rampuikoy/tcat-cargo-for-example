<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\FormRequest;
use App\Rules\Base64Image;
use Illuminate\Support\Facades\Auth;

class UpdateProfileRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $id = Auth::guard('admins')->user()->id;
        return [
            'name'      => 'required|max:191',
            'lastname'  => 'sometimes|required',
            'email'     => 'sometimes|email|unique:admins,email,'.$id,
            'photo'     => ['nullable', new Base64Image],
            'tel'       => 'sometimes|nullable|max:20',
            'remark'    => 'sometimes|nullable|string',
        ];
    }

   /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name'         => __('form.auth.name'),
            'lastname'     => __('form.auth.lastname'),
            'email'        => __('form.auth.email'),
            'photo'        => __('form.auth.photo'),
            'tel'          => __('form.auth.phone'),
            'remark'       => __('form.auth.remark'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
