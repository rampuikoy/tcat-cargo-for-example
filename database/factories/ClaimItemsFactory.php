<?php

namespace Database\Factories;

use App\Models\ClaimItems;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

class ClaimItemsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ClaimItems::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $sufGroup = [
            '1890172591',
            '1850101',
            '90172201',
            '59182581',
            '7852921',
        ];
        $sufType = ['lose', 'lost_some', 'broken'];

        return [
            'claim_id' => random_int(1, 10),
            'tracking' => Arr::random($sufGroup) . '-' . random_int(1, 100),
            'amount' => random_int(1, 100),
            'price' => random_int(100, 10000) / 100,
            'total' => random_int(100, 10000) / 100,
            'type' => Arr::random($sufType),
        ];
    }
}
