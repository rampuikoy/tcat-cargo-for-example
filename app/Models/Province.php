<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    //
    protected $fillable = [
        'code',
        'title_th',
        'title_en',
        'geo_id',
    ];

}
