<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Support\Facades\Hash;
use App\Models\Admin;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Foundation\Testing\WithFaker;

class AuthTest extends TestCase
{
    use WithFaker;

    protected $url = '/api/backend';
    protected $user;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    protected function authenticate(string $guard = 'admins')
    {
        $this->user = factory(Admin::class)->create();
        auth()->shouldUse($guard);
        $token = JWTAuth::fromUser($this->user);
        return $token;
    }

    protected function deleteUser()
    {
        $this->user->forceDelete();
    }

    /** @test */
    public function RegisterSuccessTest()
    {
        $userTest = 'TestAtestAtest';
        $response = $this->json('POST', "{$this->url}/register", [
            'name'          => $userTest,
            'lastname'      => $this->faker->lastName(),
            'email'         =>  $this->faker->email(),
            'password'      => 'a12345678',
            'password_confirmation' => 'a12345678',
            // 'role'          => 'SuperAdmin',
            'username'      =>  $this->faker->firstName(),
            // 'pattern_user'  => 'LTa6sd46',
        ]);
        $response->assertStatus(200);
        Admin::where('name', $userTest)->forceDelete();
    }

    /** @test */
    public function RegisterFailTest()
    {
        $response = $this->json('POST', "{$this->url}/register", ['']);
        $response->assertStatus(422);
    }

    /** @test */
    public function LoginSuccessTest()
    {
        $user = factory(Admin::class)->create();
        $response = $this->json('POST', "{$this->url}/login", [
            'email' => $user->email,
            'password' => '123456789'
        ]);
        $response->assertStatus(200);
    }

    /** @test */
    public function LoginFailTest()
    {
        $response = $this->json('POST', "{$this->url}/login", [
            'email' => 'testFail@mail.com',
            'password' => 'password',
        ]);
        $response->assertStatus(422);
    }

    /** @test */
    public function MeSuccessTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->json('GET', "{$this->url}/me");
        $response->assertStatus(200);
        $this->deleteUser();
    }

    /** @test */
    public function MeFailTest()
    {
        $response = $this->json('GET', "{$this->url}/me");
        $response->assertStatus(401);
    }

    /** @test */
    public function UpdateProfileSuccessTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->json('PUT', "{$this->url}/setting/profile", [
            "name" => "แอดมิน",
        ]);
        $response->assertStatus(200);
        $this->deleteUser();
    }

    /** @test */
    public function UpdateProfileFailTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->json('PUT', "{$this->url}/setting/profile");
        $response->assertStatus(422);
        $this->deleteUser();
    }

    /** @test */
    public function UpdateProfileAuthFailTest()
    {
        $response = $this->json('PUT', "{$this->url}/setting/profile");
        $response->assertStatus(401);
    }

    /** @test */
    public function UpdatePasswordSuccessTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->json('PUT', "{$this->url}/setting/password", [
            "password_old" => "123456789",
            "password" => "mm112233",
            "password_confirmation" => "mm112233"
        ]);
        $response->assertStatus(200);
        $this->deleteUser();
    }

    /** @test */
    public function UpdatePasswordFailTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->json('PUT', "{$this->url}/setting/password");
        $response->assertStatus(422);
        $this->deleteUser();
    }

    /** @test */
    public function UpdatePasswordAuthFailTest()
    {
        $response = $this->json('PUT', "{$this->url}/setting/password");
        $response->assertStatus(401);
    }

    /** @test */
    public function PasswordResendSuccessTest()
    {
        $token = $this->authenticate();
        $response = $this->json('POST', "{$this->url}/password/email", [
            "email" => $this->user->email
        ]);
        $response->assertStatus(200);
        $this->deleteUser();
    }

    /** @test */
    public function PasswordResendFailTest()
    {
        $token = $this->authenticate();
        $response = $this->json('POST', "{$this->url}/password/email");
        $response->assertStatus(422);
        $this->deleteUser();
    }

    // /** @test */
    // public function PasswordResetSuccessTest()
    // {
    //     $token = $this->authenticate();
    //     $response = $this->withHeaders([
    //         'Authorization' => 'Bearer ' . $token,
    //     ])->json('POST', "{$this->url}/password/reset", [
    //         "email" => $this->user->email
    //     ]);
    //     $response->assertStatus(200);
    //     $this->deleteUser();
    // }

    // /** @test */
    // public function PasswordResetFailTest()
    // {
    //     $token = $this->authenticate();
    //     $response = $this->withHeaders([
    //         'Authorization' => 'Bearer ' . $token,
    //     ])->json('POST', "{$this->url}/password/reset");
    //     $response->assertStatus(422);
    //     $this->deleteUser();
    // }

    // /** @test */
    // public function PasswordResetAuthFailTest()
    // {
    //     $response = $this->json('POST', "{$this->url}/password/reset");
    //     $response->assertStatus(401);
    // }

    //TODO no get request
    public function VerificationResendSuccessTest()
    {
        $token = $this->authenticate();
        $response = $this->json('POST', "{$this->url}/email/resend", [
            "email" => $this->user->email
        ]);
        $response->assertStatus(200);
        $this->deleteUser();
    }

    /** @test */
    public function VerificationResendFailTest()
    {
        $token = $this->authenticate();
        $response = $this->json('POST', "{$this->url}/email/resend");
        $response->assertStatus(422);
        $this->deleteUser();
    }

    /** @test */
    public function LogOutSuccessTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', "{$this->url}/logout");
        $response->assertStatus(200);
        $this->deleteUser();
    }

    /** @test */
    public function LogOutFailTest()
    {
        $token = '';
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $token,
        ])->json('POST', "{$this->url}/logout");
        $response->assertStatus(401);
    }
}
