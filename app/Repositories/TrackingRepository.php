<?php

namespace App\Repositories;

use App\Interfaces\TrackingInterface;
use App\Models\Tracking;
use App\Models\TrackingTmall;
use App\Models\TrackingTaoBao;
use App\Models\TrackingTaoBaoDetail;
use App\Models\TrackingTaoBaoHasBill;
use App\Models\TrackingTaoBaoRemark;
use App\Models\TrackingDetail;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use Illuminate\Support\Facades\Cache;
use App\Helpers\Util;
use Illuminate\Support\Facades\DB;

class TrackingRepository implements TrackingInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const MINUTE_CACHE              = 0;
    const TABLE_RELETIONS           = ['warehouse', 'productTypes', 'shippingMethods', 'images'];
    const TABLE_RELETIONS_BY_ID     = ['productTypes', 'shippingMethods', 'trackingDetail', 'images'];
    const TABLE_RELETIONS_USER_CODE = ['warehouse', 'productTypes', 'shippingMethods'];

    public function __construct(Tracking $tracking, TrackingDetail $trackingDtial, TrackingTmall $trackingTmall, TrackingTaoBao $trackingTaoBao, TrackingTaoBaoDetail $trackingTaoBaoDetail, TrackingTaoBaoHasBill $trackingTaoBaoHasBill, TrackingTaoBaoRemark $trackingTaoBaoRemark)
    {
        $this->tracking = $tracking;
        $this->trackingDtial = $trackingDtial;
        $this->trackingTmall = $trackingTmall;
        $this->trackingTaoBao = $trackingTaoBao;
        $this->trackingTaoBaoDetail = $trackingTaoBaoDetail;
        $this->trackingTaoBaoHasBill = $trackingTaoBaoHasBill;
        $this->trackingTaoBaoRemark = $trackingTaoBaoRemark;
    }

    public function all(array $columns = ['*'], $sorting = TrackingInterface::DEFAULT_SORTING, $ascending = TrackingInterface::DEFAULT_ASCENDING)
    {
        $key = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time = $this->getTime(self::MINUTE_CACHE);

        $trackings = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
            return $this->tracking::select($columns)->orderBy($sorting, $ascending)->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.trackings.tracking')]), $trackings);
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = TrackingInterface::DEFAULT_LIMIT, $sorting = TrackingInterface::DEFAULT_SORTING, $ascending = TrackingInterface::DEFAULT_ASCENDING)
    {
        $key = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time = $this->getTime(self::MINUTE_CACHE);

        $trackings = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
            return $this->tracking::select($columns)
                ->search($conditions)
                ->orderBy($sorting, $ascending)
                ->skip($offset)
                ->limit($limit)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.trackings.tracking')]), $trackings);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = TrackingInterface::DEFAULT_LIMIT, $sorting = TrackingInterface::DEFAULT_SORTING, $ascending = TrackingInterface::DEFAULT_ASCENDING)
    {
        $key = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time = $this->getTime(self::MINUTE_CACHE);

        $tracking = Cache::tags([

            $this->getClassPrefix(),

            $this->getPaginationWithClassPrefix(),

        ])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
            $model = $this->tracking::select($columns)
                ->with(self::TABLE_RELETIONS)
                ->searchExtend($conditions)
                ->search($conditions)
                ->orderBy($sorting, $ascending);

            if ($page == 1) {
                return $model->paginate($limit);
            } else {
                return $model->skip($limit * ($page - 1))->paginate($limit);
            }
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.trackings.tracking')]), $tracking);
    }

    public function findByUserCode($userCode, array $columns = ['*'])
    {
        $tracking = $this->findByUserCodeMethod($userCode, $columns);

        if ($tracking->isEmpty()) {
            return $this->coreResponse(404, __('messages.no_data_with_user_code', ['data' => __('messages.trackings.tracking'), 'user_code' => $userCode]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.trackings.tracking')]), $tracking);
    }

    public function findByCode($tracking_code, array $columns = ['*'])
    {
        $key = $this->getCacheKey(null, $tracking_code);
        $time = $this->getTime(self::MINUTE_CACHE);

        $tracking = Cache::tags([$this->getClassPrefix(), $this->getTrackingInternalPrefix(), $this->getTrackingCodePrefix($tracking_code)])->remember($key, $time, function () use ($tracking_code, $columns) {
            return $this->tracking::select($columns)
                ->with(self::TABLE_RELETIONS)
                ->Where('code', $tracking_code)
                ->first();
        });

        if (!$tracking) {
            return $this->coreResponse(404, __('messages.no_data_with_code', ['data' => __('messages.trackings.tracking'), 'code' => $tracking_code]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.trackings.tracking')]), $tracking);
    }

    public function findExternalByCode($tracking_code, array $columns = ['*'])
    {
        $key = $this->getCacheKey(null, $tracking_code);
        $time = $this->getTime(self::MINUTE_CACHE);

        $tracking = Cache::tags([
            $this->getClassPrefix(),
            $this->getTrackingExternalPrefix(),
            $this->getTrackingCodePrefix($tracking_code),
        ])->remember($key, $time, function () use ($tracking_code, $columns) {
            $trackingTmall = $this->trackingTmall::select($columns)
                ->search($tracking_code)
                ->first();

            $trackingTaoBao = $this->trackingTaoBao::select($columns)
                ->search($tracking_code)
                ->first();

            $trackingTaoBaoDetail = $this->trackingTaoBaoDetail::select($columns)
                ->search($tracking_code)
                ->first();

            $trackingTaoBaoHasBill = $this->trackingTaoBaoHasBill::select($columns)
                ->search($tracking_code)
                ->first();

            $trackingTaoBaoRemark = $this->trackingTaoBaoRemark::select($columns)
                ->search($tracking_code)
                ->first();

            return [
                'taobao' => $trackingTaoBao,
                'taobaoDetail' => $trackingTaoBaoDetail,
                'taobaoBill' =>  $trackingTaoBaoHasBill,
                'taobaoRemark' =>  $trackingTaoBaoRemark,
                'tmall' => $trackingTmall

            ];
        });

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.trackings.tracking')]), $tracking);
    }

    public function searchManyByCode($tracking_code, array $columns = ['*'])
    {
        $key = $this->getCacheKey(null, $tracking_code);
        $time = $this->getTime(self::MINUTE_CACHE);

        $tracking_code = Cache::tags([
            $this->getClassPrefix(),
            $this->getSearchWithClassPrefix(),
            $this->getTrackingCodePrefix($tracking_code)
        ])->remember($key, $time, function () use ($tracking_code, $columns) {
            return $this->tracking::select($columns)->with(self::TABLE_RELETIONS)->where('code', 'LIKE', "%{$tracking_code}%")->get();
        });

        if (!$tracking_code) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.trackings.tracking'), 'id' => $tracking_code]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.trackings.tracking')]), $tracking_code);
    }

    public function findById($id, array $columns = ['*'])
    {

        $tracking =  $this->findByIdMethod($id, $columns = ['*']);
        if (!$tracking) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.trackings.tracking'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.trackings.tracking')]), $tracking);
    }

    public function dropdownList()
    {
        $key        = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $dropdown   = Cache::remember($key, $time, function () {
                        return [
                            'status'            => $this->tracking::statusList(),
                            'type'              => $this->tracking::typeList(),
                            'calculate_type'    => $this->tracking::calculateTypeList(),
                            'date_type'         => $this->tracking::dateTypeList(),
                            'condition'         => $this->tracking::conditionList(),
                            'tracking_detail'   => $this->trackingDtial::productTypeList(),
                        ];
                    });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function store(array $data)
    {
        $count_code = $this->tracking::whereCode($data['code'])->count();

        if ($count_code > 0) {
            return $this->coreResponse(404, __('messages.data_with_code', ['data' => $data['code']]));
        }

        if ($data['warehouse_id'] == 1 || $data['warehouse_id'] == 14 || $data['warehouse_id'] == 15) {
            $data['status'] = 1;
        } else {
            $data['status'] = 2;
        }

        if (!empty($data['warehouse_zone'])) {
            $data['status'] = 3;
        }

        if (empty($data['thai_id']) && $data['status'] > 1) {
            $data['thai_in'] = date('Y-m-d');
        }

        $tracking = $this->tracking::create($data);
        $this->clearCacheTags();
        app('App\Repositories\UserRepository')->clearCacheTagByUserCode($tracking->user_code);
        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.trackings.tracking')]), $tracking);
    }

    public function updateById($id, array $data)
    {

        $tracking = $this->findByIdMethod($id);

        if (!$tracking) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.trackings.tracking'), 'id' => $id]));
        }

        if ($tracking->status > 4) {
            $tracking->warehouse_id = $data['warehouse_id'];
            $tracking->warehouse_zone = !empty($data['warehouse_zone']) ? $data['warehouse_zone'] : '';
            $tracking->admin_remark = !empty($data['admin_remark']) ? $data['admin_remark'] : '';
            $tracking->thai_out = !empty($data['thai_out']) ? $data['thai_out'] : '';
        }

        if (!empty($data['thai_in']) && $tracking->status < 2) {
            $data['warehouse_id'] = 2;
        }

        if (!empty($data['warehouse_zone']) && $tracking->status < 3) {
            $data['warehouse_id'] = 2;
            $tracking->status = 3;
            $data['thai_in'] = date('Y-m-d');
        }

        if (!empty($data['warehouse_id'])) {
            if ($data['warehouse_id'] == 1 || $data['warehouse_id'] == 14 || $data['warehouse_id'] == 15) {
                $tracking->status = 1;
            } elseif ($tracking->status == 1) {
                $tracking->status = 2;
                $data['thai_in'] = date('Y-m-d');
            }
        }

        $tracking->update($data);
        $this->clearCacheTags();
        $this->clearCacheTagById($id);
        app('App\Repositories\UserRepository')->clearCacheTagByUserCode($tracking->user_code);
        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.trackings.tracking')]), $tracking);
    }

    public function updateTrackingBill($id, array $data)
    {

        $tracking = $this->findByIdMethod($id);
        $tracking->update($data);
        $this->clearCacheTags();
        $this->clearCacheTagById($id);
        app('App\Repositories\UserRepository')->clearCacheTagByUserCode($tracking->user_code);
        return $tracking;
    }

    public function trackingDetail($id, array $data)
    {
        $tracking = $this->trackingDtial->updateOrCreate(['tracking_id' => $id], $data);

        $this->clearCacheTags();
        $this->clearCacheTagById($id);
        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.trackings.tracking')]), $tracking);
    }

    public function softDeleteById($id)
    {
        $tracking = $this->findByIdMethod($id);

        if (!$tracking) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.trackings.tracking'), 'id' => $id]));
        }

        if ($tracking->status < 4) {
            $tracking->code = $tracking->code . "-delete";
            $tracking->deleted_at = date('Y-m-d H:i:s');
            $tracking->update();
            $this->clearCacheTags();
            $this->clearCacheTagById($id);
            return $this->coreResponse(200, __('messages.delete_data', ['data' => __('messages.trackings.tracking')]), $tracking);
        } else {
            return $this->coreResponse(500, __('messages.not_delete_data', ['data' => __('messages.trackings.tracking')]));
        }
    }

    public function deleteMany(array $ids)
    {
        DB::beginTransaction();
        try {
            $results = [];
            foreach ($ids as $id) {
                $tracking = $this->findByIdMethod($id);

                if ($tracking) {
                    if ($tracking->status < 4) {
                        $tracking->code = $tracking->code . "-delete";
                        $tracking->deleted_at = date('Y-m-d H:i:s');
                        $tracking->update();
                        array_push($results, $tracking);
                        $this->clearCacheTagById($id);
                    } else {
                        throw new \UnexpectedValueException("Not delete data");
                    }
                }
            }
            $this->clearCacheTags();
            DB::commit();
            return $this->coreResponse(200, __('messages.delete_data', ['data' => __('messages.trackings.tracking')]), $results);
        } catch (\Throwable $t) {
            DB::rollBack();
            return $this->coreResponse(500, __('messages.not_delete_data', ['data' => __('messages.trackings.tracking')]));
        }
    }

    public function putZoneIn(array $data)
    {
        $tracking = $this->tracking::select(['*'])->with(self::TABLE_RELETIONS_BY_ID)->whereCode($data['code'])->first();
        $messages = 'messages.tracking';
        $action = null;
        if ($tracking) {
            if ($tracking->status > 5) {
                $response = [
                    'data'      => $tracking,
                    'action'    => $action,
                ];
                $results = $this->coreResponse(200, __('messages.update_data', ['data' => __($messages)]), $response);
            } else {
                if ($tracking->status < 3) {
                    $tracking->status  = !empty($data['confirm']) ? ($data['confirm'] ?  3 :  2) : 2;
                }

                if (!empty($data['reference_code'])) {
                    if ($tracking->reference_code && $tracking->reference_code != $data['reference_code']) {
                        $data['reference_code'] = Util::concatField($tracking->reference_code, $data['reference_code']);
                    }

                    if (!empty($data['set_user_code'])) {
                        if ($data['set_user_code']) {
                            if (!$tracking->user_code) {
                                $data['user_code'] = $data['reference_code'];
                            } else {
                                $data['user_code'] = $tracking->user_code;
                                $action = 'confirm';
                                $messages = 'admin.confirm_update_user_code';
                            }
                        }
                    }
                }
                $tracking->thai_in = date('Y-m-d');
                $data['warehouse_zone'] = Util::concatField($tracking->warehouse_zone, $data['warehouse_zone']);
                $tracking->update($data);
                $response = [
                    'data'      => $tracking,
                    'action'    => $action,
                ];
                $results = $this->coreResponse(200, __('messages.update_data', ['data' => __($messages)]),  $response);
            }
        }
        if (!$tracking) {
            $trackings = $this->tracking::select(['id', 'code', 'code_group', 'user_code', 'status'])->where('code', 'LIKE', "%{$data['code']}%")->where('status', '<', 4)->get();
            if ($trackings->count() > 0) {
                $response =  [
                    'action' => 'modal',
                    'data' => $trackings
                ];
                $results = $this->coreResponse(200, __('messages.update_data', ['data' => __($messages)]), $response);
            } else {
                $response =  [
                    'action' => 'new'
                ];
                $results = $this->coreResponse(200, __('messages.update_data', ['data' => __($messages)]),  $response);
            }
        }
        return $results;
    }

    public function putZoneInModal(array $data)
    {
        $results = [];
        $reference_code = !empty($data['reference_code']) ? $data['reference_code'] : null;
        $warehouse_zone = !empty($data['warehouse_zone']) ? $data['warehouse_zone'] : null;
        try {
            foreach ($data['ids'] as $id) {

                $tracking = $this->findByIdMethod($id);
                if ($tracking) {
                    if ($tracking->status < 4) {
                        if ($tracking->status < 3) {
                            $tracking->status  = !empty($data['confirm']) ? ($data['confirm'] ?  3 :  2) : 2;
                        }

                        if (!empty($data['reference_code'])) {
                            if ($tracking->reference_code && $tracking->reference_code != $data['reference_code']) {
                                $data['reference_code'] = Util::concatField($tracking->reference_code, $reference_code);
                            }
                            if (!empty($data['set_user_code'])) {
                                if ($data['set_user_code']) {
                                    if (!$tracking->user_code) {
                                        $data['user_code'] = $data['reference_code'];
                                    } else {
                                        $data['user_code'] = $tracking->user_code;
                                    }
                                }
                            }
                        }
                        $tracking->thai_in = date('Y-m-d');
                        $data['warehouse_zone'] = Util::concatField($tracking->warehouse_zone, $warehouse_zone);
                        $tracking->update($data);
                        $this->clearCacheTagById($id);
                        array_push($results, $tracking);
                    }
                }
            }
            return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.trackings.tracking')]), $results);
        } catch (\Throwable $t) {
            return $this->coreResponse(500, __('messages.not_update_data', ['data' => __('messages.trackings.tracking')]));
        }
    }

    public function postZoneIn(array $data)
    {
        $today = date('Y-m-d');
        $array_merge = [
            "type" => null,
            "detail" => null,
            "images" => []
        ];
        try {
            $tracking = $this->tracking::with(self::TABLE_RELETIONS_BY_ID)->firstOrNew(['code' => $data['code']]);
            if ($tracking->id) {
                $tracking->reference_code     = !empty($data['reference_code']) ?  $data['reference_code'] : null;
                $tracking->shipping_method_id = !empty($data['shipping_method_id']) ?  $data['shipping_method_id'] : null;
                $tracking->product_type_id    = !empty($data['product_type_id']) ?  $data['product_type_id'] : null;
                $tracking->warehouse_id       = $data['warehouse_id'];
                $tracking->weight             = !empty($data['weight']) ? $data['weight'] : null;
                $tracking->status             = !empty($data['confirm']) ? ($data['confirm'] ?  3 :  2) : 2;
                $messages = 'messages.update_data';
            } else {
                if (!empty($data['reference_code'])) {
                    $tracking->user_code    = $data['reference_code'];
                }
                $tracking->reference_code   = !empty($data['reference_code']) ?  $data['reference_code'] : null;
                $tracking->warehouse_id     = $data['warehouse_id'];
                $tracking->warehouse_zone   = $data['warehouse_zone'];
                $tracking->status           = !empty($data['confirm']) ? ($data['confirm'] ?  3 :  2) : 2;

                $tracking->china_in = $today;
                $tracking->china_out = $today;
                $tracking->thai_in = $today;
                $tracking->system_remark = __('admin.created_at_zone_in');
                $messages = 'messages.create_data';
            }
            $tracking->save();

            Cache::tags([$this->getTrackingCodePrefix($data['code'])]);

            $messages === 'messages.update_data' ? $results = $tracking : $results = array_merge($tracking->toArray(), $array_merge);
            return $this->coreResponse(200, __($messages, ['data' => __('messages.trackings.tracking')]), $results);
        } catch (\Throwable $t) {
            return $this->coreResponse(500, __('messages.not_update_data', ['data' => __('messages.trackings.tracking')]));
        }
    }

    public function findByUserCodeExtend($userCode, array $columns = ['*'])
    {
        $trackings  = $this->findByUserCodeMethod($userCode);
        $filter     = collect($trackings)->whereIn('status', [1, 2, 3])->all();
        $data       = [];

        foreach ($filter as $item) {
            $data[] = $item;
        }

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.trackings.tracking')]), $data);
    }

    public function billCancelTrackingById(array $ids)
    {
        if (count($ids) === 0) {
            return null;
        }
        $results = [];
        foreach ($ids as $id) {
            $tracking                   = $this->findByIdMethod($id);
            $tracking->bill_id          = null;
            $tracking->status           = 3;
            $tracking->packed_admin     = null;
            $tracking->packed_admin_id  = null;
            $tracking->packed_at        = null;
            $tracking->update();
            $results[]                  = $tracking;
            
            $this->clearCacheTagById($id);
            app('App\Repositories\UserRepository')->clearCacheTagByUserCode($tracking->user_code);
        }
        $this->clearCacheTags();
        return $results;
    }

    public function billUpdateSignatureTrackingById(array $ids)
    {
        if (count($ids) === 0) {
            return null;
        }
        $results = [];
        foreach ($ids as $id) {
            $tracking               = $this->findByIdMethod($id);
            $tracking->status       = 6;
            $tracking->thai_out     = date('Y-m-d');
            $tracking->update();

            $results[]              = $tracking;
            $this->clearCacheTagById($id);
            app('App\Repositories\UserRepository')->clearCacheTagByUserCode($tracking->user_code);
        }
        $this->clearCacheTags();
        return $results;
    }

    public function findByIdMethod($id, array $columns = ['*'])
    {
        $key = $this->getCacheKey(null, $id);
        $time = $this->getTime(self::MINUTE_CACHE);

        $tracking = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($id)
        ])->remember($key, $time, function () use ($id, $columns) {
            return $this->tracking::select($columns)->with(self::TABLE_RELETIONS_BY_ID)->whereId($id)->first();
        });

        return $tracking;
    }

    private function findByUserCodeMethod($userCode, $columns = ['*'])
    {
        $key = $this->getCacheKey(null, $userCode);
        $time = $this->getTime(self::MINUTE_CACHE);

        $tracking = Cache::tags([
            $this->getClassPrefix(),
            $this->getUserCodePrefix($userCode),
        ])->remember($key, $time, function () use ($userCode, $columns) {

            return $this->tracking::select($columns)
                ->with(self::TABLE_RELETIONS_USER_CODE)
                ->whereUserCode($userCode)
                ->get();
        });

        return $tracking;
    }

    public function clearCacheTags()
    {
        Cache::tags([
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix()
        ])->flush();
    }

    public function clearCacheTagById($id)
    {
        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();
    }
}
