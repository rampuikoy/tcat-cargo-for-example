<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
     */

    'failed' => 'ข้อมูลรับรองเหล่านี้ไม่ตรงกับบันทึกของเรา',
    'throttle' => 'พยายามเข้าสู่ระบบมากเกินไป โปรดลองอีกครั้งใน :seconds วินาที',
    'login' => 'เข้าสู่ระบบแล้ว',
    'logout' => 'ออกจากระบบแล้ว',
    'registered' => 'สมัครสมาชิกสำเร็จ',
    'user_detail' => 'ข้อมูลผู้ใช้ระบบ',
    'update_profile' => "แก้ไขโปรไฟล์แล้ว",
    'update_password' => "แก้ไขรหัสผ่านแล้ว",
    'not_data' => 'ไม่พบข้อมูลผู้ใช้',
];
