<?php

namespace App\Models;

use App\Helpers\ScopeQueries;
use App\Models\Amphur;
use App\Models\ThaiShippingMethod;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    use HasFactory;

    const PREFIX = 'SH-';

    public static $imageFolder = 'uploads/bill/';
    public static $imageSubFolderPack = 'pack/';
    public static $imageSubFolderSing = 'sing/';
    public static $imageWidth = 300;
    public static $imageHeight = 300;

    protected $fillable = [
        'rate_id',
        'rate_title',
        'shipping_weight_counter',
        'shipping_cubic_counter',
        'shipping_weight_price',
        'shipping_cubic_price',
        'shipping_min_rate',
        'shipping_price',
        'shipping_rate',
        'counter',
        'thai_shipping_method_id',
        'thai_shipping_method_sub_id',
        'thai_shipping_method_sub_title',
        'thai_shipping_method_charge',
        'thai_shipping_method_price',
        'thai_shipping_method_note',
        'thai_shipping_raw_price',
        'thai_shipping_raw_charge',
        'thai_shipping_date',
        'thai_shipping_time',
        'thai_shipping_code',
        'thai_shipping_admin_id',
        'thai_shipping_admin',
        'thai_shipped_at',
        'thai_shipping_detail',

        'other_charge_title1',
        'other_charge_price1',
        'other_charge_title2',
        'other_charge_price2',
        'discount_title1',
        'discount_price1',
        'discount_title2',
        'discount_price2',
        'addon_refund_title',
        'addon_refund_credit',
        'coupon_id',
        'coupon_code',
        'coupon_price',
        'total_weight',
        'total_cubic',
        'sub_total',
        'withholding',
        'withholding_amount',
        'total_price',

        'shipping_name',
        'shipping_tel',
        'shipping_address',
        'shipping_district_code',
        'shipping_amphur_id',
        'shipping_province_id',
        'shipping_zipcode',

        'status',
        'user_remark',
        'system_remark',
        'admin_remark',
        'admin_packing_remark',
        'admin_thai_shipping_remark',
        'user_code',
        'user_address_id',
        'created_admin',
        'created_admin_id',
        'updated_admin',
        'updated_admin_id',

        'exchange_rate',
        'cost_box_china',
        'cost_shipping_china',
        'cost_box_thai',
        'cost_shipping_thai',

        'printed_thai_shipping',
        'printed_admin_id',
        'printed_at',
        'printed_admin',

        'droppoint_id',
        'droppoint_at',
        'droppoint_remark',
        'droppoint_admin',
        'droppoint_admin_id',
        'droppoint_noti',
        'sign_image',
        'truck_id',
        'driver_admin',
        'driver_admin_id',
        'driver2_admin',
        'driver2_admin_id',

        'mark',
        'marked_at',
        'marked_user_id',
        'marked_user',
        'commission_rate_id',
        'latitude',
        'longitude',
        'tcatexpress_address_api',

        'approved_at',
        'approved_admin',
        'approved_admin_id',
        'commission_admin_id',
        'succeed_at',
        'commission_id',
        'commission_amount',
        'paid_at',

        'peak_at',
        'peak_code',
        'peak_status',
        'peak_amount',
        'peak_remark',
    ];

    public static function statusList()
    {
        $array = [
            [
                'id' => 0,
                'name' => __('messages.bills.status.0'),
            ],
            [
                'id' => 1,
                'name' => __('messages.bills.status.1'),
            ],
            [
                'id' => 2,
                'name' => __('messages.bills.status.2'),
            ],
            [
                'id' => 3,
                'name' => __('messages.bills.status.3'),
            ],
            [
                'id' => 4,
                'name' => __('messages.bills.status.4'),
            ],
            [
                'id' => 5,
                'name' => __('messages.bills.status.5'),
            ],
            [
                'id' => 6,
                'name' => __('messages.bills.status.6'),
            ],
            [
                'id' => 7,
                'name' => __('messages.bills.status.7'),
            ],
            [
                'id' => 8,
                'name' => __('messages.bills.status.8'),
            ],
        ];
        return $array;
    }

    public static function droppointInList()
    {
        $array = [
            'waiting' => __('messages.bills.droppoint_in.waiting'),
            'success' => __('messages.bills.droppoint_in.success'),
        ];
        return $array;
    }

    public static function conditionList()
    {
        $array = [
            'other_charge' => __('messages.bills.other_charge'),
            'discount' => __('messages.bills.discount'),
            'coupon' => __('messages.bills.coupon'),
            'withholding' => __('messages.bills.withholding'),
            'not_print' => __('messages.bills.not_print'),
            'print_not_pack' => __('messages.bills.print_not_pack'),
            'user_remark' => __('messages.bills.user_remark'),
            'admin_remark' => __('messages.bills.admin_remark'),
            'not_cancel' => __('messages.bills.not_cancel'),
            'success_or_cancel' => __('messages.bills.success_or_cancel'),
            'not_printed_thai_shipping' => __('messages.bills.not_printed_thai_shipping'),
            'printed_thai_shipping' => __('messages.bills.printed_thai_shipping'),
            'sign' => __('messages.bills.sign'),
            'refund' => __('messages.bills.refund'),
            'mark' => __('messages.bills.mark'),
            'map' => __('messages.bills.map'),
            'truck' => __('messages.bills.truck'),
            'user_created' => __('messages.bills.user_created'),
            'user_created_min_rate' => __('messages.bills.user_created_min_rate'),
            'commission' => __('messages.bills.commission'),
            'commission_null' => __('messages.bills.commission_null'),
        ];
        return $array;
    }

    public static function template()
    {
        $array = [
            'default' => __('messages.bills.template_default'),
            'shipping' => __('messages.bills.template_shipping'),
        ];
        return $array;
    }

    public static function texeCancelList()
    {
        $array = [
            'เพิ่ม/แก้ไข แทรคค่ะ',
            'เปลี่ยนที่อยู่ค่ะ',
            'เปลี่ยนการขนส่งค่ะ',
            'ขนาด/น้ำหนัก เกินค่ะ',
            'เปลี่ยนโปรโมชั่นค่ะ',
            'รวมบิล/แยกบิลค่ะ',
            'เพิ่มคูปอง/ส่วนลดค่ะ',
            'ออกบิลผิดค่ะ',
            'ทดสอบระบบ',
        ];
        return $array;
    }

    public static function timeFormatList()
    {
        $array = [
            'created_at' => __('messages.bills.created_at'),
            'approved_at' => __('messages.bills.approved_at'),
            'paid_at' => __('messages.bills.paid_at'),
            'updated_at' => __('messages.bills.updated_at'),
            'droppoint_at' => __('messages.bills.droppoint_at'),
            'thai_shipping_date' => __('messages.bills.thai_shipping_date'),
            'succeed_at' => __('messages.bills.succeed_at'),
        ];
        return $array;
    }

    protected $appends = ['no', 'current_status', 'total', 'sign_url'];
    protected $casts = [
        'shipping_rate' => 'array',
    ];

    public function getSignUrlAttribute()
    {
        $urlFront =  env('URL_FRONTEND', null);
        $urlBillEdit = $urlFront . '/bill/' . $this->id;
        $urlSignature = $urlFront . '/bill/signature/' . $this->id;
        return in_array($this->status, [4, 5]) && !$this->sign_image ? $urlSignature : $urlBillEdit;
    }

    public function setShippingRateFirstNameAttribute($value)
    {
        $this->attributes['shipping_rate'] = json_decode($value);
    }

    public function getNoAttribute()
    {
        return ($this->id) ? self::PREFIX . str_pad($this->id, 6, "0", STR_PAD_LEFT) : null;
    }

    public function getCurrentStatusAttribute()
    {
        $status = collect(self::statusList())->firstWhere('id', $this->status);
        return (!empty($status['name'])) ? $status['name'] : null;
    }

    public function getTotalAttribute()
    {
        return $this->total_price ?? null;
    }

    public function getTruckIdAttribute($value)
    {
        return ($value) ? (string) $value : null;
    }

    public function getDriverAdminIdAttribute($value)
    {
        return ($value) ? (string) $value : null;
    }

    public function getDriver2AdminIdAttribute($value)
    {
        return ($value) ? (string) $value : null;
    }

    // public function getTcatExpressTypeAttribute()
    // {
    //    if($this->id){
    //         return '1';
    //         $type = [ 1=>'ทั่วไป', 'ส่งด่วน 12 ชม.', 'เหมารถ'];
    //         return ($this->thai_shipping_method_id==25)? $type[$this->thai_shipping_method_sub_id] : null ;
    //     }
    // }

    /** mutator */
    public function setShippingWeightPriceAttribute($value)
    {
        $this->attributes['shipping_weight_price'] = round($value, 2);
    }
    public function setShippingCubicPriceAttribute($value)
    {
        $this->attributes['shipping_cubic_price'] = round($value, 2);
    }
    public function setShippingPriceAttribute($value)
    {
        $this->attributes['shipping_price'] = round($value, 2);
    }
    public function setThaiShippingMethodPriceAttribute($value)
    {
        $this->attributes['thai_shipping_method_price'] = round($value, 2);
    }
    public function setTotalPriceAttribute($value)
    {
        $this->attributes['total_price'] = round($value, 2);
    }
    public function setSubTotalAttribute($value)
    {
        $this->attributes['sub_total'] = round($value, 2);
    }

    /**
     * relation
     */
    public function trackings()
    {
        return $this->hasMany('App\Models\Tracking', 'bill_id');
    }

    public function pivotTrackings()
    {
        return $this->belongsToMany('App\Models\Tracking', 'bill_trackings', 'bill_id', 'tracking_id');
    }

    public function commissionRate()
    { // rate for calculate commission
        return $this->hasOne('App\Models\CommissionRate', 'bill_id');
    }

    public function packingQueue()
    { // rate for calculate commission
        return $this->hasOne('App\Models\PackingQueue', 'bill_id');
    }

    public function thaiShippingMethod()
    {
        return $this->belongsTo('App\Models\ThaiShippingMethod', 'thai_shipping_method_id');
    }

    public function province ()
    {
        return $this->belongsTo('App\Models\ThaiShippingAreaPrice', 'thai_shipping_method_id', 'id');
    }

    public function coupon()
    {
        return $this->belongsTo('App\Models\Coupon', 'coupon_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_code', 'code');
    }

    public function droppoint()
    {
        return $this->belongsTo('App\Models\ThaiShippingMethod', 'droppoint_id', 'id');
    }

    public function address()
    {
        return $this->belongsTo('App\Models\UserAddress', 'user_address_id');
    }

    public function credits()
    {
        return $this->morphMany('App\Models\Credit', 'creditable');
    }

    public function emails()
    {
        return $this->morphMany('App\Models\EmailQueue', 'mailable');
    }

    public function fcm()
    {
        return $this->morphMany('App\Models\FcmQueue', 'fcmable');
    }

    public function sms()
    {
        return $this->morphMany('App\Models\SmsQueue', 'smsable');
    }

    public function shippingProvince()
    {
        return $this->belongsTo('App\Models\Province', 'shipping_province_id', 'id');
    }

    public function shippingAmphur()
    {
        return $this->belongsTo('App\Models\Amphur', 'shipping_amphur_id', 'id');
    }

    public function shippingDistrict()
    {
        return $this->belongsTo('App\Models\District', 'shipping_district_code', 'code');
    }
    // public function autopay()
    // {
    //     return $this->morphOne('App\Models\TopupAutopay', 'payable');
    // }
    public function credit()
    {
        return $this->hasOne('App\Models\Credit', 'user_code', 'user_code')
            ->selectRaw('user_code,SUM(IF(type="topup",amount,-amount)) as total')
            ->groupBy('user_code');
    }

    public function truck()
    {
        return $this->belongsTo('App\Models\Truck', 'truck_id', 'id');
    }

    public function driver()
    {
        return $this->belongsTo('App\Models\Admin', 'driver_admin_id', 'id');
    }

    public function driver2()
    {
        return $this->belongsTo('App\Models\Admin', 'driver2_admin_id', 'id');
    }

    public function payments()
    {
        return $this->morphToMany(Payment::class, 'payable');
    }

    public function paid()
    {
        return $this->morphToMany(Payment::class, 'payable');
    }

    public function commission()
    {
        return $this->belongsTo('App\Models\Commission', 'commission_id', 'id');
    }

    public function point()
    {
        return $this->morphOne('App\Models\Point', 'pointable');
    }

    public function coupons()
    {
        return $this->belongsToMany('App\Models\Coupon', 'bill_coupons', 'bill_id', 'coupon_id');
    }

    public function images()
    {
        return $this->morphMany('App\Models\Upload', 'relatable');
    }

    public function scopeSearch($query, $conditions)
    {
        return $query->where(function ($query) use ($conditions) {

            ScopeQueries::scopeUserCodeBySale($query);

            if (@$conditions['bill_no']) {
                $trimmed = ltrim($conditions['bill_no'], self::PREFIX . '0');
                $query->where('id', 'like', '%' . $trimmed . '%');
            }
            if (@$conditions['bill_list']) {
                $query->whereIn('id', $conditions['bill_list']);
            }
            if (@$conditions['user_code']) {
                $query->where('user_code', 'LIKE', "%{$conditions['user_code']}%");
            }
            if (@$conditions['coupon_code']) {
                $query->where('coupon_code', 'LIKE', "%{$conditions['coupon_code']}%");
            }
            if (@$conditions['droppoint_in']) {
                switch ($conditions['droppoint_in']) {
                    case 'waiting':
                        $droppoints = ThaiShippingMethod::select('id');
                        $query->whereNull('droppoint_id');
                        $query->whereIn('status', [3, 4, 5, 6, 7]);
                        $query->whereIn('thai_shipping_method_id', $droppoints);
                        break;
                    case 'success':
                        $query->whereNotNull('droppoint_id');
                        break;
                }
            }

            if (@$conditions['status']) {
                $query->where('status', $conditions['status']);
            }
            if (@$conditions['thai_shipping_method_id']) {
                $query->where('thai_shipping_method_id', $conditions['thai_shipping_method_id']);
            }
            if (@$conditions['shipping_province_id']) {
                $query->where('shipping_province_id', $conditions['shipping_province_id']);
            }
            if (@$conditions['amphur']) {
                $amphur = Amphur::where('title_th', 'like', '%' . $conditions['amphur'] . '%')->pluck('id')->toArray();
                $query->whereIn('shipping_amphur_id', $amphur);
            }

            //
            $time_format = (@$conditions['time_format']) ? $conditions['time_format'] : 'created_at';
            if (@$conditions['date_start'] && @$conditions['date_end']) {
                ScopeQueries::scopeDateRange($query, $time_format, $conditions['date_start'], $conditions['date_end']);
            } else if (@$conditions['date_start']) {
                ScopeQueries::scopeDateStart($query, $time_format, $conditions['date_start']);
            } else if (@$conditions['date_end']) {
                ScopeQueries::scopeDateEnd($query, $time_format, $conditions['date_end']);
            }

            //
            if (@$conditions['staff']) {
                $query->where('created_admin', 'LIKE', "%{$conditions['staff']}%")
                    ->orWhere('updated_admin', 'LIKE', "%{$conditions['staff']}%")
                    ->orWhere('printed_admin', 'LIKE', "%{$conditions['staff']}%")
                    ->orWhere('droppoint_admin', 'LIKE', "%{$conditions['staff']}%")
                    ->orWhere('driver_admin', 'LIKE', "%{$conditions['staff']}%");
            }
            ///
            if (@$conditions['user_remark']) {
                $query->where('user_remark', 'LIKE', "%{$conditions['user_remark']}%");
            }
            if (@$conditions['admin_remark']) {
                $query->where('admin_remark', 'LIKE', "%{$conditions['admin_remark']}%");
            }
            if (@$conditions['truck_id']) {
                $query->where('truck_id', $conditions['truck_id']);
            }
            if (@$conditions['driver_id']) {
                $query->where('driver2_admin_id', $conditions['driver_id'])
                    ->orWhere('driver_admin_id', $conditions['driver_id']);
            }

            if (@$conditions['condition']) {
                switch ($conditions['condition']) {
                    case 'not_print':
                        $query->whereNull('printed_admin');
                        break;
                    case 'print_not_pack':
                        $query->whereNotNull('printed_admin');
                        $query->where('status', 3);
                        break;
                    case 'user_remark':
                        $query->whereNotNull('user_remark');
                        break;
                    case 'admin_remark':
                        $query->whereNotNull('admin_remark');
                        break;
                    case 'admin_packing_remark':
                        $query->whereNotNull('admin_packing_remark');
                        break;
                    case 'coupon':
                        $query->whereNotNull('coupon_id');
                        break;
                    case 'mark':
                        $query->where('mark', '>', '0');
                        break;
                    case 'withholding':
                        $query->where('withholding', '=', 1);
                        break;
                    case 'not_cancel':
                        $query->where('status', '!=', 8);
                        break;
                    case 'success_or_cancel':
                        $query->whereIn('status', [7, 8]);
                        break;
                    case 'other_charge':
                        $query->whereNotNull('other_charge_title1');
                        $query->orWhereNotNull('other_charge_title2');
                        break;
                    case 'discount':
                        $query->whereNotNull('discount_title1');
                        $query->orWhereNotNull('discount_title2');
                        break;
                    case 'not_printed_thai_shipping':
                        $query->whereNull('printed_thai_shipping');
                        break;
                    case 'printed_thai_shipping':
                        $query->whereNotNull('printed_thai_shipping');
                        break;
                    case 'sign':
                        $query->whereNotNull('sign_image');
                        break;
                    case 'refund':
                        $query->whereNotNull('addon_refund_credit');
                        break;
                    case 'map':
                        $query->whereNotNull('latitude');
                        break;
                    case 'truck':
                        $query->whereNotNull('truck_id');
                        break;
                    case 'user_created':
                        $query->whereNotNull('created_admin');
                        $query->whereNull('created_admin_id');
                        break;
                    case 'user_created_min_rate':
                        $query->whereNull('created_admin_id');
                        $query->where('shipping_price', '<', 100);
                        break;
                    case 'commission':
                        $query->whereNotNull('commission_id');
                        break;
                    case 'commission_null':
                        $query->whereNull('commission_id');
                        break;
                }
            }
            if (@$conditions['rate_id']) {
                $query->where('rate_id', $conditions['rate_id']);
            }

            $warehouseIds = [];
            if (@$conditions['zone_a'] && $conditions['zone_a'] == 'true') {
                array_push($warehouseIds, 7);
            }
            if (@$conditions['zone_b'] && $conditions['zone_b'] == 'true') {
                array_push($warehouseIds, 8);
            }
            if (@$conditions['zone_c'] && $conditions['zone_c'] == 'true') {
                array_push($warehouseIds, 9);
            }
            if (@$conditions['zone_d'] && $conditions['zone_d'] == 'true') {
                array_push($warehouseIds, 10);
            }
            if (count($warehouseIds) > 0) {
                $query->whereHas('trackings', function ($query) use ($warehouseIds) {
                    $query->whereIn('warehouse_id', $warehouseIds);
                });
            }
        });
    }

    public function scopeSearchCustomRate($query, $custom_rate)
    {
        if ($custom_rate) {
            $query->whereNotNull('commission_rate_id');
        } else {
            $query->whereNull('commission_rate_id');
        }
    }

    public function scopeSearchCommission($query, $conditions)
    {
        return $query->where(function ($query) use ($conditions) {
            if (@$conditions['admin_ref_id']) {
                $id = $conditions['admin_ref_id'];
                $query->whereIn('user_code', function ($query) use ($id) {
                    $query->from('users')->select('code')->where('admin_ref_id', $id);
                });
            }

            if (is_numeric(@$conditions['status'])) {
                $query->whereStatus($conditions['status']);
            }
            if (@$conditions['bill_no']) {
                $trimmed = ltrim($conditions['bill_no'], Bill::PREFIX . '0');
                $query->where('id', 'like', '%' . $trimmed . '%');
            }
            if (@$conditions['user_code']) {
                $query->where('user_code', 'like', '%' . $conditions['user_code'] . '%');
            }
            if (@$conditions['user_remark']) {
                $query->where('user_remark', 'like', '%' . $conditions['user_remark'] . '%');
            }
            if (@$conditions['admin_remark']) {
                $query->orWhere('admin_remark', 'like', '%' . $conditions['admin_remark'] . '%');
            }
            if (@$conditions['rate_id']) {
                $query->whereRateId($conditions['rate_id']);
            }
            if (@$conditions['thai_shipping_method_id']) {
                $query->whereThaiShippingMethodId($conditions['thai_shipping_method_id']);
            }
            if (@$conditions['shipping_province_id']) {
                $query->whereShippingProvinceId($conditions['shipping_province_id']);
            }

            $time_format = (@$conditions['time_format']) ? $conditions['time_format'] : 'created_at';
            if (@$conditions['date_start'] && @$conditions['date_end']) {
                ScopeQueries::scopeDateRange($query, $time_format, $conditions['date_start'], $conditions['date_end']);
            } else if (@$conditions['date_start']) {
                ScopeQueries::scopeDateStart($query, $time_format, $conditions['date_start']);
            } else if (@$conditions['date_end']) {
                ScopeQueries::scopeDateEnd($query, $time_format, $conditions['date_end']);
            }

            if (@$conditions['condition']) {
                switch ($conditions['condition']) {
                    case 'not_print':
                        $query->whereNull('printed_admin');
                        break;
                    case 'user_remark':
                        $query->whereNotNull('user_remark');
                        break;
                    case 'admin_remark':
                        $query->whereNotNull('admin_remark');
                        break;
                    case 'admin_packing_remark':
                        $query->whereNotNull('admin_packing_remark');
                        break;
                    case 'coupon':
                        $query->whereNotNull('coupon_id');
                        break;
                    case 'withholding':
                        $query->where('withholding', '=', 1);
                        break;
                    case 'not_cancel':
                        $query->where('status', '!=', 8);
                        break;
                    case 'success_or_cancel':
                        $query->whereIn('status', [7, 8]);
                        break;
                    case 'other_charge':
                        $query->whereNotNull('other_charge_title1');
                        $query->orWhereNotNull('other_charge_title2');
                        break;
                    case 'discount':
                        $query->whereNotNull('discount_title1');
                        $query->orWhereNotNull('discount_title2');
                        break;
                    case 'not_printed_thai_shipping':
                        $query->whereNull('printed_thai_shipping');
                        break;
                    case 'printed_thai_shipping':
                        $query->whereNotNull('printed_thai_shipping');
                        break;
                    case 'sign':
                        $query->whereNotNull('sign_image');
                        break;
                    case 'refund':
                        $query->whereNotNull('addon_refund_credit');
                        break;
                }
            }
        });
    }

    /*
    calculate close bill refund credit
     */
    public function calculateColseBillRefund()
    {
        $charged_credit = $this->thai_shipping_method_price + $this->thai_shipping_method_charge;
        $raw_credit = $this->thai_shipping_raw_charge + $this->thai_shipping_raw_price;
        return $charged_credit - $raw_credit;
    }
}
