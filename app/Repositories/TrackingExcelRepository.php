<?php

namespace App\Repositories;

use App\Helpers\Calculator;
use App\Helpers\Util;
use App\Imports\TrackingImport;
use App\Interfaces\TrackingExcelInterface;
use App\models\ProductType;
use App\Models\ShippingMethod;
use App\Models\Tracking;
use App\Models\Warehouse;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Maatwebsite\Excel\Facades\Excel;

class TrackingExcelRepository implements TrackingExcelInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const MINUTE_CACHE      = 0;

    public function __construct(Tracking $tracking, ProductType $productType, ShippingMethod $shippingMethod, Warehouse $warehouse)
    {
        $this->tracking           = $tracking;
        $this->productType        = $productType;
        $this->shippingMethod     = $shippingMethod;
        $this->warehouseType      = $warehouse;
    }

    public function validateExcelFile($file)
    {
        // แปลงจากฟังชั่น ProcessExcelFile2
        $start_time         = microtime(true);
        $import             = new TrackingImport();
        try {
            Excel::import($import, $file);
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $errors         = Util::customExcelFailures($e->failures());
            return $this->unprocessResponse($errors);
        }

        $track_duplicate    = $import->getTrackDuplicate();
        $arrCode            = $import->getTrackCode(); // [TRACK0001, TRACK0002, TRACK0003]
        $tags               = collect($arrCode)->map(function($code){
                                return $this->getTrackingCodePrefix($code);
                            })
                            ->push($this->getClassPrefix())
                            ->toArray();
        $key                = $this->getCacheKey(null, implode(",", $arrCode));
        $time               = $this->getTime(self::MINUTE_CACHE);
        $track_exists       = Cache::tags($tags)->remember($key, $time, function () use ($arrCode) {
                                return $this->tracking::whereIn('code', $arrCode)->get();
                            });

        $codes              = $track_exists->pluck('code')->toArray();
        $track_create       = $import->getTrackCreate($codes);
        $track_not_create   = $import->getTrackExsist($codes);

        // processExistTracking
        $track_confirm      = [];
        $track_update       = [];
        $track_reject       = [];
        $track_status       = $this->tracking::statusList();

        if ($track_not_create->count() > 0) {
            $formatters     = $track_not_create->map(function ($track) use ($track_exists, $track_status) {
                $exist      = $track_exists->firstWhere('code', $track['code']);
                // reject
                if ($exist->status >= 5) {
                    $track['admin_remark']          = '*จัดส่งแล้ว';
                    $track['import_type']           = 'reject';
                }
                // exist and different user_code, so create new
                elseif (!empty($exist->user_code) && !empty($track['user_code']) && $exist->user_code != $track['user_code']) {

                    $count                          = $this->tracking::where('code', 'like', $track['code'] . '-a%')->count();
                    $count                          = $count + 1;
                    $track['code'] .= '-a'          . $count;
                    $track['admin_remark']          = '*ซ้ำ (' . $exist->user_code . ')';
                    $track['import_type']           = 'confirm';
                } else {
                    // exist update data
                    $track['select']                = true;
                    $track['id']                    = $exist->id;

                    $track['status']                = ($track['status'] > $exist->status) ? $track['status'] : $exist->status;
                    $track['status_text']           = collect($track_status)->firstWhere('id', $track['status'])['name'];

                    $track['code_group']            = Util::getOrSet($track['code_group'], $exist->code_group);
                    $track['user_code']             = Util::getOrSet($track['user_code'], $exist->user_code);
                    $track['product_type_id']       = Util::getOrSet($track['product_type_id'], $exist->product_type_id);
                    $track['shipping_method_id']    = Util::getOrSet($track['shipping_method_id'], $exist->shipping_method_id);
                    $track['warehouse_id']          = Util::getOrSet($track['warehouse_id'], $exist->warehouse_id);

                    $track['weight']                = Util::getOrSetNumeric($track['weight'], $exist->weight, 3);
                    $track['width']                 = Util::getOrSetNumeric($track['width'], $exist->width, 2);
                    $track['length']                = Util::getOrSetNumeric($track['length'], $exist->length, 2);
                    $track['height']                = Util::getOrSetNumeric($track['height'], $exist->height, 2);

                    $track['dimension']             = Calculator::dimensionWeight($track['width'], $track['length'], $track['height']);
                    $track['cubic']                 = Calculator::cubic($track['width'], $track['length'], $track['height']);
                    $track['calculate_type']        = Calculator::calculateType($track['dimension'], $track['weight']);

                    $track['china_cost_box']        = Util::getOrSetNumeric($track['china_cost_box'], 0);
                    $track['china_cost_shipping']   = Util::getOrSetNumeric($track['china_cost_shipping'], 0);

                    $track['china_in']              = Util::getOrSet($track['china_in'], $exist->china_in);
                    $track['china_out']             = Util::getOrSet($track['china_out'], $exist->china_out);
                    $track['thai_in']               = Util::getOrSet($track['thai_in'], $exist->thai_in);
                    $track['thai_out']              = Util::getOrSet($track['thai_out'], $exist->thai_out);

                    $track['warehouse_zone']        = Util::concatField($exist->warehouse_zone, $track['warehouse_zone']);
                    $track['admin_remark']          = Util::concatField($exist->admin_remark, $track['admin_remark']);
                    $track['reference_code']        = Util::concatField($exist->reference_code, $track['reference_code']);

                    $track['import_type'] = 'update';
                }
                return $track;
            });

            $track_confirm      = $formatters->where('import_type', 'confirm')->values();
            $track_update       = $formatters->where('import_type', 'update')->values();
            $track_reject       = $formatters->where('import_type', 'reject')->values();
        }

        return $this->coreResponse(200, __('messages.import_excel'), [
            'duplicate'         => $track_duplicate,
            'create'            => $track_create,
            'update'            => $track_update,
            'confirm'           => $track_confirm,
            'reject'            => $track_reject,
            'log'               => [
                'duplicate'     => count($track_duplicate),
                'create'        => count($track_create),
                'update'        => count($track_update),
                'confirm'       => count($track_confirm),
                'reject'        => count($track_reject),
                'execute_time'  => round(microtime(true) - $start_time, 2),
            ],
        ]);
    }

    public function createTrack(array $data)
    {
        $admin          = auth()->user();
        $insert_uid     = uniqid();

        $insert         = collect($data['create'])
            ->concat($data['confirm'])
            ->where('select', true)
            ->values();

        $update         = collect($data['update'])
            ->where('select', true)
            ->values();
        //  check empty
        if ($insert->count() === 0 && $update->count() === 0) {
            return $this->coreResponse(500, __('messages.no_data_create_or_update', ['data' => __('messages.trackings.tracking')]));
        }

        if ($insert->count() > 0) {
            $this->tracking->insert(
                $insert->map(function ($track) use ($admin, $insert_uid) {
                    $track                      = $this->exceptTrack($track);
                    $track['created_admin']     = $admin->username;
                    $track['created_admin_id']  = $admin->id;
                    $track['created_at']        = date('Y-m-d H:i:s');
                    $track['updated_at']        = date('Y-m-d H:i:s');
                    $track['insert_uid']        = $insert_uid;
                    return $track;
                })->toArray()
            );
        }

        if ($update->count() > 0) {
            $update->each(function ($track) use ($admin) {

                $data_track                 = $this->exceptTrack($track);
                $data_track                 = $this->tracking->find($track['id']);

                $data_track->update([
                    'updated_admin'         => $admin->username,
                    'updated_admin_id'      => $admin->id,
                    'status'                => $track['status'],
                    'code_group'            => @$track['code_group'],
                    'user_code'             => $track['user_code'],
                    'product_type_id'       => $track['product_type_id'],
                    'shipping_method_id'    => $track['shipping_method_id'],
                    'warehouse_id'          => $track['warehouse_id'],
                    'weight'                => $track['weight'],
                    'width'                 => $track['width'],
                    'length'                => $track['length'],
                    'height'                => $track['height'],
                    'cubic'                 => $track['cubic'],
                    'dimension'             => $track['dimension'],
                    'calculate_type'        => $track['calculate_type'],
                    'china_cost_box'        => $track['china_cost_box'],
                    'china_cost_shipping'   => $track['china_cost_shipping'],
                    'china_in'              => $track['china_in'],
                    'china_out'             => $track['china_out'],
                    'thai_in'               => $track['thai_in'],
                    'thai_out'              => $track['thai_out'],
                    'warehouse_zone'        => $track['warehouse_zone'],
                    'admin_remark'          => $track['admin_remark'],
                    'reference_code'        => $track['reference_code'],
                ]);
            });
        }

        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.trackings.tracking')]));
    }

    public function createOrUpdateTrack(array $data)
    {
        $insert_uid         = uniqid();

        $productType        = $this->productType::$types;
        $shippingMethod     = $this->shippingMethod::$methods;
        $warehouseType      = $this->warehouseType::$types;

        $results = collect($data)->map(function ($tmp) use ($insert_uid, $productType, $shippingMethod, $warehouseType) {
            //  check tmp data
            if (empty($tmp['code'])) {
                return $tmp;
            }

            $code                    = Util::trimUpperCase($tmp['code']);
            $exist                   = $this->tracking::whereCode($code)->first();

            $code_group              = Util::trimUpperCase($tmp['code_group']);
            $user_code               = Util::trimUpperCase($tmp['user_code']);
            $reference_code          = Util::trimUpperCase($tmp['reference_code']);

            $product_type_id         = Util::getOrSetDefaultEnum($productType, $tmp['product_type_text']);
            $shipping_method_id      = Util::getOrSetDefaultEnum($shippingMethod, $tmp['shipping_method_text']);
            $warehouse_id            = Util::getOrSetDefaultEnum($warehouseType, $tmp['warehouse_text']);

            // create new track
            if (!$exist) {
                $weight                 = round($tmp['weight'], 3);
                $width                  = round($tmp['width'], 2);
                $height                 = round($tmp['height'], 2);
                $length                 = round($tmp['length'], 2);

                $dimension              = Calculator::dimensionWeight($width, $length, $height);
                $cubic                  = Calculator::cubic($width, $length, $height);
                $calculate_type         = Calculator::calculateType($dimension, $weight);

                $status                 = Util::getOrSet($tmp['status'], 1);
                $status                 = ($tmp['thai_in'] && $status < 2) ? 2 : $status;

                $exist                  = $this->tracking::create([
                    'warehouse_zone'        => $tmp['warehouse_zone'],
                    'china_in'              => $tmp['china_in'],
                    'china_out'             => $tmp['china_out'],
                    'thai_in'               => $tmp['thai_in'],
                    'admin_remark'          => $tmp['admin_remark'],
                    'code'                  => $code,
                    'insert_uid'            => $insert_uid,
                    'code_group'            => $code_group,
                    'user_code'             => $user_code,
                    'reference_code'        => $reference_code,
                    'product_type_id'       => $product_type_id,
                    'shipping_method_id'    => $shipping_method_id,
                    'warehouse_id'          => $warehouse_id,
                    'weight'                => $weight,
                    'width'                 => $width,
                    'height'                => $height,
                    'length'                => $length,
                    'dimension'             => $dimension,
                    'cubic'                 => $cubic,
                    'calculate_type'        => $calculate_type,
                    'status'                => $status
                ]);
            }
            // update track has status less or equal 4
            else if ($exist->status <= 4) {
                $warehouse_zone          = Util::concatField($exist->warehouse_zone, $tmp['warehouse_zone']);

                $weight                  = Util::getOrSetNumeric($tmp['weight'], $exist->weight, 3);
                $width                   = Util::getOrSetNumeric($tmp['width'], $exist->width, 2);
                $height                  = Util::getOrSetNumeric($tmp['height'], $exist->height, 2);
                $length                  = Util::getOrSetNumeric($tmp['length'], $exist->length, 2);

                $dimension               = Calculator::dimensionWeight($width, $length, $height);
                $cubic                   = Calculator::cubic($width, $length, $height);
                $calculate_type          = Calculator::calculateType($dimension, $weight);

                $china_in                = Util::getOrSet($tmp['china_in'], $exist->china_in);
                $china_out               = Util::getOrSet($tmp['china_out'], $exist->china_out);
                $thai_in                 = Util::getOrSet($tmp['thai_in'], $exist->thai_in);
                $admin_remark            = Util::getOrSet($tmp['admin_remark'], $exist->admin_remark);

                $stauts                  = Util::getOrSet($tmp['status'], $exist->status);
                $status                  = ($thai_in && $stauts < 2) ? 2 : $stauts;

                $exist->update([
                    'insert_uid'            => $insert_uid,
                    'code_group'            => $code_group,
                    'user_code'             => $user_code,
                    'reference_code'        => $reference_code,
                    'warehouse_zone'        => $warehouse_zone,
                    'product_type_id'       => $product_type_id,
                    'shipping_method_id'    => $shipping_method_id,
                    'warehouse_id'          => $warehouse_id,
                    'weight'                => $weight,
                    'width'                 => $width,
                    'height'                => $height,
                    'length'                => $length,
                    'dimension'             => $dimension,
                    'cubic'                 => $cubic,
                    'calculate_type'        => $calculate_type,
                    'china_in'              => $china_in,
                    'china_out'             => $china_out,
                    'thai_in'               => $thai_in,
                    'admin_remark'          => $admin_remark,
                    'status'                => $status
                ]);
            }

            return $exist->load(['warehouse', 'productTypes', 'shippingMethods']);
        });

        return $this->coreResponse(200, __('messages.save_data', ['data' => __('messages.trackings.tracking')]), $results);
    }

    private function exceptTrack($track)
    {
        return Arr::except($track, [
            'select',
            'warehouse',
            'product_type',
            'shipping_method',
            'uuid',
            'status_text',
            'import_type',
        ]);
    }
}
