<?php

namespace App\Repositories;

use App\Helpers\ScopeQueries;
use App\Interfaces\ReportInterface;
use App\Models\ThaiShippingMethod;
use App\Models\User;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class ReportRepository implements ReportInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;


    const MINUTE_CACHE      = 1;

    public function __construct(User $user, ThaiShippingMethod $thaiShippingMethod)
    {
        $this->user                 = $user;
        $this->thaiShippingMethod   = $thaiShippingMethod;
    }

    public function fetchUserByFields(array $conditions = [])
    {
        $key        = $this->getCacheKey(null, $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $reports    = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions) {
            $date   = DB::table('users')
                ->selectRaw("DATE(created_at) as date, COUNT(*) as counter")
                ->where(function ($query) use ($conditions) {
                    ScopeQueries::scopeDateRangeRaw($query, 'created_at', @$conditions['date_start'], @$conditions['date_end']);
                })
                ->groupBy('date')
                ->orderBy('date', 'DESC')
                ->get();

            $shippings = $this->thaiShippingMethod::join(
                DB::raw('(' . $this->subQueryUser($conditions) . ') AS sub'),
                'sub.thai_shipping_method_id',
                '=',
                'thai_shipping_methods.id'
            )
                ->select(DB::raw('id, title_th, counter'))
                ->orderBy('counter', 'DESC')
                ->get();

            $provinces = DB::table('provinces')
                ->join(
                    DB::raw('(' . $this->subQueryProvince($conditions) . ') AS sub'),
                    'sub.province_id',
                    '=',
                    'provinces.id'
                )
                ->select(DB::raw('id, title_th, counter'))
                ->orderBy('counter', 'DESC')
                ->get();


            return [
                'date' => $date,
                'shippings' => $shippings,
                'provinces' => $provinces,
            ];
        });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.report')]), $reports);
    }

    private function subQueryUser($conditions)
    {
        return $this->user::select(DB::raw('thai_shipping_method_id, COUNT(*) as counter'))
            ->where(function ($query) use ($conditions) {
                ScopeQueries::scopeDateRangeRaw($query, 'created_at', @$conditions['date_start'], @$conditions['date_end']);
            })
            ->groupBy('thai_shipping_method_id')
            ->toSql();
    }

    private function subQueryProvince($conditions)
    {
        return $this->user::select(DB::raw('province_id, COUNT(*) as counter'))
            ->where(function ($query) use ($conditions) {
                ScopeQueries::scopeDateRangeRaw($query, 'created_at', @$conditions['date_start'], @$conditions['date_end']);
            })
            ->groupBy('province_id')
            ->toSql();
    }

    public function tracking(array $conditions)
    {
        $key        = $this->getCacheKey(null, $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);
        $reports = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions) {
            return  DB::table('trackings')->select(DB::raw('DATE(' . @$conditions['field'] . ') as date, COUNT(*) as counter,
                    SUM(IF((shipping_method_id=1),price,0)) AS price_car,
                    SUM(IF((product_type_id=1 AND shipping_method_id=1 AND calculate_type="weight"), weight, 0)) AS weight_car_general,
                    SUM(IF((product_type_id=1 AND shipping_method_id=1 AND calculate_type="cubic"), cubic, 0)) AS cubic_car_general,

                    SUM(IF((product_type_id=2 AND shipping_method_id=1 AND calculate_type="weight"), weight, 0)) AS weight_car_iso,
                    SUM(IF((product_type_id=2 AND shipping_method_id=1 AND calculate_type="cubic"), cubic, 0)) AS cubic_car_iso,

                    SUM(IF((product_type_id=3 AND shipping_method_id=1 AND calculate_type="weight"), weight, 0)) AS weight_car_brand,
                    SUM(IF((product_type_id=3 AND shipping_method_id=1 AND calculate_type="cubic"), cubic, 0)) AS cubic_car_brand,

                    SUM(price) AS price,

                    SUM(IF((shipping_method_id=2),price,0)) AS price_ship,

                    SUM(IF((product_type_id=1 AND shipping_method_id=2 AND calculate_type="weight"), weight, 0)) AS weight_ship_general,
                    SUM(IF((product_type_id=1 AND shipping_method_id=2 AND calculate_type="cubic"), cubic, 0)) AS cubic_ship_general,
                    SUM(IF((product_type_id=2 AND shipping_method_id=2 AND calculate_type="weight"), weight, 0)) AS weight_ship_iso,
                    SUM(IF((product_type_id=2 AND shipping_method_id=2 AND calculate_type="cubic"), cubic, 0)) AS cubic_ship_iso,
                    SUM(IF((product_type_id=3 AND shipping_method_id=2 AND calculate_type="weight") ,weight, 0)) AS weight_ship_brand,
                    SUM(IF((product_type_id=3 AND shipping_method_id=2 AND calculate_type="cubic"), cubic, 0)) AS cubic_ship_brand

                '))
                ->where(function ($query) use ($conditions) {
                    $query->whereNotNull($conditions['field']);
                    if (@$conditions['date_start']) {
                        $query->whereRaw('DATE(' . $conditions['field'] . ') >= "' . $conditions['date_start'] . '"');
                    }
                    if (@$conditions['date_end']) {
                        $query->whereRaw('DATE(' . $conditions['field'] . ') <= "' . $conditions['date_end'] . '"');
                    }
                })
                ->where(function ($query) use ($conditions) {
                    if (@$conditions['status']) {
                        $query->where('status', '=', $conditions['status']);
                    }
                })
                ->whereNull('deleted_at')
                ->groupBy('date')
                ->orderBy('date', 'DESC')
                ->get();
        });
        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.report')]), $reports);
    }

    public function topup(array $conditions)
    {
        $key        = $this->getCacheKey(null, $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);
        $reports    = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions) {
            $total = DB::table('topups')
                ->select(
                    DB::raw('
                            SUM(IF(method="transfer", amount,0)) AS transfer,
                            SUM(IF(method="cash", amount,0)) AS cash,
                            SUM(IF(method="credit", amount,0)) AS credit,
                            SUM(IF(method="cheque", amount,0)) AS cheque,
                            SUM(IF(method="bt_card", amount,0)) AS bt_card,
                            SUM(IF(method="other", amount,0)) AS other,
                            SUM(amount) AS total')
                )
                ->where('status', 'approved')
                ->where(function ($query) use ($conditions) {
                    if (@$conditions['date_start']) {
                        $query->whereRaw('DATE(date) >= "' . $conditions['date_start'] . '"');
                    }
                    if (@$conditions['date_end']) {
                        $query->whereRaw('DATE(date) <= "' . $conditions['date_end'] . '"');
                    }
                })
                ->first();

            $date = DB::table('topups')->select(DB::raw('date, COUNT(*) as counter,
            SUM(IF(status="cancel",1,0)) AS cancel,             
            ROUND(SUM(IF(status="approved" AND method="transfer", amount,0)),2) AS amount_transfer,
            ROUND(SUM(IF(status="approved" AND method="cash", amount,0)),2) AS amount_cash,
            ROUND(SUM(IF(status="approved" AND method="credit", amount,0)),2) AS amount_credit,
            ROUND(SUM(IF(status="approved" AND method="cheque", amount,0)),2) AS amount_cheque,
            ROUND(SUM(IF(status="approved" AND method="bt_card", amount,0)),2) AS amount_bt_card,
            ROUND(SUM(IF(status="approved" AND method="other", amount,0)),2) AS amount_other,

            ROUND(SUM(IF(status="approved" AND method="transfer", 1,0)),2) AS count_transfer,
            ROUND(SUM(IF(status="approved" AND method="cash", 1,0)),2) AS count_cash,
            ROUND(SUM(IF(status="approved" AND method="credit", 1,0)),2) AS count_credit,
            ROUND(SUM(IF(status="approved" AND method="cheque", 1,0)),2) AS count_cheque,
            ROUND(SUM(IF(status="approved" AND method="bt_card", 1,0)),2) AS count_bt_card,
            ROUND(SUM(IF(status="approved" AND method="other", 1,0)),2) AS count_other,
            ROUND(SUM(IF(status="approved", amount,0)),2) AS total,

            SUM(IF(status="approved", 1,0)) AS approved'))
                ->whereIn('status', ['approved', 'cancel'])
                ->where(function ($query) use ($conditions) {
                    if (@$conditions['date_start']) {
                        $query->whereRaw('DATE(date) >= "' . $conditions['date_start'] . '"');
                    }
                    if (@$conditions['date_end']) {
                        $query->whereRaw('DATE(date) <= "' . $conditions['date_end'] . '"');
                    }
                })
                ->groupBy('date')
                ->orderBy(DB::raw('DATE(approved_at)'), 'DESC')
                ->get();

            $sub_query =  DB::table('topups')
                ->select(DB::raw('COUNT(*) as counter, system_bank_account_id, ROUND(SUM(amount),2) AS total'))
                ->where(function ($query) use ($conditions) {
                    if (@$conditions['date_start']) {
                        $query->whereRaw('DATE(date) >= "' . $conditions['date_start'] . '"');
                    }
                    if (@$conditions['date_end']) {
                        $query->whereRaw('DATE(date) <= "' . $conditions['date_end'] . '"');
                    }
                    $query->whereRaw('status = "approved"');
                    $query->whereRaw('method = "transfer"');
                })
                ->groupBy('system_bank_account_id')
                ->toSql();

            $banks = DB::table('bank_accounts')
                ->join(
                    DB::raw('(' . $sub_query . ') AS sub'),
                    'sub.system_bank_account_id',
                    '=',
                    'bank_accounts.id'
                )
                ->select(DB::raw('id, bank, account, title, counter, total'))
                ->orderBy('total', 'DESC')
                ->get();

            return [
                'total' => $total,
                'date'  => $date,
                'banks' => $banks
            ];
        });
        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.report')]), $reports);
    }
    public function fetchBillList(array $conditions = [])
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);
        $reports    = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions) {

            $total  = DB::table('bills')
                ->select(DB::raw('COUNT(*) as counter,
                                    SUM(IF(status=8,1,0)) AS cancel,
                                    SUM(IF((status=7), total_cubic,0)) AS cubic,
                                    SUM(IF((status=7), total_weight,0)) AS weight,
                                    ROUND(SUM(IF((status=7), shipping_price,0)),2) AS oversea_shipping,
                                    ROUND(SUM(IF((status=7), thai_shipping_method_price,0)),2) AS thai_shipping,
                                    ROUND(SUM(IF((status=7), cost_box_china,0)),2) AS cost_box_china,
                                    ROUND(SUM(IF((status=7), cost_shipping_china,0)),2) AS cost_shipping_china,
                                    ROUND(SUM(IF((status=7), thai_shipping_method_charge,0)),2) AS charge,
                                    ROUND(SUM(IF((status=7), discount_price1,0)),2) AS discount1,
                                    ROUND(SUM(IF((status=7), discount_price2,0)),2) AS discount2,
                                    ROUND(SUM(IF((status=7), other_charge_price1,0)),2) AS other_charge1,
                                    ROUND(SUM(IF((status=7), other_charge_price2,0)),2) AS other_charge2,
                                    ROUND(SUM(IF((status=7), coupon_price,0)),2) AS coupon,
                                    ROUND(SUM(IF((status=7), thai_shipping_raw_charge,0)),2) AS raw_charge,
                                    ROUND(SUM(IF((status=7), thai_shipping_raw_price,0)),2) AS raw_thai_shipping,
                                    ROUND(SUM(IF((status=7), addon_refund_credit,0)),2) AS addon_refund,
                                    ROUND(SUM(IF((status=7), sub_total,0)),2) AS sub_total,
                                    ROUND(SUM(IF((status=7), withholding_amount,0)),2) AS withholding,
                                    ROUND(SUM(IF((status=7), total_price,0)),2) AS total,
                                    SUM(IF((status=7), 1,0)) AS success'))
                ->whereIn('status', [7, 8])
                ->where(function ($query) use ($conditions) {
                    ScopeQueries::scopeDateRangeRaw($query, 'succeed_at', @$conditions['date_start'], @$conditions['date_end']);
                    $this->searchBill($query, $conditions);
                })
                ->first();

            $data   = DB::table('bills')
                ->select(DB::raw('DATE_FORMAT(succeed_at,"%Y-%m-%d") as date, COUNT(*) as counter,
                                    SUM(IF(status=8,1,0)) AS cancel,
                                    SUM(IF((status=7), total_cubic,0)) AS cubic,
                                    SUM(IF((status=7), total_weight,0)) AS weight,
                                    ROUND(SUM(IF((status=7), shipping_price,0)),2) AS oversea_shipping,
                                    ROUND(SUM(IF((status=7), thai_shipping_method_price,0)),2) AS thai_shipping,
                                    ROUND(SUM(IF((status=7), cost_box_china,0)),2) AS cost_box_china,
                                    ROUND(SUM(IF((status=7), cost_shipping_china,0)),2) AS cost_shipping_china,
                                    ROUND(SUM(IF((status=7), thai_shipping_method_charge,0)),2) AS charge,
                                    ROUND(SUM(IF((status=7), discount_price1,0)),2) AS discount1,
                                    ROUND(SUM(IF((status=7), discount_price2,0)),2) AS discount2,
                                    ROUND(SUM(IF((status=7), other_charge_price1,0)),2) AS other_charge1,
                                    ROUND(SUM(IF((status=7), other_charge_price2,0)),2) AS other_charge2,
                                    ROUND(SUM(IF((status=7), coupon_price,0)),2) AS coupon,
                                    ROUND(SUM(IF((status=7), thai_shipping_raw_charge,0)),2) AS raw_charge,
                                    ROUND(SUM(IF((status=7), thai_shipping_raw_price,0)),2) AS raw_thai_shipping,
                                    ROUND(SUM(IF((status=7), addon_refund_credit,0)),2) AS addon_refund,
                                    ROUND(SUM(IF((status=7), sub_total,0)),2) AS sub_total,
                                    ROUND(SUM(IF((status=7), withholding_amount,0)),2) AS withholding,
                                    ROUND(SUM(IF((status=7), total_price,0)),2) AS total,
                                    SUM(IF((status=7), 1,0)) AS success'))
                ->whereIn('status', [7, 8])
                ->where(function ($query) use ($conditions) {
                    ScopeQueries::scopeDateRangeRaw($query, 'succeed_at', @$conditions['date_start'], @$conditions['date_end']);
                    $this->searchBill($query, $conditions);
                })
                ->groupBy('date')
                ->orderBy('date', 'DESC')
                ->get();

            $thai_shipping_methods = DB::table('thai_shipping_methods')
                ->select(DB::raw('title_th as title, counter, id,
                                                    coupon_price,
                                                    cubic,
                                                    weight,
                                                    oversea_shipping,
                                                    type,
                                                    total,
                                                    thai_shipping_method_price,
                                                    thai_shipping_raw_price,
                                                    thai_shipping_method_charge,
                                                    thai_shipping_raw_charge'))
                ->join(
                    DB::raw('(' . $this->subQueryBillThaiShippingMethod($conditions) . ') AS sub'),
                    'sub.thai_shipping_method_id',
                    '=',
                    'thai_shipping_methods.id'
                )
                ->orderBy('thai_shipping_method_price', 'DESC')
                ->get();

            $provinces = DB::table('provinces')
                ->select(DB::raw('title_th as title, counter, id,
                                        coupon_price, cubic,
                                        weight,
                                        oversea_shipping,
                                        total,
                                        thai_shipping_method_price,
                                        thai_shipping_raw_price,
                                        thai_shipping_method_charge,
                                        thai_shipping_raw_charge'))
                ->join(
                    DB::raw('(' . $this->subQueryBillProvinces($conditions) . ') AS sub'),
                    'sub.shipping_province_id',
                    '=',
                    'provinces.id'
                )
                ->orderBy('thai_shipping_method_price', 'DESC')
                ->get();


            return [
                'total'                  => $total,
                'data'                   => $data,
                'shippings'              => $thai_shipping_methods,
                'provinces'              => $provinces,
            ];
        });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.report')]), $reports);
    }

    private function searchBill($query, $conditions)
    {
        if (@$conditions['province_id']) {
            $query->whereRaw('shipping_province_id = ' . $conditions['province_id']);
        }
        if (@$conditions['shipping_method_id']) {
            $query->whereRaw('thai_shipping_method_id = ' . $conditions['shipping_method_id']);;
        }
        if (@$conditions['truck_id']) {
            $query->whereRaw('truck_id = ' . $conditions['truck_id']);
        }
        return  $query;
    }

    private function subQueryBillThaiShippingMethod($conditions)
    {
        return DB::table('bills')
            ->select(DB::raw('COUNT(*) as counter,
                        thai_shipping_method_id,
                        SUM(total_cubic) AS cubic,
                        SUM(total_weight) AS weight,
                        ROUND(SUM(shipping_price),2) AS oversea_shipping,
                        ROUND(SUM(total_price),2) AS total,
                        SUM(thai_shipping_method_price) AS thai_shipping_method_price,
                        SUM(thai_shipping_raw_price) AS thai_shipping_raw_price,
                        SUM(thai_shipping_method_charge) AS thai_shipping_method_charge,
                        SUM(coupon_price) AS coupon_price,
                        SUM(thai_shipping_raw_charge) AS thai_shipping_raw_charge'))
            ->where(function ($query) use ($conditions) {
                ScopeQueries::scopeDateRangeRaw($query, 'succeed_at', @$conditions['date_start'], @$conditions['date_end']);
                $this->searchBill($query, $conditions);
                $query->whereRaw('(status=7)');
            })
            ->groupBy('thai_shipping_method_id')
            ->toSql();
    }

    private function subQueryBillProvinces($conditions)
    {
        return DB::table('bills')
            ->select(DB::raw('COUNT(*) as counter, shipping_province_id,
                        SUM(total_cubic) AS cubic,
                        SUM(total_weight) AS weight,
                        ROUND(SUM(shipping_price),2) AS oversea_shipping,
                        ROUND(SUM(total_price),2) AS total,
                        SUM(thai_shipping_method_price) AS thai_shipping_method_price,
                        SUM(thai_shipping_raw_price) AS thai_shipping_raw_price,
                        SUM(thai_shipping_method_charge) AS thai_shipping_method_charge,
                        SUM(coupon_price) AS coupon_price,
                        SUM(thai_shipping_raw_charge) AS thai_shipping_raw_charge'))
            ->where(function ($query) use ($conditions) {
                ScopeQueries::scopeDateRangeRaw($query, 'succeed_at', @$conditions['date_start'], @$conditions['date_end']);
                $this->searchBill($query, $conditions);
                $query->whereRaw('(status=7)');
            })
            ->groupBy('shipping_province_id')
            ->toSql();
    }

    public function fetchWithdrawListByFields(array $conditions = [])
    {

        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);
        $reports    = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions) {

            $date  =     DB::table('withdraws')->select(DB::raw('date, COUNT(*) as counter,
                            SUM(IF(status="cancel",1,0)) AS cancel,
                            ROUND(SUM(IF(status="approved" AND method="transfer", amount,0)),2) AS amount_transfer,
                            ROUND(SUM(IF(status="approved" AND method="cash", amount,0)),2) AS amount_cash,
                            ROUND(SUM(IF(status="approved" AND method="credit", amount,0)),2) AS amount_credit,
                            ROUND(SUM(IF(status="approved" AND method="cheque", amount,0)),2) AS amount_cheque,
                            ROUND(SUM(IF(status="approved" AND method="other", amount,0)),2) AS amount_other,

                            ROUND(SUM(IF(status="approved" AND method="transfer", 1,0)),2) AS count_transfer,
                            ROUND(SUM(IF(status="approved" AND method="cash", 1,0)),2) AS count_cash,
                            ROUND(SUM(IF(status="approved" AND method="credit", 1,0)),2) AS count_credit,
                            ROUND(SUM(IF(status="approved" AND method="cheque", 1,0)),2) AS count_cheque,
                            ROUND(SUM(IF(status="approved" AND method="other", 1,0)),2) AS count_other,

                            ROUND(SUM(IF(status="approved", charge,0)),2) AS charge,
                            ROUND(SUM(IF(status="approved", receive,0)),2) AS receive,
                            ROUND(SUM(IF(status="approved", amount,0)),2) AS total,
                            SUM(IF(status="approved", 1,0)) AS approved'))
                ->whereIn('status', ['approved', 'cancel'])
                ->where(function ($query) use ($conditions) {
                    ScopeQueries::scopeDateRangeRaw($query, 'date', @$conditions['date_start'], @$conditions['date_end']);
                })
                ->groupBy('date')
                ->orderBy(DB::raw('DATE(approved_at)'), 'DESC')
                ->get();

            $banks = DB::table('bank_accounts')
                ->join(
                    DB::raw('(' . $this->subQueryWithdraw($conditions) . ') AS sub'),
                    'sub.system_bank_account_id',
                    '=',
                    'bank_accounts.id'
                )
                ->select(DB::raw('id, bank, account, title, counter, total'))
                ->orderBy('total', 'DESC')
                ->get();

            return [
                'date'                  => $date,
                'banks'                 => $banks,
            ];
        });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.report')]), $reports);
    }

    private function subQueryWithdraw($conditions)
    {
        return  DB::table('withdraws')
            ->select(DB::raw('COUNT(*) as counter, system_bank_account_id, ROUND(SUM(amount),2) AS total'))
            ->where(function ($query) use ($conditions) {
                ScopeQueries::scopeDateRangeRaw($query, 'date', @$conditions['date_start'], @$conditions['date_end']);
                $query->whereRaw('status = "approved"');
                $query->whereRaw('method = "transfer"');
            })
            ->groupBy('system_bank_account_id')
            ->toSql();
    }

    public function paginateActiveUserByFields(array $conditions = [], $page = 1, $limit = ReportInterface::DEFAULT_LIMIT, $sorting = ReportInterface::DEFAULT_SORTING, $ascending = ReportInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $reports    = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $page, $limit, $sorting, $ascending) {
            $model  = DB::table('credits')
                ->selectRaw('users.*')
                ->leftJoin('users', 'users.code', '=', 'credits.user_code')
                ->where(function ($query) use ($conditions) {
                    ScopeQueries::queryUserCode($query, 'code');
                    ScopeQueries::queryAdminBranch($query);

                    if (@$conditions['code']) {
                        $query->where('code', 'like', '%' . $conditions['code'] . '%');
                    }
                    if (@$conditions['name']) {
                        $query->where('name', 'like', '%' . $conditions['name'] . '%');
                    }
                    if (@$conditions['tel']) {
                        $query->where('tel1', 'like', '%' . $conditions['tel'] . '%');
                        $query->orWhere('tel2', 'like', '%' . $conditions['tel'] . '%');
                    }
                    if (@$conditions['email']) {
                        $query->where('email', 'like', '%' . $conditions['email'] . '%');
                    }
                    if (@$conditions['date_start']) {
                        $query->whereRaw('DATE(credits.created_at) >= "' . $conditions['date_start'] . '"');
                    }
                    if (@$conditions['date_end']) {
                        $query->whereRaw('DATE(credits.created_at) <= "' . $conditions['date_end'] . '"');
                    }
                    if (@$conditions['user_remark']) {
                        $query->where('user_remark', 'like', '%' . $conditions['user_remark'] . '%');
                    }
                    if (@$conditions['admin_remark']) {
                        $query->where('admin_remark', 'like', '%' . $conditions['admin_remark'] . '%');
                    }
                })
                ->groupBy('user_code')
                ->orderBy($sorting, $ascending);

            if ($page == 1) {
                return $model->paginate($limit);
            } else {
                return $model->skip($limit * ($page - 1))->paginate($limit);
            }
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.report')]), $reports);
    }

    public function fetchTruckList(array $conditions = [])
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $reports    = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions) {
            //  Query 1
            $track = DB::table('bills')
                ->leftJoin('trucks', 'bills.truck_id', '=', 'trucks.id')
                ->select(
                    DB::raw(
                        'truck_id,title,
                                                        ROUND(sum(total_weight), 2) AS weight,
                                                        ROUND(sum(total_cubic), 4) AS cubic,
                                                        ROUND(sum(thai_shipping_raw_price), 2) AS cost,
                                                        ROUND(sum(thai_shipping_raw_charge), 2) AS charge,
                                                        sum(counter) AS tracking,
                                                        count(*) AS counter'
                    )
                )
                ->where(function ($query) use ($conditions) {
                    if (@$conditions['date_start']) {
                        $query->whereRaw('DATE(thai_shipping_date) >= "' . $conditions['date_start'] . '"');
                    }
                    if (@$conditions['date_end']) {
                        $query->whereRaw('DATE(thai_shipping_date) <= "' . $conditions['date_end'] . '"');
                    }
                    if (@$conditions['truck_id']) {
                        $query->whereRaw('bills.truck_id = ' . $conditions['truck_id']);
                    }
                    if (@$conditions['shipping_method_id']) {
                        $query->whereRaw('bills.thai_shipping_method_id = ' . $conditions['shipping_method_id']);
                    }
                    if (@$conditions['driver_id']) {
                        $query->whereRaw('(driver_admin_id = ' . $conditions['driver_id'] . ' OR driver2_admin_id = ' . $conditions['driver_id'] . ')');
                    }
                    $query->whereNotNull('truck_id');
                    $query->where('bills.status', '!=', 8);
                })
                ->groupBy('truck_id')
                ->orderBy('cubic', 'desc')
                ->get();

            //  Query 2
            $date = DB::table('bills')
                ->select(
                    DB::raw(
                        'DATE(thai_shipping_date) AS date,
                                                        ROUND(sum(total_weight), 2) AS weight,
                                                        ROUND(sum(total_cubic), 4) AS cubic,
                                                        ROUND(sum(thai_shipping_raw_price), 2) AS cost,
                                                        ROUND(sum(thai_shipping_raw_charge), 2) AS charge,
                                                        sum(counter) AS tracking,
                                                        count(*) AS counter'
                    )
                )
                ->where(function ($query) use ($conditions) {
                    if (@$conditions['date_start']) {
                        $query->whereRaw('DATE(thai_shipping_date) >= "' . $conditions['date_start'] . '"');
                    }
                    if (@$conditions['date_end']) {
                        $query->whereRaw('DATE(thai_shipping_date) <= "' . $conditions['date_end'] . '"');
                    }
                    if (@$conditions['truck_id']) {
                        $query->whereRaw('bills.truck_id = ' . $conditions['truck_id']);
                    }
                    if (@$conditions['shipping_method_id']) {
                        $query->whereRaw('bills.thai_shipping_method_id = ' . $conditions['shipping_method_id']);
                    }
                    if (@$conditions['driver_id']) {
                        $query->whereRaw('(driver_admin_id = ' . $conditions['driver_id'] . ' OR driver2_admin_id = ' . $conditions['driver_id'] . ')');
                    }
                    $query->whereNotNull('truck_id');
                    $query->where('bills.status', '!=', 8);
                })
                ->groupBy('date')
                ->orderBy('date', 'desc')
                ->get();

            return (object) ['truck' => $track, 'date' => $date];
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.report')]), $reports);
    }
}
