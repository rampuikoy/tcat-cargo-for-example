<?php

namespace App\Interfaces;

interface AuthInterface
{
    /**
     * get auth detail
     *
     * @return object|null
     */
    public function me();

    /**
     * update auth prfile
     *
     * @param array $data
     *
     * @return object|null
     */
    public function updateProfile(array $data);

    /**
     * update auth password
     *
     * @param array $data
     *
     * @return boolean
     */
    public function updatePassword(array $data);
}
