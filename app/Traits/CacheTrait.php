<?php

namespace App\Traits;

use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\URL;

trait CacheTrait
{
    /**
     * Variable for cache default
     *
     */
    protected $cache_time_default;

    /**
     * Variable for word "ALL" prefix
     *
     */
    protected $all_prefix = 'ALL';

    /**
     * Variable for word "URL" prefix
     *
     */
    protected $url_prefix = 'URL';

    /**
     * Variable for word "FETCH" prefix
     *
     */
    protected $fetch_prefix = 'FETCH';

    /**
     * Variable for word "PAGINATION" prefix
     *
     */
    protected $pagination_prefix = 'PAGINATION';

    /**
     * Variable for word "DUPLICATE" prefix
     *
     */
    protected $duplicate_prefix = 'DUPLICATE';

    /**
     * Variable for word "OPTION_CREATE" prefix
     *
     */
    protected $option_create_prefix = 'OPTION_CREATE';

    /**
     * Variable for word "OPTION_EDIT" prefix
     *
     */
    protected $option_edit_prefix = 'OPTION_EDIT';

    /**
     * Variable for word "ID" prefix
     *
     */
    protected $id_prefix = 'ID';

    /**
     * Variable for word "FIND BY ID" prefix
     *
     */
    protected $find_by_id = 'FIND_BY_ID';

    /**
     * Variable for word "USER CODE" prefix
     *
     */
    protected $usercode_prefix = 'USER_CODE';

    /**
     * Variable for word "USER EMAIL" prefix
     *
     */
    protected $useremail_prefix = 'USER_EMAIL';


    /**
     * Variable for word "USER CODE" prefix
     *
     */
    protected $code_prefix = 'CODE';

    /**
     * Variable for word "TRACKING CODE" prefix
     *
     */
    protected $tracking_code_prefix = 'TRACKING_CODE';

    /**
     * Variable for word "COUPON CODE" prefix
     *
     */
    protected $coupon_code_prefix = 'COUPON_CODE';

    /**
     * Variable for word "BILL ID" prefix
     *
     */
    protected $bill_id_prefix = 'BILL_ID';

    /**
     * Variable for word "SALE ID" prefix
     *
     */
    protected $sale_id_prefix = 'SALE_ID';

    /**
     * Variable for word "ADMIN ID" prefix
     *
     */
    protected $admin_id_prefix = 'ADMIN_ID';

    /**
     * Variable for word "SEARCH" prefix
     *
     */
    protected $search_prefix = 'SEARCH';

    /**
     * Variable for word "INTERNAL" prefix
     *
     */
    protected $internal_prefix = 'INTERNAL';

    /**
     * Variable for word "EXTERNAL" prefix
     *
     */
    protected $external_prefix = 'EXTERNAL';

    /**
     * Variable for word "DROPDOWN" prefix
     *
     */
    protected $dropdown_prefix = 'DROPDOWN';

    /**
     * Variable for word "DRIPPOINT" prefix
     *
     */
    protected $droppoint_prefix = 'DRIPPOINT';

    /**
     * Variable for word "DEFAULT_RATE_USER" prefix
     *
     */
    protected $default_rate_user_code_prefix = 'DEFAULT_RATE_USER_CODE';

    /**
     * Variable for word "FIND BY SHIPPING METHOD ID" prefix
     *
     */
    protected $find_by_shipping_method_id = 'FIND_BY_SHIPPING_METHOD_ID';
    /**
     * Variable for word "CHECKED" prefix
     *
     */
    protected $check_product_limit_user_code_prefix = 'CHECKED_PRODUCT_LIMIT_USER_CODE';

    /**
     * Get the class is call back this trait
     *
     * @return string - Class Name
     */
    public static function theClass()
    {
        return static::class;
    }

    /**
     * Get the class name
     *
     * @return string - Class Name
     */
    public function getClassPrefix()
    {
        return self::theClass();
    }

    /**
     * Get prefix $all_prefix with class name
     *
     * @return string - $all_prefix + Classname Prefix
     */
    public function getAllWithClassPrefix()
    {
        return $this->buildPrefixWithClass($this->all_prefix);
    }

    /**
     * Get prefix $url_prefix with class name
     *
     * @return string - $url_prefix + Classname Prefix
     */
    public function getUrlWithClassPrefix()
    {
        return $this->buildPrefixWithClass($this->url_prefix);
    }

    /**
     * Get prefix $fetch_prefix with class name
     *
     * @return string - $fetch_prefix + Classname Prefix
     */
    public function getFetchWithClassPrefix()
    {
        return $this->buildPrefixWithClass($this->fetch_prefix);
    }

    /**
     * Get prefix $pagination_prefix with class name
     *
     * @return string - $pagination_prefix + Classname Prefix
     */
    public function getPaginationWithClassPrefix()
    {
        return $this->buildPrefixWithClass($this->pagination_prefix);
    }

    /**
     * Get prefix $duplicate_prefix with class name
     *
     * @return string - $duplicate_prefix + Classname Prefix
     */
    public function getDuplicateWithClassPrefix()
    {
        return $this->buildPrefixWithClass($this->duplicate_prefix);
    }

    /**
     * Get prefix $option_create_prefix with class name
     *
     * @return string - $option_create_prefix + Classname Prefix
     */
    public function getOptionCreateWithClassPrefix()
    {
        return $this->buildPrefixWithClass($this->option_create_prefix);
    }

    /**
     * Get prefix $option_edit_prefix with class name
     *
     * @return string - $option_edit_prefix + Classname Prefix
     */
    public function getOptionEditWithClassPrefix()
    {
        return $this->buildPrefixWithClass($this->option_edit_prefix);
    }

    /**
     * Get prefix $find_by_id with class name
     *
     * @return string - $find_by_id + Classname Prefix
     */
    public function getFindByIdWithClassPrefix()
    {
        return $this->buildPrefixWithClass($this->id_prefix);
    }

    /**
     * Get prefix $search_prefix with class name
     *
     * @return string - $search_prefix + Classname Prefix
     */
    public function getSearchWithClassPrefix()
    {
        return $this->buildPrefixWithClass($this->search_prefix);
    }

    /**
     * Get prefix $dropdown_prefix with class name
     *
     * @return string - $dropdown_prefix + Classname Prefix
     */
    public function getDropDownWithClassPrefix()
    {
        return $this->buildPrefixWithClass($this->dropdown_prefix);
    }

    /**
     * Get prefix $post droppoint  with class name
     *
     * @return string - $droppoint_prefix + Classname Prefix
     */
    public function getDropPointClassPrefix()
    {
        return $this->buildPrefixWithClass($this->droppoint_prefix);
    }

    /**
     * Get prefix $usercode_prefix with class name
     *
     * @param string $iuserCode - ID of user
     *
     * @return string - $usercode_prefix + user code + Classname Prefix
     */
    public function getUserCodeWithClassPrefix($userCode)
    {
        return $this->buildPrefixWithClass($this->usercode_prefix . $userCode);
    }

   /**
     * Get prefix $usercode_prefix with class name
     *
     * @param string $iuserEmail - ID of user
     *
     * @return string - $useremail_prefix + user code + Classname Prefix
     */
    public function getUserEmailWithClassPrefix($email)
    {
        return $this->buildPrefixWithClass($this->useremail_prefix . $email);
    }

    /**
     * Get prefix $code_prefix with class name
     *
     * @param string $code
     *
     * @return string - $code_prefix +  code + Classname Prefix
     */
    public function getCodeWithClassPrefix($code)
    {
        return $this->buildPrefixWithClass($this->code_prefix . $code);
    }

    /**
     * Get prefix $sale_id_prefix with sale id and class name
     *
     * @param string $saleId - Id of sale
     *
     *
     * @return string - $sale_id_prefix + $saleId + Classname Prefix
     */
    public function getSaleIdWithClassPrefix($saleId)
    {
        return $this->buildPrefixWithClass($this->sale_id_prefix . $saleId);
    }

    /**
     * Get prefix $admin_id_prefix with admin id and class name
     *
     * @param string $adminId - Id of admin
     *
     *
     * @return string - $admin_id_prefix + $adminId + Classname Prefix
     */
    public function getAdminIdWithClassPrefix($adminId)
    {
        return $this->buildPrefixWithClass($this->admin_id_prefix . $adminId);
    }

    /**
     * Get prefix $id_prefix with class name
     *
     * @param string $id - Id of record
     *
     *
     * @return string - $id_prefix + id + Classname Prefix
     */
    public function getIdWithClassPrefix($id)
    {
        return $this->buildPrefixWithClass($this->id_prefix . '.' . $id);
    }

    /**
     * Get prefix $bill_id_prefix with class name
     *
     * @param string $id - Id of Bill
     *
     *
     * @return string - $biil_id_prefix + id + Classname Prefix
     */
    public function getBillIdWithClassPrefix($id)
    {
        return $this->buildPrefixWithClass($this->bill_id_prefix . '.' . $id);
    }

    /**
     * Get prefix $default_rate_user_code_prefix with class name
     *
     * @param string $code - code of user
     *
     * @return string - $default_rate_user_code_prefix + Classname Prefix
     */
    public function getDefaultRateUserCodeWithClassPrefix($code)
    {
        return $this->buildPrefixWithClass($this->default_rate_user_code_prefix . '.' . $code);
    }

    /**
     * Get prefix $find_by_shipping_method_id with class name
     *
     * @return string - $find_by_shipping_method_id + Classname Prefix
     */
    public function getFindByShippingMethodIdWithClassPrefix($id)
    {
        return $this->buildPrefixWithClass($this->find_by_shipping_method_id . '.' . $id);
    }

    /**
     * Get prefix $internal_prefix with tracking
     *
     * @return string - "TRACKING" + $internal_prefix
     */
    public function getTrackingInternalPrefix()
    {
        return 'TRACKING.' . $this->internal_prefix;
    }

    /**
     * Get prefix $external_prefix with tracking
     *
     * @return string - "TRACKING" + $external_prefix
     */
    public function getTrackingExternalPrefix()
    {
        return 'TRACKING.' . $this->external_prefix;
    }

    /**
     * Get prefix $usercode_prefix with user code
     *
     * @param string $userCode - Id of user
     *
     *
     * @return string - $usercode_prefix + $userCode
     */
    public function getUserCodePrefix($userCode)
    {
        return $this->usercode_prefix . $userCode;
    }

    /**
     * Get prefix $useremail_prefix with user code
     *
     * @param string $userCode - Id of user
     *
     *
     * @return string - $useremail_prefix + $email
     */
    public function getUserEmailPrefix($email)
    {
        return $this->useremail_prefix . $email;
    }

    /**
     * Get prefix $code_prefix with code
     *
     * @param string $code
     *
     *
     * @return string - $code_prefix + $code
     */
    public function getCodePrefix($code)
    {
        return $this->code_prefix . $code;
    }

    /**
     * Get prefix $sale_id_prefix with sale id
     *
     * @param string $saleId - Id of sale
     *
     *
     * @return string - $sale_id_prefix + $saleId
     */
    public function getSaleIdPrefix($saleId)
    {
        return $this->sale_id_prefix . $saleId;
    }

    /**
     * Get prefix $admin_id_prefix with admin id
     *
     * @param string $adminId - Id of sale
     *
     *
     * @return string - $admin_id_prefix + $adminId
     */
    public function getAdminIdPrefix($adminId)
    {
        return $this->admin_id_prefix . $adminId;
    }

    /**
     * Get prefix $bill_id_prefix with bill id
     *
     * @param string $billId - Id of Bill
     *
     *
     * @return string - $bill_id_prefix + $billId
     */
    public function getBillIdPrefix($billId)
    {
        return $this->bill_id_prefix . $billId;
    }

    /**
     * Get prefix $tracking_code_prefix with tracking code
     *
     * @param string $trackingCode - Code of tracking
     *
     *
     * @return string - $tracking_code_prefix + $trackingCode
     */
    public function getTrackingCodePrefix($trackingCode)
    {
        return $this->tracking_code_prefix . $trackingCode;
    }

    /**
     * Get prefix $coupon_code_prefix with coupon code
     *
     * @param string $couponCode - Code of coupon
     *
     *
     * @return string - $coupon_code_prefix + $couponCode
     */
    public function getCouponCodePrefix($couponCode)
    {
        return $this->coupon_code_prefix . $couponCode;
    }

    /**
     * Get prefix $check_product_limit_user_code_prefix with user code
     *
     * @param string $userCode - Code of user
     *
     *
     * @return string - $check_product_limit_user_code_prefix + $userCode
     */
    public function getProductLimitByUserCodePrefix($userCode)
    {
        return $this->check_product_limit_user_code_prefix . $userCode;
    }

    public function getCacheKey($prefix = null, $key)
    {
        if (!is_null($prefix)) {
            $prefix = $prefix . ".";
        }
        $prefix = strtoupper($prefix);
        $key = strtoupper($key);
        return $prefix . $key;
    }

    public function getTime($time = null)
    {
        if (is_null($time)) {

            $time = $this->getCacheDefaultTime();
        }

        $gettime = Carbon::now()->addminutes($time);

        return $gettime;
    }

    public function getCurrentUrl()
    {
        $url = urldecode(URL::full());
        return $url;
    }

    public function getUserCacheTag($code)
    {
        return 'TAG_USER_CODE' . "." . strtoupper($code);
    }

    public function clearCacheUser($code)
    {
        Cache::tags('TAG_USER_CODE' . "." . strtoupper($code))->flush();
    }

    /**
     * Get prefix with class method
     *
     * @param string $prefix - prefix or name
     *
     *
     * @return string - class name with prefix
     */
    private function buildPrefixWithClass($prefix)
    {
        return $this->getClassPrefix() . '.' . $prefix;
    }

    /**
     * Get cache default time
     *
     *
     * @return int - cache default time
     */
    private function getCacheDefaultTime()
    {
        return (int) $this->cache_time_default = env('CACHE_DEFAULT_TIME') ?? 1;
    }
}
