<?php

namespace App\Interfaces;

interface CommissionRateInterface
{
    const DEFAULT_LIMIT = 25;
    const DEFAULT_SORTING = 'id';
    const DEFAULT_ASCENDING = 'desc';

    /**
     * find commissionrate by id
     *
     * @param $id
     * @param array $columns
     *
     * @return object|null
     */
    public function findById($id, array $columns = ['*']);

    /**
     * create new commissionrate
     *
     * @param array $data
     *
     * @return object|null
     */
    public function store(array $data);

}
