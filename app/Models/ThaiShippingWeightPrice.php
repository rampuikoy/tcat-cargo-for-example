<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ThaiShippingWeightPrice extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'title',
        'min_weight',
        'max_weight',
        'price',
        'thai_shipping_method_id',
    ];
    
}
