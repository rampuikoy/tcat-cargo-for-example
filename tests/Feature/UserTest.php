<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Admin;
use Illuminate\Support\Facades\File;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserTest extends TestCase
{
  use WithFaker;
  protected $user;
  protected $test;
  protected $url = '/api/backend/user';
  private $uploadedFile = __DIR__ . '/public/excel/users.csv';
  /**
   * A basic feature test example.
   *
   * @return void
   */
  protected function authenticate(string $guard = 'admins')
  {
    $this->user = factory(Admin::class)->create();
    auth()->shouldUse($guard);
    // $this->test = User::create([
    //   'name'                      => 'Tcat-test',
    //   'email'                     => 'Tcat-test@gmail.com',
    //   'password'                  => 'a123456789',
    //   "password_confirmation"     => "a123456789",
    //   'status'                    => 'active',
    //   'withholding'               => 'active',
    //   'alert_box'                 => 'inactive',
    //   'sms_notification'          => 'inactive',
    //   'create_bill'               => 'self',
    // ]);
    $this->test = factory(User::class)->create();
    $token = JWTAuth::fromUser($this->user);
    return 'Bearer ' . $token;
  }

  protected function deleteUser()
  {
    $this->user->forceDelete();
    $this->test->forceDelete();
  }

  /** @test */
  public function GetUserSearchSuccessTest()
  {
    $id = random_int(1, 200);
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', "{$this->url}/search", [
      "id" => $id,
      "search" => "",
      "code" => "",
      "name" => "",
      "email" => "",
      "tel" => "",
      "default_rate_id" => "",
      "thai_shipping_method_id" => "",
      "province_id" => "",
      "withholding" => "",
      "tax_id" => "",
      "date_type" => "",
      "user_remark" => "",
      "admin_remark" => "",
      "sms_notification" => "",
      "status" => "",
      "admin_ref_id" => "",
    ]);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function GetUserSearchFailTest()
  {
    $response = $this->json('GET', "{$this->url}/search");
    $response->assertStatus(401);
  }

  /** @test */
  public function GetUserDropdownSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', "{$this->url}/dropdown");
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function GetUserDropdownFailTest()
  {
    $response = $this->json('GET', "{$this->url}/dropdown");
    $response->assertStatus(401);
  }

  public function UserImportExcelSuccessTest()
  {
    //TODO BUG get File excel
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('POST', "{$this->url}/import-excel", [
      'file' => File::allFiles(public_path('excel')),
    ]);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function UserImportExcelFailAuthTest()
  {
    $response = $this->json('POST', "{$this->url}/import-excel");
    $response->assertStatus(401);
  }

  /** @test */
  public function UserImportExcelFailTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('POST', "{$this->url}/import-excel");
    $response->assertStatus(422);
    $this->deleteUser();
  }

  //TODO BUG Status 400
  // /** @test */
  // public function UserResetPasswordSuccessTest()
  // {
  //   $token = $this->authenticate();
  //   $response = $this->withHeaders([
  //     'Authorization' => $token,
  //   ])->json('POST', "$this->url/{$this->test->id}/password", [
  //     'password'                  => 'a123456789',
  //     "password_confirmation"     => "a123456789",
  //   ]);
  //   $response->assertStatus(200);
  //   $this->deleteUser();
  // }

  // /** @test */
  // public function UserResetPasswordFailAuthTest()
  // {
  //   $response = $this->json('POST', "{$this->url}/1/password");
  //   $response->assertStatus(401);
  // }

  // /** @test */
  // public function UserResetPasswordFailTest()
  // {
  //   $token = $this->authenticate();
  //   $response = $this->withHeaders([
  //     'Authorization' => $token,
  //   ])->json('POST', "{$this->url}/{$this->test->id}/password");
  //   $response->assertStatus(422);
  //   $this->deleteUser();
  // }

  // /** @test */
  // public function UserSettingBillSuccessTest()
  // {
  //   $token = $this->authenticate();
  //   $response = $this->withHeaders([
  //     'Authorization' => $token,
  //   ])->json('POST', "{$this->url}/{$this->test->id}/setting-bill", [
  //     'withholding'    => 'inactive',
  //     'create_bill'    => 'self',
  //   ]);
  //   $response->assertStatus(200);
  //   $this->deleteUser();
  // }

  // /** @test */
  // public function UserSettingBillFailAuthTest()
  // {
  //   $response = $this->json('POST', "{$this->url}/1/setting-bill");
  //   $response->assertStatus(401);
  // }

  // /** @test */
  // public function UserSettingBillFailTest()
  // {
  //   $token = $this->authenticate();
  //   $response = $this->withHeaders([
  //     'Authorization' => $token,
  //   ])->json('POST', "{$this->url}/{$this->test->id}/setting-bill");
  //   $response->assertStatus(422);
  //   $this->deleteUser();
  // }

  // /** @test */
  // public function UserSettingNotificationSuccessTest()
  // {
  //   $token = $this->authenticate();
  //   $response = $this->withHeaders([
  //     'Authorization' => $token,
  //   ])->json('POST', "{$this->url}/{$this->test->id}/setting-notification", [
  //     'sms_notification'  => 'inactive',
  //     'alert_box'         => 'inactive'
  //   ]);
  //   $response->assertStatus(200);
  //   $this->deleteUser();
  // }

  // /** @test */
  // public function UserSettingNotificationFailAuthTest()
  // {
  //   $response = $this->json('POST', "{$this->url}/1/setting-notification");
  //   $response->assertStatus(401);
  // }

  // /** @test */
  // public function UserSettingNotificationFailTest()
  // {
  //   $token = $this->authenticate();
  //   $response = $this->withHeaders([
  //     'Authorization' => $token,
  //   ])->json('POST', "{$this->url}/{$this->test->id}/setting-notification");
  //   $response->assertStatus(422);
  //   $this->deleteUser();
  // }

  /** @test */
  public function GetUserAllSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', $this->url);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function GetUserAllFailTest()
  {
    $response = $this->json('GET', $this->url);
    $response->assertStatus(401);
  }

  /** @test */
  public function UserPostSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('POST', $this->url, [
      'name'                      => 'test3',
      'email'                     => 'test3@gmail.com',
      'password'                  => 'a123456789',
      "password_confirmation"     => "a123456789",
      'status'                    => 'active',
      'withholding'               => 'active',
      'alert_box'                 => 'inactive',
      'sms_notification'          => 'inactive',
      'create_bill'               => 'self',
    ]);
    $response->assertStatus(200);
    User::where('name', 'test3')->forceDelete();
    $this->deleteUser();
  }

  /** @test */
  public function UserPostFailAuthTest()
  {
    $response = $this->json('POST', $this->url);
    $response->assertStatus(401);
  }

  /** @test */
  public function UserPostFailTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('POST', $this->url);
    $response->assertStatus(422);
    $this->deleteUser();
  }

  /** @test */
  public function UserUpdateByIdSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('PUT', "{$this->url}/{$this->test->id}", [
      'name'                      => 'test',
      'email'                     => 'test3231@gmail.com',
      'status'                    => 'active',
      'withholding'               => 'active',
      'alert_box'                 => 'inactive',
      'sms_notification'          => 'inactive',
      'create_bill'               => 'self',
    ]);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function UserUpdateByIdFailAuthTest()
  {
    $response = $this->json('PUT', "{$this->url}/11");
    $response->assertStatus(401);
  }

  /** @test */
  public function UserUpdateByIdFailTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' =>  $token,
    ])->json('PUT', "{$this->url}/{$this->test->id}");
    $response->assertStatus(422);
    $this->deleteUser();
  }

  /** @test */
  public function UserDeleteSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('DELETE', "{$this->url}/{$this->test->id}");
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function UserDeleteFailTest()
  {
    $response = $this->json('DELETE', "{$this->url}/5");
    $response->assertStatus(401);
  }
}
