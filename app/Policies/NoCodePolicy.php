<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class NoCodePolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.nocode.index');
    }

    public function show(Admin $admin)
    {
        return $admin->hasAnyPermission(['backend.nocode.show', 'backend.nocode.update']);
    }

    public function store(Admin $admin)
    {
        return $admin->can('backend.nocode.store');
    }

    public function update(Admin $admin)
    {
        return $admin->can('backend.nocode.update');
    }

    public function destroy(Admin $admin)
    {
        return $admin->can('backend.nocode.destroy');
    }
}
