<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\PointOrder\ConfirmPointOrderRequest;
use App\Http\Requests\PointOrder\ShipPointOrderRequest;
use App\Http\Requests\PointOrder\StorePointOrderRequest;
use App\Http\Requests\PointOrder\UpdatePointOrderRequest;
use App\Interfaces\AdminInterface;
use App\Interfaces\PointOrderInterface;
use App\Interfaces\ProductInterface;
use App\Interfaces\ThaiShippingMethodInterface;
use App\Interfaces\TruckInterface;
use App\Interfaces\UserAddressInterface;
use App\Models\PointOrder;
use App\Traits\CoreResponseTrait;
use Illuminate\Http\Request;

class PointOrderController extends Controller
{
    use CoreResponseTrait;

    protected $pointOrderInterface, $thaiShippingMethodInterface, $userAddressInterface;

    public function __construct(PointOrderInterface $pointOrderInterface, ThaiShippingMethodInterface $thaiShippingMethodInterface, UserAddressInterface $userAddressInterface, ProductInterface $productInterface, AdminInterface $adminInterface, TruckInterface $truckInterface)
    {
        $this->pointOrderInterface          = $pointOrderInterface;
        $this->thaiShippingMethodInterface  = $thaiShippingMethodInterface;
        $this->userAddressInterface         = $userAddressInterface;
        $this->productInterface             = $productInterface;
        $this->adminInterface               = $adminInterface;
        $this->truckInterface               = $truckInterface;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('index', PointOrder::class);
        $columns     = $request->input('columns', ['*']);
        $point_order     = $this->pointOrderInterface->all($columns);
        return response()->json($point_order, $point_order->code);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePointOrderRequest $request)
    {
        $data           = $request->only([
                            'user_code',
                            'shipping_method_id',
                            'address_id',
                            'user_remark',
                            'admin_remark',
                            'products'
                        ]);
        $point_order    = $this->pointOrderInterface->store($data);
        return response()->json($point_order, $point_order->code);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $this->authorize('show', PointOrder::class);
        $columns     = $request->input('columns', ['*']);
        $point_order     = $this->pointOrderInterface->findById($id, $columns);
        return response()->json($point_order, $point_order->code);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePointOrderRequest $request, $id)
    {
        $data           = $request->only([
                            'user_remark',
                            'admin_remark'
                        ]);
        $point_order    = $this->pointOrderInterface->updateById($id, $data);
        return response()->json($point_order, $point_order->code);
    }
    
    /**
     * Display a listing search of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $this->authorize('index', PointOrder::class);
        $conditions     = $request->only(['status', 'shipping_method_id', 'user_code', 'code', 'tracking', 'user_remark', 'admin_remark', 'time_format', 'date_start', 'date_end']);
        $columns        = $request->input('columns', ['*']);
        $limit          = $request->input('limit', $this->pointOrderInterface::DEFAULT_LIMIT);
        $sorting        = $request->input('order_by', $this->pointOrderInterface::DEFAULT_SORTING);
        $ascending      = $request->input('ascending') == 1 ? 'asc' : 'desc';
        if ($request->boolean('pagination')) {
            $page       = $request->input('page', 1);
            $point_order    = $this->pointOrderInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset     = $request->input('page', 0);
            $point_order    = $this->pointOrderInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($point_order, $point_order->code);
    }

    /**
     * Display a listing of the dropdown.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function dropdown()
    {
        $dropdown = $this->pointOrderInterface->dropdownList();
        if (!$dropdown->error) {
            $thai_shipping_method   = $this->thaiShippingMethodInterface->all(['id', 'title_th']);
            $dropdown->results      = collect($dropdown->results)
                ->merge([
                    'thai_shipping_method'   => (!$thai_shipping_method->error) ? $thai_shipping_method->results : [],
                ]);
        }
        return response()->json($dropdown, $dropdown->code);
    }

        /**
     * Display a listing of the dropdown.
     *
     * @param  string  $user_code
     * @return \Illuminate\Http\Response
     */
    public function optionCreate($user_code)
    {
        $dropdown = $this->pointOrderInterface->dropdownList();
        if (!$dropdown->error) {
            $product                = $this->productInterface->all();
            $address                = $this->userAddressInterface->findByUserCodeExtend($user_code);
            $thai_shipping_method   = $this->thaiShippingMethodInterface->all();
            $dropdown->results      = collect($dropdown->results)->merge([
                                        'product'               => (!$product->error) ? $product->results : [],
                                        'address'               => (!$address->error) ? $address->results : [],
                                        'thai_shipping_method'  => (!$thai_shipping_method->error) ? $thai_shipping_method->results : [],
                                    ]);
        }
        return response()->json($dropdown, $dropdown->code);
    }

    public function checkLimit(Request $request, $user_code){
        try {
            $data           = $request->input('products', []);
            $products        = collect($data)->map(function($product) use ($user_code) {
                                $product    = $this->pointOrderInterface->checkLimit($user_code, $product['id'], $product['amount']);
                                if($product->error){
                                    throw new \Exception($product->message);
                                }
                                return $product->results;
                            });
            $response = $this->coreResponse(200, __('messages.point_order_checked'), $products);
        } catch (\Throwable $th) {
            $response = $this->coreResponse(500, $th->getMessage());
        } 
        return response()->json($response, $response->code);
    }

    public function cancel(Request $request, $id)
    {
        $this->authorize('destroy', PointOrder::class);
        $point_order    = $this->pointOrderInterface->cancelById($id);
        return response()->json($point_order, $point_order->code);
    }

    public function approve(Request $request, $id)
    {
        $this->authorize('approve', PointOrder::class);
        $point_order    = $this->pointOrderInterface->approveById($id);
        return response()->json($point_order, $point_order->code);
    }

    public function pack(Request $request, $code)
    {
        $this->authorize('update', PointOrder::class);
        $point_order    = $this->pointOrderInterface->packByCode($code);
        return response()->json($point_order, $point_order->code);
    }

    public function optionShip()
    {
        $dropdown = $this->truckInterface->all();
        if (!$dropdown->error) {
            $drivers                = $this->adminInterface->findManyDriver();
            $dropdown->results      = [
                                        'trucks' => (!$dropdown->error) ? $dropdown->results : [],
                                        'drivers' => (!$drivers->error) ? $drivers->results : [],
                                    ];
        }
        return response()->json($dropdown, $dropdown->code);
    }

    public function ship(ShipPointOrderRequest $request, $code)
    {
        $data           = $request->only(['truck_id', 'driver_admin_id', 'driver2_admin_id']);
        $point_order    = $this->pointOrderInterface->shipByCode($code, $data);
        return response()->json($point_order, $point_order->code);
    }

    public function findByCode(Request $request, $code)
    {
        $this->authorize('show', PointOrder::class);
        $columns        = $request->input('columns', ['*']);
        $point_order    = $this->pointOrderInterface->findByCode($code, $columns);
        return response()->json($point_order, $point_order->code);
    }

    public function confirm(ConfirmPointOrderRequest $request, $code)
    {
        $data           = $request->only(['shipping_method_id', 'shipping_cost', 'tracking']);
        $point_order    = $this->pointOrderInterface->confirmByCode($code, $data);
        return response()->json($point_order, $point_order->code);
    }

    public function success(Request $request, $code)
    {
        $this->authorize('update', PointOrder::class);
        $point_order    = $this->pointOrderInterface->successByCode($code);
        return response()->json($point_order, $point_order->code);
    }
}
