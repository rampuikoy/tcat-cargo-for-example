@component('mail::message')
# เรียนคุณ {{$bill->user->name}}
แจ้งอัพเดทสถานะบิลขนส่ง <br>
เลขที่บิล: <a href="#" target="_blank">{{$bill->no}}</a> <br>
รหัสลูกค้า: {{$bill->user->code}} <br>
สถานะบิล:  <span style="color: red">ยกเลิกบิล</span>
<br><br><br>
# ยกเลิกบิลแล้ว
หมายเหตุ(ลูกค้า): {{$bill->user_remark}} <br>
หมายเหตุ(เจ้าหน้าที่): {{$bill->admin_remark}} <br>
@endcomponent

