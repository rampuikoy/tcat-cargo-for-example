@component('mail::message')
# เรียนคุณ {{$topup->user->name}}
แจ้งเติมเครดิตสำเร็จ <br>
รหัสลูกค้า: {{$topup->user->code}}

@component('mail::table')
| ข้อมูลบิล      |               |
|:-------------|--------------:|
| เลขที่บิล      | {{ $topup->no }} |
| ยอดที่เติม     | {{number_format($topup->amount, 2)}} |
| สถานะบิล      | <span style="color: #1bc5bd">สำเร็จ</span> |
@if(isset($topup->userBankAccount))
| ธนาคาร        | {{$topup->userBankAccount->bank_name}} |
| ชื่อบัญชี        | {{$topup->userBankAccount->title}} |
| เลขบัญชี       | {{$topup->userBankAccount->account}} |
@endif
@endcomponent
@if(isset($topup->credit))
# ยอดเครดิตคงเหลือของคุณ <span style="color: #1bc5bd">{{number_format($topup->credit->after, 2)}}</span>
@endif
@endcomponent
