<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Admin;
use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;

$factory->define(Admin::class, function (Faker $faker , $sufAdmin) {
    return [
        'name'          => $faker->name,
        'lastname'      => $faker->name,
        'email'         => $faker->email,
        'password'      =>  Hash::make('123456789'),
        'role'          => 'SuperAdmin',
        'username'      =>  $faker->name,
        'email_verified_at' => Carbon::now(),
        'pattern_user'  => $faker->lexify('???????'),
    ];
});
