<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->text('address');
            $table->string('name', 255);
            $table->string('tel', 20)->nullable();

            $table->integer('province_id')->nullable()->unsigned()->index();
            $table->foreign('province_id')->references('id')->on('provinces');

            $table->integer('amphur_id')->nullable()->unsigned()->index();
            $table->foreign('amphur_id')->references('id')->on('amphurs');

            $table->string('district_code', 6)->nullable();
            $table->string('zipcode', 6)->nullable();

            $table->string('user_code', 20)->nullable();
            $table->foreign('user_code')->references('code')->on('users')->onDelete('restrict')->onUpdate('cascade');
            
            $table->enum('status', ['active', 'inactive'])->default('active');
            
            $table->decimal('latitude', 11, 8)->nullable();
            $table->decimal('longitude', 11, 8)->nullable();

            $table->text('user_remark')->nullable();
            $table->text('admin_remark')->nullable();

            $table->integer('created_admin_id')->unsigned()->nullable();
            $table->integer('updated_admin_id')->unsigned()->nullable();
            $table->string('created_admin', 64)->nullable();
            $table->string('updated_admin', 64)->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_addresses');
    }
}
