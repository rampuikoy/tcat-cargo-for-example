<?php

namespace App\Repositories;

use App\Interfaces\PointWithdrawInterface;
use App\Models\PointWithdraw;
use App\Traits\CacheTrait;
use Illuminate\Support\Facades\Cache;
use App\Traits\CoreResponseTrait;

class PointWithdrawRepository implements PointWithdrawInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const MINUTE_CACHE          = 1;

    public function __construct(PointWithdraw $pointWithdraw)
    {
        $this->pointWithdraw        = $pointWithdraw;
    }

    public function all(array $columns = ['*'], $sorting = PointWithdrawInterface::DEFAULT_SORTING, $ascending = PointWithdrawInterface::DEFAULT_ASCENDING)
    {
        $key            = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time           = $this->getTime(self::MINUTE_CACHE);

        $point_withdraw = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
                            return $this->pointWithdraw::select($columns)
                                                    ->orderBy($sorting, $ascending)
                                                    ->get();
                        });
        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.point_withdraw')]), $point_withdraw);
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = PointWithdrawInterface::DEFAULT_LIMIT, $sorting = PointWithdrawInterface::DEFAULT_SORTING, $ascending = PointWithdrawInterface::DEFAULT_ASCENDING)
    {
        $key            = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time           = $this->getTime(self::MINUTE_CACHE);

        $point_withdraw = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
                            return $this->pointWithdraw::select($columns)
                                                    ->search($conditions)
                                                    ->orderBy($sorting, $ascending)
                                                    ->skip($offset)
                                                    ->limit($limit)
                                                    ->get();
                        });
        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.point_withdraw')]), $point_withdraw);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = PointWithdrawInterface::DEFAULT_LIMIT, $sorting = PointWithdrawInterface::DEFAULT_SORTING, $ascending = PointWithdrawInterface::DEFAULT_ASCENDING)
    {
        $key            = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time           = $this->getTime(self::MINUTE_CACHE);

        $point_withdraw = Cache::tags([$this->getClassPrefix(), $this->getPaginationWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
                            $model = $this->pointWithdraw::select($columns)
                                ->search($conditions)
                                ->orderBy($sorting, $ascending);
                            if ($page == 1) {
                                return $model->paginate($limit);
                            } else {
                                return $model->skip($limit * ($page - 1))->paginate($limit);
                            }
                        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.point_withdraw')]), $point_withdraw);
    }

    public function findById($id, array $columns = ['*'])
    {
        $point_withdraw    = $this->findByIdMethod($id);

        if(!$point_withdraw){
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.point_withdraw'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.point_withdraw')]), $point_withdraw);
    }

    public function dropdownList()
    {
        $key        = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $dropdown   = Cache::remember($key, $time, function () {
                        return [
                            'type'      => $this->pointWithdraw::typeList(),
                            'status'    => $this->pointWithdraw::statusList(),
                        ];
                    });
        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function store(array $data)
    {
        if($data['type'] === 'withdraw'){
            $total = app('App\Repositories\UserRepository')->findByCodeMethod($data['user_code'])->point->total;
            if($total < $data['amount']){
                return $this->coreResponse(500, __('messages.no_data_with_point', ['amount' => $data['amount'], 'point' => $total]));
            }
        }
        
        $point_withdraw = $this->pointWithdraw::create([
                            'status'        => 1,
                            'user_code'     => $data['user_code'],
                            'type'          => $data['type'],
                            'amount'        => $data['amount'],
                        ]);
        $this->clearCacheTags();
        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.point_withdraw')]), $point_withdraw);
    }

    public function approveById($id)
    {
        $point_withdraw    = $this->findByIdMethod($id);

        if (!$point_withdraw) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.point_withdraw'), 'id' => $id]));
        }
        if ($point_withdraw->status !== 1) {
            return $this->coreResponse(500, __('messages.no_data_status', ['data' => __('messages.no_code'), 'status' => $point_withdraw->status]));
        }

        $auth       = auth()->user();
        $updated    = $point_withdraw->update([
                                    'status'            => 2,
                                    'approved_admin'    => $auth->username,
                                    'approved_admin_id' => $auth->id
                                ]);
        if ($updated) {
            $point_withdraw->point()->create([
                                    'user_code'         => $point_withdraw->user_code,
                                    'type'              => $point_withdraw->type,
                                    'amount'            => $point_withdraw->amount,
                                ]);
        }
        $this->clearCacheTagById($id);
        app('App\Repositories\PointRepository')->clearCacheTags();
        app('App\Repositories\UserRepository')->clearCacheTagByUserCode($point_withdraw->user_code);
        return $this->coreResponse(200, __('messages.approve_data', ['data' => __('messages.point_withdraw')]), $point_withdraw);
    }

    public function closeById($id)
    {
        $point_withdraw        = $this->findByIdMethod($id);

        if (!$point_withdraw) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.point_withdraw'), 'id' => $id]));
        }
        if ($point_withdraw->status !== 2) {
            return $this->coreResponse(500, __('messages.no_data_status', ['data' => __('messages.no_code'), 'status' => $point_withdraw->status]));
        }

        $point_withdraw->update(['status' => 3]);
        $this->clearCacheTagById($id);
        app('App\Repositories\UserRepository')->clearCacheTagByUserCode($point_withdraw->user_code);
        return $this->coreResponse(200, __('messages.close_data', ['data' => __('messages.point_withdraw')]), $point_withdraw);
    }

    public function cancelById($id)
    {
        $point_withdraw        = $this->findByIdMethod($id);

        if (!$point_withdraw) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.point_withdraw'), 'id' => $id]));
        }
        if (!in_array($point_withdraw->status, [1, 2])) {
            return $this->coreResponse(500, __('messages.no_data_status', ['data' => __('messages.no_code'), 'status' => $point_withdraw->status]));
        }

        
        $updated = $point_withdraw->update(['status' => 4]);
        if($updated){
            $point_withdraw->point->map(function($point) use ($point_withdraw){
                                        $point_withdraw->point()->create([
                                            'user_code' => $point_withdraw->user_code,
                                            'type'      => ($point_withdraw->type === 'topup') ? 'withdraw' : 'topup',
                                            'amount'    => $point_withdraw->amount,
                                        ]);
            });
        }

        $this->clearCacheTagById($id);
        app('App\Repositories\PointRepository')->clearCacheTags();
        app('App\Repositories\UserRepository')->clearCacheTagByUserCode($point_withdraw->user_code);
        return $this->coreResponse(200, __('messages.cancel_data', ['data' => __('messages.point_withdraw')]), $point_withdraw);
    }

    private function findByIdMethod($id, array $columns = ['*'])
    {
        $key            = $this->getCacheKey(null, $id);
        $time           = $this->getTime(self::MINUTE_CACHE);

        $point_withdraw = Cache::tags([
                            $this->getClassPrefix(),
                            $this->getFindByIdWithClassPrefix(),
                            $this->getIdWithClassPrefix($id)
                        ])->remember($key, $time, function () use ($id, $columns) {
                            return $this->pointWithdraw::select($columns)->whereId($id)->first();
                        });

        return $point_withdraw;
    }
    
    public function clearCacheTags()
    {
        Cache::tags([
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix()
        ])->flush();
    }
    
    private function clearCacheTagById($id)
    {
        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();
    }
}
