<?php

namespace App\Http\Requests\NoCode;

use App\Http\Requests\FormRequest;

class NoCodeExcelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->hasAnyPermission(['backend.nocode.store', 'backend.nocode.update']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nocode'           => 'required|array|min:1',
            'nocode.*.code'    => 'required',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'nocode'    => __('messages.no_code'),
            'code'      => __('form.no_code.code'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
