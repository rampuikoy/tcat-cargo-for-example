<?php

namespace App\Http\Requests\Topup;

use App\Http\Requests\FormRequest;
use App\Rules\Base64Image;
use App\Rules\ExistsBankCompany;

class StoreTopupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.topup.store');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_code'                 => 'required|exists:users,code',
            'method'                    => 'required',
            'system_bank_account_id'    => ['nullable', 'required_if:method,transfer', new ExistsBankCompany],
            'amount'                    => 'required|numeric',
            'date'                      => 'required|date_format:Y-m-d',
            'time'                      => 'required|date_format:H:i',
            'file'                      => ['nullable', 'required_if:method,transfer', new Base64Image],
            'admin_remark'              => ['nullable','required_if:status,cancel'],
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'user_code'               => __('form.withdraw.user_code'),
            'amount'                  => __('form.topup.amount'),
            'method'                  => __('form.topup.method'),
            'system_bank_account_id'  => __('form.topup.system_bank_account_id'),
            'admin_remark'            => __('form.withdraw.admin_remark'),
            'time'                    => __('form.topup.time'),
            'date'                    => __('form.topup.tidateme'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
