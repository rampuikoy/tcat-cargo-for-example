<?php
namespace App\Traits;

trait CoreResponseTrait
{
    public function coreResponse($statusCode, $message, $resulst = null)
    {
        return (object) ['error' => $statusCode !== 200, 'code' => $statusCode, 'message' => $message, 'results' => $resulst];
    }

    public function unprocessResponse($validatedErrors, $message = 'The given data was invalid.')
    {
        return (object) ['message' => $message, 'code' => 422, 'error' => true, 'errors' => $validatedErrors];
    }
}
