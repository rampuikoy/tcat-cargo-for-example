<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShippingMethod extends Model
{
    use softDeletes;

    protected $table = 'shipping_methods';

    protected $fillable = [
        'title_th',
        'title_en',
        'title_cn',
        'remark',
        'status',
        'created_admin',
        'updated_admin',
        'created_admin_id',
        'updated_admin_id',
    ];

    protected $appends = ['status_text'];

    public static function statusList()
    {
        $array = [
            'active' => __('messages.active'),
            'inactive' => __('messages.inactive'),
        ];
        return $array;
    }

    public static $methods = [
        1 => 'ทางรถ',
        2 => 'ทางเรือ',
        3 => 'ทางเครื่องบิน',
    ];

    public function getStatusTextAttribute()
    {
        return collect(self::statusList())->get($this->status);
    }

    public function scopeSearch($query, $conditions)
    {
        return $query->where(function ($query) use ($conditions) {
            if (@$conditions['id']) {
                $query->where('id', $conditions['id']);
            }
            if (@$conditions['search']) {
                $query->where('title_th', 'LIKE', "%{$conditions['search']}%")
                    ->orWhere('title_en', 'LIKE', "%{$conditions['search']}%")
                    ->orWhere('title_cn', 'LIKE', "%{$conditions['search']}%")
                    ->orWhere('status', 'LIKE', "%{$conditions['search']}%");
            }
            if (@$conditions['title_th']) {
                $query->where('title_th', 'LIKE', "%{$conditions['title_th']}%");
            }
        });
    }
}
