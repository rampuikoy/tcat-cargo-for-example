<?php

namespace App\Interfaces;

interface TrackingExcelInterface
{
    public function validateExcelFile($file);

    public function createTrack(array $data);

    public function createOrUpdateTrack(array $data);

}
