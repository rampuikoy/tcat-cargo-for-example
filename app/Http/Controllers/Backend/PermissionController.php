<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Permission\DestroyPermissionRequest;
use App\Http\Requests\Permission\StorePermissionRequest;
use App\Http\Requests\Permission\UpdatePermissionRequest;
use App\Interfaces\PermissionInterface;
use App\Models\Permission;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    protected $permissionInterface;

    public function __construct(PermissionInterface $permissionInterface)
    {
        $this->permissionInterface = $permissionInterface;
    }

    public function index(Request $request)
    {

        $this->authorize('index', Permission::class);
        $columns = $request->input('columns', ['*']);
        $permissions = $this->permissionInterface->all($columns);
        return response()->json($permissions, $permissions->code);
    }

    public function show(Request $request, $id)
    {
        $this->authorize('show', Permission::class);
        $columns = $request->input('columns', ['*']);
        $permission = $this->permissionInterface->findById($id, $columns);
        return response()->json($permission, $permission->code);
    }

    public function search(Request $request)
    {
        $this->authorize('index', Permission::class);
        $conditions = $request->only(['id', 'search', 'name', 'tag', 'description']);
        $columns = $request->input('columns', ['*']);
        $limit = $request->input('limit', $this->permissionInterface::DEFAULT_LIMIT);
        $sorting = $request->input('order_by', $this->permissionInterface::DEFAULT_SORTING);
        $ascending = $request->input('ascending') == 1 ? 'asc' : 'desc';

        if ($request->boolean('pagination')) {
            $page = $request->input('page', 1);
            $permissions = $this->permissionInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset = $request->input('page', 0);
            $permissions = $this->permissionInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($permissions, $permissions->code);
    }

    public function dropdown()
    {
        $dropdownList = $this->permissionInterface->dropdownList();
        return response()->json($dropdownList, $dropdownList->code);
    }

    public function store(StorePermissionRequest $request)
    {
        $data = $request->only([
            'name',
            'tag',
            'description',
            'guard_name',
        ]);
        $permission = $this->permissionInterface->store($data);
        return response()->json($permission, $permission->code);
    }

    public function update(UpdatePermissionRequest $request, $id)
    {
        $data = $request->only([
            'name',
            'tag',
            'description',
            'guard_name',
        ]);
        $permission = $this->permissionInterface->updateById($id, $data);
        return response()->json($permission, $permission->code);
    }

    public function destroy($id)
    {
        $this->authorize('destroy', Permission::class);
        $permission = $this->permissionInterface->deleteById($id);
        return response()->json($permission, $permission->code);
    }

    public function destroyAll(DestroyPermissionRequest $request)
    {
        $this->authorize('destroy', Permission::class);
        $permissions = $this->permissionInterface->deleteMany($request->ids);
        return response()->json($permissions, $permissions->code);
    }
}
