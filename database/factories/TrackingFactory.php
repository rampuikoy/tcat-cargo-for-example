<?php

namespace Database\Factories;

use App\Models\Tracking;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;


class TrackingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Tracking::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $sufGroup = [
            '1890172591',
            '1850101',
            '90172201',
            '59182581',
            '7852921',
        ];

        $sufUser = [
            'SF0002',
            'SF0003',
            'SF0004',
            'SF0005',
            'SF0006',
        ];

        $sufType = [
            'weight',
            'cubic',
        ];

        $sufStatus = [1, 2, 3, 4, 5, 6];
        $sufWareHouse = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

        return [
            'user_code' => Arr::random($sufUser),
            'code_group' => Arr::random($sufGroup),
            'code' => Arr::random($sufGroup) . '-' . random_int(1, 100),
            // 'reference_code',
            'weight' => random_int(1, 500) / 10,
            'width' => random_int(10, 100),
            'height' =>  random_int(10, 100),
            'length' => random_int(10, 100),
            'cubic' => 0.0000,
            'dimension' => random_int(100, 10000) / 100,
            'calculate_type' => Arr::random($sufType),
            'calculate_rate' => random_int(20, 1000),
            'price' => random_int(100, 100000) / 100,
            'cubic_rate' => random_int(1000, 10000),
            'weight_rate' => random_int(10, 100),
            'cubic_price' => random_int(100, 10000) / 100,
            'weight_price' => random_int(100, 10000) / 100,
            // 'china_in',
            // 'china_out',
            // 'thai_in',
            // 'thai_out',
            // 'user_remark',
            // 'admin_remark',
            // 'system_remark',
            'bill_id' => random_int(1, 10),
            'shipping_method_id' => 1,
            'product_type_id' => 1,
            'warehouse_id' => Arr::random($sufWareHouse),
            'warehouse_zone' => $this->faker->title(),
            'status' => Arr::random($sufStatus),
            // 'china_cost_box',
            // 'china_cost_shipping',
            // 'thai_cost_box',
            // 'thai_cost_shipping',
            'exchange_rate' => 4.97,
            // 'created_admin_id',
            // 'updated_admin_id',
            // 'created_admin',
            // 'updated_admin',
            // 'packed_admin',
            // 'packed_admin_id',
            // 'packed_at',
            // 'duplicate_order',
            'duplicate_check' => 1,
            // 'sort',
            // 'deleted_at',
            // 'insert_uid',
            // 'detail'
        ];
    }
}
