<?php

namespace App\Repositories;

use App\Interfaces\WarehouseInterface;
use App\Models\Warehouse;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use App\Traits\UploadTrait;
use Illuminate\Support\Facades\Cache;

class WarehouseRepository implements WarehouseInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, UploadTrait, CoreResponseTrait;

    const MINUTE_CACHE          = 1;

    public function __construct(Warehouse $warehouse)
    {
        $this->warehouse        = $warehouse;
    }

    public function all(array $columns = ['*'], $sorting = WarehouseInterface::DEFAULT_SORTING, $ascending = WarehouseInterface::DEFAULT_ASCENDING)
    {
        $key = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time = $this->getTime(self::MINUTE_CACHE);
        $warehouse      = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
            return $this->warehouse::select($columns)->orderBy($sorting, $ascending)->get();
        });
        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.warehouse')]), $warehouse);
    }
}
