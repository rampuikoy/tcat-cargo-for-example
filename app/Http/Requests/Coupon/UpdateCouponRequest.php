<?php

namespace App\Http\Requests\Coupon;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCouponRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.coupon.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'price' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'type' => 'in:one_time,life_time',
            'limit_user_code' => 'required|in:yes,no',
            'limit_shipping_method' => 'required|in:yes,no',
            'user_code' => 'nullable|required_if:limit_user_code,yes|exists:users,code',
            'shipping_method_id' => 'nullable|required_if:limit_shipping_method,yes|exists:thai_shipping_methods,id',
            'bill' => 'in:shipping,money_transfer,trip,all',
            'status' => 'in:active,inactive',
        ];
    }
}
