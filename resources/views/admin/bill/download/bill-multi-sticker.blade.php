<!DOCTYPE html>
<html lang="th">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>สติ๊กเกอร์ปะหน้ากล่อง</title>
	{!! Html::style('public/css/pdf.css') !!}
	<link rel="stylesheet" href="https://res.imemo.co/tcatcargo_alpha/misc/css/pdf.css" media="all" />
	<style>
		body {
			margin: 0cm 0.5cm 0 0;
		}

		table.cover td {
			border: 1px solid #000;
		}

		tbody {
			font-size: 25px;
		}
	</style>
</head>

<body>
	@foreach($bills as $key => $bill)

	<div>
		<table class="cover" aria-describedby="bill-sticker">
			<tr>
				<th scope="col"></th>
			</tr>
			<tr style="font-size: 22px;">
				@include('admin.bill.download.partials.bill-qr2')
			</tr>
			<tr style="line-height: 13px;font-size: 18px;">
				<td width="65%" style="padding: 5px;">
					<div>@lang('admin.user_code'):&nbsp;{{$bill->user_code}}&nbsp;({{$bill->user->name}})&nbsp;<br>
						@lang('admin.thai_shipping_method'):&nbsp;{{ $bill->thaiShippingMethod->title}}<br>
						@lang('admin.thai_shipping_detail'):&nbsp;{{ $bill->thai_shipping_detail}}
					</div>
				</td>
				<td>
					<div style="font-size:80px;text-align:center;margin-top:-10px;line-height:50px;">
						<strong>{{$bill->user_code}}</strong>
					</div>
					<div style="font-size:30px;text-align:center;">
						<strong>tracking&nbsp;:&nbsp;{{$bill->counter}}</strong>
					</div>
				</td>

			</tr>
			<tr>
				<td colspan="2">
					<div style="font-size:45px;text-align:left;margin-top:-25px;">
						<strong>{{ $bill->thaiShippingMethod->title}}</strong>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div style="line-height: 17px;padding: 8px 5px 8px 5px;font-size: 26px;">
						ผู้รับ:&nbsp;{{$bill->shipping_name}}&nbsp;/&nbsp;โทร&nbsp;{{$bill->shipping_tel}} <br>
						{!!str_replace(' ', '&nbsp;',$bill->shipping_address)!!}
						ต.{{$bill->shippingDistrict->title_th}}&nbsp;อ.{{$bill->shippingAmphur->title_th}}&nbsp;จ.{{$bill->shippingProvince->title_th}}&nbsp;{{$bill->shipping_zipcode}}
					</div>
				</td>
			</tr>
		</table>

	</div>
	@if($key+1<count($bills)) <div class="page-break">
		</div>
		@endif
		@endforeach

</body>

</html>