<?php

namespace App\Interfaces;

interface NoCodeInterface
{
    const DEFAULT_LIMIT = 25;
    const DEFAULT_SORTING = 'id';
    const DEFAULT_ASCENDING = 'desc';

    /**
     * fetch all nocode
     *
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function all(array $columns = ['*'], $sorting = NoCodeInterface::DEFAULT_SORTING, $ascending = NoCodeInterface::DEFAULT_ASCENDING);

    /**
     * list nocode
     *
     * @param array $conditions
     * @param array $columns
     * @param int $offset
     * @param int $limit
     *
     * @return \Illuminate\Support\Collection
     */
    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * pagination nocode
     *
     * @param array $conditions
     * @param array $columns
     * @param int $page
     * @param int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $perPage = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);
    
    /**
     * pagination gallery nocode
     *
     * @param array $conditions
     * @param array $columns
     * @param int $page
     * @param int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateGalleryByFields(array $conditions, array $columns = ['*'], $page = 1, $perPage = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * get nocode dropdown list
     *
     * @return dropdown
     */
    public function dropdownList();

    /**
     * find nocode by id
     *
     * @param string $code
     *
     * @return object|null
     */
    public function findById($id);

    /**
     * find nocode by code
     *
     * @param string $code
     *
     * @return object|null
     */
    public function findByCode($code);

    /**
     * create nocode by code
     *
     * @param string $code
     * @param array $options
     *
     * @return object|null
     */
    public function store($code, array $options);

    /**
     * update nocode by id
     *
     * @param $id
     * @param array $data
     *
     * @return object
     */
    public function updateById($id, array $data);

    /**
     * cancel nocode by id
     *
     * @param $id
     * @param array $data
     *
     * @return object
     */
    public function cancelById($id);

    /**
     * zone in nocode by code
     *
     * @param $code
     * @param array $data
     *
     * @return object
     */
    public function zoneInByCode($code, array $data);

    /**
     * zone out nocode by code
     *
     * @param $code
     * @param array $data
     *
     * @return object
     */
    public function zoneOutByCode($code, array $data);
}
