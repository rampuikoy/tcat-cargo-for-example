<?php

namespace App\Repositories;

use App\Interfaces\SeoInterface;
use App\Models\Seo;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use Illuminate\Support\Facades\Cache;

class SeoRepository implements SeoInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const MINUTE_CACHE      = 1;

    public function __construct(Seo $seo)
    {
        $this->seo = $seo;
    }

    function getDefault(array $columns = ['*'])
    {
        $seo = $this->getDefaultMethod($columns);

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.seo')]), $seo);
    }

    public function update(array $data)
    {

        $seo    = $this->getDefaultMethod();

        if (!$seo) {
            return $this->coreResponse(404, __('messages.no_data', ['data' => __('messages.seo')]));
        }

        $seo->update($data);

        Cache::tags([$this->getClassPrefix()])->flush();

        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.seo')]), $seo);
    }

    private function getDefaultMethod(array $columns = ['*'])
    {
        $key    = $this->getCacheKey(null, 'DEFAULT');
        $time   = $this->getTime(self::MINUTE_CACHE);

        $seo    = Cache::tags([$this->getClassPrefix()])->remember($key, $time, function () use ($columns) {
            return $this->seo::select($columns)->first();
        });

        return $seo;
    }
}
