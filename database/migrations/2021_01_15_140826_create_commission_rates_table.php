<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommissionRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commission_rates', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->double('kg_car_genaral', 8, 2);
            $table->double('kg_car_iso', 8, 2);
            $table->double('kg_car_brand', 8, 2);

            $table->double('kg_ship_genaral', 8, 2);
            $table->double('kg_ship_iso', 8, 2);
            $table->double('kg_ship_brand', 8, 2);

            $table->double('cubic_car_genaral', 8, 2);
            $table->double('cubic_car_iso', 8, 2);
            $table->double('cubic_car_brand', 8, 2);

            $table->double('cubic_ship_genaral', 8, 2);
            $table->double('cubic_ship_iso', 8, 2);
            $table->double('cubic_ship_brand', 8, 2);

            $table->text('admin_remark')->nullable();
            $table->string('created_admin')->nullable();
            $table->string('updated_admin')->nullable();
            $table->integer('created_admin_id')->unsigned()->nullable();
            $table->integer('updated_admin_id')->unsigned()->nullable();
            $table->integer('bill_id')->unsigned()->nullable();
            $table->integer('admin_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commission_rates');
    }
}
