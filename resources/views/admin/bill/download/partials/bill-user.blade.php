<table class="user" aria-describedby="bill-pdf-user">
	<tr>
		<th scope="col"></th>
	</tr>
	<tbody>
		<tr>
			<td rowspan="4">
				{!! DNS2D::getBarcodeHTML($bill->sign_url, "QRCODE",3,3) !!}
			</td>
			<td class="text-right" width="12%">
				<div>@lang('admin.user_code')&nbsp;: </div>
			</td>
			<td class="" width="30%">
				<div style="font-weight: bold;font-size: 20px;">{{$bill->user_code}}</div>
			</td>
			<td class="text-right" width="12%">
				<div>@lang('admin.bill_no')&nbsp;: </div>
			</td>
			<td class="">
				<div style="font-weight: bold;font-size: 20px;">{{$bill->no}}</div>
			</td>
		</tr>
		<tr>
			<td class="text-right">
				<div>@lang('admin.fullname')&nbsp;: </div>
			</td>
			<td class="">
				<div>{!!str_replace(' ', '&nbsp;',$bill->user->name)!!}</div>
			</td>
			<td class="text-right">
				<div>@lang('admin.bill_created')&nbsp;: </div>
			</td>
			<td class="">
				<div>{{$bill->created_at->format('Y-m-d')}}</div>
			</td>
		</tr>
		<tr>
			<td class="text-right">
				<div>@lang('admin.email')&nbsp;: </div>
			</td>
			<td class="">
				<div>{{$bill->user->email}}</div>
			</td>
			<td class="text-right">
				<div>@lang('admin.created_admin')&nbsp;: </div>
			</td>
			<td class="">
				<div>{{$bill->created_admin}}</div>
			</td>
		</tr>
		<tr>
			<td class="text-right">
				<div>@lang('admin.tel1')&nbsp;: </div>
			</td>
			<td class="">
				<div>{{$bill->user->tel1}}</div>
			</td>
			<td class="text-right">
				<div>@lang('admin.status')&nbsp;: </div>
			</td>
			<td class="">
				<div>{!!str_replace(' ', '&nbsp;',$bill->current_status)!!}</div>
			</td>
		</tr>

	</tbody>
</table>