<?php

namespace App\Http\Requests\Withdraw;

use App\Http\Requests\FormRequest;
use App\Rules\ExistsBankCompany;

class ApproveWithdrawRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.withdraw.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date'                      => 'required|date_format:Y-m-d',
            'time'                      => 'required|date_format:H:i',
            'system_bank_account_id'    => ['required_if:method,transfer', 'nullable', new ExistsBankCompany],
            'user_bank_account_id'      => 'required_if:method,transfer|nullable',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'date' => __('form.withdraw.date'),
            'time' => __('form.withdraw.time'),
            'system_bank_account_id' => __('form.withdraw.system_bank_account_id'),
            'user_bank_account_id' => __('form.withdraw.user_bank_account_id'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
