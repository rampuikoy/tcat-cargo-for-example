<?php

namespace App\Observers;

use App\Models\Admin;
use App\Observers\BaseObserver;
use App\Traits\UploadTrait;

class AdminObserver extends BaseObserver
{  use UploadTrait;

    /**
     * Handle the admin "creating" event.
     *
     * @param  \App\Models\Admin  $admin
     * @return void
     */
    public function creating(Admin $admin)
    {
        //
        $this->stampCreated($admin);
    }

    /**
     * Handle the admin "updating" event.
     *
     * @param  \App\Models\Admin  $admin
     * @return void
     */
    public function updating(Admin $admin)
    {
        //
        $this->stampUpdated($admin);
    }

    /**
     * Handle the admin "created" event.
     *
     * @param  \App\Models\Admin  $admin
     * @return void
     */
    public function created(Admin $admin)
    {
        $admin->assignRole($admin->role);
    }

    /**
     * Handle the admin "updated" event.
     *
     * @param  \App\Models\Admin  $admin
     * @return void
     */
    public function updated(Admin $admin)
    {
        if ($admin->isDirty('photo')) {
            $oldFilePath = $admin->getOriginal('photo');
            $this->removeFile($admin->getPhotoUrlAttribute($oldFilePath));
        }

        if ($admin->isDirty('role')) {
            $admin->syncRoles($admin->role);
        }
    }
}
