<?php

namespace App\Http\Requests\PointOrder;

use App\Http\Requests\FormRequest;

class UpdatePointOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.point-order.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'user_remark'           => 'sometimes|nullable|string',
            'admin_remark'          => 'sometimes|nullable|string',
        ];
        return $rules;
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'user_remark'           => __('form.point_order.user_remark'),
            'admin_remark'          => __('form.point_order.admin_remark'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
