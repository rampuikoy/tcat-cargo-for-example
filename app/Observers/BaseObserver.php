<?php

namespace App\Observers;

use Illuminate\Support\Facades\Auth;
class BaseObserver
{
    //
    public static function stampCreated($data)
    {
        $data->created_at = date('Y-m-d H:i:s');

        if (Auth::guard('admins')->check()) {
            $auth = Auth::guard('admins')->user();
            $data->created_admin_id = $auth->id;
            $data->created_admin = $auth->username;
        } else if (Auth::guard('users')->check()) {
            $auth = Auth::guard('users')->user();

            $data->created_admin_id = $auth->id;
            $data->created_admin = $auth->code;
        }
    }

    public static function stampUpdated($data)
    {
        $data->updated_at = date('Y-m-d H:i:s');

        if (Auth::guard('admins')->check()) {
            $auth = Auth::guard('admins')->user();
            $data->updated_admin_id = $auth->id;
            $data->updated_admin = $auth->username;
        } else if (Auth::guard('users')->check()) {
            $auth = Auth::guard('users')->user();
            $data->updated_admin_id = $auth->id;
            $data->updated_admin = $auth->code;
        }
    }
}
