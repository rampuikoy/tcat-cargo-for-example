<?php

namespace App\Http\Controllers\Backend\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ResendEmailVerificationRequest;
use App\Models\Admin;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class VerificationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }

    public function verify(Request $request, $id)
    {
        $admin = Admin::where('id', $id)->first();
        if (!Url::hasValidSignature($request)) {
            return response()->json(['message' => __('verification.invalid'), 'error' => true, 'code' => 500], 500);
        }

        if ($admin->hasVerifiedEmail()) {
            return response()->json(['message' => __('verification.already_verified'), 'error' => true, 'code' => 500], 500);
        }
        $admin->markEmailAsVerified();

        event(new Verified($admin));

        return response()->json(['message' => __('verification.verified'), 'error' => false, 'code' => 200], 200);
    }

    /**
     * Resend the email verification notification.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function resend(ResendEmailVerificationRequest $request)
    {
        $admin = Admin::where('email', $request->email)->first();
        if (!$admin) {
            return response()->json(['message' => 'Unprocessable Entity', 'error' => true, 'errors' => ['email' => __('verification.user')], 'code' => 422], 422);
        }

        if ($admin->hasVerifiedEmail()) {
            return response()->json(['message' => 'Unprocessable Entity', 'error' => true, 'errors' => ['email' => __('verification.already_verified')], 'code' => 422], 422);
        }

        $admin->sendEmailVerificationNotification();

        return response()->json(['message' => __('verification.sent'), 'error' => false, 'code' => 200], 200);
    }
}
