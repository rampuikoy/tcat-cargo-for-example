<?php

namespace App\Observers;

use App\Models\CommissionRate;

class CommissionRateObserver extends BaseObserver
{
    /**
     * Handle the admin "commission" event.
     *
     * @param  App\Models\CommissionRate $commission
     * @return void
     */
    public function creating(CommissionRate $commission)
    {
        //
        $this->stampCreated($commission);
    }

    /**
     * Handle the commission "updating" event.
     *
     * @param  App\Models\CommissionRate $commission
     * @return void
     */
    public function updating(CommissionRate $commission)
    {
        //
        $this->stampUpdated($commission);
    }
}
