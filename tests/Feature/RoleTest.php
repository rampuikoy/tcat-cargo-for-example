<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Role;
use App\Models\Admin;
use Illuminate\Support\Facades\File;
use Tymon\JWTAuth\Facades\JWTAuth;

class RoleTest extends TestCase
{
  use WithFaker;
  protected $user;
  protected $role;
  protected $url = '/api/backend/role';
  /**
   * A basic feature test example.
   *
   * @return void
   */
  protected function authenticate(string $guard = 'admins')
  {
    $this->user = factory(Admin::class)->create();
    auth()->shouldUse($guard);
    $this->role = Role::create([
      'name'          => 'test-test',
      'guard_name'    => 'admins',
    ]);
    $token = JWTAuth::fromUser($this->user);
    return 'Bearer ' . $token;
  }

  protected function deleteUser()
  {
    $this->role->forceDelete();
    $this->user->forceDelete();
  }

  /** @test */
  public function GetRoleSearchSuccessTest()
  {
    $id = random_int(1, 200);
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', "{$this->url}/search", [
      "id" => $id,
      "search" => "",
    ]);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function GetRoleSearchFailTest()
  {
    $response = $this->json('GET', "{$this->url}/search");
    $response->assertStatus(401);
  }

  /** @test */
  public function GetRoleAllSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', $this->url);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function GetRoleAllFailTest()
  {
    $response = $this->json('GET', $this->url);
    $response->assertStatus(401);
  }

  /** @test */
  public function RolePostSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('POST', $this->url, [
      'name'          => 'test',
      'guard_name'    => 'admins',
    ]);
    $response->assertStatus(200);
    Role::where('name', 'test')->forceDelete();
    $this->deleteUser();
  }

  /** @test */
  public function RolePostFailAuthTest()
  {
    $response = $this->json('POST', $this->url);
    $response->assertStatus(401);
  }

  /** @test */
  public function RolePostFailTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('POST', $this->url);
    $response->assertStatus(422);
    $this->deleteUser();
  }

  /** @test */
  public function RoleUpdateByIdSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('PUT', "{$this->url}/{$this->role->id}", [
      'name'          => 'test',
      'guard_name'    => 'admins',
    ]);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function RoleUpdateByIdFailAuthTest()
  {
    $response = $this->json('PUT', "{$this->url}/5");
    $response->assertStatus(401);
  }

  /** @test */
  public function RoleUpdateByIdFailTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' =>  $token,
    ])->json('PUT', "{$this->url}/{$this->role->id}");
    $response->assertStatus(422);
    $this->deleteUser();
  }

  /** @test */
  public function RoleDeleteSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('DELETE', "{$this->url}/{$this->role->id}");
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function RoleDeleteFailTest()
  {
    $response = $this->json('DELETE', "{$this->url}/21");
    $response->assertStatus(401);
  }
}
