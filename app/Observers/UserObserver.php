<?php

namespace App\Observers;

use App\Jobs\JobSendEmail;
use App\Jobs\JobSendSms;
use App\Models\Chat;
use App\Models\User;
use App\Observers\BaseObserver;

class UserObserver extends BaseObserver
{
    //
    public function creating(User $user)
    {
        if (empty($user->code)) {
            $user->code = $user->generateUserCode();
        }
        $this->stampCreated($user);
    }

    public function created(User $user)
    {
        Chat::withoutEvents(function () use ($user) {
            $user->chats()->create([
                'created_type'      => 'admin',
                'message'           => __('chat.registered'),
                'user_code'         => $user->code,
                'created_admin'     => $user->created_admin,
                'created_admin_id'  => $user->created_admin_id,
            ]);
        });

        $email = $user->emails()->create([
            'mailable_class'    => 'App\Mail\User\UserRegistered',
            'status'            => 'queue',
            'to'                => $user->email,
            'created_admin'     => $user->created_admin,
            'created_admin_id'  => $user->created_admin_id,
        ]);
        JobSendEmail::dispatch($email->id)->onQueue(env('QUEUE_EMAIL_PREFIX'));

        if ($user->sms_notification == 'active') {
            $sms = $user->sms()->create([
                'status'            => 'queue',
                'msisdn'            => $user->tel1,
                'message'           => __('sms.registered', [
                                            'name' => $user->name, 
                                            'code' => $user->code
                                        ]),
                'user_code'         => $user->code,
                'created_admin'     => $user->created_admin,
                'created_admin_id'  => $user->created_admin_id,
            ]);
            JobSendSms::dispatch($sms->id)->onQueue(env('QUEUE_SMS_PREFIX'));
        }
    }

    public function updating(User $user)
    {
        $this->stampUpdated($user);
    }
}
