<?php

namespace App\Interfaces;

interface TripInterface
{
    const DEFAULT_LIMIT = 25;
    const DEFAULT_SORTING = 'id';
    const DEFAULT_ASCENDING = 'desc';

    /**
     * fetch all trip
     *
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function all(array $columns = ['*'], $sorting = TripInterface::DEFAULT_SORTING, $ascending = TripInterface::DEFAULT_ASCENDING);

    /**
     * list trip
     *
     * @param array $conditions
     * @param array $columns
     * @param int $offset
     * @param int $limit
     *
     * @return \Illuminate\Support\Collection
     */
    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * pagination trip
     *
     * @param array $conditions
     * @param array $columns
     * @param int $page
     * @param int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $perPage = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * find trip by id
     *
     * @param $id
     * @param array $columns
     *
     * @return object|null
     */
    public function findById($id, array $columns = ['*']);

    /**
     * get trip dropdown list
     *
     * @return dropdown
     */
    public function dropdownList();

    /**
     * create new trip
     *
     * @param array $data
     *
     * @return object|null
     */
    public function store(array $data);

    /**
     * update trip by id
     *
     * @param $id
     * @param array $data
     *
     * @return object
     */
    public function updateById($id, array $data);


    /**
     * soft delete trip by id
     *
     * @param $id
     *
     * @return object|null
     */
    public function softDeleteById($id);


    /**
     * create trip by id
     *
     * @param $id
     *
     * @return object
     */
    public function notifyCreate($id, array $data);

    /**
     * close bill trip by id
     *
     * @param $id
     *
     * @return object
     */
    public function notifyClose($id);

    /**
     * detail  bill trip by id
     *
     * @param $id
     *
     * @return object
     */
    public function notifyDetail($id);

    /**
     * cancel  bill trip by id
     *
     * @param $id
     *
     * @return object
     */
    public function postCancel($id, array $data);


    /**
     * find trip by id get data
     *
     * @param $id
     * @param array $columns
     *
     * @return object|null
     */
    public function optionEdit($id, array $columns = ['*']);


    /**
     * get trip by user_code
     *
     * @param $user_code
     * @param array $columns
     *
     * @return object|null
     */
    public function findByUserCode($code);
}
