<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\WarehouseChineseChinese;
use App\Models\Admin;
use Illuminate\Support\Facades\File;
use Tymon\JWTAuth\Facades\JWTAuth;

class WarehouseChineseChineseTest extends TestCase
{
  use WithFaker;
  protected $user;
  protected $warehouse;
  protected $url = '/api/backend/warehouse';
  /**
   * A basic feature test example.
   *
   * @return void
   */
  protected function authenticate(string $guard = 'admins')
  {
    $this->user = factory(Admin::class)->create();
    auth()->shouldUse($guard);
    $this->warehouse = factory(WarehouseChinese::class)->create();
    $token = JWTAuth::fromUser($this->user);
    return 'Bearer ' . $token;
  }

  protected function deleteUser()
  {
    $this->user->forceDelete();
    $this->warehouse->forceDelete();
  }

  /** @test */
  public function GetWarehouseChineseSearchSuccessTest()
  {
    $id = random_int(1, 200);
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', "{$this->url}/search", [
      "id" => $id,
      "search" => "",
      "title_th" => "",
      "status"=>""
    ]);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function GetWarehouseChineseSearchFailTest()
  {
    $response = $this->json('GET', "{$this->url}/search");
    $response->assertStatus(401);
  }

  /** @test */
  public function GetWarehouseChineseDropdownSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', "{$this->url}/dropdown");
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function GetWarehouseChineseDropdownFailTest()
  {
    $response = $this->json('GET', "{$this->url}/dropdown");
    $response->assertStatus(401);
  }

  /** @test */
  public function GetWarehouseChineseAllSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', $this->url);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function GetWarehouseChineseAllFailTest()
  {
    $response = $this->json('GET', $this->url);
    $response->assertStatus(401);
  }

  /** @test */
  public function WarehouseChinesePostSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('POST', $this->url, [
      'file'          => File::get(public_path('images/test/testbase64.txt')),
      'title_th'      => 'test3',
      'title_en'      => 'test3',
      'title_cn'      => 'test3',
      'latitude'      => '-8.651651',
      'longitude'     => '-63.561119',
      'info_th'       => 'test3',
      'info_en'       => 'test3',
      'info_cn'       => 'test3',
    ]);
    $response->assertStatus(200);
    WarehouseChinese::where('title_th', 'test3')->forceDelete();
    $this->deleteUser();
  }

  /** @test */
  public function WarehouseChinesePostFailAuthTest()
  {
    $response = $this->json('POST', $this->url);
    $response->assertStatus(401);
  }

  /** @test */
  public function WarehouseChinesePostFailTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('POST', $this->url);
    $response->assertStatus(422);
    $this->deleteUser();
  }

  /** @test */
  public function WarehouseChineseUpdateByIdSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('PUT', "{$this->url}/{$this->warehouse->id}", [
      'title_th'      => 'test',
      'title_en'      => 'test',
      'title_cn'      => 'test',
      'latitude'      => '-8.651651',
      'longitude'     => '-63.561119',
      'info_th'       => 'test',
      'info_en'       => 'test',
      'info_cn'       => 'test',
    ]);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function WarehouseChineseUpdateByIdFailAuthTest()
  {
    $response = $this->json('PUT', "{$this->url}/11");
    $response->assertStatus(401);
  }

  /** @test */
  public function WarehouseChineseUpdateByIdFailTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' =>  $token,
    ])->json('PUT', "{$this->url}/{$this->warehouse->id}");
    $response->assertStatus(422);
    $this->deleteUser();
  }

  /** @test */
  public function WarehouseChineseDeleteSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('DELETE', "{$this->url}/{$this->warehouse->id}");
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function WarehouseChineseDeleteFailTest()
  {
    $response = $this->json('DELETE', "{$this->url}/5");
    $response->assertStatus(401);
  }
}
