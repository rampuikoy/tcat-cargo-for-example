<?php

namespace App\Http\Requests\Rate;

use App\Http\Requests\FormRequest;

class StoreRateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.rate.store');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'                 => 'required|max:50',
            'status'                => 'required|in:active,inactive',
            'admin_remark'          => 'required',
            'user_code'             => 'required_if:type,user|required_if:type,sale_only_user|nullable|exists:users,code',
            'type'                  => 'required|in:general,user,sale,sale_only_user',
            'admin_id'              => 'required_if:type,sale|required_if:type,sale_only_user|nullable|exists:admins,id',

            'kg_car_genaral'        => 'required|numeric',
            'kg_car_iso'            => 'required|numeric',
            'kg_car_brand'          => 'required|numeric',

            'kg_ship_genaral'       => 'required|numeric',
            'kg_ship_iso'           => 'required|numeric',
            'kg_ship_brand'         => 'required|numeric',

            'kg_plane_genaral'      => 'required|numeric',
            'kg_plane_iso'          => 'required|numeric',
            'kg_plane_brand'        => 'required|numeric',

            'cubic_car_genaral'     => 'required|numeric',
            'cubic_car_iso'         => 'required|numeric',
            'cubic_car_brand'       => 'required|numeric',

            'cubic_ship_genaral'    => 'required|numeric',
            'cubic_ship_iso'        => 'required|numeric',
            'cubic_ship_brand'      => 'required|numeric',

            'cubic_plane_genaral'   => 'required|numeric',
            'cubic_plane_iso'       => 'required|numeric',
            'cubic_plane_brand'     => 'required|numeric',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'title'                 => __('form.rate.title'),
            'status'                => __('form.rate.status'),
            'admin_remark'          => __('form.rate.admin_remark'),
            'user_code'             => __('form.rate.user_code'),
            'type'                  => __('form.rate.type'),
            'admin_id'              => __('form.rate.admin_id'),

            'kg_car_genaral'        => __('form.rate.kg_car_genaral'),
            'kg_car_iso'            => __('form.rate.kg_car_iso'),
            'kg_car_brand'          => __('form.rate.kg_car_brand'),

            'kg_ship_genaral'       => __('form.rate.kg_ship_genaral'),
            'kg_ship_iso'           => __('form.rate.kg_ship_iso'),
            'kg_ship_brand'         => __('form.rate.kg_ship_brand'),

            'kg_plane_genaral'      => __('form.rate.kg_plane_genaral'),
            'kg_plane_iso'          => __('form.rate.kg_plane_iso'),
            'kg_plane_brand'        => __('form.rate.kg_plane_brand'),

            'cubic_car_genaral'     => __('form.rate.cubic_car_genaral'),
            'cubic_car_iso'         => __('form.rate.cubic_car_iso'),
            'cubic_car_brand'       => __('form.rate.cubic_car_brand'),

            'cubic_ship_genaral'    => __('form.rate.cubic_ship_genaral'),
            'cubic_ship_iso'        => __('form.rate.cubic_ship_iso'),
            'cubic_ship_brand'      => __('form.rate.cubic_ship_brand'),

            'cubic_plane_genaral'   => __('form.rate.cubic_plane_genaral'),
            'cubic_plane_iso'       => __('form.rate.cubic_plane_iso'),
            'cubic_plane_brand'     => __('form.rate.cubic_plane_brand'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
