<?php

namespace App\Http\Requests\Bill;

use App\Http\Requests\FormRequest;

class StoreBillRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.bill.store');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'thai_shipping_detail'   => 'nullable|string',
            'user_remark'            => 'nullable|string',
            'admin_remark'           => 'nullable|string',

            'withholding'            => 'required|boolean',

            'other_charge1.title'    => 'required_with:other_charge1.price',
            'other_charge1.price'    => 'sometimes|nullable|numeric',
            'other_charge2.title'    => 'required_with:other_charge2.price',
            'other_charge2.price'    => 'sometimes|nullable|numeric',

            'discount1.title'        => 'required_with:discount1.price',
            'discount1.price'        => 'sometimes|nullable|numeric',
            'discount2.title'        => 'required_with:discount2.price',
            'discount2.price'        => 'sometimes|nullable|numeric',

            'user_code'              => 'required|exists:users,code',
            'rate_id'                => 'required',
            'thai_shipping_id'       => 'required',
            'thai_shipping_type'     => 'required|string',
            'user_address_id'        => 'required_unless:thai_shipping_type,droppoint', // required_unless ตรวจสอบความถูกต้องเป็นปัจจุบันและไม่ว่างเปล่าเว้นแต่ anotherfield (thai_shipping_type)ฟิลด์เท่ากับใดๆ (droppoint)
            'coupon'                 => 'sometimes|nullable',
            'trackings'              => 'required|array',
        ];
    }
    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'user_code'              => __('form.bill_store.user_code'),
            'thai_shipping_detail'   => __('form.bill_store.thai_shipping_detail'),

            'withholding'            => __('form.bill_store.withholding'),

            'other_charge1.title'    => __('form.bill_store.other_charge1_title'),
            'other_charge1.price'    => __('form.bill_store.other_charge1_price'),
            'other_charge2.title'    => __('form.bill_store.other_charge2_title'),
            'other_charge2.price'    => __('form.bill_store.other_charge2_price'),

            'discount1.title'        => __('form.bill_store.discount1_title'),
            'discount1.price'        => __('form.bill_store.discount1_price'),
            'discount2.title'        => __('form.bill_store.discount2_title'),
            'discount2.price'        => __('form.bill_store.discount2_price'),

            'rate_id'                => __('form.bill_store.rate_id'),
            'thai_shipping_id'       => __('form.bill_store.thai_shipping_id'),
            'thai_shipping_type'     => __('form.bill_store.thai_shipping_type'),
            'user_address_id'        => __('form.bill_store.thai_shipping_type'),
            'coupon'                 => __('form.bill_store.coupon'),
            'trackings'              => __('form.bill_store.tracking'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
