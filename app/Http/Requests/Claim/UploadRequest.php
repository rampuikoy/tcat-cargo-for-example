<?php

namespace App\Http\Requests\Claim;

use App\Http\Requests\FormRequest;
use App\Rules\Base64Image;

class UploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.claim.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file_path'     =>  ['required', new Base64Image],
        ];
    }
}
