<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->softDeletes();
            $table->string('title');
            $table->string('image')->nullable();
            $table->string('code')->nullable();
            $table->text('description')->nullable();
            $table->integer('stock')->default(0);
            $table->integer('point')->default(0);
            $table->integer('limit')->default(0);
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->text('remark')->nullable();
            
            $table->string('created_admin', 20)->nullable();
            $table->string('updated_admin', 20)->nullable();

            $table->integer('created_admin_id')->unsigned()->nullable();
            $table->integer('updated_admin_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
