<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
 */

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

Broadcast::channel('App.Models.User.{userCode}', function ($user, $userCode) {
    if (Auth::guard('admins')->check() || Auth::guard('users')->check() && $user->code === $userCode) {
        return [
            'id' => $user->id,
            'name' => $user->name,
            'image' => $user->image_url ?? $user->photo_url,
        ];
    }
    return false;
});

Broadcast::channel('App.Models.Chat.Broadcast', function ($user) {
    if (Auth::check()) {
        return [
            'id' => $user->id,
            'name' => $user->name,
            'image' => $user->image_url ?? $user->photo_url,
        ];
    }
    return false;
});

Broadcast::channel('App.Models.Chat.{userCode}', function ($user, $userCode) {
    if (Auth::guard('admins')->check() || Auth::guard('users')->check() && $user->code === $userCode) {
        return [
            'id' => $user->id,
            'name' => $user->name,
            'image' => $user->image_url ?? $user->photo_url,
        ];
    }
    return false;
});

Broadcast::channel('App.Models.Noti.Broadcast', function ($user) {
    if (Auth::check()) {
        return [
            'id' => $user->id,
            'name' => $user->name,
            'image' => $user->image_url ?? $user->photo_url,
        ];
    }
    return false;
});
