<!DOCTYPE html>
<html lang="th">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Trackings</title>
	{!! Html::style('public/css/pdf.css') !!}
	<link rel="stylesheet" href="https://res.imemo.co/tcatcargo_alpha/misc/css/pdf.css" media="all" />
</head>
<body>
	<div style="padding: 5px;">
		@foreach($bills as $bill)
		@include('admin.bill.download.partials.bill-tracking')
		@if($bills->last() != $bill)
		<div class="page-break"></div>
		@endif
		@endforeach
	</div>
</body>

</html>