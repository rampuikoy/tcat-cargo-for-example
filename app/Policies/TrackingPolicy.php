<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class TrackingPolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.tracking.index');
    }
    public function show(Admin $admin)
    {
        return $admin->hasAnyPermission(['backend.tracking.show', 'backend.tracking.update']);
    }

    public function store(Admin $admin)
    {
        return $admin->can('backend.tracking.store');
    }
    public function update(Admin $admin)
    {
        return $admin->can('backend.tracking.update');
    }

    public function destroy(Admin $admin)
    {
        return $admin->can('backend.tracking.destroy');
    }

    public function approve(Admin $admin)
    {
        return $admin->can('backend.tracking.approve');
    }

}
