<?php

namespace App\Models;

use App\Helpers\ScopeQueries;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    use HasFactory;

    protected $fillable = [
        'pointable_type',
        'pointable_id',
        'user_code',
        'user_id',
        'amount',
        'before',
        'after',
        'type',
        'created_admin',
        'updated_admin',
        'created_admin_id',
        'updated_admin_id',
    ];

    protected $appends = ['type_text', 'bill_no'];

    public static function typeList()
    {
        $array = [
            'topup'      => __('messages.points.type.topup'),
            'withdraw'   => __('messages.points.type.withdraw'),
        ];
        return $array;
    }

    public function getTypeTextAttribute()
    {
        return collect(self::typeList())->get($this->type);
    }

    public function getBillNoAttribute()
    {
        if ($this->pointable_type) {
            $instance = new $this->pointable_type;
            return $instance::PREFIX . str_pad($this->pointable_id, 6, "0", STR_PAD_LEFT);
        } else {
            return null;
        }
    }

    // relation
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_code', 'code');
    }

    // scope sql query
    public function scopeSearch($query, $conditions)
    {
        return $query->where(function ($query) use ($conditions) {
            ScopeQueries::scopeUserCodeBySale($query);

            if (@$conditions['type']) {
                $query->whereType($conditions['type']);
            }
            if (@$conditions['user_code']) {
                $query->where('user_code', 'like', '%' . $conditions['user_code'] . '%');
            }
            if (@$conditions['amount']) {
                $query->whereAmount($conditions['amount']);
            }

            $time_format = 'created_at';
            if (@$conditions['date_start'] && @$conditions['date_end']) {
                ScopeQueries::scopeDateRange($query, $time_format, $conditions['date_start'], $conditions['date_end']);
            } else if (@$conditions['date_start']) {
                ScopeQueries::scopeDateStart($query, $time_format, $conditions['date_start']);
            } else if (@$conditions['date_end']) {
                ScopeQueries::scopeDateEnd($query, $time_format, $conditions['date_end']);
            }
        })->whereRaw('date(created_at) >= "2020-01-01"');
    }
}
