<?php

namespace App\Repositories;

use App\Interfaces\ThaiShippingMethodInterface;
use App\Models\ThaiShippingMethod;
use App\Traits\CacheTrait;
use App\Traits\UploadTrait;
use Illuminate\Support\Str;
use App\Traits\CoreResponseTrait;
use Illuminate\Support\Facades\Cache;

class ThaiShippingMethodRepository implements ThaiShippingMethodInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, UploadTrait, CoreResponseTrait;

    const MINUTE_CACHE             = 1;
    public function __construct(ThaiShippingMethod $thaishippingmethod)
    {
        $this->thaishippingmethod        = $thaishippingmethod;
    }

    public function all(array $columns = ['*'], $sorting = ThaiShippingMethodInterface::DEFAULT_SORTING, $ascending = ThaiShippingMethodInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $thaishippings  = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
            return $this->thaishippingmethod::select($columns)->whereStatus('active')->orderBy($sorting, $ascending)->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.thai_shipping')]), $thaishippings);
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = ThaiShippingMethodInterface::DEFAULT_LIMIT, $sorting = ThaiShippingMethodInterface::DEFAULT_SORTING, $ascending = ThaiShippingMethodInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $thaishippings  = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
            return $this->thaishippingmethod::select($columns)
                ->search($conditions)
                ->orderBy($sorting, $ascending)
                ->skip($offset)
                ->limit($limit)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.thai_shipping')]), $thaishippings);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = ThaiShippingMethodInterface::DEFAULT_LIMIT, $sorting = ThaiShippingMethodInterface::DEFAULT_SORTING, $ascending = ThaiShippingMethodInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);
        $thaishippings  = Cache::tags($this->getClassPrefix(), $this->getPaginationWithClassPrefix())->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
            $model = $this->thaishippingmethod::select($columns)
                ->search($conditions)
                ->orderBy($sorting, $ascending);

            if ($page == 1) {
                return $model->paginate($limit);
            } else {
                return $model->skip($limit * ($page - 1))->paginate($limit);
            }
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.thai_shipping')]), $thaishippings);
    }

    public function findById($id, array $columns = ['*'])
    {
        $thaishippings = $this->findByIdMethod($id, $columns);

        if (!$thaishippings) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.thai_shipping'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.thai_shipping')]), $thaishippings);
    }

    public function findManyByType($type, array $columns = ['*'])
    {
        $key        = $this->getCacheKey('TYPE', strtoupper($type));
        $time       = $this->getTime(self::MINUTE_CACHE);

        $thaishippings  = Cache::tags([$this->getClassPrefix()])->remember($key, $time, function () use ($type, $columns) {
            return $this->thaishippingmethod::select($columns)->whereType($type)->get();
        });

        if (!$thaishippings) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.thai_shipping'), 'id' => $type]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.thai_shipping')]), $thaishippings);
    }

    public function findDroppointById($id, array $columns = ['*'])
    {
        $key        = $this->getCacheKey('DROPPOINT', $id);
        $time       = $this->getTime(self::MINUTE_CACHE);

        $thaishippings  = Cache::tags([$this->getClassPrefix()])->remember($key, $time, function () use ($id, $columns) {
            return $this->thaishippingmethod::select($columns)->whereIn('type', ['droppoint', 'delivery'])->whereId($id)->first();
        });

        if (!$thaishippings) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.thai_shipping'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.thai_shipping')]), $thaishippings);
    }

    public function dropdownList()
    {
        $key        = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $dropdown   = Cache::remember($key, $time, function () {
            return [
                'typeList'            => $this->thaishippingmethod::typeList(),
                'calculateTypeList'   => $this->thaishippingmethod::calculateTypeList(),
                'accessList'          => $this->thaishippingmethod::accessList(),
                'statusList'          => $this->thaishippingmethod::statusList()
            ];
        });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function postDroppoint()
    {
        $key        = $this->getCacheKey(null, $this->getDropPointClassPrefix());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $thaishippings   = Cache::remember($key, $time, function () {
            return $this->thaishippingmethod::whereType('droppoint')->where('status', 'active')->orderBy('title')->get();
        });


        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.thai_shipping')]), $thaishippings);
    }
    public function findByIdMethod($id, array $columns = ['*'])
    {
        $key        = $this->getCacheKey(null, $id);
        $time       = $this->getTime(self::MINUTE_CACHE);

        $thaishippings   = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($id)
        ])->remember($key, $time, function () use ($id, $columns) {
            return $this->thaishippingmethod::with('province', 'amphur', 'district')->select($columns)->whereId($id)->first();
        });

        return $thaishippings;
    }

    public function store(array $data)
    {
        if (@$data['image']) {
            $data['image'] = $this->storeFile($data['image']);
        }
        
        $thaishippingmethod   = $this->thaishippingmethod::create([
            'title_th'                  => $data['title'],
            'address'                   => $data['address'],
            'province_id'               => $data['province_id'],
            'amphur_id'                 => $data['amphur_id'],
            'district_code'             => $data['district_code'],
            'zipcode'                   => $data['zipcode'],
            'latitude'                  => $data['latitude'],
            'longitude'                 => $data['longitude'],
            'max_weight'                => $data['max_weight'], 
            'max_distance'              => $data['max_distance'],
            'charge'                    => $data['charge'],
            'shipping_rate_weight'      => $data['shipping_rate_weight'],
            'shipping_rate_cubic'       => $data['shipping_rate_cubic'], 
            'api'                       => $data['api'],
            'calculate_type'            => $data['calculate_type'],
            'type'                      => $data['type'],
            'status'                    => $data['status'],
            'image'                     => $data['image'],
            'refund'                    => $data['refund'],
            'access'                    => $data['access'],
            'tel'                       => $data['tel'],
            'detail'                    => $data['detail'],
            'remark'                    => $data['remark'],
            'min_price'                 => $data['min_price']
        ]);

        $this->clearCacheTags();
        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.thai_shipping')]), $thaishippingmethod);
    }

    public function updateById($id, array $data)
    {
        $thaishippingmethod       = $this->findByIdMethod($id);

        if (!$thaishippingmethod) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.thai_shipping'), 'id' => $id]));
        }

        if (@$data['image']) {
            $data['image'] = $this->storeFile($data['image']);
        } else {
            $data['image'] = $data['image'] ?? $thaishippingmethod->image;
        }

        $thaishippingmethod->update([
            'title_th'                  => $data['title'],
            'address'                   => $data['address'],
            'province_id'               => $data['province_id'],
            'amphur_id'                 => $data['amphur_id'],
            'district_code'             => $data['district_code'],
            'zipcode'                   => $data['zipcode'],
            'latitude'                  => $data['latitude'],
            'longitude'                 => $data['longitude'],
            'max_weight'                => $data['max_weight'],
            'max_distance'              => $data['max_distance'],
            'charge'                    => $data['charge'],
            'shipping_rate_weight'      => $data['shipping_rate_weight'],
            'shipping_rate_cubic'       => $data['shipping_rate_cubic'],
            'api'                       => $data['api'],
            'calculate_type'            => $data['calculate_type'],
            'type'                      => $data['type'],
            'status'                    => $data['status'],
            'image'                     => $data['image'],
            'refund'                    => $data['refund'],
            'access'                    => $data['access'],
            'tel'                       => $data['tel'],
            'detail'                    => $data['detail'],
            'remark'                    => $data['remark'],
            'min_price'                 => $data['min_price']
        ]);

        $this->clearCacheTags();
        $this->clearCacheTagById($id);
        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.thai_shipping')]), $thaishippingmethod);
    }

    public function storeFile($file)
    {
        if (!$file) {
            return null;
        }

        $chr = Str::random(40);
        $fileName = date('ymdHis') . '_' . $chr . '.' . $this->getExtensionFile($file);
        $this->uploadBase64($file, $this->thaishippingmethod::$imageFolder, $fileName);
        return $fileName;
    }

    public function softDeleteById($id)
    {
        $thaishippingmethod       = $this->findByIdMethod($id);

        if (!$thaishippingmethod) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.thai_shipping'), 'id' => $id]));
        }

        $thaishippingmethod->delete();
        $this->clearCacheTags();
        $this->clearCacheTagById($id);
        return $this->coreResponse(200, __('messages.delete_data', ['data' => __('messages.thai_shipping')]), $thaishippingmethod);
    }

    public function clearCacheTags()
    {
        $keys = [
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix(),
            $this->getDropPointClassPrefix()
        ];

        Cache::tags($keys)->flush();
    }

    public function clearCacheTagById($id)
    {
        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();
    }
}
