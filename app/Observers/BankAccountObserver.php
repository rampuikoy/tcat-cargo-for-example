<?php

namespace App\Observers;

use App\Models\BankAccount;
use App\Observers\BaseObserver;

class BankAccountObserver extends BaseObserver
{
    /**
     * Handle the bank_account "creating" event.
     *
     * @param  \App\Models\BankAccount  $bank_account
     * @return void
     */
    public function creating(BankAccount $bank_account)
    {
        //
        $this->stampCreated($bank_account);
    }

    /**
     * Handle the bank_account "updating" event.
     *
   * @param  \App\Models\BankAccount  $bank_account
     * @return void
     */
    public function updating(BankAccount $bank_account)
    {
        //
        $this->stampUpdated($bank_account);
    }

}
