<?php

namespace App\Observers;

use App\Models\Role;
use App\Observers\BaseObserver;

class RoleObserver extends BaseObserver
{

    /**
     * Handle the admin "creating" event.
     *
     * @param  \App\Models\Role  $admin
     * @return void
     */
    public function creating(Role $admin)
    {
        //
        $this->stampCreated($admin);
    }

    /**
     * Handle the admin "updating" event.
     *
     * @param  \App\Models\Role  $admin
     * @return void
     */
    public function updating(Role $admin)
    {
        //
        $this->stampUpdated($admin);
    }
}
