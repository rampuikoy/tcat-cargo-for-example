<?php

namespace App\Interfaces;

interface WarehouseInterface
{
    const DEFAULT_LIMIT = 25;
    const DEFAULT_SORTING = 'id';
    const DEFAULT_ASCENDING = 'desc';

    /**
     * fetch all warehouse
     *
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function all(array $columns = ['*'], $sorting = WarehouseInterface::DEFAULT_SORTING, $ascending = WarehouseInterface::DEFAULT_ASCENDING);
}
