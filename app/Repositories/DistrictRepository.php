<?php

namespace App\Repositories;

use App\Interfaces\DistrictInterface;
use App\Models\District;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use Illuminate\Support\Facades\Cache;

class DistrictRepository implements DistrictInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const MINUTE_CACHE = 1;

    public function __construct(District $district)
    {
        $this->district = $district;
    }

    public function all(array $columns = ['*'], $sorting = DistrictInterface::DEFAULT_SORTING, $ascending = DistrictInterface::DEFAULT_ASCENDING)
    {
        $key            = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time           = $this->getTime(self::MINUTE_CACHE);

        $allDistrict    = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
                            return $this->district::select($columns)->orderBy($sorting, $ascending)->get();
                        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.district')]), $allDistrict);
    }

    public function findManyByAmphurId($id, array $columns = ['*'])
    {
        $key            = $this->getCacheKey(null, $id);
        $time           = $this->getTime(self::MINUTE_CACHE);

        $districtList   = Cache::tags([
                            $this->getClassPrefix(),
                            $this->getFindByIdWithClassPrefix(),
                            $this->getIdWithClassPrefix($id)
                        ])->remember($key, $time, function () use ($id, $columns) {
                            return $this->district::select($columns)
                                ->where('amphur_id', $id)
                                ->get();
                        });

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.district')]), $districtList);
    }

    public function findById($id, array $columns = ['*'])
    {
        $district = $this->findByIdMethod($id, $columns)->first();
        if (!$district) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.district'), 'id' => $id]));
        }
        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.district')]), $district);
    }

    private function findByIdMethod($id, array $columns = ['*'])
    {
        $key        = $this->getCacheKey(null, $id);
        $time       = $this->getTime(self::MINUTE_CACHE);

        $district    = Cache::tags([
            $this->getClassPrefix(),
            $this->getIdWithClassPrefix($id)
        ])->remember($key, $time, function () use ($id, $columns) {
            return $this->district::select($columns)->whereId($id)->first();
        });

        return $district;
    }
}
