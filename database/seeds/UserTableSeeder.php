<?php

use App\Helpers\MoveSqlData;
use Illuminate\Database\Seeder;
use App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $move_geo_data    = collect(MoveSqlData::insertUser());
        $message          = $move_geo_data->get('message');
        $error            = $move_geo_data->get('error');

        if($error){
            $this->command->error($message);
            $this->command->warning('Please insert fresh again');
            return;
        }

        factory(User::class, 10)->create();

        $this->command->info($message);
    }
}
