<?php

namespace App\Http\Requests\User;

use App\Http\Requests\FormRequest;
use App\Rules\Phone;

class UpdateUserRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return $this->user('admins')->can('backend.user.update');
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    $id =  $this->route('user');
    return [
      'name'                      => 'required|string',
      'email'                     => 'required|unique:users,email,' . $id,

      'tel1'                      => ['sometimes', 'nullable', new Phone],
      'tel2'                      => ['sometimes', 'nullable', new Phone],

      'status'                    => 'required|in:active,inactive',
      'tax_id'                    => 'sometimes|nullable|min:10|max:20|regex:/^[0-9]*$/',

      'province_id'               => 'sometimes|nullable|exists:provinces,id',
      'admin_ref_id'              => 'sometimes|nullable|exists:admins,id',
    ];
  }
}
