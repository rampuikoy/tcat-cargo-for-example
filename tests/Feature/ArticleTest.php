<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Admin;
use App\Models\Article;
use Illuminate\Support\Facades\File;
use Tymon\JWTAuth\Facades\JWTAuth;

class ArticleTest extends TestCase
{
  use WithFaker;
  protected $user;
  protected $article;
  protected $url = '/api/backend/article';
  /**
   * A basic feature test example.
   *
   * @return void
   */
  protected function authenticate(string $guard = 'admins')
  {
    $this->user = factory(Admin::class)->create();
    auth()->shouldUse($guard);
    $this->article = factory(Article::class)->create();
    $token = JWTAuth::fromUser($this->user);
    return 'Bearer ' . $token;
  }

  protected function deleteUser()
  {
    Article::find($this->article->id)->forceDelete();
    return $this->user->forceDelete();
  }

  /** @test */
  public function GetArticleAllSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', $this->url);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function GetArticleAllFailTest()
  {
    $response = $this->json('GET', $this->url);
    $response->assertStatus(401);
  }

  /** @test */
  public function GetArticleSearchSuccessTest()
  {
    $id = random_int(1, 200);
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', "{$this->url}/search", [
      "id" => $id,
      "type" => "",
      "search" => "",
    ]);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function GetArticleSearchFailTest()
  {
    $response = $this->json('GET', "{$this->url}/search");
    $response->assertStatus(401);
  }

  /** @test */
  public function GetArticleDropdownSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', "{$this->url}/dropdown");
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function GetArticleDropdownFailTest()
  {
    $response = $this->json('GET', "{$this->url}/dropdown");
    $response->assertStatus(401);
  }

  /** @test */
  public function GetArticleByIdSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', "{$this->url}/2");
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function GetArticleByIdFailTest()
  {
    $response = $this->json('GET', "{$this->url}/2");
    $response->assertStatus(401);
  }

  /** @test */
  public function ArticlePostSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('POST', $this->url, [
      'status'         => 'active',
      'type'           => 'news',
      'title'          => 'test-test',
      'content'        => 'test-testtest-testtest-test',
      'file'           => File::get(public_path('images/test/testbase64.txt')),
    ]);
    $response->assertStatus(200);
    Article::where('title', 'test-test')->forceDelete();
    $this->deleteUser();
  }

  /** @test */
  public function ArticlePostFailAuthTest()
  {
    $response = $this->json('POST', $this->url);
    $response->assertStatus(401);
  }

  /** @test */
  public function ArticlePostFailTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('POST', $this->url);
    $response->assertStatus(422);
    $this->deleteUser();
  }

  /** @test */
  public function ArticUpdateByIdSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('PUT', "$this->url/{$this->article->id}", [
      'status'         => 'active',
      'type'           => 'news',
      'title'          => 'test-test',
      'content'        => 'test-testtest-testtest-test',
    ]);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function ArticUpdateByIdFailAuthTest()
  {
    $response = $this->json('PUT', "{$this->url}/2");
    $response->assertStatus(401);
  }

  public function ArticUpdateByIdFailTest()
  {
    //TODO ArticUpdate Not have Vaildate
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' =>  $token,
    ])->json('PUT', "{$this->url}/{$this->article->id}");
    $response->assertStatus(422);
    $this->deleteUser();
  }

  /** @test */
  public function ArticleDeleteSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('DELETE', "{$this->url}/{$this->article->id}");
    $response->assertStatus(200);
    $this->article->forceDelete();
    $this->user->forceDelete();
  }

  /** @test */
  public function ArticleDeleteFailTest()
  {
    $response = $this->json('DELETE', "{$this->url}/5");
    $response->assertStatus(401);
  }
}
