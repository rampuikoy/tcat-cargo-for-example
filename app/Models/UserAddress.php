<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    use softDeletes;

    protected $table = 'user_addresses';

    protected $fillable = [
        'name',
        'tel',
        'address',
        'province_id',
        'amphur_id',
        'district_code',
        'zipcode',
        'user_code',
        'user_remark',
        'admin_remark',
        'status',
        'latitude',
        'longitude',
        'crated_admin_id',
        'updated_admin_id',
        'created_admin',
        'updated_admin',
    ];

    protected $appends = ['status_text'];

    public static function statusList()
    {
        $array = [
            'active'    => __('messages.active'),
            'inactive'  => __('messages.inactive'),
        ];
        return $array;
    }
    
    /**
     * Get the name for enum columns
     *
     * @return string
     */
    public function getStatusTextAttribute()
    {
        return collect(self::statusList())->get($this->status);
    }

    /**
     * relation
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_code', 'code');
    }
    public function province()
    {
        return $this->belongsTo('App\Models\Province', 'province_id');
    }
    public function amphur()
    {
        return $this->belongsTo('App\Models\Amphur', 'amphur_id');
    }
    public function district()
    {
        return $this->belongsTo('App\Models\District', 'district_code', 'code');
    }

    /**
     * scope
     */
    public function scopeSearch($query, $conditions)
    {
        return $query->where(function ($query) use ($conditions) {
            if (@$conditions['id']) {
                $query->where('id', $conditions['id']);
            }
            if (@$conditions['user_code']) {
                $query->where('user_code', 'LIKE', "%{$conditions['user_code']}%");
            }
            if (@$conditions['name']) {
                $query->where('name', 'LIKE', "%{$conditions['name']}%");
            }
            if (@$conditions['tel']) {
                $query->where('tel', 'LIKE', "%{$conditions['tel']}%");
            }
            if (@$conditions['address']) {
                $query->where('address', 'LIKE', "%{$conditions['address']}%");
            }
            if (@$conditions['province_id']) {
                $query->where('province_id', $conditions['province_id']);
            }
            if (@$conditions['zipcode']) {
                $query->where('zipcode', 'LIKE', "%{$conditions['zipcode']}%");
            }
            if (@$conditions['status']) {
                $query->where('status', '=', $conditions['status']);
            }
            if (@$conditions['user_remark']) {
                $query->where('user_remark', 'LIKE', "%{$conditions['user_remark']}%");
            }
            if (@$conditions['admin_remark']) {
                $query->where('admin_remark', 'LIKE', "%{$conditions['admin_remark']}%");
            }
        });
    }
}
