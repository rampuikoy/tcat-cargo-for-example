<?php

namespace Database\Seeders;

use App\Models\NoCode;
use Illuminate\Database\Seeder;

class NoCodeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        NoCode::factory(70)->create()->each(function($nocode) {
            $nocode->images()->create([
                'type'              => 'image',
                'file_path'         => 'product-image-dummy.jpg',
                'created_admin_id'  => $nocode->taken_admin_id,
                'created_admin'     => $nocode->taken_admin,
            ]);
        });
    }
}
