<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class NotificationPolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.noti.index');
    }

    public function update(Admin $admin)
    {
        return $admin->can('backend.noti.update');
    }

}
