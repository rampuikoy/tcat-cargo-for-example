<?php

namespace App\Http\Controllers\Backend\Notification;

use App\Http\Controllers\Controller;
use App\Http\Requests\NotiQueue\UpdateManyQueueRequest;
use App\Interfaces\EmailQueueInterface;
use App\Policies\NotificationPolicy;
use Illuminate\Http\Request;

class EmailQueueController extends Controller
{

    protected $emailQueueInterface;

    public function __construct(EmailQueueInterface $emailQueueInterface)
    {
        $this->emailQueueInterface = $emailQueueInterface;
    }

    public function search(Request $request)
    {
        $this->authorize('index', NotificationPolicy::class);
        $conditions     = $request->only(['id', 'email', 'status', 'type', 'date_start', 'date_end']);
        $columns        = $request->input('columns', ['*']);
        $limit          = $request->input('limit', $this->emailQueueInterface::DEFAULT_LIMIT);
        $sorting        = $request->input('order_by', $this->emailQueueInterface::DEFAULT_SORTING);
        $ascending      = $request->input('ascending') == 1 ? 'asc' : 'desc';
        $offset         = $request->input('page', 0);

        if ($request->boolean('pagination')) {
            $page           = $request->input('page', 1);
            $emailQueue     = $this->emailQueueInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset         = $request->input('page', 0);
            $emailQueue     = $this->emailQueueInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($emailQueue, $emailQueue->code);
    }

    public function dropdown()
    {
        $dropdown = $this->emailQueueInterface->dropdownList();
        return response()->json($dropdown, $dropdown->code);
    }

    public function updateManyQueue(UpdateManyQueueRequest $request)
    {
        $data           = $request->input('ids', []);
        $emailQueue     = $this->emailQueueInterface->updateManyQueue($data);
        return response()->json($emailQueue, $emailQueue->code);
    }
}
