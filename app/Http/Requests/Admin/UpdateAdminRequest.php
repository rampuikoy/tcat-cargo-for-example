<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\FormRequest;
use App\Rules\Base64Image;

class UpdateAdminRequest extends FormRequest
{
    public function authorize()
    {
        return $this->user('admins')->can('backend.admin.update');
    }

    public function rules()
    {
        $id = $this->route('admin');
        return [
            'name'          => 'required|max:191',
            'lastname'      => 'sometimes|required',
            'email'         => 'required|email|unique:admins,email,'.$id,
            'photo'         => ['nullable', new Base64Image],
            'role'          => 'required|exists:roles,name',
            
            'tel'           => 'sometimes|nullable|string|max:20',
            'department'    => 'required|in:tdar,cargo,taobao,tmall,alibaba,express,china',
            'droppoint_id'  => 'sometimes|nullable|exists:thai_shipping_methods,id',
            'branch'        => 'required|in:headquarter,branch',
            'type'          => 'required|in:admin,staff,accountant,warehouse,sale,driver',
            'status'        => 'required|in:active,inactive',
            'regex_user'    => 'sometimes|in:active,inactive',
            'pattern_user'  => 'required_if:regex_user,active|max:20',
            'remark'        => 'sometimes|nullable|string',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name'          => __('form.admin.name'),
            'lastname'      => __('form.admin.lastname'),
            'email'         => __('form.admin.email'),
            'role'          => __('form.admin.role'),
            'photo'         => __('form.admin.photo'),
            'tel'           => __('form.admin.phone'),
            'department'    => __('form.admin.department'),
            'droppoint_id'  => __('form.admin.droppoint_id'),
            'branch'        => __('form.admin.branch'),
            'type'          => __('form.admin.type'),
            'status'        => __('form.admin.status'),
            'regex_user'    => __('form.admin.regex_user'),
            'pattern_user'  => __('form.admin.pattern_user'),
            'remark'        => __('form.admin.remark'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
