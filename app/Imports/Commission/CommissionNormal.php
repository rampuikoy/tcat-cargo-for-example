<?php

namespace App\Imports\Commission;

use App\Models\Bill;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class CommissionNormal implements ToCollection
{
    protected $commission;

    public function collection(Collection $collection)
    {
        $this->commission    = $collection;
    }

    public function getBills()
    {
        return collect($this->commission)->skip(11)
                            ->map(function ($bill) {
                                return [
                                    'index'                 => $bill[0],
                                    'user_code'             => $bill[1],
                                    'no'                    => $bill[2],
                                    'bill_id'               => ltrim($bill['2'], Bill::PREFIX.'0'),
                                    'user_expense'          => $bill[3] ?? 0,
                                    'company_revenue'       => $bill[4] ?? 0,
                                    'sum_price_brand'       => $bill[5] ?? 0,
                                    'sale_revenue'          => $bill[6] ?? 0,
                                    'user_shipping_rate'    => $this->mappingUserShippingRate($bill),
                                    'company_shipping_rate' => $this->mappingCompanyShippingRate($bill),
                                ];
                            })
                            ->values();
    }

    private function mappingUserShippingRate($bill)
    {
        return [
            // Car Genaral KG
            'sum_unit_kg_car_genaral'       => $bill[7] ?? 0,
            'rate_kg_car_genaral'           => $bill[8] ?? 0,
            'sum_price_kg_car_genaral'      => $bill[9] ?? 0,
            // Car Genaral CBM
            'sum_unit_cubic_car_genaral'    => $bill[10] ?? 0,
            'rate_cubic_car_genaral'        => $bill[11] ?? 0,
            'sum_price_cubic_car_genaral'   => $bill[12] ?? 0,
            // Car Iso KG
            'sum_unit_kg_car_iso'           => $bill[13] ?? 0,
            'rate_kg_car_iso'               => $bill[14] ?? 0,
            'sum_price_kg_car_iso'          => $bill[15] ?? 0,
            // Car Iso CBM
            'sum_unit_cubic_car_iso'        => $bill[16] ?? 0,
            'rate_cubic_car_iso'            => $bill[17] ?? 0,
            'sum_price_cubic_car_iso'       => $bill[18] ?? 0,
            // Car Total
            'car_total'                     => $bill[19] ?? 0,
            // Ship Genaral KG
            'sum_unit_kg_ship_genaral'      => $bill[20] ?? 0,
            'rate_kg_ship_genaral'          => $bill[21] ?? 0,
            'sum_price_kg_ship_genaral'     => $bill[22] ?? 0,
            // Ship Genaral CBM
            'sum_unit_cubic_ship_genaral'   => $bill[23] ?? 0,
            'rate_cubic_ship_genaral'       => $bill[24] ?? 0,
            'sum_price_cubic_ship_genaral'  => $bill[25] ?? 0,
            // Ship Iso KG
            'sum_unit_kg_ship_iso'          => $bill[26] ?? 0,
            'rate_kg_ship_iso'              => $bill[27] ?? 0,
            'sum_price_kg_ship_iso'         => $bill[28] ?? 0,
            // Ship Iso CBM
            'sum_unit_cubic_ship_iso'       => $bill[29] ?? 0,
            'rate_cubic_ship_iso'           => $bill[30] ?? 0,
            'sum_price_cubic_ship_iso'      => $bill[31] ?? 0,
            // Ship Total
            'ship_total'                    => $bill[32] ?? 0,
            // Bill Total
            'bill_total'                    => $bill[33] ?? 0,
        ];
    }

    private function mappingCompanyShippingRate($bill)
    {
        return [
            // Car Genaral KG
            'sum_unit_kg_car_genaral'       => $bill[35] ?? 0,
            'rate_kg_car_genaral'           => $bill[36] ?? 0,
            'sum_price_kg_car_genaral'      => $bill[37] ?? 0,
            // Car Genaral CBM
            'sum_unit_cubic_car_genaral'    => $bill[38] ?? 0,
            'rate_cubic_car_genaral'        => $bill[39] ?? 0,
            'sum_price_cubic_car_genaral'   => $bill[40] ?? 0,
            // Car Iso KG
            'sum_unit_kg_car_iso'           => $bill[41] ?? 0,
            'rate_kg_car_iso'               => $bill[42] ?? 0,
            'sum_price_kg_car_iso'          => $bill[43] ?? 0,
            // Car Iso CBM
            'sum_unit_cubic_car_iso'        => $bill[44] ?? 0,
            'rate_cubic_car_iso'            => $bill[45] ?? 0,
            'sum_price_cubic_car_iso'       => $bill[46] ?? 0,
            // Car Total
            'car_total'                     => $bill[47] ?? 0,
            // Ship Genaral KG
            'sum_unit_kg_ship_genaral'      => $bill[48] ?? 0,
            'rate_kg_ship_genaral'          => $bill[49] ?? 0,
            'sum_price_kg_ship_genaral'     => $bill[50] ?? 0,
            // Ship Genaral CBM
            'sum_unit_cubic_ship_genaral'   => $bill[51] ?? 0,
            'rate_cubic_ship_genaral'       => $bill[52] ?? 0,
            'sum_price_cubic_ship_genaral'  => $bill[53] ?? 0,
            // Ship Iso KG
            'sum_unit_kg_ship_iso'          => $bill[54] ?? 0,
            'rate_kg_ship_iso'              => $bill[55] ?? 0,
            'sum_price_kg_ship_iso'         => $bill[56] ?? 0,
            // Ship Iso CBM
            'sum_unit_cubic_ship_iso'       => $bill[57] ?? 0,
            'rate_cubic_ship_iso'           => $bill[58] ?? 0,
            'sum_price_cubic_ship_iso'      => $bill[59] ?? 0,
            // Ship Total
            'ship_total'                    => $bill[60] ?? 0,
            // Bill Total
            'bill_total'                    => $bill[61] ?? 0,
        ];
    }

    public function getSaleName()
    {
        return $this->commission[2][0] ?? null;
    }

    public function getDateStart()
    {
        return $this->commission[3][16] ?? null;
    }

    public function getDateEnd()
    {
        return $this->commission[3][19] ?? null;
    }

    public function getProfit()
    {
        return [
            'total_user_expense'        => $this->commission[3][3] ?? 0,
            'total_company_revenue'     => $this->commission[3][4] ?? 0,
            'total_sale_revenue'        => $this->commission[3][6] ?? 0,
        ];
    }
}