<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;

HeadingRowFormatter::extend('mapping-key', function ($value) {
    switch ($value) {
        case 'ชื่อลูกค้า':
            return 'name';
        case 'อีเมล์':
            return 'email';
        case 'เบอร์โทร':
            return 'tel1';
        case 'เบอร์โทร(สำรอง)':
            return 'tel2';
        case 'วิธีการจัดส่งในประเทศ':
            return 'thai_shipping_method';
        case 'จังหวัด':
            return 'province';
        case 'หมายเหตุ(เจ้าหน้าที่)':
            return 'admin_remark';
        default:
            return;
    }
});

HeadingRowFormatter::default('mapping-key');

class UserImport implements ToCollection, WithValidation, WithBatchInserts, WithChunkReading, WithHeadingRow
{
    use Importable;

    protected $users;

    public function collection(Collection $collection)
    {
      $province             = app('App\Repositories\ProvinceRepository')->all(['id', 'title_th']);
      $thaiShippingMethod   = app('App\Repositories\ThaiShippingMethodRepository')->all(['id', 'title_th']); 
      $this->users          = $collection->map(function ($item) use ($province, $thaiShippingMethod) {
                                  return [
                                      'name'                    => $item['name'],
                                      'email'                   => $item['email'],
                                      'tel1'                    => $item['tel1'],
                                      'tel2'                    => $item['tel2'],
                                      'province_id'             => collect($province)->firstWhere('title_th', $item['province'])->title_th ?? null,
                                      'thai_shipping_method_id' => collect($thaiShippingMethod)->firstWhere('title_th', $item['thai_shipping_method'])->title_th ?? null,
                                  ];
                              });
    }

    public function rules(): array
    {
        return [
            'name'    => 'required',
            'email'   => 'required',
        ];
    }

    public function batchSize(): int
    {
        return 100;
    }

    public function chunkSize(): int
    {
        return 100;
    }

    public function headingRow(): int
    {
        return 1;
    }

    public function getUsers()
    {
        return collect($this->users)
                ->unique('email')
                ->values();
    }
}
