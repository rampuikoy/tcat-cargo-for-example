<?php

namespace App\Observers;

use App\Models\Bill;
use App\Models\Point;
use App\Models\Setting;
use App\Jobs\JobSendFcm;
use App\Jobs\JobSendSms;
use App\Events\NotiEevent;
use App\Jobs\JobSendEmail;
use App\Models\PackingQueue;
use App\Observers\BaseObserver;

class BillObserver extends BaseObserver
{
    //
    public function creating(Bill $bill)
    {
        $bill->total_price  = ($bill->total_price > 0) ? round($bill->total_price, 2) : 0;
        $bill->sub_total    = round($bill->sub_total, 2);
        $user               = $bill->user;
        if ($user->admin_ref_id) {
            $bill->commission_admin_id = $user->admin_ref_id;
        }
        $this->stampCreated($bill);
    }

    public function created()
    {
        broadcast(new NotiEevent())->toOthers();
    }
    public function updating(Bill $bill)
    {
        $this->stampUpdated($bill);
        if ($bill->isDirty('status')) {

            $user       = $bill->user;
            $total      = number_format($bill->total_price, 2);
            if ($bill->status == 2) {  //send created noti Status 2

                $sms        = $bill->sms()->create([
                    'status'            => 'queue',
                    'user_code'         => $user->code,
                    'msisdn'            => $user->tel1,
                    'message'           => __('sms.sms_bill_created', ['no' => $bill->no, 'total' => $total]),
                    'created_admin'     => $bill->created_admin,
                    'created_admin_id'  => $bill->created_admin_id,
                ]);
                JobSendSms::dispatch($sms->id)->onQueue(env('QUEUE_SMS_PREFIX'));


                $email      = $bill->emails()->create([
                    'mailable_class'    => 'App\Mail\Bill\BillCreated',
                    'status'            => 'queue',
                    'to'                => $user->email,
                    'created_admin'     => $bill->created_admin,
                    'created_admin_id'  => $bill->created_admin_id,
                ]);
                JobSendEmail::dispatch($email->id)->onQueue(env('QUEUE_EMAIL_PREFIX'));

                $fcm        = $bill->fcm()->create([
                    'status'            => 'queue',
                    'user_code'         => $bill->user_code,
                    'title'             => __('fcm.bill.bill_new_title', ['no' => $bill->no]),
                    'message'           => __('fcm.bill.bill_new_message', [
                        'no'          => $bill->no,
                        'total'       => $total
                    ]),
                ]);
                JobSendFcm::dispatch($fcm->id)->onQueue(env('QUEUE_FCM_PREFIX'));
            } elseif ($bill->status == 3) {  //send created noti  Status 3 จ่ายเงินเเล้ว
                if ($bill->getOriginal('status') < 3) {

                    $email      = $bill->emails()->create([
                        'mailable_class'    => 'App\Mail\Bill\BillPayment',
                        'status'            => 'queue',
                        'to'                => $user->email,
                        'created_admin'     => $bill->created_admin,
                        'created_admin_id'  => $bill->created_admin_id,
                    ]);
                    JobSendEmail::dispatch($email->id)->onQueue(env('QUEUE_EMAIL_PREFIX'));


                    $sms        = $bill->sms()->create([
                        'status'            => 'queue',
                        'user_code'         => $user->code,
                        'msisdn'            => $user->tel1,
                        'message'           => __('sms.sms_bill_paid', ['no' => $bill->no]),
                        'created_admin'     => $bill->created_admin,
                        'created_admin_id'  => $bill->created_admin_id,
                    ]);
                    JobSendSms::dispatch($sms->id)->onQueue(env('QUEUE_SMS_PREFIX'));


                    $fcm        = $bill->fcm()->create([
                        'status'            => 'queue',
                        'user_code'         => $bill->user_code,
                        'title'             => __('fcm.bill.bill_paid_title', ['no' => $bill->no]),
                        'message'           => __('fcm.bill.bill_paid_message', ['no' => $bill->no,]),
                    ]);
                    JobSendFcm::dispatch($fcm->id)->onQueue(env('QUEUE_FCM_PREFIX'));

                    /** check if all tracking packed auto set status = 4 =>ถ้ายิง pack หมด เปลี่ยสถานะ*/
                    $count_unpacked = $bill->trackings()->whereNull('packed_at')->get()->count();
                    if ($count_unpacked == 0) {
                        $bill->status = 4;
                        $queue = PackingQueue::where('bill_id', $bill->id)->first();
                        if ($queue) {
                            $queue->fill(['status' => 'shipping'])->save();
                        }
                    }
                } else {
                    /** bill backward, do nothing, set tracking status = 4 */
                    $bill->trackings()->update(['status' => 4]);
                }
            }
            elseif ($bill->status == 4) { // Status 4
                $queue = PackingQueue::where('bill_id', $bill->id)->first();
                if ($queue) {
                    $queue->fill(['status' => 'shipping'])->save();
                }
            } elseif ($bill->status == 5) { // Status 5
                $queue = PackingQueue::where('bill_id', $bill->id)->first();
                if ($queue) {
                    $queue->fill(['status' => 'success'])->save();
                }
                $fcm        = $bill->fcm()->create([
                    'status'            => 'queue',
                    'user_code'         => $bill->user_code,
                    'title'             => __('fcm.bill.bill_shipping_title', ['no' => $bill->no]),
                    'message'           => __('fcm.bill.bill_shipping_message', ['no' => $bill->no,]),
                ]);
                JobSendFcm::dispatch($fcm->id)->onQueue(env('QUEUE_FCM_PREFIX'));
            } elseif ($bill->status == 6) {  //send created noti Status 6
                $queue = PackingQueue::where('bill_id', $bill->id)->first();
                if ($queue) {
                    $queue->fill(['status' => 'success'])->save();
                }

                $email      = $bill->emails()->create([
                    'mailable_class'    => 'App\Mail\Bill\BillShipped',
                    'status'            => 'queue',
                    'to'                => $user->email,
                    'created_admin'     => $bill->created_admin,
                    'created_admin_id'  => $bill->created_admin_id,
                ]);
                JobSendEmail::dispatch($email->id)->onQueue(env('QUEUE_EMAIL_PREFIX'));

                $method = $bill->thaiShippingMethod->title;

                $sms        = $bill->sms()->create([
                    'status'            => 'queue',
                    'user_code'         => $user->code,
                    'msisdn'            => $user->tel1,
                    'message'           => __('sms.sms_bill_shipped', [
                        'no'     => $bill->no,
                        'method' => $method
                    ]),
                    'created_admin'     => $bill->created_admin,
                    'created_admin_id'  => $bill->created_admin_id,
                ]);
                JobSendSms::dispatch($sms->id)->onQueue(env('QUEUE_SMS_PREFIX'));

                $fcm        = $bill->fcm()->create([
                    'status'            => 'queue',
                    'user_code'         => $bill->user_code,
                    'title'             => __('fcm.bill.bill_shipped_title', ['no' => $bill->no]),
                    'message'           => __('fcm.bill.bill_shipped_message', ['no' => $bill->no,]),
                ]);
                JobSendFcm::dispatch($fcm->id)->onQueue(env('QUEUE_FCM_PREFIX'));
            } elseif ($bill->status == 7) {  // Status 7

                $email      = $bill->emails()->create([
                    'mailable_class'    => 'App\Mail\Bill\BillSuccess',
                    'status'            => 'queue',
                    'to'                => $user->email,
                    'created_admin'     => $bill->created_admin,
                    'created_admin_id'  => $bill->created_admin_id,
                ]);
                JobSendEmail::dispatch($email->id)->onQueue(env('QUEUE_EMAIL_PREFIX'));

                $fcm        = $bill->fcm()->create([
                    'status'            => 'queue',
                    'user_code'         => $bill->user_code,
                    'title'             => __('fcm.bill.bill_success_title', ['no' => $bill->no]),
                    'message'           => __('fcm.bill.bill_success_message', ['no' => $bill->no,]),
                ]);
                JobSendFcm::dispatch($fcm->id)->onQueue(env('QUEUE_FCM_PREFIX'));

            } elseif ($bill->status == 8) {  //send created noti // Status8

                $queue = PackingQueue::where('bill_id', $bill->id)->first();
                if ($queue) {
                    $queue->delete();
                }

                if ($bill->getOriginal('status') > 1) {

                        $email      = $bill->emails()->create([
                            'mailable_class'    => 'App\Mail\Bill\BillCancel',
                            'status'            => 'queue',
                            'to'                => $user->email,
                            'created_admin'     => $bill->created_admin,
                            'created_admin_id'  => $bill->created_admin_id,
                        ]);
                        JobSendEmail::dispatch($email->id)->onQueue(env('QUEUE_EMAIL_PREFIX'));

                    $sms        = $bill->sms()->create([
                        'status'            => 'queue',
                        'user_code'         => $user->code,
                        'msisdn'            => $user->tel1,
                        'message'           => __('sms.sms_bill_cancel', ['no' => $bill->no,]),
                        'created_admin'     => $bill->created_admin,
                        'created_admin_id'  => $bill->created_admin_id,
                    ]);
                    JobSendSms::dispatch($sms->id)->onQueue(env('QUEUE_SMS_PREFIX'));

                    $fcm        = $bill->fcm()->create([
                        'status'            => 'queue',
                        'user_code'         => $bill->user_code,
                        'title'             => __('fcm.bill.bill_cancel_title', ['no' => $bill->no]),
                        'message'           => __('fcm.bill.bill_cancel_message', [
                                                    'no' => $bill->no,
                                                    'remark' => $bill->user_remark,
                                            ]),
                    ]);
                    JobSendFcm::dispatch($fcm->id)->onQueue(env('QUEUE_FCM_PREFIX'));
                }

                $bill->payments()->whereStatus('waiting')->update(['status' => 'fail']);
            }
        }
    }
}
