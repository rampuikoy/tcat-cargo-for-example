<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrackingDetail extends Model
{
    use HasFactory;

    protected $table = 'tracking_details';

    protected $fillable = [
        'product_type',
        'product_remark',
        'qty',
        'tracking_id',
        'created_admin',
        'updated_admin',
        'created_admin_id',
        'updated_admin_id'
    ];

    protected $appends = ['product_type_text'];

    public static function productTypeList()
    {
        $array = [
            'clothing'      => __('messages.trackings_detail.clothing'),
            'shoes'         => __('messages.trackings_detail.shoes'),
            'bag'           => __('messages.trackings_detail.bag'),
            'accessories'   => __('messages.trackings_detail.accessories'),
            'electrical'    => __('messages.trackings_detail.electrical'),
            'it'            => __('messages.trackings_detail.it'),
            'car'           => __('messages.trackings_detail.car'),
            'home'          => __('messages.trackings_detail.home'),
            'office'        => __('messages.trackings_detail.office'),
            'stationery'    => __('messages.trackings_detail.stationery'),
            'handyman_tool' => __('messages.trackings_detail.handyman_tool'),
            'pet'           => __('messages.trackings_detail.pet'),
            'toy'           => __('messages.trackings_detail.toy'),
            'furniture'     => __('messages.trackings_detail.furniture'),
            'food_and_drug' => __('messages.trackings_detail.food_and_drug'),
            'cosmetics'     => __('messages.trackings_detail.cosmetics'),
            'other'         => __('messages.trackings_detail.other'),
        ];
        return $array;
    }

    /**
     * relation
     */
    public function tracking()
    {
        return $this->belongsTo('App\Models\Tracking', 'tracking_id');
    }

    public function getProductTypeTextAttribute()
    {
        return ($this->product_type) ? collect(self::productTypeList())->get($this->product_type) : null;
    }
}
