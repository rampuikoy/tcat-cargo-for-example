<?php

namespace App\Interfaces;

interface FcmQueueInterface
{
    const DEFAULT_LIMIT = 25;
    const DEFAULT_SORTING = 'id';
    const DEFAULT_ASCENDING = 'desc';

    /**
     * fetch all fcmqueue
     *
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function all(array $columns = ['*'], $sorting = FcmQueueInterface::DEFAULT_SORTING, $ascending = FcmQueueInterface::DEFAULT_ASCENDING);

    /**
     * list fcmqueue
     *
     * @param array $conditions
     * @param array $columns
     * @param int $offset
     * @param int $limit
     *
     * @return \Illuminate\Support\Collection
     */
    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * pagination fcmqueue
     *
     * @param array $conditions
     * @param array $columns
     * @param int $page
     * @param int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $perPage = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * find fcmqueue by id
     *
     * @param $id
     * @param array $columns
     *
     * @return object|null
     */
    public function findById($id, array $columns = ['*']);

    /**
     * get fcmqueue dropdown list
     *
     * @return dropdown
     */
    public function dropdownList();

    /**
     * update email status and dispatch now
     *
     * @param array $id
     *
     * @return object
     */
    public function updateManyQueue(array $ids);
    
    /**
     * resend mail
     *
     * @param $id
     *
     * @return object
     */
    public function resend($id);

}
