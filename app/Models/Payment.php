<?php

namespace App\Models;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Helpers\ScopeQueries;
use App\Models\PaymentType;

class Payment extends Model
{
    use HasFactory;
    public $incrementing = false;
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    protected static function boot()
    {
        parent::boot();

        static::creating(function (Payment $payment) {
            $payment->id = (string) Str::uuid();
        });
    }

    protected $fillable = [
        'code',
        'before',
        'amount',
        'after',
        'cash',
        'payment_type_id',
        'status',
        'user_code',
        'created_admin',
        'updated_admin',
        'approved_at',
        'approved_admin',
        'created_admin_id',
        'updated_admin_id',
        'approved_admin_id',
    ];

    protected $appends = [
        'current_status',
        'current_payment_type'
    ];

    public static function statusList()
    {
        $array = [
            'waiting'  =>  __('messages.payments.status.waiting'),
            'success'  =>  __('messages.payments.status.success'),
            'fail'     =>  __('messages.payments.status.fail'),
            'cancel'   =>  __('messages.payments.status.cancel'),
        ];
        return $array;
    }

    public static function TypePaymentBillList()
    {
        $array = [
            'transport_bill' => __('messages.payments.type_bill.tyep_transport_bill'),
            'china_trip_tour' => __('messages.payments.type_bill.tyep_transport_trip'),
        ];
        return $array;
    }

    public static function timeFormatList()
    {
        $array = [
            'created_at'  =>  __('messages.payments.created_at'),
            'approved_at' =>  __('messages.payments.approved_at'),
            'updated_at'  =>  __('messages.payments.updated_at'),
        ];
        return $array;
    }
    public static function paymentsType()
    {
        return PaymentType::select(['id', 'name'])->get()->toArray();
    }

    public function getCurrentStatusAttribute()
    {
        return collect(self::statusList())->get($this->status);
    }

    public function getCurrentPaymentTypeAttribute()
    {
        $paymentType = PaymentType::select(['name'])->whereId($this->payment_type_id)->first();
        return  $paymentType->name;
    }

    public function scopeSearch($query, $conditions)
    {
        return $query->where(function ($query) use ($conditions) {
            if (@$conditions['code']) {
                $query->where('code', 'LIKE', "%{$conditions['code']}%");
            }
            if (@$conditions['user_code']) {
                $query->where('user_code', 'LIKE', "%{$conditions['user_code']}%");
            }
            if (@$conditions['before']) {
                $query->where('before', 'LIKE', "%{$conditions['before']}%");
            }
            if (@$conditions['amount']) {
                $query->where('amount', 'LIKE', "%{$conditions['amount']}%");
            }
            if (@$conditions['after']) {
                $query->where('after', 'LIKE', "%{$conditions['after']}%");
            }
            if (@$conditions['cash']) {
                $query->where('cash', 'LIKE', "%{$conditions['cash']}%");
            }
            if (@$conditions['payment_type_id']) {
                $query->where('payment_type_id', $conditions['payment_type_id']);
            }
            if (@$conditions['status']) {
                $query->where('status', $conditions['status']);
            }
            ///
            if (@$conditions['staff']) {
                $query->where('created_admin', 'LIKE', "%{$conditions['staff']}%")
                    ->orWhere('updated_admin', 'LIKE', "%{$conditions['staff']}%")
                    ->orWhere('approved_admin', 'LIKE', "%{$conditions['staff']}%");
            }

            if (@$conditions['payment_type_bill']) {
                $pamareter = $conditions['payment_type_bill'];
                    $query->whereIn('id', function($sub_query) use ($pamareter){
                        $sub_query->from('payables')
                                   ->select('payment_id')
                                   ->where('payable_type', $pamareter);
                    });

            }

            ///
            $time_format = (@$conditions['time_format']) ? $conditions['time_format'] : 'created_at';
            if (@$conditions['date_start'] && @$conditions['date_end']) {
                ScopeQueries::scopeDateRange($query, $time_format, $conditions['date_start'], $conditions['date_end']);
            } else if (@$conditions['date_start']) {
                ScopeQueries::scopeDateStart($query, $time_format, $conditions['date_start']);
            } else if (@$conditions['date_end']) {
                ScopeQueries::scopeDateEnd($query, $time_format, $conditions['date_end']);
            }
            ///
        });
    }

    // public function paymentsTrackingBill()
    // {
    //     return $this->belongsToMany('App\Models\Bill', 'payments_tracking_bills',  'payment_id', 'bill_id');
    // }
    public function credits()
    {
        return $this->morphMany('App\Models\Credit', 'creditable');
    }

    public function trackingBills() {
        return $this->morphedByMany(Bill::class,'payable');
    }

    public function trips() {
        return $this->morphedByMany(Trip::class,'payable');
    }
}
