<style>
	td,
	th {
		word-wrap: break-word;
	}
</style>
<div class="text-center" style="font-size: 24px;font-weight: bold;">ตารางแจกแจงรายการ&nbsp;Tracking </div>
<div>
	@lang('admin.user_code')&nbsp;:&nbsp;{{$bill->user_code}}&nbsp;/&nbsp;@lang('admin.bill_no')&nbsp;:&nbsp;{{$bill->no}} </div>
<div>
	@lang('admin.thai_shipping_method')&nbsp;: {!!str_replace(' ', '&nbsp;',$bill->shipping_name)!!}&nbsp;/&nbsp;โทร&nbsp;:&nbsp;{{$bill->shipping_tel}}
</div>

<table class="bordered tracking" aria-describedby="tracking">
	<tr style="background: #ccc">
		<th scope="col" width="14">
			<div>#</div>
		</th>
		<th width="55" scope="col">
			<div>@lang('admin.tracking_code')</div>
		</th>
		<th width="30" scope="col">@lang('admin.warehouse')</th>
		<th class="text-center" scope="col">
			<div>@lang('admin.zone')</div>
		</th>
		<th scope="col">
			<div>@lang('admin.thai_in')</div>
		</th>
		<th scope="col">
			<div>@lang('admin.shipping_method')</div>
		</th>
		<th class="text-right" scope="col">
			<div>น้ำหนัก<br>(kg)</div>
		</th>
		<th class="text-right" scope="col">
			<div>คิว<br>(m3)</div>
		</th>
		<th class="text-right" scope="col">
			<div>ขนาด<br>(กxยxส)</div>
		</th>
		<th scope="col">
			<div>@lang('admin.calculate_method')</div>
		</th>
		<th class="text-right" scope="col">
			<div>ค่าตีลังไม้<br>(บาท)</div>
		</th>
		<th class="text-right" scope="col">
			<div>ค่าขนส่งที่จีน<br>(บาท)</div>
		</th>
		<th width="50" class="text-right" scope="col">
			<div>ค่าขนส่งระหว่าง<br>ประเทศ(บาท)</div>
		</th>
	</tr>
	@php
	$i = 0;
	$trackings = $bill->pivotTrackings;
	@endphp
	@foreach($trackings as $key => $tracking)
	<?php if ($tracking->user_remark || $tracking->admin_remark) {
		$rowspan = 2;
	} else {
		$rowspan = 1;
	}
	?>
	<tr>
		<td class="text-right" rowspan="{{$rowspan}}">
			<div>{{++$i}}</div>
		</td>
		<?php
		if (!$trackings[$key]->sort) {
			$trackings[$key]->sort = $i;
			$trackings[$key]->save();
		}
		?>
		<td class="text-left" rowspan="{{$rowspan}}">
			<div>{{$tracking->code}}</div>
		</td>
		<td class="text-left" rowspan="{{$rowspan}}">
			<div>{!!str_replace(' ', '&nbsp;',$tracking->warehouse->title)!!}</div>
		</td>
		<td class="text-center" rowspan="{{$rowspan}}">
			<div>{!!str_replace(' ', '&nbsp;',$tracking->current_zone)!!}</div>
		</td>
		<td class="text-center">
			<div>{{$tracking->thai_in}}</div>
		</td>
		<td class="text-center">
			<div>{{$tracking->shippingMethods->title_th}}/{{$tracking->productTypes->title_th}}</div>
		</td>
		<td class="text-right">
			<div>{{number_format($tracking->weight,2)}}</div>
		</td>
		<td class="text-right">
			<div>{{number_format($tracking->cubic,4)}}</div>
		</td>
		<td class="text-right">
			<div>{{ $tracking->width }}x{{ $tracking->length }}x{{ $tracking->height }}</div>
		</td>

		<td class="text-center">
			<div>@lang('admin.'.$tracking->calculate_type)</div>
		</td>
		<td class="text-right">
			<div>{{number_format($tracking->thai_cost_box,2)}}</div>
		</td>
		<td class="text-right">
			<div>{{number_format($tracking->thai_cost_shipping,2)}}</div>
		</td>
		<td class="text-right">
			<div>{{number_format($tracking->price,2)}}</div>
		</td>
	</tr>
	@if($rowspan==2)
	<tr>
		<td colspan="9" style="background: #eee;">
			<div>
				@if($tracking->user_remark)
				@lang('admin.user_remark'):&nbsp;{!!str_replace(' ', '&nbsp;',$tracking->user_remark)!!}<br>
				@endif
				@if($tracking->admin_remark)
				@lang('admin.admin_remark'):&nbsp;{!!str_replace(' ', '&nbsp;',$tracking->admin_remark)!!}
				@endif
			</div>
		</td>
	</tr>
	@endif
	@endforeach
	<tr style="font-weight: bold;">
		<td class="text-right" colspan="2">
			<div>@lang('admin.summary')</div>
		</td>
		<td class="text-right">
			<div>-</div>
		</td>
		<td class="text-right">
			<div>-</div>
		</td>
		<td class="text-right">
			<div>-</div>
		</td>
		<td class="text-right">
			<div>-</div>
		</td>
		<td class="text-right">
			<div>{{ number_format($bill->pivotTrackings->sum('weight'),2)}}</div>
		</td>
		<td class="text-right">
			<div>{{ number_format($bill->pivotTrackings->sum('cubic'),4)}}</div>
		</td>
		<td class="text-right">
			<div>-</div>
		</td>
		<td class="text-right">
			<div>-</div>
		</td>
		<td class="text-right">
			<div>{{ number_format($bill->pivotTrackings->sum('thai_cost_box'),2)}}</div>
		</td>
		<td class="text-right">
			<div>{{ number_format($bill->pivotTrackings->sum('thai_cost_shipping'),2)}}</div>
		</td>
		<td class="text-right">
			<div>{{ number_format($bill->pivotTrackings->sum('price'),2)}}</div>
		</td>
	</tr>

</table>