<?php

namespace App\Mail\Topup;

use App\Models\Topup;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TopupApproved extends Mailable
{
    use Queueable, SerializesModels;

    public $topup;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Topup $topup)
    {
        //
        $this->topup    = $topup->load('credit');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to($this->topup->user->email)
        ->subject(__('emails.topup_approved', ['no' => $this->topup->no]))
        ->markdown('emails.topup.approved');
    }
}
