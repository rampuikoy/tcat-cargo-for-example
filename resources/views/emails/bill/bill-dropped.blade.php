@component('mail::message')
# เรียนคุณ {{$bill->user->name}}
แจ้งอัพเดทสถานะบิลขนส่ง <br>
รหัสลูกค้า: {{$bill->user->code}}<br>
เลขที่บิล: <a href="#" target="_blank">{{$bill->no}}</a> <br>
@component('mail::table')
| ข้อมูลชำระบิล   |               |
|:-------------|--------------:|
| สถานะบิล | <span style="color: #1bc5bd; text-align: left;">จัดส่งแล้ว</span> |
| จำนวน       | {{ $bill->counter }} ชิ้น|
| การขนส่งภายในประเทศ | {{ $bill->thaiShippingMethod->title }}|
| ชื่อผู้รับ   {{ $bill->shipping_name }} / เบอร์โทร: {{$bill->shipping_tel}} |
|ที่อยู่จัดส่ง {{$bill->shipping_address}} ต.{{@$bill->shippingDistrict->title_th}}|
| อ.{{@$bill->shippingAmphur->title_th}} จ.{{@$bill->shippingProvince->title_th}} {{$bill->shipping_zipcode}}|
| <span style="color: #1bc5bd; text-align: left;">จัดส่งถึง {{$bill->thaiShippingMethod->title}} เรียบร้อยแล้ว สามารถมารับสินค้าได้ในเวลาทำการ</span> |
@endcomponent
@endcomponent
