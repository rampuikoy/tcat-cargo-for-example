<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('points', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('pointable_type');
            $table->integer('pointable_id')->unsigned();
            $table->string('user_code')->nullable();
            $table->double('amount', 8, 2)->unsigned();
            $table->double('before', 8, 2)->unsigned();
            $table->double('after', 8, 2)->unsigned();
            $table->enum('type', ['topup', 'withdraw'])->default('topup');
            $table->string('created_admin')->nullable();
            $table->string('updated_admin')->nullable();
            $table->integer('created_admin_id')->unsigned()->nullable();
            $table->integer('updated_admin_id')->unsigned()->nullable();

            $table->foreign('created_admin_id')->references('id')->on('admins')->onDelete('set null');
            $table->foreign('updated_admin_id')->references('id')->on('admins')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('points');
    }
}
