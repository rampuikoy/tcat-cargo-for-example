<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommissionPolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('extend.commission');
    }

    public function show(Admin $admin)
    {
        return $admin->can('extend.commission');
    }

    public function store(Admin $admin)
    {
        return $admin->can('extend.commission');
    }
}
