<?php

namespace App\Models;

use App\Helpers\ScopeQueries;
use Illuminate\Database\Eloquent\Model;

class EmailQueue extends Model
{
    //
    protected $table = 'email_queues';

    protected $fillable = [
        'to',
        'mailable_class',
        'mailable_type',
        'mailable_id',
        'status',
    ];

    protected $appends = ['status_text', 'type_text'];

    public static function statusList()
    {
        $array = [
            'queue'     => __('messages.queue'),
            'success'   => __('messages.sended'),
        ];
        return $array;
    }

    public static $typeList = [
        'App\Mail\User\UserRegistered' => 'ยินดีต้อนรับ',
        // 'App\Mail\BillCancel'              => 'บิลขนส่ง ยกเลิก',
        // 'App\Mail\BillCreated'             => 'บิลขนส่ง บิลใหม่',
        // 'App\Mail\BillDropped'             => 'บิลขนส่ง รับสินค้า',
        // 'App\Mail\BillPayment'             => 'บิลขนส่ง ชำระเงินแล้ว',
        // 'App\Mail\BillShipped'             => 'บิลขนส่ง จัดส่งแล้ว ',
        // 'App\Mail\BillSuccess'             => 'บิลขนส่ง สรุปบิล ',
        // 'App\Mail\MoneyTransferCancel'     => 'บิลโอนเงินไปจีน ยกเลิก',
        // 'App\Mail\MoneyTransferCreated'    => 'บิลโอนเงินไปจีน บิลใหม่',
        // 'App\Mail\MoneyTransferPayment'    => 'บิลโอนเงินไปจีน ชำระเงินแล้ว ',
        // 'App\Mail\MoneyTransferTransfered' => 'บิลโอนเงินไปจีน โอนเงินสำเร็จ ',
        // 'App\Mail\TripCreated'             => 'บิล ChinaTripTour บิลใหม่',
        // 'App\Mail\TripCancel'              => 'บิล ChinaTripTour ยกเลิก',
        // 'App\Mail\TripPaid'                => 'บิล ChinaTripTour ชำระเงินแล้ว',
        // 'App\Mail\TripSuccess'             => 'บิล ChinaTripTour รายละเอียด',
        // 'App\Mail\MoneyReceiveApproved'    => 'รับเงินจีน สำเร็จ',
        // 'App\Mail\MoneyReceiveCancel'      => 'รับเงินจีน ยกเลิก',
        // 'App\Mail\TopupApproved'           => 'เติมเครดิต สำเร็จ',
        'App\Mail\Withdraw\WithdrawApproved'        => 'ถอนเครดิต สำเร็จ',
        // 'App\Mail\PointOrderSuccess'       => 'แลกสินค้า สำเร็จ',
        // 'App\Mail\PointOrderCancel'        => 'แลกสินค้า ยกเลิก',
    ];

    public function getStatusTextAttribute()
    {
        return collect(self::statusList())->get($this->status);
    }
    public function getTypeTextAttribute()
    {
        return collect(self::$typeList)->get($this->mailable_class);
    }

    public function mailable()
    {
        return $this->morphTo();
    }

    public function scopeSearch($query, $conditions)
    {
        return $query->where(function ($query) use ($conditions) {
            if (@$conditions['id']) {
                $query->where('id', $conditions['id']);
            }
            if (@$conditions['email']) {
                $query->where('to', 'LIKE', "%{$conditions['email']}%");
            }
            if (@$conditions['status']) {
                $query->where('status', $conditions['status']);
            }
            if (@$conditions['type']) {
                $query->whereMailableClass($conditions['type']);
            }
            $date_type = (@$conditions['date_type']) ? $conditions['date_type'] : 'created_at';
            if (@$conditions['date_start'] && @$conditions['date_end']) {
                ScopeQueries::scopeDateRange($query, $date_type, $conditions['date_start'], $conditions['date_end']);
            } else if (@$conditions['date_start']) {
                ScopeQueries::scopeDateStart($query, 'created_at', $conditions['date_start']);
            } else if (@$conditions['date_end']) {
                ScopeQueries::scopeDateEnd($query, 'created_at', $conditions['date_end']);
            }
        });
    }
}
