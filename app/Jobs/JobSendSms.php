<?php

namespace App\Jobs;

use App\Interfaces\SmsQueueInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class JobSendSms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        //
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(SmsQueueInterface $smsQueueInterface)
    {
        try {
            $result = $smsQueueInterface->resend($this->id);
            if (is_string($result)) {
                Log::warning($result);
            }
        } catch (\Throwable $th) {
            Log::error($th);
        }
    } //

}
