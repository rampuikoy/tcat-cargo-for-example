<?php

namespace App\Observers;

use App\Models\TopupAutopay;
use App\Observers\BaseObserver;

class TopupAutopayObserver extends BaseObserver
{
    //
    public function creating(TopupAutopay $topupAutopay)
    {
        $this->stampCreated($topupAutopay);
    }

    public function updating(TopupAutopay $topupAutopay)
    {
        $this->stampUpdated($topupAutopay);
    }

}
