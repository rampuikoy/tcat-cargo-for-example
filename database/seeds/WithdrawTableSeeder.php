<?php

namespace Database\Seeders;

use App\Models\BankAccount;
use App\Models\User;
use App\Models\Withdraw;
use Illuminate\Database\Seeder;

class WithdrawTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::where('code', 'LIKE', "SF%")->limit(5)->get();
        $bank_company = BankAccount::whereNull('user_code')->first();

        $users->each(function ($user) use ($bank_company) {
            $user_bank = $user->banks->first();
            if (!$user_bank) {
                return;
            }
            $withdraw = Withdraw::factory()->create([
                'user_code'                 => $user->code,

                'user_bank_account_id'      => $user_bank->id,
                'user_bank'                 => $user_bank->bank,
                'user_account'              => $user_bank->account,

                'system_bank_account_id'    => @$bank_company->id,
                'system_bank'               => @$bank_company->bank,
                'system_account'            => @$bank_company->account,
            ]);

            $total = $withdraw->user->credit->total ?? 0;

            if ($withdraw->status !== 'cancel'){
                // withdraw reletion create credit
                $withdraw->credit()->create([
                    'type'              => 'withdraw',
                    'user_code'         => $withdraw->user_code,
                    'amount'            => $withdraw->amount,
                    'before'            => $total,
                    'after'             => $total - $withdraw->amount,
                    'created_admin'     => $withdraw->created_admin,
                    'created_admin_id'  => $withdraw->created_admin_id,
                    'updated_admin'     => $withdraw->updated_admin,
                    'updated_admin_id'  => $withdraw->updated_admin_id,
                    'approved_at'       => $withdraw->approved_at,
                    'approved_admin'    => $withdraw->approved_admin,
                    'approved_admin_id' => $withdraw->approved_admin_id,
                ]);
            }
        });
    }
}
