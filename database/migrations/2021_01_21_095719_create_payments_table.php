<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->datetime('approved_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->string('code')->unique();
            $table->double('before', 8, 2)->unsigned()->nullable();
            $table->double('amount', 8, 2)->unsigned()->nullable();
            $table->double('after', 10, 2)->unsigned()->nullable();
            $table->double('cash', 10, 2)->unsigned()->nullable();
            $table->integer('payment_type_id')->unsigned()->nullable();
            $table->integer('topup_id')->unsigned()->nullable();
            $table->enum('status', ['waiting', 'success', 'fail','cancel'])->default('waiting');
            $table->string('user_code')->nullable();

            $table->string('created_admin')->nullable();
            $table->string('updated_admin')->nullable();
            $table->string('approved_admin')->nullable();

            $table->integer('created_admin_id')->unsigned()->nullable();
            $table->integer('updated_admin_id')->unsigned()->nullable();
            $table->integer('approved_admin_id')->unsigned()->nullable();


            $table->foreign('created_admin_id')->references('id')->on('admins')->onDelete('set null');
            $table->foreign('updated_admin_id')->references('id')->on('admins')->onDelete('set null');
            $table->foreign('approved_admin_id')->references('id')->on('admins')->onDelete('set null');

            $table->foreign('user_code')->references('code')->on('users')->onDelete('set null');
            $table->foreign('payment_type_id')->references('id')->on('payment_types')->onDelete('set null');
            $table->foreign('topup_id')->references('id')->on('topups')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
