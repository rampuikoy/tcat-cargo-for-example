<?php
namespace App\Helpers;

class FirebaseNotification
{
    public static function send($token, $payload)
    {
        $endPoint = 'https://fcm.googleapis.com/fcm/send';
        $serverKey = env('FIREBASE_SERVER_KEY');

        if (!$serverKey) {
            return (object) ['error' => true, 'message' => 'Firebase server key invalid'];
        }

        if (!array_key_exists('title', $payload)) {
            return (object) ['error' => true, 'message' => 'Fill your title'];
        }
        if (!array_key_exists('body', $payload)) {
            return (object) ['error' => true, 'message' => 'Fill your body'];
        }

        $data = [
            "to" => $token,
            "notification" => [
                'title' => $payload['title'],
                'body' => $payload['body'],
                // 'image' => 'https://image.flaticon.com/icons/png/512/270/270014.png',
                // 'icon' => "https://image.flaticon.com/icons/png/512/270/270014.png", /*Default Icon*/
            ],
        ];
        $dataString = json_encode($data);

        $headers = [
            'Authorization: key=' . env('FIREBASE_SERVER_KEY'),
            'Content-Type: application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $endPoint);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
        $response = curl_exec($ch);

        $results = json_decode($response);

        if (!$results) {
            return (object) ['error' => true, 'message' => 'Can\'t not connect firebase'];
        }

        if (@$results['success'] === 0) {
            return (object) ['error' => true, 'message' => 'Sending notification failed'];
        }

        return (object) ['error' => false, 'message' => 'Sending notification success', 'results' => $results];
    }
}
