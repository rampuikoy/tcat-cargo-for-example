<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Helpers\ScopeQueries;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Withdraw extends Model
{
    use SoftDeletes, HasFactory;

    const PREFIX                = "WD-";
    const DEFAULT_CHARGE        = 10;

    public static $imageFolder  = 'uploads/withdraw/';
    public static $imageWidth   = 700;
    public static $imageHeight  = 700;

    protected $fillable = [
        'approved_at',
        'amount',
        'charge',
        'receive',
        'date',
        'time',
        'user_code',
        'user_bank_account_id',
        'system_bank_account_id',
        'method',
        'user_bank',
        'user_account',
        'system_bank',
        'system_account',
        'status',
        'user_remark',
        'admin_remark',
        'system_remark',
        'created_admin',
        'updated_admin',
        'approved_admin',
        'created_admin_id',
        'updated_admin_id',
        'approved_admin_id',
    ];

    protected $appends = [
        'no',
        'status_text',
        'method_text',
        'amount_text',
        'receive_text',
    ];

    public static function statusList()
    {
        $array = [
            'cancel'    => __('messages.cancel'),
            'waiting'   => __('messages.waiting'),
            'approved'  => __('messages.approved'),
        ];
        return $array;
    }

    public static function methodList()
    {
        $array = [
            'transfer'  => __('messages.withdraw_method.transfer'),
            'cash'      => __('messages.withdraw_method.cash'),
            'cheque'    => __('messages.withdraw_method.cheque'),
            'credit'    => __('messages.withdraw_method.credit'),
            'other'     => __('messages.withdraw_method.other'),
        ];
        return $array;
    }

    public static function dateTypeList()
    {
        $array = [
            'created_at'    => __('messages.created_at'),
            'updated_at'    => __('messages.updated_at'),
            'approved_at'   => __('messages.approved_at'),
            'date'          => __('messages.date_transfer'),
        ];
        return $array;
    }

    public static function conditionList()
    {
        $array = [
            'only_has_image'        => __('messages.conditions.only_has_image'),
            'only_not_image'        => __('messages.conditions.only_not_image'),
            'user_remark'           => __('messages.conditions.only_user_remark'),
            'admin_remark'          => __('messages.conditions.only_admin_remark'),
            'created_admin_null'    => __('messages.conditions.only_created_admin_null'),
        ];
        return $array;
    }

    public function getNoAttribute()
    {
        return ($this->id) ? self::PREFIX . str_pad($this->id, 6, "0", STR_PAD_LEFT) : null;
    }

    public function getStatusTextAttribute()
    {
        return collect(self::statusList())->get($this->status);
    }

    public function getMethodTextAttribute()
    {
        return collect(self::methodList())->get($this->method);
    }

    public function getAmountTextAttribute()
    {
        return is_numeric($this->amount) ? number_format($this->amount, 2) : null;
    }

    public function getReceiveTextAttribute()
    {
        return is_numeric($this->receive) ? number_format($this->receive, 2) : null;
    }

    // public function setDateAttribute($value)
    // {
    //     $this->attributes['date'] = ($value) ? $value : null;
    // }

    // public function setTimeAttribute($value)
    // {
    //     $this->attributes['time'] = ($value) ? $value : null;
    // }

    // public function setUserBankAccountIdAttribute($value)
    // {
    //     $this->attributes['user_bank_account_id'] = ($value) ? $value : null;
    // }

    // public function setSystemBankAccountIdAttribute($value)
    // {
    //     if ($value && $value != 'null') {
    //         $this->attributes['system_bank_account_id'] = $value;
    //     }else{
    //         $this->attributes['system_bank_account_id'] =  null;
    //     }
    // }

    /**
     * relation
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_code', 'code');
    }

    public function userBankAccount()
    {
        return $this->belongsTo('App\Models\BankAccount', 'user_bank_account_id');
    }

    public function systemBankAccount()
    {
        return $this->belongsTo('App\Models\BankAccount', 'system_bank_account_id');
    }

    public function credit()
    {
        return $this->morphOne('App\Models\Credit', 'creditable');
    }

    public function credits()
    {
        return $this->morphMany('App\Models\Credit', 'creditable');
    }

    public function emails()
    {
        return $this->morphMany('App\Models\EmailQueue', 'mailable');
    }

    public function fcm()
    {
        return $this->morphMany('App\Models\FcmQueue', 'fcmable');
    }

    public function sms()
    {
        return $this->morphMany('App\Models\SmsQueue', 'smsable');
    }

    public function images()
    {
        return $this->morphMany('App\Models\Upload', 'relatable');
    }

    // scoped query sql
    public function scopeSearch($query, $conditions)
    {
        return  $query->where(function ($query) use ($conditions) {
            if (@$conditions['id']) {
                $trimmed = ltrim($conditions['id'], Withdraw::PREFIX.'0');
                $query->where('id', 'LIKE', '%'.$trimmed.'%');
            }
            if (@$conditions['system_bank_account_id']) {
                $query->where('system_bank_account_id', 'LIKE', "%{$conditions['system_bank_account_id']}%");
            }
            if (@$conditions['user_code']) {
                $query->where('user_code', 'LIKE', "%{$conditions['user_code']}%");
            }
            if (@$conditions['status']) {
                $query->where('status',$conditions['status']);
            }
            if (@$conditions['amount']) {
                $query->where('amount',$conditions['amount']);
            }
            if (@$conditions['method']) {
                $query->where('method',$conditions['method']);
            }
            if (@$conditions['time']) {
                $query->where('time', 'LIKE', "%{$conditions['time']}%");
            }
            if (@$conditions['user_remark']) {
                $query->where('user_remark', 'LIKE', "%{$conditions['user_remark']}%");
            }
            if (@$conditions['admin_remark']) {
                $query->where('admin_remark', 'LIKE', "%{$conditions['admin_remark']}%");
            }

            $date_type = (@$conditions['date_type']) ? $conditions['date_type'] : 'created_at';
            if (@$conditions['date_start'] && @$conditions['date_end']) {
                ScopeQueries::scopeDateRange($query, $date_type, $conditions['date_start'], $conditions['date_end']);
            } else if (@$conditions['date_start']) {
                ScopeQueries::scopeDateStart($query, 'created_at', $conditions['date_start']);
            } else if (@$conditions['date_end']) {
                ScopeQueries::scopeDateEnd($query, 'created_at', $conditions['date_end']);
            }

            if (@$conditions['condition']) {

                switch (@$conditions['condition']) {
                    case 'only_has_image':
                        $query->whereIn('id', function($sub_query){
                            $sub_query->from('uploads')
                                       ->select('relatable_id')
                                       ->where('relatable_type','App\Models\Withdraw');
                        });
                        break;
                    case 'only_not_image':
                        $query->whereNotIn('id', function($sub_query){
                            $sub_query->from('uploads')
                                       ->select('relatable_id')
                                       ->where('relatable_type','App\Models\Withdraw');
                        });
                        break;
                    case 'admin_remark':
                        $query->whereRaw('admin_remark != "" AND admin_remark IS NOT NULL');
                        break;
                    case 'user_remark':
                        $query->whereRaw('user_remark != "" AND user_remark IS NOT NULL');
                        break;
                    case 'created_admin_null':
                        $query->whereNull('created_admin');
                        break;

                }
            }
        });
    }
}
