<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Article\StoreArticleRequest;
use App\Http\Requests\Article\UpdateArticleRequest;
use App\Interfaces\ArticleInterface;
use App\Models\Article;

class ArticleController extends Controller
{
    protected $articleInterface;

    public function __construct(ArticleInterface $articleInterface)
    {
        $this->articleInterface = $articleInterface;
    }

    public function index(Request $request)
    {
        $this->authorize('index', Article::class);
        $columns    = $request->input('columns', ['*']);
        $articles = $this->articleInterface->all($columns);
        return response()->json($articles, $articles->code);
    }

    public function show(Request $request, $id)
    {
        $this->authorize('show', Article::class);
        $columns        = $request->input('columns', ['*']);
        $article    = $this->articleInterface->findById($id, $columns);
        return response()->json($article, $article->code);
    }    

    public function search(Request $request)
    {
        $this->authorize('index', Article::class);$conditions = $request->only(['id', 'search']);
        $columns    = $request->input('columns', ['*']);
        $limit      = $request->input('limit', $this->articleInterface::DEFAULT_LIMIT);
        $sorting    = $request->input('order_by', $this->articleInterface::DEFAULT_SORTING);
        $ascending  = $request->input('ascending') == 1 ? 'asc' : 'desc';

        if ($request->boolean('pagination')) {
            $page           = $request->input('page', 1);
            $articles  = $this->articleInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset         = $request->input('page', 0);
            $articles  = $this->articleInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($articles, $articles->code);
    }

    public function dropdown()
    {
        $dropdown = $this->articleInterface->dropdownList();
        return response()->json($dropdown, $dropdown->code);
    }

    public function store(StoreArticleRequest $request)
    {
        $data           = $request->only([
                            'status',
                            'type',
                            'title',
                            'content',
                            'views',
                            'published_at',
                            'file'
                        ]);
        $article        = $this->articleInterface->store($data);
        return response()->json($article, $article->code);
    }

    public function update(UpdateArticleRequest $request, $id)
    {
        $data           = $request->only([
            'status',
            'type',
            'title',
            'content',
            'views',
            'published_at',
            'file'
        ]);
        $article        = $this->articleInterface->updateById($id, $data);
        return response()->json($article, $article->code);
    }

    public function destroy($id)
    {
        $this->authorize('destroy', Article::class);
        $article = $this->articleInterface->softDeleteById($id);
        return response()->json($article, $article->code);
    }
}
