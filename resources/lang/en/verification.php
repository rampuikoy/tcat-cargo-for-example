<?php

return [

    'verified' => 'Your email has been verified!',
    'unverified' => 'Your email address is not verified',
    'invalid' => 'The verification link is invalid.',
    'already_verified' => 'The email is already verified.',
    'user' => 'We can\'t find a user with that e-mail address.',
    'sent' => 'We have e-mailed your verification link!',
    

];
