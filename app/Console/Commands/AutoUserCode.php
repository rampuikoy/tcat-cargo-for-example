<?php

namespace App\Console\Commands;

use App\Helpers\Util;
use App\Models\Tracking;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class AutoUserCode extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature =  'tracking:auto-user-code';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto set userCode = refferrence code after thai in 14 days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Tracking $tracking,User $user)
    {
        $this->tracking=$tracking;
        $this->user=$user;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('run tracking auto user code...');
        $trackings = $this->tracking->stuck()->get();

        collect($trackings)
            ->map(function ($track) {
                if ($track->reference_code) {
                    $result = explode("/", $track->reference_code);
                    $current = end($result);
                    $user_code = strtoupper(str_replace(' ', '', $current));
                    $user = $this->user->select('code')->whereCode($user_code)->get();
                        if ($user->count() > 0) {
                            $track->update([
                                'user_code'          =>  $user_code,
                                'system_remark'      => $track->system_remark = Util::concatField($track->system_remark, 'เกิน5วัน'),
                            ]);
                        }
                }
            });
    }
}
