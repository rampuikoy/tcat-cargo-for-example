<?php

namespace Database\Factories;

use App\Models\CommissionRate;
use Illuminate\Database\Eloquent\Factories\Factory;

class CommissionRateFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CommissionRate::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'kg_car_genaral' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 100),
            'kg_car_iso' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 100),
            'kg_car_brand' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 100),

            'kg_ship_genaral' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 100),
            'kg_ship_iso' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 100),
            'kg_ship_brand' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 100),

            'cubic_car_genaral' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 100),
            'cubic_car_iso' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 100),
            'cubic_car_brand' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 100),

            'cubic_ship_genaral' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 100),
            'cubic_ship_iso' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 100),
            'cubic_ship_brand' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 1, $max = 100),

            'admin_remark' => $this->faker->lexify('??????') ,
            'created_admin' => $this->faker->lexify('??????') ,
            'updated_admin' => $this->faker->lexify('??????') ,
            'bill_id' => random_int(1, 10),
        ];
    }
}
