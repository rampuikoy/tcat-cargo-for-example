<?php

namespace App\Http\Requests\NoCode;

use App\Http\Requests\FormRequest;

class FindOrCreateNoCodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.nocode.store');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|string',
            'create' => 'required|boolean',
            'tcat' => 'required|boolean',
            'taobao' => 'required|boolean',
            'tmall' => 'required|boolean',
        ];
    }

    public function attributes()
    {
        return [
            'code'              => __('form.no_code.code'),
            'create'            => __('form.no_code.option.create'),
            'tcat'              => __('form.no_code.option.tcat'),
            'taobao'            => __('form.no_code.option.taobao'),
            'tmall'             => __('form.no_code.option.tmall'),
        ];
    }
}
