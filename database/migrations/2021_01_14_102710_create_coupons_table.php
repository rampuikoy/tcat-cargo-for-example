<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('title');
            $table->string('code');
            $table->string('user_code')->nullable();
            $table->integer('shipping_method_id')->unsigned()->nullable();
            $table->enum('limit_user_code', ['yes', 'no'])->default('no');
            $table->enum('limit_shipping_method', ['yes', 'no'])->default('no');
            $table->enum('type', ['one_time', 'life_time'])->default('one_time');
            $table->double('price', 8, 2)->default(0.00);
            $table->enum('bill', ['shipping', 'money_transfer', 'trip', 'all'])->default('all');
            $table->double('bill_min_price', 8, 2)->default(0.00);
            $table->datetime('start_date')->nullable();
            $table->datetime('end_date')->nullable();
            $table->integer('limit')->nullable();
            $table->integer('counter')->default(0);
            $table->text('remark')->nullable();
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->integer('created_admin_id')->unsigned()->nullable();
            $table->integer('updated_admin_id')->unsigned()->nullable();
            $table->string('created_admin')->nullable();
            $table->string('updated_admin')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
