<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AssignGuard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard, $guard2 = null)
    {
        if ($guard !== null) {
            auth()->shouldUse($guard);
        } elseif ($guard2 !== null) {
            auth()->shouldUse($guard2);
        } else {
            return response()->json(['message' => 'not found gaurds'], 400);
        }
        return $next($request);
    }
}
