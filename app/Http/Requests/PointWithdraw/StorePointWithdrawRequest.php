<?php

namespace App\Http\Requests\PointWithdraw;

use App\Http\Requests\FormRequest;

class StorePointWithdrawRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.nocode.store');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'user_code'         => 'required|max:10|exists:users,code',
            'amount'            => 'required|numeric',
            'type'              => 'required|in:topup,withdraw',
            'remark'            => 'sometimes|nullable|string'
        ];
        return $rules;
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'user_code'         => __('form.point_withdraw.user_code'),
            'amount'            => __('form.point_withdraw.amount'),
            'type'              => __('form.point_withdraw.type'),
            'remark'            => __('form.point_withdraw.remark'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
