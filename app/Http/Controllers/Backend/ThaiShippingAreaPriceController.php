<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\ThaiShippingAreaPrice\StoreAreaPriceMethodRequest;
use App\Http\Requests\ThaiShippingAreaPrice\UpdateAreaPriceMethodRequest;
use App\Interfaces\ProvinceInterface;
use Illuminate\Http\Request;
use App\Interfaces\ThaiShippingAreaPriceInterface;

class ThaiShippingAreaPriceController extends Controller
{
    protected $thaiShippingAreaPriceInterface;

    public function __construct(ThaiShippingAreaPriceInterface $thaiShippingAreaPriceInterface, ProvinceInterface $provinceInterface)
    {
        $this->thaiShippingAreaPriceInterface   = $thaiShippingAreaPriceInterface;
        $this->provinceInterface                = $provinceInterface;
    }

    public function index(Request $request)
    {
        $this->authorize('index', ThaiShippingAreaPrice::class);
        $columns         = $request->input('columns', ['*']);
        $thaiShippingAreaPrice            = $this->thaiShippingAreaPriceInterface->all($columns);
        return response()->json($thaiShippingAreaPrice, $thaiShippingAreaPrice->code);
    }

    public function show(Request $request, $id)
    {
        $this->authorize('show', ThaiShippingAreaPrice::class);
        $columns          = $request->input('columns', ['*']);
        $thaiShippingAreaPrice             = $this->thaiShippingAreaPriceInterface->findById($id, $columns);
        return response()->json($thaiShippingAreaPrice, $thaiShippingAreaPrice->code);
    }

    public function search(Request $request)
    {
        $this->authorize('index', ThaiShippingAreaPrice::class);
        $conditions     = $request->only(['thai_shipping_method_id']);
        $columns        = $request->input('columns', ['*']);
        $limit          = $request->input('limit', $this->thaiShippingAreaPriceInterface::DEFAULT_LIMIT);
        $sorting        = $request->input('order_by', $this->thaiShippingAreaPriceInterface::DEFAULT_SORTING);
        $ascending      = $request->input('ascending') == 1 ? 'asc' : 'desc';

        if ($request->boolean('pagination')) {
            $page             = $request->input('page', 1);
            $thaiShippingAreaPrice  = $this->thaiShippingAreaPriceInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset           = $request->input('page', 0);
            $thaiShippingAreaPrice  = $this->thaiShippingAreaPriceInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($thaiShippingAreaPrice, $thaiShippingAreaPrice->code);
    }

    public function store(StoreAreaPriceMethodRequest $request)
    {
        $data                   = $request->only(['province_id', 'price_a', 'price_b', 'price_c', 'price_d', 'price_e', 'thai_shipping_method_id']);
        $thaiShippingAreaPrice  = $this->thaiShippingAreaPriceInterface->store($data);
        return response()->json($thaiShippingAreaPrice, $thaiShippingAreaPrice->code);
    }

    public function update(UpdateAreaPriceMethodRequest $request, $id)
    {
        $data                   = $request->only(['province_id', 'price_a', 'price_b', 'price_c', 'price_d', 'price_e']);
        $thaiShippingAreaPrice  = $this->thaiShippingAreaPriceInterface->updateById($id, $data);
        return response()->json($thaiShippingAreaPrice, $thaiShippingAreaPrice->code);
    }

    public function destroy($id)
    {
        $this->authorize('destroy', ThaiShippingAreaPrice::class);
        $thaiShippingAreaPrice         = $this->thaiShippingAreaPriceInterface->softDeleteById($id);
        return response()->json($thaiShippingAreaPrice, $thaiShippingAreaPrice->code);
    }
}
