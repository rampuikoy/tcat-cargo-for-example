<?php

namespace App\Http\Requests\Chat;

use App\Rules\Base64Image;
use App\Http\Requests\FormRequest;

class StoreBroadcastRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.chat.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message'          => 'required_without:file',
            // required file ถ้า message ไม่มีค่า หรือเท่ากับ null
            'file'             => ['required_without:message', 'nullable', new Base64Image],
            'status'           => 'required|in:active,inactive',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'message'       => __('form.chat.message'),
            'file'          => __('form.chat.file'),
            'status'        => __('form.chat.status'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
