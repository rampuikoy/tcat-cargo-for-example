<?php

use Database\Seeders\BankMapTableSeeder;
use Database\Seeders\BankTableSeeder;
use Database\Seeders\BillsSeeder;
use Database\Seeders\CommissionRateSeeder;
use Database\Seeders\CommissionSeeder;
use Database\Seeders\CouponSeeder;
use Database\Seeders\NoCodeTableSeeder;
use Database\Seeders\PackingQueueSeeder;
use Database\Seeders\PaymentTableSeeder;
use Database\Seeders\PaymentTypeTableSeeder;
use Database\Seeders\ClaimItemsSeeder;
use Database\Seeders\ClaimsSeeder;
use Database\Seeders\PointOrderTableSeeder;
use Database\Seeders\PointWithdrawTableSeeder;
use Database\Seeders\ProductTableSeeder;
use Database\Seeders\ThaiShippingAreaPriceTableSeeder;
use Database\Seeders\ThaiShippingWeightPriceTableSeeder;
use Database\Seeders\ThaiShippingWeightSizePriceTableSeeder;
use Database\Seeders\TopupAutopayTableSeeder;
use Database\Seeders\TopupTableSeeder;
use Database\Seeders\TrackingSeeder;
use Database\Seeders\WarehouseChineseTableSeeder;
use Database\Seeders\WarehouseTableSeeder;
use Database\Seeders\WithdrawTableSeeder;
use Database\Seeders\TripTableSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LocationTableSeeder::class);
        $this->call(ShippingMethodTableSeeder::class);
        $this->call(ThaiShippingMethodTableSeeder::class);

        $this->call(AdminTableSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(ArticleTableSeeder::class);
        $this->call(SeoTableSeeder::class);
        $this->call(SettingTableSeeder::class);

        $this->call(ProductTypeTableSeeder::class);
        $this->call(RateSeeder::class);
        $this->call(ExchangeRateSeeder::class);
        $this->call(WarehouseTableSeeder::class);
        $this->call(WarehouseChineseTableSeeder::class);
        $this->call(ThaiShippingAreaPriceTableSeeder::class);
        $this->call(ThaiShippingWeightPriceTableSeeder::class);
        $this->call(ThaiShippingWeightSizePriceTableSeeder::class);
        $this->call(TruckTableSeeder::class);

        $this->call(UserTableSeeder::class);
        $this->call(UserAddressTableSeeder::class);
        $this->call(BankTableSeeder::class);
        $this->call(BankMapTableSeeder::class);
        $this->call(BankAccountTableSeeder::class);

        $this->call(CouponSeeder::class);
        $this->call(BillsSeeder::class);
        $this->call(TrackingSeeder::class);
        // $this->call(TrackingDetailSeeder::class);

        $this->call(CommissionSeeder::class);
        $this->call(CommissionRateSeeder::class);

        // $this->call(CreditSeeder::class);
        $this->call(TopupTableSeeder::class);
        $this->call(TopupAutopayTableSeeder::class);
        $this->call(WithdrawTableSeeder::class);
        $this->call(PackingQueueSeeder::class);
        $this->call(PaymentTypeTableSeeder::class);
        $this->call(PaymentTableSeeder::class);
        $this->call(ClaimItemsSeeder::class);
        $this->call(ClaimsSeeder::class);
        $this->call(NoCodeTableSeeder::class);
        $this->call(TripTableSeeder::class);
        $this->call(PointWithdrawTableSeeder::class);
        $this->call(ProductTableSeeder::class);
        $this->call(PointOrderTableSeeder::class);
    }
}
