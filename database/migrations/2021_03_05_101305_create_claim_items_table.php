<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClaimItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claim_items', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('claim_id')->unsigned();
            $table->string('tracking')->nullable();
            $table->integer('amount')->unsigned();
            $table->double('price', 8, 2)->unsigned();
            $table->double('total', 8, 2)->unsigned();
            $table->text('remark')->nullable();
            $table->enum('type', ['lose', 'lost_some', 'broken'])->default('broken');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claim_items');
    }
}
