<?php

namespace App\Repositories;

use App\Interfaces\PermissionInterface;
use App\Models\Permission;
use App\Models\Route;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use Illuminate\Support\Facades\Cache;

class PermissionRepository implements PermissionInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const MINUTE_CACHE = 1;

    public function __construct(Permission $permission)
    {
        $this->permission = $permission;
    }

    public function all(array $columns = ['*'], $sorting = PermissionInterface::DEFAULT_SORTING, $ascending = PermissionInterface::DEFAULT_ASCENDING)
    {
        $key  = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time = $this->getTime(self::MINUTE_CACHE);

        $permissions = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
            return $this->permission::select($columns)
                ->orderBy($sorting, $ascending)
                ->showOnly()
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.permission')]), $permissions);
    }

    public function fetchListByFields(array $conditions, array $columns = ['*'], $offset = 0, $limit = PermissionInterface::DEFAULT_LIMIT, $sorting = PermissionInterface::DEFAULT_SORTING, $ascending = PermissionInterface::DEFAULT_ASCENDING)
    {
        $key = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time = $this->getTime(self::MINUTE_CACHE);

        $permissions = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
            return $this->permission::select($columns)
                ->search($conditions)
                ->orderBy($sorting, $ascending)
                ->skip($offset)
                ->limit($limit)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.permission')]), $permissions);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = PermissionInterface::DEFAULT_LIMIT, $sorting = PermissionInterface::DEFAULT_SORTING, $ascending = PermissionInterface::DEFAULT_ASCENDING)
    {
        $key = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time = $this->getTime(self::MINUTE_CACHE);

        $permissions = Cache::tags([

            $this->getClassPrefix(),
            $this->getPaginationWithClassPrefix(),

        ])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
            $model = $this->permission::select($columns)
                ->search($conditions)
                ->orderBy($sorting, $ascending);

            if ($page == 1) {
                return $model->paginate($limit);
            } else {
                return $model->skip($limit * ($page - 1))->paginate($limit);
            }
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.permission')]), $permissions);
    }

    public function findById($id, array $columns = ['*'])
    {
        // $key        = $this->getCacheKey(null, $id);
        $permission = $this->findByIdMethod($id, $columns);
        if (!$permission) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.permission'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.permission')]), $permission);
    }

    public function dropdownList()
    {
        $key        = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $dropdown = Cache::remember($key, $time, function () {
            $columns = ['id', 'ability', 'method', 'guard_name'];
            return [
                'routes' => Route::select($columns)
                    ->guardAdmin()
                    ->get(),
            ];
        });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function store(array $data)
    {

        $permission = $this->permission::create($data);
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.permission')]), $permission);
    }

    public function updateById($id, array $data)
    {

        $permission = $this->findByIdMethod($id);
        if (!$permission) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.permission'), 'id' => $id]));
        }

        $permission->update($data);
        Cache::tags([$this->getIdWithClassPrefix($permission->id)])->flush();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.permission')]), $permission);
    }

    public function deleteById($id)
    {
        $permission = $this->findByIdMethod($id);

        if (!$permission) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.permission'), 'id' => $id]));
        }

        $permission->delete();
        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.delete_data', ['data' => __('messages.permission')]), $permission);
    }

    public function deleteMany(array $ids)
    {
        if (count($ids) === 0) {
            return null;
        }

        $results = [];
        foreach ($ids as $id) {
            $permission = $this->findByIdMethod($id);

            if ($permission) {
                $permission->delete();
                array_push($results, $permission);
                Cache::tags([$this->getIdWithClassPrefix($id)])->flush();
            }
        }

        if (count($results) === 0) {
            return $this->coreResponse(500, __('messages.not_delete_data', ['data' => __('messages.permission')]));
        }

        $this->clearCacheTags();
        return $this->coreResponse(200, __('messages.delete_data', ['data' => __('messages.permission')]), $results);
    }

    private function findByIdMethod($id, array $columns = ['*'])
    {
        $key        = $this->getCacheKey(null, $id);
        $time       = $this->getTime(self::MINUTE_CACHE);

        $permission    = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($id)
        ])->remember($key, $time, function () use ($id, $columns) {
            return $this->permission::select($columns)
                ->with('pivotRoles')
                ->find($id);
        });

        return $permission;
    }

    private function clearCacheTags()
    {
        Cache::tags([
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix()
        ])->flush();
    }
}
