<?php

namespace App\Models;

use App\Helpers\ScopeQueries;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;

class Tracking extends Model
{
    use SoftDeletes, HasFactory;

    protected $table = 'trackings';
    protected $dates = ['deleted_at'];
    const NOCODE_PREFIX = 'NC-';

    public static $imageFolder  = 'uploads/trackings/';
    public static $imageWidth   = 700;
    public static $imageHeight  = 700;

    protected $fillable = [
        'user_code',
        'code_group',
        'code',
        'reference_code',
        'weight',
        'width',
        'height',
        'length',
        'cubic',
        'dimension',
        'calculate_type',
        'calculate_rate',
        'price',
        'cubic_rate',
        'weight_rate',
        'cubic_price',
        'weight_price',
        'china_in',
        'china_out',
        'thai_in',
        'thai_out',
        'user_remark',
        'admin_remark',
        'system_remark',
        'bill_id',
        'shipping_method_id',
        'product_type_id',
        'warehouse_id',
        'warehouse_zone',
        'status',
        'china_cost_box',
        'china_cost_shipping',
        'thai_cost_box',
        'thai_cost_shipping',
        'exchange_rate',
        'created_admin_id',
        'updated_admin_id',
        'created_admin',
        'updated_admin',
        'packed_admin',
        'packed_admin_id',
        'packed_at',
        'duplicate_order',
        'duplicate_check',
        'sort',
        'deleted_at',
        'insert_uid',
        'type',
        'detail'
    ];

    protected $appends = [
        'calculate_text',
        'current_status',
        // 'current_type',
        "current_zone",
        'warehouse_text',
        'bill_no',
        'nocode_no'
    ];

    public static function conditionList()
    {
        $array = [
            'user_code'           =>  __('messages.trackings.user_code'),
            'user_code_null'      =>  __('messages.trackings.user_code_null'),
            'user_code_ref_null'  =>  __('messages.trackings.user_code_ref_null'),
            'bill_id'             =>  __('messages.trackings.bill_id'),
            'bill_id_null'        =>  __('messages.trackings.bill_id_null'),
            'china_cost_box'      =>  __('messages.trackings.china_cost_box'),
            'china_cost_shipping' =>  __('messages.trackings.china_cost_shipping'),
            'weight_null'         =>  __('messages.trackings.weight_null'),
            'cubic_null'          =>  __('messages.trackings.cubic_null'),
            'duplicate'           =>  __('messages.trackings.duplicate'),
            'stuck'               =>  __('messages.trackings.stuck'),
            'user_remark'         =>  __('messages.trackings.user_remark'),
            'admin_remark'        =>  __('messages.trackings.admin_remark'),
            'system_remark'       =>  __('messages.trackings.system_remark'),
            'include_deleted'     =>  __('messages.trackings.include_deleted'),
            'deleted'             =>  __('messages.trackings.deleted'),
            'duplicate_track'     =>  __('messages.trackings.duplicate_track'),
            'type'                =>  __('messages.trackings.type'),
            'waiting_confirm'     =>  __('messages.trackings.waiting_confirm'),
        ];
        return $array;
    }

    public static function dateTypeList()
    {
        $array = [

            'china_in'   => __('messages.trackings.china_in'),
            'china_out'  => __('messages.trackings.china_out'),
            'thai_in'    => __('messages.trackings.thai_in'),
            'thai_out'   => __('messages.trackings.thai_out'),
            'created_at' => __('messages.trackings.created_at'),
            'updated_at' => __('messages.trackings.updated_at'),
            'packed_at'  => __('messages.trackings.packed_at'),
            'deleted_at' => __('messages.trackings.deleted_at'),
        ];
        return $array;
    }

    public static function calculateTypeList()
    {
        $array = [
            'weight'     => __('messages.trackings.weight'),
            'cubic'      => __('messages.trackings.cubic'),
        ];
        return $array;
    }


    public static function statusList()
    {
        $array = [
            [
                'id' => 1,
                'name' => __('messages.trackings.status.1')
            ],
            [
                'id' => 2,
                'name' => __('messages.trackings.status.2')
            ],
            [
                'id' => 3,
                'name' => __('messages.trackings.status.3')
            ],
            [
                'id' => 4,
                'name' => __('messages.trackings.status.4')
            ],
            [
                'id' => 5,
                'name' => __('messages.trackings.status.5')
            ],
            [
                'id' => 6,
                'name' => __('messages.trackings.status.6')
            ],
        ];
        return $array;
    }

    public static function typeList()
    {
        $array = [

            'clothes'     => __('messages.trackings.clothes'),
            'bag'         => __('messages.trackings.bag'),
            'accessories' => __('messages.trackings.accessories'),
            'electronics' => __('messages.trackings.electronics'),
            'toy'         => __('messages.trackings.toy'),
            'shoe'        => __('messages.trackings.shoe'),
            'spares'      => __('messages.trackings.spares'),
            'cosmetics'   => __('messages.trackings.cosmetics'),
            'other'       => __('messages.trackings.other'),
        ];
        return $array;
    }


    public function getCurrentZoneAttribute()
    {
        $result = explode(" / ", $this->warehouse_zone);
        $current = end($result);
        return trim($current);
    }

    public function getCalculateTextAttribute()
    {
        return collect(self::calculateTypeList())->get($this->calculate_type);
    }

    public function getCurrentStatusAttribute()
    {
        $status = collect(self::statusList())->firstWhere('id', $this->status);
        return (!empty($status['name'])) ? $status['name'] : null;
    }

    // public function getCurrentProductTypeAttribute()
    // {
    //     return collect(self::productType())->get($this->product_type_id);
    // }

    public function getCurrentTypeAttribute()
    {
        return collect(self::typeList())->get($this->type);
    }

    public function getWarehouseTextAttribute()
    {
        return collect(Warehouse::typeList())->get($this->warehouse_id);
    }


    public function getBillNoAttribute()
    {
        if ($this->bill_id){
            $bill_no = Bill::PREFIX.str_pad($this->bill_id, 6, "0", STR_PAD_LEFT);
        }else{
            $bill_no = null;
        }
        return $bill_no;
    }

    public function getNoCodeNoAttribute()
    {
        if ($this->id) {
            $nocode_no = self::NOCODE_PREFIX . str_pad($this->id, 6, "0", STR_PAD_LEFT);
        } else {
            $nocode_no = null;
        }
        return $nocode_no;
    }

    // setter function
    // set null if input blank
    public function setCodeAttribute($code)
    {
        $this->attributes['code'] =  strtoupper(str_replace(' ', '', $code));
    }

    public function setCodeGroupAttribute($code_group)
    {
        $this->attributes['code_group'] =  strtoupper(str_replace(' ', '', $code_group));
    }

    public function setUserCodeAttribute($user_code)
    {
        $this->attributes['user_code'] = trim($user_code) !== '' ?  strtoupper(str_replace(' ', '', $user_code)) : null;
    }

    public function setReferenceCodeAttribute($reference_code)
    {
        $this->attributes['reference_code'] = trim($reference_code) !== '' ?  strtoupper(str_replace(' ', '', $reference_code)) : null;
    }

    public function setWeightAttribute($weight)
    {
        $this->attributes['weight'] = trim($weight) !== '' ? $weight : null;
    }

    public function setWidthAttribute($width)
    {
        $this->attributes['width'] = trim($width) !== '' ? $width : null;
    }

    public function setHeightAttribute($height)
    {
        $this->attributes['height'] = trim($height) !== '' ? $height : null;
    }

    public function setLengthAttribute($length)
    {
        $this->attributes['length'] = trim($length) !== '' ? $length : null;
    }

    public function setCubicAttribute($cubic)
    {
        $this->attributes['cubic'] = trim($cubic) !== '' ? $cubic : null;
    }

    public function setDimensionAttribute($dimension)
    {
        $this->attributes['dimension'] = trim($dimension) !== '' ? $dimension : null;
    }

    public function setChinaInAttribute($china_in)
    {
        $this->attributes['china_in'] = trim($china_in) !== '' ? $china_in : null;
    }

    public function setChinaOutAttribute($china_out)
    {
        $this->attributes['china_out'] = trim($china_out) !== '' ? $china_out : null;
    }

    public function setThaiInAttribute($thai_in)
    {
        $this->attributes['thai_in'] = trim($thai_in) !== '' ? $thai_in : null;
    }

    public function setThaiOutAttribute($thai_out)
    {
        $this->attributes['thai_out'] = trim($thai_out) !== '' ? $thai_out : null;
    }

    public function setChinaCostBoxAttribute($china_cost_box)
    {
        $this->attributes['china_cost_box'] = $china_cost_box ? $china_cost_box : 0;
    }

    public function setChinaCostShippingAttribute($china_cost_shipping)
    {
        $this->attributes['china_cost_shipping'] = $china_cost_shipping ? $china_cost_shipping : 0;
    }

    public function setWarehouseZoneAttribute($warehouse_zone)
    {
        $this->attributes['warehouse_zone'] = strtoupper($warehouse_zone);
    }


    /**
     * relation
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_code', 'code');
    }

    public function productTypes()
    {
        return $this->belongsTo('App\Models\ProductType', 'product_type_id')->select('id', 'title_th');
    }

    public function shippingMethods()
    {
        return $this->belongsTo('App\Models\ShippingMethod', 'shipping_method_id')->select('id', 'title_th');
    }
    public function warehouse()
    {
        return $this->belongsTo('App\Models\Warehouse', 'warehouse_id')->select('id', 'title');
    }
    public function trackingDetail()
    {
        return $this->hasone('App\Models\TrackingDetail');
    }
    public function bill()
    {
        return $this->belongsTo('App\Models\Bill','bill_id');
    }
    public function pivotBill()
    {
        return $this->belongsToMany('App\Models\Bill', 'bill_trackings','tracking_id','bill_id');
    }
    public function images()
    {
        return $this->morphMany('App\Models\Upload', 'relatable');
    }

    public function scopeSearchExtend(Builder $query, $conditions): Builder
    {
        if (isset($conditions['condition']) && (($conditions['condition'] == 'deleted'))) {
            return $query->onlyTrashed();
        }
        if (isset($conditions['condition']) && (($conditions['condition'] == 'include_deleted'))) {
            return $query->withTrashed();
        }
        if (isset($conditions['condition']) && ($conditions['condition'] == 'duplicate_track')) {
            return  $query->join(DB::raw('(SELECT code AS track, COUNT(*) c FROM trackings GROUP BY track HAVING c > 1) dup'), function ($join) {
                $join->on('trackings.code', '=', 'dup.track');
            });
        }
        if (isset($conditions['time_format']) && (($conditions['time_format'] == 'deleted_at'))) {
            if (@$conditions['date_start'] && @$conditions['date_end']) {
                return ScopeQueries::scopeDateRange($query->onlyTrashed(), 'deleted_at', $conditions['date_start'], $conditions['date_end']);
            }
        }
        return $query;
    }

    public function scopeSearch($query, $conditions)
    {
        return $query->where(function ($query) use ($conditions) {

            ScopeQueries::scopeUserCodeBySale($query, 'user_code');

            if (@$conditions['id']) {
                $query->where('id', $conditions['id']);
            }
            if (@$conditions['nocode']) {
                $trimmed = ltrim($conditions['nocode'], self::NOCODE_PREFIX . '0');
                $query->where('id', $trimmed);
            }
            if (@$conditions['code_group']) {
                $query->where('code_group', 'LIKE', "%{$conditions['code_group']}%");
            }
            if (@$conditions['product_type_id']) {
                $query->where('product_type_id', $conditions['product_type_id']);
            }
            if (@$conditions['shipping_method_id']) {
                $query->where('shipping_method_id', $conditions['shipping_method_id']);
            }
            if (@$conditions['code']) {
                $query->where('code', 'LIKE', "%{$conditions['code']}%");
            }
            if (@$conditions['user_code']) {
                $query->where('user_code', 'LIKE', "%{$conditions['user_code']}%");
            }
            if (@$conditions['status']) {
                $query->where('status', $conditions['status']);
            }
            if (@$conditions['calculate_type']) {
                $query->where('calculate_type', $conditions['calculate_type']);
            }
            if (@$conditions['bill_no']) {
                $trimmed = ltrim($conditions['bill_no'], Bill::PREFIX.'0');
                $query->where('bill_id', 'LIKE', '%'.$trimmed.'%');
            }
            $time_format = (@$conditions['time_format']) ? $conditions['time_format'] : 'created_at';
            if($time_format != 'deleted_at'){
                if (@$conditions['date_start'] && @$conditions['date_end']) {
                    ScopeQueries::scopeDateRange($query, $time_format, $conditions['date_start'], $conditions['date_end']);
                }  else if (@$conditions['date_start']) {
                    ScopeQueries::scopeDateStart($query, 'created_at', $conditions['date_start']);
                } else if (@$conditions['date_end']) {
                    ScopeQueries::scopeDateEnd($query, 'created_at', $conditions['date_end']);
                }
            }
            if (@$conditions['warehouse_id']) {
                $query->where('warehouse_id', $conditions['warehouse_id']);
            }
            if (@$conditions['reference_code']) {
                $query->where('reference_code', 'LIKE', "%{$conditions['reference_code']}%");
            }
            if (@$conditions['warehouse_zone']) {
                $query->where('warehouse_zone', 'LIKE', "%{$conditions['warehouse_zone']}%");
            }
            if (@$conditions['user_remark']) {
                $query->where('user_remark', 'LIKE', "%{$conditions['user_remark']}%");
            }
            if (@$conditions['admin_remark']) {
                $query->where('admin_remark', 'LIKE', "%{$conditions['admin_remark']}%");
            }
            if (@$conditions['system_remark']) {
                $query->where('system_remark', 'LIKE', "%{$conditions['system_remark']}%");
            }
            if (@$conditions['weight']) {
                $query->where('weight', 'LIKE', "%{$conditions['weight']}%");
            }
            if (@$conditions['width']) {
                $query->where('width', 'LIKE', "%{$conditions['width']}%");
            }
            if (@$conditions['height']) {
                $query->where('height', 'LIKE', "%{$conditions['height']}%");
            }
            if (@$conditions['length']) {
                $query->where('length', 'LIKE', "%{$conditions['length']}%");
            }
            if (@$conditions['detail']) {
                $query->where('detail', 'LIKE', "%{$conditions['detail']}%");
            }
            if (@$conditions['type']) {
                $query->where('type', $conditions['type']);
            }
            if (@$conditions['staff']) {
                $query->whereRaw('(created_admin like "%' . $conditions['staff'] . '%" OR updated_admin like "%' . $conditions['staff'] . '%" OR packed_admin like "%' . $conditions['staff'] . '%" )');
            }

            if (@$conditions['condition']) {
                switch ($conditions['condition']) {
                    case 'user_code_ref_null':
                        $query->whereRaw('(user_code IS NULL OR user_code = "") AND (reference_code IS NULL OR reference_code = "")');
                        break;
                    case 'user_code':
                        $query->whereRaw('(user_code IS NOT NULL AND user_code != "")');
                        break;
                    case 'type':
                        $query->whereRaw('(type IS NOT NULL AND type != "")');
                        break;
                    case 'cubic_null':
                        $query->whereRaw('cubic = 0 AND ( width > 0 OR height > 0 OR length > 0)');
                        break;
                    case 'user_code_null':
                        $query->whereNull('user_code');
                        break;
                    case 'bill_id':
                        $query->whereNotNull('bill_id');
                        break;
                    case 'duplicate':
                        $query->whereRaw('duplicate_order != "" AND user_remark IS NOT NULL');
                        break;
                    case 'duplicate_check':
                        $query->where('duplicate_check', 0);
                        break;
                    case 'bill_id_null':
                        $query->whereNull('bill_id');
                        break;
                    case 'china_cost_shipping':
                        $query->where('china_cost_shipping', '>', 0);
                        break;
                    case 'china_cost_box':
                        $query->where('china_cost_box', '>', 0);
                        break;
                    case 'weight_null':
                        $query->whereNull('weight');
                        break;
                    case 'user_remark':
                        $query->whereRaw('user_remark != "" AND user_remark IS NOT NULL');
                        break;
                    case 'admin_remark':
                        $query->whereRaw('admin_remark != "" AND admin_remark IS NOT NULL');
                        break;
                    case 'system_remark':
                        $query->whereRaw('system_remark != "" AND system_remark IS NOT NULL');
                        break;
                    case 'stuck':
                        $query->whereNull('user_code')
                            ->whereRaw('DATEDIFF(CURDATE(), thai_in) > 14');
                        break;
                    case 'waiting_confirm':
                        $query->where('status', 2);
                        $query->whereRaw('warehouse_zone != "" AND warehouse_zone IS NOT NULL');
                        break;
                }
            }
        });
    }

    /**
     * Scope a query to only include auto user code.
     */
    public function scopeStuck($query, $diff_day = 5)
    {
        return $query->whereIn('status', [2, 3])
            ->whereNull('user_code')
            ->whereRaw('reference_code != "" AND reference_code IS NOT NULL')
            ->whereRaw('DATEDIFF(CURDATE(), thai_in) > '.$diff_day);
    }
}
