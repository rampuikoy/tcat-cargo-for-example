<?php

namespace App\Interfaces;

interface PointOrderInterface
{
    const DEFAULT_LIMIT = 25;
    const DEFAULT_SORTING = 'id';
    const DEFAULT_ASCENDING = 'desc';

    /**
     * fetch all pointorder
     *
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function all(array $columns = ['*'], $sorting = PointOrderInterface::DEFAULT_SORTING, $ascending = PointOrderInterface::DEFAULT_ASCENDING);

    /**
     * list pointorder
     *
     * @param array $conditions
     * @param array $columns
     * @param int $offset
     * @param int $limit
     *
     * @return \Illuminate\Support\Collection
     */
    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * pagination pointorder
     *
     * @param array $conditions
     * @param array $columns
     * @param int $page
     * @param int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $perPage = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * find pointorder by id
     *
     * @param $id
     * @param array $columns
     *
     * @return object|null
     */
    public function findById($id, array $columns = ['*']);

    /**
     * find pointorder by code
     *
     * @param $code
     * @param array $columns
     *
     * @return object|null
     */
    public function findByCode($id, array $columns = ['*']);

    /**
     * get pointorder dropdown list
     *
     * @return dropdown
     */
    public function dropdownList();

    /**
     * create new pointorder
     *
     * @param array $data
     *
     * @return object|null
     */
    public function store(array $data);

    /**
     * update pointorder by id
     *
     * @param $id
     * @param array $data
     *
     * @return object
     */
    public function updateById($id, array $data);

    /**
     * check point order by user code
     *
     * @param string $userCode
     * @param int $productId
     * @param int $amount
     *
     * @return object|null
     */
    public function checkLimit(string $userCode, int $productId, int $amount);

    /**
     * cancel pointorder by id
     *
     * @param $id
     *
     * @return object
     */
    public function cancelById($id);

    /**
     * approve pointorder by id
     *
     * @param $id
     *
     * @return object
     */
    public function approveById($id);

    /**
     * update status to packed by code
     *
     * @param $code
     *
     * @return object
     */
    public function packByCode($code);

    /**
     * update status to shiped by code
     *
     * @param $code
     * @param array $data
     *
     * @return object
     */
    public function shipByCode($code, array $data);

    /**
     * update status to confirm by code
     *
     * @param $code
     * @param array $data
     *
     * @return object
     */
    public function confirmByCode($code, array $data);

    /**
     * update status to success by code
     *
     * @param $code
     *
     * @return object
     */
    public function successByCode($code);

    /**
     * fetchListByIds bill
     *
     * @param $ids
     *
     * @return object|null
     */
    public function fetchListByIds(array $ids);
}
