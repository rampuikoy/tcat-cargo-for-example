<?php

namespace App\Repositories;

use App\Models\Trip;
use App\Models\User;
use App\Models\Credit;
use App\Models\Payment;
use App\Helpers\Util;
use App\Models\TripOrder;
use App\Jobs\JobSendEmail;
use App\Models\EmailQueue;
use App\Traits\CacheTrait;
use App\Interfaces\TripInterface;
use App\Traits\CoreResponseTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;


class TripRepository implements TripInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const MINUTE_CACHE          = 0;
    const TABLE_RELETIONS       = ['orders', 'credits', 'user', 'payments'];

    public function __construct(Trip $trip, TripOrder $tripOrder, EmailQueue $emailQueue, User $user, Credit $credit)
    {
        $this->trip             = $trip;
        $this->tripOrder        = $tripOrder;
        $this->emailQueue       = $emailQueue;
        $this->user             = $user;
        $this->credit           = $credit;
    }

    public function all(array $columns = ['*'], $sorting = TripInterface::DEFAULT_SORTING, $ascending = TripInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $trips     = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])
            ->remember($key, $time, function () use ($columns, $sorting, $ascending) {
                return $this->trip::select($columns)->orderBy($sorting, $ascending)->get();
            });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.trip')]), $trips);
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = tripInterface::DEFAULT_LIMIT, $sorting = tripInterface::DEFAULT_SORTING, $ascending = tripInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $trips     = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
            return $this->trip::select($columns)
                ->search($conditions)
                ->orderBy($sorting, $ascending)
                ->skip($offset)
                ->limit($limit)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.trip')]), $trips);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = tripInterface::DEFAULT_LIMIT, $sorting = tripInterface::DEFAULT_SORTING, $ascending = tripInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $trips     = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
            $model = $this->trip::select($columns)
                ->search($conditions)
                ->orderBy($sorting, $ascending);

            if ($page == 1) {
                return $model->paginate($limit);
            } else {
                return $model->skip($limit * ($page - 1))->paginate($limit);
            }
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.trip')]), $trips);
    }

    public function findById($id, array $columns = ['*'])
    {
        $trip = $this->findByIdMethod($id, $columns);

        if (!$trip) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.trip'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.trip')]), $trip);
    }

    public function dropdownList()
    {
        $key        = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $dropdown   = Cache::remember($key, $time, function () {
            return [
                'billList'         => $this->trip->texeBilllList(),
                'status'           => $this->trip->statusList(),
                'tripType'         => $this->trip->tripType(),
                'textCondition'    => $this->trip->textCondition(),
                'tripOrder'        => $this->tripOrder->tripType()
            ];
        });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function store(array $data)
    {

        $total = collect($data['orders'])
            ->map(function ($order) {
                $total = $order['price'] * $order['amount'];
                return $total;
            })->sum();
        if ($data['withholding']) {
            $subTotal =   $total - ($total * 0.03);
        } else {
            $subTotal =  $total;
        }
        $data = collect($data)->merge([
            'total' => $subTotal ? $subTotal : 0,
            'sub_total' => $subTotal ? $subTotal : 0,
        ]);

        $tripCollect   = collect($data);
        $dataTrip      = $tripCollect->except(['orders']);
        $trip          = $this->trip::create($dataTrip->all());
        $this->clearCacheTags();

        if ($trip->id) {
            collect($data['orders'])
                ->map(function ($order) use ($trip) {
                    $order = new  $this->tripOrder($order);
                    $order->total = $order['price'] * $order['amount'];
                    $trip->orders()->save($order);
                });
        }
        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.trip')]), $trip);
    }

    public function updateById($id, array $data)
    {
        $trip      = $this->findByIdMethod($id);
        if (!$trip) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.trip'), 'id' => $id]));
        }
        @$data['admin_remark'] = Util::concatField($trip->admin_remark, @$data['admin_remark']);

        $trip->update($data);
        Cache::tags([$this->getIdWithClassPrefix($trip->id)])->flush();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.trip')]), $trip);
    }


    public function notifyCreate($id, array $data)
    {
        $trip      = $this->findByIdMethod($id);
        if (!$trip) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.trip'), 'id' => $id]));
        }
        app('App\Repositories\PaymentRepository')->store($data);
        $trip->status = 2;
        $trip->save();
        Cache::tags([$this->getIdWithClassPrefix($trip->id)])->flush();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.trip')]), $trip);
    }

    public function notifyClose($id)
    {
        $trip      = $this->findByIdMethod($id);
        if (!$trip) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.trip'), 'id' => $id]));
        }
        $trip->status = 4;
        $trip->save();
        Cache::tags([$this->getIdWithClassPrefix($trip->id)])->flush();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.trip')]), $trip);
    }

    public function notifyDetail($id)
    {
        $trip      = $this->findByIdMethod($id);
        if (!$trip) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.trip'), 'id' => $id]));
        }
        $user      = $trip->user;
        $email      = new $this->emailQueue([
            'mailable_class'    => 'App\Mail\Trip\TripSuccess',
            'status'            => 'queue',
            'to'                => $user->email,
            'created_admin'     => $trip->created_admin,
            'created_admin_id'  => $trip->created_admin_id,
        ]);
        $trip->emails()->save($email);
        JobSendEmail::dispatch($email->id)->onQueue(env('QUEUE_EMAIL_PREFIX'));

        Cache::tags([$this->getIdWithClassPrefix($trip->id)])->flush();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.trip')]), $trip);
    }

    public function postCancel($id, array $data)
    {
        $trip      = $this->findByIdMethod($id);
        if (!$trip) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.trip'), 'id' => $id]));
        }

        if ($trip->payments()->count() > 0) {
        $paid       = collect($trip->payments);
        $cash       = $paid->pluck('cash')[0];
        $after      = $paid->pluck('after')[0];
        $amount     = $paid->pluck('amount')[0];
        $befores    = $paid->pluck('before')[0];
        }

        $trip->status = 5;
        $trip->admin_remark = Util::concatField($trip->admin_remark, $data['admin_remark']);
        $admin =  Auth::user();
        if ($trip->credits()->count() > 0) {
            collect($trip->credits)
                ->map(function ($credits) use ($admin, $trip, $amount, $cash, $after, $befores) {

                    $before              = app('App\Repositories\UserRepository')->findByCodeMethod($credits->user_code)->credit->total ?? 0;
                    $credit              = new $this->credit();
                    $credit->user_code   = $trip->user_code;
                    $credit->amount      = $after == 0  && $cash != 0 ?  ($amount - $befores) + $credits->amount :  $credits->amount;
                    $credit->before      = $before;
                    if ($credits->type == 'withdraw') {
                        $credit->type  = 'topup';
                        $credit->after =  $before + $credit->amount;
                    } else {
                        $credit->type  = 'withdraw';
                        $credit->after = $before - $credit->amount;
                    }

                    $credit->created_admin     = $admin->username;
                    $credit->created_admin_id  = $admin->id;
                    $credit->approved_at       = date('Y-m-d');
                    $credit->approved_admin    = $admin->username;
                    $credit->approved_admin_id = $admin->id;

                    $trip->credits()->save($credit);
                    app('App\Repositories\CreditRepository')->clearCacheTagByUserCode($trip->user_code);
                    app('App\Repositories\UserRepository')->clearCacheTagByUserCode($trip->user_code);
                });
        }
        if ($trip->payments()->count() > 0) {
            collect($trip->payments)->map(function ($trips) {
                $payment = app('App\Repositories\PaymentRepository')->findByIdMethod($trips->id);
                $payment->status            = 'cancel';
                $payment->save();
            });
        }
        $trip->save();
        Cache::tags([$this->getIdWithClassPrefix($trip->id)])->flush();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.trips.trip_cancel')]), $trip);
    }

    public function optionEdit($id, array $columns = ['*'])
    {
        $trip = $this->findByIdMethod($id, $columns);

        if (!$trip) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.trip'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.trip')]), $trip);
    }

    public function softDeleteById($id)
    {
        $trip      = $this->findByIdMethod($id);

        if (!$trip) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.trip'), 'id' => $id]));
        }

        $trip->delete();
        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.delete_data', ['data' => __('messages.trip')]), $trip);
    }

    private function clearCacheTags()
    {
        Cache::tags([
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix()
        ])
            ->flush();
    }

    public function findByIdMethod($id, array $columns = ['*'])
    {
        $key        = $this->getCacheKey(null, $id);
        $time       = $this->getTime(self::MINUTE_CACHE);

        $trip      = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($id)
        ])
            ->remember($key, $time, function () use ($id, $columns) {
                return $this->trip::select($columns)->with(self::TABLE_RELETIONS)->whereId($id)->first();
            });

        return $trip;
    }

    public function findByUserCode($userCode, array $columns = ['*'])
    {
        $trip = $this->findByUserCodeMethod($userCode, $columns);
        if ($trip->isEmpty()) {
            return $this->coreResponse(404, __('messages.no_data_with_user_code', ['data' => __('messages.trip'), 'user_code' => $userCode]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.trip')]), $trip);
    }

    private function findByUserCodeMethod($userCode, $columns = ['*'])
    {
        $key        = $this->getCacheKey(null,  $userCode);
        $time       = $this->getTime(self::MINUTE_CACHE);

        $trip      = Cache::tags([
            $this->getClassPrefix(),
            $this->getUserCodePrefix($userCode),
        ])->remember($key, $time, function () use ($userCode, $columns) {
            return $this->trip::select($columns)
                ->with(self::TABLE_RELETIONS)
                ->whereUserCode($userCode)
                ->get();
        });

        return $trip;
    }
}
