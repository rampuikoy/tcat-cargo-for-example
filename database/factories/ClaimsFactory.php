<?php

namespace Database\Factories;

use App\Models\Claims;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

class ClaimsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Claims::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $sufUser = [
            'SF0002',
            'SF0003',
            'SF0004',
            'SF0005',
            'SF0006',
        ];

        $sufTypeStatus = ['waiting', 'cancel', 'approved'];
        $sufTypeShipping = ['car', 'ship'];
        $sufType = ['shiping_price', 'coupon', 'product_price'];

        return [
            //
            'user_code' => Arr::random($sufUser),
            'status' => Arr::random($sufTypeStatus),
            'shipping' => Arr::random($sufTypeShipping),
            'city' => $this->faker->title(),
            'china_in' => $this->faker->dateTimeBetween('-30 days', '+0 days'),
            // 'china_bill',
            // 'shop_bill',
            // 'shipping_bill',
            // 'images',
            'detail' => $this->faker->title(),
            'remark' => $this->faker->title(),
            'type' => Arr::random($sufType),
            'total' => random_int(100, 10000) / 100,
            'sub_total' => random_int(100, 10000) / 100,
            // 'created_admin',
            // 'updated_admin',
            // 'approved_admin',
            // 'claimed_admin',
            // 'contacted_admin',
            // 'created_admin_id',
            // 'updated_admin_id',
            // 'approved_admin_id',
            // 'claimed_admin_id',
            'approved_at' => $this->faker->dateTimeBetween('-30 days', '+0 days'),
        ];
    }
}
