<?php

namespace App\Http\Controllers\Backend\Notification;

use App\Http\Controllers\Controller;
use App\Http\Requests\NotiQueue\UpdateManyQueueRequest;
use App\Interfaces\FcmQueueInterface;
use App\Policies\NotificationPolicy;
use Illuminate\Http\Request;

class FcmQueueController extends Controller
{

    protected $fcmQueueInterface;

    public function __construct(FcmQueueInterface $fcmQueueInterface)
    {
        $this->fcmQueueInterface = $fcmQueueInterface;
    }

    public function search(Request $request)
    {
        $this->authorize('index', NotificationPolicy::class);
        $conditions     = $request->only(['id', 'user_code', 'status', 'title', 'message', 'send', 'date_start', 'date_end']);
        $columns        = $request->input('columns', ['*']);
        $limit          = $request->input('limit', $this->fcmQueueInterface::DEFAULT_LIMIT);
        $sorting        = $request->input('order_by', $this->fcmQueueInterface::DEFAULT_SORTING);
        $ascending      = $request->input('ascending') == 1 ? 'asc' : 'desc';
        $offset         = $request->input('page', 0);

        if ($request->boolean('pagination')) {
            $page = $request->input('page', 1);
            $fcmQueue = $this->fcmQueueInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset = $request->input('page', 0);
            $fcmQueue = $this->fcmQueueInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }

        return response()->json($fcmQueue, $fcmQueue->code);
    }

    public function dropdown()
    {
        $dropdown = $this->fcmQueueInterface->dropdownList();
        return response()->json($dropdown, $dropdown->code);
    }

    public function updateManyQueue(UpdateManyQueueRequest $request)
    {
        $data       = $request->input('ids', []);
        $fcmQueue   = $this->fcmQueueInterface->updateManyQueue($data);
        return response()->json($fcmQueue, $fcmQueue->code);
    }
}
