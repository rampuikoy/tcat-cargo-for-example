<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seo', function (Blueprint $table) {
            $table->id('id');
            $table->string('title', 255);
            $table->text('keyword');
            $table->text('description');
            $table->text('code');
            $table->text('footer');
            $table->string('created_admin', 20)->nullable();
            $table->string('updated_admin', 20)->nullable();
            $table->integer('created_admin_id')->nullable()->unsigned();
            $table->integer('updated_admin_id')->nullable()->unsigned();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seo');
    }
}
