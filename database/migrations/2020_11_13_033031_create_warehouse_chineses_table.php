<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWarehouseChinesesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse_chineses', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('image')->default('default.png');
            $table->string('title_th')->nullable();
            $table->string('title_en')->nullable();
            $table->string('title_cn')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->text('info_th')->nullable();
            $table->text('info_en')->nullable();
            $table->text('info_cn')->nullable();
            $table->text('remark')->nullable();
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->integer('created_admin_id')->unsigned()->nullable();
            $table->integer('updated_admin_id')->unsigned()->nullable();
            $table->string('created_admin', 50)->nullable();
            $table->string('updated_admin', 50)->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouse_chineses');
    }
}
