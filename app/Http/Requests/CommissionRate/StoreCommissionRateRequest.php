<?php

namespace App\Http\Requests\CommissionRate;

use App\Http\Requests\FormRequest;

class StoreCommissionRateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('extend.commission-rate');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'kg_car_genaral'     => 'required',
            'kg_car_iso'         => 'required',
            'kg_car_brand'       => 'required',
            'kg_ship_genaral'    => 'required',
            'kg_ship_iso'        => 'required',
            'kg_ship_brand'      => 'required',
            'cubic_car_genaral'  => 'required',
            'cubic_car_iso'      => 'required',
            'cubic_car_brand'    => 'required',
            'cubic_ship_genaral' => 'required',
            'cubic_ship_iso'     => 'required',
            'cubic_ship_brand'   => 'required',
            'bill_id'            => 'exists:bills,id',
            'admin_id'           => 'exists:admins,id',
        ];
    }
}
