<?php

namespace App\Observers;

use App\Models\Product;
use App\Traits\UploadTrait;
use App\Observers\BaseObserver;

class ProductObserver extends BaseObserver
{
    use UploadTrait;

    /**
     * Handle the product "creating" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function creating(Product $product)
    {
        $this->stampCreated($product);
    }

    /**
     * Handle the product "updating" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function updating(Product $product)
    {
        $this->stampUpdated($product);
    }

    /**
     * Handle the product "updated" event.
     *
     * @param  \App\Models\Product  $product
     * @return void
     */
    public function updated(Product $product)
    {
        if ($product->isDirty('image')) {
            $oldFilePath = $product->getOriginal('image');
            $this->removeFile($product->getFileUrlAttribute($oldFilePath));
        }
    }
}
