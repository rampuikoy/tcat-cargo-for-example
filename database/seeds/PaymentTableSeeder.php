<?php

namespace Database\Seeders;

use App\Models\Bill;
use App\Models\Payment;
use Illuminate\Database\Seeder;

class PaymentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Payment::factory(5)->create();
        // $payments = Payment::all();
        // $payment = $payments[1];
        // $bill = Bill::find(148);
        // $payment->trackingBills()->save($bill);
    }
}
