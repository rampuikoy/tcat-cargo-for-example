<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTopupAutopaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topup_autopays', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('topup_id')->unsigned()->nullable();
            $table->string('user_code')->nullable();
            $table->integer('payable_id')->unsigned();

            $table->string('payable_type');
            $table->enum('status',['waiting','success','failed'])->default('waiting');

            $table->string('created_admin', 20);
            $table->string('updated_admin', 20)->nullable();

            $table->integer('created_admin_id')->unsigned()->nullable();
            $table->integer('updated_admin_id')->unsigned()->nullable();
            $table->text('remark')->nullable();

            $table->foreign('topup_id')->references('id')->on('topups')->onDelete('set null');
            $table->foreign('created_admin_id')
                ->references('id')->on('admins')->onDelete('set null');
            $table->foreign('updated_admin_id')
                ->references('id')->on('admins')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topup_autopays');
    }
}
