<?php

namespace App\Repositories;

use App\Interfaces\AmphurInterface;
use App\Models\Amphur;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use Illuminate\Support\Facades\Cache;

class AmphurRepository implements AmphurInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const MINUTE_CACHE = 1;

    public function __construct(Amphur $amphur)
    {
        $this->amphur = $amphur;
    }

    public function all(array $columns = ['*'], $sorting = AmphurInterface::DEFAULT_SORTING, $ascending = AmphurInterface::DEFAULT_ASCENDING)
    {
        $key            = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time           = $this->getTime(self::MINUTE_CACHE);

        $allAmphur = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix(),])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
            return $this->amphur::select($columns)->orderBy($sorting, $ascending)->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.amphur')]), $allAmphur);
    }

    public function findManyByProvinceId($id, array $columns = ['*'])
    {
        $key = $this->getCacheKey(null, $id);
        $time = $this->getTime(self::MINUTE_CACHE);

        $amphurList = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($id)
        ])->remember($key, $time, function () use ($id, $columns) {
            return $this->amphur::select($columns)
                ->where('province_id', $id)
                ->get();
        });

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.amphur')]), $amphurList);
    }

    public function findById($id, array $columns = ['*'])
    {
        $amphur = $this->findByIdMethod($id);

        if (!$amphur) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.amphur'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.amphur')]), $amphur);
    }


    private function findByIdMethod($id, array $columns = ['*'])
    {
        $key        = $this->getCacheKey(null, $id);
        $time       = $this->getTime(self::MINUTE_CACHE);

        $amphur    = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($id)
        ])->remember($key, $time, function () use ($id, $columns) {
            return $this->amphur::select($columns)->whereId($id)->first();
        });

        return $amphur;
    }
}
