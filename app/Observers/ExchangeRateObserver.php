<?php

namespace App\Observers;

use App\Models\ExchangeRate;
use App\Observers\BaseObserver;

class ExchangeRateObserver extends BaseObserver
{
    public function creating(ExchangeRate $exchangeRate)
    {
        //
        $this->stampCreated($exchangeRate);
    }

    /**
     * Handle the product_type "updating" event.
     *
     * @param  \App\Models\ProductType  $product_type
     * @return void
     */
}
