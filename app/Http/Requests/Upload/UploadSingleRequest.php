<?php

namespace App\Http\Requests\Upload;

use App\Http\Requests\FormRequest;
use App\Rules\Base64Image;

class UploadSingleRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'type'          => 'required|in:image,file',
            'file_path'     => ['required_if:type,image', new Base64Image]
            // 'file' => 'required_if:type,file|mimes:jpeg,jpg,gift,png,csv,xlsx,xls,docx',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'type'      => __('form.upload.type'),
            'file_path'      => __('form.upload.file'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
