<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\ExchangeRate;
use App\Models\Admin;
use Illuminate\Support\Facades\File;
use Tymon\JWTAuth\Facades\JWTAuth;

class ExchangeRateTest extends TestCase
{
  use WithFaker;
  protected $user;
  protected $exchangeRate;
  protected $url = '/api/backend/exchange-rate';
  /**
   * A basic feature test example.
   *
   * @return void
   */
  protected function authenticate(string $guard = 'admins')
  {
    $this->user = factory(Admin::class)->create();
    auth()->shouldUse($guard);
    $this->exchangeRate = ExchangeRate::create([
      'rate'          => 0.01,
      'raw_rate'    => 0.02,
    ]);
    $token = JWTAuth::fromUser($this->user);
    return 'Bearer ' . $token;
  }

  protected function deleteUser()
  {
    $this->exchangeRate->forceDelete();
    $this->user->forceDelete();
  }

  /** @test */
  public function GetExchangeRateSearchSuccessTest()
  {
    $id = random_int(1, 200);
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', "{$this->url}/search", [
      "id" => $id,
      "search" => "",
    ]);
    $response->assertStatus(200);
    $this->deleteUser();
  }

   /** @test */
  public function GetExchangeRateSearchFailTest()
  {
    $response = $this->json('GET', "{$this->url}/search");
    $response->assertStatus(401);
  }

  /** @test */
  public function GetExchangeRateAllSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', $this->url);
    $response->assertStatus(200);
    $this->deleteUser();
  }

   /** @test */
  public function GetExchangeRateAllFailTest()
  {
    $response = $this->json('GET', $this->url);
    $response->assertStatus(401);
  }

  /** @test */
  public function ExchangeRatePostSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('POST', $this->url, [
      'rate'          => 0.05,
      'raw_rate'    => 0.03,
    ]);
    $response->assertStatus(200);
    ExchangeRate::where('rate', 0.05)->forceDelete();
    $this->deleteUser();
  }

   /** @test */
  public function ExchangeRatePostFailAuthTest()
  {
    $response = $this->json('POST', $this->url);
    $response->assertStatus(401);
  }

  /** @test */
  public function ExchangeRatePostFailTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('POST', $this->url);
    $response->assertStatus(422);
    $this->deleteUser();
  }

  /** @test */
  public function ExchangeRateUpdateByIdSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('PUT', "{$this->url}/{$this->exchangeRate->id}", [
      'rate'          => 0.01,
      'raw_rate'    => 0.02,
    ]);
    $response->assertStatus(200);
    $this->deleteUser();
  }

   /** @test */
  public function ExchangeRateUpdateByIdFailAuthTest()
  {
    $response = $this->json('PUT', "{$this->url}/5");
    $response->assertStatus(401);
  }

  /** @test */
  public function ExchangeRateUpdateByIdFailTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' =>  $token,
    ])->json('PUT', "{$this->url}/{$this->exchangeRate->id}");
    $response->assertStatus(422);
    $this->deleteUser();
  }

  /** @test */
  public function ExchangeRateDeleteSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('DELETE', "{$this->url}/{$this->exchangeRate->id}");
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function ExchangeRateDeleteFailTest()
  {
    $response = $this->json('DELETE', "{$this->url}/21");
    $response->assertStatus(401);
  }
}
