@component('mail::message')
# เรียนคุณ {{$user->name}}

รหัสลูกค้าของคุณคือ: {{$user->code}} 

ลูกค้าสามารถศึกษาการใช้งานระบบได้จาก 

@component('mail::button', ['url' => 'https://www.youtube.com/watch?v=fw8XF36RnjM'])
วีดีโอแนะนำระบบลูกค้า
@endcomponent
@endcomponent
