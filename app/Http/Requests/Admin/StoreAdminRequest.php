<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\FormRequest;
use App\Rules\Base64Image;

class StoreAdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.admin.store');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'name'          => 'required|max:191',
            'lastname'      => 'sometimes|required',
            'email'         => 'required|email|unique:admins,email',
            'password'      => 'required|confirmed|min:4|max:20|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9]).*$/',
            'role'          => 'required|exists:roles,name',
            'photo'         => ['nullable', new Base64Image],

            'username'      => 'required|min:4|max:20|unique:admins,username',
            'tel'           => 'sometimes|nullable|string|max:20',
            'department'    => 'sometimes|in:tdar,cargo,taobao,tmall,alibaba,express,china',
            'droppoint_id'  => 'sometimes|nullable|exists:thai_shipping_methods,id',
            'branch'        => 'sometimes|in:headquarter,branch',
            'type'          => 'sometimes|in:admin,staff,accountant,warehouse,sale,driver',
            'status'        => 'sometimes|in:active,inactive',
            'regex_user'    => 'sometimes|in:active,inactive',
            'pattern_user'  => 'required_if:regex_user,active|max:20',
            'remark'        => 'sometimes|nullable|string',                
        ];
        return $rules;
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name'          => __('form.admin.name'),
            'lastname'      => __('form.admin.lastname'),
            'email'         => __('form.admin.email'),
            'password'      => __('form.admin.password'),
            'role'          => __('form.admin.role'),
            'photo'         => __('form.admin.photo'),

            'username'      => __('form.admin.username'),
            'tel'           => __('form.admin.phone'),
            'department'    => __('form.admin.department'),
            'droppoint_id'  => __('form.admin.droppoint_id'),
            'branch'        => __('form.admin.branch'),
            'type'          => __('form.admin.type'),
            'status'        => __('form.admin.status'),
            'regex_user'    => __('form.admin.regex_user'),
            'pattern_user'  => __('form.admin.pattern_user'),
            'remark'        => __('form.admin.remark'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
