<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $suf_status          = ['active', 'inactive'];
        return [
            //
            'title'         => $this->faker->word(),
            'code'          => $this->faker->numerify('Point-####'),
            'image'         => 'default.png',
            'description'   => $this->faker->sentence(6),
            'stock'         => $this->faker->numberBetween(1, 10),
            'limit'         => $this->faker->numberBetween(1, 10),
            'point'         => $this->faker->numberBetween(10, 100),
            'status'        => Arr::random($suf_status),
            'remark'        => 'REMARK : ' . $this->faker->sentence(10),
        ];
    }
}
