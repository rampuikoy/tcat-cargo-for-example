<?php

namespace Database\Factories;

use App\Models\PaymentType;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;
use Carbon\Carbon;

class PaymentTypeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PaymentType::class;

    const SUF_NAME = ['เครดิต','เครดิต/เงินสด', 'คิวอาร์โค๊ด'];

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        $name     = Arr::random(self::SUF_NAME);
        $timestamp  = Carbon::now();

        $paymentType = [
            'name'                  =>  $name,
            'created_at'            =>  $timestamp,
            'updated_at'            =>  $timestamp,

        ];

        return $paymentType;
    }
}
