<?php

namespace App\Models;

use App\Helpers\ScopeQueries;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'code',
        'user_code',
        'shipping_method_id',
        'limit_user_code',
        'limit_shipping_method',
        'type',
        'price',
        'bill',
        'bill_min_price',
        'start_date',
        'end_date',
        'limit',
        'counter',
        'remark',
        'status',
        'created_admin_id',
        'updated_admin_id',
        'created_admin',
        'updated_admin',
    ];

    public static function typeList()
    {
        $array = [
            'one_time'  => __('messages.coupons.type.one_time'),
            'life_time' => __('messages.coupons.type.life_time'),
        ];
        return $array;
    }

    public static function statusList()
    {
        $array = [
            'active' => __('messages.active'),
            'inactive' => __('messages.inactive'),
        ];
        return $array;
    }

    public static function limitList()
    {
        $array = [
            'yes' =>  __('messages.coupons.limit.yes'),
            'no'  => __('messages.coupons.limit.no'),
        ];
        return $array;
    }

    public static function billList()
    {
        $array = [
            'shipping'       => __('messages.coupons.bill.shipping'),
            'money_transfer' => __('messages.coupons.bill.money_transfer'),
            'trip'           => __('messages.coupons.bill.trip'),
            'all'            => __('messages.coupons.bill.all'),
        ];
        return $array;
    }
    public static function conditionList()
    {
        $array = [
            'active' => __('messages.coupons.condition.active'),
        ];
        return $array;
    }

    protected $appends = ['current_type', 'current_status', 'current_bill', 'current_user_limit', 'current_shipping_limit', 'current_thai_shipping'];

    public function getCurrentStatusAttribute()
    {
        return collect(self::statusList())->get($this->status);
    }

    public function getCurrentThaiShippingAttribute()
    {
        if ($this->shipping_method_id !== null) {
            $thaiShipping = ThaiShippingMethod::get();
            $title = collect($thaiShipping)->firstWhere('id', $this->shipping_method_id);
            return $title['title'];
        }
    }


    public function getCurrentTypeAttribute()
    {
        return collect(self::typeList())->get($this->type);
    }

    public function getCurrentBillAttribute()
    {
        return collect(self::billList())->get($this->bill);
    }

    public function getCurrentUserLimitAttribute()
    {
        return collect(self::limitList())->get($this->limit_user_code);
    }

    public function getCurrentShippingLimitAttribute()
    {
        return collect(self::limitList())->get($this->limit_shipping_method);
    }

    public function getCounterAttribute()
    {
        return $this->hasMany('App\Models\Bill', 'coupon_id')->where('status', '!=', 8)->count();
    }

    public function setUserCodeAttribute($value)
    {
        $this->attributes['user_code'] = empty($value) ? null : $value;
    }

    public function setShippingMethodIdAttribute($value)
    {
        $this->attributes['shipping_method_id'] = empty($value) ? null : $value;
    }

    public function bills()
    {
        return $this->hasMany('App\Models\Bill', 'coupon_id');
    }

    public function shippingMethod()
    {
        return $this->belongsTo('App\Models\ThaiShippingMethod', 'shipping_method_id');
    }

    public function scopeSearch($query, $conditions)
    {
        return $query->where(function ($query) use ($conditions) {
            if (@$conditions['type']) {
                $query->whereType($conditions['type']);
            }
            if (@$conditions['title']) {
                $query->where('title', 'like', '%' . $conditions['title'] . '%');
            }
            if (@$conditions['code']) {
                $query->where('code', 'like', '%' . $conditions['code'] . '%');
            }
            if (@$conditions['user_code']) {
                $query->where('user_code', 'like', '%' . $conditions['user_code'] . '%');
            }
            if (@$conditions['price']) {
                $query->where('price', '=', $conditions['price']);
            }
            if (@$conditions['bill_min_price']) {
                $query->where('bill_min_price', '=', $conditions['bill_min_price']);
            }
            if (@$conditions['bill']) {
                $query->where('bill', $conditions['bill']);
            }
            if (@$conditions['status']) {
                $query->where('status', $conditions['status']);
            }
            if (@$conditions['limit_user_code']) {
                $query->where('limit_user_code', $conditions['limit_user_code']);
            }
            if (@$conditions['limit_shipping_method']) {
                $query->where('limit_shipping_method', $conditions['limit_shipping_method']);
            }
            if (@$conditions['shipping_method_id']) {
                $query->where('shipping_method_id', $conditions['shipping_method_id']);
            }

            if (@$conditions['date_start']) {
                $query->whereStartDate($conditions['date_start']);
            }
            if (@$conditions['date_end']) {
                $query->whereEndDate($conditions['date_end']);
            }

            if (@$conditions['limits']) {
                $query->where('limit', $conditions['limits']);
            }
            if (@$conditions['remark']) {
                $query->where('remark', 'like', '%' . $conditions['remark'] . '%');
            }
            if (@$conditions['condition']) {
                switch ($conditions['condition']) {
                    case 'active':
                        $query->whereRaw('DATE(NOW()) BETWEEN start_date AND end_date');
                        break;
                }
            }
        });
    }
}
