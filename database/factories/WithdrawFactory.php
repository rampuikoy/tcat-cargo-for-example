<?php

namespace Database\Factories;

use App\Models\Admin;
use App\Models\Withdraw;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;
use Carbon\Carbon;

class WithdrawFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Withdraw::class;

    const SUF_METHOD = ['transfer', 'cash', 'cheque', 'credit', 'other'];
    const SUF_STATUS = ['waiting', 'approved']; //, 'cancel'
    
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $admin      = Admin::whereRole('SuperAdmin')->first();
        $status     = Arr::random(self::SUF_STATUS);
        $timestamp  = Carbon::now();
        $amount     = $this->faker->randomFloat($nbMaxDecimals = 2, $min = 100, $max = 200);
        $charge     = 10;

        $withdraw = [
            'method'            => Arr::random(self::SUF_METHOD),
            'status'            => $status,
            'amount'            => $amount,
            'charge'            => $charge,
            'receive'           => round($amount - $charge, 2),
            'user_remark'       => $this->faker->text($maxNbChars = 30),
            'admin_remark'      => $this->faker->text($maxNbChars = 30),
            'system_remark'     => $this->faker->text($maxNbChars = 30),
            
            'created_admin'     => $admin->name,
            'updated_admin'     => $admin->name,
            'created_admin_id'  => $admin->id,
            'updated_admin_id'  => $admin->id,
        ];

        if($status === 'approved'){
            $withdraw =  collect($withdraw)
                         ->merge([
                                'date'              => $this->faker->date($format = 'Y-m-d', $max = 'now'),
                                'time'              => $this->faker->time($format = 'H:i', $max = 'now'),
                                'approved_admin'    => $admin->name,
                                'approved_admin_id' => $admin->id,
                                'approved_at'       => $timestamp
                          ])
                          ->toArray();
        }

        return $withdraw;
    }
}
