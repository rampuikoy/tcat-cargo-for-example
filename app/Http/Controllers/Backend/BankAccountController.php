<?php

namespace App\Http\Controllers\Backend;

use App\Models\BankAccount;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\BankAccountInterface;
use App\Http\Requests\BankAccount\StoreBankAccountRequest;
use App\Http\Requests\BankAccount\UpdateBankAccountRequest;

class BankAccountController extends Controller
{
    protected $bankAccountInterface;

    public function __construct(BankAccountInterface $bankAccountInterface)
    {
        $this->bankAccountInterface = $bankAccountInterface;
    }
    public function index(Request $request)
    {
        $this->authorize('index', BankAccount::class);
        $columns    = $request->input('columns', ['*']);
        $banks      = $this->bankAccountInterface->all($columns);
        return response()->json($banks, $banks->code);
    }

    public function show(Request $request, $id)
    {
        $this->authorize('show', BankAccount::class);
        $columns    = $request->input('columns', ['*']);
        $bank       = $this->bankAccountInterface->findById($id, $columns);
        return response()->json($bank, $bank->code);
    }

    public function search(Request $request)
    {
        $this->authorize('index', BankAccount::class);
        $conditions     = $request->only(['id', 'user_code', 'title', 'account', 'bank', 'branch', 'user_remark', 'admin_remark','status']);
        $columns        = $request->input('columns', ['*']);
        $limit          = $request->input('limit', $this->bankAccountInterface::DEFAULT_LIMIT);
        $sorting        = $request->input('order_by', $this->bankAccountInterface::DEFAULT_SORTING);
        $ascending      = $request->input('ascending') == 1 ? 'asc' : 'desc';

        if ($request->boolean('pagination')) {
            $page       = $request->input('page', 1);
            $banks      = $this->bankAccountInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset     = $request->input('page', 0);
            $banks      = $this->bankAccountInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($banks, $banks->code);
    }

    public function dropdown()
    {
        $dropdown = $this->bankAccountInterface->dropdownList();
        return response()->json($dropdown, $dropdown->code);
    }

    public function store(StoreBankAccountRequest $request)
    {
        $data  = $request->only([
            'title',
            'account',
            'status',
            'bank',
            'branch',
            'user_code',
            'user_remark',
            'admin_remark'
        ]);
        $bank   = $this->bankAccountInterface->store($data);
        return response()->json($bank, $bank->code);
    }
    public function update(UpdateBankAccountRequest $request, $id)
    {
        $data  = $request->only([
            'title',
            'account',
            'status',
            'bank',
            'branch',
            'user_code',
            'user_remark',
            'admin_remark'
        ]);
        $bank   = $this->bankAccountInterface->updateById($id, $data);
        return response()->json($bank, $bank->code);
    }

    public function destroy($id)
    {
        $this->authorize('destroy', BankAccount::class);
        $bank = $this->bankAccountInterface->softDeleteById($id);
        return response()->json($bank, $bank->code);
    }
}
