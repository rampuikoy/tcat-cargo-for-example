<?php

namespace App\Repositories;

use App\Interfaces\AdminInterface;
use App\Models\Admin;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use App\Traits\UploadTrait;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AdminRepository implements AdminInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, UploadTrait, CoreResponseTrait;

    const MINUTE_CACHE          = 1;
    const PREFIX_SALSE          = 'SALSE';
    const PREFIX_DRIVER         = 'DRIVER';
    const TABLE_RELETIONS       = ['rate'];
    
    public function __construct(Admin $admin)
    {
        $this->admin = $admin;
    }

    public function all(array $columns = ['*'], $sorting = AdminInterface::DEFAULT_SORTING, $ascending = AdminInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);


        $admins     = Cache::tags([
            $this->getClassPrefix(),
            $this->getAllWithClassPrefix(),
        ])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
            return $this->admin::select($columns)->orderBy($sorting, $ascending)->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.admin')]), $admins);
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = AdminInterface::DEFAULT_LIMIT, $sorting = AdminInterface::DEFAULT_SORTING, $ascending = AdminInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $admins     = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
            return $this->admin::select($columns)
                ->search($conditions)
                ->orderBy($sorting, $ascending)
                ->skip($offset)
                ->limit($limit)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.admin')]), $admins);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = AdminInterface::DEFAULT_LIMIT, $sorting = AdminInterface::DEFAULT_SORTING, $ascending = AdminInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $admins     = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
            $model = $this->admin::select($columns)
                ->search($conditions)
                ->orderBy($sorting, $ascending);

            if ($page == 1) {
                return $model->paginate($limit);
            } else {
                return $model->skip($limit * ($page - 1))->paginate($limit);
            }
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.admin')]), $admins);
    }

    public function findById($id, array $columns = ['*'])
    {
        $admin = $this->findByIdMethod($id, $columns);

        if (!$admin) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.admin'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.admin')]), $admin);
    }

    public function findManySale(array $columns = ['*'])
    {
        $key        = $this->getCacheKey(null, self::PREFIX_SALSE);
        $time       = $this->getTime(self::MINUTE_CACHE);

        $admins     = Cache::tags([
            $this->getClassPrefix(),
        ])->remember($key, $time, function () use ($columns) {
            return $this->admin::select($columns)->whereType('sale')->with(self::TABLE_RELETIONS)->get();
        });

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.admin')]), $admins);
    }

    public function findManyDriver(array $columns = ['*'])
    {
        $key        = $this->getCacheKey(null, self::PREFIX_DRIVER);
        $time       = $this->getTime(self::MINUTE_CACHE);

        $admins     = Cache::tags([
            $this->getClassPrefix(),
        ])->remember($key, $time, function () use ($columns) {
            return $this->admin::select($columns)->whereType('driver')->with(self::TABLE_RELETIONS)->get();
        });

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.admin')]), $admins);
    }

    public function dropdownList()
    {
        $key        = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $dropdown   = Cache::remember($key, $time, function () {
            return [
                'status'        => $this->admin::statusList(),
                'type'          => $this->admin::typeList(),
                'department'    => $this->admin::departmentList(),
                'branch'        => $this->admin::branchList(),
                'date_type'     => $this->admin::dateTypeList(),
                'condition'     => $this->admin::conditionList(),
            ];
        });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function store(array $data)
    {
        if (@$data['password']) {
            $data['password'] = Hash::make($data['password']);
        }
        if (@$data['tel']) {
            $data['tel'] = Str::replaceFirst('-', '', $data['tel']);
        }
        if (@$data['photo']) {
            $data['photo'] = $this->storeFile($data['photo']);
        }

        $admin  = $this->admin::create($data);

        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.admin')]), $admin);
    }

    public function updateById($id, array $data)
    {
        $admin      = $this->findByIdMethod($id);

        if (!$admin) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.admin'), 'id' => $id]));
        }

        if (@$data['photo']) {
            $data['photo'] = $this->storeFile($data['photo']);
        } else {
            $data['photo'] = $admin->photo;
        }

        $admin->update($data);
        Cache::tags([$this->getIdWithClassPrefix($admin->id)])->flush();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.admin')]), $admin);
    }

    public function updatePasswordById($id, string $password)
    {
        $admin      = $this->findByIdMethod($id);

        if (!$admin) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.admin'), 'id' => $id]));
        }

        $admin->update(['password' => Hash::make($password)]);
        Cache::tags([$this->getIdWithClassPrefix($admin->id)])->flush();

        return $this->coreResponse(200, __('messages.reset_data', ['data' => __('messages.password')]), $admin);
    }

    public function softDeleteById($id)
    {
        $admin      = $this->findByIdMethod($id);

        if (!$admin) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.admin'), 'id' => $id]));
        }

        $admin->delete();
        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.delete_data', ['data' => __('messages.admin')]), $admin);
    }

    private function storeFile($file)
    {
        if (!$file) {
            return null;
        }
        return $this->uploadBase64($file, $this->admin::$imageFolder, '', $this->admin::$imageWidth, $this->admin::$imageHeight);
    }

    private function clearCacheTags()
    {
        Cache::tags([
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix()
        ])->flush();
    }

    public function findByIdMethod($id, array $columns = ['*'])
    {
        $key        = $this->getCacheKey(null, $id);
        $time       = $this->getTime(self::MINUTE_CACHE);

        $admin    = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($id)
        ])->remember($key, $time, function () use ($id, $columns) {
            return $this->admin::select($columns)->whereId($id)->first();
        });

        return $admin;
    }
}
