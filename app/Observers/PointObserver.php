<?php

namespace App\Observers;

use App\Events\UpdateUserEvent;
use App\Models\Point;
use App\Observers\BaseObserver;

class PointObserver extends BaseObserver
{
    /**
     * Handle the point "creating" event.
     *
     * @param  \App\Models\Point  $point
     * @return void
     */
    public function creating(Point $point)
    {
        $before         = $point->user->point->total ?? 0;
        $after          = ($point->type === 'topup') ? $before + $point->amount : $before - $point->amount;
        $point->before  = sprintf('%d', $before);
        $point->after   = sprintf('%d', $after);
        $this->stampCreated($point);
    }

    /**
     * Handle the point "created" event.
     *
     * @param  \App\Models\Point  $point
     * @return void
     */
    public function created(Point $point)
    {
        $this->broadcastUserUpdated($point->user_code);
    }

    private function broadcastUserUpdated($code)
    {
                app('App\Repositories\UserRepository')->clearCacheTagByUserCode($code);
        $user = app('App\Repositories\UserRepository')->findByCode($code);
        broadcast(new UpdateUserEvent($user->results))->toOthers();
    }
}
