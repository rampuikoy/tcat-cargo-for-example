<?php

namespace App\Repositories;

use App\Helpers\Util;
use App\Imports\UserImport;
use App\Interfaces\UserExcelInterface;
use App\Models\User;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use Maatwebsite\Excel\Facades\Excel;

class UserExcelRepository implements UserExcelInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;
    
    const MINUTE_CACHE      = 0;

    public function __construct(User $user)
    {
        $this->user         = $user;
    }

    public function validateExcelFile($file)
    {
        $start_time         = microtime(true);
        $import             = new UserImport();
        try {
            Excel::import($import, $file);
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $errors         = Util::customExcelFailures($e->failures());
            return $this->unprocessResponse($errors);
        }

        $users              = $import->getUsers()->reduce(function($current, $user){
                                    $exist  =   app('App\Repositories\UserRepository')->findByEmailMethod($user['email']);
                                    if($exist) {
                                        array_push($current['exsits'], $user);
                                    }else{
                                        array_push($current['users'], $user);
                                    }
                                    return $current;
                                },
                                [
                                    'exsits'    => [],
                                    'users'     => []
                                ]);

        return $this->coreResponse(200, __('messages.import_excel'), [
            'users'             => $users['users'],
            'exsits'            => $users['exsits'],
            'log'               => [
                                    'users'         => count($users['users']),
                                    'exsits'        => count($users['exsits']),
                                    'execute_time'  => round(microtime(true) - $start_time, 2),
                                ],
        ]);
    }

    public function createMany(array $data)
    {
        $users          = collect($data)->map(function($item){
                            $user = $this->user::create([
                                'name'                      =>  $item['name'],
                                'email'                     =>  $item['email'],
                                'tel1'                      =>  $item['tel1'],
                                'tel2'                      =>  $item['tel2'],
                                'province_id'               =>  $item['province_id'],
                                'thai_shipping_method_id'   =>  $item['thai_shipping_method_id'],
                            ]);
                            return $user;
                        });
        app('App\Repositories\UserRepository')->clearCacheTags();
        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.user')]), $users);
    }
}
