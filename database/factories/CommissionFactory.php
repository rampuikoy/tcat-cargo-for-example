<?php

namespace Database\Factories;

use App\Models\Commission;
use Illuminate\Database\Eloquent\Factories\Factory;

class CommissionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Commission::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'code' =>  $this->faker->lexify('???') . '-' . $this->faker->numerify($string = '#####'),
            'username' => $this->faker->lexify('???????'),
            'start_date' => $this->faker->dateTimeBetween('-30 days', '+0 days'),
            'file' => 'commission.xlsx',
            'normal_income' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 2000, $max = 900000),
            'normal_commission' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 90000),
            'normal_profit' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 90000),
            'custom_income' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 90000),
            'custom_commission' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 90000),
            'custom_profit' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 90000),
            'total_commission' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 100, $max = 900000),
            'total' => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 90000),
        ];
    }
}
