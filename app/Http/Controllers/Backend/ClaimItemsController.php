<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\ClaimItems;
use Illuminate\Http\Request;
use App\Interfaces\ClaimItemsInterface;

class ClaimItemsController extends Controller
{
    protected $claimItemsInterface;

    public function __construct(ClaimItemsInterface $claimItemsInterface)
    {
        $this->ClaimItemsInterface = $claimItemsInterface;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ClaimItems  $claimItems
     * @return \Illuminate\Http\Response
     */
    public function show(ClaimItems $claimItems)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ClaimItems  $claimItems
     * @return \Illuminate\Http\Response
     */
    public function edit(ClaimItems $claimItems)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ClaimItems  $claimItems
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ClaimItems $claimItems)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ClaimItems  $claimItems
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClaimItems $claimItems)
    {
        //
    }
}
