<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ExchangeRate;
use App\Interfaces\ExchangeRateInterface;
use App\Http\Requests\ExchangeRate\StoreExchangeRateRequest;
use App\Http\Requests\ExchangeRate\UpdateExchangeRateRequest;

class ExchangeRateController extends Controller
{
  protected $exchangeRateInterface;

  public function __construct(ExchangeRateInterface $ExchangeRateInterface)
  {
    $this->exchangeRateInterface = $ExchangeRateInterface;
  }

  public function index(Request $request)
  {
    $this->authorize('index', ExchangeRate::class);
    $columns    = $request->input('columns', ['*']);
    $exchange   = $this->exchangeRateInterface->all($columns);
    return response()->json($exchange, $exchange->code);
  }

  
  public function show(Request $request, $id)
  {
    $this->authorize('show', ExchangeRate::class);
    $columns    = $request->input('columns', ['*']);
    $exchange   = $this->exchangeRateInterface->findById($id, $columns);
    return response()->json($exchange, $exchange->code);
  }

  public function search(Request $request)
  {
    $this->authorize('index', ExchangeRate::class);
    $conditions   = $request->only(['date']);
    $columns      = $request->input('columns', ['*']);
    $limit        = $request->input('limit', $this->exchangeRateInterface::DEFAULT_LIMIT);
    $sorting      = $request->input('order_by', $this->exchangeRateInterface::DEFAULT_SORTING);
    $ascending    = $request->input('ascending') == 1 ? 'asc' : 'desc';
    if ($request->boolean('pagination')) {
      $page       = $request->input('page', 1);
      $exchanges  = $this->exchangeRateInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
    } else {
      $offset     = $request->input('page', 0);
      $exchanges  = $this->exchangeRateInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
    }
    return response()->json($exchanges, $exchanges->code);
  }

  public function store(StoreExchangeRateRequest $request)
  {
    $data       = $request->only(['rate', 'raw_rate']);
    $exchange   = $this->exchangeRateInterface->store($data);
    return response()->json($exchange, $exchange->code);
  }

  public function update(UpdateExchangeRateRequest $request, $id)
  {
    $data       = $request->only(['rate', 'raw_rate']);
    $exchange   = $this->exchangeRateInterface->updateById($id, $data);
    return response()->json($exchange, $exchange->code);
  }

  public function destroy($id)
  {
    $this->authorize('destroy', ExchangeRate::class);
    $exchange   = $this->exchangeRateInterface->softDeleteById($id);
    return response()->json($exchange, $exchange->code);
  }
}
