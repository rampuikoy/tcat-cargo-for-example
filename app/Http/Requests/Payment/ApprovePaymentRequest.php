<?php

namespace App\Http\Requests\Payment;

use App\Http\Requests\FormRequest;

class ApprovePaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.payment.store');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bill_id'                 => 'required',
            'cash'                    => 'nullable|required|numeric',
            'credit_withdraw'         => 'boolean',
            'type'                    => 'required|in:tracking,trip',
            'type_pay'                => 'required|in:cash,credit',
            'add_on_total_price'      => 'nullable|required_if:type_pay,cash|numeric',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'bill_id'               => __('form.payment.bill_id'),
            'cash'                  => __('form.payment.cash'),
            'credit_withdraw'       => __('form.payment.credit_withdraw'),
            'type'                  => __('form.payment.type'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
