<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Withdraw;
use App\Interfaces\WithdrawInterface;
use App\Interfaces\BankCompanyInterface;
use App\Interfaces\BankAccountInterface;
use App\Http\Requests\Withdraw\StoreWithdrawRequest;
use App\Http\Requests\Withdraw\UpdateWithdrawRequest;
use App\Http\Requests\Withdraw\ApproveWithdrawRequest;
use App\Http\Requests\Withdraw\CancelWithdrawRequest;
use App\Http\Requests\Withdraw\UploadWithdrawRequest;
use App\Interfaces\UploadInterface;

class WithdrawController extends Controller
{
    protected $withdrawInterface, $bankCompanyInterface, $bankAccountInterface, $uploadInterface;

    public function __construct(WithdrawInterface $withdrawInterface, BankCompanyInterface $bankCompanyInterface, BankAccountInterface $bankAccountInterface, UploadInterface $uploadInterface)
    {
        $this->withdrawInterface        = $withdrawInterface;
        $this->bankCompanyInterface     = $bankCompanyInterface;
        $this->bankAccountInterface     = $bankAccountInterface;
        $this->uploadInterface          = $uploadInterface;
    }

    public function index(Request $request)
    {
        $this->authorize('index', Withdraw::class);
        $columns         = $request->input('columns', ['*']);
        $withdraw        = $this->withdrawInterface->all($columns);
        return response()->json($withdraw, $withdraw->code);
    }

    public function show(Request $request, $id)
    {
        $this->authorize('show', Withdraw::class);
        $columns          = $request->input('columns', ['*']);
        $withdraw         = $this->withdrawInterface->findById($id, $columns);
        return response()->json($withdraw, $withdraw->code);
    }

    public function search(Request $request)
    {
        $this->authorize('index', Withdraw::class);
        $conditions     = $request->only(['id', 'system_bank_account_id', 'user_code', 'status', 'method', 'date_start', 'date_end', 'time', 'user_remark', 'admin_remark', 'date_type', 'condition']);
        $columns        = $request->input('columns', ['*']);
        $limit          = $request->input('limit', $this->withdrawInterface::DEFAULT_LIMIT);
        $sorting        = $request->input('order_by', $this->withdrawInterface::DEFAULT_SORTING);
        $ascending      = $request->input('ascending') == 1 ? 'asc' : 'desc';

        if ($request->boolean('pagination')) {
            $page             = $request->input('page', 1);
            $withdraw  = $this->withdrawInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset           = $request->input('page', 0);
            $withdraw  = $this->withdrawInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($withdraw, $withdraw->code);
    }

    public function dropdown(Request $request)
    {
        $user_code  = $request->user_code;
        $dropdown   = $this->withdrawInterface->dropdownList();
        if (!$dropdown->error) {
            $bankCompany        = $this->bankCompanyInterface->all(['id', 'account', 'title', 'branch', 'bank']);
            $bankUser           = $this->bankAccountInterface->findManyByUserCode($user_code, ['id', 'account', 'title', 'branch', 'bank', 'user_code']);
            $collection         = collect($dropdown->results)->merge([
                'bank_company'   => (!$bankCompany->error) ? $bankCompany->results : [],
                'bank_user'      => (!$bankUser->error) ? $bankUser->results : []
            ]);
            $dropdown->results = $collection;
        }
        return response()->json($dropdown, $dropdown->code);
    }

    public function store(StoreWithdrawRequest $request)
    {
        $data              = $request->only([
            'method',
            'amount',
            'user_code',
            'user_bank_account_id',
            'admin_remark',
        ]);
        $withdraw    = $this->withdrawInterface->store($data);
        return response()->json($withdraw, $withdraw->code);
    }

    public function update(UpdateWithdrawRequest $request, $id)
    {
        $data              = $request->only([
            'charge',
            'date',
            'time',
            'user_bank_account_id',
            'system_bank_account_id',
            'admin_remark',
            'user_remark',
            'method',
        ]);
        $withdraw    = $this->withdrawInterface->updateById($id, $data);
        return response()->json($withdraw, $withdraw->code);
    }

    public function approve(ApproveWithdrawRequest $request, $id)
    {
        $this->authorize('approve', Withdraw::class);
        $data              = $request->only([
            'charge',
            'date',
            'time',
            'user_bank_account_id',
            'system_bank_account_id',
            'admin_remark',
            'user_remark',
            'method',
        ]);
        $withdraw    = $this->withdrawInterface->approveById($id, $data);
        return response()->json($withdraw, $withdraw->code);
    }

    public function cancel(CancelWithdrawRequest $request, $id)
    {
        $this->authorize('cancel', Withdraw::class);
        $data        = $request->only(['remark']);
        $withdraw    = $this->withdrawInterface->cancelById($id, $data);
        return response()->json($withdraw, $withdraw->code);
    }

    public function destroy($id)
    {
        $this->authorize('destroy', Withdraw::class);
        $withdraw     = $this->withdrawInterface->softDeleteById($id);
        return response()->json($withdraw, $withdraw->code);
    }

    public function upload(UploadWithdrawRequest $request, $id)
    {
        $data = [
            'type'              => 'image',
            'relatable_type'    => 'App\Models\Withdraw',
            'relatable_id'      => $id,
            'file_path'         => $request->file_path,
        ];
        $upload    = $this->uploadInterface->store($data);
        return response()->json($upload, $upload->code);
    }
}
