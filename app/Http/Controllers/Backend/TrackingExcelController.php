<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Tracking\TrackingExcelRequest;
use App\Http\Requests\User\ImportExcelRequest;
use App\Interfaces\TrackingExcelInterface;
use Illuminate\Http\Request;

class TrackingExcelController extends Controller
{
    protected $trackingExcelInterface;

    public function __construct(TrackingExcelInterface $trackingExcelInterface)
    {
        $this->trackingExcelInterface = $trackingExcelInterface;
    }

    public function importValidate(ImportExcelRequest $request)
    {
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 300);
        $tracks = $this->trackingExcelInterface->validateExcelFile($request->file);
        return response()->json($tracks, $tracks->code);
    }

    public function createTrack(Request $request)
    {
        $data       = [
                        'create'    => $request->input('create', []),
                        'update'    => $request->input('update', []),
                        'confirm'   => $request->input('confirm', []),
                    ];
        $tracks     = $this->trackingExcelInterface->createTrack($data);
        return response()->json($tracks, $tracks->code);
    }

    public function createTrackExcel(TrackingExcelRequest $request)
    {
        $tracks = $this->trackingExcelInterface->createOrUpdateTrack($request->tracks);
        return response()->json($tracks, $tracks->code);
    }
}
