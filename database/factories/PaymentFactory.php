<?php

namespace Database\Factories;

use App\Models\Admin;
use App\Models\Bill;
use App\Models\Payment;
use App\Models\User;
use App\Models\PaymentType;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;
use Carbon\Carbon;

class PaymentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Payment::class;

    const SUF_STATUS = ['waiting', 'success', 'fail'];

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $admin      = Admin::whereRole('SuperAdmin')->first();
        $pamentType = PaymentType::all()->random();
        $user       = User::all()->random();
        $status     = Arr::random(self::SUF_STATUS);
        $timestamp  = Carbon::now();
        $amount     = $this->faker->randomFloat($nbMaxDecimals = 2, $min = 500, $max = 2000);
        $before      = 500;


        $payment = [
            'status'                => $status,
            'amount'                => $amount,
            'before'                => $before,
            'after'                 => round($amount - $before, 2),
            'cash'                  => 1000,
            'payment_type_id'       => $pamentType->id,

            'created_admin'         => $admin->name,
            'updated_admin'         => $admin->name,
            'approved_admin'        => $admin->name,

            'user_code'             => $user->code,
            'created_admin_id'      => $admin->id,
            'updated_admin_id'      => $admin->id,
            'approved_admin_id'     => $admin->id,

            'created_at'            =>  $timestamp,
            'updated_at'            =>  $timestamp,
            'approved_at'           =>  $timestamp,
        ];
        return $payment;
    }
}
