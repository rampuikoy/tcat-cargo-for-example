<?php

namespace App\Observers;

use App\Models\Chat;
use App\Events\ChatEevent;
use App\Events\NotiEevent;
use App\Events\BroadcastEevent;
use App\Observers\BaseObserver;

class ChatObserver extends BaseObserver
{
    /**
     * Handle the chat "creating" event.
     *
     * @param  \App\Models\Chat  $chat
     * @return void
     */
    public function creating(Chat $chat)
    {
        //
        $this->stampCreated($chat);
    }

    public function created(Chat $chat)
    {
        if ($chat->created_type === 'broadcast') {
            broadcast(new BroadcastEevent($chat))->toOthers();
            return;
        }

        broadcast(new ChatEevent($chat))->toOthers();
        broadcast(new NotiEevent())->toOthers();
    }
}
