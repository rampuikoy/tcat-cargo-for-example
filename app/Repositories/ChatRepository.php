<?php

namespace App\Repositories;

use App\Interfaces\ChatInterface;
use App\Models\Chat;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use App\Traits\UploadTrait;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class ChatRepository implements ChatInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, UploadTrait, CoreResponseTrait;

    const MINUTE_CACHE          = 1;

    public function __construct(Chat $chat)
    {
        $this->chat = $chat;
    }

    public function all(array $columns = ['*'], $sorting = ChatInterface::DEFAULT_SORTING, $ascending = ChatInterface::DEFAULT_ASCENDING)
    {
        $key       = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $chats      = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
            return $this->chat::select($columns)
                            ->excepBroadcast()
                            ->orderBy($sorting, $ascending)
                            ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.chat_message')]), $chats);
    }

    public function fetchListByFields(array $conditions = [], $offset = 0, $limit = ChatInterface::DEFAULT_LIMIT, $sorting = ChatInterface::DEFAULT_SORTING, $ascending = ChatInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $chats      = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $offset, $limit, $sorting, $ascending) {
            return DB::table('chats')
                ->select('chats.*')
                ->join(DB::raw($this->subQueryUserCode($conditions)), function ($join) {
                    $join->on('chats.id', '=', 'b.maxid');
                })
                ->where(function ($query) use ($conditions) {
                    $this->chat::scopeExcepBroadcast($query, $conditions);
                    $this->chat::scopeSearch($query, $conditions);
                })
                ->orderBy($sorting, $ascending)
                ->skip($offset)
                ->limit($limit)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.chat_message')]), $chats);
    }

    public function paginateListByFields(array $conditions, $page = 1, $limit = ChatInterface::DEFAULT_LIMIT, $sorting = ChatInterface::DEFAULT_SORTING, $ascending = ChatInterface::DEFAULT_ASCENDING)
    {
        $key      = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $chats      = Cache::tags([

            $this->getClassPrefix(),
            $this->getPaginationWithClassPrefix(),

        ])->remember($key, $time, function () use ($conditions, $page, $limit, $sorting, $ascending) {
            $model = DB::table('chats')
                ->select('chats.*')
                ->join(DB::raw($this->subQueryUserCode($conditions)), function ($join) {
                    $join->on('chats.id', '=', 'b.maxid');
                })
                ->where(function ($query) use ($conditions) {
                    $this->chat::scopeExcepBroadcast($query, $conditions);
                    $this->chat::scopeSearch($query, $conditions);
                })
                ->orderBy($sorting, $ascending);

            if ($page == 1) {
                return $model->paginate($limit);
            } else {
                return $model->skip($limit * ($page - 1))->paginate($limit);
            }
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.chat_message')]), $chats);
    }

    public function findByCode($userCode, array $columns = ['*'], $types)
    {
        $chats   = $this->findByUserCodeMethod($userCode, $columns);
        $updated = $this->chat::whereUserCode($userCode)
                            ->whereReaded(0)
                            ->whereCreatedType($types['created_type'])
                            ->update(['readed' => 1]);


        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.chat_message')]), $chats);
    }

    public function dropdownList()
    {
        $key        = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $dropdown   = Cache::remember($key, $time, function () {
            return [
                'status'            => $this->chat::statusList(),
                'messages'          => $this->chat::messages(),
                'conditionStatus'   => $this->chat::conditionStatusList(),
            ];
        });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function store(array $data)
    {
        if (@$data['file']) {
            $data['file'] = $this->storeFile($data['file'], $data['user_code']);
        }

        $chat       = $this->chat::create($data);
        $findChat   = $this->findByCode($chat->user_code, ['*'], ['created_type' => 'user']);

        $this->clearCacheTags();

        if ($findChat) {
            $key            = $this->getCacheKey(null, $chat->user_code);
            $time           = $this->getTime(self::MINUTE_CACHE);
            $addMessage     = collect($findChat->results)->concat([$chat]);
            Cache::put($key, $addMessage, $time);
        }

        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.chat_message')]), $chat);
    }

    public function readAll()
    {
        $chat = $this->chat::whereReaded(0)
            ->whereCreatedType('user')
            ->update(['readed' => 1]);
        if ($chat) {
            $this->clearCacheTags();
        }

        return $this->coreResponse(200, __('messages.update_status', ['data' => __('messages.chat')]));
    }

    private function storeFile($file, $userCode = null)
    {
        if (!$file) {
            return null;
        }

        $newPath    = ($userCode)
            ? $this->chat::$imageFolder . $userCode . '/'
            : $this->chat::$imageBroadcastFolder;

        return $this->uploadBase64($file, $newPath, '', $this->chat::$imageWidth, $this->chat::$imageHeight);
    }

    private function clearCacheTags()
    {
        Cache::tags([
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix()
        ])->flush();
    }

    private function subQueryUserCode($conditions)
    {
        if (@$conditions['message']) {
            $sub_sql = '(SELECT user_code, MAX(id) AS maxid FROM chats WHERE message LIKE "%' . $conditions['message'] . '%" GROUP BY user_code ) b';
        } else {
            $sub_sql = '(SELECT user_code, MAX(id) AS maxid FROM chats GROUP BY user_code) b';
        }
        return $sub_sql;
    }

    private function findByUserCodeMethod($userCode, $columns = ['*'])
    {
        $key    = $this->getCacheKey(null, $userCode);
        $time   = $this->getTime(self::MINUTE_CACHE);

        $chat   = Cache::tags([$this->getClassPrefix(), $this->getUserCodePrefix($userCode)])->remember($key, $time, function () use ($userCode, $columns) {

                        return $this->chat::select($columns)
                                        ->where(function ($query) use ($userCode) {
                                            $query->whereUserCode($userCode)
                                                ->orWhereNull('user_code')
                                                ->where('created_type', 'broadcast');
                                        })
                                        ->get();
                    });

        return $chat;
    }
}
