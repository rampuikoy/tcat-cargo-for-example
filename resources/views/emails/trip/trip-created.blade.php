@component('mail::message')
# เรียนคุณ {{$trip->user->name}}
แจ้งบิลใหม่ <br>
รหัสลูกค้า: {{$trip->user->code}}

@component('mail::table')
| ข้อมูลบิลขนส่งใหม่   |                   |
|:-------------|--------------:|
| เลขที่บิล | {{ $trip->no }} |
| ยอดที่ต้องชำระ(บิลปัจจุบัน) | {{number_format($trip->total,2)}} บาท|
| สถานะบิล | <span style="color: #1bc5bd; text-align: left;">รอชำระเงิน</span> |
@endcomponent
<?php
$credit = ($trip->user->credit)? $trip->user->credit->total : 0 ;
$remain_credit = $credit - $trip->total_price;
?>
<p>
  <h3>รายละเอียด: </h3>
  <?php $detail = str_replace(' ', '&nbsp;',$trip->detail) ?>
  {!! nl2br($detail) !!}
</p>
@component('mail::table')
| การชำระเงิน   |                        |
|:-------------|--------------:|
| เครดิตคงเหลือ   <span style="font-weight:lighter"> {{number_format($credit, 2)}} บาท</span>|
@if($remain_credit >= 0)
| ลูกค้าสามารถชำระบิลโดยหักจากยอดเครดิตคงเหลือได้ทันที |
| <a href="#" target="_blank"> <span style="font-weight:bold">คลิกชำระบิล</span> </a> เพื่อยืนยันการชำระบิล |
@endif
@if($remain_credit <= 0)
| เครดิตคงเหลือของคุณไม่พอ <span style="font-weight:lighter"> กรุณาเติมเงินเข้าระบบ {{number_format($trip->total_price-$credit,2)}} บาท </span>|
| <a href="#" target="_blank"> <span style="font-weight:bold">คลิกแจ้งเติมเงิน</span> </a> เพื่อยืนยันการชำระบิล |
@endif
<br><br>
|**กรุณาตรวจสอบบิลขนส่งให้ครบถ้วน หากรายการยังไม่ครบ หรือไม่ถูกต้อง|
| <a href="#" target="_blank"> <span style="color: #c51b1b; text-align: left; font-weight:bold;">คลิกยกเลิกบิล</span> </a> เพื่อยกเลิกบิลและออกบิลใหม่ภายหลัง หรือติดต่อฝ่ายบริการลูกค้า|
@endcomponent
@endcomponent

