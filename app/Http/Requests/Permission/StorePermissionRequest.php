<?php

namespace App\Http\Requests\Permission;

use App\Http\Requests\FormRequest;

class StorePermissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.permission.store');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|max:64|unique:permissions,name',
            'tag'           => 'required|max:64',
            'description'   => 'required|max:64',
            'guard_name'    => 'required|in:users,admins',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name'          => __('form.permission.name'),
            'tag'           => __('form.permission.tag'),
            'description'   => __('form.permission.description'),
            'guard_name'    => __('form.permission.guard_name'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
