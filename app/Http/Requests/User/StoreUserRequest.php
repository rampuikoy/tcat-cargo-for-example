<?php

namespace App\Http\Requests\User;

use App\Http\Requests\FormRequest;
use App\Rules\Phone;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.user.store');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                      => 'required|string',
            'code'                      => 'sometimes|nullable|min:5|unique:users,code',
            'email'                     => 'required||unique:users,email',
            'password'                  => 'required|confirmed|min:4|max:20|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9]).*$/',

            'tel1'                      => ['sometimes', 'nullable', new Phone],
            'tel2'                      => ['sometimes', 'nullable', new Phone],

            'tax_id'                    => 'sometimes|nullable|min:10|max:20|regex:/^[0-9]*$/',

            'status'                    => 'required|in:active,inactive',
            'withholding'               => 'required|in:active,inactive',
            'alert_box'                 => 'required|in:active,inactive',
            'sms_notification'          => 'required|in:active,inactive',
            'create_bill'               => 'required|in:all,self,admin',

            'province_id'               => 'sometimes|nullable|exists:provinces,id',
            'thai_shipping_method_id'   => 'sometimes|nullable|exists:thai_shipping_methods,id',
            'admin_ref_id'              => 'sometimes|nullable|exists:admins,id',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name'                      => __('form.user.name'),
            'code'                      => __('form.user.code'),
            'email'                     => __('form.user.email'),
            'password'                  => __('form.user.password'),

            'tel1'                      => __('form.user.tel1'),
            'tel2'                      => __('form.user.tel2'),

            'tax_id'                    => __('form.user.tax_id'),

            'status'                    => __('form.user.status'),
            'withholding'               => __('form.user.withholding'),
            'alert_box'                 => __('form.user.alert_box'),
            'sms_notification'          => __('form.user.sms_notification'),
            'create_bill'               => __('form.user.create_bill'),

            'province_id'               => __('form.user.province_id'),
            'thai_shipping_method_id'   => __('form.user.thai_shipping_method_id'),
            'admin_ref_id'              => __('form.user.admin_ref_id'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
