<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class ThaiShippingMethod extends Model
{
    use softDeletes;
    protected $table = 'thai_shipping_methods';
    public static $imageFolder = 'uploads/droppoints/';
    public static $imageWidth   = 300;
    public static $imageHeight  = 300;

    protected $fillable = [
        'id',
        'type',
        'status',
        'access',
        'title_th',
        'title_en',
        'title_cn',
        'address',
        'detail',
        'tel',
        'province_id',
        'amphur_id',
        'district_code',
        'zipcode',
        'latitude',
        'longitude',
        'calculate_type',
        'refund',
        'shipping_rate_weight',
        'shipping_rate_cubic',
        'charge',
        'max_weight',
        'delivery',
        'min_price',
        'max_distance',
        'delivery_rate_km',
        'delivery_rate_kg',
        'delivery_rate_cubic',
        'remark',
        'api',
        'created_admin_id',
        'updated_admin_id',
        'created_admin',
        'updated_admin',
        'image'
    ];

    protected $appends = ['type_text', 'calculate_type_text', 'access_text', 'status_text', 'file_url', 'title'];

    public static function typeList()
    {
        $array = [
            'droppoint'   => __('messages.this_shipping_method.droppoint'),
            'delivery'    => __('messages.this_shipping_method.delivery'),
            'other'       => __('messages.this_shipping_method.other'),
        ];
        return $array;
    }

    public static function calculateTypeList()
    {
        $array = [
            'fixed'          => __('messages.this_shipping_method.fixed'),
            'area'           => __('messages.this_shipping_method.area'),
            'weight'         => __('messages.this_shipping_method.weight'),
            'weight_size'    => __('messages.this_shipping_method.weight_size'),
            'api'            => __('messages.this_shipping_method.api'),
        ];
        return $array;
    }

    public static function accessList()
    {
        $array = [
            'staff_only'    => __('messages.this_shipping_method.staff_only'),
            'all'           => __('messages.this_shipping_method.all'),
        ];
        return $array;
    }

    public static function statusList()
    {
        $array = [
            'active'        => __('messages.this_shipping_method.active'),
            'inactive'      => __('messages.this_shipping_method.inactive'),
        ];
        return $array;
    }

    public function getTitleAttribute()
    {
        return $this->title = $this->title_th;
    }

    public function getFileUrlAttribute($handle = null)
    {
        $file = $handle ?? $this->image;
        return ($file) ?  env('APP_MEDIA_URL') . '/' . self::$imageFolder . $file : null;
    }

    public function getStatusTextAttribute()
    {
        return collect(self::statusList())->get($this->status);
    }

    public function getTypeTextAttribute()
    {
        return collect(self::typeList())->get($this->type);
    }

    public function getCalculateTypeTextAttribute()
    {
        return collect(self::calculateTypeList())->get($this->calculate_type);
    }

    public function getAccessTextAttribute()
    {
        return collect(self::accessList())->get($this->access);
    }

    public function scopeSearch($query, $conditions)
    {
        return $query->where(function ($query) use ($conditions) {
            if (@$conditions['id']) {
                $query->where('id', $conditions['id']);
            }
            if (@$conditions['status']) {
                $query->where('status', $conditions['status']);
            }
            if (@$conditions['title']) {
                $query->where('title', 'LIKE', "%{$conditions['title']}%");
            }
            if (@$conditions['remark']) {
                $query->where('remark', 'LIKE', "%{$conditions['remark']}%");
            }
            if (@$conditions['type']) {
                $query->where('type', $conditions['type']);
            }
            if (@$conditions['calculate_type']) {
                $query->where('calculate_type', $conditions['calculate_type']);
            }
            if (@$conditions['access']) {
                $query->where('access', $conditions['access']);
            }
        });
    }


    /**
     * relation
     */
    public function province()
    {
        return $this->belongsTo('App\Models\Province', 'province_id');
    }
    public function amphur()
    {
        return $this->belongsTo('App\Models\Amphur', 'amphur_id');
    }
    public function district()
    {
        return $this->belongsTo('App\Models\District', 'district_code', 'code');
    }
}
