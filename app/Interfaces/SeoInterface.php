<?php

namespace App\Interfaces;

interface SeoInterface
{

    /**
     * find seo
     *
     * @param $id
     * @param array $columns
     *
     * @return object|null
     */
    function getDefault(array $columns = ['*']);

    /**
     * update seo 
     *
     * @param array $data
     *
     * @return object
     */
    public function update(array $data);

}
