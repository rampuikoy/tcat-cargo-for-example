<!DOCTYPE html>
<html lang="th"><head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>{{$bill->no}}</title>	
	{!! Html::style('public/css/pdf.css') !!}
	<link rel="stylesheet" href="https://res.imemo.co/tcatcargo_alpha/misc/css/pdf.css" media="all" />
	<style>
		div{
			line-height: 12px;
		}
		html{
			margin: 30px 20px;
		}
	</style>
</head><body>
	<script type="text/php">

        if (isset($pdf)) {
            $x = 570;
            $y = 15;
            $text = "{PAGE_NUM} / {PAGE_COUNT}";
            $font = $fontMetrics->get_font("helvetica");
            $size = 10;
            $color = array(0,0,0);
            $word_space = 0.0;  //  default
            $char_space = 0.0;  //  default
            $angle = 0.0;   //  default
            $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
        }
    </script>

	<div style="padding: 5px;">		
		@include('admin.bill.download.partials.bill-tracking')
	</div>
</body></html>