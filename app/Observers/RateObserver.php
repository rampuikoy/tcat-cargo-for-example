<?php

namespace App\Observers;

use App\Models\Rate;
use App\Models\User;
use App\Observers\BaseObserver;

class RateObserver extends BaseObserver
{
    /**
     * Handle the rate module "created" event.
     *
     * @param  \App\Models\Rate  $Rate
     * @return void
     */
    public function creating(Rate $Rate)
    {
        $this->stampCreated($Rate);
    }

    /**
     * Handle the rate module "updated" event.
     *
     * @param  \App\Models\Rate  $Rate
     * @return void
     */
    public function updating(Rate $rate)
    {
        if ($rate->isDirty('status') == 'active') {
            User::where('default_rate_id', $rate->id)->update(['default_rate_id' => 0]);
        }

        $this->stampUpdated($rate);
    }
}
