<?php

namespace App\Observers;

use App\Jobs\JobSendEmail;
use App\Jobs\JobSendFcm;
use App\Jobs\JobSendSms;
use App\Models\PointOrder;
use App\Observers\BaseObserver;

class PointOrderObserver extends BaseObserver
{
    /**
     * Handle the point_order "creating" event.
     *
     * @param  \App\Models\PointOrder  $point_order
     * @return void
     */
    public function creating(PointOrder $point_order)
    {
        
        $id                 = PointOrder::orderBy('id', 'DESC')->first()->id ?? 0;
        $point_order->code  = PointOrder::PREFIX.str_pad(($id + 1), 4, "0", STR_PAD_LEFT);
        $this->stampCreated($point_order);
    }

    /**
     * Handle the point_order "updating" event.
     *
     * @param  \App\Models\PointOrder  $point_order
     * @return void
     */
    public function updating(PointOrder $point_order)
    {
        $this->stampUpdated($point_order);
    }

    /**
     * Handle the point_order "updated" event.
     *
     * @param  \App\Models\PointOrder  $point_order
     * @return void
     */
    public function updated(PointOrder $point_order)
    {
        if (!$point_order->isDirty('status')) {
            return;
        }
        if ($point_order->status === 'success') {
            $this->sendSuccess($point_order);
        }
        elseif ($point_order->status === 'cancel') {
            $this->sendCancel($point_order);
        }
    }

    private function sendSuccess(PointOrder $point_order){
        $user           = app('App\Repositories\UserRepository')->findByCodeMethod($point_order->user_code);
        $remain_credit  = $user->credit->total ?? 0;
        $amount         = number_format($point_order->shipping_cost, 2);
        $total          = number_format($remain_credit - $point_order->total, 2);

        $email          = $point_order->emails()->create([
                            'mailable_class'    => 'App\Mail\PointOrder\PointOrderSuccess',
                            'status'            => 'queue',
                            'to'                => $user->email,
                            'created_admin'     => $point_order->succeed_admin,
                            'created_admin_id'  => $point_order->succeed_admin_id,
                        ]);
        JobSendEmail::dispatch($email->id)->onQueue(env('QUEUE_EMAIL_PREFIX'));

        $fcm            = $point_order->fcm()->create([
                            'status'            => 'queue',
                            'user_code'         => $user->code,
                            'title'             => __('fcm.point_order_success.title', ['no' => $point_order->code]),
                            'message'           => __('fcm.point_order_success.message', ['amount' => $amount, 'credit' => $total]),
                        ]);
        JobSendFcm::dispatch($fcm->id)->onQueue(env('QUEUE_FCM_PREFIX'));

        if ($user->sms_notification !== 'active') {
            return;
        }
        $sms            = $point_order->sms()->create([
                            'status'            => 'queue',
                            'user_code'         => $user->code,
                            'msisdn'            => $user->tel1,
                            'message'           => __('sms.point_order_success', ['no' => $point_order->code, 'tracking' => $point_order->tracking, 'cost' => $point_order->shipping_cost]),
                            'created_admin'     => $point_order->succeed_admin,
                            'created_admin_id'  => $point_order->succeed_admin_id,
                        ]);
        JobSendSms::dispatch($sms->id)->onQueue(env('QUEUE_SMS_PREFIX'));
    }

    private function sendCancel(PointOrder $point_order){
        $user           = app('App\Repositories\UserRepository')->findByCodeMethod($point_order->user_code);
        $email          = $point_order->emails()->create([
                            'mailable_class'    => 'App\Mail\PointOrder\PointOrderCancel',
                            'status'            => 'queue',
                            'to'                => $user->email,
                            'created_admin'     => $point_order->cancelled_admin,
                            'created_admin_id'  => $point_order->cancelled_admin_id,
                        ]);
        JobSendEmail::dispatch($email->id)->onQueue(env('QUEUE_EMAIL_PREFIX'));

        $fcm            = $point_order->fcm()->create([
                            'status'            => 'queue',
                            'user_code'         => $user->code,
                            'title'             => __('fcm.point_order_cancel.title', ['no' => $point_order->code]),
                            'message'           => __('fcm.point_order_cancel.message'),
                        ]);
        JobSendFcm::dispatch($fcm->id)->onQueue(env('QUEUE_FCM_PREFIX'));

        if ($user->sms_notification !== 'active') {
            return;
        }
        $sms            = $point_order->sms()->create([
                            'status'            => 'queue',
                            'user_code'         => $user->code,
                            'msisdn'            => $user->tel1,
                            'message'           => __('sms.point_order_cancel', ['no' => $point_order->code]),
                            'created_admin'     => $point_order->cancelled_admin,
                            'created_admin_id'  => $point_order->cancelled_admin_id,
                        ]);
        JobSendSms::dispatch($sms->id)->onQueue(env('QUEUE_SMS_PREFIX'));
    }
}
