<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->integer('province_id')->nullable()->unsigned();
            $table->foreign('province_id')->references('id')->on('provinces');

            $table->integer('admin_ref_id')->nullable()->unsigned();
            $table->foreign('admin_ref_id')->references('id')->on('admins');

            $table->integer('default_rate_id')->unsigned()->default(0);
            $table->integer('thai_shipping_method_id')->nullable()->unsigned();
            $table->foreign('thai_shipping_method_id')->references('id')->on('thai_shipping_methods');

            $table->index(['province_id', 'admin_ref_id', 'thai_shipping_method_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->dropColumn('remark');
            $table->dropColumn('province_id');
            $table->dropColumn('default_rate_id');
            $table->dropColumn('thai_shipping_method_id');
        });
    }
}
