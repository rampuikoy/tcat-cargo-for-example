<?php

namespace App\Http\Requests\Trip;

use App\Http\Requests\FormRequest;

class StoreTripRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.trip.store');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [


            'destination'      => 'required',
            'customer'         => 'required',
            'depart_date'      => 'date_format:Y-m-d',
            'return_date'      => 'date_format:Y-m-d',
            'discount_title'   => 'required_with:discount',
            'sub_total'        => 'required|numeric',
            'total'            => 'required|numeric',
            'user_code'        => 'exists:users,code',
            'orders'           => 'required|array|min:1',
            'orders.*.type'    =>  'required_with:orders',
            'orders.*.title'   => 'required_with:orders.*.type',
            'orders.*.amount'  => 'required_with:orders.*.title|numeric',
            'orders.*.price'   => 'required_with:orders.*.amount|numeric',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'destination'       => __('form.trip.destination'),
            'customer'          => __('form.trip.customer'),
            'depart_date'       => __('form.trip.depart_date'),
            'return_date'       => __('form.trip.return_date'),
            'discount_title'    => __('form.trip.discount_title'),
            'sub_total'         => __('form.trip.sub_total'),
            'total'             => __('form.trip.total'),
            'user_code'         => __('form.trip.user_code'),
            'orders'            => __('form.trip.order'),
            'orders.*.type'     => __('form.trip.orders.type'),
            'orders.*.title'    => __('form.trip.orders.title'),
            'orders.*.amount'   => __('form.trip.orders.amount'),
            'orders.*.price'    => __('form.trip.orders.price'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
