<?php

namespace App\Http\Requests\Article;

use App\Http\Requests\FormRequest;
use App\Rules\Base64Image;

class UpdateArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.article.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'status'         => 'required|in:active,inactive',
            'type'           => 'required|in:article,news,promotion',
            'title'          => 'required|string|max:191',
            'content'        => 'required|string',
            'views'          => 'required|nullable|numeric',
            'published_at'   => 'required|nullable|date_format:Y-m-d H:i:s',
            'file'           => ['sometimes','nullable', new Base64Image],
        ];
        return $rules;
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'status'        => __('form.article.status'),
            'type'          => __('form.article.type'),
            'title'         => __('form.article.title'),
            'content'       => __('form.article.content'),
            'views'         => __('form.article.views'),
            'published_at'  => __('form.article.published_at'),
            'file'          => __('form.article.file'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
