<?php

namespace App\Console\Commands\Make;

use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputArgument;

class MakeInterface extends GeneratorCommand
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $name = 'make:interface';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Create a new interface';

	/**
	 * The type of class being generated.
	 *
	 * @var string
	 */
	protected $type = 'Interface';

    /**
     * Replace the class name for the given stub.
     *
     * @param  string  $stub
     * @param  string  $name
     * @return string
     */
    protected function replaceClass($stub, $name)
    {
		$stub = parent::replaceClass($stub, $name);

		$removeWord = str_replace('Interface', '', $this->argument('name'));
		$stub 		= str_replace('DummyInterface', $this->argument('name'), $stub);
		$stub 		= str_replace('dummy', strtolower($removeWord), $stub);
        return $stub;
    }

	/**
	 * Get the stub file for the generator.
	 *
	 * @return string
	 */
	protected function getStub()
	{
		return  './stubs/make.interface.stub';
	}

	/**
	 * Get the default namespace for the class.
	 *
	 * @param  string  $rootNamespace
	 * @return string
	 */
	protected function getDefaultNamespace($rootNamespace)
	{
		return $rootNamespace . '\Interfaces';
	}

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the interface.'],
        ];
    }
}