<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserAddress\StoreUserAddressRequest;
use App\Http\Requests\UserAddress\UpdateUserAddressRequest;
use App\Interfaces\ProvinceInterface;
use App\Interfaces\UserAddressInterface;
use App\Models\UserAddress;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;

class UserAddressController extends Controller
{
    protected $userAddressInterface, $provinceInterface;

    public function __construct(UserAddressInterface $userAddressInterface, ProvinceInterface $provinceInterface)
    {
        $this->userAddressInterface = $userAddressInterface;
        $this->provinceInterface    = $provinceInterface;
    }

    public function index(Request $request)
    {
        $columns       = $request->input('columns', ['*']);
        $addresses     = $this->userAddressInterface->all($columns);
        return response()->json($addresses, $addresses->code);
    }


    public function show(Request $request, $id)
    {
        $this->authorize('show', UserAddress::class);
        $columns        = $request->input('columns', ['*']);
        $address        = $this->userAddressInterface->findById($id, $columns);
        return response()->json($address, $address->code);
    }


    public function search(Request $request)
    {
        $this->authorize('index', UserAddress::class);
        $conditions     = $request->only(['id', 'user_code', 'name', 'tel', 'address', 'province_id', 'zipcode', 'user_remark', 'admin_remark', 'status']);
        $columns        = $request->input('columns', ['*']);
        $limit          = $request->input('limit', $this->userAddressInterface::DEFAULT_LIMIT);
        $sorting        = $request->input('order_by', $this->userAddressInterface::DEFAULT_SORTING);
        $ascending      = $request->input('ascending') == 1 ? 'asc' : 'desc';

        if ($request->boolean('pagination')) {
            $page       = $request->input('page', 1);
            $addresses  = $this->userAddressInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset     = $request->input('page', 0);
            $addresses  = $this->userAddressInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($addresses, $addresses->code);
    }

    public function dropdown()
    {
        $dropdown               = $this->userAddressInterface->dropdownList();

        if (!$dropdown->error) {
            $province           = $this->provinceInterface->all(['id', 'title_th']);
            $dropdown->results  = collect($dropdown->results)
                ->merge(['province' => (!$province->error) ? $province->results : []]);
        }

        return response()->json($dropdown, $dropdown->code);
    }

    public function store(StoreUserAddressRequest $request)
    {
        $data       = $request->only([
            'user_code',
            'status',
            'name',
            'tel',
            'address',
            'province_id',
            'amphur_id',
            'district_code',
            'zipcode',
            'admin_remark'
        ]);
        $address   = $this->userAddressInterface->store($data);
        return response()->json($address, $address->code);
    }

    public function update(UpdateUserAddressRequest $request, $id)
    {
        $data       = $request->only([
            'user_code',
            'status',
            'name',
            'tel',
            'address',
            'province_id',
            'amphur_id',
            'district_code',
            'zipcode',
            'admin_remark'
        ]);
        $address   = $this->userAddressInterface->updateById($id, $data);
        return response()->json($address, $address->code);
    }

    public function destroy($id)
    {
        $this->authorize('destroy', UserAddress::class);
        $address    = $this->userAddressInterface->softDeleteById($id);
        return response()->json($address, $address->code);
    }

    public function getSticker($id)
    {
        $this->authorize('show', UserAddress::class);
        $address = $this->userAddressInterface->findById($id);
        $pdf = PDF::loadView('admin.address.sticker', ['address' => $address->results]);
        return $pdf->setPaper(array(0, 0, 297.64, 480.00), 'landscape')->stream();
    }
}
