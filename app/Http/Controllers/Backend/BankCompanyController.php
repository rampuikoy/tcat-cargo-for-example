<?php

namespace App\Http\Controllers\Backend;

use App\Models\BankAccount;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\BankCompany\StoreBankCompanyRequest;
use App\Http\Requests\BankCompany\UpdateBankCompanyRequest;
use App\Interfaces\BankCompanyInterface;

class BankCompanyController extends Controller
{
    protected $bankCompanyInterface;

    public function __construct(BankCompanyInterface $bankCompanyInterface)
    {
        $this->bankCompanyInterface = $bankCompanyInterface;
    }
    public function index(Request $request)
    { 
        $this->authorize('index', BankAccount::class);
        $columns        = $request->input('columns', ['*']);
        $banks          = $this->bankCompanyInterface->all($columns);
        return response()->json($banks, $banks->code);
        
    }

    public function show(Request $request, $id)
    {
        $this->authorize('show', BankAccount::class);
        $columns        = $request->input('columns', ['*']);
        $bank           = $this->bankCompanyInterface->findById($id, $columns);
        return response()->json($bank, $bank->code);
    }

    public function search(Request $request)
    {
        $this->authorize('index', BankAccount::class);
        $conditions     = $request->only(['search']);
        $columns        = $request->input('columns', ['*']);
        $limit          = $request->input('limit', $this->bankCompanyInterface::DEFAULT_LIMIT);
        $sorting        = $request->input('order_by', $this->bankCompanyInterface::DEFAULT_SORTING);
        $ascending      = $request->input('ascending') == 1 ? 'asc' : 'desc';

        if ($request->boolean('pagination')) {
            $page               = $request->input('page', 1);
            $banks   = $this->bankCompanyInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset             = $request->input('page', 0);
            $banks = $this->bankCompanyInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($banks, $banks->code);
    }

    public function dropdown()
    {
        $dropdown = $this->bankCompanyInterface->dropdownList();
        return response()->json($dropdown, $dropdown->code);
    }

    public function store(StoreBankCompanyRequest $request)
    {
        $data  = $request->only([
            'title',
            'account',
            'status',
            'bank',
            'branch',
            'user_remark',
            'admin_remark'
        ]);
        $bank   = $this->bankCompanyInterface->store($data);
        return response()->json($bank, $bank->code);
    }

    public function update(UpdateBankCompanyRequest $request, $id)
    {
        $data  = $request->only([
            'title',
            'account',
            'status',
            'bank',
            'branch',
            'user_remark',
            'admin_remark'
        ]);
        $bank   = $this->bankCompanyInterface->updateById($id, $data);
        return response()->json($bank, $bank->code);
    }

    public function destroy($id)
    {
        $this->authorize('destroy', BankAccount::class);
        $bank = $this->bankCompanyInterface->softDeleteById($id);
        return response()->json($bank, $bank->code);
    }
}
