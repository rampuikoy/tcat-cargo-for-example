<?php

namespace App\Http\Controllers\Backend\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\Models\Admin;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Create a new admin instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\Admin
     */
    public function register(RegisterRequest $request)
    {
        $admin = Admin::create([
            'name' => $request->name,
            'lastname' => $request->lastname,
            'username' => $request->username,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        event(new Registered($admin));

        $results = collect($admin)->merge(['must_verify_email' => true])->toArray();
        return response()->json(['message' => __('auth.registered'), 'error' => false, 'results' => $results, 'code' => 200], 200);
    }
}
