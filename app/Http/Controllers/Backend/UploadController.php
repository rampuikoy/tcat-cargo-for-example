<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Upload\UploadSingleRequest;
use App\Interfaces\UploadInterface;

class UploadController extends Controller
{
    protected $uploadInterface;

    public function __construct(UploadInterface $uploadInterface)
    {
        $this->uploadInterface = $uploadInterface;
    }

    public function store(UploadSingleRequest $request)
    {
        $data   = $request->only(['type', 'file_path']);
        $upload = $this->uploadInterface->store($data);
        return response()->json($upload, $upload->code);
    }

    public function destroy($id)
    {
        $upload = $this->uploadInterface->delete($id);
        return response()->json($upload, $upload->code);
    }
}
