<?php

namespace App\Observers;

use App\Jobs\JobSendFcm;
use App\Jobs\JobSendSms;
use App\Models\Withdraw;
use App\Events\NotiEevent;
use App\Jobs\JobSendEmail;
use App\Observers\BaseObserver;

class WithdrawObserver extends BaseObserver
{
    //
    public function creating(Withdraw $withdraw)
    {
        $this->stampCreated($withdraw);
    }

    public function updating(Withdraw $withdraw)
    {
        $this->stampUpdated($withdraw);
    }

    public function created()
    {
        broadcast(new NotiEevent())->toOthers();
    }

    public function updated(Withdraw $withdraw)
    {

        if ($withdraw->isDirty('status') && $withdraw->status === 'approved') {
            $user       = $withdraw->user;
            $amount     = number_format($withdraw->amount, 2);
            $total      = number_format($user->credit->total ?? 0, 2);

            $email      = $withdraw->emails()->create([
                'mailable_class'    => 'App\Mail\Withdraw\WithdrawApproved',
                'status'            => 'queue',
                'to'                => $user->email,
                'created_admin'     => $withdraw->created_admin,
                'created_admin_id'  => $withdraw->created_admin_id,
            ]);
            JobSendEmail::dispatch($email->id)->onQueue(env('QUEUE_EMAIL_PREFIX'));


            $fcm        = $withdraw->fcm()->create([
                'status'            => 'queue',
                'user_code'         => $user->code,
                'title'             => __('fcm.withdraw_approved.title', ['no' => $withdraw->no]),
                'message'           => __('fcm.withdraw_approved.message', ['amount' => $amount, 'credit' => $total]),
            ]);
            JobSendFcm::dispatch($fcm->id)->onQueue(env('QUEUE_FCM_PREFIX'));

            if ($user->sms_notification === 'active') {
                return;
            }
            $sms        = $withdraw->sms()->create([
                'status'            => 'queue',
                'user_code'         => $user->code,
                'msisdn'            => $user->tel1,
                'message'           => __('sms.withdraw_approved', ['no' => $withdraw->no, 'total' => $total, 'amount' => $amount]),
                'created_admin'     => $withdraw->created_admin,
                'created_admin_id'  => $withdraw->created_admin_id,
            ]);
            JobSendSms::dispatch($sms->id)->onQueue(env('QUEUE_SMS_PREFIX'));
        }
    }
}
