@component('mail::message')
# เรียนคุณ {{$trip->user->name}}
แจ้งอัพเดทสถานะบิล <br>
รหัสลูกค้า: {{$trip->user->code}}

@component('mail::table')
| แจ้งอัพเดทสถานะบิล   |                   |
|:-------------|--------------:|
| เลขที่บิล | {{ $trip->no }} |
| รหัสลูกค้า| {{$trip->user->code}}|
| สถานะบิล | <span style="color: red; text-align: left;">ชำระเงินแล้ว</span> |
@endcomponent
	<p>
		<h3>รายละเอียด: </h3>
		{!! nl2br($trip->detail) !!}
	</p>
@endcomponent

