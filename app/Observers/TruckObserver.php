<?php

namespace App\Observers;

use App\Models\Truck;
use App\Observers\BaseObserver;

class TruckObserver extends BaseObserver
{
    /**
     * Handle the truck "creating" event.
     *
     * @param  \App\Models\Truck  $truck
     * @return void
     */
    public function creating(Truck $truck)
    {
        //
        $this->stampCreated($truck);
    }

    /**
     * Handle the truck "updating" event.
     *
   * @param  \App\Models\Truck  $truck
     * @return void
     */
    public function updating(Truck $truck)
    {
        //
        $this->stampUpdated($truck);
    }



}
