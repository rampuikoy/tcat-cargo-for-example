<?php

namespace App\Imports;

use App\Helpers\Calculator;
use App\Helpers\Util;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;
use App\models\ProductType;
use App\models\ShippingMethod;
use App\Models\Tracking;
use App\Models\Warehouse;

HeadingRowFormatter::extend('mapping-key', function ($value) {
    switch ($value) {
        case 'เลข Tracking':
            return 'code';
        case 'Track Group':
            return 'code_group';
        case 'รหัสลูกค้า':
            return 'user_code';
        case 'รหัสอ้างอิงหน้า Tracking':
            return 'reference_code';
        case 'โซนเก็บของ':
            return 'warehouse_zone';
        case 'น้ำหนัก(kg)':
            return 'weight';
        case 'กว้าง(cm)':
            return 'width';
        case 'ยาว(cm)':
            return 'length';
        case 'สูง(cm)':
            return 'height';
        case 'ชนิดสินค้า':
            return 'product_type';
        case 'การขนส่ง':
            return 'shipping_method';
        case 'หมายเหตุ(เจ้าหน้าที่)':
            return 'admin_remark';
        case 'ตำแหน่งเก็บสินค้า':
            return 'warehouse';
        case 'วันที่ของเข้าโกดังจีน':
            return 'china_in';
        case 'วันที่ของออกโกดังจีน':
            return 'china_out';
        case 'วันที่ของเข้าโกดังไทย':
            return 'thai_in';
        case 'วันที่ของออกโกดังไทย':
            return 'thai_out';
        case 'ค่าตีลังไม้(หยวน)':
            return 'china_cost_box';
        case 'ค่าขนส่งทีจีน(หยวน)':
            return 'china_cost_shipping';
        default:
            return;
    }
});

HeadingRowFormatter::default('mapping-key');

class TrackingImport implements ToCollection, WithValidation, WithBatchInserts, WithChunkReading, WithHeadingRow
{
    use Importable;
    
    protected $trackings;

    public function collection(Collection $collection)
    {
        $productType        = ProductType::$types;
        $shippingMethod     = ShippingMethod::$methods;
        $warehouseType      = Warehouse::$types;
        $trackingStatus     = Tracking::statusList();
        
        $this->trackings    = $collection->map(function($track) use ($productType, $shippingMethod, $warehouseType, $trackingStatus){

                                $track['code']                  = Util::trimUpperCase($track['code']);
                                $track['code_group']            = Util::trimUpperCase($track['code_group']);
                                $track['user_code']             = Util::trimUpperCase($track['user_code']);
                                $track['reference_code']        = Util::trimUpperCase($track['reference_code']);

                                $track['status']                = 1;

                                if(!empty($track['warehouse_zone'])){
                                    $track['status']            = 3;
                                    $track['warehouse_id']      = 2;
                                    $track['warehouse_zone']    = strtoupper($track['warehouse_zone']);
                                }
                                
                                if(!empty($track['thai_in']) && !empty($track['warehouse_id']) && $track['warehouse_id'] === 1){
                                    $track['warehouse_id']      = 2;
                                    $track['status']            = ($track['status'] > 1) ? $track['status'] : 2;
                                }



                                $track['weight']                = round($track['weight'], 3);
                                $track['width']                 = round($track['width'], 2);
                                $track['length']                = round($track['length'], 2);
                                $track['height']                = round($track['height'], 2);

                                $track['dimension']             = Calculator::dimensionWeight($track['width'], $track['length'], $track['height']);
                                $track['cubic']                 = Calculator::cubic($track['width'], $track['length'], $track['height']);
                                $track['calculate_type']        = Calculator::calculateType($track['dimension'], $track['weight']);
   
                                $status                         = collect($trackingStatus)->firstWhere('id', $track['status']);
                                $track['status_text']           = $status['name'];

                                $track['product_type_id']       = Util::getOrSetDefaultEnum($productType, $track['product_type']);
                                $track['shipping_method_id']    = Util::getOrSetDefaultEnum($shippingMethod, $track['shipping_method']);
                                $track['warehouse_id']          = Util::getOrSetDefaultEnum($warehouseType, $track['warehouse']);
                                

                                $track['china_in']              = Util::excelToDate($track['china_in']);
                                $track['china_out']             = Util::excelToDate($track['china_out']);
                                $track['thai_in']               = Util::excelToDate($track['thai_in']);
                                $track['thai_out']              = Util::excelToDate($track['thai_out']);

                                
                                $track['china_cost_box']        = Util::getOrSetNumeric($track['china_cost_box'], 0);
                                $track['china_cost_shipping']   = Util::getOrSetNumeric($track['china_cost_shipping'], 0);
                                
                                return $track;
                            });
    }

    public function rules(): array
    {
        return [
            'code'          => 'required',
            // 'user_code'     => 'required',
        ];
    }

    public function batchSize(): int
    {
        return 100;
    }

    public function chunkSize(): int
    {
        return 100;
    }

    public function headingRow(): int
    {
        return 1;
    }

    public function getTrackCode(){
        return collect($this->trackings)->unique('code')->pluck('code')->toArray();
    }

    public function getTrackCreate($codes)
    {
        return collect($this->trackings)
                ->unique('code')
                ->filter(function($track) use ($codes){
                    return collect($codes)->contains($track['code']) === false;
                })->map(function($track){
                    $track['select'] = false;
                    return $track;
                })
                ->values();
    }

    public function getTrackExsist($codes)
    {
        return collect($this->trackings)
                ->unique('code')
                ->filter(function($track) use ($codes){
                    return collect($codes)->contains($track['code']);
                })
                ->values();
    }

    public function getTrackDuplicate()
    {
        $collection     = collect($this->trackings);
        $unique         = $collection->unique('code');

        if ($unique->count() == 0) {
            return [];
        }

        return $collection->filter(function ($track) use ($unique) {
            $exist = $unique->firstWhere('code', $track['code'])->toArray();
            $track = $track->toArray();
            return  $exist != $track;
        })->values();
    }
}
