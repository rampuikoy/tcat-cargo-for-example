<?php

namespace App\Observers;

use App\Models\Seo;

class SeoObserver extends BaseObserver
{
   /**
     * Handle the settingseo "creating" event.
     *
     * @param  \App\Models\Seo  $settingseo
     * @return void
     */
    public function creating(Seo $settingseo)
    {
        //
        $this->stampCreated($settingseo);
    }

    /**
     * Handle the warehouse "updating" event.
     *
   * @param  \App\Models\Seo  $warehouse
     * @return void
     */
    public function updating(Seo $settingseo)
    {
        //
        $this->stampUpdated($settingseo);
    }

}
