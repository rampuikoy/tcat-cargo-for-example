<?php

namespace App\Repositories;

use App\Interfaces\ThaiShippingWeightPriceInterface;
use App\Models\ThaiShippingWeightPrice;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use Illuminate\Support\Facades\Cache;

class ThaiShippingWeightPriceRepository implements ThaiShippingWeightPriceInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const MINUTE_CACHE = 1;
    const TABLE_RELETIONS = [];

    public function __construct(ThaiShippingWeightPrice $thaiShippingWeightPrice)
    {
        $this->thaiShippingWeightPrice = $thaiShippingWeightPrice;
    }

    public function all(array $columns = ['*'], $sorting = ThaiShippingWeightPriceInterface::DEFAULT_SORTING, $ascending = ThaiShippingWeightPriceInterface::DEFAULT_ASCENDING)
    {
        $key = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time = $this->getTime(self::MINUTE_CACHE);

        $thaishippingweightprices = Cache::tags([
            $this->getClassPrefix(),
            $this->getAllWithClassPrefix(),
        ])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
            return $this->thaiShippingWeightPrice::select($columns)->orderBy($sorting, $ascending)->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.thai_shipping_weight_price')]), $thaishippingweightprices);
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = ThaiShippingWeightPriceInterface::DEFAULT_LIMIT, $sorting = ThaiShippingWeightPriceInterface::DEFAULT_SORTING, $ascending = ThaiShippingWeightPriceInterface::DEFAULT_ASCENDING)
    {
        $key = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time = $this->getTime(self::MINUTE_CACHE);

        $thaishippingweightprices = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
            return $this->thaiShippingWeightPrice::select($columns)
                                                ->where(function ($query) use ($conditions) {
                                                    if (@$conditions['thai_shipping_method_id']) {
                                                        $query->whereThaiShippingMethodId($conditions['thai_shipping_method_id']);
                                                    }
                                                })
                                                ->orderBy($sorting, $ascending)
                                                ->skip($offset)
                                                ->limit($limit)
                                                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.thai_shipping_weight_price')]), $thaishippingweightprices);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = ThaiShippingWeightPriceInterface::DEFAULT_LIMIT, $sorting = ThaiShippingWeightPriceInterface::DEFAULT_SORTING, $ascending = ThaiShippingWeightPriceInterface::DEFAULT_ASCENDING)
    {
        $key = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time = $this->getTime(self::MINUTE_CACHE);

        $thaishippingweightprices = Cache::tags([$this->getClassPrefix(), $this->getPaginationWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
            $model = $this->thaiShippingWeightPrice::select($columns)
                                                    ->where(function ($query) use ($conditions) {
                                                        if (@$conditions['thai_shipping_method_id']) {
                                                            $query->whereThaiShippingMethodId($conditions['thai_shipping_method_id']);
                                                        }
                                                    })
                                                    ->orderBy($sorting, $ascending);

            if ($page == 1) {
                return $model->paginate($limit);
            } else {
                return $model->skip($limit * ($page - 1))->paginate($limit);
            }
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.thai_shipping_weight_price')]), $thaishippingweightprices);
    }

    public function findById($id, array $columns = ['*'])
    {
        $thaiShippingWeightPrice = $this->findByIdMethod($id, $columns);

        if (!$thaiShippingWeightPrice) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.thai_shipping_weight_price'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.thai_shipping_weight_price')]), $thaiShippingWeightPrice);
    }

    public function findManyByShippingId($id, array $columns = ['*'])
    {
        $thaiShippingWeightPrice = $this->findManyByShippingIdMethod($id, $columns);

        if ($thaiShippingWeightPrice->count() === 0) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.thai_shipping_weight_price'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.thai_shipping_weight_price')]), $thaiShippingWeightPrice);
    }

    public function store(array $data)
    {
        $thaiShippingWeightPrice = $this->thaiShippingWeightPrice::create([
            'title'                     => $data['title'],
            'min_weight'                => $data['min_weight'],
            'max_weight'                => $data['max_weight'],
            'price'                     => $data['price'],
            'thai_shipping_method_id'   => $data['thai_shipping_method_id']
        ]);

        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.thai_shipping_weight_price')]), $thaiShippingWeightPrice);
    }

    public function updateById($id, array $data)
    {
        $thaiShippingWeightPrice = $this->findByIdMethod($id);

        if (!$thaiShippingWeightPrice) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.thai_shipping_weight_price'), 'id' => $id]));
        }

        $thaiShippingWeightPrice->update([
            'title'                     => $data['title'],
            'min_weight'                => $data['min_weight'],
            'max_weight'                => $data['max_weight'],
            'price'                     => $data['price']
        ]);
        $this->clearCacheTags();
        $this->clearCacheTagById($id);
        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.thai_shipping_weight_price')]), $thaiShippingWeightPrice);
    }

    public function softDeleteById($id)
    {
        $thaiShippingWeightPrice = $this->findByIdMethod($id);

        if (!$thaiShippingWeightPrice) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.thai_shipping_weight_price'), 'id' => $id]));
        }

        $thaiShippingWeightPrice->delete();
        $this->clearCacheTags();
        $this->clearCacheTagById($id);
        return $this->coreResponse(200, __('messages.delete_data', ['data' => __('messages.thai_shipping_weight_price')]), $thaiShippingWeightPrice);
    }

    private function findByIdMethod($id, array $columns = ['*'])
    {
        $key = $this->getCacheKey(null, $id);
        $time = $this->getTime(self::MINUTE_CACHE);

        $thaiShippingWeightPrice = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($id),
        ])->remember($key, $time, function () use ($id, $columns) {
            return $this->thaiShippingWeightPrice::select($columns)->whereId($id)->first();
        });

        return $thaiShippingWeightPrice;
    }

    private function findManyByShippingIdMethod($id, array $columns = ['*'])
    {
        $key = $this->getCacheKey('SHIPPING_METHOD', $id);
        $time = $this->getTime(self::MINUTE_CACHE);

        $thaiShippingWeightPrice = Cache::tags([
            $this->getClassPrefix(),
            $this->getIdWithClassPrefix($id),
            $this->getFindByShippingMethodIdWithClassPrefix($id),
        ])->remember($key, $time, function () use ($id, $columns) {
            return $this->thaiShippingWeightPrice::select($columns)->whereThaiShippingMethodId($id)->get();
        });

        return $thaiShippingWeightPrice;
    }

    private function clearCacheTags()
    {
        $keys = [
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix(),
        ];
        Cache::tags($keys)->flush();
    }

    private function clearCacheTagById($id)
    {
        Cache::tags($this->getIdWithClassPrefix($id))->flush();
    }
}
