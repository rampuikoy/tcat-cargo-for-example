<?php

namespace App\Http\Controllers\Backend\NoRole;

use App\Http\Controllers\Controller;
use App\Interfaces\TrackingInterface;
use Illuminate\Http\Request;

class TrackingController extends Controller
{
    protected $trackingInterface;

    public function __construct(TrackingInterface $trackingInterface)
    {
        $this->trackingInterface = $trackingInterface;
    }

    public function searchMany(Request $request, $code)
    {
        $columns = $request->input('columns', ['*']);
        $tracking = $this->trackingInterface->searchManyByCode($code, $columns);
        return response()->json($tracking, $tracking->code);
    }
}
