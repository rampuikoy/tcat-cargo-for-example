<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBankAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('title');
            $table->string('account');
            $table->string('branch',100)->nullable();
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->string('bank', 10);
            $table->string('user_code', 20)->nullable();
            $table->text('user_remark')->nullable();
            $table->text('admin_remark')->nullable();
            $table->string('created_admin', 50)->nullable();
            $table->string('updated_admin', 50)->nullable();
            $table->integer('created_admin_id')->unsigned()->nullable();
            $table->integer('updated_admin_id')->unsigned()->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_accounts');
    }
}
