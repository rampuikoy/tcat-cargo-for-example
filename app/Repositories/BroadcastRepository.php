<?php

namespace App\Repositories;

use App\Interfaces\BroadcastInterface;
use App\Models\Chat;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use App\Traits\UploadTrait;
use Illuminate\Support\Facades\Cache;


class BroadcastRepository implements BroadcastInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait, UploadTrait;

    const MINUTE_CACHE          = 1;

    public function __construct(Chat $broadcast)
    {
        $this->broadcast        = $broadcast;
    }

    public function all(array $columns = ['*'], $sorting = BroadcastInterface::DEFAULT_SORTING, $ascending = BroadcastInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $broadcasts     = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
                            return $this->broadcast::select($columns)
                                                    ->broadcastOnly()
                                                    ->orderBy($sorting, $ascending)
                                                    ->get();
                        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.broadcast')]), $broadcasts);
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = broadcastInterface::DEFAULT_LIMIT, $sorting = broadcastInterface::DEFAULT_SORTING, $ascending = broadcastInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $broadcasts     = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
                            return $this->broadcast::select($columns)
                                ->broadcastOnly()
                                ->broadcastSearch($conditions)
                                ->orderBy($sorting, $ascending)
                                ->skip($offset)
                                ->limit($limit)
                                ->get();
                        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.broadcast')]), $broadcasts);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = broadcastInterface::DEFAULT_LIMIT, $sorting = broadcastInterface::DEFAULT_SORTING, $ascending = broadcastInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $broadcasts = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
                        $model = $this->broadcast::select($columns)
                                                ->broadcastOnly()
                                                ->broadcastSearch($conditions)
                                                ->orderBy($sorting, $ascending);

                        if ($page == 1) {
                            return $model->paginate($limit);
                        } else {
                            return $model->skip($limit * ($page - 1))->paginate($limit);
                        }
                    });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.broadcast')]), $broadcasts);
    }

    public function findById($id, array $columns = ['*'])
    {
        $broadcast = $this->findByIdMethod($id, $columns);

        if (!$broadcast) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.broadcast'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.broadcast')]), $broadcast);
    }

    public function dropdownList()
    {
        $key        = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $dropdown   = Cache::remember($key, $time, function () {
                            return [
                                'status' => $this->broadcast::statusList(),
                            ];
                        });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function store(array $data)
    {
        if (@$data['file']) {
            $data['file'] = $this->storeFile($data['file']);
        }

        $broadcast  = $this->broadcast::create([
                                            'created_type'  => 'broadcast',
                                            'message'       => $data['message'],
                                            'status'        => $data['status'],
                                            'file'          => $data['file']
                                        ]);
        $this->clearCacheTags();
        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.broadcast')]), $broadcast);
    }

    public function updateById($id, array $data)
    {
        $broadcast      = $this->findByIdMethod($id);

        if (!$broadcast) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.broadcast'), 'id' => $id]));
        }

        if (@$data['file']) {
            $data['file'] = $this->storeFile($data['file']);
        }
        
        $broadcast->update([
                            'message'       => $data['message'],
                            'status'        => $data['status'],
                            'file'          => $data['file'] ?? $broadcast->file
                        ]);

        $this->clearCacheTags();
        $this->clearCacheTagById($broadcast->id);
        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.broadcast')]), $broadcast);
    }

    public function findByIdMethod($id, array $columns = ['*'])
    {
        $key        = $this->getCacheKey(null, $id);
        $time       = $this->getTime(self::MINUTE_CACHE);

        $broadcast  = Cache::tags([
                            $this->getClassPrefix(),
                            $this->getFindByIdWithClassPrefix(),
                            $this->getIdWithClassPrefix($id)
                        ])
                        ->remember($key, $time, function () use ($id, $columns) {
                            return $this->broadcast::select($columns)
                                                    ->broadcastOnly()
                                                    ->whereId($id)
                                                    ->first();
                        });

        return $broadcast;
    }

    private function storeFile($file)
    {
        if (!$file) {
            return null;
        }
        return $this->uploadBase64($file, $this->broadcast::$imageBroadcastFolder, '', $this->broadcast::$imageWidth, $this->broadcast::$imageHeight);
    }

    private function clearCacheTags()
    {
        Cache::tags([
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix()
        ])
        ->flush();
    }

    private function clearCacheTagById($id)
    {
        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();
    }
}
