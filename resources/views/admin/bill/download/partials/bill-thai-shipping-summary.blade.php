<table class="bordered" aria-describedby="bill-thai-shipping-summary">
	<thead style="background: #dfdfdf;">
		<tr>
			<th scope="col" colspan="4">
				<div>ค่าขนส่งในประเทศ</div>
			</th>
		</tr>
		<tr>
			<th scope="col">
				<div>วันที่จัดส่ง</div>
			</th>
			<th scope="col" class="text-right" width="90">
				<div>ยอดที่หักมา(@lang('admin.thb'))</div>
			</th>
			<th scope="col" class="text-right" width="90">
				<div>ค่าใช้จ่ายจริง(@lang('admin.thb'))</div>
			</th>
			<th scope="col" class="text-right" width="90">
				<div>ส่วนต่าง(@lang('admin.thb'))</div>
			</th>
		</tr>
	</thead>
	<tbody>

		<tr>
			<td class="text-center">
				<div>{!!str_replace(' ', '&nbsp;',$bill->thai_shipping_date)!!}</div>
			</td>

			@php $system_price = $bill->thai_shipping_method_price + $bill->thai_shipping_method_charge; @endphp
			<td class="text-right">
				<div>
					{{number_format($system_price , 2)}}
				</div>
			</td>

			@php $raw_price = $bill->thai_shipping_raw_price + $bill->thai_shipping_raw_charge; @endphp
			<td class="text-right">
				<div>{{number_format($raw_price, 2)}}</div>
			</td>

			@php $dif = $system_price - $raw_price ; @endphp
			<td class="text-right">
				<div>
					{{ ($dif>0) ? '+' : '' }}
					{{number_format($dif, 2)}}
				</div>
			</td>
		</tr>

	</tbody>

</table>