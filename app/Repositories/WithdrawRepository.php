<?php

namespace App\Repositories;

use App\Models\User;
use App\Models\Withdraw;
use App\Traits\CacheTrait;
use App\Traits\UploadTrait;
use App\Traits\CoreResponseTrait;
use App\Interfaces\WithdrawInterface;
use Illuminate\Support\Facades\Cache;
use App\Helpers\Util;
use App\Models\Credit;
use Illuminate\Support\Facades\DB;

class WithdrawRepository implements WithdrawInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, UploadTrait, CoreResponseTrait;

    const MINUTE_CACHE              = 1;
    const RELETION_ALL              = ['userBankAccount', 'systemBankAccount', 'images'];
    const RELESTION_FIND            = ['userBankAccount', 'systemBankAccount', 'images', 'credits'];

    public function __construct(Withdraw $withdraw, User $user, Credit $credit)
    {
        $this->withdraw        = $withdraw;
        $this->user            = $user;
        $this->credit          = $credit;
    }

    public function all(array $columns = ['*'], $sorting = WithdrawInterface::DEFAULT_SORTING, $ascending = WithdrawInterface::DEFAULT_ASCENDING)
    {
        $key = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time = $this->getTime(self::MINUTE_CACHE);

        $withdraws  = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
            return $this->withdraw::select($columns)->with(self::RELETION_ALL)->orderBy($sorting, $ascending)->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.withdraw')]), $withdraws);
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = WithdrawInterface::DEFAULT_LIMIT, $sorting = WithdrawInterface::DEFAULT_SORTING, $ascending = WithdrawInterface::DEFAULT_ASCENDING)
    {
        $key = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time = $this->getTime(self::MINUTE_CACHE);

        $withdraws  = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
            return $this->withdraw::select($columns)
                ->with(self::RELETION_ALL)
                ->search($conditions)
                ->orderBy($sorting, $ascending)
                ->skip($offset)
                ->limit($limit)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.withdraw')]), $withdraws);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = WithdrawInterface::DEFAULT_LIMIT, $sorting = WithdrawInterface::DEFAULT_SORTING, $ascending = WithdrawInterface::DEFAULT_ASCENDING)
    {
        $key = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time = $this->getTime(self::MINUTE_CACHE);

        $withdraws  = Cache::tags([
            $this->getClassPrefix(),
            $this->getPaginationWithClassPrefix(),
        ])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
            $model = $this->withdraw::select($columns)
                ->with(self::RELETION_ALL)
                ->search($conditions)
                ->orderBy($sorting, $ascending);

            if ($page == 1) {
                return $model->paginate($limit);
            } else {
                return $model->skip($limit * ($page - 1))->paginate($limit);
            }
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.withdraw')]), $withdraws);
    }

    public function findById($id, array $columns = ['*'])
    {
        $withdraw  =  $this->findByIdMethod($id, $columns = ['*']);
        if (!$withdraw) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.withdraw'), 'id' => $id]));
        }
        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.withdraw')]), $withdraw);
    }

    public function findByUserCode($userCode, array $columns = ['*'])
    {
        $withdraw  = $this->findByUserCodeMethod($userCode, $columns);
        if ($withdraw->isEmpty()) {
            return $this->coreResponse(404, __('messages.no_data_with_user_code', ['data' => __('messages.withdraw.withdraw'), 'user_code' => $userCode]));
        }
        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.withdraw.withdraw')]), $withdraw);
    }

    public function dropdownList()
    {
        $key = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time = $this->getTime(self::MINUTE_CACHE);
        $dropdown   = Cache::remember($key, $time, function () {
            return [
                'condition'    => $this->withdraw::conditionList(),
                'date_type'     => $this->withdraw::dateTypeList(),
                'method'       => $this->withdraw::methodList(),
                'status'       => $this->withdraw::statusList()
            ];
        });
        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function store(array $data)
    {
        $remain_credit  = app('App\Repositories\UserRepository')->findByCodeMethod($data['user_code'])->credit ?? null;
        if (!$remain_credit) {
            return $this->coreResponse(500, __('messages.no_credit_with_user', ['code' => $data['user_code']]));
        }
        // check user enough credit
        if ($data['method'] != 'credit') {
            if (round($remain_credit->total, 2) < round($data['amount'], 2)) {
                $message    = __('messages.no_data_with_total', [
                    'data'      => __('messages.withdraw_credit'),
                    'amont'     => round($data['amount'], 2),
                    'credit'    => round($remain_credit->total, 2),
                ]);
                return $this->coreResponse(500, $message);
            }
        }

        // charge
        if ($data['method'] == 'credit') {
            $data['charge']     = 0;
        } else {
            $data['charge']     = $this->withdraw::DEFAULT_CHARGE;
        }
        // sum recevie
        $data['receive']        = $data['amount'] - $data['charge'];

        // stranr transction
        DB::beginTransaction();
        try {
            // withdraw create
            $withdraw           = $this->withdraw::create($data);
            $userCredit         = $withdraw->user->credit;

            // withdraw reletion create credit
            $withdraw->credit()->create([
                'type'                => 'withdraw',
                'user_code'           => $withdraw->user_code,
                'amount'              => $withdraw->amount,
                'before'              => $userCredit->total,
                'after'               => $userCredit->total - $withdraw->amount,
                'updated_admin'       => $withdraw->updated_admin,
                'updated_admin_id'    => $withdraw->updated_admin_id,
                'approved_admin'      => $withdraw->approved_admin,
                'approved_admin_id'   => $withdraw->approved_admin_id,
            ]);

            // remove cache
            $this->clearCacheTags();
            app('App\Repositories\UserRepository')->clearCacheTagByUserCode($withdraw->user_code);

            DB::commit();
            return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.withdraw')]), $withdraw);
        } catch (\Throwable $th) {
            // rollback insert sql
            DB::rollBack();
            return $this->coreResponse(500, __('messages.not_create_data', ['data' => __('messages.withdraw')]));
        }
    }


    public function updateById($id, array $data)
    {
        $withdraw  = $this->findByIdMethod($id);
        if (!$withdraw) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.withdraw'), 'id' => $id]));
        }
        // stranr transction
        DB::beginTransaction();
        try {
            $withdraw->update([
                'charge'                    => round($data['charge'], 2),
                'receive'                   => round($withdraw->amount, 2) - round($data['charge']),
                'date'                      => $data['date'],
                'time'                      => $data['time'],
                'system_bank_account_id'    => $data['system_bank_account_id'],
                'admin_remark'              => $data['admin_remark'],
                'user_remark'               => $data['user_remark'] ?? $withdraw->user_remark,
            ]);
            $this->clearCacheTags();
            $this->clearCacheTagById($withdraw->id);
            DB::commit();
            return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.withdraw')]), $withdraw);
        } catch (\Throwable $th) {
            // rollback insert sql
            DB::rollBack();
            return $this->coreResponse(500, __('messages.not_update_data', ['data' => __('messages.withdraw')]), $th->getMessage());
        }
    }

    public function approveById($id, array $data)
    {
        $withdraw = $this->findByIdMethod($id);

        if (!$withdraw) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.withdraw'), 'id' => $id]));
        }

        if ($withdraw->status != 'waiting') {
            return $this->coreResponse(404, __('messages.no_data_with_credit', ['data' => __('messages.status_credit'), 'credit' => $withdraw->status]));
        }

        //check image upload for transfer
        $image = $withdraw->images;
        if ($data['method'] == 'transfer' && $image->isEmpty()) {
            return $this->coreResponse(404, __('messages.no_data_with_image', ['data' => __('messages.withdraw_not_image')]));
        }

        $user_bank         = $withdraw->user_bank;
        $user_account      = $withdraw->user_account;
        $system_bank       = $withdraw->system_bank;
        $system_account    = $withdraw->system_account;

        if (@$data['method'] === 'transfer') {
            $withdraw->user_bank_account_id      = $data['user_bank_account_id'];
            $withdraw->system_bank_account_id    = $data['system_bank_account_id'];
            $user_banks                          = $withdraw->load('userBankAccount')->userBankAccount;
            $system_banks                        = $withdraw->load('systemBankAccount')->systemBankAccount;

            $user_bank            = $user_banks->bank;
            $user_account         = $user_banks->account;
            $system_bank          = $system_banks->bank;
            $system_account       = $system_banks->account;
        }
        // stranr transction
        DB::beginTransaction();
        try {
            $auth                     = auth()->user();
            $withdraw->update([
                'charge'                    => $data['charge'],
                'date'                      => $data['date'],
                'time'                      => $data['time'],
                'receive'                   => ($withdraw->amount - $data['charge']),
                // 'user_bank_account_id'      => $data['user_bank_account_id'],
                // 'system_bank_account_id'    => $data['system_bank_account_id'],
                'admin_remark'              => @$data['admin_remark'],
                'user_remark'               => @$data['user_remark'],
                'method'                    => $data['method'],
                'approved_at'               => date('Y-m-d H:i:s'),
                'approved_admin'            => $auth->username,
                'approved_admin_id'         => $auth->id,
                'status'                    => 'approved',
                'user_bank'                 => $user_bank,
                'user_account'              => $user_account,
                'system_bank'               => $system_bank,
                'system_account'            => $system_account,
            ]);

            $remain_credit              = app('App\Repositories\UserRepository')->findByCodeMethod($withdraw->user_code);
            $this->credit::updateOrCreate(
                [
                    'type'              => 'withdraw',
                    'creditable_type'   => 'App\Models\Withdraw',
                    'creditable_id'     => $withdraw->id,
                    'user_code'         => $withdraw->user_code,
                    'amount'            => $withdraw->amount,
                ],
                [
                    'before'            => $remain_credit->total,
                    'after'             => ($remain_credit->total - $withdraw->amount),
                    'approved_at'       => date('Y-m-d H:i:s'),
                    'approved_admin'    => $auth->username,
                    'approved_admin_id' => $auth->id,
                ]
            );

            $this->clearCacheTags();
            $this->clearCacheTagById($withdraw->id);
            app('App\Repositories\UserRepository')->clearCacheTagByUserCode($withdraw->user_code);
            DB::commit();
            return $this->coreResponse(200, __('messages.approve_data', ['data' => __('messages.withdraw')]), $withdraw);
        } catch (\Throwable $th) {
            // rollback insert sql
            DB::rollBack();
            return $this->coreResponse(500, __('messages.not_approve_data', ['data' => __('messages.withdraw')]), $th->getMessage());
        }
    }

    public function cancelById($id, array $data)
    {
        $withdraw      = $this->findByIdMethod($id);
        if (!$withdraw) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.withdraw'), 'id' => $id]));
        }
        if ($withdraw->status != 'waiting') {
            return $this->coreResponse(500, __('messages.no_data_with_credit', ['data' => __('messages.status_credit'), 'credit' => $withdraw->status]));
        }
        // stranr transction
        DB::beginTransaction();
        try {
            $auth                  = auth()->user();
            $timestamp             = date('Y-m-d H:i:s');
            $withdraw->update([
                'status'                =>  'cancel',
                'user_remark'           =>  Util::concatField($withdraw->user_remark, $data['remark']),
                'approved_admin'        =>  $auth->username,
                'approved_admin_id'     =>  $auth->id,
                'approved_at'           =>  $timestamp,

            ]);

            $withdraw->credits->each(function ($credit) use ($withdraw, $auth, $timestamp) {
                $remain_credit              = app('App\Repositories\UserRepository')->findByCodeMethod($withdraw->user_code);
                $current_credit             = $remain_credit->total;
                $withdraw->credit()->create([
                    'type'                 => 'topup',
                    'user_code'            => $credit->user_code,
                    'amount'               => $credit->amount,
                    'before'                => $current_credit,
                    'after'                 => $current_credit + $credit->amount,
                    'approved_admin'       => $auth->username,
                    'approved_admin_id'    => $auth->id,
                    'approved_at'          => $timestamp
                ]);
            });

            $this->clearCacheTags();
            $this->clearCacheTagById($withdraw->id);
            app('App\Repositories\UserRepository')->clearCacheTagByUserCode($withdraw->user_code);
            DB::commit();
            return $this->coreResponse(200, __('messages.cancel_data', ['data' => __('messages.withdraw')]), $withdraw);
        } catch (\Throwable $th) {
            // rollback insert sql
            DB::rollBack();
            return $this->coreResponse(500, __('messages.not_cancel_data', ['data' => __('messages.withdraw')]));
        }
    }

    public function softDeleteById($id)
    {
        $withdraw       = $this->findByIdMethod($id);
        if (!$withdraw) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.withdraw'), 'id' => $id]));
        }
        $withdraw->delete();
        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();
        $this->clearCacheTags();
        return $this->coreResponse(200, __('messages.delete_data', ['data' => __('messages.withdraw')]), $withdraw);
    }

    private function findByIdMethod($id, array $columns = ['*'])
    {
        $key = $this->getCacheKey(null, $id);
        $time = $this->getTime(self::MINUTE_CACHE);
        $withdraw  =  Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getUserCodePrefix($id),
        ])->remember($key, $time, function () use ($id, $columns) {
            return $this->withdraw::select($columns)
                ->with(self::RELESTION_FIND)
                ->find($id);
        });
        return $withdraw;
    }

    private function findByUserCodeMethod($userCode, $columns = ['*'])
    {
        $key = $this->getCacheKey(null, $userCode);
        $time = $this->getTime(self::MINUTE_CACHE);

        $withdraw  = Cache::tags([
            $this->getClassPrefix(),
            $this->getUserCodePrefix($userCode),
        ])->remember($key, $time, function () use ($userCode, $columns) {
            return $this->withdraw::select($columns)
                ->with(self::RELESTION_FIND)
                ->whereUserCode($userCode)
                ->get();
        });
        return $withdraw;
    }

    private function clearCacheTags()
    {
        Cache::tags([
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix()
        ])->flush();
    }

    public function clearCacheTagById($id)
    {
        Cache::tags([$this->getFindByIdWithClassPrefix($id)])->flush();
    }
}
