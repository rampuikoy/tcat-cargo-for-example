<?php

return [
    'registered'            => 'TCAT-CARGO ยินดีต้อนรับคุณ :name รหัสลูกค้า :code https://tcatcargo.com',
    'withdraw_approved'     => 'บิล :no ถอนเครดิตสำเร็จ คงเหลือ :total บ.',
    'topup_approved'        => 'บิล :no เติมเครดิตสำเร็จ คงเหลือ :total บ.',
    'sms_bill_created'      => 'บิลขนส่งใหม่ :no ยอดที่ต้องชำระ :total บ.',
    'sms_bill_paid'         => 'บิลขนส่ง :no ชำระเงินแล้ว/เตรียมจัดส่ง',
    'sms_bill_shipped'      => 'บิลขนส่ง :no จัดส่งแล้ว ช่องทาง :method',
    'sms_bill_cancel'       => 'บิลขนส่ง :no ถูกยกเลิก',
    'sms_bill_dropped'      => 'บิลขนส่ง :no จัดส่งถึง :method แล้ว สามารถติดต่อรับสินค้าได้ในเวลาทำการ',
    'sms_bill_shipping'     => 'บิลขนส่ง :no พร้อมจัดส่งวันนี้  ผู้ส่ง: :tel(:driver)',
    // 'bill_shipping'     => 'บิลขนส่ง :no พร้อมจัดส่งวันนี้  ผู้ส่ง: :tel(:driver)',

    // 'bill_shipped'      => 'บิลขนส่ง :no จัดส่งแล้ว ช่องทาง :method',
    // 'bill_dropped'      => 'บิลขนส่ง :no จัดส่งถึง :method แล้ว สามารถรับติดต่อสินค้าได้ในเวลาทำการ',

    // 'money_created'     => 'บิลโอนเงินใหม่ :no จำนวน :amount หยวน ยอดที่ต้องชำระ :total บ.',
    // 'money_cancel'      => 'บิลโอนเงิน :no ถูกยกเลิก',
    // 'money_paid'        => 'บิลโอนเงิน :no ชำระเงินแล้ว/รอทำรายการ',
    // 'money_success'     => 'บิลโอนเงิน :no สำเร็จ/ปิดบิล',
    // 'topup_success'     => 'บิล :no เติมเครดิตสำเร็จ คงเหลือ :total บ.',
    'trip_created'  	=> 'บิลใหม่ ChinaTripTour :no  ยอดที่ต้องชำระ :total บ.',
    'trip_paid'         => 'บิล ChinaTripTour :no ชำระเงินแล้ว',
    // 'trip_success'      => 'บิล ChinaTripTour :no สำเร็จ/ปิดบิล',
    'trip_cancel'       => 'บิล ChinaTripTour :no ถูกยกเลิก',

    'point_order_success'   => 'แลกสินค้า :no สำเร็จแล้ว เลขพัสดุ :tracking ค่าขนส่ง :cost บาท',
    'point_order_cancel'    => 'แลกสินค้า :no ถูกยกเลิก',
];
