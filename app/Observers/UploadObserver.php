<?php

namespace App\Observers;

use App\Observers\BaseObserver;
use App\Models\Upload;
use App\Traits\UploadTrait;

class UploadObserver extends BaseObserver
{
    use UploadTrait;
    /**
     * Handle the upload "created" event.
     *
     * @param  \App\Upload  $upload
     * @return void
     */
    public function creating(Upload $upload)
    {
        //
        $this->stampCreated($upload);
    }

    /**
     * Handle the upload "deleted" event.
     *
     * @param  \App\Upload  $upload
     * @return void
     */
    public function deleted(Upload $upload)
    {
        //
        $oldFilePath = $upload->getFileUrlAttribute('file_path');
        $this->removeFile($upload->getFileUrlAttribute($oldFilePath));
    }
}
