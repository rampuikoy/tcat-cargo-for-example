@component('mail::message')
# เรียนคุณ {{$bill->user->name}}
แจ้งอัพเดทสถานะบิลขนส่ง <br>
รหัสลูกค้า: {{$bill->user->code}}

@component('mail::table')
| ข้อมูลชำระบิล   |               |
|:-------------|--------------:|
| เลขที่บิล      | {{ $bill->no }} |
| ยอดที่ต้องชำระ  | {{number_format($bill->total_price,2)}} |
| สถานะบิล | <span style="color: #1bc5bd; text-align: left;">ชำระเงินแล้ว/เตรียมจัดส่ง</span> |
| จำนวน       | {{ $bill->counter }} ชิ้น|
| การขนส่งภายในประเทศ | {{ $bill->thaiShippingMethod->title }}|
| ชื่อผู้รับ   {{ $bill->shipping_name }} / เบอร์โทร: {{$bill->shipping_tel}} |
|ที่อยู่จัดส่ง {{$bill->shipping_address}} ต.{{@$bill->shippingDistrict->title_th}}|
| อ.{{@$bill->shippingAmphur->title_th}} จ.{{@$bill->shippingProvince->title_th}} {{$bill->shipping_zipcode}}|
@endcomponent
<?php
$credit = ($bill->user->credit)? $bill->user->credit->total : 0 ;
?>
 <span style="color: #1bc5bd">ชำระเงินเรียบร้อยแล้ว</span><br>
# ยอดเครดิตคงเหลือของคุณ <span style="color: #1bc5bd">{{number_format($credit, 2)}}</span> บาท
@endcomponent
