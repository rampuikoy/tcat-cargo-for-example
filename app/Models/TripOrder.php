<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TripOrder extends Model
{
    use HasFactory;
    protected $fillable = [
    	'title',
        'type',
    	'price',
    	'amount',
    	'total',
    	'trip_id',
	];

    public static function tripType()
    {
        $array = [
            'visa'          => __('messages.trips_order.visa'),
            'airplane'      => __('messages.trips_order.airplane'),
            'hotel'         => __('messages.trips_order.hotel'),
            'car'           => __('messages.trips_order.car'),
            'giude'         => __('messages.trips_order.giude'),
            'group_tour'    => __('messages.trips_order.group_tour'),
            'private_tour'  => __('messages.trips_order.private_tour'),
            'other'         => __('messages.trips_order.other'),
        ];
        return $array;
    }

    protected $appends = ['current_type'];

    public function getCurrentTypeAttribute()
    {
        $type = collect(self::tripType())->get($this->type);
        return (!empty($type)) ? $type : null;
    }

    public function trip()
    {
        return $this->belongsTo('App\Models\Trip','trip_id');
    }
}
