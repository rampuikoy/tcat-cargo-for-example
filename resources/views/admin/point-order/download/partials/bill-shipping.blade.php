<table class="thai-shipping" aria-describedby="bill-point-order-thai-shipping">
	<thead style="background: #dfdfdf;">
		<tr>
			<th colspan="2"  scope="col" class="bordered">
				<div>
					@lang('admin.thai_shipping_method')&nbsp;/&nbsp;{!!str_replace(' ', '&nbsp;',$bill->shippingMethod->title)!!}
				</div>
			</th>
		</tr>
	</thead>
	<tbody class="bordered" style="font-size: 19px;">
		<tr>
			<td width="25%" class="text-right">
				<div>ชื่อผู้รับ&nbsp;: </div>
			</td>
			<td>
				<div>{!!str_replace(' ', '&nbsp;',$bill->title)!!}&nbsp;/&nbsp;โทร&nbsp;:&nbsp;{{$bill->tel}}
				</div>
			</td>
		</tr>

		<tr>
			<td class="text-right">
				<div>@lang('admin.address')&nbsp;: </div>
			</td>
			<td>
				<div>
					{!!str_replace(' ', '&nbsp;',$bill->address)!!}&nbsp;ต.{{$bill->district}}&nbsp;อ.{{$bill->amphur}}&nbsp;จ.{{$bill->province}}&nbsp;{{@$bill->zipcode}}
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<div style="height: 3px;"></div>
			</td>
		</tr>
	</tbody>
</table>