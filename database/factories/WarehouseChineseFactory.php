<?php

namespace Database\Factories;

use App\Models\WarehouseChinese;
use Illuminate\Database\Eloquent\Factories\Factory;
use Carbon\Carbon;
class WarehouseChineseFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = WarehouseChinese::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->state;
        return [
        'created_at' => Carbon::now(),
        'title_th' =>  $title,
        'title_en' => $title,
        'title_cn' => $title,
        'latitude' => $this->faker->latitude($min = -90, $max = 90),
        'longitude' =>  $this->faker->longitude($min = -180, $max = 180),
        'info_th' =>  $this->faker->text($maxNbChars = 300),
        'info_en' => $this->faker->text($maxNbChars = 300),
        'info_cn' => $this->faker->text($maxNbChars = 300),
        'remark' => $this->faker->text($maxNbChars = 100),
        ];
    }
}
