<?php

namespace App\Http\Requests\NoCodeByTrackId;

use App\Http\Requests\FormRequest;

class UpdateNoCodeByTrackIdRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.tracking.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'sometimes|in:clothes,bag,accessories,electronics,toy,other,cosmetics,spares,shoe'
        ];
    }
}
