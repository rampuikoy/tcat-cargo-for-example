<?php

namespace App\Http\Requests\NoCode;

use App\Http\Requests\FormRequest;

class UpdateNoCodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.nocode.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code'              => 'required|string',
            'date_in'           => 'required|date_format:Y-m-d',
            'order_code'        => 'sometimes|nullable|string',
            'reference_code'    => 'sometimes|nullable|string',
            'zone'              => 'sometimes|nullable|string',
            'description'       => 'sometimes|nullable|string',
            'remark'            => 'sometimes|nullable|string',
            'type'              => 'sometimes|nullable|in:clothes,bag,accessories,electronics,toy,other,cosmetics,spares,shoe',
            'department'        => 'sometimes|nullable|in:TCATCARGO,TMALL,TAOBAO,ALIBABA',
            'weight'            => 'sometimes|nullable|numeric',
            'width'             => 'sometimes|nullable|numeric',
            'height'            => 'sometimes|nullable|numeric',
            'length'            => 'sometimes|nullable|numeric',
            'amount'            => 'sometimes|nullable|integer',
            'list'              => 'sometimes|nullable|integer',
        ];
    }

        /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'code'              => __('form.no_code.code'),
            'date_in'           => __('form.no_code.date_in'),
            'order_code'        => __('form.no_code.order_code'),
            'reference_code'    => __('form.no_code.reference_code'),
            'zone'              => __('form.no_code.zone'),
            'description'       => __('form.no_code.description'),
            'remark'            => __('form.no_code.remark'),
            'type'              => __('form.no_code.type'),
            'department'        => __('form.no_code.department'),
            'weight'            => __('form.no_code.weight'),
            'width'             => __('form.no_code.width'),
            'height'            => __('form.no_code.height'),
            'length'            => __('form.no_code.length'),
            'amount'            => __('form.no_code.amount'),
            'list'              => __('form.no_code.list'),
        ];
    }
}
