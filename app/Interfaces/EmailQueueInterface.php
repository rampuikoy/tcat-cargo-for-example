<?php

namespace App\Interfaces;

interface EmailQueueInterface
{
    const DEFAULT_LIMIT = 25;
    const DEFAULT_SORTING = 'id';
    const DEFAULT_ASCENDING = 'desc';

    /**
     * fetch all email
     *
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function all(array $columns = ['*'], $sorting = EmailQueueInterface::DEFAULT_SORTING, $ascending = EmailQueueInterface::DEFAULT_ASCENDING);

    /**
     * list email
     *
     * @param array $conditions
     * @param array $columns
     * @param int $offset
     * @param int $limit
     *
     * @return \Illuminate\Support\Collection
     */
    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * pagination email
     *
     * @param array $conditions
     * @param array $columns
     * @param int $page
     * @param int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $perPage = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * find email by id
     *
     * @param $id
     * @param array $columns
     *
     * @return object|null
     */
    public function findById($id);

    /**
     * get email dropdown list
     *
     * @return object
     */
    public function dropdownList();

    /**
     * update email status and dispatch now
     *
     * @param array $id
     *
     * @return object
     */
    public function updateManyQueue(array $ids);

    /**
     * resend mail
     *
     * @param $id
     *
     * @return object
     */
    public function resend($id);
}
