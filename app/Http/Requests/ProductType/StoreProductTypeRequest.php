<?php

namespace App\Http\Requests\ProductType;

use App\Http\Requests\FormRequest;

class StoreProductTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.product-type.store');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_th'          => 'required|string',
            'title_en'          => 'required|string',
            'title_cn'          => 'required|string',
            'status'            => 'required|in:active,inactive',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'title_th'          => __('form.product_type.title_th'),
            'title_en'          => __('form.product_type.title_en'),
            'title_cn'          => __('form.product_type.title_cn'),
            'status'            => __('form.product_type.status')
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
