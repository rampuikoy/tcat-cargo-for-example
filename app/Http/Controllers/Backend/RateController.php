<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Rate\StoreRateRequest;
use App\Http\Requests\Rate\UpdateRateRequest;
use App\Http\Requests\Rate\UpdateRateUserRequest;
use App\Interfaces\AdminInterface;
use App\Interfaces\RateInterface;
use App\Models\Rate;
use Illuminate\Http\Request;

class RateController extends Controller
{
    protected $rateInterface,$adminInterface;

    public function __construct(RateInterface $rateInterface, AdminInterface $adminInterface)
    {
        $this->rateInterface = $rateInterface;
        $this->adminInterface = $adminInterface;
    }

    public function getDefault()
    {
        $this->authorize('show', Rate::class);
        $rate = $this->rateInterface->findById(1, ['*']);
        return response()->json($rate, $rate->code);
    }

    public function index(Request $request)
    {
        $this->authorize('index', Rate::class);
        $columns    = $request->input('columns', ['*']);
        $rates      = $this->rateInterface->all($columns);
        return response()->json($rates, $rates->code);
    }

    public function show(Request $request, $id)
    {
        $this->authorize('show', Rate::class);
        $columns    = $request->input('columns', ['*']);
        $rate       = $this->rateInterface->findById($id, $columns);
        return response()->json($rate, $rate->code);
    }

    public function search(Request $request)
    {
        $this->authorize('index', Rate::class);
        $conditions = $request->only(['id', 'search', 'title', 'admin_remark', 'user_code', 'type', 'status', 'condition']);
        $columns    = $request->input('columns', ['*']);
        $limit      = $request->input('limit', $this->rateInterface::DEFAULT_LIMIT);
        $sorting    = $request->input('order_by', $this->rateInterface::DEFAULT_SORTING);
        $ascending  = $request->input('ascending') == 1 ? 'asc' : 'desc';
        if ($request->boolean('pagination')) {
            $page   = $request->input('page', 1);
            $rates  = $this->rateInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset = $request->input('page', 0);
            $rates  = $this->rateInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($rates, $rates->code);
    }

    public function searchUser(Request $request, $id)
    {
        $this->authorize('index', Rate::class);
        $columns    = $request->input('columns', ['*']);
        $rate       = $this->rateInterface->findManyByIdSalse($id, $columns);
        return response()->json($rate, $rate->code);
    }

    public function dropdown()
    {
        $dropdown   = $this->rateInterface->dropdownList();
        if (!$dropdown->error) {
            $sales  = $this->adminInterface->findManySale(['id', 'name']);
            $dropdown->results  = collect($dropdown->results)
                ->merge(['sales' => (!$sales->error) ? $sales->results : []]);
        }
        return response()->json($dropdown, $dropdown->code);
    }

    public function store(StoreRateRequest $request)
    {
        $data   = $request->only([
            'title',
            'status',
            'admin_remark',
            'user_code',

            'kg_car_genaral',
            'kg_car_iso',
            'kg_car_brand',

            'kg_ship_genaral',
            'kg_ship_iso',
            'kg_ship_brand',

            'kg_plane_genaral',
            'kg_plane_iso',
            'kg_plane_brand',

            'cubic_car_genaral',
            'cubic_car_iso',
            'cubic_car_brand',

            'cubic_ship_genaral',
            'cubic_ship_iso',
            'cubic_ship_brand',

            'cubic_plane_genaral',
            'cubic_plane_iso',
            'cubic_plane_brand',

            'default_rate',
            'type',
            'admin_id',
        ]);

        $rate   = $this->rateInterface->store($data);
        return response()->json($rate, $rate->code);
    }

    public function update(UpdateRateRequest $request, $id)
    {
        $data   = $request->only([
            'title',
            'status',
            'admin_remark',
            'user_code',

            'kg_car_genaral',
            'kg_car_iso',
            'kg_car_brand',

            'kg_ship_genaral',
            'kg_ship_iso',
            'kg_ship_brand',

            'kg_plane_genaral',
            'kg_plane_iso',
            'kg_plane_brand',

            'cubic_car_genaral',
            'cubic_car_iso',
            'cubic_car_brand',

            'cubic_ship_genaral',
            'cubic_ship_iso',
            'cubic_ship_brand',

            'cubic_plane_genaral',
            'cubic_plane_iso',
            'cubic_plane_brand',

            'default_rate',
            'type',
            'admin_id',
        ]);

        $rate   = $this->rateInterface->updateById($id, $data);
        return response()->json($rate, $rate->code);
    }

    public function destroy($id)
    {
        $this->authorize('destroy', Rate::class);
        $rate   = $this->rateInterface->softDeleteById($id);
        return response()->json($rate, $rate->code);
    }

    public function getUserRate(Request $request, $code)
    {
        $this->authorize('show', Rate::class);
        $columns        = $request->input('columns', ['*']);
        $rate           = $this->rateInterface->findByUserCode($code, $columns);
        $defaultRate    = $this->rateInterface->findById(1, ['*']);

        if (!$rate->error) {
            $rate->results = [
                'rate'      => $rate->results,
                'default'   => $defaultRate->results,
            ];
        }

        return response()->json($rate, $rate->code);
    }

    public function putUserRate(UpdateRateUserRequest $request, $code)
    {
        $data       = $request->only([
            'admin_id',
            "cubic_car_brand",
            "cubic_car_genaral",
            "cubic_car_iso",
            "cubic_plane_brand",
            "cubic_plane_genaral",
            "cubic_plane_iso",
            "cubic_ship_brand",
            "cubic_ship_genaral",
            "cubic_ship_iso",
            "default_rate",
            "kg_car_brand",
            "kg_car_genaral",
            "kg_car_iso",
            "kg_plane_brand",
            "kg_plane_genaral",
            "kg_plane_iso",
            "kg_ship_brand",
            "kg_ship_genaral",
            "kg_ship_iso",
            "title",
            "user_code",
        ]);
        $rate   = $this->rateInterface->updateByUserCode($code, $data);
        return response()->json($rate, $rate->code);
    }
}
