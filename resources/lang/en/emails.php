<?php

return [
    'user_registed'         => 'TCAT-CARGO บิลขนส่ง ยินดีต้อนรับค่ะ',
    'topup_approved'        => 'TCAT-CARGO เติมเครดิต แจ้งเติมสำเร็จ',
    'withdraw_approved'     => 'TCAT-CARGO บิลถอนเครดิต: :no แจ้งถอนสำเร็จ'
];
