<?php

namespace App\Http\Requests\Bill;

use App\Http\Requests\FormRequest;

class PackBillRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.bill.store');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tracking_id'              => 'required|array',
        ];
    }
    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'tracking_id'                => __('form.bill.trackings'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'user_address_id.required_unless'         => __('form.bill.user_address_id'),
            'other_charge1.title.required_with_all'   => __('form.bill.other_charge'),
            'other_charge2.title.required_with_all'   => __('form.bill.other_charge'),
            'discount1.title.required_with_all'       => __('form.bill.discount'),
            'discount2.title.required_with_all'       => __('form.bill.discount')
        ];
    }
}
