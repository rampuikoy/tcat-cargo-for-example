<?php

namespace App\Http\Requests\PointOrder;

use App\Http\Requests\FormRequest;

class StorePointOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.point-order.store');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'user_code'             => 'required|max:10|exists:users,code',
            'shipping_method_id'    => 'required',
            'address_id'            => 'sometimes|nullable',
            'products'              => 'required|array|min:1',
            'products.*.id'         => 'required|integer',
            'products.*.amount'     => 'required|integer',
        ];
        return $rules;
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'user_code'             => __('form.point_order.user_code'),
            'shipping_method_id'    => __('form.point_order.shipping_method_id'),
            'address_id'            => __('form.point_order.address_id'),
            'products'              => __('form.point_order.products'),
            'products.*.id'         => __('form.point_order.id'),
            'products.*.amount'     => __('form.point_order.amount'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
