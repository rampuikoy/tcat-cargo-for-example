<?php

return [

    'Verified' => 'อีเมลของคุณได้รับการยืนยันแล้ว!',
    'invalid' => 'ลิงค์ยืนยันไม่ถูกต้อง',
    'already_verified' => 'อีเมลได้รับการยืนยันแล้ว',
    'user' => 'เราไม่พบผู้ใช้ที่มีที่อยู่อีเมลนั้น',
    'sent' => 'เราได้ส่งลิงค์ยืนยันของคุณไปทางอีเมลแล้ว!',

];
