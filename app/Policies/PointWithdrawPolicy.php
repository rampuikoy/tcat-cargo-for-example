<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class PointWithdrawPolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.point.index');
    }

    public function show(Admin $admin)
    {
        return $admin->hasAnyPermission(['backend.point.show', 'backend.point.update']);
    }

    public function store(Admin $admin)
    {
        return $admin->can('backend.point.store');
    }

    public function approve(Admin $admin)
    {
        return $admin->can('backend.point.approve');
    }

    public function destroy(Admin $admin)
    {
        return $admin->can('backend.point.destroy');
    }
}
