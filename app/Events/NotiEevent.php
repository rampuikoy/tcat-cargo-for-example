<?php

namespace App\Events;

use App\Helpers\Noti;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NotiEevent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $noti;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->noti = Noti::counterNoti();
        $this->dontBroadcastToCurrentUser();

    }

    /**
     * Get the channels the event should noti on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PresenceChannel('App.Models.Noti.Broadcast');
    }

    public function broadcastAs()
    {
        return 'new-notification';
    }
}
