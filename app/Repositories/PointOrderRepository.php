<?php

namespace App\Repositories;

use App\Interfaces\PointOrderInterface;
use App\Models\PointOrder;
use App\Models\PointOrderItem;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class PointOrderRepository implements PointOrderInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const MINUTE_CACHE          = 1;
    const TABLE_RELETIONS       = ['shippingMethod', 'products'];

    public function __construct(PointOrder $point_order, PointOrderItem $point_orderItem)
    {
        $this->pointOrder        = $point_order;
        $this->pointOrderItem    = $point_orderItem;
    }

    public function all(array $columns = ['*'], $sorting = PointOrderInterface::DEFAULT_SORTING, $ascending = PointOrderInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $point_orders     = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])
            ->remember($key, $time, function () use ($columns, $sorting, $ascending) {
                return $this->pointOrder::with('shippingMethod')
                    ->select($columns)
                    ->orderBy($sorting, $ascending)
                    ->get();
            });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.point_order')]), $point_orders);
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = pointOrderInterface::DEFAULT_LIMIT, $sorting = pointOrderInterface::DEFAULT_SORTING, $ascending = pointOrderInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $point_orders     = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
            return $this->pointOrder::with('shippingMethod')
                ->select($columns)
                ->search($conditions)
                ->orderBy($sorting, $ascending)
                ->skip($offset)
                ->limit($limit)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.point_order')]), $point_orders);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = pointOrderInterface::DEFAULT_LIMIT, $sorting = pointOrderInterface::DEFAULT_SORTING, $ascending = pointOrderInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $point_orders     = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
            $model = $this->pointOrder::with('shippingMethod')
                ->select($columns)
                ->search($conditions)
                ->orderBy($sorting, $ascending);

            if ($page == 1) {
                return $model->paginate($limit);
            } else {
                return $model->skip($limit * ($page - 1))->paginate($limit);
            }
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.point_order')]), $point_orders);
    }

    public function findById($id, array $columns = ['*'])
    {
        $point_order = $this->findByIdMethod($id, $columns);

        if (!$point_order) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.point_order'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.point_order')]), $point_order);
    }

    public function findByCode($code, array $columns = ['*'])
    {
        $point_order = $this->findByCodeMethod($code, $columns);

        if (!$point_order) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.point_order'), 'id' => $code]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.point_order')]), $point_order);
    }

    public function dropdownList()
    {
        $key        = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $dropdown   = Cache::remember($key, $time, function () {
            return [
                'status'        => $this->pointOrder::statusList(),
                'time_format'   => $this->pointOrder::timeFormatList(),
            ];
        });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function store(array $data)
    {
        // stranr transction
        DB::beginTransaction();
        try {
            $user_point         = app('App\Repositories\UserRepository')->findByCodeMethod($data['user_code'])->point->total ?? 0;
            $shipping           = app('App\Repositories\ThaiShippingMethodRepository')->findByIdMethod($data['shipping_method_id']);

            $products           = collect($data['products'])->map(function ($item) use ($data) {
                $product =  $this->checkProductLimit($data['user_code'], $item['id'], $item['amount']);
                return collect($product)->merge(['amount' => $item['amount']])->toArray();
            });
            $total_point        = collect($products)->reduceWithKeys(function ($sum, $item) {
                $sum_point      = $item['point'] * $item['amount'];
                return $sum     += $sum_point;
            }, 0);

            if ($total_point > $user_point) {
                throw new \Exception(__('messages.point_not_enough', ['total' => $user_point]));
            }
            if ($shipping->type !== 'droppoint' && !$data['address_id']) {
                throw new \Exception(__('messages.no_data_with_id', ['data' => __('messages.user_address'), 'id' => $data['address_id']]));
            }

            $address            = $this->getShippingOrAddress($shipping, $data['address_id']);

            // create point oreder
            $point_order        = $this->pointOrder::create([
                'user_code'             => $data['user_code'],
                'user_remark'           => $data['user_remark'],
                'admin_remark'          => $data['admin_remark'],
                'total'                 => $total_point,
                'shipping_method_id'    => $shipping->id,
                'address_id'            => $shipping->type !== ' droppoint' ? $address->id : null,
                'title'                 => $shipping->type !== ' droppoint' ? $shipping->title : $address->name,
                'tel'                   => $address->tel,
                'address'               => $address->address,
                'province'              => $address->province->title_th,
                'amphur'                => $address->amphur->title_th,
                'district'              => $address->district->title_th,
                'zipcode'               => $address->zipcode,
                'province_id'           => $address->province->id,
                'amphur_id'             => $address->amphur->id,
                'district_id'           => $address->district->id,
            ]);

            // create point order item releation
            $point_order->products()->sync($products->map(function ($product) use ($point_order) {
                return [
                    'order_id'          => $point_order->id,
                    'product_id'        => $product['id'],
                    'amount'            => $product['amount'],
                    'point'             => $product['point'],
                    'total'             => $product['point'] * $product['amount'],
                ];
            }));

            // create point record withdraw
            $point_order->point()->create([
                'type'              => 'withdraw',
                'user_code'         => $point_order->user_code,
                'amount'            => $point_order->total,
            ]);

            $this->clearCacheTags();
            $this->clearCacheTagCheckLimit($point_order->user_code);
            app('App\Repositories\UserRepository')->clearCacheTagByUserCode($point_order->user_code);
            DB::commit();
            return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.point_order')]), $point_order);
        } catch (\Throwable $th) {
            // rollback insert sql
            DB::rollBack();
            $message = $th->getMessage() ?? __('messages.not_create_data', ['data' => __('messages.point_order')]);
            return $this->coreResponse(500, $message, $th->getTrace());
        }
    }

    public function updateById($id, array $data)
    {
        $point_order        = $this->findByIdMethod($id);

        if (!$point_order) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.point_order'), 'id' => $id]));
        }

        $point_order->update([
            'user_remark'   => $data['user_remark'],
            'admin_remark'  => $data['admin_remark'],
        ]);
        $this->clearCacheTags();
        $this->clearCacheTagById($point_order->id);
        $this->clearCacheTagByCode($point_order->code);
        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.point_order')]), $point_order);
    }

    public function approveById($id)
    {
        $point_order            = $this->findByIdMethod($id);
        // stranr transction
        DB::beginTransaction();
        try {  
            if (!$point_order) {
                throw new \Exception(__('messages.no_data_with_id', ['data' => __('messages.point_order'), 'id' => $id]));
            }
            if ($point_order->status !== 'waiting') {
                throw new \Exception(__('messages.no_data_status', ['data' => __('messages.point_order'), 'status' => $point_order->status]));
            }

            $admin = auth()->user();
            // update approved
            $point_order->update([
                'status'            => 'approved',
                'approved_admin'    => $admin->username,
                'approved_admin_id' => $admin->id,
                'approved_at'       => date('Y-m-d H:i:d'),
            ]);
            $this->clearCacheTags();
            $this->clearCacheTagById($point_order->id);
            $this->clearCacheTagByCode($point_order->code);
            DB::commit();
            return $this->coreResponse(200, __('messages.approve_data', ['data' => __('messages.point_order')]), $point_order);
        } catch (\Throwable $th) {
            // rollback insert sql
            DB::rollBack();
            $message = $th->getMessage() ?? __('messages.no_approve_data', ['data' => __('messages.point_order')]);
            return $this->coreResponse(500, $message, $th->getTrace());
        }
    }

    public function packByCode($code)
    {
        $point_order        = $this->findByCodeMethod($code);
        // stranr transction
        DB::beginTransaction();
        try {            
            if (!$point_order) {
                throw new \Exception(__('messages.no_data_with_id', ['data' => __('messages.point_order'), 'id' => $code]));
            }
            if ($point_order->status !== 'approved') {
                throw new \Exception(__('messages.no_data_status', ['data' => __('messages.point_order'), 'status' => $point_order->status]));
            }
        
            $admin = auth()->user();
            // update to status pack
            $point_order->update([
                'status'             => 'packed',
                'packed_admin'       => $admin->username,
                'packed_admin_id'    => $admin->id,
                'packed_at'          => date('Y-m-d H:i:d')
            ]);

            $this->clearCacheTags();
            $this->clearCacheTagById($point_order->id);
            $this->clearCacheTagByCode($point_order->code);
            DB::commit();
            return $this->coreResponse(200, __('messages.approve', ['data' => __('messages.product')]), $point_order);
        } catch (\Throwable $th) {
            // rollback insert sql
            DB::rollBack();
            $message = $th->getMessage() ?? __('messages.no_packed_data', ['data' => __('messages.product')]);
            return $this->coreResponse(500, $message, $th->getTrace());
        }
    }

    public function shipByCode($code, array $data)
    {
        $point_order        = $this->findByCodeMethod($code);
        // stranr transction
        DB::beginTransaction();
        try {            
            if (!$point_order) {
                throw new \Exception(__('messages.no_data_with_id', ['data' => __('messages.point_order'), 'id' => $code]));
            }
            if ($point_order->status !== 'packed') {
                throw new \Exception(__('messages.no_data_status', ['data' => __('messages.point_order'), 'status' => $point_order->status]));
            }
        
            $admin      = auth()->user();
            $truck      = app('App\Repositories\TruckRepository')->findByIdMethod($data['truck_id']); 
            $driver_1   = app('App\Repositories\AdminRepository')->findByIdMethod($data['driver_admin_id']) ??  null;  
            $driver_2   = app('App\Repositories\AdminRepository')->findByIdMethod($data['driver2_admin_id']) ??  null;  

            // update to status ship
            $point_order->update([
                'status'             => 'shipped',
                'shipped_admin'      => $admin->username,
                'shipped_admin_id'   => $admin->id,
                'shipped_at'         => date('Y-m-d H:i:d'),

                'truck_id'           => @$truck->id,
                'driver_admin_id'    => @$driver_1->id,
                'driver_admin'       => @$driver_1->name,
                'driver2_admin_id'   => @$driver_2->id,
                'driver2_admin'      => @$driver_2->name,
            ]);

            $this->clearCacheTags();
            $this->clearCacheTagById($point_order->id);
            $this->clearCacheTagByCode($point_order->code);
            DB::commit();
            return $this->coreResponse(200, __('messages.save_data', ['data' => __('messages.point_order')]), $point_order->load('truck'));
        } catch (\Throwable $th) {
            // rollback insert sql
            DB::rollBack();
            $message = $th->getMessage() ?? __('messages.no_save_data', ['data' => __('messages.point_order')]);
            return $this->coreResponse(500, $message, $th->getTrace());
        }
    }

    public function confirmByCode($code, array $data)
    {
        $point_order        = $this->findByCodeMethod($code);
        // stranr transction
        DB::beginTransaction();
        try {            
            if (!$point_order) {
                throw new \Exception(__('messages.no_data_with_id', ['data' => __('messages.point_order'), 'id' => $code]));
            }
            if ($point_order->status !== 'shipped') {
                throw new \Exception(__('messages.no_data_status', ['data' => __('messages.point_order'), 'status' => $point_order->status]));
            }
        
            $admin      = auth()->user();
            // update to status cofirm
            $point_order->update([
                'status'             => 'confirmed',
                'confirmed_admin'    => $admin->username,
                'confirmed_admin_id' => $admin->id,
                'confirmed_at'       => date('Y-m-d H:i:d'),

                'shipping_method_id' => $data['shipping_method_id'],
                'shipping_cost'      => $data['shipping_cost'] ?? 0,
                'tracking'           => $data['tracking'],
            ]);

            $this->clearCacheTags();
            $this->clearCacheTagById($point_order->id);
            $this->clearCacheTagByCode($point_order->code);
            DB::commit();
            return $this->coreResponse(200, __('messages.save_data', ['data' => __('messages.point_order')]), $point_order->load('shippingMethod'));
        } catch (\Throwable $th) {
            // rollback insert sql
            DB::rollBack();
            $message = $th->getMessage() ?? __('messages.no_save_data', ['data' => __('messages.point_order')]);
            return $this->coreResponse(500, $message, $th->getTrace());
        }
    }

    public function checkLimit(string $userCode, int $productId, int $amount)
    {
        $product        = $this->checkProductLimit($userCode, $productId, $amount);
        $product_text   = $product->code . ' : ' . $product->title;
        return $this->coreResponse(200, __('messages.product_limit', ['data' => $product_text]), $product);
    }

    public function successByCode($code)
    {
        $point_order    = $this->findByCodeMethod($code);
        $remain_credit  = app('App\Repositories\UserRepository')->findByCodeMethod($point_order->user_code)->credit->total ?? 0;
        // stranr transction
        DB::beginTransaction();
        try {            
            if (!$point_order) {
                throw new \Exception(__('messages.no_data_with_id', ['data' => __('messages.point_order'), 'id' => $code]));
            }
            if ($point_order->status !== 'confirmed') {
                throw new \Exception(__('messages.no_data_status', ['data' => __('messages.point_order'), 'status' => $point_order->status]));
            }
            if ($remain_credit < $point_order->shipping_cost) {
                throw new \Exception(__('messages.no_pay_point_order', ['amount' => number_format($point_order->shipping_cost), 'credit' => number_format($remain_credit, 2)]));
            }
            $admin = auth()->user();
            // update stock product
            $point_order->products->each(function ($product) {
                $stock = $product->stock - $product->pivot->amount;
                if ($stock < 0) {
                    throw new \Exception(__('messages.no_enough_stock', ['data' => $product->code . ' : ' . $product->title]));
                }
                $product->update(['stock' => $stock]);
            });
            // update to status pack
            $point_order->update([
                'status'             => 'success',
                'succeed_admin'       => $admin->username,
                'succeed_admin_id'    => $admin->id,
                'succeed_at'          => date('Y-m-d H:i:d')
            ]);
            // create point record topup
            if(!$point_order->credit){
                $point_order->credit()->create([
                    'type'              => 'withdraw',
                    'user_code'         => $point_order->user_code,
                    // cal credit
                    'before'            => $remain_credit,
                    'amount'            => $point_order->shipping_cost,
                    'after'             => ($remain_credit - $point_order->shipping_cost),
                    // stamp
                    'created_admin'     => $admin->username,
                    'created_admin_id'  => $admin->id,
                    'approved_admin'    => $admin->username,
                    'approved_admin_id' => $admin->id,
                    'approved_at'       => date('Y-m-d H:i:s'),
                ]);
            }else{
                $point_order->credit->update([
                    'updated_admin'     => $admin->username,
                    'updated_admin_id'  => $admin->id,
                    'approved_admin'    => $admin->username,
                    'approved_admin_id' => $admin->id,
                    'approved_at'       => date('Y-m-d H:i:s'),
                ]);
            }

            $this->clearCacheTags();
            $this->clearCacheTagById($point_order->id);
            $this->clearCacheTagByCode($point_order->code);
            $this->clearCacheTagCheckLimit($point_order->user_code);
            app('App\Repositories\UserRepository')->clearCacheTagByUserCode($point_order->user_code);
            // DB::commit();
            return $this->coreResponse(200, __('messages.save_data', ['data' => __('messages.point_order')]), $point_order->load('credit'));
        } catch (\Throwable $th) {
            // rollback insert sql
            DB::rollBack();
            $message = $th->getMessage() ?? __('messages.no_save_data', ['data' => __('messages.point_order')]);
            return $this->coreResponse(500, $message, $th->getTrace());
        }
    }

    public function cancelById($id)
    {
        $point_order      = $this->findByIdMethod($id);
        // stranr transction
        DB::beginTransaction();
        try {
            if (!$point_order) {
                throw new \Exception(__('messages.no_data_with_id', ['data' => __('messages.point_order'), 'id' => $id]));
            }
            if (!in_array($point_order->status, ['waiting', 'approved'])) {
                throw new \Exception(__('messages.no_data_status', ['data' => __('messages.point_order'), 'status' => $point_order->status]));
            }

            $admin = auth()->user();
            // update to status cancel
            $point_order->update([
                'status'                => 'cancel',
                'cancelled_admin'       => $admin->username,
                'cancelled_admin_id'    => $admin->id,
                'cancelled_at'          => date('Y-m-d H:i:d')
            ]);

            // create point record topup
            $point_order->point()->create([
                'type'              => 'topup',
                'user_code'         => $point_order->user_code,
                'amount'            => $point_order->total,
            ]);

            $this->clearCacheTags();
            $this->clearCacheTagById($point_order->id);
            $this->clearCacheTagByCode($point_order->code);
            $this->clearCacheTagCheckLimit($point_order->user_code);
            app('App\Repositories\UserRepository')->clearCacheTagByUserCode($point_order->user_code);
            DB::commit();
            return $this->coreResponse(200, __('messages.cancel_data', ['data' => __('messages.point_order')]), $point_order->refresh());
        } catch (\Throwable $th) {
            // rollback insert sql
            DB::rollBack();
            $message = $th->getMessage() ?? __('messages.no_cancel_data', ['data' => __('messages.point_order')]);
            return $this->coreResponse(500, $message, $th->getTrace());
        }
    }

    private function checkProductLimit(string $userCode, int $productId, int $amount)
    {
        $key        = $this->getCacheKey(null, 'USER_CODE.' . $userCode . '.PRODUCT.' . $productId . '.AMOUNT.' . $amount);
        $time       = $this->getTime(self::MINUTE_CACHE);

        $product    = app('App\Repositories\ProductRepository')->findByIdMethod($productId);

        if (!$product) {
            throw new \Exception(__('messages.no_data_with_id', ['data' => __('messages.product'), 'id' => $productId]));
        }

        $point_order = Cache::tags([$this->getClassPrefix(), $this->getProductLimitByUserCodePrefix($userCode)])
            ->remember($key, $time, function () use ($product, $userCode) {
                $orderId     = $this->pointOrder::select('id')
                    ->whereUserCode($userCode)
                    ->where('status', '!=', 'cancel')
                    ->get()
                    ->pluck('id');

                $item       = $this->pointOrderItem::select(DB::raw('sum(amount) as amount'))
                    ->whereIn('order_id', $orderId)
                    ->where('product_id', $product->id)
                    ->first();
                return $item;
            });

        $product_text   = $product->code . ' : ' . $product->title;
        $over_limit     = ($point_order->amount + $amount) > $product->limit;
        $qty_limit      = ($point_order->amount > 0) ? max($product->limit - $point_order->amount, 0) : $product->limit;

        if ($over_limit) {
            throw new \Exception(__('messages.no_product_limit', ['data' => $product_text, 'qty' => $qty_limit]));
        }
        return $product;
    }

    // for store
    private function getShippingOrAddress($shipping, $addressId = null)
    {
        if ($shipping->type === 'droppoint') {
            return $shipping;
        }
        return app('App\Repositories\UserAddressRepository')->findByIdMethod($addressId);
    }

    public function fetchListByIds(array $ids = [])
    {
        $key    = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time   = $this->getTime(self::MINUTE_CACHE);

        $bills  = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($ids) {
            return $this->pointOrder::select(['*'])
                ->with(self::TABLE_RELETIONS)
                ->whereIn('id', $ids)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.point_order')]), $bills);
    }

    
    public function findByIdMethod($id, array $columns = ['*'])
    {
        $key        = $this->getCacheKey(null, $id);
        $time       = $this->getTime(self::MINUTE_CACHE);

        $point_order      = Cache::tags([
                            $this->getClassPrefix(),
                            $this->getFindByIdWithClassPrefix(),
                            $this->getIdWithClassPrefix($id)
                        ])
                        ->remember($key, $time, function () use ($id, $columns) {
                            return $this->pointOrder::with('shippingMethod', 'products', 'point')
                                                    ->select($columns)
                                                    ->whereId($id)
                                                    ->first();
                        });

        return $point_order;
    }

    public function findByCodeMethod($code, array $columns = ['*'])
    {
        $key        = $this->getCacheKey(null, $code);
        $time       = $this->getTime(self::MINUTE_CACHE);

        $point_order      = Cache::tags([
                            $this->getClassPrefix(),
                            $this->getCodePrefix($code),
                            $this->getCodeWithClassPrefix($code)
                        ])
                        ->remember($key, $time, function () use ($code, $columns) {
                            return $this->pointOrder::with('shippingMethod', 'truck', 'credit')
                                                    ->select($columns)
                                                    ->whereCode($code)
                                                    ->first();
                        });

        return $point_order;
    }

    private function clearCacheTags()
    {
        Cache::tags([
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix()
        ])
            ->flush();
    }

    private function clearCacheTagById($id)
    {
        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();
    }
    
    private function clearCacheTagByCode($code)
    {
        Cache::tags([$this->getCodeWithClassPrefix($code)])->flush();
    }

    private function clearCacheTagCheckLimit($userCode)
    {
        Cache::tags([$this->getProductLimitByUserCodePrefix($userCode)])->flush();
    }
}
