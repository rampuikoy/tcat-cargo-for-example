<?php

namespace App\Interfaces;

interface PointWithdrawInterface
{
    const DEFAULT_LIMIT = 25;
    const DEFAULT_SORTING = 'id';
    const DEFAULT_ASCENDING = 'desc';

    /**
     * fetch all PointWithdraw
     *
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function all(array $columns = ['*'], $sorting = PointWithdrawInterface::DEFAULT_SORTING, $ascending = PointWithdrawInterface::DEFAULT_ASCENDING);

    /**
     * list PointWithdraw
     *
     * @param array $conditions
     * @param array $columns
     * @param int $offset
     * @param int $limit
     *
     * @return \Illuminate\Support\Collection
     */
    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * pagination PointWithdraw
     *
     * @param array $conditions
     * @param array $columns
     * @param int $page
     * @param int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $perPage = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * find PointWithdraw by id
     *
     * @param $id
     * @param array $columns
     *
     * @return object|null
     */
    public function findById($id, array $columns = ['*']);

    /**
     * get PointWithdraw dropdown list
     *
     * @return object|null
     */
    public function dropdownList();

    /**
     * create new PointWithdraw
     *
     * @param array $data
     *
     * @return object|null
     */
    public function store(array $data);

    /**
     * approve PointWithdraw by id
     *
     * @param $id
     *
     * @return object|null
     */
    public function approveById($id);

    /**
     * close PointWithdraw by id
     *
     * @param $id
     *
     * @return object|null
     */

    public function closeById($id);

    /**
     * cancel PointWithdraw by id
     *
     * @param $id
     *
     * @return object|null
     */

    public function cancelById($id);
}
