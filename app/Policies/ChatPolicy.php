<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\Admin;
class ChatPolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.chat.index');
    }
    public function show(?Admin $admin)
    {
        return $admin->hasAnyPermission(['backend.chat.show', 'backend.chat.update']);
    }

    public function store(?Admin $admin)
    {
        return $admin->can('backend.chat.store');
    }
    public function update(?Admin $admin)
    {
        return $admin->can('backend.chat.update');
    }

    public function destroy(?Admin $admin)
    {
        return $admin->can('backend.chat.destroy');
    }

}
