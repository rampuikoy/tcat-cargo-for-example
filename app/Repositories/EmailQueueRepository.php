<?php

namespace App\Repositories;

use App\Interfaces\EmailQueueInterface;
use App\Jobs\JobSendEmail;
use App\Models\EmailQueue;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Mail;

class EmailQueueRepository implements EmailQueueInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const MINUTE_CACHE = 1;

    public function __construct(EmailQueue $emailQueue)
    {
        $this->emailQueue = $emailQueue;
    }

    public function all(array $columns = ['*'], $sorting = EmailQueueInterface::DEFAULT_SORTING, $ascending = EmailQueueInterface::DEFAULT_ASCENDING)
    {
        $key = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time = $this->getTime(self::MINUTE_CACHE);

        $emailQueues = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
            return $this->emailQueue::select($columns)->orderBy($sorting, $ascending)->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.email_queue')]), $emailQueues);
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = EmailQueueInterface::DEFAULT_LIMIT, $sorting = EmailQueueInterface::DEFAULT_SORTING, $ascending = EmailQueueInterface::DEFAULT_ASCENDING)
    {
        $key = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time = $this->getTime(self::MINUTE_CACHE);

        $emailQueues = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
            return $this->emailQueue::select($columns)
                ->with('mailable')
                ->search($conditions)
                ->orderBy($sorting, $ascending)
                ->skip($offset)
                ->limit($limit)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.email_queue')]), $emailQueues);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = EmailQueueInterface::DEFAULT_LIMIT, $sorting = EmailQueueInterface::DEFAULT_SORTING, $ascending = EmailQueueInterface::DEFAULT_ASCENDING)
    {
        $key = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time = $this->getTime(self::MINUTE_CACHE);

        $emailQueues = Cache::tags([

            $this->getClassPrefix(),
            $this->getPaginationWithClassPrefix(),

        ])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
            $model = $this->emailQueue::select($columns)
                ->with('mailable')
                ->search($conditions)
                ->orderBy($sorting, $ascending);

            if ($page == 1) {
                return $model->paginate($limit);
            } else {
                return $model->skip($limit * ($page - 1))->paginate($limit);
            }
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.email_queue')]), $emailQueues);
    }

    public function findById($id)
    {
        $emailQueue = $this->findByIdMethod($id);

        if (!$emailQueue) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.email_queue'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.email_queue')]), $emailQueue);
    }

    public function dropdownList()
    {
        $key        = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $dropdown = Cache::remember($key, $time, function () {
            return [
                'status' => $this->emailQueue::statusList(),
                'type' => $this->emailQueue::$typeList,
            ];
        });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function updateManyQueue(array $ids)
    {
        $results = [];
        foreach ($ids as $id) {

            $emailQueue = $this->findByIdMethod($id);
            $key = $this->getCacheKey(null, $id);
            $time = $this->getTime(self::MINUTE_CACHE);
            if ($emailQueue->update(['status' => 'queue'])) {

                array_push($results, $emailQueue);
                Cache::put($key, $emailQueue, $time);
                JobSendEmail::dispatch($emailQueue->id)->onQueue(env('QUEUE_EMAIL_PREFIX'));
            }
        }

        if (count($results) === 0) {
            return $this->coreResponse(500, __('messages.not_update_queue', ['data' => __('messages.email')]));
        }

        $this->clearCacheTags();
        return $this->coreResponse(200, __('messages.update_queue', ['data' => __('messages.email')]), $results);
    }

    public function resend($id)
    {

        $emailQueue = $this->findByIdMethod($id);

        if (!$emailQueue) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.email_queue'), 'id' => $id]));
        }

        if ($emailQueue->status === 'queue') {
            Mail::to($emailQueue->to)->send(new $emailQueue->mailable_class($emailQueue->mailable));
        }

        $emailQueue->update(['status' => 'success']);
        Cache::tags([$this->getIdWithClassPrefix($emailQueue->id)])->flush();

        return $this->coreResponse(200, __('messages.update_queue', ['data' => __('messages.email')]), $emailQueue);
    }


    private function findByIdMethod($id, array $columns = ['*'])
    {
        $key        = $this->getCacheKey(null, $id);
        $time       = $this->getTime(self::MINUTE_CACHE);

        $emailQueue    = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($id)
        ])->remember($key, $time, function () use ($id, $columns) {
            return $this->emailQueue::with('mailable')->find($id);
        });

        return $emailQueue;
    }

    private function clearCacheTags()
    {
        Cache::tags([
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix()
        ])->flush();
    }
}
