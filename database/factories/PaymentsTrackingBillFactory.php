<?php

namespace Database\Factories;

use App\Models\PaymentsTrackingBill;
use Illuminate\Database\Eloquent\Factories\Factory;

class PaymentsTrackingBillFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PaymentsTrackingBill::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
