<?php

namespace App\Observers;

use App\Models\Tracking;
use App\Observers\BaseObserver;
use Carbon\Carbon;

class TrackingObserver extends BaseObserver
{
    /**
     * Handle the tracking "creating" event.
     *
     * @param  \App\Models\Tracking  $tracking
     * @return void
     */
    public function creating(Tracking $tracking)
    {
        //
        $this->stampCreated($tracking);

        // set default = car
        if (!$tracking->shipping_method_id) {
            $tracking->shipping_method_id = 1;
        }

        // set default = ganeral
        if (!$tracking->product_type_id) {
            $tracking->product_type_id = 1;
        }

        // set default = weight
        if (!$tracking->calculate_type) {
            $tracking->calculate_type = 'weight';
        }

        if (!$tracking->status) {
            if (!$tracking->thai_in) {
                $tracking->status = 1;
                $tracking->warehouse_id = 1;
            } else {
                $tracking->status = 2;
                $tracking->warehouse_id = 2;
            }
        }
    }

    /**
     * Handle the tracking "updating" event.
     *
     * @param  \App\Models\Tracking  $tracking
     * @return void
     */
    public function updating(Tracking $tracking)
    {
        //
        $this->stampUpdated($tracking);
        // if(!$data->duplicate_check) {
        //     if(($data->status == 2 || $data->status == 3) && $data->duplicate_check ==0) {
        //         // check tmall duplicate
        //         $tmp = \App\Helpers\Tracking::checkTmallDuplicate($data);
        //         $data->system_remark = $tmp->system_remark;
        //         $data->duplicate_order = $tmp->duplicate_order;
        //         $tmp = \App\Helpers\Tracking::checkTaobaoDuplicate($data);
        //         $data->system_remark = $tmp->system_remark;
        //         $data->duplicate_order = $tmp->duplicate_order;

        //         $data->duplicate_check = 1;
        //     }
        // }
    }

    public function deleting(Tracking $tracking)
    {
        $this->stampUpdated($tracking);
    }

    public function saving(Tracking $tracking)
    {
        $this->stampUpdated($tracking);
    }
}
