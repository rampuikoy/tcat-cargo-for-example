<?php

namespace App\Http\Requests\Product;

use App\Http\Requests\FormRequest;
use App\Rules\Base64Image;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.product.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $id     = $this->route('product');
        $rules  = [
                    'title'            => 'required|string',
                    'code'             => 'required|unique:products,code,'.$id,
                    'status'           => 'required|in:active,inactive',

                    'point'            => 'required|numeric',
                    'stock'            => 'required|numeric',
                    'limit'            => 'required|numeric',

                    'description'      => 'sometimes|nullable|string',
                    'remark'           => 'sometimes|nullable|string',
                    'image'            => ['sometimes', 'nullable', new Base64Image],
                ];
        return $rules;
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'title'            => __('form.product.title'),
            'code'             => __('form.product.code'),
            'status'           => __('form.product.status'),

            'point'            => __('form.product.point'),
            'stock'            => __('form.product.stock'),
            'limit'            => __('form.product.limit'),

            'description'      => __('form.product.description'),
            'remark'           => __('form.product.remark'),
            'image'            => __('form.product.image'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
