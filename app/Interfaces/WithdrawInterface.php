<?php

namespace App\Interfaces;

interface WithdrawInterface
{
    const DEFAULT_LIMIT = 25;
    const DEFAULT_SORTING = 'id';
    const DEFAULT_ASCENDING = 'desc';

    /**
     * fetch all withdraw
     *
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function all(array $columns = ['*'], $sorting = WithdrawInterface::DEFAULT_SORTING, $ascending = WithdrawInterface::DEFAULT_ASCENDING);

    /**
     * list withdraw
     *
     * @param array $conditions
     * @param array $columns
     * @param int $offset
     * @param int $limit
     *
     * @return \Illuminate\Support\Collection
     */
    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * pagination withdraw
     *
     * @param array $conditions
     * @param array $columns
     * @param int $page
     * @param int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $perPage = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * find withdraw by id
     *
     * @param $id
     * @param array $columns
     *
     * @return object|null
     */
    public function findById($id, array $columns = ['*']);

      /**
     * find withdraw by user code
     *
     * @param $user code
     * @param array $columns
     *
     * @return object|null
     */
    public function findByUserCode($userCode, array $columns = ['*']);

    /**
     * get withdraw dropdown list
     *
     * @return dropdown
     */
    
    public function dropdownList();

    /**
     * create new withdraw
     *
     * @param array $data
     *
     * @return object|null
     */
    public function store(array $data);

    /**
     * update withdraw by id
     *
     * @param $id
     * @param array $data
     *
     * @return object
     */
    public function updateById($id, array $data);

    /**
     * approve withdraw by id
     *
     * @param $id
     * @param array $data
     *
     * @return object
     */
    public function approveById($id, array $data);

        /**
     * cancel withdraw by id
     *
     * @param $id
     * @param array $data
     *
     * @return object
     */
    public function cancelById($id, array $data);


	/**
     * soft delete withdraw by id
     *
     * @param $id
     *
     * @return object|null
     */
    public function softDeleteById($id);

}
