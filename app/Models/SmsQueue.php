<?php

namespace App\Models;

use App\Helpers\ScopeQueries;
use Illuminate\Database\Eloquent\Model;

class SmsQueue extends Model
{
    //
    protected $fillable = [
        'send',
        'send_at',
        'msisdn',
        'status',
        'detail',
        'transaction',
        'message',
        'transaction',
        'used_credit',
        'remain_credit',
        'created_admin',
        'user_code',
        'smsable_id',
        'smsable_type',
    ];

    protected $appends = ['status_text', 'send_text'];

    public static function statusList()
    {
        $array = [
            'queue' => __('messages.queue'),
            'success' => __('messages.sended'),
        ];
        return $array;
    }

    public static function sendList()
    {
        $array = [
            0 => __('messages.failed'),
            1 => __('messages.success'),
        ];
        return $array;
    }

    public function getStatusTextAttribute()
    {
        return collect(self::statusList())->get($this->status);
    }

    public function getSendTextAttribute()
    {
        if ($this->send_at) {
            return collect(self::sendList())->get($this->send);
        } else {
            return null;
        }
    }

    public function setUsedCreditAttribute($credit)
    {
        $this->attributes['used_credit'] = $credit ? $credit : null;
    }

    public function setRemainCreditAttribute($credit)
    {
        $this->attributes['remain_credit'] = $credit ? $credit : null;
    }

    /**
     * relation
     */
    public function smsable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_code', 'code');
    }

    // scope query
    public function scopeSearch($query, $conditions)
    {
        return $query->where(function ($query) use ($conditions) {
            if (@$conditions['id']) {
                $query->where('id', $conditions['id']);
            }
            if (@$conditions['user_code']) {
                $query->where('user_code', 'LIKE', "%{$conditions['user_code']}%");
            }
            if (@$conditions['status']) {
                $query->where('status', $conditions['status']);
            }
            if (@$conditions['send']) {
                $query->where('send', $conditions['send']);
            }
            if (@$conditions['msisdn']) {
                $query->where('msisdn', 'LIKE', "%{$conditions['msisdn']}%");
            }
            if (@$conditions['message']) {
                $query->where('message', 'LIKE', "%{$conditions['message']}%");
            }
            $date_type = (@$conditions['date_type']) ? $conditions['date_type'] : 'created_at';
            if (@$conditions['date_start'] && @$conditions['date_end']) {
                ScopeQueries::scopeDateRange($query, $date_type, $conditions['date_start'], $conditions['date_end']);
            } else if (@$conditions['date_start']) {
                ScopeQueries::scopeDateStart($query, 'created_at', $conditions['date_start']);
            } else if (@$conditions['date_end']) {
                ScopeQueries::scopeDateEnd($query, 'created_at', $conditions['date_end']);
            }
        });
    }
}
