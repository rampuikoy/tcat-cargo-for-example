<?php

namespace Database\Seeders;

use App\Models\PointWithdraw;
use Illuminate\Database\Seeder;

class PointWithdrawTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        PointWithdraw::factory(1)->create([
            'user_code'     => 'SF0002',
            'type'          => 'topup',
            'status'        => 3,
            'amount'        => 10000,
        ])
        ->each(function ($point_withdraw) {
            $point_withdraw->point()->create([
                'user_code' => $point_withdraw->user_code,
                'type'      => $point_withdraw->type,
                'amount'    => $point_withdraw->amount,
            ]);
        });
        
        PointWithdraw::factory(10)->create(['user_code' => 'SF0002'])->each(function ($point_withdraw) {
            $point_withdraw->point()->create([
                'user_code' => $point_withdraw->user_code,
                'type'      => $point_withdraw->type,
                'amount'    => $point_withdraw->amount,
            ]);
        });
    }
}
