<?php

namespace App\Observers;

use App\Models\Permission;
use App\Models\Role;
use App\Observers\BaseObserver;

class PermissionObserver extends BaseObserver
{
    /**
     * Handle the permission "created" event.
     *
     * @param  \App\Permission  $permission
     * @return void
     */
    public function created(Permission $permission)
    {
        //Admin get all permission
        $role = Role::superAdmin()->first();
        $role->syncPermissions(Permission::where('guard_name', $permission->guard_name)->get());
    }

}
