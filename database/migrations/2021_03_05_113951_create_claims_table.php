<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClaimsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claims', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('user_code')->nullable();
            $table->enum('status', ['waiting', 'cancel', 'approved'])->default('waiting');
            $table->enum('shipping', ['car', 'ship'])->default('car');
            $table->string('city')->nullable();
            $table->date('china_in')->nullable();
            $table->integer('china_bill')->unsigned()->default(0);
            $table->integer('shop_bill')->unsigned()->default(0);
            $table->integer('shipping_bill')->unsigned()->default(0);
            $table->integer('images')->unsigned()->default(0);
            $table->text('detail')->nullable();
            $table->text('remark')->nullable();
            $table->enum('type', ['shiping_price', 'coupon', 'product_price'])->default('coupon');
            $table->double('total', 8, 2);
            $table->double('sub_total', 8, 2);
            $table->string('created_admin')->nullable()->comment('คนออกบิล');
            $table->string('updated_admin')->nullable();
            $table->string('approved_admin')->nullable()->comment('คนกดอนุมัติ');
            $table->string('claimed_admin')->nullable()->comment('คนให้อนุมัติ');
            $table->string('contacted_admin')->nullable()->comment('คนรับเรื่อง');
            $table->integer('created_admin_id')->nullable();
            $table->integer('updated_admin_id')->nullable();
            $table->integer('approved_admin_id')->nullable();
            $table->integer('claimed_admin_id')->nullable();
            $table->datetime('approved_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claims');
    }
}
