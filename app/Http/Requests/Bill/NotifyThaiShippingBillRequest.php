<?php

namespace App\Http\Requests\Bill;

use App\Http\Requests\FormRequest;

class NotifyThaiShippingBillRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.bill.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'thai_shipping_method_id'               => 'required',
            'thai_shipping_date'                    => 'required|date_format:Y-m-d',
            'thai_shipping_time'                    => 'required|date_format:H:i',
            'thai_shipping_raw_price'               => 'required|numeric',
            'thai_shipping_raw_charge'              => 'required',
            'addon_refund_title'                    => 'required_with:addon_refund_credit',
            'addon_refund_credit'                   => 'required_with:addon_refund_title',
        ];
    }
    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'thai_shipping_method_id'                   => __('form.bill_update.thai_shipping_method_id'),
            'thai_shipping_date'                        => __('form.bill_update.thai_shipping_date'),
            'thai_shipping_time'                        => __('form.bill_update.thai_shipping_time'),
            'thai_shipping_raw_price'                   => __('form.bill_update.thai_shipping_cost'),
            'thai_shipping_raw_charge'                  => __('form.bill_update.thai_shipping_raw_charge'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'addon_refund_title.required_with'   => __('form.bill_update.addon_refund_title'),
            'addon_refund_credit.required_with'   => __('form.bill_update.addon_refund_credit'),
        ];
    }
}
