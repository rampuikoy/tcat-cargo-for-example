<?php

namespace App\Observers;

use App\Models\TrackingDetail;
use App\Observers\BaseObserver;

class TrackingDetailObserver extends BaseObserver
{
    /**
     * Handle the TrackingDetail "creating" event.
     *
     * @param  \App\Models\TrackingDetail  $truck
     * @return void
     */
    public function creating(TrackingDetail $TrackingDetail)
    {
        //
        $this->stampCreated($TrackingDetail);
    }

    /**
     * Handle the truck "updating" event.
     *
   * @param  \App\Models\Truck  $truck
     * @return void
     */
    public function updating(TrackingDetail $TrackingDetail)
    {
        //
        $this->stampUpdated($TrackingDetail);
    }



}
