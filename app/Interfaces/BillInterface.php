<?php

namespace App\Interfaces;

interface BillInterface
{
    const DEFAULT_LIMIT = 25;
    const DEFAULT_SORTING = 'id';
    const DEFAULT_ASCENDING = 'desc';

    /**
     * fetch all bill
     *
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function all(array $columns = ['*'], $sorting = BillInterface::DEFAULT_SORTING, $ascending = BillInterface::DEFAULT_ASCENDING);

    /**
     * list bill
     *
     * @param array $conditions
     * @param array $columns
     * @param int $offset
     * @param int $limit
     *
     * @return \Illuminate\Support\Collection
     */
    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * pagination bill
     *
     * @param array $conditions
     * @param array $columns
     * @param int $page
     * @param int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $perPage = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * find bill by id
     *
     * @param $id
     * @param array $columns
     *
     * @return object|null
     */
    public function findById($id, array $columns = ['*']);

    /**
     * get bill dropdown list
     *
     * @return dropdown
     */
    public function dropdownList();

    /**
     * create new bill
     *
     * @param array $data
     * @param object $exchange_rate
     * @param object $rate
     * @param object $thai_shipping
     * @param object|null $user_address
     *
     * @return object|null
     */
    public function store(array $data, object $exchange_rate, object $rate, object $thai_shipping, object $user_address);

    /**
     * update bill by id
     *
     * @param $id
     * @param array $data
     *
     * @return object
     */
    public function updateById($id, array $data);

    /**
     * update bill by id
     *
     * @param $id
     * @param array $data
     *
     * @return object
     */
    public function updateStepCreate(array $data);

    /**
     * get bill by user_code
     *
     * @param $user_code
     * @param array $columns
     *
     * @return object|null
     */
    public function optionCreate($code);

    /**
     * get bill by id
     *
     * @param $id
     * @param array $columns
     *
     * @return object|null
     */
    public function packProduct($id, array $data);

    /**
     * get bill by id
     *
     * @param $id
     * @param array $columns
     *
     * @return object|null
     */
    public function notifyThaiShipping($id, array $data);

    /**
     * soft delete bill by id
     *
     * @param $id
     *
     * @return object|null
     */
    public function optionEdit($id);

    /**
     * soft delete bill by id
     *
     * @param $id
     *
     * @return object|null
     */
    public function softDeleteById($id);

    /**
     * update driver to bill tracking
     *
     * @param $id
     * @param array $data
     *
     * @return object|null
     */
    public function postDriver($id, array $data);

    /**
     * get bill by id
     *
     * @param $id
     *
     * @return object|null
     */
    public function postBackward($id);

    /**
     * Cancel bill by id
     *
     * @param $id
     * @param array $data
     *
     * @return object|null
     */
    public function postCancel($id, array $data);

    /**
     * get bill by id
     *
     * @param $id
     *
     * @return object|null
     */
    public function notifyShipped($id);

    /**
     * get bill by id
     *
     * @param $id
     *
     * @return object|null
     */
    public function closeBill($id);

    /**
     * get bill by id
     *
     * @param $id
     *
     * @return object|null
     */
    public function notifyCreate($id, array $data);

    /**
     * fetch bill comission by condition
     *
     * @param array $conditions
     * @param boolean $custom_rate
     *
     * @return object|null
     */
    public function allCommissionByCondition(array $conditions, $custom_rate = false);

    /**
     * paginate bill commission
     *
     * @param array $conditions
     * @param array $columns
     * @param int $page
     * @param int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateListCommissionByFields(array $conditions, array $columns = ['*'], $page = 1, $perPage = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * update bill signature
     *
     * @param $id
     * @param array $data
     *
     * @return object|null
     */
    public function signature($id, array $data);

    /**
     * fetchListByIds bill
     *
     * @param $ids
     *
     * @return object|null
     */
    public function fetchListByIds(array $ids);

    /**
     * get bill by id
     *
     * @param $id
     *
     * @return object|null
     */
    public function postDroppointOut($id, array $data);

    /**
     * get bill by id
     *
     * @param $id
     *
     * @return object|null
     */
    public function updateDroppointOut($id, array $data);

    /**
     * get bill by id
     *
     * @param $id
     *
     * @return object|null
     */
    public function postDroppointIn($id, array $data);

    /**
     * get bill by id
     *
     * @param $id
     *
     * @return object|null
     */
    public function updateDroppointIn(array $data);

    /**
     * get bill by user_code
     *
     * @param $user_code
     * @param array $columns
     *
     * @return object|null
     */
    public function findByUserCode($code);
}
