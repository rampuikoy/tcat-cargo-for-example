@php
$total = $bill->shipping_price +
$bill->thai_shipping_method_price +
$bill->thai_shipping_method_charge;
@endphp

<table aria-describedby="bill-point-order">
	<tbody>
		<tr>
			<th scope="col"></th>
		</tr>
		<tr>
			<td class="text-right">
				<div>@lang('admin.thai_shipping_method_price')(บาท)&nbsp;:</div>
			</td>
			<td class="text-center bordered">
				<div>{{number_format($bill->shipping_cost,2)}}</div>
			</td>
		</tr>
		<tr style="font-weight: bold">
			<td class="text-right">
				<div>@lang('admin.sub_total')&nbsp;:</div>
			</td>
			<td class="text-center bordered">
				<div>{{number_format($bill->sub_total,2)}}</div>
			</td>
		</tr>
	</tbody>
	<tfoot>
		<tr style="font-weight: bold;">
			<td class="text-right">
				<div>@lang('admin.bill_total')&nbsp;:</div>
			</td>
			<td class="text-center" style="border-bottom: 1px solid;">
				<div style="font-size: 20px;">{{number_format($bill->total_price,2)}}</div>
			</td>
		</tr>
	</tfoot>
</table>