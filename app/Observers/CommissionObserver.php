<?php

namespace App\Observers;

use App\Models\Commission;

class CommissionObserver extends BaseObserver
{
    /**
     * Handle the admin "commission" event.
     *
     * @param  App\Models\Commission $commission
     * @return void
     */
    public function creating(Commission $commission)
    {
        //
        $this->stampCreated($commission);
    }

    /**
     * Handle the commission "updating" event.
     *
     * @param  App\Models\Commission $commission
     * @return void
     */
    public function updating(Commission $commission)
    {
        //
        $this->stampUpdated($commission);
    }
}
