<!DOCTYPE html>
<html lang="th">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>{{$bill->no}}</title>
	{!! Html::style('public/css/pdf.css') !!}
	<link rel="stylesheet" href="https://res.imemo.co/tcatcargo_alpha/misc/css/pdf.css" media="all" />
	<style>
		div {
			line-height: 12px;
		}

		html {
			margin: 30px 20px;
		}
	</style>
</head>

<body>
	<div>

		<table aria-describedby="claim">
			@include('admin.bill.download.partials.bill-header')
			<tr>
				<th scope="col"></th>
			</tr>
			<tr>
				<td colspan="2" class="text-center">
					<div class="page-header">ใบเคลมสินค้า</div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<table class="user" aria-describedby="user">
						<tr>
							<th scope="col"></th>
						</tr>
						<tbody>
							<tr>
								<td class="text-right" width="20%">
									<div>@lang('admin.user_code')&nbsp;: </div>
								</td>
								<td class="" width="30%">
									<div style="font-weight: bold;font-size: 20px;">{{$bill->user_code}}</div>
								</td>
								<td class="text-right" width="20%">
									<div>@lang('admin.bill_no')&nbsp;: </div>
								</td>
								<td class="">
									<div style="font-weight: bold;font-size: 20px;">{{$bill->no}}</div>
								</td>
							</tr>
							<tr>
								<td class="text-right">
									<div>@lang('admin.fullname')&nbsp;: </div>
								</td>
								<td class="">
									<div>{!!str_replace(' ', '&nbsp;',$bill->user->name)!!}</div>
								</td>
								<td class="text-right">
									<div>@lang('admin.bill_created')&nbsp;: </div>
								</td>
								<td class="">
									<div>{{$bill->created_at->format('Y-m-d')}}</div>
								</td>
							</tr>
							<tr>
								<td class="text-right">
									<div>@lang('admin.email')&nbsp;: </div>
								</td>
								<td class="">
									<div>{{$bill->user->email}}</div>
								</td>
								<td class="text-right">
									<div>@lang('admin.created_admin')&nbsp;: </div>
								</td>
								<td class="">
									<div>{{$bill->created_admin}}</div>
								</td>
							</tr>
							<tr>
								<td class="text-right">
									<div>@lang('admin.tel1')&nbsp;: </div>
								</td>
								<td class="">
									<div>{{$bill->user->tel1}}</div>
								</td>
								<td class="text-right">
									<div>@lang('admin.status')&nbsp;: </div>
								</td>
								<td class="">
									<div>{!!str_replace(' ', '&nbsp;',$bill->current_status)!!}</div>
								</td>
							</tr>
							<tr>
								<td class="text-right">
									<div>@lang('admin.city')&nbsp;: </div>
								</td>
								<td class="">
									<div>{{$bill->city}}</div>
								</td>
								<td class="text-right">
									<div>@lang('admin.contact_admin')&nbsp;: </div>
								</td>
								<td class="">
									<div>{!!str_replace(' ', '&nbsp;',$bill->contacted_admin)!!}</div>
								</td>
							</tr>
							<tr>
								<td class="text-right">
									<div>@lang('admin.shipping_method')&nbsp;: </div>
								</td>
								<td class="">
									<div>{{$bill->current_shipping}}</div>
								</td>
								<td class="text-right">
									<div>@lang('admin.china_in')&nbsp;: </div>
								</td>
								<td class="">
									<div>{!!str_replace(' ', '&nbsp;',$bill->china_in)!!}</div>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<?php $items = $bill->items; ?>
					<table class="bordered" aria-describedby="bordered" style="margin-top: 10px;">
						<thead style="background: #dfdfdf;">
							<tr>
								<th scope="col" colspan="6" class="text-center">
									<div>รายการเคลม</div>
								</th>
							</tr>
							<tr>
								<th scope="col" class="text-right" width="20">
									<div>#</div>
								</th>
								<th scope="col" width="70">
									<div>@lang('admin.type')</div>
								</th>
								<th scope="col" width="100">
									<div>@lang('admin.tracking')</div>
								</th>
								<th scope="col">
									<div>@lang('admin.remark')
								</th>
								<th scope="col" class="text-right" width="30">
									<div>@lang('admin.amount')</div>
								</th>
								<th scope="col" class="text-right" width="50">
									<div>@lang('admin.price')</div>
								</th>
							</tr>
						</thead>
						<tbody>
							@foreach($items as $key => $item)
							<tr>
								<td class="text-right">
									<div>{{$key+1}}</div>
								</td>
								<td class="text-center">
									<div>{{$item->current_type}}</div>
								</td>
								<td class="text-left">
									<div>{{$item->tracking}}</div>
								</td>
								<td>
									<div>{{str_replace(' ', '&nbsp;',$item->remark)}}</div>
								</td>
								<td class="text-right">
									<div>{{$item->amount}}</div>
								</td>
								<td class="text-right">
									<div>{{number_format($item->price,2)}}</div>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td>

				</td>
				<td style="padding-top: 15px; padding-bottom: 20px;">
					<table aria-describedby="list">
						<tr>
							<th scope="col"></th>
						</tr>
						<tbody>
							@if($bill->withholding>0)
							<tr>
								<td class="text-right">
									<div>@lang('admin.withholding')</div>
								</td>
								<td class="text-center" style="border-bottom: 1px solid;">
									<div>{{number_format($bill->withholding,2)}}</div>
								</td>
							</tr>
							@endif
							<tr class="text-bold">
								<td class="text-right">
									<div>@lang('admin.sub_total')</div>
								</td>
								<td class="text-center" style="border-bottom: 1px solid;">
									<div style="font-size: 20px;">{{number_format($bill->total,2)}}</div>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<table aria-describedby="detail" style="margin-bottom: 10px;">
						<thead class="thai-shipping" style="background: #ddd;">
							<tr>
								<th scope="col" colspan="2" class="bordered">
									<div>@lang('admin.detail')</div>
								</th>
							</tr>
						</thead>
						<tbody class="bordered">
							<tr>
								<td class="text-right" width="20%">
									<div>@lang('admin.detail')&nbsp;:</div>
								</td>
								<td>
									<?php $detail = str_replace(' ', '&nbsp;', $bill->detail) ?>
									<div>{!! ($bill->detail)? nl2br($detail) : '-' !!}</div>
								</td>
							</tr>
							<tr>
								<td class="text-right">
									<div>@lang('admin.type')&nbsp;:</div>
								</td>
								<td class="">
									<div>{{$bill->current_type}}</div>
								</td>
							</tr>
							<tr>
								<td class="text-right">
									<div>@lang('admin.remark')&nbsp;:</div>
								</td>
								<td class="">
									<div>{{str_replace(' ', '&nbsp;', $bill->remark)}}</div>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>

			<tr>
				<td colspan="2" style="padding: 5px 5px 5px 5px;">
					@include('admin.bill.download.partials.bill-contact')
				</td>
			</tr>
			<tr>
				<td class="text-center" style="padding: 10px;">
					{!!'<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($bill->user_code, 'C128',2,30) . '" alt="barcode" />'!!} <br>
					<small>@lang('admin.user_code'):&nbsp;{{$bill->user_code}}</small>
				</td>
				<td class="text-center" style="padding: 10px;">
					{!!'<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($bill->no, 'C128',2,30) . '" alt="barcode" />'!!} <br>
					<small>@lang('admin.bill_no'):&nbsp;{{$bill->no}}</small>
				</td>
			</tr>
		</table>
	</div>
</body>

</html>