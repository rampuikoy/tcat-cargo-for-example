<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Interfaces\TrackingInterface;
use App\Interfaces\UploadInterface;
use App\Http\Requests\NoCodeByTrackId\UpdateNoCodeByTrackIdRequest;
use App\Http\Requests\NoCodeByTrackId\UploadNoCodeByTrackIdRequest;

class NoCodeByTrackIdController extends Controller
{
    protected $trackingInterface,$uploadInterface;

    public function __construct(TrackingInterface $trackingInterface, UploadInterface $uploadInterface)
    {
        $this->trackingInterface = $trackingInterface;
        $this->uploadInterface   = $uploadInterface;
    }

    public function update(UpdateNoCodeByTrackIdRequest $request, $id)
    {
        $data = $request->only([
            'type',
            'detail',
        ]);
        $tracking = $this->trackingInterface->updateById($id, $data);
        return response()->json($tracking, $tracking->code);
    }

    public function uploadImage(UploadNoCodeByTrackIdRequest $request, $id)
    {
        $data = [
            'type'              => 'image',
            'relatable_type'    => 'App\Models\Tracking',
            'relatable_id'      => $id,
            'file_path'         => $request->file_path,
        ];
        $upload    = $this->uploadInterface->store($data);
        return response()->json($upload, $upload->code);
    }

    public function destroyUploadImage($id)
    {
        $upload = $this->uploadInterface->delete($id);
        return response()->json($upload, $upload->code);
    }
}
