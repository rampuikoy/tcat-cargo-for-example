<?php

namespace App\Http\Requests\ThaiShippingAreaPrice;

use App\Http\Requests\FormRequest;

class StoreAreaPriceMethodRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.thai-shipping-area-price.store');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'thai_shipping_method_id'   => 'required|exists:thai_shipping_methods,id',
            'province_id'               => 'required|exists:provinces,id',
            'price_a'                   => 'required|numeric|min:0',
            'price_b'                   => 'required|numeric|min:0',
            'price_c'                   => 'required|numeric|min:0',
            'price_d'                   => 'required|numeric|min:0',
            'price_e'                   => 'required|numeric|min:0',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'thai_shipping_method_id'   => __('form.thai_shipping_area_price.thai_shipping_method_id'),
            'province_id'               => __('form.thai_shipping_area_price.province_id'),
            'price_a'                   => __('form.thai_shipping_area_price.price_a'),
            'price_b'                   => __('form.thai_shipping_area_price.price_b'),
            'price_c'                   => __('form.thai_shipping_area_price.price_c'),
            'price_d'                   => __('form.thai_shipping_area_price.price_d'),
            'price_e'                   => __('form.thai_shipping_area_price.price_e'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
