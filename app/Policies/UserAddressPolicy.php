<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserAddressPolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.user-address.index');
    }

    public function show(Admin $admin)
    {
        return $admin->hasAnyPermission(['backend.user-address.show', 'backend.user-address.update']);
    }

    public function store(Admin $admin)
    {
        return $admin->can('backend.user-address.store');
    }

    public function update(Admin $admin)
    {
        return $admin->can('backend.user-address.update');
    }

    public function destroy(Admin $admin)
    {
        return $admin->can('backend.user-address.destroy');
    }
}
