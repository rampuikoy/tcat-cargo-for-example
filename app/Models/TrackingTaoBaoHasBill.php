<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrackingTaoBaoHasBill extends Model
{
    use HasFactory;
    protected $connection = 'taobaoconew';
    protected $table = 'tracking_has_bills';
    protected $primaryKey = 'tracking_id';
    protected $fillable = [
        'duplicate',
    ];
    public $timestamps = false;

    public function scopeSearch($query, $code)
    {
        return $query->where('tracking_code', $code);
    }
    public function scopeCode($query, $code)
    {
        return $query->where('tracking_code', $code)->first();
    }
}
