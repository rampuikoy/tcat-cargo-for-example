<?php

namespace App\Repositories;

use App\Interfaces\ZipcodeInterface;
use App\Models\Zipcode;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use Illuminate\Support\Facades\Cache;

class ZipcodeRepository implements ZipcodeInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const MINUTE_CACHE = 1;

    public function __construct(Zipcode $zipcode)
    {
        $this->zipcode = $zipcode;
    }

    public function all(array $columns = ['*'], $sorting = ZipcodeInterface::DEFAULT_SORTING, $ascending = ZipcodeInterface::DEFAULT_ASCENDING)
    {
        $key = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time = $this->getTime(self::MINUTE_CACHE);

        $allZipcode = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
            return $this->zipcode::select($columns)->orderBy($sorting, $ascending)->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.zipcode')]), $allZipcode);
    }

    public function findManyByDistrictCode($code, array $columns = ['*'])
    {
        $key = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time = $this->getTime(self::MINUTE_CACHE);

        $zipcodeList = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($code, $columns) {
            return $this->zipcode::select($columns)
                ->where('district_code', $code)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.zipcode')]), $zipcodeList);
    }

    public function findById($id, array $columns = ['*'])
    {
        $zipcode =  $this->findByIdMethod($id, $columns = ['*']);
        if (!$zipcode) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.zipcode'), 'id' => $id]));
        }
        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.zipcode')]), $zipcode);
    }

    private function findByIdMethod($id, array $columns = ['*'])
    {
        $key = $this->getCacheKey(null, $id);
        $time = $this->getTime(self::MINUTE_CACHE);

        $zipcode = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($id)
        ])->remember($key, $time, function () use ($id, $columns) {
            return $this->zipcode::select($columns)->whereId($id)->first();
        });
        return $zipcode;
    }
}
