<?php

return [
    'registered'       => 'สวัสดีค่ะ <br>TCAT-CARGO ยินดีต้อนรับ<br> ลูกค้าสามารถคุยกับเจ้าหน้าที่ของเราโดยตรง ผ่านระบบกล่องข้อความค่ะ',
    'bill_created'     => 'แจ้งบิลขนส่งใหม่ :no <br>ยอดที่ต้องชำระ :total บาท <br><a href=":url" target="_blank">ดูเพิ่มเติม</a>',
    'bill_cancel'      => 'แจ้งยกเลิกบิลขนส่ง :no <br><a href=":url" target="_blank">ดูเพิ่มเติม</a>',
    'bill_paid'        => 'บิลขนส่ง :no ชำระเงินแล้ว กำลังเตรียมจัดส่ง <br><a href=":url" target="_blank">ดูเพิ่มเติม</a>',
    'bill_shipped'     => 'บิลขนส่ง :no กำลังจัดส่ง <br>ช่องทาง :method <br><a href=":url" target="_blank">ดูเพิ่มเติม</a>',
];
