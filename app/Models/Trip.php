<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    use HasFactory;

    const PREFIX = 'CT-';
    protected $fillable = [
        'title',
        'destination',
        'customer',
        'depart_date',
        'return_date',
        'sub_total',
        'discount_title',
        'discount',
        'withholding',
        'total',
        'user_remark',
        'admin_remark',
        'detail',
        'status',
        'created_admin',
        'updated_admin',
        'user_code',
        'created_admin_id',
        'updated_admin_id',
        'bill'
    ];

    public static function statusList()
    {
        $array = [
            [
                'id' => 1,
                'name' => __('messages.trips.status.1'),
            ],
            [
                'id' => 2,
                'name' => __('messages.trips.status.2'),
            ],
            [
                'id' => 3,
                'name' => __('messages.trips.status.3'),
            ],
            [
                'id' => 4,
                'name' => __('messages.trips.status.4'),
            ],
            [
                'id' => 5,
                'name' => __('messages.trips.status.5'),
            ],
        ];
        return $array;
    }


    public static function texeBilllList()
    {
        $array = [
            'normal'   => __('messages.trips.normal'),
            'company'  => __('messages.trips.company'),
        ];
        return $array;
    }

    public static function tripType()
    {
        $array = [
            'visa'          => __('messages.trips.visa'),
            'airplane'      => __('messages.trips.airplane'),
            'hotel'         => __('messages.trips.hotel'),
            'car'           => __('messages.trips.car'),
            'giude'         => __('messages.trips.giude'),
            'group_tour'    => __('messages.trips.group_tour'),
            'private_tour'  => __('messages.trips.private_tour'),
            'other'         => __('messages.trips.other'),
        ];
        return $array;
    }

    public static function textCondition()
    {
        $array = [
            'not_cancel'          => __('messages.trips.not_cancel'),
            'user_remark'      => __('messages.trips.user_remark'),
            'admin_remark'         => __('messages.trips.admin_remark'),
        ];
        return $array;
    }

    protected $appends = ['current_status', 'current_bill', 'no', 'sign_url'];

    public function getSignUrlAttribute()
    {
        $urlFront =  env('URL_FRONTEND', null);
        return $urlFront . '/trip/' . $this->id;
    }

    public function getCurrentStatusAttribute()
    {
        $status = collect(self::statusList())->firstWhere('id', $this->status);
        return (!empty($status['name'])) ? $status['name'] : null;
    }

    public function getCurrentBillAttribute()
    {
        $bill = collect(self::texeBilllList())->get($this->bill);
        return (!empty($bill)) ? $bill : null;
    }

    public function getNoAttribute()
    {
        return ($this->id) ? self::PREFIX . str_pad($this->id, 6, "0", STR_PAD_LEFT) : null;
    }

    /**
     * relation
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_code', 'code');
    }
    public function orders()
    {
        return $this->hasMany('App\Models\TripOrder', 'trip_id', 'id');
    }
    public function emails()
    {
        return $this->morphMany('App\Models\EmailQueue', 'mailable');
    }
    public function sms()
    {
        return $this->morphMany('App\Models\SmsQueue', 'smsable');
    }
    public function credits()
    {
        return $this->morphMany('App\Models\Credit', 'creditable');
    }
    public function payments()
    {
        return $this->morphToMany('App\Models\Payment', 'payable');
    }

    public function scopeSearch($query, $conditions)
    {
        return $query->where(function ($query) use ($conditions) {
            if (@$conditions['id']) {
                $query->where('id', $conditions['id']);
            }
            if (@$conditions['user_code']) {
                $query->where('user_code', 'LIKE', "%{$conditions['user_code']}%");
            }
            if (@$conditions['destination']) {
                $query->where('destination', 'LIKE', "%{$conditions['destination']}%");
            }
            if (@$conditions['status']) {
                $query->where('status', $conditions['status']);
            }
            if (@$conditions['bill']) {
                $query->where('bill', $conditions['bill']);
            }
            if (@$conditions['bill_no']) {
                $no = ltrim($conditions['bill_no'], Trip::PREFIX . '0');
                $query->where('id', 'LIKE', "%{$no}%");
            }
            if (@$conditions['depart_date']) {
                $query->whereRaw('DATE(depart_date) >= "' . $conditions['depart_date'] . '"');
            }
            if (@$conditions['to_date']) {
                $query->whereRaw('DATE(return_date) <= "' . $conditions['to_date'] . '"');
            }
            if (@$conditions['user_remark']) {
                $query->where('user_remark', 'LIKE', "%{$conditions['user_remark']}%");
            }
            if (@$conditions['admin_remark']) {
                $query->where('admin_remark', 'LIKE', "%{$conditions['admin_remark']}%");
            }
            if (@$conditions['type']) {
                $trips = TripOrder::where('type', $conditions['type'])
                    ->distinct()->get()
                    ->pluck('trip_id');
                $query->whereIn('id', $trips);
            }
            if (@$conditions['condition']) {
                $type = TripOrder::tripType();
                $type_keys = array_keys($type);
                if (in_array($conditions['condition'], $type_keys)) {
                    $trips = TripOrder::whereType($conditions['condition'])
                        ->distinct()->get()
                        ->pluck('trip_id');
                    $query->whereIn('id', $trips);
                } else {
                    switch ($conditions['condition']) {
                        case 'not_cancel':
                            $query->where('status', '!=', '5');
                            break;
                        case 'user_remark':
                            $query->whereNotNull('user_remark');
                            break;
                        case 'admin_remark':
                            $query->whereNotNull('admin_remark');
                            break;
                    }
                }
            }
        });
    }
}
