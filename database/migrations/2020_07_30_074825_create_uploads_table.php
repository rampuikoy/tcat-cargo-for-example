<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploads', function (Blueprint $table) {
            $table->id();
            $table->enum('type', ['image', 'file']);
            $table->string('file_path', 191);
            $table->string('sub_path', 191)->nullable();
            $table->integer('created_admin_id');
            $table->string('created_admin', 64);
            $table->dateTime('created_at');
            $table->nullableMorphs('relatable');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uploads');
    }
}
