<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Coupon;
use Illuminate\Http\Request;
use App\Interfaces\CouponInterface;
use App\Interfaces\ThaiShippingMethodInterface;
use App\Http\Requests\Coupon\StoreCouponRequest;
use App\Http\Requests\Coupon\UpdateCouponRequest;
use App\Http\Requests\Coupon\CheckCouponRequest;

class CouponController extends Controller
{
    protected $couponInterface;

    public function __construct(CouponInterface $couponInterface,ThaiShippingMethodInterface $thaiShippingMethodInterface)
    {
        $this->couponInterface = $couponInterface;
        $this->thaiShippingMethodInterface = $thaiShippingMethodInterface;
    }

    public function index(Request $request)
    {
        $this->authorize('index', Coupon::class);
        $columns = $request->input('columns', ['*']);
        $coupon = $this->couponInterface->all($columns);
        return response()->json($coupon, $coupon->code);
    }

    public function search(Request $request)
    {
        $this->authorize('index', Coupon::class);
        $conditions = $request->only([
            'type',
            'title',
            'code',
            'user_code',
            'price',
            'bill_min_price',
            'bill',
            'status',
            'limit_user_code',
            'limit_shipping_method',
            'shipping_method_id',
            'date_start',
            'date_end',
            'limits',
            'remark',
            'condition',
        ]);
        $columns = $request->input('columns', ['*']);
        $limit = $request->input('limit', $this->couponInterface::DEFAULT_LIMIT);
        $sorting = $request->input('order_by', $this->couponInterface::DEFAULT_SORTING);
        $ascending = $request->input('ascending') == 1 ? 'asc' : 'desc';

        if ($request->boolean('pagination')) {
            $page = $request->input('page', 1);
            $coupon = $this->couponInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset = $request->input('page', 0);
            $coupon = $this->couponInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($coupon, $coupon->code);
    }

    public function dropdown()
    {
        $dropdown = $this->couponInterface->dropdownList();
        if (!$dropdown->error) {
            $thai_shipping_method = $this->thaiShippingMethodInterface->all();

            $dropdown->results = collect($dropdown->results)->merge([
                'thai_shipping_method' => (!$thai_shipping_method->error) ? $thai_shipping_method->results : [],
            ]);
        }
        return response()->json($dropdown, $dropdown->code);
    }

    public function show(Request $request, $id)
    {
        $this->authorize('show', Coupon::class);
        $columns = $request->input('columns', ['*']);
        $coupon = $this->couponInterface->findById($id, $columns);
        return response()->json($coupon, $coupon->code);
    }

    public function store(StoreCouponRequest $request)
    {
        $data = $request->only([
            'title',
            'code',
            'price',
            'limit',
            'start_date',
            'end_date',
            'bill_min_price',
            'type',
            'limit_user_code',
            'user_code',
            'limit_shipping_method',
            'shipping_method_id',
            'bill',
            'remark',
            'status',
        ]);
        $coupon = $this->couponInterface->store($data);
        return response()->json($coupon, $coupon->code);
    }

    public function update(UpdateCouponRequest $request, $id)
    {
        $data = $request->only([
            'title',
            'price',
            'limit',
            'start_date',
            'end_date',
            'bill_min_price',
            'type',
            'limit_user_code',
            'user_code',
            'limit_shipping_method',
            'shipping_method_id',
            'bill',
            'remark',
            'status',
        ]);
        $coupon = $this->couponInterface->updateById($id, $data);
        return response()->json($coupon, $coupon->code);
    }

    public function check(CheckCouponRequest $request, $code)
    {
        $data = $request->only([
            'bill',
            'thai_shipping_method_id',
            'user_code',
        ]);
        $coupon = $this->couponInterface->check($code, $data);
        return response()->json($coupon, $coupon->code);
    }
}
