<?php

namespace App\Http\Requests\NoCode;

use App\Http\Requests\FormRequest;

class ZoneOutNoCodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.nocode.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code'          => 'required|string',
            'taken_admin'   => 'required|string',
            'order_code'    => 'sometimes|nullable|string',
        ];
    }

    public function attributes()
    {
        return [
            'code'          => __('form.no_code.code'),
            'taken_admin'   => __('form.no_code.taken_admin'),
            'order_code'    => __('form.no_code.option.order_code'),
        ];
    }
}
