<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\ScopeQueries;

class Claims extends Model
{
    use HasFactory;

    const PREFIX = 'CL-';

    public static $imageFolder = 'uploads/claim/';
    public static $imageWidth = 300;
    public static $imageHeight = 300;

    protected $table = 'claims';

    protected $fillable = [
        'user_code',
        'status',
        'shipping',
        'city',
        'china_in',
        'china_bill',
        'shop_bill',
        'shipping_bill',
        'images',
        'detail',
        'remark',
        'type',
        'total',
        'sub_total',
        'created_admin',
        'updated_admin',
        'approved_admin',
        'claimed_admin',
        'contacted_admin',
        'created_admin_id',
        'updated_admin_id',
        'approved_admin_id',
        'claimed_admin_id',
        'approved_at',
    ];

    public static function statusList()
    {
        $array = [
            'waiting'  => __('messages.claim.status.waiting'),
            'approved' => __('messages.claim.status.approved'),
            'cancel'   => __('messages.claim.status.cancel'),
        ];
        return $array;
    }

    public static function shippingList()
    {
        $array = [
            'car'  => __('messages.claim.shipping.car'),
            'ship' => __('messages.claim.shipping.ship'),
        ];
        return $array;
    }

    public static function typeList()
    {
        $array = [
            'coupon'      => __('messages.claim.type.coupon'),
            'shiping_price' => __('messages.claim.type.shiping_price'),
            'product_price' => __('messages.claim.type.product_price'),
        ];
        return $array;
    }
    protected $appends = ['no', 'current_status', 'current_shipping', 'current_type'];

    public function getNoAttribute()
    {
        return ($this->id) ? self::PREFIX . str_pad($this->id, 6, "0", STR_PAD_LEFT) : null;
    }

    public function getCurrentStatusAttribute()
    {
        return collect(self::statusList())->get($this->status);
    }

    public function getCurrentShippingAttribute()
    {
        return collect(self::shippingList())->get($this->shipping);
    }

    public function getCurrentTypeAttribute()
    {
        return collect(self::typeList())->get($this->type);
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_code', 'code');
    }

    public function items()
    {
        return $this->hasMany('App\Models\ClaimItems', 'claim_id', 'id');
    }

    public function emails()
    {
        return $this->morphMany('App\Models\EmailQueue', 'mailable');
    }

    public function images()
    {
        return $this->morphMany('App\Models\Upload', 'relatable');
    }

    public function scopeSearch($query, $conditions)
    {
        return $query->where(function ($query) use ($conditions) {
            ScopeQueries::scopeUserCodeBySale($query);

            if (@$conditions['bill_no']) {
                $trimmed = ltrim($conditions['bill_no'], self::PREFIX . '0');
                $query->where('id', 'like', '%' . $trimmed . '%');
            }
            if (@$conditions['user_code']) {
                $query->where('user_code', 'like', '%' . $conditions['user_code'] . '%');
            }
            if (@$conditions['contacted_admin']) {
                $query->where('contacted_admin', 'like', '%' . $conditions['contacted_admin'] . '%');
            }
            if (@$conditions['city']) {
                $query->where('city', 'like', '%' . $conditions['city'] . '%');
            }
            if (@$conditions['remark']) {
                $query->where('remark', 'like', '%' . $conditions['remark'] . '%');
            }
            if (@$conditions['status']) {
                $query->where('status', $conditions['status']);
            }
            if (@$conditions['shipping']) {
                $query->where('shipping', $conditions['shipping']);
            }
            if (@$conditions['type']) {
                $query->where('type', $conditions['type']);
            }
            if (@$conditions['china_in']) {
                $date = date_create($conditions['china_in']);
                $query->whereDate('china_in', '=', date_format($date, 'Y-m-d'));
            }
        });
    }
}
