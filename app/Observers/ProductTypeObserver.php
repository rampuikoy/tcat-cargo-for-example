<?php

namespace App\Observers;

use App\Models\ProductType;
use App\Observers\BaseObserver;

class ProductTypeObserver extends BaseObserver
{
    /**
     * Handle the product_type "creating" event.
     *
     * @param  \App\Models\ProductType  $product_type
     * @return void
     */
    public function creating(ProductType $product_type)
    {
        //
        $this->stampCreated($product_type);
    }

    /**
     * Handle the product_type "updating" event.
     *
   * @param  \App\Models\ProductType  $product_type
     * @return void
     */
    public function updating(ProductType $product_type)
    {
        //
        $this->stampUpdated($product_type);
    }

}
