<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    use HasFactory;

    public static $types = [
        '1' => 'รอสินค้าเข้าไทย',
        '14' => 'รอเข้าไทย-อี้อู',
        '15' => 'รอเข้าไทย-กวางโจว',
        '2' => 'โกดังไทย',
        '3' => 'NoCode',
        '13' => 'NoCode-Shipping',
        '4' => 'Taobao',
        '5' => 'Alibaba',
        '6' => 'Tmall',
        '7' => 'Shipping Zone A',
        '8' => 'Shipping Zone B',
        '9' => 'Shipping Zone C',
        '10' => 'Shipping Zone D',
        '11' => 'ของออกไทย',
        '12' => 'Sale',
    ];

    public static function typeList()
    {
        return [
            '1' => __('messages.warehouse_th.waiting_th'),
            '14' => __('messages.warehouse_th.waiting_th_yiwu'),
            '15' => __('messages.warehouse_th.waiting_th_guangzhou'),
            '2' => __('messages.warehouse_th.warehouse_th'),
            '3' => __('messages.warehouse_th.no_code'),
            '13' => __('messages.warehouse_th.noCode_shipping'),
            '4' => __('messages.warehouse_th.taobao'),
            '5' => __('messages.warehouse_th.alibaba'),
            '6' => __('messages.warehouse_th.tmall'),
            '7' => __('messages.warehouse_th.shipping_zone_a'),
            '8' => __('messages.warehouse_th.shipping_zone_b'),
            '9' => __('messages.warehouse_th.shipping_zone_c'),
            '10' => __('messages.warehouse_th.shipping_zone_d'),
            '11' => __('messages.warehouse_th.without_th'),
            '12' => __('messages.warehouse_th.sale'),
        ];
    }

    public static function getOptions()
    {
        return self::typeList();
    }

    public static function excelToId($text)
    {
        return (string) array_search($text, self::typeList());
    }

}
