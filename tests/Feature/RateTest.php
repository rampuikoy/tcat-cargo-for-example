<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Admin;
use App\Models\Rate;
use Tymon\JWTAuth\Facades\JWTAuth;

class RateTest extends TestCase
{
    use WithFaker;
    protected $user;
    protected $rate;
    protected $url = '/api/backend/rate';
    /**
     * A basic feature test example.
     *
     * @return void
     */
    protected function authenticate(string $guard = 'admins')
    {
        $this->user = factory(Admin::class)->create();
        auth()->shouldUse($guard);
        $this->rate = Rate::create([
            "title" => "ราคาปกติ",
        "status" => "active",
        "admin_remark" => "",
        "user_code" => 'SF0003',
        "kg_car_genaral" => "49.00",
        "kg_car_iso" => "89.00",
        "kg_car_brand" => "149.00",
        "kg_ship_genaral" => "39.00",
        "kg_ship_iso" => "79.00",
        "kg_ship_brand" => "149.00",
        "kg_plane_genaral" => "300.00",
        "kg_plane_iso" => "350.00",
        "kg_plane_brand" => "350.00",
        "cubic_car_genaral" => "8900.00",
        "cubic_car_iso" => "10000.00",
        "cubic_car_brand" => "18500.00",
        "cubic_ship_genaral" => "5500.00",
        "cubic_ship_iso" => "9000.00",
        "cubic_ship_brand" => "18500.00",
        "cubic_plane_genaral" => "54800.00",
        "cubic_plane_iso" => "54800.00",
        "cubic_plane_brand" => "54800.00",
        "created_admin" => "lowe.bette",
        "updated_admin" => "Tucky",
        "created_admin_id" => "1",
        "updated_admin_id" => "51",
        "default_rate" => "0",
        "type" => "general",
        "admin_id" => "0"
        ]);
        $token = JWTAuth::fromUser($this->user);
        return 'Bearer ' . $token;
    }

    protected function deleteUser()
    {
        $this->rate->forceDelete();
        $this->user->forceDelete();
    }

    /** @test */
    public function GetRateAllSuccessTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('GET', $this->url);
        $response->assertStatus(200);
        $this->deleteUser();
    }

    /** @test */
    public function GetRateAllFailTest()
    {
        $response = $this->json('GET', $this->url);
        $response->assertStatus(401);
    }

    /** @test */
    public function GetRateSearchSuccessTest()
    {
        $id = random_int(1, 200);
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('GET', "{$this->url}/search", [
            "id" => $id,
            "type" => "",
            "search" => "",
        ]);
        $response->assertStatus(200);
        $this->deleteUser();
    }

    /** @test */
    public function GetRateSearchFailTest()
    {
        $response = $this->json('GET', "{$this->url}/search");
        $response->assertStatus(401);
    }

    /** @test */
    public function GetRateDefaultSuccessTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('GET', "{$this->url}/default");
        $response->assertStatus(200);
        $this->deleteUser();
    }

    /** @test */
    public function GetRateDefaultFailTest()
    {
        $response = $this->json('GET', "{$this->url}/default");
        $response->assertStatus(401);
    }

    /** @test */
    public function GetRateDropdownSuccessTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('GET', "{$this->url}/dropdown");
        $response->assertStatus(200);
        $this->deleteUser();
    }

    /** @test */
    public function GetRateDropdownFailTest()
    {
        $response = $this->json('GET', "{$this->url}/dropdown");
        $response->assertStatus(401);
    }

    /** @test */
    public function GetRateByIdSuccessTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('GET', "{$this->url}/{$this->rate->id}");
        $response->assertStatus(200);
        $this->deleteUser();
    }

    /** @test */
    public function GetRateUserCodeSuccessTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('GET', "{$this->url}/user/{$this->rate->user_code}");
        $response->assertStatus(200);
        $this->deleteUser();
    }

    /** @test */
    public function GetRateUserCodeFailTest()
    {
        $response = $this->json('GET', "{$this->url}/user/SF0003");
        $response->assertStatus(401);
    }

    /** @test */
    public function GetRateByIdFailTest()
    {
        $response = $this->json('GET', "{$this->url}/2");
        $response->assertStatus(401);
    }

    /** @test */
    public function RatePostSuccessTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('POST', $this->url, [
            'title' => 'test-test',
            'status' => 'active',
            'admin_remark' => 'asdaoi',
            'kg_car_genaral' => '1.1',
            'kg_car_iso' => '1.1',
            'kg_car_brand' => '1.1',
            'kg_ship_genaral' => '1.1',
            'kg_ship_iso' => '0.21',
            'kg_ship_brand' => '0.21',
            'kg_plane_genaral' => '2',
            'kg_plane_iso' => '0.21',
            'kg_plane_brand' => '2',
            'cubic_car_genaral' => '0.21',
            'cubic_car_iso' => '2',
            'cubic_car_brand' => '0.21',
            'cubic_ship_genaral' => '2',
            'cubic_ship_iso' => '0.21',
            'cubic_ship_brand' => '0.21',
            'cubic_plane_genaral' => '0.21',
            'cubic_plane_iso' => '0.21',
            'cubic_plane_brand' => '0.21',
            'default_rate' => '0',
            'type' => 'general'
        ]);
        $response->assertStatus(200);
        Rate::where('title', 'test-test')->forceDelete();
        $this->deleteUser();
    }

    /** @test */
    public function RatePostFailAuthTest()
    {
        $response = $this->json('POST', $this->url);
        $response->assertStatus(401);
    }

    /** @test */
    public function RatePostFailTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('POST', $this->url);
        $response->assertStatus(422);
        $this->deleteUser();
    }

     /** @test */
     public function RateUpdateUserCodeByIdSuccessTest()
     {
         $token = $this->authenticate();
         $response = $this->withHeaders([
             'Authorization' => $token,
         ])->json('PUT', "{$this->url}/user/{$this->rate->user_code}", [
             'title' => 'asdja;djlkasdjl',
             'status' => 'active',
             'admin_remark' => 'asdaoi',
             'kg_car_genaral' => '1.1',
             'kg_car_iso' => '1.1',
             'kg_car_brand' => '1.1',
             'kg_ship_genaral' => '1.1',
             'kg_ship_iso' => '0.21',
             'kg_ship_brand' => '0.21',
             'kg_plane_genaral' => '2',
             'kg_plane_iso' => '0.21',
             'kg_plane_brand' => '2',
             'cubic_car_genaral' => '0.21',
             'cubic_car_iso' => '2',
             'cubic_car_brand' => '0.21',
             'cubic_ship_genaral' => '2',
             'cubic_ship_iso' => '0.21',
             'cubic_ship_brand' => '0.21',
             'cubic_plane_genaral' => '0.21',
             'cubic_plane_iso' => '0.21',
             'cubic_plane_brand' => '0.21',
             'default_rate' => '0',
             'type' => 'general'
         ]);
         $response->assertStatus(200);
         Rate::where('title', 'asdja;djlkasdjl')->forceDelete();
         $this->deleteUser();
     }
 
     /** @test */
     public function RateUpdateUserCodeByIdFailAuthTest()
     {
         $response = $this->json('PUT', "{$this->url}/user/SF0003");
         $response->assertStatus(401);
     }
 
     /** @test */
     public function RateUpdateUserCodeByIdFailTest()
     {
         $token = $this->authenticate();
         $response = $this->withHeaders([
             'Authorization' =>  $token,
         ])->json('PUT', "{$this->url}/user/{$this->rate->user_code}");
         $response->assertStatus(422);
         $this->deleteUser();
     }

    /** @test */
    public function RateUpdateByIdSuccessTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('PUT', "{$this->url}/{$this->rate->id}", [
            'title' => 'asdja;djlkasdjl',
            'status' => 'active',
            'admin_remark' => 'asdaoi',
            'kg_car_genaral' => '1.1',
            'kg_car_iso' => '1.1',
            'kg_car_brand' => '1.1',
            'kg_ship_genaral' => '1.1',
            'kg_ship_iso' => '0.21',
            'kg_ship_brand' => '0.21',
            'kg_plane_genaral' => '2',
            'kg_plane_iso' => '0.21',
            'kg_plane_brand' => '2',
            'cubic_car_genaral' => '0.21',
            'cubic_car_iso' => '2',
            'cubic_car_brand' => '0.21',
            'cubic_ship_genaral' => '2',
            'cubic_ship_iso' => '0.21',
            'cubic_ship_brand' => '0.21',
            'cubic_plane_genaral' => '0.21',
            'cubic_plane_iso' => '0.21',
            'cubic_plane_brand' => '0.21',
            'default_rate' => '0',
            'type' => 'general'
        ]);
        $response->assertStatus(200);
        $this->deleteUser();
    }

    /** @test */
    public function RateUpdateByIdFailAuthTest()
    {
        $response = $this->json('PUT', "{$this->url}/2");
        $response->assertStatus(401);
    }

    /** @test */
    public function RateUpdateByIdFailTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' =>  $token,
        ])->json('PUT', "{$this->url}/{$this->rate->id}");
        $response->assertStatus(422);
        $this->deleteUser();
    }

    /** @test */
    public function RateleDeleteSuccessTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('DELETE', "{$this->url}/{$this->rate->id}");
        $response->assertStatus(200);
        $this->deleteUser();
    }

    /** @test */
    public function RateleDeleteFailTest()
    {
        $response = $this->json('DELETE', "{$this->url}/5");
        $response->assertStatus(401);
    }
}
