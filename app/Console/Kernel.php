<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        Commands\Make\MakeInterface::class,
        Commands\Make\MakeRepository::class,
        Commands\SyncRoute::class,
        Commands\AutoUserCode::class,
        Commands\TrimTracking::class,
        Commands\CheckDuplicateTracking::class,
        Commands\GenTrackingFcm::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        $schedule->command('tracking:check')->everyTenMinutes();
        $schedule->command('tracking:auto-user-code')->dailyAt('02:00');
        $schedule->command('trim:tracking')->dailyAt('04:00');
        $schedule->command('tracking:fcm')->dailyAt('04:00');

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
