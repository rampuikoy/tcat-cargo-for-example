<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class PaymentPolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.payment.index');
    }

    public function show(Admin $admin)
    {
        return $admin->hasAnyPermission(['backend.payment.show', 'backend.payment.update']);
    }

    public function store(Admin $admin)
    {
        return $admin->can('backend.payment.store');
    }

    public function update(Admin $admin)
    {
        return $admin->can('backend.payment.update');
    }

    public function destroy(Admin $admin)
    {
        return $admin->can('backend.payment.destroy');
    }

}
