<?php

namespace App\Repositories;

use App\Interfaces\CouponInterface;
use App\Models\Coupon;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;


class CouponRepository implements CouponInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const MINUTE_CACHE          = 1;
    const TABLE_RELETIONS       = ['shippingMethod', 'bills'];

    public function __construct(Coupon $coupon)
    {
        $this->coupon        = $coupon;
    }

    public function all(array $columns = ['*'], $sorting = CouponInterface::DEFAULT_SORTING, $ascending = CouponInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $coupons     = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])
            ->remember($key, $time, function () use ($columns, $sorting, $ascending) {
                return $this->coupon::select($columns)->orderBy($sorting, $ascending)->get();
            });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.coupon')]), $coupons);
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = couponInterface::DEFAULT_LIMIT, $sorting = couponInterface::DEFAULT_SORTING, $ascending = couponInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $coupons     = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
            return $this->coupon::select($columns)
                ->search($conditions)
                ->orderBy($sorting, $ascending)
                ->skip($offset)
                ->limit($limit)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.coupon')]), $coupons);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = couponInterface::DEFAULT_LIMIT, $sorting = couponInterface::DEFAULT_SORTING, $ascending = couponInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $coupons     = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
            $model = $this->coupon::select($columns)
                ->search($conditions)
                ->orderBy($sorting, $ascending);

            if ($page == 1) {
                return $model->paginate($limit);
            } else {
                return $model->skip($limit * ($page - 1))->paginate($limit);
            }
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.coupon')]), $coupons);
    }

    public function findById($id, array $columns = ['*'])
    {
        $coupon = $this->findByIdMethod($id, $columns);

        if (!$coupon) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.coupon'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.coupon')]), $coupon);
    }

    public function dropdownList()
    {
        $key        = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $dropdown   = Cache::remember($key, $time, function () {
            return [
                'type' => $this->coupon::typeList(),
                'status' => $this->coupon::statusList(),
                'limit' => $this->coupon::limitList(),
                'bill' => $this->coupon::billList(),
                'condition' => $this->coupon::conditionList(),
            ];
        });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function store(array $data)
    {
        $coupon  = $this->coupon::create($data);
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.coupon')]), $coupon);
    }

    public function updateById($id, array $data)
    {
        $coupon      = $this->findByIdMethod($id);

        if (!$coupon) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.coupon'), 'id' => $id]));
        }

        $coupon->update($data);
        Cache::tags([
            $this->getIdWithClassPrefix($coupon->id),
            $this->getCouponCodePrefix($coupon->code)
        ])->flush();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.coupon')]), $coupon);
    }

    public function check($code, array $data)
    {
        try {
            $coupon = $this->findByCodeMethod($code);

            if (!$coupon) {
                return $this->coreResponse(404, __('messages.no_data_with_code', ['data' => __('messages.coupon'), 'code' => $code]));
            }
            // check status
            if ($coupon->status == 'inactive') {
                throw new \Exception(__('admin.coupon_not_active'));
            }
            // check bill type
            if ($coupon->bill != 'all') {
                if ($coupon->bill != $data['bill']) {
                    throw new \Exception(__('admin.coupon_bill_invalid', ['bill' => $coupon->current_bill]));
                }
            }
            // check user_code
            if ($coupon->limit_user_code == 'yes') {
                if ($data['user_code'] != $coupon->user_code) {
                    throw new \Exception(__('admin.coupon_user_code_not_match'));
                }
            }
            // check shipping method
            if ($coupon->limit_shipping_method == 'yes') {
                if ($coupon->shipping_method_id != $data['thai_shipping_method_id']) {
                    throw new \Exception(__('admin.coupon_limit_shipping_method', ['shipping' => $coupon->shippingMethod->title_th]));
                }
            }
            // check coupone limit
            if ($coupon->limit <= $coupon->counter) {
                throw new \Exception(__('admin.coupon_limited'));
            }
            // check one_time coupon
            $counterOneTime = app('App\Repositories\BillRepository')->findUserCodeCouponMethod($data['user_code'], $coupon->code);
            if ($coupon->type == 'one_time' && $counterOneTime->count() > 0) {
                throw new \Exception(__('admin.coupon_used'));
            }
            // check date rank 
            $today = Carbon::now();
            $start_date = Carbon::createFromFormat('Y-m-d H:i:s', $coupon->start_date);
            $end_date = Carbon::createFromFormat('Y-m-d H:i:s', $coupon->end_date);
            if ($today->lt($start_date) || $today->gt($end_date)) {
                throw new \Exception(__('admin.coupon_not_in_date_rank'));
            }
            return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.coupon')]), $coupon);
        } catch (\Throwable $th) {
            return $this->coreResponse(500, $th->getMessage());
        }
    }

    public function softDeleteById($id)
    {
        $coupon      = $this->findByIdMethod($id);

        if (!$coupon) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.coupon'), 'id' => $id]));
        }

        $coupon->delete();
        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.delete_data', ['data' => __('messages.coupon')]), $coupon);
    }

    private function clearCacheTags()
    {
        Cache::tags([
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix()
        ])
            ->flush();
    }

    public function findByIdMethod($id, array $columns = ['*'])
    {
        $key        = $this->getCacheKey(null, $id);
        $time       = $this->getTime(self::MINUTE_CACHE);

        $coupon      = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($id)
        ])
            ->remember($key, $time, function () use ($id, $columns) {
                return $this->coupon::select($columns)->with(self::TABLE_RELETIONS)->whereId($id)->first();
            });

        return $coupon;
    }

    public function findByCodeMethod($code, array $columns = ['*'])
    {
        $key = $this->getCacheKey(null, $code);
        $time = $this->getTime(self::MINUTE_CACHE);
        $coupon = Cache::tags([
            $this->getClassPrefix(),
            $this->getCouponCodePrefix($code),
        ])->remember($key, $time, function () use ($code, $columns) {
            return $this->coupon::select($columns)->with(self::TABLE_RELETIONS)->whereCode($code)->first();
        });
        return $coupon;
    }

    public function clearCouponCodeCache($code)
    {
        Cache::tags([
            $this->getCouponCodePrefix($code)
        ])->flush();
    }
}
