<?php

namespace App\Http\Requests\Seo;

use App\Http\Requests\FormRequest;

class UpdateSeoRequest extends FormRequest
{
    /**
     * Determine if the user is authoriz ed to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'         => 'required|string',
            'keyword'       => 'required|string',
            'description'   => 'required|string',
            'code'          => 'required',
            'footer'        => 'required|string',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'title'         => __('form.seo.title'),
            'keyword'       => __('form.seo.keyword'),
            'description'   => __('form.seo.description'),
            'code'          => __('form.seo.code'),
            'footer'        => __('form.seo.footer'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
