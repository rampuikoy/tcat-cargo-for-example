<?php

namespace App\Interfaces;

interface ArticleInterface
{
    const DEFAULT_LIMIT = 25;
    const DEFAULT_SORTING = 'id';
    const DEFAULT_ASCENDING = 'desc';

    /**
     * fetch all article
     * 
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function all(array $columns = ['*'], $sorting = ArticleInterface::DEFAULT_SORTING, $ascending = ArticleInterface::DEFAULT_ASCENDING);

    /**
     * list article 
     * 
     * @param array $conditions
     * @param array $columns
     * @param int $offset
     * @param int $limit
     *
     * @return \Illuminate\Support\Collection
     */
    public function fetchListByFields(array $conditions, array $columns = ['*'], $offset = 0, $limit = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * pagination article
     * 
     * @param array $conditions
     * @param array $columns
     * @param int $page
     * @param int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $perPage = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * find article by id
     * 
     * @param $id
     * @param array $columns
     *
     * @return object|null
     */
    public function findById($id, array $columns = ['*']);

    /**
     * get article dropdown list
     * 
     * @return object
     */
    public function dropdownList();

    /**
     * create new article 
     * 
     * @param array $data
     *
     * @return object|null
     */
    public function store(array $data);

    /**
     * update article by id
     * 
     * @param $id
     * @param array $data
     *
     * @return object|null
     */
    public function updateById($id, array $data);

    /**
     * soft delete article by id
     * 
     * @param $id
     *
     * @return object|null
     */
    public function softDeleteById($id);

}
