<?php

namespace App\Traits;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;

trait UploadTrait
{
    public function getExtensionFile($tmp_file)
    {
        return explode('/', explode(':', substr($tmp_file, 0, strpos($tmp_file, ';')))[1])[1];
    }

    public function uploadBase64($tmp_file, $folder, $filename = '', $resizeWidth = null, $resizeHeight = null)
    {
        if ($filename == '') {
            $chr = Str::random(40);
            $filename = date('ymdHis') . '_' . $chr . '.' . explode('/', explode(':', substr($tmp_file, 0, strpos($tmp_file, ';')))[1])[1];
        }

        $image = Image::make($tmp_file);
        $fileWidth = $image->width();

        if ($fileWidth > $resizeWidth) {
            if ($resizeWidth > 0 && $resizeHeight > 0) {
                $image->resize($resizeWidth, $resizeHeight);
            }
            if ($resizeWidth > 0) {
                $image->resize($resizeWidth, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            } else {
                $image->resize(900, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }
        }

        Storage::disk('s3')->put(env('AWS_S3_FLODER') . '/' . $folder . $filename, $image->encode(), 'public');
        return $filename;
    }

    
    public function upload($tmp_file, $folder, $filename)
    {
        Storage::disk('s3')->put(env('AWS_S3_FLODER') . '/' . $folder . $filename, file_get_contents($tmp_file), 'public');
        return $filename;
    }

    public function removeFile($path)
    {
        try {
            $new_path = str_replace(env('APP_MEDIA_URL'), env('AWS_S3_FLODER') . '/', $path);
            if (Storage::disk('s3')->exists($new_path)) {
                Storage::disk('s3')->delete($new_path);
            }
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function removeFiles($paths = [])
    {
        if (count($paths) > 0) {
            try {
                foreach ($paths as $path) {
                    $new_path = str_replace(env('APP_MEDIA_URL'), env('AWS_S3_FLODER') . '/', $path);
                    if (Storage::disk('s3')->exists($new_path)) {
                        Storage::disk('s3')->delete($new_path);
                    }
                }
            } catch (\Throwable $th) {
                //throw $th;
            }
        }
    }
}
