<?php

namespace App\Models;

use App\Helpers\ScopeQueries;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NoCode extends Model
{
    use HasFactory;

    public static $imageFolder = 'uploads/nocode/';
    public static $imageWidth = 760;

    protected $fillable = [
        'code',
        'order_code',
        'reference_code',
        'zone',
        'weight',
        'width',
        'height',
        'length',
        'date_in',
        'date_out',
        'amount',
        'list',
        'department',
        'description',
        'remark',
        'system_remark',
        'status',
        'type',
        'source',
        'source_order',
        'taken_admin',
        'taken_admin_id',
    ];

    protected $appends = ['status_text', 'type_text', 'department_text'];


    public static function statusList()
    {
        $array = [
            [
                'id'            => 0,
                'name'          => __('messages.no_codes.status.0'),
            ],
            [
                'id'            => 1,
                'name'          => __('messages.no_codes.status.1'),
            ],
            [
                'id'            => 2,
                'name'          => __('messages.no_codes.status.2'),
            ],
            [
                'id'            => 3,
                'name'          => __('messages.no_codes.status.3'),
            ],
        ];
        return $array;
    }

    public static function typeList()
    {
        $array = [
            'clothes'           => __('messages.no_codes.type.clothes'),
            'bag'               => __('messages.no_codes.type.bag'),
            'accessories'       => __('messages.no_codes.type.accessories'),
            'electronics'       => __('messages.no_codes.type.electronics'),
            'toy'               => __('messages.no_codes.type.toy'),
            'shoe'              => __('messages.no_codes.type.shoe'),
            'spares'            => __('messages.no_codes.type.spares'),
            'cosmetics'         => __('messages.no_codes.type.cosmetics'),
            'other'             => __('messages.no_codes.type.other'),
        ];
        return $array;
    }

    public static function departmentList()
    {
        $array = [
            'TCATCARGO'         => 'TCAT-CARGO',
            'TMALL'             => 'TCAT-MALL',
            'TAOBAO'            => 'TAOBAO',
            'ALIBABA'           => 'ALIBABA',
        ];
        return $array;
    }

    public static function timeFormatList()
    {
        $array = [
            'created_at'        => __('messages.no_codes.created_at'),
            'updated_at'        => __('messages.no_codes.updated_at'),
            'date_in'           => __('messages.no_codes.date_in'),
            'date_out'          => __('messages.no_codes.date_out'),
        ];
        return $array;
    }

    public static function sourceList()
    {
        $array = [
            'TCAT'              => 'TCAT-CARGO',
            'TMALL'             => 'TCAT-MALL',
            'TAOBAO'            => 'TAOBAO',
        ];
        return $array;
    }

    public static function conditionList()
    {
        $array = [
            'ref_order'         => __('messages.no_codes.condition.ref_order'),
            'null_ref_order'    => __('messages.no_codes.condition.null_ref_order'),
            'order_code'        => __('messages.no_codes.condition.order_code'),
            'null_order_code'   => __('messages.no_codes.condition.null_order_code'),    
            'department'        => __('messages.no_codes.condition.department'),
            'null_type'         => __('messages.no_codes.condition.null_type'),
            'null_weight'       => __('messages.no_codes.condition.null_weight'),
            'null_size'         => __('messages.no_codes.condition.null_size'),
            'description'       => __('messages.no_codes.condition.description'),
            'null_description'  => __('messages.no_codes.condition.null_description'),
            'source_order'      => __('messages.no_codes.condition.source_order'),
            'source'            => __('messages.no_codes.condition.source'),
        ];
        return $array;
    }

    public function getStatusTextAttribute()
    {
        $status = collect(self::statusList())->get($this->status);
        return $status['name'] ?? null;
    }

    public function getTypeTextAttribute()
    {
        return collect(self::typeList())->get($this->type);
    }

    public function getDepartmentTextAttribute()
    {
        return collect(self::departmentList())->get($this->department);
    }

    // reletions
    public function images()
    {
        return $this->morphMany('App\Models\Upload', 'relatable');
    }
}