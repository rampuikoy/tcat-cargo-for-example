<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Interfaces\CreditInterface;
use App\Models\Credit;
use App\Http\Requests\Credit\StoreCreditRequest;
use App\Http\Requests\Credit\UpdateCreditRequest;

class CreditController extends Controller
{
    protected $creditInterface;

    public function __construct(CreditInterface $creditInterface)
    {
        $this->creditInterface = $creditInterface;
    }
    public function index(Request $request)
    {
        $this->authorize('index', Credit::class);
        $columns        = $request->input('columns', ['*']);
        $Credit  = $this->creditInterface->all($columns);
        return response()->json($Credit, $Credit->code);
    }

    public function search(Request $request)
    {
        $this->authorize('index', Credit::class);
        $conditions   = $request->only(['id', 'date_start', 'date_end', 'user_code', 'baht', 'type', 'condition']);
        $columns      = $request->input('columns', ['*']);
        $limit        = $request->input('limit', $this->creditInterface::DEFAULT_LIMIT);
        $sorting      = $request->input('order_by', $this->creditInterface::DEFAULT_SORTING);
        $ascending    = $request->input('ascending') == 1 ? 'asc' : 'desc';
        if ($request->boolean('pagination')) {
            $page       = $request->input('page', 1);
            $Credit  = $this->creditInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset     = $request->input('page', 0);
            $Credit  = $this->creditInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($Credit, $Credit->code);
    }

    public function dropdown()
    {
        $dropdown = $this->creditInterface->dropdownList();
        return response()->json($dropdown, $dropdown->code);
    }

    public function show(Request $request, $id)
    {
        $this->authorize('show', Credit::class);
        $columns    = $request->input('columns', ['*']);
        $Credit       = $this->creditInterface->findById($id, $columns);
        return response()->json($Credit, $Credit->code);
    }
}
