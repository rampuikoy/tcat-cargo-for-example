<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrackingTaoBaoDetail extends Model
{
    use HasFactory;
    protected $connection = 'taobaoconew';
    protected $table = 'tracking_detail';
    public $timestamps = false;

    protected $appends = [
        'shipping_method_text',
        'product_type_text',
    ];
    public function getShippingMethodTextAttribute()
    {
        return collect(TrackingTmall::shippingTypeList())->get($this->shipping_id);
    }
    public function getProductTypeTextAttribute()
    {
        return collect(TrackingTmall::productTypeList())->get($this->basket_type_id);
    }
    public function scopeSearch($query, $code)
    {
        return $query->where('tracking_code', $code);
    }


}
