<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {

        /*  is this exception? */

        if (!empty($exception) && $request->wantsJson()) {

            // set default error message
            $response = [
                'code' => 400,
                'error' => true,
                'message' => 'Sorry, can not execute your request.',
            ];

            // If debug mode is enabled
            // if (config('app.debug')) {
            // Add the exception class name, message and stack trace to response
            $response['laravel_exception'] = get_class($exception); // Reflection might be better here
            $response['laravel_message'] = $exception->getMessage();
            $response['laravel_trace'] = $exception->getTrace();
            // }

            // get correct status code
            // is this validation exception
            if ($exception instanceof ValidationException) {
                return $this->convertValidationExceptionToResponse($exception, $request);

                // is it authentication exception
            } else if ($exception instanceof AuthorizationException || $exception instanceof UnauthorizedHttpException) {
                $response['code'] = 401;
                $response['message'] = $exception->getMessage();
                // is it not found exception
            } else if ($exception instanceof AccessDeniedHttpException) {
                $response['code'] = 403;
                $response['message'] = $exception->getMessage();
            } else if ($exception instanceof ThrottleRequestsException) {
                $response['code'] = 429;
                $response['message'] = $exception->getMessage();
            } else if ($exception instanceof NotFoundHttpException) {
                $response['code'] = 404;
                $response['message'] = $exception->getMessage();

                //is it DB exception
            } else if ($exception instanceof \PDOException) {
                $response['code'] = 500;

            } else if ($this->isHttpException($exception)) {
                $response['code'] = $exception->getCode();
            } else {
                // for all others check do we have method getStatusCode and try to get it
                // otherwise, set the status to 400
                $response['code'] = method_exists($exception, 'getStatusCode') ? $exception->getCode() : 400;
            }
            return response()->json($response, $response['code']);
        }

        return parent::render($request, $exception);
    }
}
