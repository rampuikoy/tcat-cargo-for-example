<?php

namespace App\Repositories;

use App\Interfaces\ThaiShippingWeightSizePriceInterface;
use App\Models\ThaiShippingWeightSizePrice;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use Illuminate\Support\Facades\Cache;


class ThaiShippingWeightSizePriceRepository implements ThaiShippingWeightSizePriceInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const MINUTE_CACHE          = 1;
    const TABLE_RELETIONS       = [];

    public function __construct(ThaiShippingWeightSizePrice $thaiShippingWeightSizePrice)
    {
        $this->thaiShippingWeightSizePrice        = $thaiShippingWeightSizePrice;
    }

    public function all(array $columns = ['*'], $sorting = ThaiShippingWeightSizePriceInterface::DEFAULT_SORTING, $ascending = ThaiShippingWeightSizePriceInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $thaiShippingWeightSizePrices     = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
            return $this->thaiShippingWeightSizePrice::select($columns)
            ->orderBy($sorting, $ascending)
            ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.thai_shipping_weight_size_price')]), $thaiShippingWeightSizePrices);
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = ThaiShippingWeightSizePriceInterface::DEFAULT_LIMIT, $sorting = ThaiShippingWeightSizePriceInterface::DEFAULT_SORTING, $ascending = ThaiShippingWeightSizePriceInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $thaiShippingWeightSizePrices     = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
            return $this->thaiShippingWeightSizePrice::select($columns)
                                                    ->where(function ($query) use ($conditions) {
                                                        if (@$conditions['thai_shipping_method_id']) {
                                                            $query->whereThaiShippingMethodId($conditions['thai_shipping_method_id']);
                                                        }
                                                    })
                                                    ->orderBy($sorting, $ascending)
                                                    ->skip($offset)
                                                    ->limit($limit)
                                                    ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.thai_shipping_weight_size_price')]), $thaiShippingWeightSizePrices);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = ThaiShippingWeightSizePriceInterface::DEFAULT_LIMIT, $sorting = ThaiShippingWeightSizePriceInterface::DEFAULT_SORTING, $ascending = ThaiShippingWeightSizePriceInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $thaiShippingWeightSizePrices     = Cache::tags([$this->getClassPrefix(), $this->getPaginationWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
            $model = $this->thaiShippingWeightSizePrice::select($columns)
                                                        ->where(function ($query) use ($conditions) {
                                                            if (@$conditions['thai_shipping_method_id']) {
                                                                $query->whereThaiShippingMethodId($conditions['thai_shipping_method_id']);
                                                            }
                                                        })
                                                        ->orderBy($sorting, $ascending);

            if ($page == 1) {
                return $model->paginate($limit);
            } else {
                return $model->skip($limit * ($page - 1))->paginate($limit);
            }
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.thai_shipping_weight_size_price')]), $thaiShippingWeightSizePrices);
    }

    public function findById($id, array $columns = ['*'])
    {
        $thaiShippingWeightSizePrice = $this->findByIdMethod($id, $columns);

        if (!$thaiShippingWeightSizePrice) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.thai_shipping_weight_size_price'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.thai_shipping_weight_size_price')]), $thaiShippingWeightSizePrice);
    }

    public function findManyByShippingId($id, array $columns = ['*'])
    {
        $thaiShippingWeightSizePrice  = $this->findManyByShippingIdMethod($id, $columns);

        if ($thaiShippingWeightSizePrice->count() === 0) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.thai_shipping_weight_size_price'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.thai_shipping_weight_size_price')]), $thaiShippingWeightSizePrice);
    }

    public function store(array $data)
    {
        $thaiShippingWeightSizePrice = $this->thaiShippingWeightSizePrice::create([
            'title'                     => $data['title'],
            'max_size'                  => $data['max_size'],
            'min_weight'                => $data['min_weight'],
            'max_weight'                => $data['max_weight'],
            'price'                     => $data['price'],
            'thai_shipping_method_id'   => $data['thai_shipping_method_id'],
        ]);
        $this->clearCacheTags();
        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.thai_shipping_weight_size_price')]), $thaiShippingWeightSizePrice);
    }

    public function updateById($id, array $data)
    {
        $thaiShippingWeightSizePrice      = $this->findByIdMethod($id);

        if (!$thaiShippingWeightSizePrice) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.thai_shipping_weight_size_price'), 'id' => $id]));
        }

        $thaiShippingWeightSizePrice->update([
            'title'                     => $data['title'],
            'max_size'                  => $data['max_size'],
            'min_weight'                => $data['min_weight'],
            'max_weight'                => $data['max_weight'],
            'price'                     => $data['price'],
        ]);
        $this->clearCacheTags();
        $this->clearCacheTagById($id);
        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.thai_shipping_weight_size_price')]), $thaiShippingWeightSizePrice);
    }

    public function softDeleteById($id)
    {
        $thaiShippingWeightSizePrice      = $this->findByIdMethod($id);

        if (!$thaiShippingWeightSizePrice) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.thai_shipping_weight_size_price'), 'id' => $id]));
        }

        $thaiShippingWeightSizePrice->delete();

        $this->clearCacheTags();
        $this->clearCacheTagById($id);
        return $this->coreResponse(200, __('messages.delete_data', ['data' => __('messages.thai_shipping_weight_size_price')]), $thaiShippingWeightSizePrice);
    }

    private function findByIdMethod($id, array $columns = ['*'])
    {
        $key    = $this->getCacheKey(null, $id);
        $time   = $this->getTime(self::MINUTE_CACHE);

        $thaiShippingWeightSizePrice  = Cache::remember($key, $time, function () use ($id, $columns) {
            return $this->thaiShippingWeightSizePrice::select($columns)->whereId($id)->first();
        });

        return $thaiShippingWeightSizePrice;
    }

    private function findManyByShippingIdMethod($id, array $columns = ['*'])
    {
        $key    = $this->getCacheKey('SHIPPING_METHOD', $id);
        $time   = $this->getTime(self::MINUTE_CACHE);
        
        $thaiShippingWeightSizePrice  = Cache::tags([
                                        $this->getClassPrefix(),
                                        $this->getIdWithClassPrefix($id),
                                        $this->getFindByShippingMethodIdWithClassPrefix($id),
                                    ])->remember($key, $time, function () use ($id, $columns) {
                                        return $this->thaiShippingWeightSizePrice::select($columns)->whereThaiShippingMethodId($id)->get();
                                    });

        return $thaiShippingWeightSizePrice;
    }

    private function clearCacheTags()
    {
        $keys = [
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix(),
        ];
        Cache::tags($keys)->flush();
    }

    private function clearCacheTagById($id)
    {
        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();
    }
}
