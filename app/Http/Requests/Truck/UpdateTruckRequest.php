<?php

namespace App\Http\Requests\Truck;

use App\Http\Requests\FormRequest;
class UpdateTruckRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.truck.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'         => 'required|string',
            'code'          => 'required|string',
            'remark'        => 'required|string',
            'status'        => 'required|in:active,inactive',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'title'          => __('form.truck.title'),
            'code'           => __('form.truck.code'),
            'remark'         => __('form.truck.remark'),
            'status'         => __('form.truck.status')
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
