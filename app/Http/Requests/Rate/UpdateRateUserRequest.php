<?php

namespace App\Http\Requests\Rate;

use App\Http\Requests\FormRequest;

class UpdateRateUserRequest extends FormRequest
{
    public function authorize()
    {
        return $this->user('admins')->can('extend.user-rate');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_code'           => 'required|exists:users,code',
            'kg_car_genaral'      => 'required|numeric',
            'kg_car_iso'          => 'required|numeric',
            'kg_car_brand'        => 'required|numeric',
            'kg_ship_genaral'     => 'required|numeric',
            'kg_ship_iso'         => 'required|numeric',
            'kg_ship_brand'       => 'required|numeric',
            'cubic_car_genaral'   => 'required|numeric',
            'cubic_car_iso'       => 'required|numeric',
            'cubic_car_brand'     => 'required|numeric',
            'cubic_ship_genaral'  => 'required|numeric',
            'cubic_ship_iso'      => 'required|numeric',
            'cubic_ship_brand'    => 'required|numeric',
        ];
    }

        /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [

            'user_code'             => __('form.rate.user_code'),
            'kg_car_genaral'        => __('form.rate.kg_car_genaral'),
            'kg_car_iso'            => __('form.rate.kg_car_iso'),
            'kg_car_brand'          => __('form.rate.kg_car_brand'),

            'kg_ship_genaral'       => __('form.rate.kg_ship_genaral'),
            'kg_ship_iso'           => __('form.rate.kg_ship_iso'),
            'kg_ship_brand'         => __('form.rate.kg_ship_brand'),

            'cubic_car_genaral'     => __('form.rate.cubic_car_genaral'),
            'cubic_car_iso'         => __('form.rate.cubic_car_iso'),
            'cubic_car_brand'       => __('form.rate.cubic_car_brand'),

            'cubic_ship_genaral'    => __('form.rate.cubic_ship_genaral'),
            'cubic_ship_iso'        => __('form.rate.cubic_ship_iso'),
            'cubic_ship_brand'      => __('form.rate.cubic_ship_brand'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}