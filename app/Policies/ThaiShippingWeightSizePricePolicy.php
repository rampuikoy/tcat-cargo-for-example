<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class ThaiShippingWeightSizePricePolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.thai-shipping-weight-size-price.index');
    }

    public function show(Admin $admin)
    {
        return $admin->hasAnyPermission(['backend.thai-shipping-weight-size-price.show', 'backend.thai-shipping-weight-size-price.update']);
    }

    public function store(Admin $admin)
    {
        return $admin->can('backend.thai-shipping-weight-size-price.store');
    }

    public function update(Admin $admin)
    {
        return $admin->can('backend.thai-shipping-weight-size-price.update');
    }

    public function destroy(Admin $admin)
    {
        return $admin->can('backend.thai-shipping-weight-size-price.destroy');
    }
}
