<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seo extends Model
{
    protected $table = 'seo';

    protected $fillable = [
        'title',
        'keyword',
        'description',
        'code',
        'footer',
        'created_admin_id',
        'updated_admin_id'
    ];
}
