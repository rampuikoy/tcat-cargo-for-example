<?php

namespace App\Http\Requests\Commission;

use App\Http\Requests\FormRequest;
use App\Rules\ExcelRule;

class StoreCommissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('extend.commission');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file'       => ['required', new ExcelRule(request()->file)],
            'admin_id'   => 'required|exists:admins,id',
            'remark'     => 'sometimes|nullable|string',
        ];
    }
}
