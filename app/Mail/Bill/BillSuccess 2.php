<?php

namespace App\Mail\Bill;

use App\Models\Bill;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BillSuccess extends Mailable
{
    use Queueable, SerializesModels;
    public $bill;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Bill $bill)
    {
        $this->bill    = $bill;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->bill->counter > 400) {
            return $this->to($this->bill->user->email)
                ->subject(__('emails.bill_success', ['no' => $this->bill->no]))
                ->markdown('emails.bill.bill-success');
        } else {
            try {
                $filename = $this->bill->no . '.pdf';
                $data = app('App\Http\Controllers\Backend\BillDownloadController')->getDataBillAll($this->bill->id);
                $file = PDF::loadView('admin.bill.download.bill-pdf',  $data);
                return $this->to($this->bill->user->email)
                    ->subject(__('emails.bill_success', ['no' => $this->bill->no]))
                    ->markdown('emails.bill.bill-success')
                    ->attachData($file->stream(),  $filename, [
                        'mime' => 'application/pdf',
                    ]);
            } catch (\Throwable $t) {
                Log::error($t->getMessage());
            }
        }
    }
}
