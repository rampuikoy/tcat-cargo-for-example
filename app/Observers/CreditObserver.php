<?php

namespace App\Observers;

use App\Events\UpdateUserEvent;
use App\Models\Credit;
use App\Observers\BaseObserver;

class CreditObserver extends BaseObserver
{
    public function creating(Credit $credit)
    {
        $this->stampCreated($credit);
    }

    public function updating(Credit $credit)
    {
        $this->stampUpdated($credit);

    }

    public function created(Credit $credit)
    {
        $this->broadcastUserUpdated($credit->user_code);
    }

    public function updated(Credit $credit)
    {
        if ($credit->isDirty('after')) {
            $this->broadcastUserUpdated($credit->user_code);
        }
    }

    private function broadcastUserUpdated($code)
    {
                app('App\Repositories\UserRepository')->clearCacheTagByUserCode($code);
        $user = app('App\Repositories\UserRepository')->findByCode($code);
        broadcast(new UpdateUserEvent($user->results))->toOthers();
    }
}
