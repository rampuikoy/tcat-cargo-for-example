<?php

namespace App\Interfaces;

interface AdminInterface
{
    const DEFAULT_LIMIT = 25;
    const DEFAULT_SORTING = 'id';
    const DEFAULT_ASCENDING = 'desc';

    /**
     * fetch all admin
     *
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function all(array $columns = ['*'], $sorting = AdminInterface::DEFAULT_SORTING, $ascending = AdminInterface::DEFAULT_ASCENDING);

    /**
     * list admin
     *
     * @param array $conditions
     * @param array $columns
     * @param int $offset
     * @param int $limit
     *
     * @return \Illuminate\Support\Collection
     */
    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * pagination admin
     *
     * @param array $conditions
     * @param array $columns
     * @param int $page
     * @param int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $perPage = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * find admin by id
     *
     * @param $id
     * @param array $columns
     *
     * @return object|null
     */
    public function findById($id, array $columns = ['*']);

    /**
     * find admin by type sale
     *
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function findManySale(array $columns = ['*']);

    /**
     * find admin by type driver
     *
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function findManyDriver(array $columns = ['*']);

    /**
     * get admin dropdown list
     *
     * @return object
     */
    public function dropdownList();

    /**
     * create new admin
     *
     * @param array $data
     *
     * @return object|null
     */
    public function store(array $data);

    /**
     * update admin by id
     *
     * @param $id
     * @param array $data
     *
     * @return object
     */
    public function updateById($id, array $data);

    /**
     * update password by id
     *
     * @param $id
     * @param string $password
     *
     * @return object|null
     */
    public function updatePasswordById($id, string $password);

    /**
     * soft delete admin by id
     *
     * @param $id
     *
     * @return object|null
     */
    public function softDeleteById($id);

}
