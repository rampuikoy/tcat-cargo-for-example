<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ThaiShippingAreaPrice extends Model
{
    use HasFactory, SoftDeletes;
    
    protected $fillable = [
        'price_a',
        'price_b',
        'price_c',
        'price_d',
        'price_e',
        'province_id',
        'amphur_id',
        'thai_shipping_method_id',
    ];

    public static function ratePriceList()
    {
        $array = [
            'price_a' => 0.030,
            'price_b' => 0.050,
            'price_c' => 0.085,
            'price_d' => 0.120,
            'price_e' => 0.180,
        ];
        return $array;
    }

    /**
     * relation
     */
    public function province()
    {
        return $this->belongsTo('App\Models\Province', 'province_id');
    }
    public function amphur()
    {
        return $this->belongsTo('App\Models\Amphur', 'amphur_id');
    }
    public function thaiShippingMethod()
    {
        return $this->belongsTo('App\Models\ThaiShippingMethod', 'thai_shipping_method_id');
    }
}
