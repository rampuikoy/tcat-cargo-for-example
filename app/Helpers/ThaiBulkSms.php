<?php
namespace App\Helpers;

use Illuminate\Support\Str;

class ThaiBulkSms
{
    public static function send($msisdn, $message)
    {
        if (env('ENABLE_SMS') !== 'on') {
            return $result = [
                "send" => [
                    0 => 1,
                ],
                "detail" => [
                    0 => 'Mock Sms Data',
                ],
                "transaction" => [
                    0 => Str::uuid(),
                ],
                "used_credit" => [
                    0 => 2,
                ],
                "remain_credit" => [
                    0 => "130728",
                ],
            ];

        }

        $url = env('SMS_URL');
        if (extension_loaded('curl')) {
            $data = array(
                'username' => env('SMS_USERNAME'),
                'password' => env('SMS_PASSWORD'),
                'sender' => env('SMS_SENDER'),
                'force' => env('SMS_FORCE'),
                'msisdn' => $msisdn,
                'message' => $message,
            );
            $data_string = http_build_query($data);

            $agent = "ThaiBulkSMS API PHP Client";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_USERAGENT, $agent);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
            $xml_result = curl_exec($ch);
            $code = curl_getinfo($ch);
            curl_close($ch);

            if ($code['http_code'] == 200) {
                if (function_exists('simplexml_load_string')) {
                    $sms = new \SimpleXMLElement($xml_result);
                    $count = count($sms->QUEUE);
                    if ($count > 0) {
                        $result = [
                            'send' => $sms->QUEUE[0]->Status,
                            'detail' => $sms->QUEUE[0]->Detail,
                            'transaction' => $sms->QUEUE[0]->Transaction,
                            'used_credit' => $sms->QUEUE[0]->UsedCredit,
                            'remain_credit' => $sms->QUEUE[0]->RemainCredit,
                        ];
                        return ThaiBulkSms::xml2array($result);
                    } else {
                        $msg_string = "เกิดข้อผิดพลาดในการทำงาน, (" . $sms->Detail . ")";
                    }

                } else if (function_exists('xml_parse')) {
                    $xml = ThaiBulkSms::xml2array($xml_result);
                    $count = count($xml['SMS']['QUEUE']);
                    if ($count > 0) {
                        $count_pass = 0;
                        $count_fail = 0;
                        $used_credit = 0;
                        for ($i = 0; $i < $count; $i++) {
                            if ($xml['SMS']['QUEUE'][$i]['Status']) {
                                $count_pass++;
                                $used_credit +=
                                    $xml['SMS']['QUEUE'][$i]['UsedCredit'];
                            } else {
                                $count_fail++;
                            }
                        }
                        if ($count_pass > 0) {
                            $msg_string = "สามารถส่งออกได้จำนวน $count_pass หมายเลข, ใช้เครดิตทั้งหมด $used_credit เครดิต";
                            echo "สามารถส่งออกได้จำนวน $count_pass หมายเลข, ใช้เครดิตทั้งหมด $used_credit เครดิต";
                        }
                        if ($count_fail > 0) {
                            $msg_string = "ไม่สามารถส่งออกได้จำนวน $count_fail หมายเลข";
                            echo "ไม่สามารถส่งออกได้จำนวน $count_fail หมายเลข";
                        }
                    } else {
                        $msg_string = "เกิดข้อผิดพลาดในการทำงาน, (" . $xml['SMS']['Detail'] . ")";
                        echo "เกิดข้อผิดพลาดในการทำงาน, (" . $xml['SMS']['Detail'] . ")";
                    }
                } else {
                    $msg_string = "เกิดข้อผิดพลาดในการทำงาน: <br /> ระบบไม่รองรับฟังก์ชั่น XML";
                    echo "เกิดข้อผิดพลาดในการทำงาน: <br /> ระบบไม่รองรับฟังก์ชั่น XML";
                }
            } else {
                //$http_codes = parse_ini_file("http_code.ini");
                //$msg_string = "เกิดข้อผิดพลาดในการทำงาน: <br />" . $code['http_code'] . " " . $http_codes[$code['http_code']];
                $msg_string = "เกิดข้อผิดพลาดในการทำงาน: <br />" . $code['http_code'];
                echo "เกิดข้อผิดพลาดในการทำงาน: <br />" . $code['http_code'];
            }

        } else {
            if (function_exists('fsockopen')) {
                // $msg_string = $this->sending_fsock($username,$password,$msisdn,$message,$sender,$ScheduledDelivery,$force);
            } else {
                $msg_string = "cURL OR fsockopen is not enabled";
            }
        }
        return $msg_string;
    }

    public static function xml2array($xmlObject, $out = array())
    {
        foreach ((array) $xmlObject as $index => $node) {
            $out[$index] = (is_object($node)) ? ThaiBulkSms::xml2array($node) : $node;
        }

        return $out;
    }
}

/*

<SMS>
<QUEUE>
<Msisdn>0800411059</Msisdn>
<Status>1</Status>
<Transaction>1461977e6cd247dca77751ddbcd962b0</Transaction>
<UsedCredit>1</UsedCredit>
<RemainCredit>37732</RemainCredit>
</QUEUE>
<!-- 0.011981010437012 -->
</SMS>

<SMS>
<QUEUE>
<Msisdn></Msisdn>
<Status>0</Status>
<Detail>Invalid Phone Number</Detail>
</QUEUE>
<!-- 0.0098261833190918 -->
</SMS>

 */
