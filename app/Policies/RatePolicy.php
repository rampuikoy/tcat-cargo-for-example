<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class RatePolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.rate.index');
    }

    public function show(Admin $admin)
    {
        return $admin->hasAnyPermission(['backend.rate.show', 'backend.rate.update']);
    }

    public function store(Admin $admin)
    {
        return $admin->can('backend.rate.store');
    }

    public function update(Admin $admin)
    {
        return $admin->can('backend.rate.update');
    }

    public function destroy(Admin $admin)
    {
        return $admin->can('backend.rate.destroy');
    }
}
