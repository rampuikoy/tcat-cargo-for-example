<?php

namespace App\Http\Requests\Withdraw;

use App\Http\Requests\FormRequest;


class StoreWithdrawRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.withdraw.store');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_code'            => 'required',
            'amount'               => 'required|numeric|min:0',
            'user_bank_account_id' => 'required_if:method,transfer|nullable|exists:bank_accounts,id',
            'admin_remark'         => 'required_if:method,credit',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'user_code'             => __('form.withdraw.user_code'),
            'amount'                => __('form.withdraw.amount'),
            'user_bank_account_id'  => __('form.withdraw.user_bank_account_id'),
            'admin_remark'          => __('form.withdraw.admin_remark'),
            'status'                => __('form.withdraw.status'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
