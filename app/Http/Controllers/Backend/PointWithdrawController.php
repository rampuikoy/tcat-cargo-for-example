<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\PointWithdraw\StorePointWithdrawRequest;
use Illuminate\Http\Request;
use App\Interfaces\PointWithdrawInterface;
use App\Models\PointWithdraw;

class PointWithdrawController extends Controller
{
    protected $pointWithdrawInterface;

    public function __construct(PointWithdrawInterface $pointWithdrawInterface)
    {
        $this->pointWithdrawInterface = $pointWithdrawInterface;
    }
    public function index(Request $request)
    {
        $this->authorize('index', PointWithdraw::class);
        $columns            = $request->input('columns', ['*']);
        $point_withdraw     = $this->pointWithdrawInterface->all($columns);
        return response()->json($point_withdraw, $point_withdraw->code);
    }

    public function show(Request $request, $id)
    {
        $this->authorize('show', PointWithdraw::class);
        $columns            = $request->input('columns', ['*']);
        $point_withdraw     = $this->pointWithdrawInterface->findById($id, $columns);
        return response()->json($point_withdraw, $point_withdraw->code);
    }

    public function search(Request $request)
    {
        $this->authorize('index', PointWithdraw::class);
        $conditions   = $request->only(['user_code', 'amount', 'type', 'status', 'date_start', 'date_end']);
        $columns      = $request->input('columns', ['*']);
        $limit        = $request->input('limit', $this->pointWithdrawInterface::DEFAULT_LIMIT);
        $sorting      = $request->input('order_by', $this->pointWithdrawInterface::DEFAULT_SORTING);
        $ascending    = $request->input('ascending') == 1 ? 'asc' : 'desc';
        if ($request->boolean('pagination')) {
            $page               = $request->input('page', 1);
            $point_withdraw     = $this->pointWithdrawInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset             = $request->input('page', 0);
            $point_withdraw     = $this->pointWithdrawInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($point_withdraw, $point_withdraw->code);
    }

    public function dropdown()
    {
        $dropdown = $this->pointWithdrawInterface->dropdownList();
        return response()->json($dropdown, $dropdown->code);
    }

    public function store(StorePointWithdrawRequest $request)
    {
        $data           = $request->only([
                            'user_code',
                            'amount',
                            'type',
                            'remark'
                        ]);
        $point_withdraw = $this->pointWithdrawInterface->store($data);
        return response()->json($point_withdraw, $point_withdraw->code);
    }

    public function approve(Request $request, $id)
    {
        $this->authorize('approve', PointWithdraw::class);
        $point_withdraw  = $this->pointWithdrawInterface->approveById($id);
        return response()->json($point_withdraw, $point_withdraw->code);
    }

    public function close(Request $request, $id)
    {
        $this->authorize('approve', PointWithdraw::class);
        $point_withdraw  = $this->pointWithdrawInterface->closeById($id);
        return response()->json($point_withdraw, $point_withdraw->code);
    }

    public function cancel(Request $request, $id)
    {
        $this->authorize('destroy', PointWithdraw::class);
        $point_withdraw  = $this->pointWithdrawInterface->cancelById($id);
        return response()->json($point_withdraw, $point_withdraw->code);
    }
}
