<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    public static $imageFolder  = 'uploads/product/';
    public static $imageWidth   = 760;

    protected $fillable = [
        'title',
        'image',
        'code',
        'description',
        'stock',
        'point',
        'limit',
        'status',
        'remark',
        'created_admin',
        'updated_admin',
        'created_admin_id',
        'updated_admin_id',
    ];

    protected $appends = ['status_text', 'file_url'];

    public static function statusList()
    {
        $array = [
            'active'    => __('messages.active'),
            'inactive'  => __('messages.inactive'),
        ];
        return $array;
    }

    /**
     * Get the name for enum columns
     *
     * @return string
     */
    public function getStatusTextAttribute()
    {
        return collect(self::statusList())->get($this->status);
    }

    public function getFileUrlAttribute($handle = null)
    {
        $file = $handle ?? $this->image;
        return ($file) ?  env('APP_MEDIA_URL') . '/' . self::$imageFolder . $file : null;
    }

    // scope query
    public function scopeSearch($query, $conditions)
    {
        return $query->where(function ($query) use ($conditions) {
            if (@$conditions['title']) {
                $query->where('title', 'like', '%' . $conditions['title'] . '%');
            }
            if (@$conditions['code']) {
                $query->where('code', 'like', '%' . $conditions['code'] . '%');
            }
            if (@$conditions['status']) {
                $query->whereStatus($conditions['status']);
            }
        });
    }
}
