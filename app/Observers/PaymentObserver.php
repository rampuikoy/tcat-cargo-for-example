<?php

namespace App\Observers;

use App\Models\Payment;
use App\Observers\BaseObserver;

class PaymentObserver extends BaseObserver
{
    /**
     * Handle the Payment "created" event.
     *
     * @param  \App\Models\Payment  $payment
     * @return void
     */
    public function creating(Payment $payment)
    {
        $payment->code = $this->uniqueCode();
        $this->stampCreated($payment);
    }

    /**
     * Handle the Payment "updated" event.
     *
     * @param  \App\Models\Payment  $payment
     * @return void
     */
    public function updating(Payment $payment)
    {

        $this->stampUpdated($payment);
    }

    /**
     * Handle the Payment "deleted" event.
     *
     * @param  \App\Models\Payment  $payment
     * @return void
     */
    public function deleted(Payment $payment)
    {
        //
    }

    /**
     * Handle the Payment "restored" event.
     *
     * @param  \App\Models\Payment  $payment
     * @return void
     */
    public function restored(Payment $payment)
    {
        //
    }

    /**
     * Handle the Payment "force deleted" event.
     *
     * @param  \App\Models\Payment  $payment
     * @return void
     */
    public function forceDeleted(Payment $payment)
    {
        //
    }

    public function uniqueCode()
    {
        do {
            $unique = 'PM-' . random_int(1000000, 9999999);
        } while (Payment::where('code', $unique)->exists());
        return $unique;
    }
}
