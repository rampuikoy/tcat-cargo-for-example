<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Admin;
use Tymon\JWTAuth\Facades\JWTAuth;

class LocationTest extends TestCase
{
    use WithFaker;
    protected $user;
    protected $url = '/api/backend/norole';
    /**
     * A basic feature test example.
     *
     * @return void
     */
    protected function authenticate(string $guard = 'admins')
    {
        $this->user = factory(Admin::class)->create();
        auth()->shouldUse($guard);
        $token = JWTAuth::fromUser($this->user);
        return 'Bearer ' . $token;
    }

    protected function deleteUser()
    {
        $this->user->forceDelete();
    }

    /** @test */
    public function GetProvineAllSuccessTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('GET', "{$this->url}/province");
        $response->assertStatus(200);
        $this->deleteUser();
    }

    /** @test */
    public function GetProvineAllFailTest()
    {
        $response = $this->json('GET', "{$this->url}/province");
        $response->assertStatus(401);
    }

    /** @test */
    public function FindAmphurByProvinceIdSuccessTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('GET', "{$this->url}/province/66/amphur");
        $response->assertStatus(200);
        $this->deleteUser();
    }

    /** @test */
    public function FindAmphurByProvinceIdFailTest()
    {
        $response = $this->json('GET', "{$this->url}/province/66/amphur");
        $response->assertStatus(401);
    }

    /** @test */
    public function FindDistrictByAmphurIdSuccessTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('GET', "{$this->url}/amphur/880/district");
        $response->assertStatus(200);
        $this->deleteUser();
    }

    /** @test */
    public function FindDistrictByAmphurIdFailTest()
    {
        $response = $this->json('GET', "{$this->url}/amphur/880/district");
        $response->assertStatus(401);
    }

    /** @test */
    public function FindZipcodeByDistrictCodeSuccessTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('GET', "{$this->url}/district/830101/zipcode");
        $response->assertStatus(200);
        $this->deleteUser();
    }

    /** @test */
    public function FindZipcodeByDistrictCodeFailTest()
    {
        $response = $this->json('GET', "{$this->url}/district/830101/zipcode");
        $response->assertStatus(401);
    }

    /** @test */
    public function GetPermissionAllSuccessTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('GET', "{$this->url}/permission");
        $response->assertStatus(200);
        $this->deleteUser();
    }

    /** @test */
    public function GetPermissionAllFailTest()
    {
        $response = $this->json('GET', "{$this->url}/permission");
        $response->assertStatus(401);
    }

     /** @test */
     public function GetUseruserSearchSuccessTest()
     {
         $token = $this->authenticate();
         $response = $this->withHeaders([
             'Authorization' => $token,
         ])->json('GET', "{$this->url}/user/search");
         $response->assertStatus(200);
         $this->deleteUser();
     }
 
     /** @test */
     public function GetUseruserSearchFailTest()
     {
         $response = $this->json('GET', "{$this->url}/user/search");
         $response->assertStatus(401);
     }

      /** @test */
      public function FindByCodeOrEmailSuccessTest()
      {
          $token = $this->authenticate();
          $response = $this->withHeaders([
              'Authorization' => $token,
          ])->json('GET', "{$this->url}/user/find/test");
          $response->assertStatus(200);
          $this->deleteUser();
      }
  
      /** @test */
      public function FindByCodeOrEmailFailTest()
      {
          $response = $this->json('GET', "{$this->url}/user/find/test");
          $response->assertStatus(401);
      }
}
