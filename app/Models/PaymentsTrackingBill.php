<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentsTrackingBill extends Model
{
    use HasFactory;

    protected $table = 'fcm_queues';

    protected $fillable = [
        'code',
        'before',
        'amount',
        'after',
        'cash',
        'payment_type_id',
        'status',
        'user_id',
        'created_admin',
        'updated_admin',
        'approved_admin',
        'created_admin_id',
        'updated_admin_id',
        'approved_admin_id',
    ];
}
