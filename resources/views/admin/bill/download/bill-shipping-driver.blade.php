<!DOCTYPE html>
<html lang="th">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>ใบคุม</title>
	{!! Html::style('public/css/pdf.css') !!}
	<link rel="stylesheet" href="https://res.imemo.co/tcatcargo_alpha/misc/css/pdf.css" media="all" />
	<style>
		html {
			margin: 25px 25px 25px 25px
		}
	</style>
</head>

<body>
	<div>
		<table style="margin-bottom: 10px;" aria-describedby="shipping-droppoint-driver">
			<tbody>
				<tr>
					<th colspan="3" scope="col" class="text-center">
						<div style="font-size: 24px;">ใบคุมรถบริษัท</div>
					</th>
				</tr>
				<tr>
					<td>
						<div style="border-bottom: 1px dotted;">วันที่</div>
					</td>
					<td>
						<div style="border-bottom: 1px dotted;">รอบ</div>
					</td>
					<td>
						<div style="border-bottom: 1px dotted;">หมายเหตุ</div>
					</td>
				</tr>
			</tbody>
		</table>
		<table class="bordered" aria-describedby="droppoint-driver">
			<thead>
				<tr>
					<th scope="col">
						<div>#</div>
					</th>
					<th scope="col">
						<div>@lang('admin.bill_no')</div>
					</th>
					<th scope="col">
						<div>@lang('admin.user_code')</div>
					</th>
					<th scope="col">
						<div>T</div>
					</th>
					<th scope="col">
						<div>P</div>
					</th>
					<th scope="col">
						<div>ชื่อผู้รับ</div>
					</th>
					<th scope="col">
						<div>@lang('admin.truck')</div>
					</th>
					<th scope="col">
						<div>@lang('admin.staff')</div>
					</th>
					<th scope="col">
						<div>@lang('admin.remark')</div>
					</th>
				</tr>
			</thead>

			@foreach($bills as $key => $bill)
			<tr>
				<td class="text-right" rowspan="2">
					<div>{{ $key+1 }}</div>
				</td>
				<td class="text-center" style="font-size: 20px;">
					<div>{{ $bill->no }}</div>
				</td>
				<td class="text-center" style="font-size: 20px;">
					<div>{{ $bill->user_code }}</div>
				</td>
				<td class="text-center">
					<div>{{ $bill->counter }}</div>
				</td>
				<td>
					<div style="border-bottom: 1px dotted;height: 25px;"></div>
				</td>
				<td rowspan="2">
					<div style="line-height: 13px;padding-top: 5px;">
						{!!str_replace(' ', '&nbsp;',$bill->shipping_name)!!} &nbsp;
						({!!str_replace(' ', '&nbsp;',$bill->shipping_tel)!!}) <br>
						{!!str_replace(' ', '&nbsp;',$bill->shipping_address)!!}
						ต.{!!str_replace(' ', '&nbsp;',$bill->shippingDistrict->title_th)!!}
						อ.{!!str_replace(' ', '&nbsp;',$bill->shippingAmphur->title_th)!!}
						จ.{!!str_replace(' ', '&nbsp;',$bill->shippingProvince->title_th)!!}
						{{ $bill->shipping_zipcode }}
					</div>
					@if($bill->admin_remark)
					<div style="margin-top: 5px; font-size: 17px;line-height: 11px;background-color: #eee;">** @lang('admin.remark'): {!!str_replace(' ', '&nbsp;',$bill->admin_remark)!!}</div>
					@endif
				</td>

				<td rowspan="2">
					<?php $truck = $bill->truck; ?>
					@if($truck)
					<div style="border-bottom: 1px dotted;height: 25px;" class="text-center">{{$truck->title}}</div>
					@else
					<div style="border-bottom: 1px dotted;height: 25px;"></div>
					@endif
				</td>
				<td rowspan="2">
					@if($bill->driver_admin)
					<div style="border-bottom: 1px dotted;height: 25px;" class="text-center">{{$bill->driver_admin}}</div>
					@else
					<div style="border-bottom: 1px dotted;height: 25px;"></div>
					@endif
					@if($bill->driver2_admin)
					<div style="border-bottom: 1px dotted;height: 25px;" class="text-center">{{$bill->driver2_admin}}</div>
					@else
					<div style="border-bottom: 1px dotted;height: 25px;"></div>
					@endif
				</td>

				<td rowspan="2">
					<div style="border-bottom: 1px dotted;height: 25px;"></div>
					<div style="border-bottom: 1px dotted;height: 25px;"></div>
				</td>

			</tr>
			<tr>
				<td colspan="4" class="text-center" style="padding: 10px 5px;background-color: #eee;">
					{!!'<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($bill->no, 'C128',1.5,18) . '" alt="barcode" />'!!}
				</td>
			</tr>

			@endforeach


		</table>
	</div>


</body>

</html>