<?php

namespace App\Models;

use App\Helpers\ScopeQueries;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    public static $imageBroadcastFolder = 'uploads/chat/broadcast/';
    public static $imageFolder = 'uploads/chat/';
    public static $imageWidth = 300;
    public static $imageHeight = 300;

    protected $fillable = [
        'user_code',
        'message', 'file',
        'readed',
        'created_admin',
        'created_admin_id',
        'created_type',
        'status',
    ];

    protected $appends = ['status_text', 'file_url'];

    public static function statusList()
    {
        $array = [
            'active' => __('messages.active'),
            'inactive' => __('messages.inactive'),
        ];
        return $array;
    }

    public static function conditionStatusList()
    {
        $array = [
            '1' => __('messages.readed'),
            '2' => __('messages.unread'),
            '3' => __('messages.answered'),
        ];
        return $array;
    }

    public static function messages()
    {
        $array = [
            'ขอบคุณค่ะ',
            'สวัสดีค่ะ',
            'รับทราบค่ะ',
            'ออกบิลให้แล้วค่ะ',
            'ค่ะ',
            'ใช่ค่ะ',
            'ได้ค่ะ',
            'ยินดีค่ะ',
            '^^',
            'รบกวนขอเลขแทรคเพื่อตรวจสอบค่ะ',
            'ต้องขออภัยด้วยค่ะ',
            'ระยะเวลาขนส่งจากจีน ทางรถ 5-10 วัน/ทางเรือ 15-20 วันค่ะ',
            'คุณลูกค้าจะใช้บริการสั่งซื้อพร้อมนำเข้า หรือ นำเข้าอย่างเดียวคะ?',
            'กรณีต้องการสั่งสินค้าพร้อมนำเข้า ทางบริษัทขอแนะนำบริษัทในเครือ <a href="http://www.tcatmall.com" target="_blank">www.tcatmall.com</a>, <a href="https://www.alibabathailand.com" target="_blank">www.alibabathailand.com</a>, <a href="http://taobaocargo.com" target="_blank">www.taobaocargo.com</a> ค่ะ  ทั้ง 3 บริษัทให้บริการสั่งซื้อ + นำเข้าค่ะ ส่วน TCATCARGO จะเป็นบริการขนส่ง(นำเข้า/ส่งออก) โดยลูกค้าเป็นคนติดต่อกับร้านค้าเองค่ะ',
            'ลูกค้าสมัครสมาชิก และนำที่อยู่โกดังที่จีนจากหน้าเว็บของบริษัทให้ร้านค้าเพื่อส่งสินค้าเข้าโกดัง โดยให้เขียนรหัสลูกค้าข้างกล่องให้ชัดเจน(เพื่อความสะดวกในการหาสินค้า) ถ้าต้องการส่งทางรถ ให้เขียนรหัสลูกค้าและตามด้วย(K) เช่น SA8238(K), ถ้าส่งทางเรือ(M) เช่น SA8238(M) เมื่อร้านค้าส่งสินค้าแล้ว ให้ลูกค้าขอเลขแทรคสินค้า(เลขพัสดุ)จากร้านค้า และเพิ่มเลขแทรคสินค้าเข้าระบบลูกค้าหรือแจ้งเจ้าหน้าที่ เมื่อสินค้าถึงโกดังไทยลูกค้าจะได้รับข้อความแจ้งเตือนให้ออกบิลขนส่งภายในประเทศหรือติดต่อรับที่สาขาค่ะ',
            'เพื่อความสะดวก คุณลูกค้าสามารถดำเนินการแจ้งแทรค,ออกบิล,เติมเครดิต ผ่านระบบได้ด้วยตนเอง โดยสามารถดูวิธีการใช้งานระบบจากวีดีโอ  <a href="https://youtu.be/fw8XF36RnjM" target="_blank">ดูวีดีโอ</a>',
        ];
        return $array;
    }

    public function getStatusTextAttribute()
    {
        return collect(self::statusList())->get($this->status);
    }

    public function getMessageStatusAttribute()
    {
        return collect(self::statusMessage())->get($this->readed);
    }

    public function getFileUrlAttribute($handle = null)
    {
        $file = ($handle) ? $handle : $this->file;

        if (!$file) {
            return null;
        }

        $newPath = ($this->user_code)
        ? self::$imageFolder . $this->user_code . '/'
        : self::$imageBroadcastFolder;

        return env('APP_MEDIA_URL') . '/' . $newPath . $file;
    }

    /**
     * relation
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'code', 'user_code');
    }

    public static function scopeSearch($query, $conditions)
    {
        return $query->where(function ($query) use ($conditions) {
            ScopeQueries::queryUserCode($query, 'chats.user_code');
            if (@$conditions['id']) {
                $query->where('chats.id', $conditions['id']);
            }
            if (@$conditions['search']) {
                $query->where('chats.user_code', 'LIKE', "%{$conditions['search']}%")
                    ->orWhere('chats.message', 'LIKE', "%{$conditions['search']}%");
            }
            if (@$conditions['message']) {
                $query->where('chats.message', 'LIKE', "%{$conditions['message']}%");
            }
            if (@$conditions['user_code']) {
                $query->where('chats.user_code', 'LIKE', "%{$conditions['user_code']}%");
            }
            if (@$conditions['status']) {
                if ($conditions['status'] == 1) {
                    $query->whereCreatedType('user');
                    $query->whereReaded(1);
                }
                if ($conditions['status'] == 2) {
                    $query->whereCreatedType('user');
                    $query->whereReaded(0);
                }
                if ($conditions['status'] == 3) {
                    $query->whereCreatedType('admin');
                }
            }
            if (@$conditions['date_start'] && @$conditions['date_end']) {
                ScopeQueries::scopeDateRangeRaw($query, 'chats.created_at', @$conditions['date_start'], @$conditions['date_end']);
            }
        });
    }

    public static function scopeExcepBroadcast($query)
    {
        $query->where('chats.created_type', '!=', 'broadcast');
    }

    public static function scopeBroadcastOnly($query)
    {
        $query->whereNull('user_code');
        $query->whereCreatedType('broadcast');
    }

    public static function scopeBroadcastSearch($query, $conditions)
    {
        if (@$conditions['message']) {
            $query->where('message', 'like', '%' . $conditions['message'] . '%');
        }
        if (@$conditions['date_start']) {
            $query->whereRaw('DATE(created_at) >= "' . $conditions['date_start'] . '"');
        }
        if (@$conditions['date_end']) {
            $query->whereRaw('DATE(created_at) <= "' . $conditions['date_end'] . '"');
        }
        if (@$conditions['status']) {
            $query->where('status', $conditions['status']);
        }
    }
}
