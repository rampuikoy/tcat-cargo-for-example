<?php

namespace App\Http\Requests\ShippingMethod;

use App\Http\Requests\FormRequest;

class StoreShippingMethodRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.oversea-shipping-method.store');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_th'          => 'required',
            'title_en'          => 'required',
            'title_cn'          => 'required',
            'status'            => 'required|in:active,inactive',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'title_th'          => __('form.shipping_method.title_th'),
            'title_en'          => __('form.shipping_method.title_en'),
            'title_cn'          => __('form.shipping_method.title_cn'),
            'status'            => __('form.shipping_method.status')
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
