<?php

namespace App\Repositories;

use App\Helpers\Util;
use App\Imports\CommissionImport;
use App\Interfaces\CommissionInterface;
use App\Models\Bill;
use App\Models\Commission;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use App\Traits\UploadTrait;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class CommissionRepository implements CommissionInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait, UploadTrait;

    const MINUTE_CACHE          = 1;

    public function __construct(Commission $commission, Bill $bill)
    {
        $this->commission        = $commission;
        $this->bill              = $bill;
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = commissionInterface::DEFAULT_LIMIT, $sorting = commissionInterface::DEFAULT_SORTING, $ascending = commissionInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $commissions     = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
                            return $this->commission::select($columns)
                                ->search($conditions)
                                ->orderBy($sorting, $ascending)
                                ->skip($offset)
                                ->limit($limit)
                                ->get();
                        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.commission')]), $commissions);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = commissionInterface::DEFAULT_LIMIT, $sorting = commissionInterface::DEFAULT_SORTING, $ascending = commissionInterface::DEFAULT_ASCENDING)
    {
        $key            = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time           = $this->getTime(self::MINUTE_CACHE);

        $commissions    = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
                            $model = $this->commission::select($columns)
                                ->search($conditions)
                                ->orderBy($sorting, $ascending);

                            if ($page == 1) {
                                return $model->paginate($limit);
                            } else {
                                return $model->skip($limit * ($page - 1))->paginate($limit);
                            }
                        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.commission')]), $commissions);
    }

    public function dropdownList()
    {
        $key        = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $dropdown   = Cache::remember($key, $time, function () {
                            return [];
                        });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function findById($id, array $columns = ['*'])
    {
        $commission = $this->findByIdMethod($id, $columns);
        if (!$commission) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.commission'), 'id' => $id]));
        }
        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.commission')]), $commission);
    }

    public function store(array $data)
    {
        // stranr transction
        DB::beginTransaction();
        try {
            $import         = new CommissionImport();
            $import->import($data['file']);

            $normal_profit  = $import->sheet1->getProfit();
            $custom_profit  = $import->sheet2->getProfitCustom();
            $filename       = $this->upload($data['file'], $this->commission::$fileFolder, Util::generateFilename('commission', 'xlsx'));
            // set data for insert
            $tmp_create     = [
                                'file'              => $filename,
                                'username'          => $import->sheet1->getSaleName(),
                                'start_date'        => $import->sheet1->getDateStart(),
                                'end_date'          => $import->sheet1->getDateEnd(),
                                'total'             => $import->sheet3->getTotal(),
                                'admin_id'          => $data['admin_id'],
                                'remark'            => $data['remark'],
                                'normal_income'     => $normal_profit['total_user_expense'],
                                'normal_profit'     => $normal_profit['total_company_revenue'],
                                'normal_commission' => $normal_profit['total_sale_revenue'],
                                'custom_income'     => $custom_profit['total_user_expense'],
                                'custom_profit'     => $custom_profit['total_company_revenue'],
                                'custom_commission' => $custom_profit['total_sale_revenue'],
                                'total_commission'  => ($normal_profit['total_sale_revenue'] + $custom_profit['total_sale_revenue'])
                            ];
                            
            $commission     = $this->commission::create($tmp_create);
            $commission_id  = $commission->id;

            if (!$commission_id) {
                throw new \Exception('commission not create');
            }

            // get bill commission to array
            $tmp_bill       = collect([$import->sheet1->getBills(), $import->sheet2->getBillCustoms()])
                                ->reduceWithKeys(function ($current, $item, $key) {
                                        $type = $key === 'sheet1' ? 'normal' : 'custom';
                                        $tmp_bill = collect($item)
                                            ->filter(function ($commission) {
                                                return !empty($commission['no']) && $commission['sale_revenue'] > 0;
                                            })
                                            ->map(function ($commission) use ($type) {
                                                return [
                                                    'type'          => $type,
                                                    'code'          => $commission['no'],
                                                    'bill_id'       => $commission['bill_id'],
                                                    'commission'    => $commission['sale_revenue'],
                                                ];
                                            })
                                            ->toArray();
                                        return $current->concat($tmp_bill);
                                }, collect([]));

            // update bill relation
            $tmp_bill->each(function ($item) use ($commission_id) {
                $this->bill::whereId($item['bill_id'])->update([
                        'commission_id'     => $commission_id,
                        'commission_amount' => $item['commission'],
                    ]);
                app('App\Repositories\BillRepository')->clearCacheTagById($item['bill_id']);
            });

            DB::commit();
            $this->clearCacheTags();

            return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.commission')]), $commission);
        } catch (\Throwable $th) {
            // rollback insert sql
            DB::rollBack();
            return $this->coreResponse(500, $th->getMessage() ?? __('messages.not_create_data', ['data' => __('messages.commission')]), $th->getTrace());
        }
    }

    private function findByIdMethod($id, array $columns = ['*'])
    {
        $key            = $this->getCacheKey(null, $id);
        $time           = $this->getTime(self::MINUTE_CACHE);

        $commission     = Cache::tags([
                                $this->getClassPrefix(),
                                $this->getFindByIdWithClassPrefix(),
                                $this->getIdWithClassPrefix($id),
                            ])->remember($key, $time, function () use ($id, $columns) {
                                return $this->commission::with([
                                    'bills' => function ($query) {
                                        return $query->select('id', 'user_code', 'status', 'total_price', 'commission_id', 'commission_amount');
                                    }])->select($columns)->find($id);
                            });

        return $commission;
    }

    private function clearCacheTags()
    {
        Cache::tags([
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix()
        ])
        ->flush();
    }
}
