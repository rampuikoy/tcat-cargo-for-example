<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class ArticlePolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.article.index');
    }

    public function show(Admin $admin)
    {
        return $admin->hasAnyPermission(['backend.article.show', 'backend.article.update']);
    }

    public function store(Admin $admin)
    {
        return $admin->can('backend.article.store');
    }

    public function update(Admin $admin)
    {
        return $admin->can('backend.article.update');
    }

    public function destroy(Admin $admin)
    {
        return $admin->can('backend.article.destroy');
    }
}
