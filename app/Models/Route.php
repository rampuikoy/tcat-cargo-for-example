<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    //
    protected $fillable = [
        'method', 'ability', 'action', 'guard_name'
    ];

    public function scopeGuardAdmin($query){
        $query->where('guard_name', 'admins');
    }
    
    public function scopeOfBackend($query)
    {
        return $query->where('ability', 'LIKE', "%backend.%");
    }

}
