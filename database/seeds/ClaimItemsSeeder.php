<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ClaimItems;

class ClaimItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ClaimItems::factory(20)->create();
    }
}
