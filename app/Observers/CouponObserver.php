<?php

namespace App\Observers;

use App\Models\Coupon;
use App\Observers\BaseObserver;

class CouponObserver extends BaseObserver
{
    /**
     * Handle the Claims "created" event.
     *
     * @param  \App\Models\Claims  $claims
     * @return void
     */
    public function creating(Coupon $coupon)
    {
        $this->stampCreated($coupon);
    }

    /**
     * Handle the Claims "updated" event.
     *
     * @param  \App\Models\Claims  $claims
     * @return void
     */
    public function updating(Coupon $coupon)
    {
        $this->stampUpdated($coupon);
    }
}
