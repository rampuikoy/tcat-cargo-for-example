<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        // Register Interface and Repository in here
        // You must place Interface in first place
        // If you dont, the Repository will not get readed.
        $this->app->bind(
            'App\Interfaces\AuthInterface',
            'App\Repositories\AuthRepository'
        );

        $this->app->bind(
            'App\Interfaces\UserInterface',
            'App\Repositories\UserRepository'
        );

        $this->app->bind(
            'App\Interfaces\UserExcelInterface',
            'App\Repositories\UserExcelRepository'
        );

        $this->app->bind(
            'App\Interfaces\AdminInterface',
            'App\Repositories\AdminRepository'
        );

        $this->app->bind(
            'App\Interfaces\ArticleInterface',
            'App\Repositories\ArticleRepository'
        );

        $this->app->bind(
            'App\Interfaces\PermissionInterface',
            'App\Repositories\PermissionRepository'
        );

        $this->app->bind(
            'App\Interfaces\RoleInterface',
            'App\Repositories\RoleRepository'
        );

        $this->app->bind(
            'App\Interfaces\ChatInterface',
            'App\Repositories\ChatRepository'
        );

        $this->app->bind(
            'App\Interfaces\UploadInterface',
            'App\Repositories\UploadRepository'
        );

        $this->app->bind(
            'App\Interfaces\ProvinceInterface',
            'App\Repositories\ProvinceRepository'
        );

        $this->app->bind(
            'App\Interfaces\AmphurInterface',
            'App\Repositories\AmphurRepository'
        );

        $this->app->bind(
            'App\Interfaces\DistrictInterface',
            'App\Repositories\DistrictRepository'
        );

        $this->app->bind(
            'App\Interfaces\ZipcodeInterface',
            'App\Repositories\ZipcodeRepository'
        );

        $this->app->bind(
            'App\Interfaces\BankAccountInterface',
            'App\Repositories\BankAccountRepository'
        );

        $this->app->bind(
            'App\Interfaces\BankCompanyInterface',
            'App\Repositories\BankCompanyRepository'
        );

        $this->app->bind(
            'App\Interfaces\UserAddressInterface',
            'App\Repositories\UserAddressRepository'
        );
        $this->app->bind(
            'App\Interfaces\ProductTypeInterface',
            'App\Repositories\ProductTypeRepository'
        );
        $this->app->bind(
            'App\Interfaces\RateInterface',
            'App\Repositories\RateRepository'
        );

        $this->app->bind(
            'App\Interfaces\ShippingMethodInterface',
            'App\Repositories\ShippingMethodRepository'
        );

        $this->app->bind(
            'App\Interfaces\WarehouseChineseInterface',
            'App\Repositories\WarehouseChineseRepository'
        );

        $this->app->bind(
            'App\Interfaces\ThaiShippingMethodInterface',
            'App\Repositories\ThaiShippingMethodRepository'
        );
        $this->app->bind(
            'App\Interfaces\SeoInterface',
            'App\Repositories\SeoRepository'
        );

        $this->app->bind(
            'App\Interfaces\EmailQueueInterface',
            'App\Repositories\EmailQueueRepository'
        );

        $this->app->bind(
            'App\Interfaces\ExchangeRateInterface',
            'App\Repositories\ExchangeRateRepository'
        );

        $this->app->bind(
            'App\Interfaces\SmsQueueInterface',
            'App\Repositories\SmsQueueRepository'
        );

        $this->app->bind(
            'App\Interfaces\ReportInterface',
            'App\Repositories\ReportRepository'
        );
        $this->app->bind(
            'App\Interfaces\ChatInterface',
            'App\Repositories\ChatRepository'
        );

        $this->app->bind(
            'App\Interfaces\TruckInterface',
            'App\Repositories\TruckRepository'
        );

        $this->app->bind(
            'App\Interfaces\FcmQueueInterface',
            'App\Repositories\FcmQueueRepository'
        );

        $this->app->bind(
            'App\Interfaces\CreditInterface',
            'App\Repositories\CreditRepository'
        );

        $this->app->bind(
            'App\Interfaces\WithdrawInterface',
            'App\Repositories\WithdrawRepository'
        );

        $this->app->bind(
            'App\Interfaces\TrackingExcelInterface',
            'App\Repositories\TrackingExcelRepository'
        );

        $this->app->bind(
            'App\Interfaces\TrackingInterface',
            'App\Repositories\TrackingRepository'
        );

        $this->app->bind(
            'App\Interfaces\WarehouseInterface',
            'App\Repositories\WarehouseRepository'
        );

        $this->app->bind(
            'App\Interfaces\TopupInterface',
            'App\Repositories\TopupRepository'
        );

        $this->app->bind(
            'App\Interfaces\BillInterface',
            'App\Repositories\BillRepository'
        );

        $this->app->bind(
            'App\Interfaces\ThaiShippingAreaPriceInterface',
            'App\Repositories\ThaiShippingAreaPriceRepository'
        );

        $this->app->bind(
            'App\Interfaces\ThaiShippingWeightPriceInterface',
            'App\Repositories\ThaiShippingWeightPriceRepository'
        );

        $this->app->bind(
            'App\Interfaces\ThaiShippingWeightSizePriceInterface',
            'App\Repositories\ThaiShippingWeightSizePriceRepository'
        );

        $this->app->bind(
            'App\Interfaces\PaymentInterface',
            'App\Repositories\PaymentRepository'
        );

        $this->app->bind(
            'App\Interfaces\CommissionRateInterface',
            'App\Repositories\CommissionRateRepository'
        );

        $this->app->bind(
            'App\Interfaces\CommissionInterface',
            'App\Repositories\CommissionRepository'
        );

        $this->app->bind(
            'App\Interfaces\ClaimItemsInterface',
            'App\Repositories\ClaimItemsRepository'
        );

        $this->app->bind(
            'App\Interfaces\ClaimsInterface',
            'App\Repositories\ClaimsRepository'
        );

        $this->app->bind(
            'App\Interfaces\NoCodeInterface',
            'App\Repositories\NoCodeRepository'
        );

        $this->app->bind(
            'App\Interfaces\TripInterface',
            'App\Repositories\TripRepository'
        );

        $this->app->bind(
            'App\Interfaces\NoCodeExcelInterface',
            'App\Repositories\NoCodeExcelRepository'
        );

        $this->app->bind(
            'App\Interfaces\PointInterface',
            'App\Repositories\PointRepository'
        );

        $this->app->bind(
            'App\Interfaces\PointWithdrawInterface',
            'App\Repositories\PointWithdrawRepository'
        );

        $this->app->bind(
            'App\Interfaces\CouponInterface',
            'App\Repositories\CouponRepository'
        );

        $this->app->bind(
            'App\Interfaces\ProductInterface',
            'App\Repositories\ProductRepository'
        );

        $this->app->bind(
            'App\Interfaces\PointOrderInterface',
            'App\Repositories\PointOrderRepository'
        );

        $this->app->bind(
            'App\Interfaces\BroadcastInterface',
            'App\Repositories\BroadcastRepository'
        );
    }
}
