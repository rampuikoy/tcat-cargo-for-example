<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTopupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topups', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->datetime('approved_at')->nullable();
            $table->softDeletes();

            $table->enum('method',['transfer','cash','credit','cheque','other','bt_card'])->default('transfer');
            $table->string('code')->nullable();
            $table->date('date')->nullable();
            $table->string('time')->nullable();
            $table->string('file')->nullable();
            $table->float('amount', 8,2);

            $table->string('user_code')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('bank_id')->unsigned()->nullable();
            $table->integer('bank_account_id')->unsigned()->nullable();

            $table->integer('system_bank_account_id')->unsigned()->nullable();
            $table->string('system_bank')->nullable();
            $table->string('system_account')->nullable();

            $table->enum('status',['waiting','approved','cancel'])->default('waiting');
            $table->text('user_remark')->nullable();
            $table->text('admin_remark')->nullable();
            $table->text('system_remark')->nullable();
            $table->boolean('duplicate')->default(0);

            $table->integer('bank_report_id')->unsigned()->nullable()->comment('คนจับคู่');
            $table->integer('match_id')->unsigned()->nullable()->comment('บอทจับคู่');
            $table->tinyInteger('bot')->default(0);

            $table->foreign('user_code')->references('code')->on('users')->onDelete('set null');
            $table->foreign('user_id')->references('id')->on('banks')->onDelete('set null');
            $table->foreign('bank_id')->references('id')->on('banks')->onDelete('set null');
            $table->foreign('bank_account_id')->references('id')->on('bank_accounts')->onDelete('set null');

            $table->foreign('system_bank_account_id')->references('id')->on('bank_accounts')->onDelete('set null');

            // admin name reduce join query
            $table->string('created_admin')->nullable();
            $table->string('updated_admin')->nullable();
            $table->string('approved_admin')->nullable();

            $table->integer('created_admin_id')->unsigned()->nullable();
            $table->integer('updated_admin_id')->unsigned()->nullable();
            $table->integer('approved_admin_id')->unsigned()->nullable();

            $table->foreign('created_admin_id')->references('id')->on('admins')->onDelete('set null');
            $table->foreign('updated_admin_id')->references('id')->on('admins')->onDelete('set null');
            $table->foreign('approved_admin_id')->references('id')->on('admins')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topups');
    }
}
