<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrackingTmall extends Model
{
    use HasFactory;
    protected $connection = 'taobaocoold';
    protected $table = 'cars_tracking';
    protected $fillable = [
        'remark',
    ];
    public $timestamps = false;

    protected $appends = [
        'product_type_text',
        'shipping_method_text',
        'calculate_type_text'
    ];
    public static function productTypeList()
    {
        $array = [
            '1'     => __('messages.trackings.product_type.general'),
            '2'      => __('messages.trackings.product_type.iso_standard'),
            '3'     => __('messages.trackings.product_type.brand'),
            '4'      => __('messages.trackings.product_type.iso'),
        ];
        return $array;
    }
    public static function shippingTypeList()
    {
        $array = [
            '1'     => __('messages.trackings.shipping_method.car'),
            '2'      => __('messages.trackings.shipping_method.boat'),
        ];
        return $array;
    }
    public static function calculateTypeList()
    {
        $array = [
            '1'     => __('messages.trackings.weight'),
            '2'      => __('messages.trackings.cubic'),
        ];
        return $array;
    }
    public function getProductTypeTextAttribute()
    {
        return collect(self::productTypeList())->get($this->basket_type);
    }
    public function getShippingMethodTextAttribute()
    {
        return collect(self::shippingTypeList())->get($this->shipping);
    }
    public function getCalculateTypeTextAttribute()
    {
        return collect(self::calculateTypeList())->get($this->group_weight);
    }


    public function scopeSearch($query, $code)
    {
        return $query->where('tracking', $code);
    }

}
