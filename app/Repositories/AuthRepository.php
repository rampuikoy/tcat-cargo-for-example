<?php

namespace App\Repositories;

use App\Interfaces\AuthInterface;
use App\Models\Admin;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use App\Traits\UploadTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;

class AuthRepository implements AuthInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, UploadTrait, CoreResponseTrait;

    const MINUTE_CACHE = 1;

    public function __construct(Auth $auth)
    {
        $this->auth = $auth::guard('admins');
    }

    public function me()
    {
        $id    = $this->auth->id();
        $auth = $this->findByIdMethod($id);
        return $this->coreResponse(200, __('auth.user_detail'), $auth);
    }

    public function updateProfile(array $data)
    {
        $id = $this->auth->id();
        $auth = $this->findByIdMethod($id);
        if (@$data['photo']) {
            $data['photo'] = $this->uploadBase64($data['photo'], Admin::$imageFolder, '', Admin::$imageWidth, Admin::$imageHeight);
        } else {
            $data['photo'] = $auth->photo;
        }

        $auth->update($data);
        Cache::tags([
            $this->getIdWithClassPrefix($auth->id),
        ])->flush();

        return $this->coreResponse(200, __('auth.update_profile'), $auth);
    }

    public function updatePassword($password)
    {
        $id = $this->auth->id();
        $auth = $this->findByIdMethod($id);

        if (!$auth) {
            return $this->coreResponse(404, __('auth.not_data'));
        }

        $auth->update(['password' => Hash::make($password)]);
        Cache::tags([
            $this->getIdWithClassPrefix($auth->id),
        ])->flush();
        return $this->coreResponse(200, __('auth.update_password'), $auth);
    }

    private function findByIdMethod($id)
    {
        $key        = $this->getCacheKey(null, $id);
        $time       = $this->getTime(self::MINUTE_CACHE);

        $auth       = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($id)
        ])->remember($key, $time, function () {
            return $this->auth->user();
        });

        return $auth;
    }
}
