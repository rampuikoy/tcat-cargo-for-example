<?php

namespace Database\Factories;

use App\Models\Trip;
use App\Models\Admin;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

class TripFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Trip::class;
    const SUF_STATUS = ['1', '2', '3', '4', '5'];
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $admin      = Admin::whereRole('SuperAdmin')->first();
        $trip = [
             'destination'      => $this->faker->text($maxNbChars = 20),
             'customer'         => random_int(1, 1000),
             'depart_date'      => $this->faker->date($format = 'Y-m-d', $max = 'now'),
             'return_date'      => $this->faker->date($format = 'Y-m-d', $max = 'now'),
             'detail'           => $this->faker->text($maxNbChars = 30),
             'sub_total'        => random_int(1000, 10000),
             'total'            => random_int(1000, 10000),
             'admin_remark'     => $this->faker->text($maxNbChars = 10),
             'status'           => Arr::random(self::SUF_STATUS),
             'created_admin'    => $admin->name,
             'created_admin_id' => $admin->id,
        ];

        return $trip;

    }
}
