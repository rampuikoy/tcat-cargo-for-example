@component('mail::message')
# เรียนคุณ {{$point_order->user->name}}
แจ้งบิลแลกสินค้า <span style="color: red">ถูกยกเลิก</span> <br>
รหัสลูกค้า: {{$point_order->user->code}}

@component('mail::table')
| ข้อมูลบิล      |               |
|:-------------|--------------:|
| เลขที่บิล      | {{ $point_order->code }} |
| จำนวนแต้ม    | {{number_format($point_order->total, 2)}} |
| สถานะบิล      | <span style="color: red">ยกเลิก</span> |
@endcomponent

@component('mail::table')
| สินค้า |||||
|:-------------|--------------|--------------:|--------------:|--------------:|
| # | รายการสินค้า | จำนวนแต้ม | จำนวน | จำนวนแต้มรวม |
@foreach ($point_order->products as $index => $product)
| {{ $index + 1 }} | {{$product->title}} | {{number_format($product->point, 2)}} | {{$product->pivot->amount}} | {{number_format($product->pivot->total, 2)}} |
@endforeach
@endcomponent

@endcomponent
