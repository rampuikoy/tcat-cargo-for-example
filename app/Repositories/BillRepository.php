<?php

namespace App\Repositories;

use App\Models\Bill;
use App\Models\User;
use App\Helpers\Util;
use App\Models\Credit;
use App\Models\Setting;
use App\Jobs\JobSendFcm;
use App\Jobs\JobSendSms;
use App\Jobs\JobSendEmail;
use App\Models\Tracking;
use App\Traits\CacheTrait;
use App\Models\UserAddress;
use App\Models\PackingQueue;
use App\Interfaces\BillInterface;
use App\Traits\CoreResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use App\Traits\CalculatorThaiShipping;
use function PHPUnit\Framework\isEmpty;

class BillRepository implements BillInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait, CalculatorThaiShipping;

    const MINUTE_CACHE    = 1;
    const TABLE_RELETIONS = ['thaiShippingMethod', 'shippingProvince', 'shippingAmphur', 'droppoint', 'credit', 'truck', 'trackings'];

    public function __construct(Bill $bill, User $user, Credit $credit, Tracking $tracking, UserAddress $userAddress, PackingQueue $packingQueue, Setting $setting)
    {
        $this->bill             = $bill;
        $this->user             = $user;
        $this->tracking         = $tracking;

        $this->userAddress      = $userAddress;
        $this->packingQueue     = $packingQueue;
        $this->credit           = $credit;
        $this->setting          = $setting;
    }

    public function all(array $columns = ['*'], $sorting = BillInterface::DEFAULT_SORTING, $ascending = BillInterface::DEFAULT_ASCENDING)
    {
        $key    = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time   = $this->getTime(self::MINUTE_CACHE);

        $bills  = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
            return $this->bill::select($columns)
                ->orderBy($sorting, $ascending)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.bill')]), $bills);
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = BillInterface::DEFAULT_LIMIT, $sorting = BillInterface::DEFAULT_SORTING, $ascending = BillInterface::DEFAULT_ASCENDING)
    {
        $key    = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time   = $this->getTime(self::MINUTE_CACHE);

        $bills  = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
            return $this->bill::select($columns)
                ->with(self::TABLE_RELETIONS)
                ->search($conditions)
                ->orderBy($sorting, $ascending)
                ->skip($offset)
                ->limit($limit)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.bill')]), $bills);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = BillInterface::DEFAULT_LIMIT, $sorting = BillInterface::DEFAULT_SORTING, $ascending = BillInterface::DEFAULT_ASCENDING)
    {
        $key    = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time   = $this->getTime(self::MINUTE_CACHE);

        $bills  = Cache::tags([$this->getClassPrefix(), $this->getPaginationWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
            $model = $this->bill::select($columns)
                ->with(self::TABLE_RELETIONS)
                ->search($conditions)
                ->orderBy($sorting, $ascending);
            if ($page == 1) {
                return $model->paginate($limit);
            } else {
                return $model->skip($limit * ($page - 1))->paginate($limit);
            }
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.bill')]), $bills);
    }

    public function findById($id, array $columns = ['*'])
    {
        $bill = $this->findByIdMethod($id);
        if (!$bill) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.bill'), 'id' => $id]));
        }
        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.bill')]), $bill);
    }

    public function dropdownList()
    {
        $key        = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $dropdown   = Cache::remember($key, $time, function () {
            return [
                'droppoint_in'  => $this->bill::droppointInList(),
                'time_format'   => $this->bill::timeFormatList(),
                'status'        => $this->bill::statusList(),
                'condition'     => $this->bill::conditionList(),
                'texe_cancel'   => $this->bill::texeCancelList()
            ];
        });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function store(array $data, object $exchange_rate, object $rate, object $thai_shipping, object $user_address = null)
    {
        $tmp_tracks     = collect($data['trackings'])->filter(function ($track) {
            return $track['select'];
        })->values();
        $track_ids      = $tmp_tracks->pluck('id');

        $track_count    = $this->tracking::whereIn('id', $track_ids)->where('status', '>', 3)->count();
        if ($track_count > 0) {
            return $this->coreResponse(404, __('messages.cannot_create_bill_tracking_status_updated'));
        }
        // stranr transction
        DB::beginTransaction();
        try {
            // trackings
            $tmp_bill['counter']                        = $tmp_tracks->count();
            list($inter_ship, $trackings, $china_cost)  = $this->processCalculateInterShip($tmp_tracks, $rate, $exchange_rate->rate);
            // rate
            $tmp_bill['rate_id']                        = $rate->id;
            $tmp_bill['rate_title']                     = $rate->title;
            // exchange rate
            $tmp_bill['exchange_rate']                  = $exchange_rate->rate;
            // inter shipping
            $tmp_bill['shipping_rate']                  = json_encode($inter_ship);
            $tmp_bill['shipping_min_rate']              = $inter_ship['min_rate'] ?? 0;
            $tmp_bill['shipping_price']                 = $inter_ship['price'];
            $tmp_bill['shipping_weight_price']          = $inter_ship['price_weight'];
            $tmp_bill['shipping_cubic_price']           = $inter_ship['price_cubic'];
            $tmp_bill['total_weight']                   = $inter_ship['total_weight'];
            $tmp_bill['total_cubic']                    = $inter_ship['total_cubic'];
            $tmp_bill['shipping_weight_counter']        = $inter_ship['counter_weight'];
            $tmp_bill['shipping_cubic_counter']         = $inter_ship['counter_cubic'];
            // china cost
            $tmp_bill['cost_box_china']                 = $china_cost['raw_woodbox'];
            $tmp_bill['cost_shipping_china']            = $china_cost['raw_shipping'];
            $tmp_bill['cost_box_thai']                  = $china_cost['price_woodbox'];
            $tmp_bill['cost_shipping_thai']             = $china_cost['price_shipping'];

            $tmp_bill['coupon_id']                      = $data['coupon']['id'] ?? null;
            $tmp_bill['coupon_code']                    = $data['coupon']['code'] ?? null;
            $tmp_bill['coupon_price']                   = $data['coupon']['price'] ?? 0;
            // thai shipping set data
            $tmp_bill['thai_shipping_detail']           = $data['thai_shipping_detail']; //* รายละเอียด ข้อมูลการขนส่งภายในประเทศ
            $tmp_bill['thai_shipping_method_sub_id']    = $thai_shipping->province->id ?? null;
            $tmp_bill['thai_shipping_method_note']      = $thai_shipping->note ?? null;
            $tmp_bill['thai_shipping_method_id']        = $thai_shipping->id;
            $tmp_bill['thai_shipping_method_charge']    = $thai_shipping->charge;
            $tmp_bill['thai_shipping_raw_charge']       = $thai_shipping->charge;
            $tmp_bill['thai_shipping_method_price']     = $this->processCalculateThaiShip($thai_shipping, $tmp_tracks->toArray(), @$user_address->province_id);
            $tmp_bill['thai_shipping_raw_price']        = $this->getThaiShipgRawPrice($thai_shipping, $tmp_bill['thai_shipping_method_price']);
            // use address set data
            if ($thai_shipping->type === 'droppoint') {
                $tmp_bill['user_address_id']            = null;
                $tmp_bill['shipping_name']              = $thai_shipping->title_th;
                $tmp_bill['shipping_tel']               = $thai_shipping->tel;
                $tmp_bill['shipping_address']           = $thai_shipping->address;
                $tmp_bill['shipping_district_code']     = $thai_shipping->district_code;
                $tmp_bill['shipping_amphur_id']         = $thai_shipping->amphur_id;
                $tmp_bill['shipping_province_id']       = $thai_shipping->province_id;
                $tmp_bill['shipping_zipcode']           = $thai_shipping->zipcode;
            } else if ($thai_shipping->type !== 'delivery' || $thai_shipping->calculate_type !== 'api') {
                $tmp_bill['user_address_id']            = $user_address->id;
                $tmp_bill['shipping_name']              = $user_address->name;
                $tmp_bill['shipping_tel']               = $user_address->tel;
                $tmp_bill['shipping_address']           = $user_address->address;
                $tmp_bill['shipping_district_code']     = $user_address->district_code;
                $tmp_bill['shipping_amphur_id']         = $user_address->amphur_id;
                $tmp_bill['shipping_province_id']       = $user_address->province_id;
                $tmp_bill['shipping_zipcode']           = $user_address->zipcode;
                $tmp_bill['latitude']                   = $user_address->latitude ?? null;
                $tmp_bill['longitude']                  = $user_address->longitude ?? null;
                $tmp_bill['thai_shipping_method_sub_title'] = $user_address->province->title_th;
            }
            // form input
            $tmp_bill['user_code']                      = $data['user_code'];
            $tmp_bill['withholding']                    = $data['withholding'];
            $tmp_bill['user_remark']                    = $data['user_remark'];
            $tmp_bill['admin_remark']                   = $data['admin_remark'];

            $tmp_bill['other_charge_title1']            = $data['other_charge1']['title'];
            $tmp_bill['other_charge_price1']            = $data['other_charge1']['price'];
            $tmp_bill['other_charge_title2']            = $data['other_charge2']['title'];
            $tmp_bill['other_charge_price2']            = $data['other_charge2']['price'];

            $tmp_bill['discount_title1']                = $data['discount1']['title'];
            $tmp_bill['discount_price1']                = $data['discount1']['price'];
            $tmp_bill['discount_title2']                = $data['discount2']['title'];
            $tmp_bill['discount_price2']                = $data['discount2']['price'];

            $tmp_bill['sub_total']                      = $tmp_bill['shipping_price'] // ค่าขนส่งระหว่างประเทศ (บาท)
                + $tmp_bill['thai_shipping_method_price'] // + ค่าขนส่งในประเทศ (บาท)
                + $tmp_bill['thai_shipping_method_charge'] // + ค่าธรรมเนียมในประเทศ (บาท)
                + $tmp_bill['cost_box_thai'] // + ค่าตีลังไม้ (บาท)
                + $tmp_bill['cost_shipping_thai'] // + ค่าขนส่งในประเทศจีน (บาท)
                + $tmp_bill['other_charge_price1'] // + ค่าใช้จ่ายเพิ่มเติม 1 (บาท)
                + $tmp_bill['other_charge_price2'] // + ค่าใช้จ่ายเพิ่มเติม 2 (บาท)
                - $tmp_bill['discount_price1'] // - ลดส่วนเพิ่มเติม 1 (บาท)
                - $tmp_bill['discount_price2'] // - ลดส่วนเพิ่มเติม 2 (บาท)
                - $tmp_bill['coupon_price']; // - ลดส่วนคูปอง (บาท)
            //หัก ณ ที่จ่าย
            $tmp_bill['withholding_amount']             = ($tmp_bill['withholding'] && $tmp_bill['sub_total'] >= 1000) ? $tmp_bill['sub_total'] * 0.01 : 0;
            // ยอดสุทธิ
            $tmp_bill['total_price']                    = $tmp_bill['sub_total'] - $tmp_bill['withholding_amount'];

            $admin = auth()->user();
            $tmp_bill['approved_admin']                 = $admin->username;
            $tmp_bill['approved_admin_id']              = $admin->id;
            $tmp_bill['approved_at']                    = date('Y-m-d H:i:s');

            $bill = $this->bill::create($tmp_bill);
            // insert in bill_trackings
            $bill->pivottrackings()->sync($track_ids);

            collect($trackings)->each(function ($track) use ($bill, $exchange_rate) {
                $exist_track        = $this->tracking::find($track['id']);
                $exist_track->update([
                    'status'                => 4,
                    'bill_id'               => $bill->id,
                    'calculate_type'        => $track['calculate_type'],
                    'calculate_rate'        => $track['calculate_rate'],
                    'cubic_price'           => $track['cubic_price'],
                    'cubic_rate'            => $track['cubic_rate'],
                    'weight_price'          => $track['weight_price'],
                    'weight_rate'           => $track['weight_rate'],
                    'price'                 => $track['calculate_type'] === 'weight' ? $track['weight_price'] : $track['cubic_price'],
                    'exchange_rate'         => floatval($exchange_rate->rate),
                    'thai_cost_box'         => floatval($exist_track->china_cost_box) * floatval($exchange_rate->rate),
                    'thai_cost_shipping'    => floatval($exist_track->china_cost_shipping) * floatval($exchange_rate->rate),
                ]);
            });

            $this->clearCacheTags();
            DB::commit();

            return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.bill')]), $bill);
        } catch (\Throwable $th) {
            // rollback insert sql
            DB::rollBack();
            return $this->coreResponse(500, $th->getMessage() ?? __('messages.not_create_data', ['data' => __('messages.bill')]), $th->getTrace());
        }
    }

    public function updateById($id, array $data)
    {
        $bill = $this->findByIdMethod($id);

        if (!$bill) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.bill'), 'id' => $id]));
        }

        $bill->update($data);
        $this->clearCacheTags();
        $this->clearCacheTagById($bill->id);
        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.bill')]), $bill);
    }

    public function optionCreate($userCode)
    {
        $key        = $this->getCacheKey(null, $userCode);
        $time       = $this->getTime(self::MINUTE_CACHE);

        $recents    = Cache::tags([$this->getClassPrefix(), $this->getUserCodePrefix($userCode)])->remember($key, $time, function () use ($userCode) {
            return $this->bill::with('thaiShippingMethod', 'shippingProvince')
                ->whereUserCode($userCode)
                ->orderBy(BillInterface::DEFAULT_SORTING, BillInterface::DEFAULT_ASCENDING)
                ->limit(3)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.bill')]), ['recents' => $recents]);
    }

    public function optionEdit($id)
    {
        $bill = $this->findByIdMethod($id);
        if (!$bill) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.bill'), 'id' => $id]));
        }
        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.bill')]), $bill);
    }

    public function updateStepCreate(array $data)
    {
        $bill = $this->findByIdMethod($data['bill_id']);

        if (!$bill) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.bill'), 'id' => $data['bill_id']]));
        }

        $admin  = auth()->user();
        $bill->fill([
            'status'            => 1,
            'updated_admin'     => $admin->username,
            'updated_admin_id'  => $admin->id,
            'approved_admin'    => $admin->username,
            'approved_admin_id' => $admin->id,
            'approved_at'       => date('Y-m-d H:i:s'),
        ]);

        $bill->save();
        $this->clearCacheTags();
        $this->clearCacheTagById($bill->id);
        app('App\Repositories\UserRepository')->clearCacheTagByUserCode($bill->user_code);

        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.bill')]), $bill);
    }

    public function packProduct($id, array $data)
    {
        $bill   = $this->findByIdMethod($id);
        if (!$bill) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.bill'), 'id' => $id]));
        }

        $track  = collect($data['tracking_id'])
            ->where('select', true)
            ->map(function ($value) use ($bill) {
                $track = app('App\Repositories\TrackingRepository')->findByIdMethod($value['id']);
                if ($track) {
                    $admin = Auth::user();
                    $data = [
                        'packed_admin'          =>  $admin->username,
                        'packed_admin_id'       =>  $admin->id,
                        'packed_at'             =>  date('Y-m-d H:i:s'),
                        'status'                =>  $bill->status == 3 && $track->status == 4 ? 5 : $track->status,
                    ];
                    $track = app('App\Repositories\TrackingRepository')->updateTrackingBill($track->id, $data);
                }
                return $track;
            });

        // count unpack track
        $count  = $this->tracking::whereBillId($id)->whereStatus(4)->get();

        // update bill status
        if ($count->count() == 0 && $bill->status == 3) { // packed all
            $bill->status = 4;
            $bill->save();
            $packed = true;
        } else {
            // count prepaired track
            $count      = $this->tracking->whereBillId($id)->whereNull('packed_at')->get()->count();
            if ($count == 0) {
                // update status in packing queue not paid
                $queue  = $this->packingQueue->where('bill_id', $bill->id)->first();
                if ($queue) {
                    $queue->fill(['status' => 'prepaired'])->save();
                }
                $packed = true;
            } else {
                $packed = false;
            }
        }

        $data           = [
            'packed'    => $packed,
            'data'      => $track,
            'bill'      => $bill,
        ];

        $this->clearCacheTags();
        $this->clearCacheTagById($bill->id);
        app('App\Repositories\UserRepository')->clearCacheTagByUserCode($bill->user_code);
        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.bill_pack')]), $track);
    }

    public function softDeleteById($id)
    {
        $bill = $this->findByIdMethod($id);

        if (!$bill) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.bill'), 'id' => $id]));
        }

        $bill->delete();
        $this->clearCacheTags();
        $this->clearCacheTagById($bill->id);

        return $this->coreResponse(200, __('messages.delete_data', ['data' => __('messages.bill')]), $bill);
    }

    public function findByIdMethod($id, array $columns = ['*'])
    {
        $key    = $this->getCacheKey(null, $id);
        $time   = $this->getTime(self::MINUTE_CACHE);
        $bill   = Cache::tags([$this->getClassPrefix(), $this->getFindByIdWithClassPrefix(), $this->getIdWithClassPrefix($id)])->remember($key, $time, function () use ($id, $columns) {
            return $this->bill::with([
                'pivotTrackings',
                'shippingAmphur',
                'shippingDistrict',
                'shippingProvince',
                'thaiShippingMethod',
                'credits',
                'user',
                'pivotTrackings.shippingMethods',
                'pivotTrackings.warehouse',
                'pivotTrackings.productTypes',
                'images',
                'commissionRate',
                'payments' => function ($query) {
                    return $query->where('status', 'waiting');
                },
                'paid' => function ($query) {
                    return $query->where('status', 'success');
                },
            ])
                ->select($columns)
                ->find($id);
        });

        return $bill;
    }

    public function findUserCodeCouponMethod($userCode, $coupon)
    {
        $key = $this->getCacheKey(null, $userCode);
        $time = $this->getTime(self::MINUTE_CACHE);
        $coupon  = Cache::tags([
            $this->getClassPrefix(),
            $this->getUserCodePrefix($userCode),
        ])->remember($key, $time, function () use ($userCode, $coupon) {
            return $this->bill::select(['*'])->whereUserCode($userCode)->whereCouponCode($coupon)->where('status', '!=', 8)->get();
        });
        return $coupon;
    }

    public function notifyThaiShipping($id, array $data)
    {
        $bill   = $this->findByIdMethod($id);

        if (!$bill) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.bill'), 'id' => $id]));
        }

        $admin                          = auth()->user();
        $bill->thai_shipping_method_id  = $data['thai_shipping_method_id'];
        $bill->thai_shipping_date       = $data['thai_shipping_date'];
        $bill->thai_shipping_time       = $data['thai_shipping_time'];
        $bill->thai_shipping_code       = $data['thai_shipping_code'];
        $bill->thai_shipping_raw_price  = $data['thai_shipping_raw_price'];
        $bill->thai_shipping_raw_charge = $data['thai_shipping_raw_charge'];
        $bill->addon_refund_title       = $data['addon_refund_title'];
        $bill->addon_refund_credit      = $data['addon_refund_credit'];

        if ($bill->status < 5) {
            $bill->status = 5;
            $bill->trackings()->update(['status' => 6, 'thai_out' => $bill->thai_shipping_date]);
            app('App\Repositories\TrackingRepository')->clearCacheTags();
        }

        $bill->thai_shipping_admin      = $admin->username;
        $bill->thai_shipping_admin_id   = $admin->id;
        $bill->thai_shipped_at          = date('Y-m-d H:i:s');
        $bill->save();
        $this->clearCacheTags();
        $this->clearCacheTagById($bill->id);
        app('App\Repositories\UserRepository')->clearCacheTagByUserCode($bill->user_code);

        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.bill_notify')]), $bill);
    }

    public function billTrackingUpdatePay($bill, $admin)
    {
        $bill->updated_admin    = $admin->username;
        $bill->updated_admin_id = $admin->id;
        $bill->paid_at          = date('Y-m-d H:i:s');

        if ($bill->status == 2) {
            $bill->status       = 3;
        }
        $bill->update();
    }

    public function postDriver($id, array $data)
    {
        $bill = $this->findByIdMethod($id);
        if (!$bill) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.bill'), 'id' => $id]));
        }

        if (!$bill) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.bill'), 'id' => $id]));
        }

        if (!is_null($data['driver_admin_id'])) {
            $driver                 = app('App\Repositories\AdminRepository')->findByIdMethod($data['driver_admin_id']);
            $bill->driver_admin     = null;
            $bill->driver_admin_id  = null;

            if (!is_null($driver) && $driver->type == 'driver') {
                $bill->driver_admin     = $driver->name;
                $bill->driver_admin_id  = $driver->id;
            }
        }

        if (!is_null($data['driver2_admin_id'])) {
            $driver                 = app('App\Repositories\AdminRepository')->findByIdMethod($data['driver2_admin_id']);
            $bill->driver2_admin    = null;
            $bill->driver2_admin_id = null;

            if (!is_null($driver) && $driver->type == 'driver') {
                $bill->driver2_admin    = $driver->name;
                $bill->driver2_admin_id = $driver->id;
            }
        }

        $bill->truck_id = $data['truck_id'] ?? null;
        $bill->save();
        $this->clearCacheTags();
        $this->clearCacheTagById($bill->id);
        return $this->coreResponse(200, __('messages.post.driver', ['data' => __('messages.bill')]), $bill);
    }

    public function postBackward($id)
    {
        $bill   = $this->findByIdMethod($id);
        if (!$bill) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.bill'), 'id' => $id]));
        }

        if ($bill->status == 4) {
            $bill->status           = 3;
            $bill->system_remark    = 'ย้อนบิล';
            $bill->update();
        }

        $this->clearCacheTags();
        $this->clearCacheTagById($bill->id);
        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.bill')]), $bill);
    }

    public function postCancel($id, array $data)
    {
        // stranr transction
        DB::beginTransaction();
        try {
            $key    = $this->getCacheKey(null, $id);
            $time   = $this->getTime(self::MINUTE_CACHE);
            $admin  = auth()->user();

            $bill   = Cache::tags([$this->getClassPrefix(), $this->getFindByIdWithClassPrefix(), $this->getIdWithClassPrefix($id)])->remember($key, $time, function () use ($id) {
                return $this->bill::with(self::TABLE_RELETIONS)
                    ->select(['*'])
                    ->whereId($id)
                    ->first();
            });

            if (!$bill) {
                return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.bill'), 'id' => $id]));
            }

            if ($bill->status != 8 && $bill->status < 4) {
                $bill->status           = 8;
                $bill->user_remark      = Util::concatField($bill->user_remark, $data['remark']);
                $bill->system_remark    = Util::concatField($bill->system_remark, 'ยกเลิก(' . $bill->status . ')');
                $bill->succeed_at       = date('Y-m-d H:i:s');
                if ($bill->update()) {
                    // update tracking
                    $tracking_ids = $bill->trackings()->pluck('id')->toArray();
                    app('App\Repositories\TrackingRepository')->billCancelTrackingById($tracking_ids);
                    // refund
                    if ($bill->credits()->count() > 0) {
                        foreach ($bill->credits as $credit) {
                            $user           = app('App\Repositories\UserRepository')->findByCodeMethod($credit->user_code);
                            $before         = $user->credit->total ?? 0;
                            $data           = [
                                'creditable_type'       => 'App\Models\Bill',
                                'creditable_id'         => $bill->id,
                                'user_code'             => $credit->user_code,
                                'amount'                => $credit->amount,
                                'before'                => $credit->before,
                                'type'                  => $credit->type == 'withdraw' ? 'topup' : 'withdraw',
                                'after'                 => $credit->type == 'withdraw' ? $before + $credit->amount : $before - $credit->amount,
                                'approved_at'           => date('Y-m-d'),
                                'approved_admin'        => $admin->username,
                                'approved_admin_id'     => $admin->id,
                            ];
                            app('App\Repositories\CreditRepository')->store($data);
                            app('App\Repositories\UserRepository')->clearCacheTagByUserCode($credit->user_code);
                            // Cancel Payment
                            $paymentID = $bill->payments()->pluck('id');
                            app('App\Repositories\PaymentRepository')->updateById($paymentID[0], ['status' => 'cancel']);
                        }
                        app('App\Repositories\CouponRepository')->clearCouponCodeCache($bill->coupon_code);
                    }
                }
            }
            $this->clearCacheTags();
            $this->clearCacheTagById($bill->id);
            DB::commit();
            return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.bill')]), $bill);
        } catch (\Throwable $th) {
            DB::rollBack();
            return $this->coreResponse(500, $th->getMessage() ?? __('messages.not_update_data', ['data' => __('messages.bill')]), $th->getTrace());
        }
    }

    public function notifyShipped($id)
    {
        $bill = $this->findByIdMethod($id);
        if (!$bill) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.bill'), 'id' => $id]));
        }

        if ($bill->status == 5) {
            $bill->status = 6;
        }
        $bill->update();
        $this->clearCacheTags();
        $this->clearCacheTagById($bill->id);
        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.bill_shipped')]), $bill);
    }

    // Comission Bill
    public function allCommissionByCondition(array $conditions, $custom_rate = false)
    {
        $prefix = $custom_rate ? '.CUSTOM' : '';
        $key    = $this->getCacheKey($this->getUrlWithClassPrefix() . $prefix, $this->getCurrentUrl());
        $time   = $this->getTime(self::MINUTE_CACHE);

        $bills  = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])->remember($key, $time, function () use ($conditions, $custom_rate) {
            return $this->bill::with('commissionRate')
                ->searchCommission($conditions)
                ->searchCustomRate($custom_rate)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.bill')]), $bills);
    }

    public function paginateListCommissionByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = BillInterface::DEFAULT_LIMIT, $sorting = BillInterface::DEFAULT_SORTING, $ascending = BillInterface::DEFAULT_ASCENDING)
    {
        $key    = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time   = $this->getTime(self::MINUTE_CACHE);

        $bills  = Cache::tags([$this->getClassPrefix(), $this->getPaginationWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
            $model = $this->bill::select($columns)
                ->with('thaiShippingMethod', 'shippingProvince', 'commission')
                ->searchCommission($conditions)
                ->orderBy($sorting, $ascending);

            if ($page == 1) {
                return $model->paginate($limit);
            } else {
                return $model->skip($limit * ($page - 1))->paginate($limit);
            }
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.bill')]), $bills);
    }

    public function closeBill($id)
    {
        $bill = $this->findByIdMethod($id);
        if (!$bill) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.bill'), 'id' => $id]));
        }

        $refund_credit      = $bill->calculateColseBillRefund();
        $this->processRefund($bill, $refund_credit);

        $bill->status       = 7;
        $bill->updated_at   = date('Y-m-d H:i:s');
        $bill->succeed_at   = date('Y-m-d H:i:s');
        $bill->save();

        $setting            = $this->setting::first();
        $point              = $bill->shipping_price / floatval($setting->point_rate);
        $bill->point()->create([
            'user_code' => $bill->user_code,
            'type'      => 'topup',
            'amount'    => sprintf('%d', $point),
        ]);
        $this->clearCacheTags();
        $this->clearCacheTagById($bill->id);
        app('App\Repositories\UserRepository')->clearCacheTagByUserCode($bill->user_code);
        return $this->coreResponse(200, __('messages.update_status', ['data' => __('messages.bill_close')]), $bill);
    }

    public function notifyCreate($id, $data)
    {
        $bill = $this->findByIdMethod($id);
        if (!$bill) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.bill'), 'id' => $id]));
        }

        app('App\Repositories\PaymentRepository')->store($data);

        if ($bill->status == 1) {
            if ($bill->credits()->get()->count() > 0) { // user paid
                $bill->status = 3;
            } else {
                $bill->status = 2;
            }
        }
        $bill->save();
        $this->clearCacheTags();
        $this->clearCacheTagById($bill->id);
        app('App\Repositories\UserRepository')->clearCacheTagByUserCode($bill->user_code);
        return $this->coreResponse(200, __('messages.update_status', ['data' => __('messages.bill_notify_create')]), $bill);
    }

    public function postDroppointOut($id, array $data)
    {
        $bill = $this->findByIdMethod($id);
        if (!$bill) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.bill'), 'id' => $id]));
        }
        $admin = Auth::user();
        if ($bill->status == 3) {
            $bill->admin_remark = Util::concatField($bill->admin_remark, $data['admin_remark'] ?? null);
            $bill->update();
            $messages = [
                'no' => $bill->no,
                'status' => $bill->thaiShippingMethod->title
            ];
        } else if ($bill->status == 4 || $bill->status == 5) {
            $bill->admin_remark = Util::concatField($bill->admin_remark, $data['admin_remark'] ?? null);

            if (isset($data['driver_admin_id'])) {

                $driver                 = app('App\Repositories\AdminRepository')->findByIdMethod($data['driver_admin_id']);
                $bill->driver_admin     = null;
                $bill->driver_admin_id  = null;

                if (isset($driver) && $driver->type == 'driver') {
                    $bill->driver_admin     = $driver->name;
                    $bill->driver_admin_id  = $driver->id;
                }
            }

            if (isset($data['driver2_admin_id'])) {

                $driver                 = app('App\Repositories\AdminRepository')->findByIdMethod($data['driver2_admin_id']);
                $bill->driver2_admin    = null;
                $bill->driver2_admin_id = null;

                if (isset($driver) && $driver->type == 'driver') {
                    $bill->driver2_admin    = $driver->name;
                    $bill->driver2_admin_id = $driver->id;
                }
            }
            if (isset($data['truck_id'])) {
                $bill->truck_id = $data['truck_id'] ?? null;
            }
            if ($bill->status == 4) {
                $bill->status                  = 5;
                $bill->thai_shipping_date      = date('Y-m-d');
                $bill->thai_shipping_time      = date('H:i');
                $bill->thai_shipped_at         = date('Y-m-d H:i:s');
                $bill->thai_shipping_admin     = $admin->username;
                $bill->thai_shipping_admin_id  = $admin->id;
                $bill->save();

                $bill->trackings()->update([
                    'status'     => 6,
                    'thai_out'   => $bill->thai_shipping_date
                ]);
            }
            if ($bill->save() && isset($driver)) {
                $sms        = $bill->sms()->create([
                    'status'            => 'queue',
                    'user_code'         => $bill->user->code,
                    'msisdn'            => $bill->user->tel1,
                    'message'           => __('sms.sms_bill_shipping', [
                        'no' => $bill->no,
                        'tel' => $bill->user->tel1,
                        'driver' => $driver->name ?? null,
                    ]),
                    'created_admin'     => $bill->created_admin,
                    'created_admin_id'  => $bill->created_admin_id,
                ]);
                JobSendSms::dispatch($sms->id)->onQueue(env('QUEUE_SMS_PREFIX'));
            }
            $this->clearCacheTags();
            $this->clearCacheTagById($bill->id);
            $messages = [
                'no' => $bill->no,
                'status' => $bill->thaiShippingMethod->title
            ];
        } else {
            return $this->coreResponse(404, __('messages.bill_not_in_4', ['no' => $bill->no, 'status' =>  $bill->current_status]), $bill);
        }
        return $this->coreResponse(200, __('messages.drop_point_status', ['no' => $messages['no'], 'title' =>  $messages['status']]), $bill);
    }

    public function updateDroppointOut($id, array $data)
    {
        $bill = $this->findByIdMethod($id);
        if (!$bill) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.bill'), 'id' => $id]));
        }
        $admin = Auth::user();
        $bill->thai_shipping_code           = $data['thai_shipping_code'];
        $bill->thai_shipping_admin          = $admin->username;
        $bill->thai_shipping_admin_id       = $admin->id;
        $bill->thai_shipped_at              = date('Y-m-d H:i:s');
        if (isset($data['thai_shipping_raw_price']) && $data['thai_shipping_raw_price'] > 0) {
            $bill->thai_shipping_raw_price  = $data['thai_shipping_raw_price'];
        } else {
            $bill->thai_shipping_raw_price  = $bill->thai_shipping_method_price;
        }

        $bill->update();
        $this->clearCacheTags();
        $this->clearCacheTagById($bill->id);
        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.bill')]), $bill);
    }

    public function postDroppointIn($id, array $data)
    {
        $bill = $this->findByIdMethod($id);
        if (!$bill) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.bill'), 'id' => $id]));
        }
        $admin = Auth::user();
        if ($bill->status > 4 && $bill->status != 8) {
            // check id droppoint
            if ($bill->thai_shipping_method_id == $data['droppoint_id']) {
                $bill->droppoint_id         =  $data['droppoint_id'];
                $bill->droppoint_admin      =  $admin->username;
                $bill->droppoint_admin_id   =  $admin->id;
                $bill->droppoint_at         =  date('Y-m-d H:i:s');
                $user = $bill->user;
                $this->jobSentAll($bill, $user);
            } else {
                return $this->coreResponse(404, __('messages.droppoint_incorrect', ['code' => $bill->no, 'method' => $bill->thaiShippingMethod->title]));
            }
        } else {
            return $this->coreResponse(404, __('messages.bill_in_process', ['code' => $bill->no]));
        }
        $bill->update();
        $this->clearCacheTags();
        $this->clearCacheTagById($bill->id);
        return $this->coreResponse(200, __('messages.droppoint_in_success', ['code' => $bill->no]), $bill);
    }

    public function updateDroppointIn(array $data)
    {
        $admin = Auth::user();
        $bill  = collect($data['bills'])
            ->map(function ($id) use ($admin) {
                $bill = $this->findByIdMethod($id);
                if (!$bill) {
                    return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.bill'), 'id' => $id]));
                }
                if ($bill->status > 4 && $bill->status != 8) {
                    $bill->droppoint_id         =  $bill->thai_shipping_method_id;
                    $bill->droppoint_admin      =  $admin->username;
                    $bill->droppoint_admin_id   =  $admin->id;
                    $bill->droppoint_at         =  date('Y-m-d H:i:s');
                    $user = $bill->user;
                    $this->jobSentAll($bill, $user);
                } else {
                    return $this->coreResponse(404, $bill->no);
                }
                $bill->update();
                $this->clearCacheTags();
                $this->clearCacheTagById($bill->id);
                return $bill;
            });

        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.bill')]), $bill);
    }

    public function jobSentAll($bill, $user)
    {
        $email      = $bill->emails()->create([
            'mailable_class'    => 'App\Mail\Bill\BillDropped',
            'status'            => 'queue',
            'to'                => $user->email,
            'created_admin'     => $bill->created_admin,
            'created_admin_id'  => $bill->created_admin_id,
        ]);
        JobSendEmail::dispatch($email->id)->onQueue(env('QUEUE_EMAIL_PREFIX'));
        $method = $bill->thaiShippingMethod->title;
        $sms        = $bill->sms()->create([
            'status'            => 'queue',
            'user_code'         => $user->code,
            'msisdn'            => $user->tel1,
            'message'           => __('sms.sms_bill_dropped', [
                'no'     => $bill->no,
                'method' => $method
            ]),
            'created_admin'     => $bill->created_admin,
            'created_admin_id'  => $bill->created_admin_id,
        ]);
        JobSendSms::dispatch($sms->id)->onQueue(env('QUEUE_SMS_PREFIX'));
        $fcm        = $bill->fcm()->create([
            'status'            => 'queue',
            'user_code'         => $bill->user_code,
            'title'             => __('fcm.bill.bill_dropped_title', ['no' => $bill->no]),
            'message'           => __('fcm.bill.bill_dropped_message', ['no' => $bill->no, 'method' => $method]),
        ]);
        JobSendFcm::dispatch($fcm->id)->onQueue(env('QUEUE_FCM_PREFIX'));
    }
    public function signature($id, array $data)
    {
        // stranr transction
        DB::beginTransaction();
        try {
            $bill   = $this->findByIdMethod($id);
            if (!$bill) {
                return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.bill'), 'id' => $id]));
            }
            if (in_array($bill->status, [4, 5]) && !$bill->sign_image) {
                $signImage          = app('App\Repositories\UploadRepository')->store([
                    'type'              => 'image',
                    'sub_path'          => "sign",
                    'relatable_type'    => 'App\Models\Bill',
                    'relatable_id'      => $id,
                    'file_path'         => $data['file_path'],
                ]);

                $bill->sign_image               = 'uploads/bill/sing/' . $signImage->results->file_path;
                $bill->latitude                 = $data['latitude'] ?? null;
                $bill->longitude                = $data['longitude'] ?? null;
                $bill->thai_shipping_date       = date('Y-m-d');
                $bill->thai_shipping_time       = date('H:i');
                $bill->status                   = in_array($bill->status, [4, 5]) ? 6 : $bill->status;
                $bill->thai_shipping_raw_price  = !$bill->thai_shipping_raw_price ? $bill->thai_shipping_method_price : $bill->thai_shipping_raw_price;
                $bill->thai_shipping_raw_charge = !$bill->thai_shipping_raw_charge ? $bill->thai_shipping_method_charge : $bill->thai_shipping_raw_charge;

                // update tracking
                $tracking_ids = $bill->trackings()->pluck('id')->toArray();
                app('App\Repositories\TrackingRepository')->billUpdateSignatureTrackingById($tracking_ids);
                $bill->update();
                $this->clearCacheTagById($id);
                $this->clearCacheTags();
                DB::commit();
            }
            return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.bill')]), $bill);
        } catch (\Throwable $th) {
            DB::rollBack();
            return $this->coreResponse(500, $th->getMessage() ?? __('messages.not_update_data', ['data' => __('messages.bill')]), $th->getTrace());
        }
    }

    // start private function use in store method //
    private function getThaiShipgRawPrice($thai_shipping, $thai_shipping_method_price)
    {
        $thai_shipping_raw_price = 0;
        if ($thai_shipping->type === 'droppoint') {
            $thai_shipping_raw_price = $thai_shipping_method_price;
        }
        if ($thai_shipping->type === 'delivery' || $thai_shipping->calculate_type === 'api') {
            $thai_shipping_raw_price = $thai_shipping_method_price;
        } else if ($thai_shipping->calculate_type === 'area') {
            $thai_shipping_raw_price = $thai_shipping_method_price;
        }
        return $thai_shipping_raw_price;
    }

    private function processCalculateThaiShip($thai_shipping, $trackings, $province_id = null)
    {
        if ($thai_shipping->type === 'droppoint') {
            return $this->getCalculateDroppoint($thai_shipping->id, $trackings);
        }

        $thai_shipping_method_price = null;
        switch ($thai_shipping->calculate_type) {
            case 'weight':
                $thai_shipping_method_price = $this->getCalculatePost($thai_shipping->id, $trackings);
                break;
            case 'weight_size':
                $thai_shipping_method_price = $this->getCalculateKerry($thai_shipping->id, $trackings);
                break;
            case 'area':
                $thai_shipping_method_price = $this->getCalculateNim($thai_shipping->id, $trackings, $province_id);
                break;
            case 'fixed':
                // delivery min price
                $price = $this->getCalculateDroppoint($thai_shipping->id, $trackings);
                $thai_shipping_method_price = ($thai_shipping->min_price > $price) ? $thai_shipping->min_price : $price;
                break;
            default:
                $thai_shipping_method_price = null;
                break;
        }
        return $thai_shipping_method_price;
    }

    private function processCalculateInterShip($trackings, $rate, $exchange_rate)
    {
        $template = [
            'inter_ship'    => [
                'price'             => 0,
                'counter_weight'    => 0,
                'total_weight'      => 0,
                'price_weight'      => 0,
                'counter_cubic'     => 0,
                'total_cubic'       => 0,
                'price_cubic'       => 0,
                'totalWeight'       => 0,
                'totalCubic'        => 0,
                'min_price'         => 0,
                'track_counter'     => [
                    'kg_car_genaral'        => 0,
                    'kg_car_iso'            => 0,
                    'kg_car_brand'          => 0,
                    'kg_ship_genaral'       => 0,
                    'kg_ship_iso'           => 0,
                    'kg_ship_brand'         => 0,
                    'kg_plane_genaral'      => 0,
                    'kg_plane_iso'          => 0,
                    'kg_plane_brand'        => 0,
                    'cubic_car_genaral'     => 0,
                    'cubic_car_iso'         => 0,
                    'cubic_car_brand'       => 0,
                    'cubic_ship_genaral'    => 0,
                    'cubic_ship_iso'        => 0,
                    'cubic_ship_brand'      => 0,
                    'cubic_plane_genaral'   => 0,
                    'cubic_plane_iso'       => 0,
                    'cubic_plane_brand'     => 0,
                ],
                'sum_unit'          => [
                    'kg_car_genaral'        => 0,
                    'kg_car_iso'            => 0,
                    'kg_car_brand'          => 0,
                    'kg_ship_genaral'       => 0,
                    'kg_ship_iso'           => 0,
                    'kg_ship_brand'         => 0,
                    'kg_plane_genaral'      => 0,
                    'kg_plane_iso'          => 0,
                    'kg_plane_brand'        => 0,
                    'cubic_car_genaral'     => 0,
                    'cubic_car_iso'         => 0,
                    'cubic_car_brand'       => 0,
                    'cubic_ship_genaral'    => 0,
                    'cubic_ship_iso'        => 0,
                    'cubic_ship_brand'      => 0,
                    'cubic_plane_genaral'   => 0,
                    'cubic_plane_iso'       => 0,
                    'cubic_plane_brand'     => 0,
                ],
                'sum_price'         => [
                    'kg_car_genaral'        => 0,
                    'kg_car_iso'            => 0,
                    'kg_car_brand'          => 0,
                    'kg_ship_genaral'       => 0,
                    'kg_ship_iso'           => 0,
                    'kg_ship_brand'         => 0,
                    'kg_plane_genaral'      => 0,
                    'kg_plane_iso'          => 0,
                    'kg_plane_brand'        => 0,
                    'cubic_car_genaral'     => 0,
                    'cubic_car_iso'         => 0,
                    'cubic_car_brand'       => 0,
                    'cubic_ship_genaral'    => 0,
                    'cubic_ship_iso'        => 0,
                    'cubic_ship_brand'      => 0,
                    'cubic_plane_genaral'   => 0,
                    'cubic_plane_iso'       => 0,
                    'cubic_plane_brand'     => 0,
                ],
            ],
            'trackings'         => [],
            'china_cost'        => [
                'raw_woodbox'       => 0,
                'raw_shipping'      => 0,
                'price_woodbox'     => 0,
                'price_shipping'    => 0,
            ],
        ];

        $reduce = collect($trackings)->reduce(function ($tmp, $track) use ($rate, $exchange_rate) {
            switch ($track['product_type_id']) {
                case 1:
                    $tmp_product_type = 'genaral';
                    break;
                case 2:
                    $tmp_product_type = 'iso';
                    break;
                case 3:
                    $tmp_product_type = 'brand';
                    break;
                default:
                    $tmp_product_type = null;
                    break;
            }

            switch ($track['calculate_type']) {
                case 'weight':
                    $tmp_cal = 'kg';
                    break;
                case 'cubic':
                    $tmp_cal = 'cubic';
                    break;
                default:
                    $tmp_cal = null;
                    break;
            }

            switch ($track['shipping_method_id']) {
                case 1:
                    $tmp_method = 'car';
                    break;
                case 2:
                    $tmp_method = 'ship';
                    break;
                case 3:
                    $tmp_method = 'plane';
                    break;
                default:
                    $tmp_method = null;
                    break;
            }

            // get select rate
            $track['weight_rate'] = $rate['kg_' . $tmp_method . '_' . $tmp_product_type] ?? 0;
            $track['cubic_rate'] = $rate['cubic_' . $tmp_method . '_' . $tmp_product_type] ?? 0;

            // calculate cubic, weight price
            $track['cubic_price'] = floatval($track['cubic_rate']) * floatval($track['cubic']);
            $track['weight_price'] = floatval($track['weight_rate']) * floatval($track['weight']);

            $FIELD_PREFIX = $tmp_cal . '_' . $tmp_method . '_' . $tmp_product_type;

            if ($track['calculate_type'] === 'weight') {
                $tmp['inter_ship']['sum_unit'][$FIELD_PREFIX] += $track['weight'];
                $tmp['inter_ship']['sum_price'][$FIELD_PREFIX] += $track['weight_price'];
                $tmp['inter_ship']['track_counter'][$FIELD_PREFIX] += 1;

                $track['calculate_rate'] = $track['weight_rate'];
                $track['price'] = $track['weight_price'];

                $tmp['inter_ship']['total_weight'] += $track['weight'];
                $tmp['inter_ship']['price_weight'] += $track['weight_price'];
                $tmp['inter_ship']['totalCubic'] += ($track['cubic']) ? $track['cubic'] : $track['weight'] / 200;
                $tmp['inter_ship']['counter_weight'] += 1;
            } else {
                $tmp['inter_ship']['sum_unit'][$FIELD_PREFIX] += $track['cubic'];
                $tmp['inter_ship']['sum_price'][$FIELD_PREFIX] += $track['cubic_price'];
                $tmp['inter_ship']['track_counter'][$FIELD_PREFIX] += 1;

                $track['calculate_rate'] = $track['cubic_rate'];
                $track['price'] = $track['cubic_price'];

                $tmp['inter_ship']['total_cubic'] += $track['cubic'];
                $tmp['inter_ship']['price_cubic'] += $track['cubic_price'];
                $tmp['inter_ship']['totalCubic'] += $track['cubic'];
                $tmp['inter_ship']['counter_cubic'] += 1;
            }

            $tmp['inter_ship']['totalWeight'] += $track['weight'];
            $tmp['inter_ship']['price'] = $tmp['inter_ship']['price_weight'] + $tmp['inter_ship']['price_cubic'];

            $tmp['china_cost']['raw_woodbox'] += floatval($track['china_cost_box']);
            $tmp['china_cost']['raw_shipping'] += floatval($track['china_cost_shipping']);
            $tmp['china_cost']['price_woodbox'] += floatval($track['china_cost_box']) * floatval($exchange_rate);
            $tmp['china_cost']['price_shipping'] += floatval($track['china_cost_shipping']) * floatval($exchange_rate);

            $tmp['trackings'] = collect($tmp['trackings'])->push($track)->toArray();

            return $tmp;
        }, $template);

        // min rate
        if ($reduce['inter_ship']['price'] < 100) {
            $reduce['inter_ship']['price'] = 100;
            $reduce['inter_ship']['min_rate'] = 1;
        }

        $reduce['inter_ship'] = collect($rate)->merge($reduce['inter_ship'])->toArray();

        return [$reduce['inter_ship'], $reduce['trackings'], $reduce['china_cost']];
    }

    public function fetchListByIds(array $ids = [])
    {
        $key    = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time   = $this->getTime(self::MINUTE_CACHE);

        $bills  = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($ids) {
            return $this->bill::select(['*'])
                ->with(self::TABLE_RELETIONS)
                ->whereIn('id', $ids)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.bill')]), $bills);
    }
    // end private function use in store method //

    private function processRefund($bill, $amount)
    {
        $userCredit = app('App\Repositories\UserRepository')->findByCodeMethod($bill->user_code)->credit;
        if ($amount > 0) { // topup
            $after      = $userCredit->total + $amount;
            $data       = [
                'creditable_type'   => 'App\Models\Bill',
                'creditable_id'     => $bill->id,
                'user_code'         => $bill->user_code,
                'amount'            => $amount,
                'before'            => $userCredit->total,
                'after'             => $after,
                'type'              => 'topup',
                'created_admin'     => 'system',
                'approved_admin'    => 'system',
            ];
            app('App\Repositories\CreditRepository')->store($data);
        } elseif ($amount < 0) { // withdraw
            $after      = $userCredit->total + $amount;
            $data       = [
                'creditable_type'   => 'App\Models\Bill',
                'creditable_id'     => $bill->id,
                'user_code'         => $bill->user_code,
                'amount'            => abs($amount),
                'before'            => $userCredit->total,
                'after'             => $after,
                'type'              => 'withdraw',
                'created_admin'     => 'system',
                'approved_admin'    => 'system',
            ];
            app('App\Repositories\CreditRepository')->store($data);
        }

        if ($bill->addon_refund_credit > 0) { //add on
            $userCredit = $bill->user->credit;
            $data       = [
                'creditable_type'   => 'App\Models\Bill',
                'creditable_id'     => $bill->id,
                'user_code'         => $bill->user_code,
                'amount'            => $bill->addon_refund_credit,
                'before'            => $userCredit->total,
                'after'             => $userCredit->total + $bill->addon_refund_credit,
                'type'              => 'topup',
                'created_admin'     => $bill->thai_shipping_admin,
                'approved_admin'    => $bill->thai_shipping_admin,
            ];
            app('App\Repositories\CreditRepository')->store($data);
        }
    }

    public function findByUserCode($userCode, array $columns = ['*'])
    {
        $bill = $this->findByUserCodeMethod($userCode, $columns);
        if ($bill->isEmpty()) {
            return $this->coreResponse(404, __('messages.no_data_with_user_code', ['data' => __('messages.bill'), 'user_code' => $userCode]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.bill')]), $bill);
    }

    private function findByUserCodeMethod($userCode, $columns = ['*'])
    {
        $key        = $this->getCacheKey(null,  $userCode);
        $time       = $this->getTime(self::MINUTE_CACHE);

        $bill      = Cache::tags([
            $this->getClassPrefix(),
            $this->getUserCodePrefix($userCode),
        ])->remember($key, $time, function () use ($userCode, $columns) {
            return $this->bill::select($columns)
                ->with(self::TABLE_RELETIONS)
                ->whereUserCode($userCode)
                ->get();
        });

        return $bill;
    }

    public function clearCacheTags()
    {
        Cache::tags([
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix(),
            $this->getAllWithClassPrefix(),
        ])->flush();
    }

    public function clearCacheTagById($id)
    {
        Cache::tags([$this->getFindByIdWithClassPrefix($id)])->flush();
    }
}
