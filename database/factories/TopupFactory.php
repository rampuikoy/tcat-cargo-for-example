<?php

namespace Database\Factories;

use App\Models\Admin;
use App\Models\Topup;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;
use Carbon\Carbon;
class TopupFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Topup::class;

    const SUF_METHOD = ['transfer', 'cash', 'cheque', 'credit', 'other'];

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $admin      = Admin::whereRole('SuperAdmin')->first();
        $timestamp  = Carbon::now();

        $topup = [
            'file'              => 'default.png',
            'method'            => Arr::random(self::SUF_METHOD),
            'status'            => 'approved',
            'amount'            => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 1000, $max = 10000),
            'user_remark'       => $this->faker->text($maxNbChars = 30),
            'admin_remark'      => $this->faker->text($maxNbChars = 30),
            'system_remark'     => $this->faker->text($maxNbChars = 30),
            'bank_report_id'    =>2,

            'created_admin'     => $admin->name,
            'updated_admin'     => $admin->name,
            'created_admin_id'  => $admin->id,
            'updated_admin_id'  => $admin->id,

            'date'              => $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'time'              => $this->faker->time($format = 'H:i', $max = 'now'),
            'approved_admin'    => $admin->name,
            'approved_admin_id' => $admin->id,
            'approved_at'       => $timestamp
        ];

        return $topup;
    }
}
