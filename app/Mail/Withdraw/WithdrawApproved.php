<?php

namespace App\Mail\Withdraw;

use App\Models\Withdraw;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WithdrawApproved extends Mailable
{
    use Queueable, SerializesModels;

    public $withdraw;

    public $credit;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Withdraw $withdraw)
    {
        //
        $this->withdraw = $withdraw->load('credit');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to($this->withdraw->user->email)
            ->subject(__('emails.withdraw_approved', ['no' => $this->withdraw->no]))
            ->markdown('emails.withdraw.approved');
    }
}
