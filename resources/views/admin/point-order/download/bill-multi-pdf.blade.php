<!DOCTYPE html>
<html lang="th">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Bill</title>
	{!! Html::style('public/css/pdf.css') !!}
	<link rel="stylesheet" href="https://res.imemo.co/tcatcargo_alpha/misc/css/pdf.css" media="all" />
	<style>
		div {
			line-height: 12px;
		}

		html {
			margin: 30px 20px;
		}
	</style>
</head>

<body>
	@foreach($bills as $key => $bill)
	<div>
		<table aria-describedby="point-order">
			@include('admin.bill.download.partials.bill-header')
			<tr>
				<th colspan="2" scope="col" class="text-center">
					<div class="page-header">บิลแลกสินค้า</div>
				</th>
			</tr>
			<tr>
				<td colspan="2">
					@include('admin.point-order.download.partials.bill-user')
				</td>
			</tr>

			<tr>
				<td colspan="2">
					<table class="bordered" style="margin:10px 0 20px 0;" aria-describedby="bill-point-order">
						<thead style="background: #dfdfdf;">
							<tr>
								<th colspan="6" scope="col">
									<div>รายการสินค้า</div>
								</th>
							</tr>
							<tr>
								<th scope="col" class="text-right" width="30">
									<div>#</div>
								</th>
								<th scope="col">
									<div>สินค้า</div>
								</th>
								<th scope="col" class="text-center">
									<div>รหัสสินค้า</div>
								</th>
								<th scope="col" class="text-right" width="70">
									<div>@lang('admin.amount_point')</div>
								</th>
								<th scope="col" class="text-right" width="70">
									<div>@lang('admin.amount')</div>
								</th>
								<th scope="col" class="text-right" width="70">
									<div>@lang('admin.amount_point')@lang('admin.summary')</div>
								</th>
							</tr>
						</thead>
						<tbody>
							@foreach($bill->products as $key => $product)
							<tr>
								<td class="text-right">
									<div>{{++$key}}</div>
								</td>
								<td>
									<div>{{$product->title}}</div>
								</td>
								<td class="text-center">
									<div>{{$product->code}}</div>
								</td>
								<td class="text-right">
									<div>{{number_format($product->point)}}</div>
								</td>
								<td class="text-right">
									<div>{{$product->pivot->amount}}</div>
								</td>
								<td class="text-right">
									<div>{{number_format($product->pivot->total)}}</div>
								</td>
							</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<td></td>
								<td class="text-right" colspan="4">
									<div>@lang('admin.summary')</div>
								</td>
								<td class="text-right">
									<div>{{number_format($bill->total)}}</div>
								</td>
							</tr>
						</tfoot>

					</table>
				</td>
			</tr>

			<tr>
				<td colspan="2" style="padding-bottom: 10px;">
					@include('admin.point-order.download.partials.bill-shipping')
				</td>
			</tr>



			<tr>
				<td colspan="2" style="padding: 5px 5px 5px 5px;">
					@include('admin.bill.download.partials.bill-contact')
				</td>
			</tr>
			<tr>
				<td class="text-center" style="padding: 10px;">
					{!!'<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($bill->user_code, 'C128',2,30) . '" alt="barcode" />'!!} <br>
					<small>@lang('admin.user_code'):&nbsp;{{$bill->user_code}}</small>
				</td>
				<td class="text-center" style="padding: 10px;">
					{!!'<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($bill->code, 'C128',2,30) . '" alt="barcode" />'!!} <br>
					<small>@lang('admin.bill_no'):&nbsp;{{$bill->code}}</small>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div style="border: 1px solid #333;padding: 3px 5px;">** @lang('admin.admin_remark')&nbsp;: {{ $bill->admin_remark}} </div>
				</td>
			</tr>

		</table>
	</div>



	@if($bills->last() != $bill)
	<div class="page-break"></div>
	@endif
	@endforeach

</body>

</html>