<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Admin;
use App\Models\Upload;
use Illuminate\Support\Facades\File;
use Tymon\JWTAuth\Facades\JWTAuth;

class UploadTest extends TestCase
{
    use WithFaker;
    protected $user;
    protected $upload;
    protected $url = '/api/backend/upload';
    /**
     * A basic feature test example.
     *
     * @return void
     */
    protected function authenticate(string $guard = 'admins')
    {
        $this->user = factory(Admin::class)->create();
        auth()->shouldUse($guard);
        $token = JWTAuth::fromUser($this->user);
        return 'Bearer ' . $token;
    }

    protected function deleteUser()
    {
        $this->user->forceDelete();
    }

    protected function fakeImage()
    {
        $this->upload =  Upload::create([
            'type' => 'image',
            'file_path' => '201026080703010_N8pU5uL4uJMe6kS6mmD0ebnBTszIQsakCPd92Q9I.png',
            'created_admin_id' => $this->user->id,
            'created_admin' => $this->user->name
        ]);
    }


    /** @test */
    public function UploadPostSuccessTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('POST', $this->url, [
            'type' => 'image',
            'file' => File::get(public_path('images/test/testbase64.txt')),
        ]);
        Upload::where('created_admin_id', $this->user->id)->forceDelete();
        $response->assertStatus(200);
        $this->deleteUser();
    }

    /** @test */
    public function UploadPostFailAuthTest()
    {
        $response = $this->json('POST', $this->url);
        $response->assertStatus(401);
    }

    /** @test */
    public function UploadPostFailTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('POST', $this->url, [
            'type' => 'image',
            'file' => '',
        ]);
        $response->assertStatus(422);
        $this->deleteUser();
    }

    /** @test */
    public function UploadDeleteSuccessTest()
    {
        $token = $this->authenticate();
        $this->fakeImage();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('DELETE', "{$this->url}/{$this->upload->id}");
        $response->assertStatus(200);
        // $this->deleteUser();
    }

    /** @test */
    public function UploadDeleteFailTest()
    {
        $response = $this->json('DELETE', "{$this->url}/5");
        $response->assertStatus(401);
    }
}
