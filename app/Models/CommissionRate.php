<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CommissionRate extends Model
{
    use HasFactory;

    protected $table = 'commission_rates';

    protected $fillable = [
        'kg_car_genaral',
        'kg_car_iso',
        'kg_car_brand',
        'kg_ship_genaral',
        'kg_ship_iso',
        'kg_ship_brand',
        'cubic_car_genaral',
        'cubic_car_iso',
        'cubic_car_brand',
        'cubic_ship_genaral',
        'cubic_ship_iso',
        'cubic_ship_brand',
        'admin_remark',
        'bill_id',
        'admin_id',
        'created_admin',
        'updated_admin',
        'created_admin_id',
        'updated_admin_id',
    ];
}
