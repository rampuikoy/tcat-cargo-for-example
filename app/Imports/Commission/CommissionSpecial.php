<?php

namespace App\Imports\Commission;

use App\Models\Bill;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class CommissionSpecial implements ToCollection
{
    protected $commission;

    public function collection(Collection $collection)
    {
        $this->commission = $collection;
    }

    public function getBillCustoms()
    {
        return collect($this->commission)->skip(11)
        ->map(function ($bill_custom) {
            return [
                'index'                 => $bill_custom[0],
                'user_code'             => $bill_custom[1],
                'no'                    => $bill_custom[2],
                'bill_id'               => ltrim($bill_custom['2'], Bill::PREFIX.'0'),
                'user_expense'          => $bill_custom[3] ?? 0,
                'company_revenue'       => $bill_custom[4] ?? 0,
                'sum_price_brand'       => $bill_custom[5] ?? 0,
                'sale_revenue'          => $bill_custom[6] ?? 0,
                'user_shipping_rate'    => $this->mappingUserShippingRate($bill_custom),
                'company_shipping_rate' => $this->mappingCompanyShippingRate($bill_custom),
                'rate'                  => $this->mappingRate($bill_custom)
            ];
        })
        ->values();
    }

    private function mappingUserShippingRate($bill_custom)
    {
        return [
            // Car Genaral KG
            'sum_unit_kg_car_genaral'       => $bill_custom[7] ?? 0,
            'rate_kg_car_genaral'           => $bill_custom[8] ?? 0,
            'sum_price_kg_car_genaral'      => $bill_custom[9] ?? 0,
            // Car Genaral CBM
            'sum_unit_cubic_car_genaral'    => $bill_custom[10] ?? 0,
            'rate_cubic_car_genaral'        => $bill_custom[11] ?? 0,
            'sum_price_cubic_car_genaral'   => $bill_custom[12] ?? 0,
            // Car Iso KG
            'sum_unit_kg_car_iso'           => $bill_custom[13] ?? 0,
            'rate_kg_car_iso'               => $bill_custom[14] ?? 0,
            'sum_price_kg_car_iso'          => $bill_custom[15] ?? 0,
            // Car Iso CBM
            'sum_unit_cubic_car_iso'        => $bill_custom[16] ?? 0,
            'rate_cubic_car_iso'            => $bill_custom[17] ?? 0,
            'sum_price_cubic_car_iso'       => $bill_custom[18] ?? 0,
            // Car Total
            'car_total'                     => $bill_custom[19] ?? 0,
            // Ship Genaral KG
            'sum_unit_kg_ship_genaral'      => $bill_custom[20] ?? 0,
            'rate_kg_ship_genaral'          => $bill_custom[21] ?? 0,
            'sum_price_kg_ship_genaral'     => $bill_custom[22] ?? 0,
            // Ship Genaral CBM
            'sum_unit_cubic_ship_genaral'   => $bill_custom[23] ?? 0,
            'rate_cubic_ship_genaral'       => $bill_custom[24] ?? 0,
            'sum_price_cubic_ship_genaral'  => $bill_custom[25] ?? 0,
            // Ship Iso KG
            'sum_unit_kg_ship_iso'          => $bill_custom[26] ?? 0,
            'rate_kg_ship_iso'              => $bill_custom[27] ?? 0,
            'sum_price_kg_ship_iso'         => $bill_custom[28] ?? 0,
            // Ship Iso CBM
            'sum_unit_cubic_ship_iso'       => $bill_custom[29] ?? 0,
            'rate_cubic_ship_iso'           => $bill_custom[30] ?? 0,
            'sum_price_cubic_ship_iso'      => $bill_custom[31] ?? 0,
            // Ship Total
            'ship_total'                    => $bill_custom[32] ?? 0,
            // Bill Total
            'bill_total'                    => $bill_custom[33] ?? 0,
        ];
    }

    private function mappingCompanyShippingRate($bill_custom)
    {
        return [
            // Car Genaral KG
            'sum_unit_kg_car_genaral'       => $bill_custom[35] ?? 0,
            'rate_kg_car_genaral'           => $bill_custom[36] ?? 0,
            'sum_price_kg_car_genaral'      => $bill_custom[37] ?? 0,
            // Car Genaral CBM
            'sum_unit_cubic_car_genaral'    => $bill_custom[38] ?? 0,
            'rate_cubic_car_genaral'        => $bill_custom[39] ?? 0,
            'sum_price_cubic_car_genaral'   => $bill_custom[40] ?? 0,
            // Car Iso KG
            'sum_unit_kg_car_iso'           => $bill_custom[41] ?? 0,
            'rate_kg_car_iso'               => $bill_custom[42] ?? 0,
            'sum_price_kg_car_iso'          => $bill_custom[43] ?? 0,
            // Car Iso CBM
            'sum_unit_cubic_car_iso'        => $bill_custom[44] ?? 0,
            'rate_cubic_car_iso'            => $bill_custom[45] ?? 0,
            'sum_price_cubic_car_iso'       => $bill_custom[46] ?? 0,
            // Car Total
            'car_total'                     => $bill_custom[47] ?? 0,
            // Ship Genaral KG
            'sum_unit_kg_ship_genaral'      => $bill_custom[48] ?? 0,
            'rate_kg_ship_genaral'          => $bill_custom[49] ?? 0,
            'sum_price_kg_ship_genaral'     => $bill_custom[50] ?? 0,
            // Ship Genaral CBM
            'sum_unit_cubic_ship_genaral'   => $bill_custom[51] ?? 0,
            'rate_cubic_ship_genaral'       => $bill_custom[52] ?? 0,
            'sum_price_cubic_ship_genaral'  => $bill_custom[53] ?? 0,
            // Ship Iso KG
            'sum_unit_kg_ship_iso'          => $bill_custom[54] ?? 0,
            'rate_kg_ship_iso'              => $bill_custom[55] ?? 0,
            'sum_price_kg_ship_iso'         => $bill_custom[56] ?? 0,
            // Ship Iso CBM
            'sum_unit_cubic_ship_iso'       => $bill_custom[57] ?? 0,
            'rate_cubic_ship_iso'           => $bill_custom[58] ?? 0,
            'sum_price_cubic_ship_iso'      => $bill_custom[59] ?? 0,
            // Ship Total
            'ship_total'                    => $bill_custom[60] ?? 0,
            // Bill Total
            'bill_total'                    => $bill_custom[61] ?? 0,
        ];
    }

    private function mappingRate($bill_custom)
    {
        return [
            'kg_car_genaral'                => $bill_custom[63] ?? 0,
            'cubic_car_genaral'             => $bill_custom[64] ?? 0,
            'kg_car_iso'                    => $bill_custom[65] ?? 0,
            'cubic_car_iso'                 => $bill_custom[66] ?? 0,
            'kg_ship_genaral'               => $bill_custom[67] ?? 0,
            'cubic_ship_genaral'            => $bill_custom[68] ?? 0,
            'kg_ship_iso'                   => $bill_custom[69] ?? 0,
            'cubic_ship_iso'                => $bill_custom[70] ?? 0,
        ];
    }

    public function getSaleName()
    {
        return $this->commission[2][0] ?? null;
    }

    public function getDateStart()
    {
        return $this->commission[3][16] ?? null;
    }

    public function getDateEnd()
    {
        return $this->commission[3][19] ?? null;
    }

    public function getProfitCustom()
    {
        return [
            'total_user_expense'        => $this->commission[3][3] ?? 0,
            'total_company_revenue'     => $this->commission[3][4] ?? 0,
            'total_sale_revenue'        => $this->commission[3][6] ?? 0,
        ];
    }
}
