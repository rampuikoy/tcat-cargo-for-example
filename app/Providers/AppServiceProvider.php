<?php

namespace App\Providers;

use App\Models\Admin;
use App\Models\Article;
use App\Models\BankAccount;
use App\Models\Bill;
use App\Models\Chat;
use App\Models\Commission;
use App\Models\CommissionRate;
use App\Models\Credit;
use App\Models\ExchangeRate;
use App\Models\NoCode;
use App\Models\Permission;
use App\Models\ProductType;
use App\Models\Rate;
use App\Models\Role;
use App\Models\Seo;
use App\Models\ShippingMethod;
use App\Models\Topup;
use App\Models\TopupAutopay;
use App\Models\Tracking;
use App\Models\TrackingDetail;
use App\Models\Truck;
use App\Models\Upload;
use App\Models\User;
use App\Models\UserAddress;
use App\Models\WarehouseChinese;
use App\Models\Withdraw;
use App\Models\Payment;
use App\Models\PackingQueue;
use App\Models\Claims;
use App\Models\Point;
use App\Models\PointWithdraw;
use App\Models\Trip;
use App\Models\Coupon;
use App\Models\PointOrder;
use App\Models\Product;
use App\Models\ThaiShippingMethod;
use App\Observers\AdminObserver;
use App\Observers\ArticleObserver; //
use App\Observers\BankAccountObserver;
use App\Observers\BillObserver;
use App\Observers\ChatObserver;
use App\Observers\CommissionObserver;
use App\Observers\CommissionRateObserver;
use App\Observers\CreditObserver;
use App\Observers\ExchangeRateObserver;
use App\Observers\NoCodeObserver;
use App\Observers\PermissionObserver;
use App\Observers\ProductTypeObserver;
use App\Observers\RateObserver;
use App\Observers\RoleObserver;
use App\Observers\SeoObserver;
use App\Observers\ShippingMethodObserver;
use App\Observers\TopupAutopayObserver;
use App\Observers\TopupObserver;
use App\Observers\TrackingDetailObserver;
use App\Observers\TrackingObserver;
use App\Observers\TruckObserver;
use App\Observers\UploadObserver;
use App\Observers\UserAddressObserver;
use App\Observers\UserObserver;
use App\Observers\WarehouseChineseObserver;
use App\Observers\WithdrawObserver;
use App\Observers\PaymentObserver;
use App\Observers\PackingQueueObserver;
use App\Observers\ClaimObserver;
use App\Observers\PointObserver;
use App\Observers\PointWithdrawObserver;
use App\Observers\TripObserver;
use App\Observers\CouponObserver;
use App\Observers\PointOrderObserver;
use App\Observers\ProductObserver;
use App\Observers\ThaiShippingMethodObserver;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Laravel\Dusk\DuskServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // if ($this->app->runningUnitTests()) {
        Schema::defaultStringLength(191);
        // }
        Admin::observe(AdminObserver::class);
        Permission::observe(PermissionObserver::class);
        Role::observe(RoleObserver::class);
        Article::observe(ArticleObserver::class);
        Upload::observe(UploadObserver::class);
        BankAccount::observe(BankAccountObserver::class);
        UserAddress::observe(UserAddressObserver::class);
        User::observe(UserObserver::class);
        ProductType::observe(ProductTypeObserver::class);
        ShippingMethod::observe(ShippingMethodObserver::class);
        WarehouseChinese::observe(WarehouseChineseObserver::class);
        Rate::observe(RateObserver::class);
        Seo::observe(SeoObserver::class);
        ExchangeRate::observe(ExchangeRateObserver::class);
        Chat::observe(ChatObserver::class);
        Truck::observe(TruckObserver::class);
        Credit::observe(CreditObserver::class);
        Withdraw::observe(WithdrawObserver::class);
        Tracking::observe(TrackingObserver::class);
        TrackingDetail::observe(TrackingDetailObserver::class);
        Topup::observe(TopupObserver::class);
        TopupAutopay::observe(TopupAutopayObserver::class);
        Bill::observe(BillObserver::class);
        Payment::observe(PaymentObserver::class);
        PackingQueue::observe(PackingQueueObserver::class);
        CommissionRate::observe(CommissionRateObserver::class);
        Commission::observe(CommissionObserver::class);
        Claims::observe(ClaimObserver::class);
        NoCode::observe(NoCodeObserver::class);
        Trip::observe(TripObserver::class);
        Point::observe(PointObserver::class);
        PointWithdraw::observe(PointWithdrawObserver::class);
        Coupon::observe(CouponObserver::class);
        Product::observe(ProductObserver::class);
        PointOrder::observe(PointOrderObserver::class);
        ThaiShippingMethod::observe(ThaiShippingMethodObserver::class);
    }
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment('local', 'testing') && class_exists(DuskServiceProvider::class)) {
            $this->app->register(DuskServiceProvider::class);
        }
    }
}
