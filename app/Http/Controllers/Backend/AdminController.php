<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreAdminRequest;
use App\Http\Requests\Admin\UpdateAdminRequest;
use App\Http\Requests\Admin\UpdatePasswordRequest;
use App\Interfaces\AdminInterface;
use App\Interfaces\RoleInterface;
use App\Interfaces\ThaiShippingMethodInterface;
use App\Models\Admin;
use Illuminate\Http\Request;

class AdminController extends Controller
{

    protected $adminInterface,  $roleInterface,  $thaiShippingMethodInterface;

    public function __construct(AdminInterface $adminInterface, RoleInterface $roleInterface, ThaiShippingMethodInterface $thaiShippingMethodInterface)
    {
        $this->adminInterface               = $adminInterface;
        $this->roleInterface                = $roleInterface;
        $this->thaiShippingMethodInterface  = $thaiShippingMethodInterface;
    }

    public function index(Request $request)
    {
        $this->authorize('index', Admin::class);
        $columns    = $request->input('columns', ['*']);
        $admins     = $this->adminInterface->all($columns);
        return response()->json($admins, $admins->code);
    }

    public function show(Request $request, $id)
    {
        $this->authorize('show', Admin::class);
        $columns    = $request->input('columns', ['*']);
        $admin      = $this->adminInterface->findById($id, $columns);
        return response()->json($admin, $admin->code);
    }

    public function search(Request $request)
    {
        $this->authorize('index', Admin::class);
        $conditions = $request->only(['username', 'email', 'tel', 'department', 'branch', 'droppoint_id', 'type', 'role', 'status', 'remark']);
        $columns    = $request->input('columns', ['*']);
        $limit      = $request->input('limit', $this->adminInterface::DEFAULT_LIMIT);
        $sorting    = $request->input('order_by', $this->adminInterface::DEFAULT_SORTING);
        $ascending  = $request->input('ascending') == 1 ? 'asc' : 'desc';
        $offset     = $request->input('page', 0);

        if ($request->boolean('pagination')) {
            $page   = $request->input('page', 1);
            $admins = $this->adminInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset = $request->input('page', 0);
            $admins = $this->adminInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($admins, $admins->code);
    }

    public function dropdown()
    {
        $dropdown               = $this->adminInterface->dropdownList();

        if (!$dropdown->error) {
            $roles              = $this->roleInterface->all(['id', 'name']);
            $shipping           = $this->thaiShippingMethodInterface->findManyByType('droppoint', ['id', 'title_th']);
            $dropdown->results  = collect($dropdown->results)
                ->merge([
                    'roles'     => (!$roles->error) ? $roles->results : [],
                    'droppoint' => (!$shipping->error) ? $shipping->results : []
                ]);
        }

        return response()->json($dropdown, $dropdown->code);
    }

    public function store(StoreAdminRequest $request)
    {
        $data = $request->only([
            'name',
            'lastname',
            'email',
            'password',
            'role',
            'photo',
            'username',
            'tel',
            'department',
            'droppoint_id',
            'branch',
            'type',
            'status',
            'regex_user',
            'pattern_user',
            'remark',
        ]);
        $admin = $this->adminInterface->store($data);
        return response()->json($admin, $admin->code);
    }

    public function update(UpdateAdminRequest $request, $id)
    {
        $data = $request->only([
            'name',
            'lastname',
            'email',
            'role',
            'photo',
            'tel',
            'department',
            'droppoint_id',
            'branch',
            'type',
            'status',
            'regex_user',
            'pattern_user',
            'remark',
        ]);
        $admin = $this->adminInterface->updateById($id, $data);
        return response()->json($admin, $admin->code);
    }

    public function updatePassword(UpdatePasswordRequest $request, $id)
    {
        $password = $request->input('password');
        $admin = $this->adminInterface->updatePasswordById($id, $password);
        return response()->json($admin, $admin->code);
    }

    public function destroy($id)
    {
        $this->authorize('destroy', Admin::class);
        $admin = $this->adminInterface->softDeleteById($id);
        return response()->json($admin, $admin->code);
    }
}
