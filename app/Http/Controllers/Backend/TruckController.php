<?php

namespace App\Http\Controllers\Backend;

use App\Models\Truck;
use App\Interfaces\TruckInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\Truck\StoreTruckRequest;
use App\Http\Requests\Truck\UpdateTruckRequest;
use Illuminate\Http\Request;

class TruckController extends Controller
{
    protected $truckInterface;

    public function __construct(TruckInterface $truckInterface)
    {
        $this->truckInterface = $truckInterface;
    }

    public function index(Request $request)
    {
        $this->authorize('index', Truck::class);
        $columns    = $request->input('columns', ['*']);
        $trucks     = $this->truckInterface->all($columns);
        return response()->json($trucks, $trucks->code);
    }

    public function show(Request $request, $id)
    {
        $this->authorize('show', Truck::class);
        $columns    = $request->input('columns', ['*']);
        $truck      = $this->truckInterface->findById($id, $columns);
        return response()->json($truck, $truck->code);
    }

    public function search(Request $request)
    {
        $this->authorize('index', Truck::class);
        $conditions     = $request->only(['search','title','code','status']);
        $columns        = $request->input('columns', ['*']);
        $limit          = $request->input('limit', $this->truckInterface::DEFAULT_LIMIT);
        $sorting        = $request->input('order_by', $this->truckInterface::DEFAULT_SORTING);
        $ascending      = $request->input('ascending') == 1 ? 'asc' : 'desc';

        if ($request->boolean('pagination')) {
            $page       = $request->input('page', 1);
            $trucks     = $this->truckInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset     = $request->input('page', 0);
            $trucks     = $this->truckInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($trucks, $trucks->code);
    }

    public function dropdown()
    {
        $dropdown = $this->truckInterface->dropdownList();
        return response()->json($dropdown, $dropdown->code);
    }

    public function store(StoreTruckRequest $request)
    {
        $data       = $request->only([
                        'title',
                        'code',
                        'status',
                        'remark'
                    ]);
        $truck      = $this->truckInterface->store($data);
        return response()->json($truck, $truck->code);
    }

    public function update(UpdateTruckRequest $request, $id)
    {
        $data       = $request->only([
                        'title',
                        'code',
                        'status',
                        'remark'
                    ]);
        $truck      = $this->truckInterface->updateById($id, $data);
        return response()->json($truck, $truck->code);
    }

    public function destroy($id)
    {
        $this->authorize('destroy', Truck::class);
        $truck      = $this->truckInterface->softDeleteById($id);
        return response()->json($truck, $truck->code);
    }
}
