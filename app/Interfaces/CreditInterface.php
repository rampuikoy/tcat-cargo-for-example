<?php

namespace App\Interfaces;

interface CreditInterface
{
    const DEFAULT_LIMIT = 25;
    const DEFAULT_SORTING = 'id';
    const DEFAULT_ASCENDING = 'desc';

    /**
     * fetch all Credit
     *
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function all(array $columns = ['*'], $sorting = CreditInterface::DEFAULT_SORTING, $ascending = CreditInterface::DEFAULT_ASCENDING);

    /**
     * list Credit
     *
     * @param array $conditions
     * @param array $columns
     * @param int $offset
     * @param int $limit
     *
     * @return \Illuminate\Support\Collection
     */
    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * pagination Credit
     *
     * @param array $conditions
     * @param array $columns
     * @param int $page
     * @param int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $perPage = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * find Credit by id
     *
     * @param $id
     * @param array $columns
     *
     * @return object|null
     */
    public function findById($id, array $columns = ['*']);

    /**
     * get Credit dropdown list
     *
     * @return object|null
     */
    public function dropdownList();

    /**
     * create new Credit
     *
     * @param array $data
     *
     * @return object|null
     */
    public function store(array $data);

    /**
     * update Credit by id
     *
     * @param $id
     * @param array $data
     *
     * @return boolean
     */
    public function updateById($id, array $data);
}
