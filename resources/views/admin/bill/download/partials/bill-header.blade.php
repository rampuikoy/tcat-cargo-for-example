<tr>
	<td colspan="2">
		<table style="margin-bottom: 5px;" aria-describedby="bill-pdf-header">
			<tr>
				<th scope="col"></th>
			</tr>
			<tbody>
				<tr>
					<td width="20%">
						<img src="https://res.imemo.co/tcatcargo_alpha/misc/logo/tcatcargo-small.png" alt="logo-tcatcargo" style="width: 100px;">
					</td>
					<td class="text-right">
						<div style="line-height: 12px;padding-left: 80px;"><strong>บริษัท&nbsp;ทีซีเอที&nbsp;จำกัด</strong><br>
							16&nbsp;ซอยเลี่ยงเมืองนนทบุรี15&nbsp;แยก10&nbsp;ถนนเลี่ยงเมืองนนทบุรี<br>
							ต.บางกระสอ&nbsp;อ.เมื่อง&nbsp;จ.นนทบุรี&nbsp;11000
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</td>
</tr>