<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Amphur extends Model
{
    //
    protected $fillable = [
        'code',
        'title_th',
        'title_en',
        'geo_id',
        'province_id',
    ];

    /**
     * relation
     */
    public function province()
    {
        return $this->belongsTo('App\Models\Province', 'province_id', 'id');
    }
}
