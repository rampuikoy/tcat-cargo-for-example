<?php

namespace App\Interfaces;

interface RateInterface
{
    const DEFAULT_LIMIT = 25;
    const DEFAULT_SORTING = 'id';
    const DEFAULT_ASCENDING = 'desc';

    /**
     * find Rate default
     *
     * @return object|null
     */
    public function getDefault();

    /**
     * find Rate default user
     *
     * @param string $code
     * @param array $columns
     *
     * @return object|null
     */
    public function getDefaultUser(string $code, array $columns = ['*']);

    /**
     * fetch all 1Rate
     *
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function all(array $columns = ['*'], $sorting = RateInterface::DEFAULT_SORTING, $ascending = RateInterface::DEFAULT_ASCENDING);

    /**
     * list 1Rate
     *
     * @param array $conditions
     * @param array $columns
     * @param int $offset
     * @param int $limit
     *
     * @return \Illuminate\Support\Collection
     */
    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * pagination 1Rate
     *
     * @param array $conditions
     * @param array $columns
     * @param int $page
     * @param int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $perPage = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * find Rate by id
     *
     * @param $id
     * @param array $columns
     *
     * @return object|null
     */
    public function findById($id, array $columns = ['*']);

    /**
     * find Rate by admin id
     *
     * @param $id
     * @param array $columns
     *
     * @return object|null
     */
    public function findByAdminId($id, array $columns = ['*']);

    /**
     * get 1Rate dropdown list
     *
     * @return object
     */
    public function dropdownList();

    /**
     * create new 1Rate
     *
     * @param array $data
     *
     * @return object|null
     */
    public function store(array $data);

    /**
     * update 1Rate by id
     *
     * @param $id
     * @param array $data
     *
     * @return object
     */
    public function updateById($id, array $data);

    /**
     * soft delete 1Rate by id
     *
     * @param $id
     *
     * @return object|null
     */
    public function softDeleteById($id);

    /**
     * find Rate by User Code
     *
     * @param $code
     * @param array $columns
     *
     * @return object|null
     */
    public function findByUserCode($code, array $columns = ['*']);

    /**
     * find Rate by User Code
     *
     * @param $code
     * @param array $columns
     *
     * @return object|null
     */
    public function findManyByIdSales($id, array $columns = ['*']);

    /**
     * update Rate by user code
     *
     * @param $id
     * @param array $data
     *
     * @return object
     */
    public function updateByUserCode($id, array $data);

}
