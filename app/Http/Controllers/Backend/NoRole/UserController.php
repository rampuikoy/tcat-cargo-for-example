<?php

namespace App\Http\Controllers\Backend\NoRole;

use App\Http\Controllers\Controller;
use App\Interfaces\UserInterface;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $userInterface;

    public function __construct(UserInterface $userInterface)
    {
        $this->userInterface = $userInterface;
    }

    public function search(Request $request)
    {
        $conditions = $request->only(['username', 'email', 'tel', 'department', 'branch', 'droppoint_id', 'type', 'role', 'status', 'remark']);
        $columns = $request->input('columns', ['*']);
        $limit = $request->input('limit', $this->userInterface::DEFAULT_LIMIT);
        $sorting = $request->input('order_by', $this->userInterface::DEFAULT_SORTING);
        $ascending = $request->input('ascending') == 1 ? 'asc' : 'desc';
        $offset = $request->input('page', 0);
        $users = $this->userInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        return response()->json($users, $users->code);
    }

    public function find(Request $request, $contidion)
    {
        $columns = $request->input('columns', ['*']);
        $user = $this->userInterface->findByUserCodeOrEmail($contidion, $columns);
        return response()->json($user, $user->code);
    }
}
