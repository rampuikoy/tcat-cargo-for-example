<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrackingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trackings', function (Blueprint $table) {
            $table->id();
            $table->string('user_code', 20)->nullable();
            $table->string('code_group', 100)->nullable()->comment('tracking Group');
            $table->string('code', 100)->comment('tracking');
            $table->string('reference_code')->nullable()->comment('รหัสอ้างอิงหน้า tracking');
            $table->double('weight', 6, 2)->nullable();
            $table->smallInteger('width')->unsigned()->nullable();
            $table->smallInteger('height')->unsigned()->nullable();
            $table->smallInteger('length')->unsigned()->nullable();
            $table->double('cubic', 6, 4)->nullable();
            $table->double('dimension', 6, 2)->nullable();
            $table->enum('calculate_type', ['weight', 'cubic'])->default('weight')->comment('การคิดราคา');
            $table->double('calculate_rate', 8, 2)->nullable();
            $table->double('price', 8, 2)->nullable()->comment('ค่าขนส่ง');
            $table->smallInteger('cubic_rate')->unsigned()->nullable()->comment('ค่าขนส่ง/คิว');
            $table->smallInteger('weight_rate')->unsigned()->nullable()->comment('ค่าขนส่ง/กก');
            $table->double('cubic_price', 8, 2)->nullable();
            $table->double('weight_price', 8, 2)->nullable();
            $table->datetime('china_in')->nullable();
            $table->datetime('china_out')->nullable();
            $table->datetime('thai_in')->nullable();
            $table->datetime('thai_out')->nullable();
            $table->text('user_remark')->nullable();
            $table->text('admin_remark')->nullable();
            $table->text('system_remark')->nullable();
            $table->integer('bill_id')->unsigned()->nullable();
            $table->integer('shipping_method_id')->unsigned()->nullable();
            $table->integer('product_type_id')->unsigned()->nullable();
            $table->integer('warehouse_id')->unsigned()->nullable();
            $table->text('warehouse_zone')->nullable()->comment('โซนเก็บของ');
            $table->integer('status')->default(1);
            $table->double('china_cost_box', 8, 2)->nullable()->default(0.00)->comment('ค่าตีลังไม้(หยวน)');
            $table->double('china_cost_shipping', 8, 2)->nullable()->default(0.00)->comment('ค่าส่งที่จีน(หยวน)');
            $table->double('thai_cost_box', 8, 2)->nullable()->default(0.00)->comment('ค่าตีลังไม้(บาท)');
            $table->double('thai_cost_shipping', 8, 2)->nullable()->default(0.00)->comment('ค่าส่งที่จีน(บาท)');
            $table->double('exchange_rate', 4, 2)->nullable()->default(0.00)->comment('อัตราแลกเปลี่ยน');
            $table->integer('created_admin_id')->unsigned()->nullable();
            $table->integer('updated_admin_id')->unsigned()->nullable();
            $table->timestamps();
            $table->string('created_admin')->nullable();
            $table->string('updated_admin')->nullable();
            $table->string('packed_admin')->nullable();
            $table->integer('packed_admin_id')->unsigned()->nullable();
            $table->datetime('packed_at')->nullable();
            $table->string('duplicate_order')->nullable()->comment('ซ้ำกับระบบเก่า');
            $table->integer('duplicate_check')->default(0);
            $table->integer('sort')->nullable();
            $table->softDeletes();
            $table->string('insert_uid', 30)->nullable();
            $table->enum('type', ['clothes', 'bag', 'accessories', 'electronics', 'toy', 'other', 'cosmetics', 'spares', 'shoe'])->nullable();
            $table->text('detail')->nullable();

            // $table->foreign('bill_id')->references('id')->on('bills')->onDelete('set null')->onUpdate('RESTRICT');
            $table->foreign('product_type_id')->references('id')->on('product_types')->onDelete('set null')->onUpdate('RESTRICT');
            $table->foreign('shipping_method_id')->references('id')->on('shipping_methods')->onDelete('set null')->onUpdate('RESTRICT');
            $table->foreign('warehouse_id')->references('id')->on('warehouses')->onDelete('set null')->onUpdate('RESTRICT');
            $table->foreign('user_code')->references('code')->on('users')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('created_admin_id')->references('id')->on('admins')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('updated_admin_id')->references('id')->on('admins')->onDelete('set null')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trackings');
    }
}
