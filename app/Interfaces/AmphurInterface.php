<?php

namespace App\Interfaces;

interface AmphurInterface
{
    const DEFAULT_LIMIT = 25;
    const DEFAULT_SORTING = 'title_th';
    const DEFAULT_ASCENDING = 'asc';

    /**
     * fetch all amphur
     *
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function all(array $columns = ['*'], $sorting = AmphurInterface::DEFAULT_SORTING, $ascending = AmphurInterface::DEFAULT_ASCENDING);

    /**
     * find amphur by id
     *
     * @param $id
     * @param array $columns
     *
     * @return object|null
     */
    public function findById($id, array $columns = ['*']);

    /**
     * find many amphur by province id
     *
     * @param int $id
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function findManyByProvinceId($id, array $columns = ['*']);

}
