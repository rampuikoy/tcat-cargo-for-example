@component('mail::message')
# เรียนคุณ {{$bill->user->name}}
แจ้งอัพเดทสถานะบิลขนส่ง <br>
รหัสลูกค้า: {{$bill->user->code}}

@component('mail::table')
| ข้อมูลบิลขนส่งใหม่   |                   |
|:-------------|--------------:|
| เลขที่บิล | {{ $bill->no }} |
| ยอดที่ต้องชำระ | {{number_format($bill->total_price,2)}} |
| สถานะบิล | <span style="color: #1bc5bd; text-align: left;">รอชำระเงิน</span> |
| จำนวน  | {{ $bill->counter }} ชิ้น|
| การขนส่งภายในประเทศ | <span style="color: #1bc5bd; text-align:left;">{{ $bill->thaiShippingMethod->title }}</span>|
| ชื่อผู้รับ {{ $bill->shipping_name }} / เบอร์โทร: {{$bill->shipping_tel}} |
|ที่อยู่จัดส่ง {{$bill->shipping_address}} ||
| ต.{{@$bill->shippingDistrict->title_th}} อ.{{@$bill->shippingAmphur->title_th}} จ.{{@$bill->shippingProvince->title_th}} {{$bill->shipping_zipcode}}|
@endcomponent
<?php
$credit = ($bill->user->credit)? $bill->user->credit->total : 0 ;
$remain_credit = $credit - $bill->total_price;
?>
@component('mail::table')
| การชำระเงิน   |                        |
|:-------------|--------------:|
| เครดิตคงเหลือ   <span style="font-weight:lighter"> {{number_format($credit, 2)}} บาท</span>|
@if($remain_credit >= 0)
| ลูกค้าสามารถชำระบิลโดยหักจากยอดเครดิตคงเหลือได้ทันที |
| <a href="#" target="_blank"> <span style="font-weight:bold">คลิกชำระบิล</span> </a> เพื่อยืนยันการชำระบิล |
@endif
@if($remain_credit < 0)
| เครดิตคงเหลือของคุณไม่พอ <span style="font-weight:lighter"> กรุณาเติมเงินเข้าระบบ {{number_format($bill->total_price-$credit,2)}} บาท </span>|
| <a href="#" target="_blank"> <span style="font-weight:bold">คลิกแจ้งเติมเงิน</span> </a> เพื่อยืนยันการชำระบิล |
@endif
<br><br>
|**กรุณาตรวจสอบบิลขนส่งให้ครบถ้วน หากรายการ Tracking ยังไม่ครบ หรือข้อมูลการจัดส่งไม่ถูกต้อง|
| <a href="#" target="_blank"> <span style="color: #c51b1b; text-align: left; font-weight:bold;">คลิกยกเลิกบิล</span> </a> เพื่อยกเลิกบิลและออกบิลใหม่ภายหลัง หรือติดต่อฝ่ายบริการลูกค้า|
@endcomponent
@component('mail::table')
| บัญชีธนาคารสำหรับชำระเงิน  |               |               |               |
|:-------------|:--------------:|:--------------:|--------------:|
| ธนาคารกรุงเทพ|(BBL) |บจก.ทีซีเอที จำกัด |924-0-14969-1|
| ธนาคารกสิกรไทย| (KBANK) |บจก.ทีซีเอที จำกัด |033-3-94465-0|
@endcomponent

@endcomponent
