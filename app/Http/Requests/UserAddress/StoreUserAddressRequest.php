<?php

namespace App\Http\Requests\UserAddress;

use App\Http\Requests\FormRequest;

class StoreUserAddressRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.user-address.store');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_code'     => 'required|exists:users,code',
            'name'          => 'required|string',
            'address'       => 'required|string',
            'province_id'   => 'required|exists:provinces,id',
            'amphur_id'     => 'required|exists:amphurs,id',
            'district_code' => 'required|max:6|exists:districts,code',
            'zipcode'       => 'required|max:5',
            'status'        => 'required|in:active,inactive',
            'tel'           => 'sometimes|nullable|string|max:20',
            'admin_remark'  => 'sometimes|nullable'
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'user_code'     => __('form.user_address.user_code'),
            'name'          => __('form.user_address.name'),
            'address'       => __('form.user_address.address'),
            'province_id'   => __('form.user_address.province_id'),
            'amphur_id'     => __('form.user_address.amphur_id'),
            'district_code' => __('form.user_address.district_code'),
            'zipcode'       => __('form.user_address.zipcode'),
            'status'        => __('form.user_address.status'),
            'tel'           => __('form.user_address.tel'),
            'admin_remark'  => __('form.user_address.admin_remark'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
