<?php

namespace App\Exports\Commission;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class CommissionSummary implements FromView, WithTitle, WithColumnFormatting, WithEvents
{
    protected $commissions;

    public function __construct(array $commissions)
    {
        $this->commissions  = $commissions;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'สรุป';
    }

    public function view(): View
    {
        return view('exports.commission.summary', ['commissions' => $this->commissions]);
    }

    public function columnFormats(): array
    {
        return [
            'J' => NumberFormat::FORMAT_NUMBER_00,
            'L' => NumberFormat::FORMAT_NUMBER_00,
            'N' => NumberFormat::FORMAT_NUMBER_00,
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                // fx รายการหัก
                $event->sheet->setCellValue('L8', '=SUM(N7)/2');
                
                // fx คงเหลือ
                $event->sheet->setCellValue('N6', '=J6-L6');
                $event->sheet->setCellValue('N7', '=(N6+J7)-L7');
                $event->sheet->setCellValue('N8', '=(N7+J8)-L8');
                $event->sheet->setCellValue('N9', '=(N8+J9)-L9');
                $event->sheet->setCellValue('N10', '=(N9+J10)-L10');
                $event->sheet->setCellValue('N17', '=N10');
            },
        ];
    }
}
