<?php

namespace App\Http\Requests\CommissionRate;

use App\Http\Requests\FormRequest;
use App\Rules\DateFormats;

class ExportCommissionRateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('extend.commission-rate');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'admin_ref_id'              => 'required|exists:admins,id',
            'time_format'               => 'sometimes|nullable|in:created_at,updated_at,approved_at,paid_at,droppoint_at,thai_shipping_date,succeed_at',
            'date_start'                => ['sometimes', 'nullable', new DateFormats()],
            'date_end'                  => ['sometimes', 'nullable', new DateFormats()],
            'status'                    => 'sometimes|nullable|integer',
            'rate_id'                   => 'sometimes|nullable|integer',
            'thai_shipping_method_id'   => 'sometimes|nullable|integer',
            'shipping_province_id'      => 'sometimes|nullable|integer',
            'bill_no'                   => 'sometimes|nullable|string',
            'user_code'                 => 'sometimes|nullable|string',
            'user_remark'               => 'sometimes|nullable|string',
            'admin_remark'              => 'sometimes|nullable|string',
            'condition'                 => 'sometimes|nullable|string',
        ];
    }

        /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'admin_ref_id'              => __('form.comnission_rate.admin_ref_id'),
            'time_format'               => __('form.comnission_rate.time_format'),
            'date_start'                => __('form.comnission_rate.date_start'),
            'date_end'                  => __('form.comnission_rate.date_end'),
            'status'                    => __('form.comnission_rate.status'),
            'rate_id'                   => __('form.comnission_rate.rate_id'),
            'thai_shipping_method_id'   => __('form.comnission_rate.thai_shipping_method_id'),
            'shipping_province_id'      => __('form.comnission_rate.shipping_province_id'),
            'bill_no'                   => __('form.comnission_rate.bill_no'),
            'user_code'                 => __('form.comnission_rate.user_code'),
            'user_remark'               => __('form.comnission_rate.user_remark'),
            'admin_remark'              => __('form.comnission_rate.admin_remark'),
            'condition'                 => __('form.comnission_rate.condition'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
