<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\ThaiShippingWeightSizePrice\StoreWeightSizePriceMethodRequest;
use App\Http\Requests\ThaiShippingWeightSizePrice\UpdateWeightSizePriceMethodRequest;
use Illuminate\Http\Request;
use App\Interfaces\ThaiShippingWeightSizePriceInterface;

class ThaiShippingWeightSizePriceController extends Controller
{
    protected $thaiShippingWeightSizePriceInterface;

    public function __construct(ThaiShippingWeightSizePriceInterface $thaiShippingWeightSizePriceInterface)
    {
        $this->thaiShippingWeightSizePriceInterface            = $thaiShippingWeightSizePriceInterface;
    }

    public function index(Request $request)
    {
        $this->authorize('index', ThaiShippingWeightSizePrice::class);
        $columns         = $request->input('columns', ['*']);
        $thaiShippingWeightSizePrice            = $this->thaiShippingWeightSizePriceInterface->all($columns);
        return response()->json($thaiShippingWeightSizePrice, $thaiShippingWeightSizePrice->code);
    }

    public function show(Request $request, $id)
    {
        $this->authorize('show', ThaiShippingWeightSizePrice::class);
        $columns          = $request->input('columns', ['*']);
        $thaiShippingWeightSizePrice             = $this->thaiShippingWeightSizePriceInterface->findById($id, $columns);
        return response()->json($thaiShippingWeightSizePrice, $thaiShippingWeightSizePrice->code);
    }

    public function search(Request $request)
    {
        $this->authorize('index', ThaiShippingWeightSizePrice::class);
        $conditions     = $request->only(['thai_shipping_method_id']);
        $columns        = $request->input('columns', ['*']);
        $limit          = $request->input('limit', $this->thaiShippingWeightSizePriceInterface::DEFAULT_LIMIT);
        $sorting        = $request->input('order_by', $this->thaiShippingWeightSizePriceInterface::DEFAULT_SORTING);
        $ascending      = $request->input('ascending') == 1 ? 'asc' : 'desc';

        if ($request->boolean('pagination')) {
            $page             = $request->input('page', 1);
            $thaiShippingWeightSizePrice  = $this->thaiShippingWeightSizePriceInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset           = $request->input('page', 0);
            $thaiShippingWeightSizePrice  = $this->thaiShippingWeightSizePriceInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($thaiShippingWeightSizePrice, $thaiShippingWeightSizePrice->code);
    }

    public function store(StoreWeightSizePriceMethodRequest $request)
    {
        $data                           = $request->only(['title', 'max_size', 'min_weight', 'max_weight', 'price', 'thai_shipping_method_id']);
        $thaiShippingWeightSizePrice    = $this->thaiShippingWeightSizePriceInterface->store($data);
        return response()->json($thaiShippingWeightSizePrice, $thaiShippingWeightSizePrice->code);
    }

    public function update(UpdateWeightSizePriceMethodRequest $request, $id)
    {
        $data                           = $request->only(['title', 'max_size', 'min_weight', 'max_weight', 'price']);
        $thaiShippingWeightSizePrice    = $this->thaiShippingWeightSizePriceInterface->updateById($id, $data);
        return response()->json($thaiShippingWeightSizePrice, $thaiShippingWeightSizePrice->code);
    }

    public function destroy($id)
    {
        $this->authorize('destroy', ThaiShippingWeightSizePrice::class);
        $thaiShippingWeightSizePrice    = $this->thaiShippingWeightSizePriceInterface->softDeleteById($id);
        return response()->json($thaiShippingWeightSizePrice, $thaiShippingWeightSizePrice->code);
    }

}
