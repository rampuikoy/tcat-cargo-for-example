<?php

namespace App\Interfaces;

interface DistrictInterface
{
    const DEFAULT_LIMIT = 25;
    const DEFAULT_SORTING = 'title_th';
    const DEFAULT_ASCENDING = 'asc';

    /**
     * fetch all district
     *
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function all(array $columns = ['*'], $sorting = DistrictInterface::DEFAULT_SORTING, $ascending = DistrictInterface::DEFAULT_ASCENDING);

    /**
     * find district by id
     *
     * @param $id
     * @param array $columns
     *
     * @return object|null
     */
    public function findById($id, array $columns = ['*']);

    /**
     * find many district by amphur id
     *
     * @param int $id
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function findManyByAmphurId($id, array $columns = ['*']);

}
