<?php

namespace App\Mail\PointOrder;

use App\Models\PointOrder;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PointOrderSuccess extends Mailable
{
    use Queueable, SerializesModels;

    public $point_order;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(PointOrder $point_order)
    {
        //
        $this->point_order = $point_order->load('shippingMethod', 'user', 'products');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to($this->point_order->user->email)
            ->subject(__('emails.point_order_success', ['no' => $this->point_order->code]))
            ->markdown('emails.point-order.success');
    }
}
