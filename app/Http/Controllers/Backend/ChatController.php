<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Chat\StoreChatRequest;
use App\Interfaces\ChatInterface;
use App\Interfaces\UserInterface;
use App\Models\Chat;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    protected $chatInterface,$userInterface;

    public function __construct(ChatInterface $chatInterface, UserInterface $userInterface)
    {
        $this->chatInterface = $chatInterface;
        $this->userInterface = $userInterface;
    }
    public function index(Request $request)
    {
        $this->authorize('index', Chat::class);
        $columns    = $request->input('columns', ['*']);
        $chats      = $this->chatInterface->all($columns);
        return response()->json($chats, $chats->code);
    }

    public function show(Request $request, $code)
    {
        $this->authorize('show', Chat::class);
        $columns    = $request->input('columns', ['*']);
        $types      = $request->merge(['created_type' => 'user'])->toArray();
        $chats      = $this->chatInterface->findByCode($code, $columns, $types);
        $user       = $this->userInterface->findByCode($code, $columns);

        if($user->error){
            return response()->json($user, $user->code);
        }

        if(!$chats->error){
           $chats->results = collect(['messages' => $chats->results ,'user' => $user->results]);
        }

        return response()->json($chats, $chats->code);
    }

    public function search(Request $request)
    {
        $this->authorize('index', Chat::class);
        $conditions = $request->only(['user_code', 'message', 'status', 'date_start', 'date_end', 'search']);
        $limit      = $request->input('limit', $this->chatInterface::DEFAULT_LIMIT);
        $sorting    = $request->input('order_by', $this->chatInterface::DEFAULT_SORTING);
        $ascending  = $request->input('ascending') == 1 ? 'asc' : 'desc';

        if ($request->boolean('pagination')) {
            $page   = $request->input('page', 1);
            $chats  = $this->chatInterface->paginateListByFields($conditions, $page, $limit, $sorting, $ascending);
        } else {
            $offset = $request->input('page', 0);
            $chats  = $this->chatInterface->fetchListByFields($conditions, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($chats, $chats->code);
    }
    public function dropdown()
    {
        $dropdown = $this->chatInterface->dropdownList();
        return response()->json($dropdown, $dropdown->code);
    }

    public function store(StoreChatRequest $request)
    {
        $columns    = $request->only([
                        'user_code',
                        'message',
                        'file',
                        'readed',
                        'status',
                        'created_type',
                    ]);
        $data       = collect($columns)->merge(['created_type' => 'admin'])->toArray();
        $chat       = $this->chatInterface->store($data);
        return response()->json($chat, $chat->code);
    }

    public function readAll()
    {
        $readed = $this->chatInterface->readAll();
        return response()->json($readed, $readed->code);
    }

}
