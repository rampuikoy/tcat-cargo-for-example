<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    //
    protected $fillable = [
        'code',
        'title_th',
        'title_en',
        'geo_id',
        'amphur_id',
        'province_id',
    ];

    /**
     * relation
     */
    public function amphur()
    {
        return $this->belongsTo('App\Models\Amphur', 'amphur_id', 'id');
    }

    public function province()
    {
        return $this->belongsTo('App\Models\Province', 'province_id', 'id');
    }
}
