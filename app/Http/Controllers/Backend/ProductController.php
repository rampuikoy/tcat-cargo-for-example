<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Product\StoreProductRequest;
use App\Http\Requests\Product\UpdateProductRequest;
use App\Interfaces\ProductInterface;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    protected $productInterface;

    public function __construct(ProductInterface $productInterface)
    {
        $this->productInterface = $productInterface;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('index', Product::class);
        $columns     = $request->input('columns', ['*']);
        $product     = $this->productInterface->all($columns);
        return response()->json($product, $product->code);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Product\StoreProductRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
        $data       = $request->only([
                        'title',
                        'code',
                        'status',
                        'point',
                        'stock',
                        'limit',
                        'description',
                        'remark',
                        'image'
                    ]);
        $product    = $this->productInterface->store($data);
        return response()->json($product, $product->code);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        $this->authorize('show', Product::class);
        $columns     = $request->input('columns', ['*']);
        $product     = $this->productInterface->findById($id, $columns);
        return response()->json($product, $product->code);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Product\UpdateProductRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, $id)
    {
        $data       = $request->only([
                        'title',
                        'code',
                        'status',
                        'point',
                        'stock',
                        'limit',
                        'description',
                        'remark',
                        'image'
                    ]);
        $product    = $this->productInterface->updateById($id, $data);
        return response()->json($product, $product->code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('destroy', Product::class);
        $product  = $this->productInterface->softDeleteById($id);
        return response()->json($product, $product->code);
    }
    
    /**
     * Display a listing search of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $this->authorize('index', Product::class);
        $conditions     = $request->only(['title', 'code', 'status']);
        $columns        = $request->input('columns', ['*']);
        $limit          = $request->input('limit', $this->productInterface::DEFAULT_LIMIT);
        $sorting        = $request->input('order_by', $this->productInterface::DEFAULT_SORTING);
        $ascending      = $request->input('ascending') == 1 ? 'asc' : 'desc';
        if ($request->boolean('pagination')) {
            $page       = $request->input('page', 1);
            $product    = $this->productInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset     = $request->input('page', 0);
            $product    = $this->productInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($product, $product->code);
    }

    /**
     * Display a listing of the dropdown.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function dropdown()
    {
        $dropdown = $this->productInterface->dropdownList();
        return response()->json($dropdown, $dropdown->code);
    }
}
