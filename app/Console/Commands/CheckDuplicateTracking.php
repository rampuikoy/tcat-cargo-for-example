<?php

namespace App\Console\Commands;

use App\Models\Tracking;
use \App\Helpers\Trackings;
use Illuminate\Console\Command;

class CheckDuplicateTracking extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tracking:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check dupicate tracking with Tmall Taobao';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Tracking $tracking)
    {
        $this->tracking=$tracking;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Check dupicate tracking with Tmall Taobao...');

        $trackings =  $this->tracking->whereDuplicateCheck(0)
                                ->whereBetween('status',[2,5]) // thai in  not thai out
                                ->limit(20)
                                ->get();

            collect($trackings)
            ->map(function ($track){
                $tmpTmall                = Trackings::checkTmallDuplicate($track);
                $track->system_remark    = $tmpTmall->system_remark;
                $track->duplicate_order  = $tmpTmall->duplicate_order;

                $tmpTaobao               = Trackings::checkTaobaoDuplicate($track);
                $track->system_remark    = $tmpTaobao->system_remark;
                $track->duplicate_order  = $tmpTaobao->duplicate_order;
                $track->duplicate_check  = 1;
                $track->save();
            });

    }
}
