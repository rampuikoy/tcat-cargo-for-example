<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chats', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('user_code', 20)->nullable();
            $table->text('message')->nullable();
            $table->string('file', 100)->nullable();
            $table->tinyInteger('readed')->default(0);
            $table->string('created_admin', 50)->nullable();
            $table->integer('created_admin_id')->unsigned()->nullable();
            $table->enum('created_type', ['user', 'admin', 'broadcast'])->default('user');
            $table->enum('status', ['active', 'inactive'])->default('active');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chats');
    }
}
