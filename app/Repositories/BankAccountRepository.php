<?php

namespace App\Repositories;

use App\Interfaces\BankAccountInterface;
use App\Models\BankAccount;
use Illuminate\Support\Facades\Cache;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;

class BankAccountRepository implements BankAccountInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const MINUTE_CACHE  = 1;

    public function __construct(BankAccount $bankAccount)
    {
        $this->bankAccount = $bankAccount;
    }

    public function all(array $columns = ['*'], $sorting = BankAccountInterface::DEFAULT_SORTING, $ascending = BankAccountInterface::DEFAULT_ASCENDING)
    {
        $key       = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time      = $this->getTime(self::MINUTE_CACHE);

        $banks     = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
            return  $this->bankAccount::select($columns)
                ->whereNotNull('user_code')
                ->orderBy($sorting, $ascending)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.bank_user')]), $banks);
    }

    public function fetchListByFields(array $conditions, array $columns = ['*'], $offset = 0, $limit = BankAccountInterface::DEFAULT_LIMIT, $sorting = BankAccountInterface::DEFAULT_SORTING, $ascending = BankAccountInterface::DEFAULT_ASCENDING)
    {
        $key      = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time     = $this->getTime(self::MINUTE_CACHE);


        $banks    = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
            return $this->bankAccount::select($columns)
                ->search($conditions)
                ->whereNotNull('user_code')
                ->orderBy($sorting, $ascending)
                ->skip($offset)
                ->limit($limit)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.bank_user')]), $banks);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = BankAccountInterface::DEFAULT_LIMIT, $sorting = BankAccountInterface::DEFAULT_SORTING, $ascending = BankAccountInterface::DEFAULT_ASCENDING)
    {
        $key      = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time     = $this->getTime(self::MINUTE_CACHE);


        $banks    =  Cache::tags([

            $this->getClassPrefix(),

            $this->getPaginationWithClassPrefix(),

        ])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
            $model = $this->bankAccount::select($columns)
                ->search($conditions)
                ->whereNotNull('user_code')
                ->orderBy($sorting, $ascending);

            if ($page == 1) {
                return $model->paginate($limit);
            } else {
                return $model->skip($limit * ($page - 1))->paginate($limit);
            }
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.bank_user')]), $banks);
    }

    public function findById($id, array $columns = ['*'])
    {

        $bank    = $this->findByIdMethod($id, $columns);

        if (!$bank) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.bank_user'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.bank_user')]), $bank);
    }

    public function findManyByUserCode($code, array $columns = ['*'])
    {
        if (!$code) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.bank_user'), 'id' => $code]));
        }

        $bank           = $this->findByUserCodeMethod($code, $columns);

        if (!$bank) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.bank_user'), 'id' => $code]));
        }

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.bank_user')]), $bank);
    }

    public function dropdownList()
    {
        $key        = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time       = $this->getTime(self::MINUTE_CACHE);


        $dropdown   = Cache::remember($key, $time, function () {
            return [
                'status'        => $this->bankAccount::statusList(),
                'bank'          =>  $this->bankAccount::bankList(),
            ];
        });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function store(array $data)
    {
        $bank       = $this->bankAccount::create($data);
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.bank_user')]), $bank);
    }

    public function updateById($id, array $data)
    {
        $bank        = $this->findByIdMethod($id);
        if (!$bank) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.bank_user'), 'id' => $id]));
        }

        $bank->update($data);
        $this->clearCacheTags();
        $this->clearCacheTagById($id);

        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.bank_user')]), $bank);
    }

    public function softDeleteById($id)
    {
        $bank        = $this->findByIdMethod($id);

        if (!$bank) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.bank_user'), 'id' => $id]));
        }

        $bank->delete();
        $this->clearCacheTags();
        $this->clearCacheTagById($id);

        return $this->coreResponse(200, __('messages.delete_data', ['data' => __('messages.bank_user')]), $bank);
    }

    private function findByIdMethod($id, array $columns = ['*'])
    {
        $key            = $this->getCacheKey(null, $id);
        $time           = $this->getTime(self::MINUTE_CACHE);

        $bankAccount    = Cache::tags([ $this->getClassPrefix(), $this->getIdWithClassPrefix($id)])->remember($key, $time, function () use ($id, $columns) {
                            return $this->bankAccount::select($columns)
                                                    ->whereNotNull('user_code')
                                                    ->whereId($id)
                                                    ->first();
                        });

        return $bankAccount;
    }

    private function findByUserCodeMethod($userCode, $columns = ['*'])
    {
        $key        = $this->getCacheKey(null, $userCode);
        $time       = $this->getTime(self::MINUTE_CACHE);

        $tracking   = Cache::tags([$this->getClassPrefix(), $this->getUserCodePrefix($userCode)])->remember($key, $time, function () use ($userCode, $columns) {
                        return $this->bankAccount::select($columns)
                                                ->whereUserCode($userCode)
                                                ->whereNotNull('user_code')
                                                ->get();
                    });

        return $tracking;
    }
    
    private function clearCacheTags()
    {
        Cache::tags([
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix()
        ])->flush();
    }

    private function clearCacheTagById($id)
    {
        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();
    }
}