<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class TruckPolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.truck.index');
    }
    public function show(Admin $admin)
    {
        return $admin->hasAnyPermission(['backend.truck.show', 'backend.truck.update']);
    }

    public function store(Admin $admin)
    {
        return $admin->can('backend.truck.store');
    }
    public function update(Admin $admin)
    {
        return $admin->can('backend.truck.update');
    }

    public function destroy(Admin $admin)
    {
        return $admin->can('backend.truck.destroy');
    }

}
