<?php

namespace App\Http\Requests\PointOrder;

use App\Http\Requests\FormRequest;

class ShipPointOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.point-order.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'truck_id'           => 'required',
            'driver_admin_id'    => 'required',
            'driver2_admin_id'   => 'sometimes|nullable',
        ];
        return $rules;
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'truck_id'           => __('form.point_order.truck_id'),
            'driver_admin_id'    => __('form.point_order.driver_admin_id'),
            'driver2_admin_id'   => __('form.point_order.driver2_admin_id'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
