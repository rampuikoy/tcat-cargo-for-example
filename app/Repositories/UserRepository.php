<?php

namespace App\Repositories;

use App\Imports\UsersImport;
use App\Interfaces\UserInterface;
use App\Models\User;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use App\Traits\UploadTrait;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;

class UserRepository implements UserInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, UploadTrait, CoreResponseTrait;

    const MINUTE_CACHE      = 1;
    const TALBE_RELEATIONS  = ['province', 'sale', 'rate', 'thaiShippingMethod', 'credit', 'point'];

    public function __construct(User $user)
    {
        $this->user         = $user;
    }

    public function all(array $columns = ['*'], $sorting = UserInterface::DEFAULT_SORTING, $ascending = UserInterface::DEFAULT_ASCENDING)
    {
        $key    = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time   = $this->getTime(self::MINUTE_CACHE);

        $users  = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
            return $this->user::select($columns)->orderBy($sorting, $ascending)->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.user')]), $users);
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = UserInterface::DEFAULT_LIMIT, $sorting = UserInterface::DEFAULT_SORTING, $ascending = UserInterface::DEFAULT_ASCENDING)
    {
        $key    = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time   = $this->getTime(self::MINUTE_CACHE);

        $users  = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
            return $this->user::select($columns)
                ->with(self::TALBE_RELEATIONS)
                ->search($conditions)
                ->orderBy($sorting, $ascending)
                ->skip($offset)
                ->limit($limit)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.user')]), $users);
    }

    public function fetchListByFieldsPoints(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = UserInterface::DEFAULT_LIMIT, $sorting = UserInterface::DEFAULT_SORTING_POINT, $ascending = UserInterface::DEFAULT_ASCENDING)
    {
        $key    = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time   = $this->getTime(self::MINUTE_CACHE);

        $users  = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
            return $this->user::select($columns)
                ->rightJoin('points', 'points.user_code', '=', 'users.code',)
                ->selectRaw('points.*, user_code,SUM(IF(type="topup",amount,-amount)) as total')
                ->search($conditions)
                ->orderBy($sorting, $ascending)
                ->skip($offset)
                ->limit($limit)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.user')]), $users);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = UserInterface::DEFAULT_LIMIT, $sorting = UserInterface::DEFAULT_SORTING, $ascending = UserInterface::DEFAULT_ASCENDING)
    {
        $key    = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time   = $this->getTime(self::MINUTE_CACHE);

        $users  = Cache::tags([$this->getClassPrefix(), $this->getPaginationWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
            $model = $this->user::select($columns)
                ->with(self::TALBE_RELEATIONS)
                ->search($conditions)
                ->orderBy($sorting, $ascending);
            if ($page == 1) {
                return $model->paginate($limit);
            } else {
                return $model->skip($limit * ($page - 1))->paginate($limit);
            }
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.user')]), $users);
    }

    public function paginateListByFieldsPoints(array $conditions, array $columns = ['*'], $page = 1, $limit = UserInterface::DEFAULT_LIMIT, $sorting = UserInterface::DEFAULT_SORTING_POINT, $ascending = UserInterface::DEFAULT_ASCENDING)
    {
        $key    = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time   = $this->getTime(self::MINUTE_CACHE);

        $users  = Cache::tags([$this->getClassPrefix(), $this->getPaginationWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
            $model = $this->user::select($columns)
                ->rightJoin('points', 'points.user_code', '=', 'users.code',)
                ->selectRaw('points.*, user_code,SUM(IF(type="topup",amount,-amount)) as total')
                ->search($conditions)
                ->orderBy($sorting, $ascending);
            if ($page == 1) {
                return $model->paginate($limit);
            } else {
                return $model->skip($limit * ($page - 1))->paginate($limit);
            }
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.user')]), $users);
    }

    public function findByCode($code, array $columns = ['*'])
    {
        $user = $this->findByCodeMethod($code, $columns);

        if (!$user) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.user'), 'id' => $code]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.user')]), $user);
    }

    public function findByEmail($email, array $columns = ['*'])
    {
        $user = $this->findByEmailMethod($email, $columns);

        if (!$user) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.user'), 'id' => $email]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.user')]), $user);
    }

    public function findByUserCodeOrEmail($slug, array $columns = ['*'])
    {
        $key    = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time   = $this->getTime(self::MINUTE_CACHE);

        $user   = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($slug, $columns) {
            return $this->user::select($columns)
                ->where('code', 'LIKE', "%{$slug}%")
                ->orWhere('email', 'LIKE', "%{$slug}%")
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.user')]), $user);
    }

    public function dropdownList()
    {
        $key        = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $activeList = $this->user::activeList();
        $dropdown   = Cache::remember($key, $time, function () use ($activeList) {
            return [
                'status'            => $activeList,
                'withholding'       => $activeList,
                'sms_notification'  => $activeList,
                'alert_box'         => $activeList,
                'create_bill'       => $this->user::createBillList(),
                'condition'         => $this->user::conditionList(),
            ];
        });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function store(array $data)
    {
        $user   = $this->user::create($data);
        $user   = $user->load(self::TALBE_RELEATIONS);
        $this->clearCacheTags();
        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.user')]), $user);
    }

    public function updateById($id, array $data)
    {
        $user   = $this->findByIdMethod($id);
        if (!$user) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.user'), 'id' => $id]));
        }

        $user->update($data);
        $this->clearCacheTags();
        $this->clearCacheTagById($user->id);
        $this->clearCacheTagByUserCode($user->code);
        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.user')]), $user->load(self::TALBE_RELEATIONS));
    }

    public function softDeleteById($id)
    {
        $user = $this->findByIdMethod($id);
        if (!$user) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.user'), 'id' => $id]));
        }

        $user->delete();
        $this->clearCacheTags();
        $this->clearCacheTagById($user->id);
        $this->clearCacheTagByUserCode($user->code);
        return $this->coreResponse(200, __('messages.delete_data', ['data' => __('messages.user')]), $user);
    }
    
    public function ResetPassword($id, $newPassword)
    {
        $user = $this->findByIdMethod($id);
        if (!$user) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.user'), 'id' => $id]));
        }
        $user->password = Hash::make($newPassword);
        $user->save();
        $this->clearCacheTagById($user->id);
        $this->clearCacheTagByUserCode($user->code);
        $this->clearCacheTagByUserEmaill($user->email);
        return $this->coreResponse(200, __('messages.reset_data', ['data' => __('messages.password')]), $user);
    }

    public function updateFcmTokenById($id, $token)
    {
        $user = $this->findByIdMethod($id);
        if (!$user) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.user'), 'id' => $id]));
        }
        $user->update(['fcm_token' => $token]);
        $this->clearCacheTagById($user->id);
        $this->clearCacheTagByUserCode($user->code);
        $this->clearCacheTagByUserEmaill($user->email);
        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.fcm_token')]), $user);
    }

    private function findByIdMethod($id)
    {
        $key    = $this->getCacheKey(null, $id);
        $time   = $this->getTime(self::MINUTE_CACHE);
        $user   = Cache::tags([
            $this->getClassPrefix(),
            $this->getIdWithClassPrefix($id),
            $this->getFindByIdWithClassPrefix(),
        ])->remember($key, $time, function () use ($id) {
            return $this->user::select(['*'])->with(self::TALBE_RELEATIONS)->whereId($id)->first();
        });
        return $user;
    }

    public function findByCodeMethod($code, array $columns = ['*'])
    {
        $key    = $this->getCacheKey(null, $code);
        $time   = $this->getTime(self::MINUTE_CACHE);
        $user   = Cache::tags([
            $this->getClassPrefix(),
            $this->getUserCodePrefix($code),
            $this->getUserCodeWithClassPrefix($code)
        ])->remember($key, $time, function () use ($code, $columns) {
            return $this->user::select($columns)->with(self::TALBE_RELEATIONS)->whereCode($code)->first();
        });
        return $user;
    }

    public function findByEmailMethod($email, array $columns = ['*'])
    {
        $key    = $this->getCacheKey(null, $email);
        $time   = $this->getTime(self::MINUTE_CACHE);
        $user   = Cache::tags([
                    $this->getClassPrefix(),
                    $this->getUserEmailPrefix($email),
                    $this->getUserCodeWithClassPrefix($email)
                ])->remember($key, $time, function () use ($email, $columns) {
                    return $this->user::select($columns)->with(self::TALBE_RELEATIONS)->whereEmail($email)->first();
                });
        return $user;
    }

    public function clearCacheTags()
    {
        Cache::tags([
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix()
        ])->flush();
    }

    public function clearCacheTagById($id)
    {
        Cache::tags([
            $this->getIdWithClassPrefix($id)
        ])->flush();
    }

    public function clearCacheTagByUserCode($userCode)
    {
        Cache::tags([
            $this->getUserCodePrefix($userCode),
            $this->getUserCodeWithClassPrefix($userCode)
        ])->flush();
    }

    public function clearCacheTagByUserEmaill($email){
        Cache::tags([
            $this->getUserEmailPrefix($email),
            $this->getUserEmailWithClassPrefix($email)
        ])->flush();
    }
}
