<?php

namespace App\Http\Requests\Chat;

use App\Rules\Base64Image;
use App\Http\Requests\FormRequest;

class StoreChatRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.chat.store');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_code'        => 'required|exists:users,code',
            // required message ถ้า file ไม่มีค่า หรือเท่ากับ null
            'message'          => 'required_without:file',
            // required file ถ้า message ไม่มีค่า หรือเท่ากับ null
            'file'             => ['required_without:message', 'nullable', new Base64Image],
            'status'           => 'required|in:active,inactive',
            'readed'           => 'required|in:0,1',
            'created_type'     => 'sometimes|in:user,admin',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'user_code'     => __('form.chat.user_code'),
            'message'       => __('form.chat.message'),
            'file'          => __('form.chat.file'),
            'status'        => __('form.chat.status'),
            'readed'        => __('form.chat.readed'),
            'created_type'  => __('form.chat.created_type'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
