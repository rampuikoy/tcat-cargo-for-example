<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\FormRequest;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Auth;

class UpdatePasswordRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'password'          => 'required|confirmed|min:6',
            'password_current'  => new MatchOldPassword,
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'password'          => __('form.auth.password'),
            'password_current'  => __('form.auth.password_current'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
