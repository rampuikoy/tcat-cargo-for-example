<?php

namespace App\Http\Controllers\Backend\NoRole;

use App\Http\Controllers\Controller;
use App\Http\Requests\Location\DistanceRequest;
use App\Interfaces\AmphurInterface;
use App\Interfaces\DistrictInterface;
use App\Interfaces\ProvinceInterface;
use App\Interfaces\ZipcodeInterface;
use Exception;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    //
    protected $provinceInterface, $amphurInterface, $districtInterface, $zipcodeInterface;

    public function __construct(ProvinceInterface $provinceInterface, AmphurInterface $amphurInterface, DistrictInterface $districtInterface, ZipcodeInterface $zipcodeInterface)
    {
        $this->provinceInterface = $provinceInterface;
        $this->amphurInterface = $amphurInterface;
        $this->districtInterface = $districtInterface;
        $this->zipcodeInterface = $zipcodeInterface;
    }

    public function province(Request $request)
    {
        $columns = $request->input('columns', ['*']);
        $provinces = $this->provinceInterface->all($columns);
        return response()->json($provinces, $provinces->code);
    }

    public function amphurById(Request $request, $id)
    {
        $columns = $request->input('columns', ['*']);
        $findAmphur = $this->amphurInterface->findManyByProvinceId($id, $columns);
        return response()->json($findAmphur, $findAmphur->code);
    }

    public function districtById(Request $request, $id)
    {
        $columns = $request->input('columns', ['*'], 'title_th', 'asc');
        $findDistrict = $this->districtInterface->findManyByAmphurId($id, $columns);
        return response()->json($findDistrict, $findDistrict->code);
    }

    public function zipcodeByCode(Request $request, $code)
    {
        $columns = $request->input('columns', ['*'], 'code', 'asc');
        $findZipcode = $this->zipcodeInterface->findManyByDistrictCode($code, $columns);
        return response()->json($findZipcode, $findZipcode->code);
    }

    public function distance(DistanceRequest $request)
    {
        try {
            $key            = env('GOOGLE_API_KEY');

            if(!$key) {
                throw new Exception('GOOGLE API KEY EMPTY');
            }

            $origin         = $request->input('origin.latitude') . ',' . $request->input('origin.longitude');
            $destination    = 'ต.' . $request->input('destination.district')
                            . '+อ.' . $request->input('destination.amphur')
                            . '+จ.' . $request->input('destination.province')
                            . '+' . $request->input('destination.zipcode');
    
            $url            = 'https://maps.googleapis.com/maps/api/distancematrix/json?key='.$key.'&units=metric&mode=driving';
            $queryString    =  '&origins=' . $origin . '&destinations=' . $destination;
    
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url . $queryString);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_PROXYPORT, 3128);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, TRUE); // Compliant; default value is TRUE
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);  // Compliant
            $resulst = curl_exec($curl);
            curl_close($curl);
    
            return response()->json(['error' => false, 'code' => 200, 'message' => __('messages.fetch_data', ['data' => __('messages.distance')]), 'results' => json_decode($resulst)], 200);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json(['error' => true, 'code' => 500, 'message' => $th->getMessage()], 500);
        }

    }
}
