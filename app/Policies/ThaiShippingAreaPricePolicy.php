<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class ThaiShippingAreaPricePolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.thai-shipping-area-price.index');
    }

    public function show(Admin $admin)
    {
        return $admin->hasAnyPermission(['backend.thai-shipping-area-price.show', 'backend.thai-shipping-area-price.update']);
    }

    public function store(Admin $admin)
    {
        return $admin->can('backend.thai-shipping-area-price.store');
    }

    public function update(Admin $admin)
    {
        return $admin->can('backend.thai-shipping-area-price.update');
    }

    public function destroy(Admin $admin)
    {
        return $admin->can('backend.thai-shipping-area-price.destroy');
    }
}
