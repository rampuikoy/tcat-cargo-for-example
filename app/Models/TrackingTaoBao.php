<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrackingTaoBao extends Model
{
    use HasFactory;
    protected $connection = 'taobaoconew';
    protected $table = 'tracking';
    public $timestamps = false;

    public function scopeSearch($query, $code)
    {
        return $query->where('tracking', $code);
    }
}
