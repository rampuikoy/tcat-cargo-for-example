<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\NoCode\FindOrCreateNoCodeRequest;
use App\Http\Requests\NoCode\UpdateNoCodeRequest;
use App\Http\Requests\NoCode\UploadNoCodeRequest;
use App\Http\Requests\NoCode\ZoneInNoCodeRequest;
use App\Http\Requests\NoCode\ZoneOutNoCodeRequest;
use App\Interfaces\NoCodeInterface;
use App\Interfaces\UploadInterface;
use App\Models\NoCode;
use Illuminate\Http\Request;

class NoCodeController extends Controller
{
    protected $nocodeInterface;

    public function __construct(NoCodeInterface $nocodeInterface, UploadInterface $uploadInterface)
    {
        $this->nocodeInterface = $nocodeInterface;
        $this->uploadInterface = $uploadInterface;
    }

    public function index(Request $request)
    {
        $this->authorize('index', NoCode::class);
        $columns    = $request->input('columns', ['*']);
        $nocode     = $this->nocodeInterface->all($columns);
        return response()->json($nocode, $nocode->code);
    }

    public function search(Request $request)
    {
        $this->authorize('index', NoCode::class);
        $conditions     = $request->only([
                            'code',
                            'order_code',
                            'reference_code',
                            'zone',
                            'taken_admin',
                            'time_format',
                            'date_start',
                            'date_end',
                            'remark',
                            'description',
                            'amount',
                            'type',
                            'status',
                            'source',
                            'source_order',
                            'department',
                            'weight',
                            'width',
                            'height',
                            'length',
                            'condition',
                        ]);
        $columns        = $request->input('columns', ['*']);
        $limit          = $request->input('limit', $this->nocodeInterface::DEFAULT_LIMIT);
        $sorting        = $request->input('order_by', $this->nocodeInterface::DEFAULT_SORTING);
        $ascending      = $request->input('ascending') == 1 ? 'asc' : 'desc';

        if ($request->boolean('pagination')) {
            $page       = $request->input('page', 1);
            $nocode     = $this->nocodeInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset     = $request->input('page', 0);
            $nocode     = $this->nocodeInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($nocode, $nocode->code);
    }

    public function gallery(Request $request)
    {
        $this->authorize('index', NoCode::class);
        $conditions     = $request->only([
                                        'code',
                                        'order_code',
                                        'reference_code',
                                        'zone',
                                        'taken_admin',
                                        'time_format',
                                        'date_start',
                                        'date_end',
                                        'remark',
                                        'description',
                                        'amount',
                                        'type',
                                        'status',
                                        'source',
                                        'source_order',
                                        'department',
                                        'weight',
                                        'width',
                                        'height',
                                        'length',
                                        'condition',
                                    ]);
        $columns        = $request->input('columns', ['*']);
        $limit          = $request->input('limit', $this->nocodeInterface::DEFAULT_LIMIT);
        $sorting        = $request->input('order_by', $this->nocodeInterface::DEFAULT_SORTING);
        $ascending      = $request->input('ascending') == 1 ? 'asc' : 'desc';
        $page           = $request->input('page', 1);
        $nocode         = $this->nocodeInterface->paginateGalleryByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        return response()->json($nocode, $nocode->code);
    }

    public function dropdown()
    {
        $dropdown = $this->nocodeInterface->dropdownList();
        if (!$dropdown->error) {
            $collection = collect($dropdown->results)->merge([]);
            $dropdown->results = $collection;
        }
        return response()->json($dropdown, $dropdown->code);
    }

    public function show(Request $request, $id)
    {
        $this->authorize('show', NoCode::class);
        $nocode = $this->nocodeInterface->findById($id);
        return response()->json($nocode, $nocode->code);
    }

    public function findOrCreateByCode(FindOrCreateNoCodeRequest $request)
    {
        $code = $request->code;
        $nocode = $this->nocodeInterface->findByCode($code);

        if ($request->create && $nocode->error) {
            $options = $request->only(['tcat', 'taobao', 'tmall']);
            $nocode = $this->nocodeInterface->store($code, $options);
        }

        return response()->json($nocode, $nocode->code);
    }

    public function update(UpdateNoCodeRequest $request, $id)
    {
        $data   = $request->only([
            'code',
            'date_in',
            'order_code',
            'reference_code',
            'zone',
            'description',
            'remark',
            'type',
            'department',
            'weight',
            'width',
            'height',
            'length',
            'amount',
            'list',
        ]);
        $nocode  = $this->nocodeInterface->updateById($id, $data);
        return response()->json($nocode, $nocode->code);
    }

    public function cancel(Request $request, $id)
    {
        $nocode  = $this->nocodeInterface->cancelById($id);
        return response()->json($nocode, $nocode->code);
    }

    public function zoneIn(ZoneInNoCodeRequest $request)
    {
        $code   = $request->code;
        $data   = $request->only(['zone', 'tcat', 'taobao', 'tmall']);
        $nocode = $this->nocodeInterface->zoneInByCode($code, $data);
        return response()->json($nocode, $nocode->code);
    }

    public function zoneOut(ZoneOutNoCodeRequest $request)
    {
        $code   = $request->code;
        $data   = $request->only(['taken_admin', 'order_code']);
        $nocode = $this->nocodeInterface->zoneOutByCode($code, $data);
        return response()->json($nocode, $nocode->code);
    }

    public function upload(UploadNoCodeRequest $request, $id)
    {
        $data = [
            'type'              => 'image',
            'relatable_type'    => 'App\Models\NoCode',
            'relatable_id'      => $id,
            'file_path'         => $request->file_path,
        ];
        $upload    = $this->uploadInterface->store($data);
        return response()->json($upload, $upload->code);
    }
}
