<?php

namespace App\Http\Controllers\Backend;

use App\Models\Tracking;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\TrackingInterface;
use App\Interfaces\WarehouseInterface;
use App\Interfaces\ProductTypeInterface;
use App\Interfaces\ShippingMethodInterface;
use App\Http\Requests\Tracking\StoreTrackingRequest;
use App\Http\Requests\Tracking\TrackingDetailRequest;
use App\Http\Requests\Tracking\UpdateTrackingRequest;
use App\Http\Requests\Tracking\DestroyTrackingRequest;
use App\Http\Requests\Tracking\PutZoneInRequest;
use App\Http\Requests\Tracking\PutZoneInModalRequest;

class TrackingController extends Controller
{
    protected $trackingInterface, $productTypeInterface, $shippingMethodInterface, $warehouseInterface;

    public function __construct(TrackingInterface $trackingInterface, ProductTypeInterface $productTypeInterface, ShippingMethodInterface $shippingMethodInterface, WarehouseInterface $warehouseInterface)
    {
        $this->trackingInterface = $trackingInterface;
        $this->productTypeInterface = $productTypeInterface;
        $this->shippingMethodInterface = $shippingMethodInterface;
        $this->warehouseInterface = $warehouseInterface;
    }

    public function show(Request $request, $id)
    {
        $this->authorize('show', Tracking::class);
        $columns = $request->input('columns', ['*']);
        $tracking = $this->trackingInterface->findById($id, $columns);
        return response()->json($tracking, $tracking->code);
    }

    public function showUserCode(Request $request, $userCode)
    {
        $this->authorize('show', Tracking::class);
        $columns = $request->input('columns', ['*']);
        $tracking = $this->trackingInterface->findByUserCode($userCode, $columns);
        return response()->json($tracking, $tracking->code);
    }

    public function showTrack(Request $request, $code)
    {
        $this->authorize('show', Tracking::class);
        $columns = $request->input('columns', ['*']);
        $tracking = $this->trackingInterface->findByCode($code, $columns);
        return response()->json($tracking, $tracking->code);
    }

    public function showTrackExternal(Request $request, $code)
    {
        $this->authorize('show', Tracking::class);
        $columns = $request->input('columns', ['*']);
        $tracking = $this->trackingInterface->findExternalByCode($code, $columns);
        return response()->json($tracking, $tracking->code);
    }


    public function search(Request $request)
    {
        $this->authorize('index', Tracking::class);
        $conditions = $request->only([
            'id',
            'nocode',
            'code_group',
            'product_type_id',
            'shipping_method_id',
            'code',
            'user_code',
            'status',
            'calculate_type',
            'time_format',
            'date_start',
            'date_end',
            'warehouse_id',
            'reference_code',
            'warehouse_zone',
            'user_remark',
            'admin_remark',
            'system_remark',
            'weight',
            'width',
            'height',
            'length',
            'detail',
            'type',
            'staff',
            'condition',
            'bill_no'
        ]);
        $columns = $request->input('columns', ['*']);
        $limit = $request->input('limit', $this->trackingInterface::DEFAULT_LIMIT);
        $sorting = $request->input('order_by', $this->trackingInterface::DEFAULT_SORTING);
        $ascending = $request->input('ascending') == 1 ? 'asc' : 'desc';

        if ($request->boolean('pagination')) {
            $page = $request->input('page', 1);
            $tracking = $this->trackingInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset = $request->input('page', 0);
            $tracking = $this->trackingInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($tracking, $tracking->code);
    }

    public function dropdown()
    {
        $dropdown = $this->trackingInterface->dropdownList();
        if (!$dropdown->error) {
            $product_type = $this->productTypeInterface->all(['id', 'title_th'], 'id', 'asc');
            $shipping_method = $this->shippingMethodInterface->all(['id', 'title_th'], 'id', 'asc');
            $warehouse = $this->warehouseInterface->all(['id', 'title'], 'id', 'asc');

            $dropdown->results = collect($dropdown->results)->merge([
                'product_type' => (!$product_type->error) ? $product_type->results : [],
                'shipping_method' => (!$shipping_method->error) ? $shipping_method->results : [],
                'warehouse' => (!$warehouse->error) ? $warehouse->results : [],
            ]);
        }
        return response()->json($dropdown, $dropdown->code);
    }

    public function store(StoreTrackingRequest $request)
    {
        $data = $request->only([
            'code',
            'code_group',
            'reference_code',
            'user_code',
            'shipping_method_id',
            'product_type_id',
            'weight',
            'width',
            'length',
            'height',
            'cubic',
            'dimension',
            'calculate_type',
            'warehouse_id',
            'warehouse_zone',
            'china_in',
            'china_out',
            'thai_in',
            'thai_out',
            'china_cost_box',
            'china_cost_shipping',
            'user_remark',
            'admin_remark',
        ]);
        $tracking = $this->trackingInterface->store($data);
        return response()->json($tracking, $tracking->code);
    }

    public function update(UpdateTrackingRequest $request, $id)
    {
        $data = $request->only([
            'reference_code',
            'code_group',
            'user_code',
            'shipping_method_id',
            'product_type_id',
            'weight',
            'width',
            'length',
            'height',
            'cubic',
            'dimension',
            'calculate_type',
            'warehouse_id',
            'warehouse_zone',
            'china_in',
            'type',
            'china_out',
            'thai_in',
            'thai_out',
            'china_cost_box',
            'china_cost_shipping',
            'user_remark',
            'admin_remark',
        ]);
        $tracking = $this->trackingInterface->updateById($id, $data);
        return response()->json($tracking, $tracking->code);
    }

    public function trackingDetail(TrackingDetailRequest $request, $id)
    {

        $data = $request->only([
            'product_type',
            'qty',
            'product_remark',
        ]);
        $tracking = $this->trackingInterface->trackingDetail($id, $data);
        return response()->json($tracking, $tracking->code);
    }

    public function destroy($id)
    {
        $this->authorize('destroy', Tracking::class);
        $tracking = $this->trackingInterface->softDeleteById($id);
        return response()->json($tracking, $tracking->code);
    }

    public function destroyAll(DestroyTrackingRequest $request)
    {
        $this->authorize('destroy', Tracking::class);
        $tracking = $this->trackingInterface->deleteMany($request->ids);
        return response()->json($tracking, $tracking->code);
    }

    public function putZoneIn(PutZoneInRequest $request)
    {
        $data = $request->only([
            'code',
            'reference_code',
            'shipping_method_id',
            'product_type_id',
            'warehouse_id',
            'warehouse_zone',
            'set_user_code',
            'confirm'
        ]);
        $tracking = $this->trackingInterface->putZoneIn($data);
        return response()->json($tracking, $tracking->code);
    }

    public function putZoneInModal(PutZoneInModalRequest $request)
    {
        $data = $request->only([
            'reference_code',
            'shipping_method_id',
            'product_type_id',
            'warehouse_id',
            'warehouse_zone',
            'set_user_code',
            'confirm',
            'ids'
        ]);
        $tracking = $this->trackingInterface->putZoneInModal($data);
        return response()->json($tracking, $tracking->code);
    }

    public function postZoneIn(PutZoneInRequest $request)
    {
        $data = $request->only([
            'reference_code',
            'shipping_method_id',
            'product_type_id',
            'warehouse_id',
            'warehouse_zone',
            'set_user_code',
            'confirm',
            'weight',
            'code'
        ]);
        $tracking = $this->trackingInterface->postZoneIn($data);
        return response()->json($tracking, $tracking->code);
    }
}
