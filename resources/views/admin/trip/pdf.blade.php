<!DOCTYPE html>
<html lang="th">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>{{$bill->no}}</title>
	{!! Html::style('public/css/pdf.css') !!}
	<link rel="stylesheet" href="https://res.imemo.co/tcatcargo_alpha/misc/css/pdf.css" media="all" />
	<style>
		div {
			line-height: 12px;
		}

		html {
			margin: 30px 20px;
		}
	</style>
</head>

<body>
	<div>

		<table aria-describedby="trip-user">
			@include('admin.bill.download.partials.bill-header')
			<tr>
				<th scope="col"></th>
			</tr>
			<tr>
				<td colspan="2" class="text-center">
					<div class="page-header">ใบแจ้งค่าบริการ&nbsp;China&nbsp;Trip&nbsp;Tour</div>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					@include('admin.bill.download.partials.bill-user')
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<?php $orders = $bill->orders; ?>
					<table aria-describedby="trip" class="bordered" style="margin-top: 10px;">
						<thead style="background: #dfdfdf;">
							<tr>
								<th scope="col" class="text-right">#</th>
								<th scope="col">@lang('admin.type')</th>
								<th scope="col">@lang('admin.list')</th>
								<th scope="col" class="text-right">@lang('admin.price')(บาท)</th>
								<th scope="col" class="text-right">@lang('admin.amount')</th>
								<th scope="col" class="text-right">@lang('admin.summary')(บาท)</th>
							</tr>
						</thead>
						<tbody>
							@foreach($orders as $key => $order)
							<tr>
								<td class="text-right">
									<div>{{$key+1}}</div>
								</td>
								<td class="text-center">
									<div>{{$order->current_type}}</div>
								</td>
								<td>
									<div>{{str_replace(' ', '&nbsp;',$order->title)}}</div>
								</td>
								<td class="text-right">
									<div>{{number_format($order->price,2)}}</div>
								</td>
								<td class="text-right">
									<div>{{$order->amount}}</div>
								</td>
								<td class="text-right">
									<div>{{number_format($order->total,2)}}</div>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td>
					{!! nl2br($bill->user_remark) !!}
				</td>
				<td style="padding-top: 15px; padding-bottom: 20px;">
					<table aria-describedby="trip-detail">
						<tr>
							<th scope="col">
							</th>
						</tr>
						<tbody>
							<tr>
								<td class="text-right">
									<div>@lang('admin.sub_total')</div>
								</td>
								<td class="text-center bordered">
									<div>{{number_format($bill->sub_total,2)}}</div>
								</td>
							</tr>
							@if($bill->withholding>0)
							<tr>
								<td class="text-right">
									<div>@lang('admin.withholding_3')</div>
								</td>
								<td class="text-center" style="border-bottom: 1px solid;">
									<div>{{number_format($bill->withholding,2)}}</div>
								</td>
							</tr>
							@endif
							<tr class="text-bold">
								<td class="text-right">
									<div>@lang('admin.bill_total')</div>
								</td>
								<td class="text-center" style="border-bottom: 1px solid;">
									<div style="font-size: 20px;">{{number_format($bill->total,2)}}</div>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<table aria-describedby="trip-detail-remark" class="bordered" style="margin-bottom: 10px;">
						<thead style="background: #ddd;">
							<tr>
								<th scope="col">
									<div>@lang('admin.detail')</div>
								</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="padding-left: 20px;padding-right: 20px;">
									<?php $detail = str_replace(' ', '&nbsp;', $bill->detail) ?>
									<div>{!! nl2br($detail) !!}</div>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			@if($bill->credits()->count()>0)
			<tr>
				<td colspan="2" style="padding-bottom: 20px;padding-top: 20px;">
					@include('admin.bill.download.partials.bill-payment')
				</td>
			</tr>
			@else
			<tr>
				<td colspan="2" style="padding: 0px 5px 5px 5px;">
					@include('admin.bill.download.partials.bill-bank')
				</td>
			</tr>
			@endif
			<tr>
				<td colspan="2" style="padding: 5px 5px 5px 5px;">
					@include('admin.bill.download.partials.bill-contact')
				</td>
			</tr>
			<tr>
				<td class="text-center" style="padding: 10px;">
					{!!'<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($bill->user_code, 'C128',2,30) . '" alt="barcode" />'!!} <br>
					<small>@lang('admin.user_code'):&nbsp;{{$bill->user_code}}</small>
				</td>
				<td class="text-center" style="padding: 10px;">
					{!!'<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($bill->no, 'C128',2,30) . '" alt="barcode" />'!!} <br>
					<small>@lang('admin.bill_no'):&nbsp;{{$bill->no}}</small>
				</td>
			</tr>

		</table>
	</div>

</body>

</html>