<?php

namespace App\Imports;

use App\Helpers\Util;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;

HeadingRowFormatter::extend('mapping-key', function ($value) {
    switch ($value) {
        case 'Tracking':
            return 'code';
        case 'ออเดอร์อ้างอิง':
            return 'reference_code';
        case 'ออเดอร์จริง':
            return 'order_code';
        case 'วันที่รับเข้า':
            return 'date_in';
        case 'โซน':
            return 'zone';
        case 'หมายเหตุ':
            return 'remark';
        default:
            return;
    }
});

HeadingRowFormatter::default('mapping-key');

class NoCodeImport implements ToCollection, WithValidation, WithBatchInserts, WithChunkReading, WithHeadingRow
{
    use Importable;

    protected $nocode;

    public function collection(Collection $collection)
    {
        $this->nocode = $collection->map(function ($nocode) {
            $nocode['date_in'] = Util::excelToDate($nocode['date_in']);
            return collect($nocode)->only(['code', 'reference_code', 'order_code', 'date_in', 'zone', 'remark']);
        });
    }

    public function rules(): array
    {
        return [
            'code' => 'required',
        ];
    }

    public function batchSize(): int
    {
        return 100;
    }

    public function chunkSize(): int
    {
        return 100;
    }

    public function headingRow(): int
    {
        return 1;
    }

    public function getNoCodes()
    {
        return $this->nocode ?? collect([]);
    }
}
