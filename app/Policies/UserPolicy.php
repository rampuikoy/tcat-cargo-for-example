<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.user.index');
    }

    public function show(Admin $admin)
    {
        return $admin->hasAnyPermission(['backend.user.show', 'backend.user.update']);
    }

    public function store(Admin $admin)
    {
        return $admin->can('backend.user.store');
    }

    public function update(Admin $admin)
    {
        return $admin->can('backend.user.update');
    }

    public function destroy(Admin $admin)
    {
        return $admin->can('backend.user.destroy');
    }

    public function ImportExcel(Admin $admin)
    {
        return $admin->can('backend.user.store');
    }

    public function ResetPassword(Admin $admin)
    {
        return $admin->can('backend.user.update');
    }
}
