<?php

namespace Database\Seeders;

use App\Models\BankAccount;
use App\Models\User;
use App\Models\PointOrder;
use App\Models\Product;
use Illuminate\Database\Seeder;


class PointOrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user      = User::whereCode('SF0002')->first();
        $user->addresses->each(function ($address) use ($user) {
            $products   = Product::select('id', 'point')->inRandomOrder()->limit(2)->get();
            $sumPoint   = $products->sum('point');
            $point_order = PointOrder::factory()->create([
                    'total'                 => $sumPoint,
                    'user_code'             => $user->code,
                    'address_id'            => $address->id,
                    'title'                 => $address->name,
                    'tel'                   => $address->tel,
                    'address'               => $address->address,
                    'province_id'           => $address->province_id,
                    'amphur_id'             => $address->amphur_id,
                    'district_id'           => $address->district_code,
                    'province'              => $address->province->title_th,
                    'amphur'                => $address->amphur->title_th,
                    'district'              => $address->district->title_th,
                    'zipcode'               => $address->zipcode,
                    ]);
                    
            if ($point_order->id) {

                $point_order->products()->sync($products->map(function($product) use ($point_order){
                    return [
                        'order_id'      => $point_order->id,
                        'product_id'    => $product->id,
                        'amount'        => 1,
                        'point'         => $product->point,
                        'total'         => $product->point,
                    ];
                }));
                
                $point_order->point()->create([
                    'type'      => 'withdraw',
                    'user_code' => $point_order->user_code,
                    'amount'    => $point_order->total,
                ]);
            }
        });
    }
}
