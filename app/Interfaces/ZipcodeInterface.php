<?php

namespace App\Interfaces;

interface ZipcodeInterface
{
    const DEFAULT_LIMIT = 25;
    const DEFAULT_SORTING = 'code';
    const DEFAULT_ASCENDING = 'asc';

    /**
     * fetch all zipcode
     *
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function all(array $columns = ['*'], $sorting = ZipcodeInterface::DEFAULT_SORTING, $ascending = ZipcodeInterface::DEFAULT_ASCENDING);

    /**
     * find zipcode by id
     *
     * @param $id
     * @param array $columns
     *
     * @return object|null
     */
    public function findById($id, array $columns = ['*']);

    /**
     * find many zipcode by district code
     *
     * @param int $code
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function findManyByDistrictCode($code, array $columns = ['*']);

}
