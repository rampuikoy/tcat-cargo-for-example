<?php

namespace App\Http\Controllers\Backend\NoRole;

use App\Http\Controllers\Controller;
use App\Interfaces\PermissionInterface;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    protected $permissionInterface;

    public function __construct(PermissionInterface $permissionInterface)
    {
        $this->permissionInterface = $permissionInterface;
    }

    public function all(Request $request)
    {
        $columns = $request->input('columns', ['*']);
        $premissions = $this->permissionInterface->all($columns);
        return response()->json($premissions, $premissions->code);
    }
}
