<?php

namespace App\Interfaces;

interface ChatInterface
{
    const DEFAULT_LIMIT = 25;
    const DEFAULT_SORTING = 'id';
    const DEFAULT_ASCENDING = 'desc';

    /**
     * fetch all chat
     *
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function all(array $columns = ['*'], $sorting = ChatInterface::DEFAULT_SORTING, $ascending = ChatInterface::DEFAULT_ASCENDING);

    /**
     * list chat
     *
     * @param array $conditions
     * @param array $columns
     * @param int $offset
     * @param int $limit
     *
     * @return \Illuminate\Support\Collection
     */
    public function fetchListByFields(array $conditions = [], $offset = 0, $limit = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * pagination chat
     *
     * @param array $conditions
     * @param array $columns
     * @param int $page
     * @param int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateListByFields(array $conditions, $page = 1, $perPage = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * find chat by id
     *
     * @param $code
     * @param $types
     * @param array $columns
     *
     * @return object|null
     */
    public function findByCode($code, array $columns = ['*'],$types);

    /**
     * get chat dropdown list
     *
     * @return object
     */
    public function dropdownList();

    /**
     * post chat read all
     *
     * @return object
     */
    public function readAll();

    /**
     * create new chat
     *
     * @param array $data
     *
     * @return object|null
     */
    public function store(array $data);

}
