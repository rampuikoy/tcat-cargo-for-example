<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class ExtendPolicy
{
    use HandlesAuthorization;

    public function report(Admin $admin)
    {
        return $admin->can('extend.report');
    }

    public function billMenuCreate(Admin $admin)
    {
        return $admin->can('extend.menu-create-bill');
    }

    public function billPrepairQueue(Admin $admin)
    {
        return $admin->can('extend.bill-prepair-queue');
    }

    public function billPackQueue(Admin $admin)
    {
        return $admin->can('extend.bill-pack-queue');
    }

    public function billPack(Admin $admin)
    {
        return $admin->can('extend.bill-pack');
    }

    public function billOut(Admin $admin)
    {
        return $admin->can('extend.bill-out');
    }

    public function billDroppointIn(Admin $admin)
    {
        return $admin->can('extend.bill-droppoint-in');
    }

    public function billGps(Admin $admin)
    {
        return $admin->can('extend.bill-gps');
    }

    public function billBackward(Admin $admin)
    {
        return $admin->can('extend.bill-backward');
    }

    public function billMenuStore(Admin $admin)
    {
        return $admin->can('extend.menu-store-bill');
    }



}
