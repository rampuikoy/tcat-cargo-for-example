<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Carbon\Carbon;
use App\Models\BankAccount;
use Faker\Generator as Faker;
use Illuminate\Support\Arr;

$suftitle = [
    'บริษัท',
    'บจก.',
    'คุณ',
];
$sufbank = [
    'BBL',
    'KTB',
    'BAY',
    'SCB',
    'KBANK',
];
$sufcreate = [
    'SF0002',
    'SF0003',
    'SF0004',
    'SF0005',
    'admin',
];
$sufuserCode = [
    'SF0002',
    'SF0003',
    'SF0004',
    'SF0005',
    null,
];
$sufbranch = [
    'สาขา เซ็นทรัลรัตนาธิเบศน์',
    'ถนน รัตนาธิเบศน์บางใหญ่',
    'ติวานน',
    'บิ๊กซีเเจ้งวัฒนะ',
];

$factory->define(BankAccount::class, function (Faker $faker) use ($suftitle, $sufbank, $sufcreate, $sufbranch,$sufuserCode) {
    $admin_id = random_int(1, 200);
    return [
        'created_at' => Carbon::now(),
        'title' =>  Arr::random($suftitle).' '.$faker->city,
        'account' => $faker->isbn10,
        'branch' => Arr::random($sufbranch),
        'bank' =>  Arr::random($sufbank),
        'user_code' => Arr::random($sufuserCode),
        'created_admin' => Arr::random($sufcreate),
        'updated_admin' => Arr::random($sufcreate),
        'created_admin_id' => $admin_id,
        'updated_admin_id' => $admin_id,

    ];
});
