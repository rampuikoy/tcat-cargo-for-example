<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClaimPolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.claim.index');
    }

    public function show(Admin $admin)
    {
        return $admin->can('backend.claim.show');
    }

    public function store(Admin $admin)
    {
        return $admin->can('backend.claim.store');
    }
    public function update(Admin $admin)
    {
        return $admin->can('backend.claim.update');
    }

    public function destroy(Admin $admin)
    {
        return $admin->can('backend.claim.destroy');
    }

    public function approve(Admin $admin)
    {
        return $admin->can('backend.claim.approve');
    }
    public function cancel(Admin $admin)
    {
        return $admin->can('backend.claim.destroy');
    }
}
