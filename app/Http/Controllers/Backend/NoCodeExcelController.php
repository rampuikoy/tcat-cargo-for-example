<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\NoCode\NoCodeExcelRequest;
use App\Http\Requests\User\ImportExcelRequest;
use App\Interfaces\NoCodeExcelInterface;

class NoCodeExcelController extends Controller
{
    protected $nocodeExcelInterface;

    public function __construct(NoCodeExcelInterface $nocodeExcelInterface)
    {
        $this->nocodeExcelInterface = $nocodeExcelInterface;
    }

    public function importValidate(ImportExcelRequest $request)
    {
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 300);
        $nocodes    = $this->nocodeExcelInterface->validateExcelFile($request->file);
        return response()->json($nocodes, $nocodes->code);
    }

    public function createOrUpdate(NoCodeExcelRequest $request)
    {
        $data           = $request->input('nocode', []);
        $nocodes        = $this->nocodeExcelInterface->createOrUpdate($data);
        return response()->json($nocodes, $nocodes->code);
    }

}
