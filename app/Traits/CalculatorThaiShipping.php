<?php
namespace App\Traits;

/**
 * calculate
 */
trait CalculatorThaiShipping
{
    /**
     * Get thai shipping price for droppoint method
     *
     * @param integer $methodId - Id of thai shipping method
     * @param array $trackings - trackings
     *
     *
     * @return float - $weight_pirce + $cubic_price;
     */
    public function getCalculateDroppoint($methodId, $trackings)
    {
        $columns = ['id', 'title', 'type', 'calculate_type', 'shipping_rate_weight', 'shipping_rate_cubic'];
        $find = app('App\Repositories\ThaiShippingMethodRepository')->findDroppointById($methodId, $columns);

        if ($find->error) {
            throw new \Exception($find->message);
        }

        $rate = $find->results;

        $tmp_tracks = collect($trackings);
        $weight = $tmp_tracks->sum(function ($track) {return $track['calculate_type'] === 'weight' ? $track['weight'] : 0;});
        $cubic = $tmp_tracks->sum(function ($track) {return $track['calculate_type'] !== 'weight' ? $track['weight'] : 0;});

        $weight_pirce = $rate->shipping_rate_weight * $weight;
        $cubic_price = $rate->shipping_rate_cubic * $cubic;
        $price = $weight_pirce + $cubic_price;

        // dd([
        //     'droppoint' => $rate->title,
        //     'kg' => $weight,
        //     'rate_kg' => $rate->shipping_rate_weight,
        //     'price_kg' => $weight_pirce,
        //     'cbm' => $cubic,
        //     'rate_cbm' => $rate->shipping_rate_cubic,
        //     'price_cbm' => $cubic_price,
        //     'total' => $price,
        // ]);

        return $price;
    }

    /**
     * Get thai shipping price for post method
     *
     * @param integer $methodId - Id of thai shipping method
     * @param array $trackings - trackings
     *
     *
     * @return float - $rate->price;
     */
    public function getCalculatePost($methodId, $trackings)
    {
        $find = app('App\Repositories\ThaiShippingWeightPriceRepository')->findManyByShippingId($methodId);

        if ($find->error) {
            throw new \Exception($find->message);
        }

        $total_weight = collect($trackings)->sum('weight');
        $rate = collect($find->results)->first(function ($rate) use ($total_weight) {
            return $rate->min_weight <= $total_weight && $total_weight <= $rate->max_weight;
        });

        if (!$rate) {
            throw new \Exception(__('messages.over_weight'));
        }

        $price = $rate->price;

        // dd([
        //     'post' => $rate->title,
        //     'kg' => $total_weight,
        //     'min kg' => $rate->min_weight,
        //     'max kg' => $rate->max_weight,
        //     'price' => $price,
        // ]);

        return $price;
    }

    /**
     * Get thai shipping price for kerry method
     *
     * @param integer $methodId - Id of thai shipping method
     * @param array $trackings - trackings
     *
     *
     * @return float - $rate->price;
     */
    public function getCalculateKerry($methodId, $trackings)
    {
        $find = app('App\Repositories\ThaiShippingWeightSizePriceRepository')->findManyByShippingId($methodId);

        if ($find->error) {
            throw new \Exception($find->message);
        }

        $rates = $find->results;
        $price = collect($trackings)->sum(function ($track) use ($rates) {
            return $this->findSizeKerry($rates, $track);
        });

        // dd(['kerry price' => $price]);

        return $price;
    }

    /**
     * find size kerry
     *
     * @param integer $rates - thai shipping method
     * @param array $trackings - trackings
     *
     *
     * @return float - $rate->price;
     */
    private function findSizeKerry($rates, $track)
    {
        // check from weight
        $weight = collect($rates)->first(function ($rate) use ($track) {
            return $rate->min_weight <= $track['weight'] && $track['weight'] <= $rate->max_weight;
        });

        // check from size
        $sum = $track['width'] + $track['height'] + $track['length'];
        $size = collect($rates)->first(function ($rate) use ($sum) {
            return $sum <= $rate->max_size;
        });

        if (!$weight && !$size) {
            throw new \Exception(__('messages.kerry_not_found'));
        }

        $weight_price = $weight->price ?? 0;
        $size_price = $size->price ?? 0;

        // dd([
        //     'track' => $track['code'],
        //     'title_weight' => $weight['title'],
        //     'weight' => $track['weight'],
        //     'price_weight' => $weight['price'],
        //     'title_size' => $size['title'],
        //     'size' => $sum,
        //     'price_size' => $size['price'],
        // ]);

        return $weight_price > $size_price ? $weight_price : $size_price;
    }

    /**
     * Get thai shipping price for nim method
     *
     * @param integer $methodId - Id of thai shipping method
     * @param integer $provinceId - Id of province
     * @param array $trackings - trackings
     *
     *
     * @return float - $rate->price;
     */
    public function getCalculateNim($methodId, $trackings, $provinceId)
    {
        $find = app('App\Repositories\ThaiShippingAreaPriceRepository')->findManyByShippingId($methodId);

        if ($find->error) {
            throw new \Exception($find->message);
        }

        $province = collect($find->results)->first(function ($rate) use ($provinceId) {
            return $rate->province->id === $provinceId;
        });
        if (!$province) {
            throw new \Exception(__('messages.not_province'));
        }

        $total_cubic = collect($trackings)->sum(function ($track) {
            return ($track['calculate_type'] === 'weight' && $track['cubic'] <= 0) ? $track['weight'] / 200 : $track['cubic'];
        });

        $counter = $this->getCountNim($total_cubic);
        $price = collect($counter)->reduceWithKeys(function ($sum, $value, $key) use ($province) {
            return ($sum += floatval($value) * floatval($province['price_' . $key]));
        }, 0);

        // dd([
        //     'nim' => null,
        //     'province' => $province->title_th,
        //     'counter' => $counter,
        //     'price' => $price,
        // ]);

        return $price;
    }

    private function getCountNim($dimention)
    {
        $prices = [
            'a' => 0.03,
            'b' => 0.05,
            'c' => 0.085,
            'd' => 0.12,
            'e' => 0.18,
        ];

        $counter = [
            'a' => 0,
            'b' => 0,
            'c' => 0,
            'd' => 0,
            'e' => 0,
        ];

        $tmp_cbm = floatval($dimention);

        while ($tmp_cbm > 0) {
            if ($tmp_cbm > $prices['e']) {
                $tmp_cbm -= floatval($prices['e']);
                $counter['e'] += 1;
            } else if ($tmp_cbm > $prices['d']) {
                $tmp_cbm -= floatval($prices['d']);
                $counter['d'] += 1;
            } else if ($tmp_cbm > $prices['c']) {
                $tmp_cbm -= floatval($prices['c']);
                $counter['c'] += 1;
            } else if ($tmp_cbm > $prices['b']) {
                $tmp_cbm -= floatval($prices['b']);
                $counter['b'] += 1;
            } else {
                $tmp_cbm -= floatval($prices['a']);
                $counter['a'] += 1;
            }
        }

        return $counter;
    }
}
