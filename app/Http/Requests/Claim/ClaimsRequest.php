<?php

namespace App\Http\Requests\Claim;

use Illuminate\Foundation\Http\FormRequest;

class ClaimsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.claim.store');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_code'        => 'required|exists:users,code',
            'shipping'         => 'required|in:car,ship',
            'contacted_admin'  => 'required',
            'city'             => 'required',
            'china_in'         => 'required|date_format:Y-m-d',
            'sub_total'        => 'required|numeric|min:0',
            'total'            => 'required|numeric|min:0',
            'items'            => 'required',
            'items.*.tracking' => 'exists:trackings,code',
            'items.*.price'    => 'required|numeric|min:0',
            'items.*.amount'   => 'required|numeric|min:0',
            'items.*.total'    => 'required|numeric|min:0',
            'items.*.type'     => 'required|in:lose,lost_some,broken',
        ];
    }
}
