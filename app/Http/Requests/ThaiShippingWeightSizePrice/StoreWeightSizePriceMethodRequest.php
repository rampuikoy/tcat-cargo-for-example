<?php

namespace App\Http\Requests\ThaiShippingWeightSizePrice ;

use App\Http\Requests\FormRequest;

class StoreWeightSizePriceMethodRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.thai-shipping-weight-size-price.store');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'thai_shipping_method_id'   => 'required|exists:thai_shipping_methods,id',
            'title'                     => 'required',
            'min_weight'                => 'required|numeric|min:0',
            'max_weight'                => 'required|numeric|gt:min_weight',
            'max_size'                  => 'required|numeric|min:0',
            'price'                     => 'required|numeric|min:0',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'thai_shipping_method_id'   => __('form.thai_shipping_weight_size_price.thai_shipping_method_id'),
            'title'                     => __('form.thai_shipping_weight_size_price.title'),
            'min_weight'                => __('form.thai_shipping_weight_size_price.min_weight'),
            'max_weight'                => __('form.thai_shipping_weight_size_price.max_weight'),
            'max_size'                  => __('form.thai_shipping_weight_size_price.max_size'),
            'price'                     => __('form.thai_shipping_weight_size_price.price'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
