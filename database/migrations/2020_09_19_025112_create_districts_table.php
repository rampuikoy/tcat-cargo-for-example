<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDistrictsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('districts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 6);
            $table->string('title_th', 150);
            $table->string('title_en', 150);
            $table->integer('geo_id');

            $table->integer('amphur_id')->unsigned()->index();
            // $table->foreign('amphur_id')->references('id')->on('amphurs');

            $table->integer('province_id')->unsigned()->index();
            // $table->foreign('province_id')->references('id')->on('provinces');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('districts');
    }
}
