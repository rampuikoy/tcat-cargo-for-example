<?php

use App\Models\Admin;
use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\Helpers\MoveSqlData;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $move_admin_data    = collect(MoveSqlData::insertAdminTable());
        $message            = $move_admin_data->get('message');
        $error              = $move_admin_data->get('error');

        if($error){
            $this->command->error($message);
            $this->command->warning('Please insert fresh again');
            return;
        }else{
            $this->command->info($message);
        }

        Role::firstOrCreate([
            'name'              => 'SuperAdmin',
            'guard_name'        => 'admins'
        ]);

        Admin::create([
            'name'              => 'Admin',
            'lastname'          => 'Me',
            'username'          => 'superadmin',
            'email'             => 'admin@me.com',
            'password'          => Hash::make('12345678'),
            'role'              => 'SuperAdmin',
            'email_verified_at' =>  Carbon::now()
        ]);

    }
}
