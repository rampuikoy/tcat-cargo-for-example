<?php

namespace App\Observers;

use App\Models\UserAddress;
use App\Observers\BaseObserver;

class UserAddressObserver extends BaseObserver
{
    //
    public function creating(UserAddress $userAddress)
    {
        $this->stampCreated($userAddress);
    }

    public function updating(UserAddress $userAddress)
    {
        $this->stampCreated($userAddress);
    }
}
