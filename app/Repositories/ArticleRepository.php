<?php

namespace App\Repositories;

use App\Interfaces\ArticleInterface;
use App\Models\Article;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use App\Traits\UploadTrait;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class ArticleRepository implements ArticleInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, UploadTrait, CoreResponseTrait;

    const MINUTE_CACHE          = 1;

    public function __construct(Article $article)
    {
        $this->article = $article;
    }

    public function all(array $columns = ['*'], $sorting = ArticleInterface::DEFAULT_SORTING, $ascending = ArticleInterface::DEFAULT_ASCENDING)
    {
        $key            = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time           = $this->getTime(self::MINUTE_CACHE);

        $articles     = Cache::tags([
            $this->getClassPrefix(),
            $this->getAllWithClassPrefix(),
        ])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
            return $this->article::select($columns)
                ->orderBy($sorting, $ascending)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.article')]), $articles);
    }

    public function fetchListByFields(array $conditions, array $columns = ['*'], $offset = 0, $limit = ArticleInterface::DEFAULT_LIMIT, $sorting = ArticleInterface::DEFAULT_SORTING, $ascending = ArticleInterface::DEFAULT_ASCENDING)
    {
        $key            = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time           = $this->getTime(self::MINUTE_CACHE);


        $articles       = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
            return $this->article::select($columns)
                ->search($conditions)
                ->orderBy($sorting, $ascending)
                ->skip($offset)
                ->limit($limit)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.article')]), $articles);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = ArticleInterface::DEFAULT_LIMIT, $sorting = ArticleInterface::DEFAULT_SORTING, $ascending = ArticleInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $articles   = Cache::tags([$this->getClassPrefix(), $this->getPaginationWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
            $model = $this->article::select($columns)
                ->search($conditions)
                ->orderBy($sorting, $ascending);

            if ($page == 1) {
                return $model->paginate($limit);
            } else {
                return $model->skip($limit * ($page - 1))->paginate($limit);
            }
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.article')]), $articles);
    }

    public function findById($id, array $columns = ['*'])
    {
        $article = $this->findByIdMethod($id, $columns);

        if (!$article) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.article'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.article')]), $article);
    }

    public function dropdownList()
    {
        $key        = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $dropdown = Cache::remember($key, $time, function () {
            return [
                'type' => $this->article::typeList(),
                'status' => $this->article::statusList(),
                'date_type' => $this->article::dateList(),
            ];
        });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function store(array $data)
    {
        if (@$data['file']) {
            $data['file_path'] = $this->storeFile($data['file']);
        }

        $article    = $this->article::create($data);

        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.article')]), $article);
    }

    public function updateById($id, array $data)
    {

        $article = $this->findByIdMethod($id);

        if (!$article) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.article'), 'id' => $id]));
        }

        if (@$data['file']) {
            $data['file_path'] = $this->storeFile($data['file']);
        }

        $article->update($data);

        Cache::tags([$this->getIdWithClassPrefix($article->id)])->flush();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.article')]), $article);
    }

    public function softDeleteById($id)
    {

        $article =  $this->findByIdMethod($id);
        if (!$article) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.article'), 'id' => $id]));
        }

        $article->delete();
        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.delete_data', ['data' => __('messages.article')]), $article);
    }

    private function storeFile($file)
    {
        if (!$file) {
            return null;
        }

        $chr = Str::random(40);
        $fileName = date('ymdHis') . '_' . $chr . '.' . $this->getExtensionFile($file);
        $this->uploadBase64($file, Article::$imageFolder, $fileName);
        $this->uploadBase64($file, Article::$thumbnailFolder, $fileName, $this->article::$thumbnailWidth, $this->article::$thumbnailHeight);
        return $fileName;
    }


    private function clearCacheTags()
    {

        Cache::tags([
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix()
        ])->flush();
    }

    private function findByIdMethod($id, array $columns = ['*'])
    {
        $key        = $this->getCacheKey(null, $id);
        $time       = $this->getTime(self::MINUTE_CACHE);

        $article    = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($id)
        ])->remember($key, $time, function () use ($id, $columns) {
            return $this->article::select($columns)->whereId($id)->first();
        });

        return $article;
    }
}
