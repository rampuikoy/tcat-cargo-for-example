<?php

namespace App\Http\Requests\PointOrder;

use App\Http\Requests\FormRequest;

class ConfirmPointOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.point-order.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'shipping_method_id'        => 'required|exists:thai_shipping_methods,id',
            'tracking'                  => 'required|string',
            'shipping_cost'             => 'sometimes|nullable|numeric',
        ];
        return $rules;
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'shipping_method_id'        => __('form.point_order.shipping_method_id'),
            'tracking'                  => __('form.point_order.tracking'),
            'shipping_cost'             => __('form.point_order.shipping_cost'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
