<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class PointOrderPolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.point-order.index');
    }

    public function show(Admin $admin)
    {
        return $admin->hasAnyPermission(['backend.point-order.show', 'backend.point-order.update']);
    }

    public function store(Admin $admin)
    {
        return $admin->can('backend.point-order.store');
    }

    public function update(Admin $admin)
    {
        return $admin->can('backend.point-order.update');
    }

    public function approve(Admin $admin)
    {
        return $admin->can('backend.point-order.approve');
    }

    public function destroy(Admin $admin)
    {
        return $admin->can('backend.point-order.destroy');
    }
}
