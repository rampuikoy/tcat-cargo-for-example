<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Permission;
use App\Models\Admin;
use Tymon\JWTAuth\Facades\JWTAuth;

class PermissionTest extends TestCase
{
  use WithFaker;
  protected $user;
  protected $permission;
  protected $url = '/api/backend/permission';
  /**
   * A basic feature test example.
   *
   * @return void
   */
  protected function authenticate(string $guard = 'admins')
  {
    $this->user = factory(Admin::class)->create();
    auth()->shouldUse($guard);
    $this->permission = Permission::create([
      'name'          => 'Tcat-test',
      'tag'           => 'Tcat-test',
      'description'   => 'Tcat-test',
      'guard_name'    => 'admins',
    ]);
    $token = JWTAuth::fromUser($this->user);
    return 'Bearer ' . $token;
  }

  protected function deleteUser()
  {
    $this->permission->forceDelete();
    $this->user->forceDelete();
  }

  /** @test */
  public function GetPermissionSearchSuccessTest()
  {
    $id = random_int(1, 200);
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', "{$this->url}/search", [
      "id" => $id,
      "search" => "",
      "name" => ""
    ]);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function GetPermissionSearchFailTest()
  {
    $response = $this->json('GET', "{$this->url}/search");
    $response->assertStatus(401);
  }

  /** @test */
  public function GetPermissionDropdownSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', "{$this->url}/dropdown");
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function GetPermissionDropdownFailTest()
  {
    $response = $this->json('GET', "{$this->url}/dropdown");
    $response->assertStatus(401);
  }

  /** @test */
  public function GetPermissionAllSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', $this->url);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function GetPermissionAllFailTest()
  {
    $response = $this->json('GET', $this->url);
    $response->assertStatus(401);
  }

  /** @test */
  public function PermissionPostSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('POST', $this->url, [
      'name'          => 'test',
      'tag'           => 'test',
      'description'   => 'test',
      'guard_name'    => 'admins',
    ]);
    $response->assertStatus(200);
    Permission::where('name', 'test')->forceDelete();
    $this->deleteUser();
  }

  /** @test */
  public function PermissionPostFailAuthTest()
  {
    $response = $this->json('POST', $this->url);
    $response->assertStatus(401);
  }

  /** @test */
  public function PermissionPostFailTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('POST', $this->url);
    $response->assertStatus(422);
    $this->deleteUser();
  }

  /** @test */
  public function PermissionUpdateByIdSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('PUT', "{$this->url}/{$this->permission->id}", [
      'name'          => 'test1',
      'tag'           => 'test1',
      'description'   => 'test1',
      'guard_name'    => 'admins',
    ]);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function PermissionUpdateByIdFailAuthTest()
  {
    $response = $this->json('PUT', "{$this->url}/20");
    $response->assertStatus(401);
  }

  
  public function PermissionUpdateByIdFailTest()
  {
    //TODO Not have vaildate
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' =>  $token,
    ])->json('PUT', "{$this->url}/{$this->permission->id}");
    $response->assertStatus(422);
    $this->deleteUser();
  }

  /** @test */
  public function PermissionDeleteSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('DELETE', "{$this->url}/{$this->permission->id}");
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function PermissionDeleteFailTest()
  {
    $response = $this->json('DELETE', "{$this->url}/5");
    $response->assertStatus(401);
  }

  /** @test */
  public function PermissionDeleteManySuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('POST', "{$this->url}/delete-all", [
      "ids" => [$this->permission->id]
    ]);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function PermissionManyDeleteFailTest()
  {
    $response = $this->json('POST', "{$this->url}/delete-all");
    $response->assertStatus(401);
  }
}
