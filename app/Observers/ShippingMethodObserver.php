<?php

namespace App\Observers;

use App\Models\ShippingMethod;
use App\Observers\BaseObserver;

class ShippingMethodObserver extends BaseObserver
{
    /**
     * Handle the shipping_method "creating" event.
     *
     * @param  \App\Models\ShippingMethod  $shipping_method
     * @return void
     */
    public function creating(ShippingMethod $shipping_method)
    {
        //
        $this->stampCreated($shipping_method);
    }

    /**
     * Handle the shipping_method "updating" event.
     *
   * @param  \App\Models\ShippingMethod  $shipping_method
     * @return void
     */
    public function updating(ShippingMethod $shipping_method)
    {
        //
        $this->stampUpdated($shipping_method);
    }

}
