<?php

namespace App\Http\Requests\Tracking;

use App\Http\Requests\FormRequest;

class TrackingDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.tracking.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_type'     => 'required|in:clothing,shoes,bag,accessories,electrical,it,car,home,office,stationery,handyman_tool,pet,toy,furniture,food_and_drug,cosmetics,other',
            'product_remark'   => 'required_if:product_type,other',
            'qty'              => 'required|numeric',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'product_type'           => __('form.tracking_detail.product_type'),
            'product_remark'         => __('form.tracking_detail.product_remark'),
            'qty'                    => __('form.tracking_detail.qty'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
