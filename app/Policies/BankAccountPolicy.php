<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class BankAccountPolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.bank-account.index');
    }
    public function show(Admin $admin)
    {
        return $admin->hasAnyPermission(['backend.bank-account.show', 'backend.bank-account.update']);
    }

    public function store(Admin $admin)
    {
        return $admin->can('backend.bank-account.store');
    }
    public function update(Admin $admin)
    {
        return $admin->can('backend.bank-account.update');
    }

    public function destroy(Admin $admin)
    {
        return $admin->can('backend.bank-account.destroy');
    }

}
