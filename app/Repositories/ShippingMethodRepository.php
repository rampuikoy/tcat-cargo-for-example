<?php

namespace App\Repositories;

use App\Interfaces\ShippingMethodInterface;
use App\Models\ShippingMethod;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use Illuminate\Support\Facades\Cache;

class ShippingMethodRepository implements ShippingMethodInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const MINUTE_CACHE          = 1;

    public function __construct(ShippingMethod $shippingMethod)
    {
        $this->shippingMethod        = $shippingMethod;
    }

    public function all(array $columns = ['*'], $sorting = ShippingMethodInterface::DEFAULT_SORTING, $ascending = ShippingMethodInterface::DEFAULT_ASCENDING)
    {
        $key            = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time           = $this->getTime(self::MINUTE_CACHE);

        $shippings      = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
            return $this->shippingMethod::select($columns)->orderBy($sorting, $ascending)->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.oversea_shipping')]), $shippings);
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = ShippingMethodInterface::DEFAULT_LIMIT, $sorting = ShippingMethodInterface::DEFAULT_SORTING, $ascending = ShippingMethodInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $shippings  = Cache::tags($this->getClassPrefix(), $this->getFetchWithClassPrefix())->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
            return $this->shippingMethod::select($columns)
                ->search($conditions)
                ->orderBy($sorting, $ascending)
                ->skip($offset)
                ->limit($limit)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.oversea_shipping')]), $shippings);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = ShippingMethodInterface::DEFAULT_LIMIT, $sorting = ShippingMethodInterface::DEFAULT_SORTING, $ascending = ShippingMethodInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $shippings  = Cache::tags([$this->getClassPrefix(), $this->getPaginationWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
            $model = $this->shippingMethod::select($columns)
                ->search($conditions)
                ->orderBy($sorting, $ascending);

            if ($page == 1) {
                return $model->paginate($limit);
            } else {
                return $model->skip($limit * ($page - 1))->paginate($limit);
            }
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.oversea_shipping')]), $shippings);
    }

    public function findById($id, array $columns = ['*'])
    {
        $shipping = $this->findByIdMethod($id, $columns);

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.oversea_shipping')]), $shipping);
    }

    public function dropdownList()
    {
        $key        = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $dropdown   = Cache::remember($key, $time, function () {
            return [
                'status' => $this->shippingMethod::statusList()
            ];
        });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function store(array $data)
    {
        $shipping   = $this->shippingMethod::create($data);

        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.oversea_shipping')]), $shipping);
    }

    public function updateById($id, array $data)
    {
        $shipping       = $this->findByIdMethod($id);

        if (!$shipping) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.oversea_shipping'), 'id' => $id]));
        }

        $shipping->update($data);

        Cache::tags([$this->getIdWithClassPrefix($id)]);

        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.oversea_shipping')]), $shipping);
    }

    public function softDeleteById($id)
    {
        $shipping       = $this->findByIdMethod($id);

        if (!$shipping) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.oversea_shipping'), 'id' => $id]));
        }

        $shipping->delete();

        Cache::tags([$this->getIdWithClassPrefix($id)]);

        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.delete_data', ['data' => __('messages.oversea_shipping')]), $shipping);
    }

    private function clearCacheTags()
    {
        $keys = [
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix(),
        ];

        Cache::tags($keys)->flush();
    }

    private function findByIdMethod($id, array $columns = ['*'])
    {
        $key    = $this->getCacheKey(null, $id);
        $time   = $this->getTime(self::MINUTE_CACHE);

        $shipping  = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($id)
        ])->remember($key, $time, function () use ($id, $columns) {
            return $this->shippingMethod::select($columns)->whereId($id)->first();
        });

        if (!$shipping) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.oversea_shipping'), 'id' => $id]));
        }

        return $shipping;
    }
}
