<?php

namespace App\Observers;

use App\Models\PackingQueue;
use App\Observers\BaseObserver;

class PackingQueueObserver extends BaseObserver
{
    /**
     * Handle the packingQueue "creating" event.
     *
     * @param  \App\Models\PackingQueue  $packingQueue
     * @return void
     */
    public function creating(PackingQueue $packingQueue)
    {
        //
        $this->stampCreated($packingQueue);
    }

    /**
     * Handle the packingQueue "updating" event.
     *
   * @param  \App\Models\PackingQueue  $packingQueue
     * @return void
     */
    public function updating(PackingQueue $packingQueue)
    {
        //
    }

}
