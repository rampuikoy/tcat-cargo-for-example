<?php

namespace App\Http\Requests\ExchangeRate;

use App\Http\Requests\FormRequest;

class StoreExchangeRateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.exchange-rate.store');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rate'      => 'required|regex:/^\d+(\.\d{1,2})?$/',
            'raw_rate'  => 'required|regex:/^\d+(\.\d{1,2})?$/',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'rate'          => __('form.exchange_rate.rate'),
            'raw_rate'      => __('form.exchange_rate.raw_rate'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
