<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Zipcode extends Model
{
    //
    protected $fillable = [
        'district_code',
        'zipcode',
    ];

    /**
     * relation
     */
    public function district()
    {
        return $this->belongsTo('App\Models\District', 'district_code', 'code');
    }
}
