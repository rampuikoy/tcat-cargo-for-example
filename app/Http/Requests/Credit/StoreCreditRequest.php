<?php

namespace App\Http\Requests\Credit;

use App\Http\Requests\FormRequest;

class StoreCreditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.credit.store');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'creditable_type' => 'required',
            'creditable_id' => 'required',
            'amount' => 'required',
            'type' => 'required|in:topup,withdraw',
        ];
    }
}
