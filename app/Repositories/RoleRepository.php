<?php

namespace App\Repositories;

use App\Interfaces\RoleInterface;
use App\Models\Role;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use App\Traits\UploadTrait;
use Illuminate\Support\Facades\Cache;

class RoleRepository implements RoleInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, UploadTrait, CoreResponseTrait;

    const MINUTE_CACHE          = 1;

    public function __construct(Role $role)
    {
        $this->role         = $role;
    }

    public function all(array $columns = ['*'], $sorting = RoleInterface::DEFAULT_SORTING, $ascending = RoleInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $roles      = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
            $callback = function ($query) {
                $query->select(['id', 'name', 'tag', 'description']);
            };
            return $this->role::select($columns)->with(['permissions' => $callback])->orderBy($sorting, $ascending)->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.role')]), $roles);
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = RoleInterface::DEFAULT_LIMIT, $sorting = RoleInterface::DEFAULT_SORTING, $ascending = RoleInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $roles      = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
            return $this->role::select($columns)
                ->search($conditions)
                ->orderBy($sorting, $ascending)
                ->skip($offset)
                ->limit($limit)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.role')]), $roles);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = RoleInterface::DEFAULT_LIMIT, $sorting = RoleInterface::DEFAULT_SORTING, $ascending = RoleInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $roles      = Cache::tags([$this->getClassPrefix(), $this->getPaginationWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
            $model = $this->role::select($columns)
                ->search($conditions)
                ->orderBy($sorting, $ascending);

            if ($page == 1) {
                return $model->paginate($limit);
            } else {
                return $model->skip($limit * ($page - 1))->paginate($limit);
            }
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.role')]), $roles);
    }

    public function findById($id, array $columns = ['*'])
    {
        $role = $this->findByIdMethod($id, $columns);

        if (!$role) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.admin'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.role')]), $role);
    }

    public function store(array $data)
    {
        $role   = $this->role::create($data);

        if (@$data['permissions'] && count(@$data['permissions']) > 0) {
            $role->syncPermissions($data['permissions']);
        }

        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.role')]), $role);
    }

    public function updateById($id, array $data)
    {

        $role   = $this->findByIdMethod($id);

        if (!$role) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.admin'), 'id' => $id]));
        }

        $role->update($data);

        if (@$data['permissions'] && count(@$data['permissions']) > 0) {
            $role->syncPermissions($data['permissions']);
        }

        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();

        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.role')]), $role);
    }

    public function softDeleteById($id)
    {
        $role   = $this->findByIdMethod($id);

        if (!$role) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.admin'), 'id' => $id]));
        }

        $role->delete();

        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();

        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.delete_data', ['data' => __('messages.role')]), $role);
    }

    private function clearCacheTags()
    {
        $keys = [
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix()
        ];

        Cache::tags($keys)->flush();
    }

    private function findByIdMethod($id, array $columns = ['*'])
    {
        $key    = $this->getCacheKey(null, 'ID.' . $id);
        $time   = $this->getTime(self::MINUTE_CACHE);

        $role   = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($id),
        ])->remember($key, $time, function () use ($id, $columns) {
            $callback = function ($query) {
                $query->select(['id', 'name', 'tag', 'description']);
            };
            return $this->role::select($columns)->with(['permissions' => $callback])->find($id);
        });

        return $role;
    }
}
