<?php

namespace App\Interfaces;

interface ExchangeRateInterface
{
    const DEFAULT_LIMIT = 25;
    const DEFAULT_SORTING = 'id';
    const DEFAULT_ASCENDING = 'desc';

    /**
     * fetch all exchangerate
     *
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function all(array $columns = ['*'], $sorting = ExchangeRateInterface::DEFAULT_SORTING, $ascending = ExchangeRateInterface::DEFAULT_ASCENDING);

    /**
     * list exchangerate
     *
     * @param array $conditions
     * @param array $columns
     * @param int $offset
     * @param int $limit
     *
     * @return \Illuminate\Support\Collection
     */
    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * pagination exchangerate
     *
     * @param array $conditions
     * @param array $columns
     * @param int $page
     * @param int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $perPage = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * find exchangerate by id
     *
     * @param $id
     * @param array $columns
     *
     * @return object|null
     */
    public function findById($id, array $columns = ['*']);

     /**
     * find exchangerate by id
     *
     * @param $code
     * @param array $columns
     *
     * @return object|null
     */
    public function getRateExtend($code);

    /**
     * create new exchangerate
     *
     * @param array $data
     *
     * @return object|null
     */
    public function store(array $data);

    /**
     * update exchangerate by id
     *
     * @param $id
     * @param array $data
     *
     * @return object
     */
    public function updateById($id, array $data);

	/**
     * soft delete exchangerate by id
     *
     * @param $id
     *
     * @return object|null
     */
    public function softDeleteById($id);

}
