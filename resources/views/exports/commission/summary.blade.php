<table cellspacing="0" border="1">
  <caption>export comission summary</caption>
  <thead>
    <tr>
      <th scope="col"></th>
      <th scope="col"></th>
      <th scope="col" rowspan="3" colspan="13"
        style="border:1px solid #000000;background-color:#dddd7d;font-size:36px;text-align:center;vertical-align:center; font-weight: bold;">
        สรุปรายการค่าคอม
      </th>
    </tr>
    <tr>
      <th scope="col"></th>
      <th scope="col"></th>
    </tr>
    <tr>
      <th scope="col"></th>
      <th scope="col"></th>
    </tr>
    <tr>
      <th scope="col"></th>
      <th scope="col"></th>
      <th scope="col" rowspan="2" colspan="2"
        style="border:1px solid #000000;background-color:#d9d9d9;font-size:16px;text-align:center;vertical-align:center; font-weight: bold;">
        ลำดับที่
      </th>
      <th scope="col" rowspan="2" colspan="5"
        style="border:1px solid #000000;background-color:#bdd7ee;font-size:16px;text-align:center;vertical-align:center; font-weight: bold;">
        รายการ
      </th>
      <th scope="col" rowspan="2" colspan="2"
        style="border:1px solid #000000;background-color:#c9c9c9;font-size:16px;text-align:center;vertical-align:center; font-weight: bold;">
        รายรับ
      </th>
      <th scope="col" rowspan="2" colspan="2"
        style="border:1px solid #000000;background-color:#c5e0b4;font-size:16px;text-align:center;vertical-align:center; font-weight: bold;">
        รายการหัก
      </th>
      <th scope="col" rowspan="2" colspan="2"
        style="border:1px solid #000000;background-color:#d9d9d9;font-size:16px;text-align:center;vertical-align:center; font-weight: bold;">
        คงเหลือ
      </th>
    </tr>
    <tr>
      <th scope="col"></th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="height:20px;"></td>
      <td></td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#f2f2f2;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
        1
      </td>
      <td colspan="5"
        style="border:1px solid #000000;background-color:#deebf7;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
        ค่าคอม ( เรทปกติ )
      </td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#dbdbdb;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
        {{ $commissions['normal_increase'] }}
      </td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#e2f0d9;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
        0.00
      </td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#ffffff;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
      </td>
    </tr>
    <tr>
      <td style="height:20px;"></td>
      <td></td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#f2f2f2;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
        2
      </td>
      <td colspan="5"
        style="border:1px solid #000000;background-color:#deebf7;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
        ค่าคอม ( เรทพิเศษ )
      </td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#dbdbdb;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
        {{ $commissions['special_increase'] }}
      </td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#e2f0d9;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
        0.00
      </td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#ffffff;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
      </td>
    </tr>
    <tr>
      <td style="height:20px;"></td>
      <td></td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#f2f2f2;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
        3
      </td>
      <td colspan="5"
        style="border:1px solid #000000;background-color:#deebf7;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
        หัก 50% ค่าคอม
      </td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#dbdbdb;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
      </td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#e2f0d9;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
      </td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#ffffff;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
      </td>
    </tr>
    <tr>
      <td style="height:20px;"></td>
      <td></td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#f2f2f2;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
        4
      </td>
      <td colspan="5"
        style="border:1px solid #000000;background-color:#deebf7;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
        เงินเดือน(ประจำ)
      </td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#dbdbdb;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
        0.00
      </td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#e2f0d9;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
        0.00
      </td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#ffffff;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
      </td>
    </tr>
    <tr>
      <td style="height:20px;"></td>
      <td></td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#f2f2f2;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
        5
      </td>
      <td colspan="5"
        style="border:1px solid #000000;background-color:#deebf7;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
        ค่าน้ำมัน
      </td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#dbdbdb;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
        0.00
      </td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#e2f0d9;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
        0.00
      </td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#ffffff;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
      </td>
    </tr>
    <tr>
      <td style="height:20px;"></td>
      <td></td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#f2f2f2;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
        6
      </td>
      <td colspan="5"
        style="border:1px solid #000000;background-color:#deebf7;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
      </td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#dbdbdb;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
      </td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#e2f0d9;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
      </td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#ffffff;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
      </td>
    </tr>
    <tr>
      <td style="height:20px;"></td>
      <td></td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#f2f2f2;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
        7
      </td>
      <td colspan="5"
        style="border:1px solid #000000;background-color:#deebf7;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
      </td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#dbdbdb;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
      </td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#e2f0d9;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
      </td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#ffffff;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
      </td>
    </tr>
    <tr>
      <td style="height:20px;"></td>
      <td></td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#f2f2f2;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
        8
      </td>
      <td colspan="5"
        style="border:1px solid #000000;background-color:#deebf7;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
      </td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#dbdbdb;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
      </td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#e2f0d9;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
      </td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#ffffff;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
      </td>
    </tr>
    <tr>
      <td style="height:20px;"></td>
      <td></td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#f2f2f2;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
        9
      </td>
      <td colspan="5"
        style="border:1px solid #000000;background-color:#deebf7;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
      </td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#dbdbdb;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
      </td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#e2f0d9;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
      </td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#ffffff;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
      </td>
    </tr>
    <tr>
      <td style="height:20px;"></td>
      <td></td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#f2f2f2;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
        10
      </td>
      <td colspan="5"
        style="border:1px solid #000000;background-color:#deebf7;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
      </td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#dbdbdb;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
      </td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#e2f0d9;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
      </td>
      <td colspan="2"
        style="border:1px solid #000000;background-color:#ffffff;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
      </td>
    </tr>
  </tbody>
  <tfoot>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td rowspan="2" colspan="2"
        style="border:1px solid #000000;background-color:#92d050;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
        สรุปยอด
      </td>
      <td rowspan="2" colspan="2"
        style="border:1px solid #000000;background-color:#c5e0b4;font-size:14px;text-align:center;vertical-align:center; font-weight: bold;">
      </td>
    </tr>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
  </tfoot>
</table>
