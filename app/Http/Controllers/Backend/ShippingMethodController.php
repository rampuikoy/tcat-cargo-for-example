<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\ShippingMethod;
use App\Http\Controllers\Controller;
use App\Interfaces\ShippingMethodInterface;
use App\Http\Requests\ShippingMethod\StoreShippingMethodRequest;
use App\Http\Requests\ShippingMethod\UpdateShippingMethodRequest;

class ShippingMethodController extends Controller
{
    protected $shippingMethodInterface;

    public function __construct(ShippingMethodInterface $shippingMethodInterface)
    {
        $this->shippingMethodInterface = $shippingMethodInterface;
    }

    public function index(Request $request)
    {
        $this->authorize('index', ShippingMethod::class);
        $columns                = $request->input('columns', ['*']);
        $shippingMethods        = $this->shippingMethodInterface->all($columns);
        return response()->json($shippingMethods, $shippingMethods->code);
    }

    public function show(Request $request, $id)
    {
        $this->authorize('show', ShippingMethod::class);
        $columns                = $request->input('columns', ['*']);
        $shippingMethod         = $this->shippingMethodInterface->findById($id, $columns);
        return response()->json($shippingMethod, $shippingMethod->code);
    }

    public function search(Request $request)
    {
        $this->authorize('index', ShippingMethod::class);
        $conditions     = $request->only(['id', 'title_th', 'title_en', 'title_cn', 'status','search']);
        $columns        = $request->input('columns', ['*']);
        $limit          = $request->input('limit', $this->shippingMethodInterface::DEFAULT_LIMIT);
        $sorting        = $request->input('order_by', $this->shippingMethodInterface::DEFAULT_SORTING);
        $ascending      = $request->input('ascending') == 1 ? 'asc' : 'desc';

        if ($request->boolean('pagination')) {
            $page             = $request->input('page', 1);
            $shippingMethods  = $this->shippingMethodInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset           = $request->input('page', 0);
            $shippingMethods  = $this->shippingMethodInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($shippingMethods, $shippingMethods->code);
    }

    public function dropdown()
    {
        $dropdown = $this->shippingMethodInterface->dropdownList();
        return response()->json($dropdown, $dropdown->code);
    }

    public function store(StoreShippingMethodRequest $request)
    {
        $data              = $request->only([
                                'title_th',
                                'title_en',
                                'title_cn',
                                'status',
                                'remark',
                            ]);
        $shippingMethod    = $this->shippingMethodInterface->store($data);
        return response()->json($shippingMethod, $shippingMethod->code);
    }

    public function update(UpdateShippingMethodRequest $request, $id)
    {
        $data              = $request->only([
                                'title_th',
                                'title_en',
                                'title_cn',
                                'status',
                                'remark',
                            ]);
        $shippingMethod    = $this->shippingMethodInterface->updateById($id, $data);
        return response()->json($shippingMethod, $shippingMethod->code);
    }

    public function destroy($id)
    {
        $this->authorize('destroy', ShippingMethod::class);
        $shippingMethod     = $this->shippingMethodInterface->softDeleteById($id);
        return response()->json($shippingMethod, $shippingMethod->code);
    }
}
