<?php

namespace App\Models;

use App\Helpers\ScopeQueries;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    //
    use SoftDeletes;

    public static $imageFolder = 'uploads/article/';
    public static $thumbnailFolder = 'uploads/article/thumbnail/';
    public static $thumbnailWidth = 300;
    public static $thumbnailHeight = 300;

    public static function statusList()
    {
        $array = [
            'active'    => __('messages.active'),
            'inactive'  => __('messages.inactive'),
        ];
        return $array;
    }

    public static function typeList()
    {
        $array = [
            'article'       => __('messages.article'),
            'news'          => __('messages.news'),
            'promotion'     => __('messages.promotion'),
        ];
        return $array;
    }

    public static function dateList()
    {
        $array = [
            'created_at'    => __('messages.created_at'),
            'updated_at'    => __('messages.updated_at'),
            'published_at'  => __('messages.published_at'),
        ];
        return $array;
    }

    public static function conditionList()
    {
        $array = [];
        return $array;
    }

    protected $fillable = [
        'status',
        'type',
        'title',
        'content',
        'views',
        'file_path',
        'published_at',
        'created_admin_id',
        'updated_admin_id',
        'created_admin',
        'updated_admin',
    ];

    protected $appends = [
        'status_text',
        'type_text',
        'file_url',
        'thumbnail_url',
    ];

    public function getStatusTextAttribute()
    {
        return collect(self::statusList())->get($this->status);
    }

    public function getTypeTextAttribute()
    {
        return collect(self::typeList())->get($this->type);
    }

    public function getFileUrlAttribute($handle = null)
    {
        $file = $handle ?? $this->file_path;
        return ($file) ?  env('APP_MEDIA_URL') . '/' . self::$imageFolder . $file : null;
    }

    public function getThumbnailUrlAttribute($handle = null)
    {
        $file = $handle ?? $this->file_path;
        return ($file) ?  env('APP_MEDIA_URL') . '/' . self::$thumbnailFolder . $file : null;
    }

    public function scopeSearch($query, $conditions)
    {
        return $query->where(function ($query) use ($conditions) {
            if (@$conditions['id']) {
                $query->where('id', $conditions['id']);
            }
            if (@$conditions['type']) {
                $query->where('type', $conditions['type']);
            }
            if (@$conditions['search']) {
                $query->where('title', 'LIKE', "%{$conditions['search']}%");
            }
        });
    }

}
