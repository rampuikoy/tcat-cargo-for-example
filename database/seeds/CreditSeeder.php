<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Credit;

class CreditSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Credit::factory(5)->create();
    }
}
