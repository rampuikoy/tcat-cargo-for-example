<?php

namespace App\Helpers;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Tymon\JWTAuth\Contracts\Providers\Auth;

class ScopeQueries
{

    /**
     * Scope a query to only include created.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function scopeDateRange(Builder $query, $column, $dateStart, $dateEnd)
    {
        if ($dateEnd == "") {
            $dateEnd = $dateStart;
        }
        $dateStart = Carbon::createFromFormat('Y-m-d H:i:s', $dateStart);
        $dateEnd = Carbon::createFromFormat('Y-m-d H:i:s', $dateEnd);
        return $query->whereBetween($column, [$dateStart, $dateEnd]);
    }

    public static function scopeDateStart(Builder $query, $column, $date)
    {
        $dateStart = Carbon::createFromFormat('Y-m-d H:i:s', $date)->startOfDay();
        return $query->whereDate($column, '>=', $dateStart);
    }

    public static function scopeDateEnd(Builder $query, $column, $date)
    {
        $dateEnd = Carbon::createFromFormat('Y-m-d H:i:s', $date)->endOfDay();
        return $query->whereDate($column, '<=', $dateEnd);
    }

    public static function scopeDateRangeRaw($query, $column, $dateStart, $dateEnd)
    {
        if ($dateStart) {
            $query->whereRaw('DATE(' . $column . ') >= "' . $dateStart . '"');
        }
        if ($dateEnd) {
            $query->whereRaw('DATE(' . $column . ') <= "' . $dateEnd . '"');
        }
        return $query;
    }

    public static function queryUserCode($query, $field = 'chats.user_code')
    {
        $admin = auth()->user();
        if ($admin->type == 'sale') {
            $user_id = User::where('admin_ref_id', $admin->id)->pluck('code')->toArray();
            $query->whereIn($field, $user_id)->get();
        }
    }

    // scope query
    public static function scopeSearchNoCode($query, $conditions)
    {
        return $query->where(function ($query) use ($conditions) {
            if (@$conditions['code']) {
                $query->where('code', 'like', '%' . $conditions['code'] . '%');
            }
            if (@$conditions['order_code']) {
                $query->where('order_code', 'like', '%' . $conditions['order_code'] . '%');
            }
            if (@$conditions['reference_code']) {
                $query->where('reference_code', 'like', '%' . $conditions['reference_code'] . '%');
            }
            if (@$conditions['zone']) {
                $query->where('zone', 'like', '%' . $conditions['zone'] . '%');
            }
            if (@$conditions['taken_admin']) {
                $query->where('taken_admin', 'like', '%' . $conditions['taken_admin'] . '%');
            }

            $time_format = (@$conditions['time_format']) ? $conditions['time_format'] : 'created_at';
            if (@$conditions['date_start'] && @$conditions['date_end']) {
                self::scopeDateRange($query, 'no_codes.'.$time_format, $conditions['date_start'], $conditions['date_end']);
            } else if (@$conditions['date_start']) {
                self::scopeDateStart($query, 'no_codes.'.$time_format, $conditions['date_start']);
            } else if (@$conditions['date_end']) {
                self::scopeDateEnd($query, 'no_codes.'.$time_format, $conditions['date_end']);
            }

            if (@$conditions['remark']) {
                $query->where('remark', 'like', '%' . $conditions['remark'] . '%');
            }
            if (@$conditions['description']) {
                $query->where('description', 'like', '%' . $conditions['description'] . '%');
            }
            if (@$conditions['amount']) {
                $query->where('amount', 'like', '%' . $conditions['amount'] . '%');
            }

            if (@$conditions['type']) {
                $query->where('type', $conditions['type']);
            }

            if (is_numeric(@$conditions['status'])) {
                $query->where('status', $conditions['status']);
            } else {
                $query->where('status', '!=', '0');
            }

            if (@$conditions['source']) {
                $query->where('source', 'like', '%' . $conditions['source'] . '%');
            }
            if (@$conditions['source_order']) {
                $query->where('source_order', 'like', '%' . $conditions['source_order'] . '%');
            }
            if (@$conditions['department']) {
                $query->where('department', $conditions['department']);
            }

            if (@$conditions['weight']) {
                $query->where('weight', 'like', '%' . $conditions['weight'] . '%');
            }
            if (@$conditions['width']) {
                $query->where('width', 'like', '%' . $conditions['width'] . '%');
            }
            if (@$conditions['height']) {
                $query->where('height', 'like', '%' . $conditions['height'] . '%');
            }
            if (@$conditions['length']) {
                $query->where('length', 'like', '%' . $conditions['length'] . '%');
            }

            if (@$conditions['condition']) {
                switch ($conditions['condition']) {
                    case 'department':
                        $query->whereRaw('department != "" AND department IS NOT NULL');
                        break;
                    case 'ref_order':
                        $query->whereRaw('reference_code != "" AND reference_code IS NOT NULL');
                        break;
                    case 'null_ref_order':
                        $query->whereRaw('reference_code = "" OR reference_code IS NULL');
                        break;
                    case 'order_code':
                        $query->whereRaw('order_code != "" AND order_code IS NOT NULL');
                        break;
                    case 'null_order_code':
                        $query->whereRaw('reference_code = "" OR reference_code IS NULL');
                        break;
                    case 'null_type':
                        $query->whereNull('type');
                        break;
                    case 'null_weight':
                        $query->whereNull('weight');
                        break;
                    case 'null_size':
                        $query->whereNull('width');
                        break;
                    case 'null_zone':
                        $query->whereRaw('zone = "" OR zone IS NULL');
                        break;
                    case 'description':
                        $query->whereRaw('description != "" AND description IS NOT NULL');
                        break;
                    case 'null_description':
                        $query->whereRaw('description = "" OR reference_code IS NULL');
                        break;
                    case 'source':
                        $query->whereRaw('source != "" AND source IS NOT NULL');
                        break;
                    case 'source_order':
                        $query->whereRaw('source_order != "" AND source_order IS NOT NULL');
                        break;
                }
            }
        });
    }

    public static function scopeUserCodeBySale($query, $field = 'user_code')
    {
        $admin = auth()->user();
        if ($admin->type === 'sale') {
            $user_id = User::where('admin_ref_id', $admin->id)->pluck('code')->toArray();
            $query->whereIn($field, $user_id)->get();
        }
    }

    public static function queryAdminBranch($query, $field = 'thai_shipping_method_id')
    {
        $admin = auth()->user();
        if ($admin->branch == 'branch') {
            $query->where($field, '=', $admin->droppoint_id);
        }
    }
}
