<?php

namespace App\Imports\Commission;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithMappedCells;

class CommissionSummary implements ToModel, WithMappedCells, WithCalculatedFormulas
{
    protected $summary;

    public function model(array $row)
    {
        $this->summary = $row;
    }

    public function mapping(): array
    {
        return [
            // ค่าคอม ( เรทปกติ )
            'normal_increase'           => 'J6',
            'normal_decrease'           => 'L6',
            'normal_total'              => 'N6',
            // ค่าคอม ( เรทพิเศษ )
            'special_increase'          => 'J7',
            'special_decrease'          => 'L7',
            'special_total'             => 'N7',
            // หัก 50% ค่าคอม
            'fifty_percent_decrease'    => 'L8',
            'fifty_percent_total'       => 'N8',
            // เงินเดือน(ประจำ)
            'salary_increase'           => 'J9',
            'salary_decrease'           => 'L9',
            'salary_total'              => 'N9',
            // ค่าน้ำมัน
            'oil_increase'              => 'J10',
            'oil_decrease'              => 'L10',
            'oil_total'                 => 'N10',
            // สรุปยอด
            'total'                     => 'N17',
        ];
    }

    public function getTotal()
    {
        return $this->summary['total'] ?? 0;
    }
}
