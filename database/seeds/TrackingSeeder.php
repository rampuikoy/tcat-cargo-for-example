<?php

namespace Database\Seeders;

use App\Helpers\MoveSqlData;
use App\Models\Tracking;
use Illuminate\Database\Seeder;

class TrackingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $trackings          = collect(MoveSqlData::insertTrackings());
        $track_message      = $trackings->get('message');
        $track_error        = $trackings->get('error');

        if ($track_error) {
            $this->command->error($track_message);
            $this->command->warning('Please insert fresh again');
            return;
        }

        $pivot              = collect(MoveSqlData::insertPivotTrackings());
        $pivot_message      = $pivot->get('message');
        $pivot_error        = $pivot->get('error');

        if ($pivot_error) {
            $this->command->error($pivot_message);
            $this->command->warning('Please insert fresh again');
            return;
        }

        Tracking::factory(50)->create();

        $this->command->info($track_message);
        $this->command->info($pivot_message);
    }
}
