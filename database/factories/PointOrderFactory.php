<?php

namespace Database\Factories;

use App\Models\PointOrder;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

class PointOrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PointOrder::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'status'                => 'waiting',
            'user_remark'           => 'REMARK : ' . $this->faker->sentence(10),
            'admin_remark'          => 'REMARK : ' . $this->faker->sentence(10),
            'total'                 => $this->faker->numberBetween(1, 2),
            'shipping_method_id'    => 27,
            'shipping_cost'         => $this->faker->numberBetween(50, 100),
            // 'truck_id',
            // 'driver_admin',
            // 'driver_admin_id',
            // 'driver2_admin',
            // 'driver2_admin_id',
    
            // 'approved_at',
            // 'packed_at',
            // 'shipped_at',
            // 'printed_at',
            // 'cancelled_at',
            // 'confirmed_at',
            // 'succeed_at',
    
            // 'created_admin',
            // 'updated_admin',
            // 'approved_admin',
            // 'packed_admin',
            // 'shipped_admin',
            // 'printed_admin',
            // 'cancelled_admin',
            // 'confirmed_admin',
            // 'succeed_admin',
    
            // 'created_admin_id',
            // 'updated_admin_id',
            // 'approved_admin_id',
            // 'packed_admin_id',
            // 'shipped_admin_id',
            // 'printed_admin_id',
            // 'cancelled_admin_id',
            // 'confirmed_admin_id',
            // 'succeed_admin_id',
        ];
    }
}
