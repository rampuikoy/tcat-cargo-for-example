<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillTrackingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bill_trackings', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->unsignedBigInteger('bill_id')->nullable();
            $table->unsignedBigInteger('tracking_id')->nullable();
            $table->foreign('bill_id')
                ->references('id')->on('bills')->onDelete('set null');
            $table->foreign('tracking_id')
                ->references('id')->on('trackings')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bill_trackings');
    }
}
