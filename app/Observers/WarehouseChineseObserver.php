<?php

namespace App\Observers;

use App\Models\WarehouseChinese;
use App\Observers\BaseObserver;
use App\Traits\UploadTrait;

class WarehouseChineseObserver extends BaseObserver
{ use UploadTrait;
    /**
     * Handle the warehouse_chinese "creating" event.
     *
     * @param  \App\Models\WarehouseChinese  $warehouse_chinese
     * @return void
     */
    public function creating(WarehouseChinese $warehouse_chinese)
    {
        //
        $this->stampCreated($warehouse_chinese);
    }

    /**
     * Handle the warehouse_chinese "updating" event.
     *
   * @param  \App\Models\WarehouseChinese  $warehouse_chinese
     * @return void
     */
    public function updating(WarehouseChinese $warehouse_chinese)
    {
        //
        $this->stampUpdated($warehouse_chinese);
    }


      /**
     * Handle the warehouse_chinese "updated" event.
     *
     * @param  \App\Models\WarehouseChinese  $warehouse_chinese
     * @return void
     */
    public function updated(WarehouseChinese $warehouse_chinese)
    {
        if ($warehouse_chinese->isDirty('image')) {
            $oldFilePath = $warehouse_chinese->getOriginal('image');
            $this->removeFile($warehouse_chinese->getImageUrlAttribute($oldFilePath));
        }
    }

}
