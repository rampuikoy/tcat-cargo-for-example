<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class ThaiShippingMethodPolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.thai-shipping-method.index');
    }
    public function show(Admin $admin)
    {
        return $admin->hasAnyPermission(['backend.thai-shipping-method.show', 'backend.thai-shipping-method.update']);
    }

    public function store(Admin $admin)
    {
        return $admin->can('backend.thai-shipping-method.store');
    }
    public function update(Admin $admin)
    {
        return $admin->can('backend.thai-shipping-method.update');
    }

    public function destroy(Admin $admin)
    {
        return $admin->can('backend.thai-shipping-method.destroy');
    }

}
