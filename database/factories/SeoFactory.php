<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Seo;
use Faker\Generator as Faker;

$factory->define(Seo::class, function (Faker $faker) {
    return [
        'title' => $faker->title,
        'keyword' => $faker->word,
        'description' => $faker->realText(),
        'code' => $faker->languageCode,
        'footer' => $faker->languageCode
    ];
});
