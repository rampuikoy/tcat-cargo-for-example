<?php

namespace App\Repositories;

use App\Helpers\ThaiBulkSms;
use App\Interfaces\SmsQueueInterface;
use App\Jobs\JobSendSms;
use App\Models\SmsQueue;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use Illuminate\Support\Facades\Cache;

class SmsQueueRepository implements SmsQueueInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const MINUTE_CACHE          = 1;

    public function __construct(SmsQueue $smsQueue)
    {
        $this->smsQueue = $smsQueue;
    }

    public function all(array $columns = ['*'], $sorting = SmsQueueInterface::DEFAULT_SORTING, $ascending = SmsQueueInterface::DEFAULT_ASCENDING)
    {
        $key    = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time   = $this->getTime(self::MINUTE_CACHE);

        $smsQueues = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
            return $this->smsQueue::select($columns)->orderBy($sorting, $ascending)->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.sms_queue')]), $smsQueues);
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = SmsQueueInterface::DEFAULT_LIMIT, $sorting = SmsQueueInterface::DEFAULT_SORTING, $ascending = SmsQueueInterface::DEFAULT_ASCENDING)
    {
        $key    = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time   = $this->getTime(self::MINUTE_CACHE);

        $smsQueues = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
            return $this->smsQueue::select($columns)
                ->with('smsable')
                ->search($conditions)
                ->orderBy($sorting, $ascending)
                ->skip($offset)
                ->limit($limit)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.sms_queue')]), $smsQueues);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = SmsQueueInterface::DEFAULT_LIMIT, $sorting = SmsQueueInterface::DEFAULT_SORTING, $ascending = SmsQueueInterface::DEFAULT_ASCENDING)
    {
        $key    = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time   = $this->getTime(self::MINUTE_CACHE);

        $smsQueues = Cache::tags([$this->getClassPrefix(), $this->getPaginationWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
            $model = $this->smsQueue::select($columns)
                ->with('smsable')
                ->search($conditions)
                ->orderBy($sorting, $ascending);

            if ($page == 1) {
                return $model->paginate($limit);
            } else {
                return $model->skip($limit * ($page - 1))->paginate($limit);
            }
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.sms_queue')]), $smsQueues);
    }

    public function findById($id)
    {
        $smsQueue = $this->findByIdMethod($id);

        if (!$smsQueue) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.sms_queue'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.sms_queue')]), $smsQueue);
    }

    public function dropdownList()
    {
        $key    = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time   = $this->getTime(self::MINUTE_CACHE);

        $dropdown = Cache::remember($key, $time, function () {
            return [
                'status'    => $this->smsQueue::statusList(),
                'send'      => $this->smsQueue::sendList(),
            ];
        });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function updateManyQueue(array $ids)
    {
        $results = [];
        foreach ($ids as $id) {
            $smsQueue   = $this->findByIdMethod($id);

            if ($smsQueue->update(['status' => 'queue'])) {
                array_push($results, $smsQueue);
                Cache::tags([$this->getIdWithClassPrefix($id)])->flush();
                JobSendSms::dispatch($smsQueue->id)->onQueue(env('QUEUE_SMS_PREFIX'));
            }
        }

        if (count($results) === 0) {
            return $this->coreResponse(500, __('messages.not_update_queue', ['data' => __('messages.sms')]));
        }

        $this->clearCacheTags();
        return $this->coreResponse(200, __('messages.update_queue', ['data' => __('messages.sms')]), $results);
    }

    public function resend($id)
    {
        $smsQueue   = $this->findByIdMethod($id);

        if (!$smsQueue) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.sms_queue'), 'id' => $id]));
        }

        if ($smsQueue->status === 'queue') {
            $reulst = ThaiBulkSms::send($smsQueue->msisdn, $smsQueue->message);
            if (is_string($reulst)) {
                return $reulst;
            }
            $smsQueue->update([
                'status'            => 'success',
                'send_at'           => date('Y-m-d H:i:s'),
                'send'              => $reulst['send'][0],
                'detail'            => (isset($reulst['detail'][0])) ? $reulst['detail'][0] : null,
                'transaction'       => $reulst['transaction'][0],
                'used_credit'       => $reulst['used_credit'][0],
                'remain_credit'     => $reulst['remain_credit'][0],
            ]);

            Cache::tags([$this->getIdWithClassPrefix($id)])->flush();
        }

        return $this->coreResponse(200, __('messages.update_queue', ['data' => __('messages.sms')]), $smsQueue);
    }

    private function clearCacheTags()
    {
        $keys = [
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix(),
        ];
        Cache::tags($keys)->flush();
    }

    private function findByIdMethod($id)
    {
        $key    = $this->getCacheKey(null, $id);
        $time   = $this->getTime(self::MINUTE_CACHE);

        $smsQueue = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($id)
        ])->remember($key, $time, function () use ($id) {
            return $this->smsQueue::with('smsable')->find($id);
        });

        return $smsQueue;
    }
}
