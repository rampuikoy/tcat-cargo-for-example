<?php

namespace App\Http\Controllers\Backend\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\UpdatePasswordRequest;
use App\Http\Requests\Auth\UpdateProfileRequest;
use App\Interfaces\AuthInterface;
use App\Models\Admin;

class UserController extends Controller
{
    public function __construct(AuthInterface $authInterface)
    {
        $this->authInterface = $authInterface;
    }

    public function me()
    {
        $auth = $this->authInterface->me();
        if ($auth->results) {
            $auth->results = $this->castData($auth->results);
        }
        return response()->json($auth, $auth->code);
    }

    public function updateProfile(UpdateProfileRequest $request)
    {
        $data = $request->only([
            'name',
            'lastname',
            'email',
            'photo',
            'tel',
            'remark',
        ]);

        $auth = $this->authInterface->updateProfile($data);

        if ($auth->results) {
            $auth->results = $this->castData($auth->results);
        }

        return response()->json($auth, $auth->code);
    }

    public function updatePassword(UpdatePasswordRequest $request)
    {
        $password = $request->get('password');

        $auth = $this->authInterface->updatePassword($password);

        if ($auth->results) {
            $auth->results = $this->castData($auth->results);
        }
        return response()->json($auth, $auth->code);
    }

    private function castData(Admin $admin)
    {
        $admin->getPermissionsViaRoles();
        if (count($admin->roles) > 0) {
            $admin->permissions = collect($admin->roles[0]->permissions)->pluck('name');
        }
        $fields = [
            'id',
            'name',
            'lastname',
            'email',
            'username',
            'role',
            'photo',
            'photo_url',
            'tel',
            'department',
            'department_text',
            'droppoint_id',
            'branch',
            'branch_text',
            'type',
            'type_text',
            'status',
            'status_text',
            'regex_user',
            'pattern_user',
            'remark',
            'permissions',
        ];
        return $admin->only($fields);
    }
}
