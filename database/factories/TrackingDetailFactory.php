<?php

namespace Database\Factories;

use App\Models\TrackingDetail;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

class TrackingDetailFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TrackingDetail::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $sufType = ['clothing', 'shoes', 'bag', 'accessories', 'electronics', 'it', 'car', 'home', 'office', 'stationery', 'handyman_tool', 'pet', 'toy', 'furniture', 'food_and_drug', 'other', 'cosmetics'];
        return [
            'product_type' => Arr::random($sufType),
            // 'product_remark',
            'qty' => random_int(0, 150),
            'tracking_id' => random_int(1000000, 9999999),
        ];
    }
}
