<?php

namespace App\Repositories;

use App\Interfaces\ProductTypeInterface;
use App\Models\ProductType;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use Illuminate\Support\Facades\Cache;

class ProductTypeRepository implements ProductTypeInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const MINUTE_CACHE          = 1;

    public function __construct(ProductType $productType)
    {
        $this->productType      = $productType;
    }

    public function all(array $columns = ['*'], $sorting = ProductTypeInterface::DEFAULT_SORTING, $ascending = ProductTypeInterface::DEFAULT_ASCENDING)
    {
        $key            = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time           = $this->getTime(self::MINUTE_CACHE);

        $productTypes   = Cache::tags([
            $this->getClassPrefix(),
            $this->getAllWithClassPrefix(),
        ])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
                            return $this->productType::select($columns)->orderBy($sorting, $ascending)->get();
                        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.tracking_type')]), $productTypes);
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = ProductTypeInterface::DEFAULT_LIMIT, $sorting = ProductTypeInterface::DEFAULT_SORTING, $ascending = ProductTypeInterface::DEFAULT_ASCENDING)
    {
        $key            = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time           = $this->getTime(self::MINUTE_CACHE);

        $productTypes   = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
                            return $this->productType::select($columns)
                                ->search($conditions)
                                ->orderBy($sorting, $ascending)
                                ->skip($offset)
                                ->limit($limit)
                                ->get();
                        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.tracking_type')]), $productTypes);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = ProductTypeInterface::DEFAULT_LIMIT, $sorting = ProductTypeInterface::DEFAULT_SORTING, $ascending = ProductTypeInterface::DEFAULT_ASCENDING)
    {
        $key            = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time           = $this->getTime(self::MINUTE_CACHE);

        $productTypes   = Cache::tags([$this->getClassPrefix(), $this->getPaginationWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
                            $model = $this->productType::select($columns)
                                ->search($conditions)
                                ->orderBy($sorting, $ascending);

                            if ($page == 1) {
                                return $model->paginate($limit);
                            } else {
                                return $model->skip($limit * ($page - 1))->paginate($limit);
                            }
                        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.tracking_type')]), $productTypes);
    }

    public function findById($id, array $columns = ['*'])
    {
        $productType            = $this->findByIdMethod($id, $columns);

        if (!$productType) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.tracking_type'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.tracking_type')]), $productType);
    }

    public function dropdownList()
    {
        $key        = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $dropdown   = Cache::remember($key, $time, function () {
                        return [
                            'status' => $this->productType::statusList()
                        ];
                    });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function store(array $data)
    {
        $productType    = $this->productType::create($data);

        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.tracking_type')]), $productType);
    }

    public function updateById($id, array $data)
    {
        $productType            = $this->findByIdMethod($id);

        if (!$productType) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.tracking_type'), 'id' => $id]));
        }

        $productType->update($data);
        Cache::tags([$this->getIdWithClassPrefix($productType->id)])->flush();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.tracking_type')]), $productType);
    }

    public function softDeleteById($id)
    {
        $productType            = $this->findByIdMethod($id);

        if (!$productType) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.tracking_type'), 'id' => $id]));
        }

        $productType->delete();
        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();

        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.tracking_type')]), $productType);
    }

    private function findByIdMethod($id, array $columns = ['*'])
    {
        $key        = $this->getCacheKey(null, $id);
        $time       = $this->getTime(self::MINUTE_CACHE);

        $productType    = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($id)
        ])->remember($key, $time, function () use ($id, $columns) {
            return $this->productType::select($columns)->whereId($id)->first();
        });

        return $productType;
    }

    private function clearCacheTags()
    {
        Cache::tags([
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix()
        ])->flush();
    }
}
