<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// With out token
Route::group(['middleware' => ['guest:users', 'throttle:60,1']], function () {
    Route::post('login', 'Auth\LoginController@login')->name('login');
    Route::post('register', 'Auth\RegisterController@register')->name('register');;
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('resetPassword.send');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('resetPassword.reset');
    Route::post('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
    Route::post('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');;

    Route::post('oauth/{driver}', 'Auth\OAuthController@redirectToProvider')->name('oauth.redirect.provider');
    Route::get('oauth/{driver}/callback', 'Auth\OAuthController@handleProviderCallback')->name('oauth.callback');
});
// With token
Route::group(['middleware' => ['assign.guard:users', 'jwt.auth', 'throttle:60,1']], function () {
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');
    Route::get('/user', 'Auth\UserController@current')->name('profile');

    Route::patch('settings/profile', 'Settings\ProfileController@update')->name('settings.profile');
    Route::patch('settings/password', 'Settings\PasswordController@update')->name('settings.password');
});

Route::group(['namespace' => 'Backend', 'prefix' => 'backend', 'as' => 'backend.'], function () {
    // With out token
    Route::group(['middleware' => 'guest:admins', 'throttle:60,1'], function () {
        Route::post('login', 'Auth\LoginController@login')->name('login');
        Route::post('register', 'Auth\RegisterController@register')->name('register');;
        Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('resetPassword.send');
        Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('resetPassword.reset');
        Route::post('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
        Route::post('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');
    });

    // With token
    Route::group(['middleware' => ['assign.guard:admins', 'jwt.auth']], function () {

        Route::group(['middleware' => ['throttle:60,1']], function () {

            Route::post('logout', 'Auth\LoginController@logout')->name('logout');
            Route::get('me', 'Auth\UserController@me')->name('me');
            Route::put('setting/profile', 'Auth\UserController@updateProfile')->name('profile.update');
            Route::put('setting/password', 'Auth\UserController@updatePassword')->name('password.update');

            // apiResoucre
            Route::get('admin/search', 'AdminController@search');
            Route::get('admin/dropdown', 'AdminController@dropdown');
            Route::put('admin/{id}/password', 'AdminController@updatePassword')->where('id', '[0-9]+');
            Route::apiResource('admin', 'AdminController');

            Route::get('article/search', 'ArticleController@search');
            Route::get('article/dropdown', 'ArticleController@dropdown');
            Route::apiResource('article', 'ArticleController');

            Route::get('role/search', 'RoleController@search');
            Route::apiResource('role', 'RoleController');

            Route::get('permission/search', 'PermissionController@search');
            Route::get('permission/dropdown', 'PermissionController@dropdown');
            Route::apiResource('permission', 'PermissionController');
            Route::post('permission/delete-all', 'PermissionController@destroyAll');

            //BankAccount
            Route::get('bank-account/search', 'BankAccountController@search');
            Route::get('bank-account/dropdown', 'BankAccountController@dropdown');
            Route::apiResource('bank-account', 'BankAccountController');

            // UserAddreess
            Route::get('user-address/search', 'UserAddressController@search');
            Route::get('user-address/dropdown', 'UserAddressController@dropdown');
            Route::apiResource('user-address', 'UserAddressController');

            //ProductType
            Route::get('product-type/search', 'ProductTypeController@search');
            Route::get('product-type/dropdown', 'ProductTypeController@dropdown');
            Route::apiResource('product-type', 'ProductTypeController');

            //ShippingMethod
            Route::get('shipping-method/search', 'ShippingMethodController@search');
            Route::get('shipping-method/dropdown', 'ShippingMethodController@dropdown');
            Route::apiResource('shipping-method', 'ShippingMethodController');

            //ThaiShippingAreaPrice
            Route::get('thai-shipping-area-price/search', 'ThaiShippingAreaPriceController@search');
            Route::apiResource('thai-shipping-area-price', 'ThaiShippingAreaPriceController');

            //ThaiShippingWeightPrice
            Route::get('thai-shipping-weight-price/search', 'ThaiShippingWeightPriceController@search');
            Route::apiResource('thai-shipping-weight-price', 'ThaiShippingWeightPriceController');

            //ThaiShippingWeightSizePrice
            Route::get('thai-shipping-weight-size-price/search', 'ThaiShippingWeightSizePriceController@search');
            Route::apiResource('thai-shipping-weight-size-price', 'ThaiShippingWeightSizePriceController');

            //Warehouse Chinese
            Route::get('warehouse-chinese/search', 'WarehouseChineseController@search');
            Route::get('warehouse-chinese/dropdown', 'WarehouseChineseController@dropdown');
            Route::apiResource('warehouse-chinese', 'WarehouseChineseController');

            //BankCompany
            Route::get('bank-company/search', 'BankCompanyController@search');
            Route::get('bank-company/dropdown', 'BankCompanyController@dropdown');
            Route::apiResource('bank-company', 'BankCompanyController');

            // User
            Route::get('user/search', 'UserController@search');
            Route::get('user/search/point', 'UserController@searchPoint');
            Route::get('user/dropdown', 'UserController@dropdown');
            Route::get('user/overview/{code}', 'UserController@overview');
            Route::post('user/import-excel', 'UserController@ImportExcel');
            Route::put('user/{id}/password', 'UserController@ResetPassword');
            Route::put('user/{id}/setting-bill', 'UserController@settingBill');
            Route::put('user/{id}/setting-notification', 'UserController@settingNotification');
            Route::put('user/{id}/setting-fcm-token', 'UserController@settingFcmToken');
            Route::apiResource('user', 'UserController');
            
            // User Excel
            Route::post('user/import/validate', 'UserExcelController@importValidate');
            Route::post('user/import', 'UserExcelController@createMany');

            // upload
            Route::post('/upload', 'UploadController@store');
            Route::delete('/upload/{id}', 'UploadController@destroy')->where('id', '[0-9]+');

            //Rate
            Route::get('rate/default', 'RateController@getDefault');
            Route::get('rate/dropdown', 'RateController@dropdown');
            Route::get('rate/search', 'RateController@search');
            Route::get('rate/search-user/{id_salse}', 'RateController@searchUser');
            Route::get('rate/user/{code}', 'RateController@getUserRate');
            Route::put('rate/user/{code}', 'RateController@putUserRate');
            Route::apiResource('rate', 'RateController');

            //Email Queue
            Route::get('email-queue/dropdown', [App\Http\Controllers\Backend\Notification\EmailQueueController::class, 'dropdown']);
            Route::get('email-queue/search', [App\Http\Controllers\Backend\Notification\EmailQueueController::class, 'search']);
            Route::post('email-queue/queue-all', [App\Http\Controllers\Backend\Notification\EmailQueueController::class, 'updateManyQueue'])->where('id', '[0-9]+');

            //Sms Queue
            Route::get('sms-queue/dropdown', [App\Http\Controllers\Backend\Notification\SmsQueueController::class, 'dropdown']);
            Route::get('sms-queue/search', [App\Http\Controllers\Backend\Notification\SmsQueueController::class, 'search']);
            Route::post('sms-queue/queue-all', [App\Http\Controllers\Backend\Notification\SmsQueueController::class, 'updateManyQueue'])->where('id', '[0-9]+');

            //Fcm Queue
            Route::get('fcm-queue/dropdown', [App\Http\Controllers\Backend\Notification\FcmQueueController::class, 'dropdown']);
            Route::get('fcm-queue/search', [App\Http\Controllers\Backend\Notification\FcmQueueController::class, 'search']);
            Route::post('fcm-queue/queue-all', [App\Http\Controllers\Backend\Notification\FcmQueueController::class, 'updateManyQueue'])->where('id', '[0-9]+');

            //ExchangeRate
            Route::get('exchange-rate/search', 'ExchangeRateController@search');
            Route::apiResource('exchange-rate', 'ExchangeRateController');

            //Seo
            Route::get('seo', 'SeoController@index');
            Route::post('seo', 'SeoController@update');

            //Report
            Route::group(['prefix' => 'report'], function () {
                Route::get('user', 'ReportController@getUser');
                Route::get('tracking', 'ReportController@getTracking');
                Route::get('bill', 'ReportController@getBill');
                Route::get('topup', 'ReportController@getTopup');
                Route::get('withdraw', 'ReportController@getWithdraw');
                Route::get('active-user', 'ReportController@getActiveUser');
                Route::get('truck', 'ReportController@getTruck');
            });

            //Truck
            Route::get('truck/search', 'TruckController@search');
            Route::get('truck/dropdown', 'TruckController@dropdown');
            Route::apiResource('truck', 'TruckController');

            //Credit
            Route::get('credit/dropdown', 'CreditController@dropdown');
            Route::get('credit/search', 'CreditController@search');
            Route::apiResource('credit', 'CreditController');

            //Withdraw
            Route::put('withdraw/{id}/approve', 'WithdrawController@approve');
            Route::put('withdraw/{id}/cancel', 'WithdrawController@cancel');
            Route::put('withdraw/{id}/upload', 'WithdrawController@upload');
            Route::get('withdraw/search', 'WithdrawController@search');
            Route::get('withdraw/dropdown', 'WithdrawController@dropdown');
            Route::apiResource('withdraw', 'WithdrawController');

            //Tracking zone
            Route::post('tracking/zone-in', 'TrackingController@postZoneIn');
            Route::put('tracking/zone-in', 'TrackingController@putZoneIn');
            Route::put('tracking/zone-in-modal', 'TrackingController@putZoneInModal');

            //Topup
            Route::put('topup/{id}/cancel', 'TopupController@cancel');
            Route::put('topup/{id}/approve', 'TopupController@approve');
            Route::get('topup/search', 'TopupController@search');
            Route::get('topup/option-create/{code}', 'TopupController@optionCreate');
            Route::get('topup/option-edit/{id}', 'TopupController@optionEdit');
            Route::get('topup/check-duplicate', 'TopupController@checkDuplicate');
            Route::get('topup/dropdown', 'TopupController@dropdown');
            Route::apiResource('topup', 'TopupController');

            //Tracking
            Route::get('tracking/dropdown', 'TrackingController@dropdown');
            Route::get('tracking/user-code/{code}', 'TrackingController@showUserCode');
            Route::get('tracking/search-track/{track}', 'TrackingController@showTrack');
            Route::get('tracking/find-external/{track}', 'TrackingController@showTrackExternal');
            Route::get('tracking/search', 'TrackingController@search');
            Route::apiResource('tracking', 'TrackingController');
            Route::post('tracking/delete-all', 'TrackingController@destroyAll');
            Route::put('tracking/{id}/detail', 'TrackingController@trackingDetail');

            //Bill
            Route::get('bill/dropdown', 'BillController@dropdown');
            Route::get('bill/search', 'BillController@search');
            Route::get('bill/option-create/{code}', 'BillController@optionCreate');
            Route::get('bill/option-edit/{id}', 'BillController@optionEdit');
            Route::post('bill/backward/{id}', 'BillController@postBackward');
            Route::post('bill/cancel/{id}', 'BillController@postCancel');
            Route::post('bill/step-create', 'BillController@stepCreate');
            Route::post('bill/{id}/driver', 'BillController@postDriver');
            Route::post('bill/pack-product/{id}', 'BillController@packProduct');
            Route::post('bill/notify-thai-shipping/{id}', 'BillController@notifyThaiShipping');
            Route::post('bill/notify-shipped/{id}', 'BillController@notifyShipped');
            Route::post('bill/close-bill/{id}', 'BillController@closeBill');
            Route::post('bill/notify-create/{id}', 'BillController@notifyCreate');
            Route::put('bill/upload/{id}', 'BillController@upload');
            Route::put('bill/upload/signature/{id}', 'BillController@signature');
            Route::get('bill/rate-commission-sale', 'BillController@rateCommission');
            Route::post('bill/droppoint-out/{id}', 'BillController@postDroppointOut');
            Route::put('bill/droppoint-out/{id}', 'BillController@updateDroppointOut');
            Route::post('bill/droppoint-in/{id}', 'BillController@postDroppointIn');
            Route::put('bill/droppoint-in', 'BillController@updateDroppointIn');
            Route::delete('bill/image-delete/{id}', 'BillController@destroyUploadImage');
            Route::apiResource('bill', 'BillController');

            //Commission Rate
            Route::get('commission-rate/excel', 'CommissionRateController@exportExcel');
            Route::get('commission-rate/search', 'CommissionRateController@search');
            Route::get('commission-rate/dropdown', 'CommissionRateController@dropdown');
            Route::post('commission-rate', 'CommissionRateController@store');

            //Commission
            Route::get('commission/search', 'CommissionController@search');
            Route::get('commission/dropdown', 'CommissionController@dropdown');
            Route::get('commission/{id}', 'CommissionController@show')->where('id', '[0-9]+');
            Route::post('commission', 'CommissionController@store');

            //PDF Bill
            Route::get('pdf/bill/download/{id}', 'BillDownloadController@getDownloadBill');
            Route::get('pdf/bill/multi/download', 'BillDownloadController@getDownloadBills');
            Route::get('pdf/bill/multi/tracking/download', 'BillDownloadController@getDownloadTrackings');
            Route::get('pdf/bill/multi/cover/download', 'BillDownloadController@getDownloadCovers');
            Route::get('pdf/bill/multi/sticker/download', 'BillDownloadController@getDownloadStickers');
            Route::get('pdf/bill/download/cover/{id}', 'BillDownloadController@getDownloadCover');
            Route::get('pdf/bill/download/sticker/{id}', 'BillDownloadController@getDownloadSticker');
            Route::get('pdf/bill/download/tracking/{id}', 'BillDownloadController@getDownloadTracking');
            Route::get('pdf/bill/download/droppoint/shipping', 'BillDownloadController@getDownloadDroppoint');
            Route::get('pdf/bill/download/other/shipping', 'BillDownloadController@getDownloadOther');
            Route::get('pdf/bill/download/driver/shipping', 'BillDownloadController@getDownloadDriver');
            // PDF UserAddreess
            Route::get('pdf/user-address/sticker/{id}', 'UserAddressController@getSticker');
            // PDF Claim
            Route::get('pdf/claim/{id}', 'ClaimsController@getDownload');
            // PDF Trip
            Route::get('pdf/trip/{id}', 'TripController@getDownload');
            //PDF PointOrder
            Route::get('pdf/point-order/download/droppoint/shipping', 'PointOrderDownloadController@getDownloadDroppoint');
            Route::get('pdf/point-order/download/other/shipping', 'PointOrderDownloadController@getDownloadOther');
            Route::get('pdf/point-order/download/driver/shipping', 'PointOrderDownloadController@getDownloadDriver');
            Route::get('pdf/point-order/multi/sticker/download', 'PointOrderDownloadController@getDownloadStickers');
            Route::get('pdf/point-order/multi/cover/download', 'PointOrderDownloadController@getDownloadCovers');
            Route::get('pdf/point-order/multi/download', 'PointOrderDownloadController@getDownloadBills');

            //Payment
            Route::put('payment/{id}/approve', 'PaymentController@approve');
            Route::get('payment/dropdown', 'PaymentController@dropdown');
            Route::get('payment/search', 'PaymentController@search');
            Route::get('payment/find-pament-list-create/{code}', 'PaymentController@findPaymentListCreate');
            Route::get('payment/find-pament-list-approve/{code}', 'PaymentController@findPaymentListApprove');
            Route::apiResource('payment', 'PaymentController');

            //Claim
            Route::get('claim/search', 'ClaimsController@search');
            Route::get('claim/dropdown', 'ClaimsController@dropdown');
            Route::put('claim/upload/{id}', 'ClaimsController@upload');
            Route::put('claim/approve/{id}', 'ClaimsController@approveById');
            Route::put('claim/cancel/{id}', 'ClaimsController@cancelById');
            Route::apiResource('claim', 'ClaimsController');

            // Trip
            Route::post('trip/notify-create/{id}', 'TripController@notifyCreate');
            Route::post('trip/notify-close/{id}', 'TripController@notifyClose');
            Route::post('trip/notify-detail/{id}', 'TripController@notifyDetail');
            Route::post('trip/post-cancel/{id}', 'TripController@postCancel');
            Route::get('trip/search', 'TripController@search');
            Route::get('trip/dropdown', 'TripController@dropdown');
            Route::get('trip/option-edit/{id}', 'TripController@optionEdit');
            Route::apiResource('trip', 'TripController');

            // Coupon
            Route::get('coupon/search', 'CouponController@search');
            Route::get('coupon/dropdown', 'CouponController@dropdown');
            Route::put('coupon/check/{code}', 'CouponController@check');
            Route::apiResource('coupon', 'CouponController');

            //Create / Update NoCode By Track ID
            Route::apiResource('nocode-track', 'NoCodeByTrackIdController');
            Route::post('nocode-track/image/{id}', 'NoCodeByTrackIdController@uploadImage');
            Route::delete('nocode-track/image/{id}', 'NoCodeByTrackIdController@destroyUploadImage');

            // Tracking Excel
            Route::post('tracking/import/validate', 'TrackingExcelController@importValidate');
            Route::post('tracking/import/track', 'TrackingExcelController@createTrack');
            Route::post('tracking/excel', 'TrackingExcelController@createTrackExcel');

            // No Code
            Route::get('nocode/dropdown', 'NoCodeController@dropdown');
            Route::get('nocode/search', 'NoCodeController@search');
            Route::get('nocode/gallery', 'NoCodeController@gallery');
            Route::post('nocode/find-create', 'NoCodeController@findOrCreateByCode');
            Route::put('nocode/{id}/cancel', 'NoCodeController@cancel');
            Route::put('nocode/{id}/upload', 'NoCodeController@upload');
            Route::post('nocode/zone-in', 'NoCodeController@zoneIn');
            Route::post('nocode/zone-out', 'NoCodeController@zoneOut');
            Route::post('nocode/import/validate', 'NoCodeExcelController@importValidate');
            Route::post('nocode/import/create', 'NoCodeExcelController@createOrUpdate');
            Route::apiResource('nocode', 'NoCodeController');

            // point
            Route::get('point/dropdown', 'PointController@dropdown');
            Route::get('point/search', 'PointController@search');
            Route::apiResource('point', 'PointController');

            // point withdraw
            Route::get('point-withdraw/dropdown', 'PointWithdrawController@dropdown');
            Route::get('point-withdraw/search', 'PointWithdrawController@search');
            Route::put('point-withdraw/{id}/approve', 'PointWithdrawController@approve');
            Route::put('point-withdraw/{id}/close', 'PointWithdrawController@close');
            Route::put('point-withdraw/{id}/cancel', 'PointWithdrawController@cancel');
            Route::apiResource('point-withdraw', 'PointWithdrawController');

            // product
            Route::get('product/dropdown', 'ProductController@dropdown');
            Route::get('product/search', 'ProductController@search');
            Route::apiResource('product', 'ProductController');

            // point order
            Route::get('point-order/dropdown', 'PointOrderController@dropdown');
            Route::get('point-order/search', 'PointOrderController@search');
            Route::get('point-order/find/{code}', 'PointOrderController@findByCode');
            Route::get('point-order/option-create/{code}', 'PointOrderController@optionCreate');
            Route::get('point-order/option-ship', 'PointOrderController@optionShip');
            Route::post('point-order/check-limit/{code}', 'PointOrderController@checkLimit');
            Route::put('point-order/{id}/approve', 'PointOrderController@approve');
            Route::put('point-order/{id}/cancel', 'PointOrderController@cancel');
            Route::put('point-order/{code}/pack', 'PointOrderController@pack');
            Route::put('point-order/{code}/ship', 'PointOrderController@ship');
            Route::put('point-order/{code}/confirm', 'PointOrderController@confirm');
            Route::put('point-order/{code}/success', 'PointOrderController@success');
            Route::apiResource('point-order', 'PointOrderController');

            // thai shpipping method
            Route::get('thai-shipping-method/dropdown', 'ThaiShippingMethodController@dropdown');
            Route::get('thai-shipping-method/search', 'ThaiShippingMethodController@search');
            Route::post('thai-shipping-method/droppoint', 'ThaiShippingMethodController@postDroppoint');
            Route::apiResource('thai-shipping-method', 'ThaiShippingMethodController');

            // Dashboard
            Route::get('dashboard/noti', 'DashboardController@getNoti');

            // norole
            Route::group(['prefix' => 'norole'], function () {
                Route::get('/user/search', [App\Http\Controllers\Backend\NoRole\UserController::class, 'search']);
                Route::get('/user/find/{slug}', [App\Http\Controllers\Backend\NoRole\UserController::class, 'find']);
                Route::get('/permission', [App\Http\Controllers\Backend\NoRole\PermissionController::class, 'all']);

                // location
                Route::get('/province', [App\Http\Controllers\Backend\NoRole\LocationController::class, 'province']);
                Route::get('/province/{id}/amphur', [App\Http\Controllers\Backend\NoRole\LocationController::class, 'amphurById'])->where('id', '[0-9]+');
                Route::get('/amphur/{id}/district', [App\Http\Controllers\Backend\NoRole\LocationController::class, 'districtById'])->where('id', '[0-9]+');
                Route::get('/district/{code}/zipcode', [App\Http\Controllers\Backend\NoRole\LocationController::class, 'zipcodeByCode'])->where('code', '[0-9]+');
                Route::post('/distance', [App\Http\Controllers\Backend\NoRole\LocationController::class, 'distance']);

                // tracking
                Route::get('/tracking/code/{code}', [App\Http\Controllers\Backend\NoRole\TrackingController::class, 'searchMany']);
                // thai shippinh method
                Route::get('/thai-shipping-method/{id}/weight-price', [App\Http\Controllers\Backend\NoRole\ThaiShippingMethodController::class, 'weightPrice']);
                Route::get('/thai-shipping-method/{id}/weight-size-price', [App\Http\Controllers\Backend\NoRole\ThaiShippingMethodController::class, 'weightSizePrice']);
                Route::get('/thai-shipping-method/{id}/area-price', [App\Http\Controllers\Backend\NoRole\ThaiShippingMethodController::class, 'areaPrice']);
                Route::get('/thai-shipping-method/{id}/droppoint-price', [App\Http\Controllers\Backend\NoRole\ThaiShippingMethodController::class, 'droppointPrice']);
            });
        });

        //60 request per 1 min
        Route::group(['middleware' => ['throttle:60,1']], function () {
            // chat
            Route::get('chat/search', 'ChatController@search');
            Route::get('chat/dropdown', 'ChatController@dropdown');
            Route::post('chat/read-all', 'ChatController@readAll');
            Route::apiResource('chat', 'ChatController')->only(['index', 'show']);
            // broadcast
            Route::get('broadcast/search', 'BroadcastController@search');
            Route::get('broadcast/dropdown', 'BroadcastController@dropdown');
            Route::apiResource('broadcast', 'BroadcastController')->only(['index', 'show', 'store', 'update']);
        });
        //60 request per 1 min / ip
        Route::post('chat/message', 'ChatController@store')->middleware('throttle:limit-ip');
    });
});
