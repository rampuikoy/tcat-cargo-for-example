<?php

namespace App\Observers;

use App\Models\NoCode;

class NoCodeObserver extends BaseObserver
{
    //
    public function creating(NoCode $nocode)
    {
        $this->stampCreated($nocode);
    }

    public function updating(NoCode $nocode)
    {
        $this->stampUpdated($nocode);
    }

}
