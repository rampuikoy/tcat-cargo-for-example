<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSmsQueuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms_queues', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->enum('status', ['queue', 'success'])->default('queue');
            $table->tinyInteger('send')->default(0);
            $table->timestamp('send_at')->nullable();
            $table->string('msisdn', 10)->nullable();
            $table->string('detail', 100)->nullable();
            $table->string('transaction', 100)->nullable();
            $table->string('message', 255)->nullable();
            $table->tinyInteger('used_credit')->unsigned()->nullable();
            $table->mediumInteger('remain_credit')->unsigned()->nullable();
            $table->string('user_code', 20)->nullable();
            $table->nullableMorphs('smsable');
            $table->integer('created_admin_id')->unsigned()->nullable();
            $table->string('created_admin', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms_queues');
    }
}
