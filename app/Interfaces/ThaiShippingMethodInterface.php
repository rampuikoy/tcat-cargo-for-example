<?php

namespace App\Interfaces;

interface ThaiShippingMethodInterface
{
    const DEFAULT_LIMIT = 25;
    const DEFAULT_SORTING = 'id';
    const DEFAULT_ASCENDING = 'desc';

    /**
     * fetch all thaishippingmethod
     *
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function all(array $columns = ['*'], $sorting = ThaiShippingMethodInterface::DEFAULT_SORTING, $ascending = ThaiShippingMethodInterface::DEFAULT_ASCENDING);

    /**
     * list thaishippingmethod
     *
     * @param array $conditions
     * @param array $columns
     * @param int $offset
     * @param int $limit
     *
     * @return \Illuminate\Support\Collection
     */
    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * pagination thaishippingmethod
     *
     * @param array $conditions
     * @param array $columns
     * @param int $page
     * @param int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $perPage = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * find thaishippingmethod by id
     *
     * @param $id
     * @param array $columns
     *
     * @return object|null
     */
    public function findById($id, array $columns = ['*']);

    /**
     * find thaishippingmethod by type
     *
     * @param $type
     * @param array $columns
     *
     * @return object|null
     */
    public function findManyByType($type, array $columns = ['*']);

    /**
     * find thaishippingmethod where droppoint by id
     *
     * @param $id
     * @param array $columns
     *
     * @return object|null
     */
    public function findDroppointById($id, array $columns = ['*']);

    /**
     * get thaishippingmethod dropdown list
     *
     * @return object
     */
    public function dropdownList();

    /**
     * post thaishippingmethod Droppoint list
     *
     * @return object
     */
    public function postDroppoint();

    /**
     * soft delete shippingmethod by id
     *
     * @param $id
     *
     * @return object|null
     */
    public function softDeleteById($id);
}
