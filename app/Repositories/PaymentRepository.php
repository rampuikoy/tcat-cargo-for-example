<?php

namespace App\Repositories;

use App\Models\Credit;
use App\Models\Payment;
use App\Traits\CacheTrait;
use App\Models\PackingQueue;
use App\Traits\CoreResponseTrait;
use Illuminate\Support\Facades\DB;
use App\Interfaces\PaymentInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class PaymentRepository implements PaymentInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const MINUTE_CACHE          = 0;
    const TABLE_RELETIONS       = ['trackingBills', 'trips'];

    public function __construct(Payment $payment, Credit $credit, PackingQueue $packingQueue)
    {
        $this->payment               = $payment;
        $this->credit                = $credit;
        $this->packingQueue          = $packingQueue;
    }

    public function all(array $columns = ['*'], $sorting = PaymentInterface::DEFAULT_SORTING, $ascending = PaymentInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $payments     = Cache::tags([
            $this->getClassPrefix(),
            $this->getAllWithClassPrefix(),
        ])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
            return $this->payment::select($columns)->orderBy($sorting, $ascending)->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.payment')]), $payments);
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = PaymentInterface::DEFAULT_LIMIT, $sorting = PaymentInterface::DEFAULT_SORTING, $ascending = PaymentInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);
        $payments     = Cache::tags([
            $this->getClassPrefix(),
            $this->getFetchWithClassPrefix()
        ])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
            return $this->payment::select($columns)
                ->search($conditions)
                ->orderBy($sorting, $ascending)
                ->skip($offset)
                ->limit($limit)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.payment')]), $payments);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = PaymentInterface::DEFAULT_LIMIT, $sorting = PaymentInterface::DEFAULT_SORTING, $ascending = PaymentInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $payments     = Cache::tags([
            $this->getClassPrefix(),
            $this->getPaginationWithClassPrefix()
        ])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
            $model = $this->payment::select($columns)
                ->search($conditions)
                ->orderBy($sorting, $ascending);

            if ($page == 1) {
                return $model->paginate($limit);
            } else {
                return $model->skip($limit * ($page - 1))->paginate($limit);
            }
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.payment')]), $payments);
    }

    public function findById($id, array $columns = ['*'])
    {
        $payment    = $this->findByIdMethod($id, $columns);

        if (!$payment) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.payment'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.payment')]), $payment);
    }

    public function dropdownList()
    {
        $key        = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $dropdown   = Cache::remember($key, $time, function () {
            return [
                'status' => $this->payment::statusList(),
                'time_format' => $this->payment::timeFormatList(),
                'payment_type' => $this->payment::paymentsType(),
                'payment_type_bill' => $this->payment::TypePaymentBillList(),
            ];
        });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function store(array $data)
    {

        $billData = $this->checktypeBill(@$data['bill_id'], @$data['type']);
        if (!$billData) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.payments.bill_id'), 'id' => @$data['bill_id']]));
        }

        $payment  = $this->payment::create($data);
        if (@$data['type'] === 'tracking') {
            $payment->trackingBills()->save($billData);
        } else if (@$data['type'] === 'trip') {
            $payment->trips()->save($billData);
        }

        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.payment')]),  $payment);
    }

    public function updateById($id, array $data)
    {
        $payment      = $this->findByIdMethod($id);

        if (!$payment) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.payment'), 'id' => $id]));
        }

        $payment->update($data);
        $this->clearCacheTags();
        $this->clearCacheTagById($payment->id);
        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.payment')]), $payment);
    }

    public function approve($id, array $data, $type_pay_credit = null)
    {
        //type : tracking, trip
        $payment          = $this->findByIdMethod($id);

        if (!$payment) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.payments.payment_id'), 'id' => $id]));
        }
        if ($payment->status !== 'waiting') {
            return $this->coreResponse(404, __('messages.find_data_payment', ['data' => __('messages.payments.payment_pay'), 'id' => $id]));
        }

        $bill = $this->checktypeBill(@$data['bill_id'], @$data['type']);

        if (!$bill) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.payments.bill_id_null'), 'id' => $bill->id]));
        }
        $user           = app('App\Repositories\UserRepository')->findByCodeMethod($bill->user_code);
        if ($bill->status >= 3) { // Paid
            return $this->coreResponse(404, __('messages.data_with_pay', ['data' => __('messages.payments.pay'), 'id' => $payment->code]));
        }

        if (@$data['type'] === 'tracking') {
            $pricePay = $bill->total_price;
        } else if (@$data['type'] === 'trip') {
            $pricePay = $bill->total;
        }

        $currentCredit  = round($user->credit->total ?? 0, 2);
        if (@$data['add_on_total_price'] &&  $pricePay !== $data['add_on_total_price'] + $currentCredit) {
            $totalPrice =  round($data['add_on_total_price'], 2);
            $addOnCredit = true;
        } else {
            $totalPrice =  round($pricePay, 2);
            $addOnCredit = false;
        }

        if ($currentCredit >= 1) { // credit
            if ($addOnCredit) {
                $totalAmount =  round($currentCredit - ($totalPrice + $currentCredit), 2);
            } else {
                $totalAmount =  round($currentCredit - $totalPrice, 2);
            }
        } else {
            $totalAmount = -round($totalPrice, 2);
        }

        if ($totalAmount < 0 && @$data['cash']) {
            if ($data['cash'] !== 0 && $data['cash'] < abs($totalAmount)) {
                return $this->coreResponse(404, __('messages.no_data_with_cash_pay'));
            } else {
                $sumAfter  =  round($data['cash'] - abs($totalAmount), 2);
            }
        } else {
            $sumAfter =  round($totalAmount, 2);
        }


        if ($sumAfter < 0) { // not pay
            return $this->coreResponse(404, __('messages.no_data_with_pay_ment'));
        }
        $admin = auth()->user();
        DB::beginTransaction();
        try {
            //selseccredit, cash
            if ($data['type_pay'] === 'credit') {  //ตัดเครดิต
                $data['after']     =  round($sumAfter, 2);
                $this->handleCredit($bill, $admin, $totalPrice, $currentCredit, $data['after'], $addOnCredit);
                $this->checkTypePayUpdate($bill, $admin, $data['type']);
            } else { // เงินสด + ตัดเครงดิต
                if ($data['credit_withdraw']) { // credit withdraw
                    $data['after']     = round($sumAfter, 2);
                    if (@$type_pay_credit === 'topup') {
                        $price = $totalPrice + $currentCredit;
                    } else {
                        $price = $totalPrice;
                    }
                    // dd( $data['after']);
                    $this->handleCredit($bill, $admin, $price, $currentCredit, $data['after'], $addOnCredit);

                    if ($data['cash'] > 0) { //bill topup
                        $this->creditCashTopup($bill, $sumAfter, $data['cash'], $data['type']);
                        $this->checkTypePayUpdate($bill, $admin, $data['type']);
                    }
                } else { // receive change
                    $data['after']     =  0;
                    $this->handleCredit($bill, $admin, $totalPrice, $currentCredit,  $data['after'], $addOnCredit);
                    $this->checkTypePayUpdate($bill, $admin, $data['type']);
                }
            }

            $data['before']                 =  $currentCredit;
            $data['amount']                 =  $totalPrice;
            $data['status']                 =  'success';
            $data['approved_admin']         =  $admin->username;
            $data['approved_admin_id']      =  $admin->id;
            $data['approved_at']            =  date('Y-m-d H:i:s');

            $payment->update($data);
            $this->clearCacheTags();
            $this->clearCacheTagById($payment->id);
            app('App\Repositories\UserRepository')->clearCacheTagByUserCode($bill->user_code);

            DB::commit();
            return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.payment')]), $data);
        } catch (\Throwable $th) {
            DB::rollBack();
            return $this->coreResponse(500, $th->getMessage() ?? __('messages.not_delete_data', ['data' => __('messages.payment')]), $th->getTrace());
        }
    }


    public function softDeleteById($id)
    {
        $payment     =  $this->findByIdMethod($id);

        if (!$payment) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.payment'), 'id' => $id]));
        }

        $payment->delete();
        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.delete_data', ['data' => __('messages.payment')]), $payment);
    }

    public function findByIdMethod($id, array $columns = ['*'])
    {
        $key        = $this->getCacheKey(null, $id);
        $time       = $this->getTime(self::MINUTE_CACHE);

        $payment    = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($id)
        ])->remember($key, $time, function () use ($id, $columns) {
            return $this->payment::select($columns)->whereId($id)->first();
        });

        return $payment;
    }

    private function handleCredit($bill, $admin, $totalPrice, $before, $after, $addOnCredit)
    {

        $credit                    = new  $this->credit();
        $credit->user_code         = $bill->user_code;
        $credit->amount            = $after === 0 ?  $before : ($addOnCredit ?  $totalPrice + $before : $totalPrice);
        $credit->type              = 'withdraw';
        $credit->before            = $before;
        $credit->after             = $addOnCredit ?  - ($totalPrice) : $credit->before - $credit->amount;

        $credit->created_admin     = $admin->username;
        $credit->created_admin_id  = $admin->id;
        $credit->approved_at       = date('Y-m-d');
        $credit->approved_admin    = $admin->username;
        $credit->approved_admin_id = $admin->id;
        $bill->credits()->save($credit);
        app('App\Repositories\CreditRepository')->clearCacheTagByUserCode($bill->user_code);
        app('App\Repositories\UserRepository')->clearCacheTagByUserCode($bill->user_code);
    }

    private function creditCashTopup($bill, $after, $cash, $type)
    {
        $data = [
            'user_code'     => $bill->user_code,
            'method'        => 'cash',
            'amount'        =>  $cash,
            'date'          => date('Y-m-d'),
            'time'          => date('H:i'),
            'system_remark' => $cash . '/' . $after,
        ];
        $topup               =  app('App\Repositories\TopupRepository')->storeMethod($data);
        $topupApprove        =  app('App\Repositories\TopupRepository')->approveById($topup->id);


        if ($type === 'tracking' && !$topupApprove->error) {

            $queue = $this->packingQueue::firstOrNew([
                'user_code' => $bill->user_code,
                'bill_code' => $bill->no,
                'bill_id'   => $bill->id,
                'tracking'  => $bill->counter,
                'status'    => 'packing',
                'type'      => 'waiting',
            ]);
            $queue->save();
        }
    }

    private function checkTypePayUpdate($bill, $admin, $type)
    {
        if ($type === 'tracking') {
            $data = [
                'updated_admin'      => $admin->username,
                'updated_admin_id'   => $admin->id,
                'paid_at'            => date('Y-m-d H:i:s'),
                'status'             => $bill->status == 2 ? 3 : $bill->status
            ];
            app('App\Repositories\BillRepository')->updateById($bill->id, $data);
        } else if ($type === 'trip') {
            $data = [
                'updated_admin'      => $admin->username,
                'updated_admin_id'   => $admin->id,
                'status'             => $bill->status == 2 ? 3 : $bill->status
            ];
            app('App\Repositories\TripRepository')->updateById($bill->id, $data);
        }
    }

    public function findPaymentList($userCode, $type, array $columns = ['*'])
    {

        $payment          = $this->findPaymentListMethod($userCode, $type);

        if ($payment->isEmpty()) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.payment'), 'id' => $userCode]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.payment')]), $payment);
    }

    public function findPaymentListMethod($userCode, $type, array $columns = ['*'])
    {
        $key        = $this->getCacheKey(null, $userCode);
        $time       = $this->getTime(self::MINUTE_CACHE);


        $payment    = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($userCode)
        ])->remember($key, $time, function () use ($userCode, $type, $columns) {
            return $this->payment::select($columns)
                ->with(self::TABLE_RELETIONS)
                ->where(function ($query) use ($type) {
                    if (@$type == 'create') {
                        return $query->whereNull('topup_id');
                    } else {
                        return $query->whereNotNull('topup_id');
                    }
                })
                ->where('user_code', $userCode)
                ->where('status', 'waiting')->get();
        });

        return $payment;
    }

    private function checktypeBill($id, $type)
    {
        if ($type == 'tracking') {
            $bill   =  app('App\Repositories\BillRepository')->findByIdMethod($id);
            return  $bill;
        } else if ($type == 'trip') {
            $trip   = app('App\Repositories\TripRepository')->findByIdMethod($id);
            return  $trip;
        }
    }

    private function clearCacheTags()
    {
        Cache::tags([
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix()
        ])->flush();
    }

    private function clearCacheTagById($id)
    {
        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();
    }

    public function findByTopuUpIdMethod($id, array $columns = ['*'])
    {
        $key        = $this->getCacheKey(null, $id);
        $time       = $this->getTime(self::MINUTE_CACHE);

        $payment    = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($id)
        ])->remember($key, $time, function () use ($id, $columns) {
            return $this->payment::select($columns)->whereTopupId($id)->get();
        });

        return $payment;
    }
}
