<?php

namespace App\Http\Requests\Withdraw;

use App\Http\Requests\FormRequest;
use App\Rules\Base64Image;
class UpdateWithdrawRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.withdraw.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'charge'                   => 'required|numeric|min:0',
            'date'                     => 'sometimes|nullable|date_format:Y-m-d',
            'time'                     => 'sometimes|nullable|date_format:H:i',
            'user_bank_account_id'     => 'required_if:method,transfer|nullable|exists:bank_accounts,id',
            'user_remark'              => 'sometimes|nullable',
            'admin_remark'             => 'required_if:method,credit',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'charge'                   => __('form.withdraw.charge'),
            'date'                     => __('form.withdraw.date'),
            'time'                     => __('form.withdraw.time'),
            'user_bank_account_id'     => __('form.withdraw.user_bank_account_id'),
            'user_remark'              => __('form.withdraw.user_remark'),
            'admin_remark'             => __('form.withdraw.admin_remark'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
