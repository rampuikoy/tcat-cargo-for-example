<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateThaiShippingAreaPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('thai_shipping_area_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->double('price_a',8,2);
            $table->double('price_b',8,2);
            $table->double('price_c',8,2);
            $table->double('price_d',8,2);
            $table->double('price_e',8,2);
            $table->integer('province_id')->unsigned()->nullable();
            $table->integer('amphur_id')->unsigned()->nullable();
            $table->string('created_admin')->nullable();
            $table->string('updated_admin')->nullable();
            $table->integer('thai_shipping_method_id')->unsigned()->nullable();
            $table->integer('created_admin_id')->unsigned()->nullable();
            $table->integer('updated_admin_id')->unsigned()->nullable();

            $table->foreign('thai_shipping_method_id')->references('id')
                        ->on('thai_shipping_methods')->onDelete('set null');
            $table->foreign('created_admin_id')->references('id')
                        ->on('admins')->onDelete('set null');
            $table->foreign('updated_admin_id')->references('id')
                        ->on('admins')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('thai_shipping_area_prices');
    }
}
