<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\Admin;

class AdminPolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.admin.index');
    }

    public function show(Admin $admin)
    {
        return $admin->hasAnyPermission(['backend.admin.show', 'backend.admin.update']);
    }

    public function store(Admin $admin)
    {
        return $admin->can('backend.admin.store');
    }

    public function update(Admin $admin)
    {
        return $admin->can('backend.admin.update');
    }

    public function destroy(Admin $admin)
    {
        return $admin->can('backend.admin.destroy');
    }
}
