<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class BillPolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.bill.index');
    }
    public function show(Admin $admin)
    {
        return $admin->hasAnyPermission(['backend.bill.show', 'backend.bill.update']);
    }

    public function store(Admin $admin)
    {
        return $admin->can('backend.bill.store');
    }
    public function update(Admin $admin)
    {
        return $admin->can('backend.bill.update');
    }

    public function destroy(Admin $admin)
    {
        return $admin->can('backend.bill.destroy');
    }
    public function approve(Admin $admin)
    {
        return $admin->can('backend.bill.approve');
    }
    public function cancel(Admin $admin)
    {
        return $admin->can('backend.bill.destroy');
    }
}
