<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 20)->unique();
            $table->string('name');
            // $table->string('email')->unique();
            $table->string('email');
            $table->string('password');
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();

            $table->string('tel1', 20)->nullable();
            $table->string('tel2', 20)->nullable();
            $table->string('image')->default('default.png');

            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->enum('create_bill', ['all', 'self', 'admin'])->default('all');

            $table->enum('withholding', ['active', 'inactive'])->default('active');
            $table->string('tax_id', 50)->nullable();

            $table->enum('sms_notification', ['active', 'inactive'])->default('active');
            $table->enum('alert_box', ['active', 'inactive'])->default('active');
            $table->text('alert_message')->nullable();
            $table->text('fcm_token')->nullable();

            $table->text('user_remark')->nullable();
            $table->text('admin_remark')->nullable();
            $table->text('system_remark')->nullable();

            $table->timestamps();
            $table->integer('created_admin_id')->nullable()->unsigned();
            $table->integer('updated_admin_id')->nullable()->unsigned();
            $table->string('created_admin', 64)->nullable();
            $table->string('updated_admin', 64)->nullable();

            $table->index(['code', 'created_admin_id', 'updated_admin_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
