<?php

namespace Database\Seeders;

use App\Helpers\MoveSqlData;
use Illuminate\Database\Seeder;
// use App\Models\CommissionRate;

class CommissionRateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // CommissionRate::factory(5)->create();
        $move_data    = collect(MoveSqlData::insertCommissionRate());
        $message          = $move_data->get('message');
        $error            = $move_data->get('error');

        if ($error) {
            $this->command->error($message);
            $this->command->warning('Please insert fresh again');
            return;
        } else {
            $this->command->info($message);
        }
    }
}
