<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Interfaces\PaymentInterface;
use App\Http\Requests\Payment\StorePaymentRequest;
use App\Http\Requests\Payment\ApprovePaymentRequest;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    protected $paymentInterface;

    public function __construct(PaymentInterface $paymentInterface)
    {
        $this->paymentInterface = $paymentInterface;
    }

    public function index(Request $request)
    {
        $this->authorize('index', Payment::class);
        $columns        = $request->input('columns', ['*']);
        $payments   = $this->paymentInterface->all($columns);
        return response()->json($payments, $payments->code);
    }

    public function show(Request $request, $id)
    {
        $this->authorize('show', Payment::class);
        $columns        = $request->input('columns', ['*']);
        $payment    = $this->paymentInterface->findById($id, $columns);
        return response()->json($payment, $payment->code);
    }

    public function search(Request $request)
    {

        $this->authorize('index', Payment::class);
        $conditions     = $request->only([
            'code',
            'user_code',
            'before',
            'amount',
            'after',
            'cash',
            'payment_type_id',
            'status',
            'staff',
            'time_format',
            'date_start',
            'date_end',
            'payment_type_bill'
        ]);
        $columns        = $request->input('columns', ['*']);
        $limit          = $request->input('limit', $this->paymentInterface::DEFAULT_LIMIT);
        $sorting        = $request->input('order_by', $this->paymentInterface::DEFAULT_SORTING);
        $ascending      = $request->input('ascending') == 1 ? 'asc' : 'desc';

        if ($request->boolean('pagination')) {
            $page           = $request->input('page', 1);
            $payments   = $this->paymentInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset         = $request->input('page', 0);
            $payments   = $this->paymentInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($payments, $payments->code);
    }

    public function dropdown()
    {
        $dropdown = $this->paymentInterface->dropdownList();
        return response()->json($dropdown, $dropdown->code);
    }

    public function store(StorePaymentRequest $request)
    {

        $data = $request->only([
            'user_code',
            'payment_type_id',
            'bill_id',
            'type'
        ]);
        $payment   = $this->paymentInterface->store($data);
        return response()->json($payment, $payment->code);
    }

    public function approve(ApprovePaymentRequest $request, $id)
    {
        $data           = $request->only([
            'bill_id',
            'before',
            'cash',
            'amount',
            'payment_type_id',
            'cash',
            'credit_withdraw',
            'type',
            'type_pay', // credit , cash
            'add_on_total_price'
        ]);
        $payment   = $this->paymentInterface->approve($id, $data);

        return response()->json($payment, $payment->code);
    }
    public function destroy($id)
    {
        $this->authorize('destroy', Payment::class);
        $payment = $this->paymentInterface->softDeleteById($id);
        return response()->json($payment, $payment->code);
    }

    public function findPaymentListCreate(Request $request, $userCode)
    {

        $this->authorize('show', Payment::class);
        $columns        = $request->input('columns', ['*']);
        $payment    = $this->paymentInterface->findPaymentList($userCode, 'create', $columns);
        return response()->json($payment, $payment->code);
    }

    public function findPaymentListApprove(Request $request, $userCode)
    {

        $this->authorize('show', Payment::class);
        $columns        = $request->input('columns', ['*']);
        $payment    = $this->paymentInterface->findPaymentList($userCode, 'approve', $columns);
        return response()->json($payment, $payment->code);
    }
}
