<?php

namespace Database\Seeders;
use App\Models\WarehouseChinese;
use Illuminate\Database\Seeder;

class WarehouseChineseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        WarehouseChinese::factory(15)->create();
    }
}
