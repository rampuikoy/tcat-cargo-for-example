<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWithdrawsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('withdraws', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->datetime('approved_at')->nullable();
            $table->softDeletes();

            $table->double('amount', 8, 2);
            $table->double('charge', 6, 2);
            $table->double('receive', 10, 2);

            $table->date('date')->nullable();
            $table->string('time', 10)->nullable();

            $table->string('user_code', 20)->nullable();
            $table->enum('method', ['transfer', 'cash', 'cheque', 'other', 'credit'])->default('transfer');

            $table->integer('user_bank_account_id')->unsigned()->nullable();
            $table->string('user_bank', 10)->nullable();
            $table->string('user_account', 20)->nullable();

            $table->integer('system_bank_account_id')->unsigned()->nullable();
            $table->string('system_bank', 20)->nullable();
            $table->string('system_account', 20)->nullable();

            $table->enum('status', ['waiting', 'approved', 'cancel'])->default('waiting');

            $table->text('user_remark')->nullable();
            $table->text('admin_remark')->nullable();
            $table->text('system_remark')->nullable();

            // admin name reduce join query
            $table->string('created_admin')->nullable();
            $table->string('updated_admin')->nullable();
            $table->string('approved_admin')->nullable();

            $table->integer('created_admin_id')->unsigned()->nullable();
            $table->integer('updated_admin_id')->unsigned()->nullable();
            $table->integer('approved_admin_id')->unsigned()->nullable();

            $table->integer('bank_report_id')->default(0);

            $table->foreign('user_bank_account_id')->references('id')->on('bank_accounts')->onDelete('set null');
            $table->foreign('system_bank_account_id')->references('id')->on('bank_accounts')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('withdraws');
    }
}
