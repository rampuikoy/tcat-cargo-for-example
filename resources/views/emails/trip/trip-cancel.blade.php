@component('mail::message')
# เรียนคุณ {{$trip->user->name}}
แจ้งอัพเดทสถานะบิล <br>
รหัสลูกค้า: {{$trip->user->code}}

@component('mail::table')
| แจ้งอัพเดทสถานะบิล   |                   |
|:-------------|--------------:|
| เลขที่บิล | {{ $trip->no }} |
| รหัสลูกค้า| {{$trip->user->code}}|
| สถานะบิล | <span style="color: red; text-align: left;">ยกเลิกบิล</span> |
@endcomponent
	<p>
		<h3 style="color: red">ยกเลิกบิลแล้ว</h3>
		หมายเหตุ(ลูกค้า): {!!str_replace(' ', '&nbsp;',$trip->user_remark)!!} <br>
		หมายเหตุ(เจ้าหน้าที่): {!!str_replace(' ', '&nbsp;',$trip->admin_remark)!!} <br>
	</p>
@endcomponent

