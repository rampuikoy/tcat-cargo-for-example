<?php

namespace App\Interfaces;

use App\Http\Requests\Upload\UploadSingleRequest;

interface UploadInterface
{
    /**
     * create new upload
     *
     * @param array $data
     *
     * @return object|null
     */
    public function store(array $data);

    /**
     * delete upload by id
     *
     * @param $id
     *
     * @return object|null
     */
    public function delete($id);

}
