<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commissions', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('code')->nullable();
            $table->string('username')->nullable();
            $table->integer('admin_id')->unsigned()->nullable();
            $table->datetime('start_date')->nullable();
            $table->datetime('end_date')->nullable();
            $table->string('file');
            $table->double('normal_income', 10, 2)->default(0.00);
            $table->double('normal_commission', 10, 2)->default(0.00);
            $table->double('normal_profit', 10, 2)->default(0.00);
            $table->double('custom_income', 10, 2)->default(0.00);
            $table->double('custom_commission', 10, 2)->default(0.00);
            $table->double('custom_profit', 10, 2)->default(0.00);
            $table->double('total_commission', 10, 2)->default(0.00);
            $table->double('total', 10, 2)->default(0.00);
            $table->text('remark')->nullable();
            $table->integer('created_admin_id')->unsigned()->nullable();
            $table->integer('updated_admin_id')->unsigned()->nullable();
            $table->string('created_admin')->nullable();
            $table->string('updated_admin')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commissions');
    }
}
