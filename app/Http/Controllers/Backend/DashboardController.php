<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\Noti;
use App\Traits\CoreResponseTrait;

class DashboardController extends Controller
{
    use CoreResponseTrait;
    protected $creditInterface;

    public function __construct(Noti $noti)
    {
        $this->noti = $noti;
    }
    public function getNoti()
    {
        try {
            $noti = $this->noti->counterNoti();
            $data =  $this->coreResponse(200, __('messages.dashboard.noti'), $noti);
            return response()->json($data, $data->code);
        } catch (\Throwable $th) {
            $data = $this->coreResponse(500, $th->getMessage());
            return response()->json($data, $data->code);
        }
    }
}
