@php $total = $bill->shipping_price +
$bill->thai_shipping_method_price +
$bill->thai_shipping_method_charge;
@endphp
<table aria-describedby="bill-summary">
	<tr>
		<th scope="col"></th>
	</tr>
	<tbody>
		<tr>
			<td class="text-right" width="60%">
				<div>@if($bill->shipping_min_rate)(*@lang('admin.min_rate_charge'))@endif&nbsp;@lang('admin.oversea_shipping_method_price')(บาท)&nbsp;:</div>
			</td>
			<td class="text-center bordered">
				<div>{{number_format($bill->shipping_price,2)}}</div>
			</td>
		</tr>
		<tr>
			<td class="text-right">
				<div>@lang('admin.thai_shipping_method_price')(บาท)&nbsp;:</div>
			</td>
			<td class="text-center bordered">
				<div>{{number_format($bill->thai_shipping_method_price+$bill->thai_shipping_method_charge,2)}}</div>
			</td>
		</tr>
		@if($bill->cost_box_thai>0)
		<tr>
			<td class="text-right">
				<div>ค่าตีลังไม้(บาท)&nbsp;:</div>
			</td>
			<td class="text-center bordered">
				<div>{{number_format($bill->cost_box_thai,2)}}</div>
			</td>
		</tr>
		@endif
		@if($bill->cost_shipping_thai>0)
		<tr>
			<td class="text-right">
				<div>ค่าขนส่งที่จีน(บาท)&nbsp;:</div>
			</td>
			<td class="text-center bordered">
				<div>{{number_format($bill->cost_shipping_thai,2)}}</div>
			</td>
		</tr>
		@endif
		@if($bill->coupon_price)
		<tr>
			<td class="text-right">
				<div>@lang('admin.coupon')(บาท)&nbsp;:</div>
			</td>
			<td class="text-center bordered">
				<div>-{{number_format($bill->coupon_price,2)}}</div>
			</td>
		</tr>
		@php $total += $bill->other_charge_price1; @endphp
		@endif
		@if($bill->other_charge_price1)
		<tr>
			<td class="text-right">
				<div>{!!str_replace(' ', '&nbsp;',$bill->other_charge_title1)!!}(@lang('admin.addon_charge'))(บาท)&nbsp;:</div>
			</td>
			<td class="text-center bordered">
				<div>{{number_format($bill->other_charge_price1,2)}}</div>
			</td>
		</tr>
		@php $total += $bill->other_charge_price1; @endphp
		@endif
		@if($bill->other_charge_price2)
		<tr>
			<td class="text-right">
				<div>{!!str_replace(' ', '&nbsp;',$bill->other_charge_title2)!!}(@lang('admin.addon_charge'))(บาท)&nbsp;:</div>
			</td>
			<td class="text-center bordered">
				<div>{{number_format($bill->other_charge_price2,2)}}</div>
			</td>
		</tr>
		@php $total += $bill->other_charge_price2; @endphp
		@endif
		@if($bill->discount_price1)
		<tr>
			<td class="text-right">
				<div>{!!str_replace(' ', '&nbsp;',$bill->discount_title1)!!}(@lang('admin.discount'))(บาท)&nbsp;:</div>
			</td>
			<td class="text-center bordered">
				<div>-{{number_format($bill->discount_price1,2)}}</div>
			</td>
		</tr>
		@php $total += $bill->discount_price1; @endphp
		@endif
		@if($bill->discount_price2)
		<tr>
			<td class="text-right">
				<div>{!!str_replace(' ', '&nbsp;',$bill->discount_title2)!!}(@lang('admin.discount'))(บาท)&nbsp;:</div>
			</td>
			<td class="text-center bordered">
				<div>-{{number_format($bill->discount_price2,2)}}</div>
			</td>
		</tr>
		@php $total += $bill->discount_price2; @endphp
		@endif
		<tr style="font-weight: bold">
			<td class="text-right">
				<div>@lang('admin.sub_total')&nbsp;:</div>
			</td>
			<td class="text-center bordered">
				<div>{{number_format($bill->sub_total,2)}}</div>
			</td>
		</tr>
	</tbody>
	<tfoot>
		@if($bill->withholding)
		<tr style="font-weight: bold">
			<td class="text-right">
				<div>{!!str_replace(' ', '&nbsp;',trans('admin.withholding'))!!}&nbsp;:</div>
			</td>
			<td class="text-center" style="border-bottom: 1px solid;">
				<div>{{ ($bill->withholding)? number_format($bill->withholding_amount,2) : '-'}}</div>
			</td>
		</tr>
		@endif
		<tr style="font-weight: bold;">
			<td class="text-right">
				<div>@lang('admin.bill_total')&nbsp;:</div>
			</td>
			<td class="text-center" style="border-bottom: 1px solid;">
				<div style="font-size: 20px;">{{number_format($bill->total_price,2)}}</div>
			</td>
		</tr>
	</tfoot>
</table>