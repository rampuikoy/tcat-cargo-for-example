<?php

namespace App\Models;

use App\Notifications\Backend\ResetPassword;
use App\Notifications\Backend\VerifyEmail;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Admin extends Authenticatable implements JWTSubject, MustVerifyEmail
{
    use Notifiable, HasRoles, SoftDeletes;

    protected $guard = 'admins';

    public static $imageFolder = 'uploads/admin/';
    public static $imageWidth = 300;
    public static $imageHeight = 300;

    public static function statusList()
    {
        $array = [
            'active'    => __('messages.active'),
            'inactive'  => __('messages.inactive'),
        ];
        return $array;
    }

    public static function typeList()
    {
        $array = [
            'admin'         => __('messages.administrator'),
            'staff'         => __('messages.staff'),
            'accountant'    => __('messages.accountant'),
            'warehouse'     => __('messages.warehouse'),
            'sale'          => __('messages.sale'),
            'driver'        => __('messages.driver'),
        ];
        return $array;
    }

    public static function departmentList()
    {
        $array = [
            'tdar'          => 'TDAR',
            'cargo'         => 'TCAT-CARGO',
            'taobao'        => 'TAOBAO',
            'tmall'         => 'TCAT-MALL',
            'alibaba'       => 'ALIBABA',
            'express'       => 'TCAT-EXPRESS',
            'china'         => 'CHINA TRIP TOUR',
        ];
        return $array;
    }

    public static function branchList()
    {
        $array = [
            'headquarter'   => __('messages.headquarter'),
            'branch'        => __('messages.other_branch'),
        ];
        return $array;
    }

    public static function dateTypeList()
    {
        $array = [
            'created_at'    => __('messages.created_at'),
            'updated_at'    => __('messages.updated_at'),
        ];
        return $array;
    }

    public static function conditionList()
    {
        $array = [
            'verifed'       => __('messages.conditions.only_verified'),
            'unverifed'     => __('messages.conditions.only_unverified'),
        ];
        return $array;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'lastname',
        'username',
        'email',
        'password',
        'photo',
        'role',

        'tel',
        'status',
        'type',
        'department',
        'branch',
        'droppoint_id',
        'regex_user',
        'pattern_user',
        'remark',
        'created_admin_id',
        'updated_admin_id',
        'created_admin',
        'updated_admin',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'photo_url',
        'status_text',
        'type_text',
        'department_text',
        'branch_text',
    ];

    /**
     * Get the profile photo URL attribute.
     *
     * @return string
     */
    public function getPhotoUrlAttribute($handle = null)
    {
        $file = $handle ?? $this->photo;
        return ($file) ?  env('APP_MEDIA_URL') . '/' . self::$imageFolder . $file : null;
    }
    /**
     * Get the name for enum columns
     *
     * @return string
     */
    public function getStatusTextAttribute()
    {
        return collect(self::statusList())->get($this->status);
    }
    public function getTypeTextAttribute()
    {
        return collect(self::typeList())->get($this->type);
    }
    public function getDepartmentTextAttribute()
    {
        return collect(self::departmentList())->get($this->department);
    }
    public function getBranchTextAttribute()
    {
        return collect(self::branchList())->get($this->branch);
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail);
    }

    /**
     * @return int
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function scopeSearch($query, $conditions)
    {
        return $query->where(function ($query) use ($conditions) {
            if (@$conditions['id']) {
                $query->where('id', $conditions['id']);
            }
            if (@$conditions['username']) {
                $query->where('username', 'LIKE', "%{$conditions['username']}%");
            }
            if (@$conditions['email']) {
                $query->where('email', 'LIKE', "%{$conditions['email']}%");
            }
            if (@$conditions['tel']) {
                $query->where('tel', 'LIKE', "%{$conditions['tel']}%");
            }
            if (@$conditions['remark']) {
                $query->where('remark', 'LIKE', "%{$conditions['remark']}%");
            }
            if (@$conditions['department']) {
                $query->where('department', $conditions['department']);
            }
            if (@$conditions['branch']) {
                $query->where('branch', $conditions['branch']);
            }
            if (@$conditions['droppoint_id']) {
                $query->where('droppoint_id', $conditions['droppoint_id']);
            }
            if (@$conditions['type']) {
                $query->where('type', $conditions['type']);
            }
            if (@$conditions['role']) {
                $query->where('role', $conditions['role']);
            }
            if (@$conditions['status']) {
                $query->where('status', $conditions['status']);
            }
        });
    }

    // relasion
    public function rate()
    {
        return $this->hasOne('App\Models\Rate', 'admin_id');
    }
}
