<?php

namespace Tests\Unit\Repositories;

use App\Models\Admin;
use App\Repositories\AdminRepository;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use tests\TestCase;

class AdminRepositoryTest extends TestCase
{

    use WithoutMiddleware;

    // protected $mockModel;

    // protected function mockRepo()
    // {
    //     $mockModel = $this->mock(Admin::class, function($mock){
    //         $mock->shouldReceive('select')->with('orderBy');
    //     });
    //     $this->mockModel = $this->instance(Admin::class, $mockModel);
    //     $repo = new AdminRepository($this->mockModel);
    //     return $repo;
    // }

    /** @test */
    public function test_it_admin_repository_all()
    {
        // $repo = $this->mockRepo();

        // $this->assertNotNull($repo->all());

        // $this->assertInstanceOf(Collection::class, $repo->all());

        $this->assertTrue(true);
    }

    // public function test_it_admin_repository_fetchListByFields()
    // {
    //     $repo = $this->mockRepo();

    //     $this->assertNotNull($repo->fetchListByFields(["name" => "test"]));

    //     $this->assertInstanceOf(Collection::class, $repo->fetchListByFields(["name" => "test"]));
    // }

    // public function test_it_admin_repository_paginateListByFields()
    // {
    //     $repo = $this->mockRepo();

    //     $this->assertNotNull($repo->paginateListByFields(["name" => "test"]));

    //     $this->assertInstanceOf(LengthAwarePaginator::class, $repo->paginateListByFields(["name" => "test"]));
    // }

    // public function test_it_admin_repository_findById()
    // {
    //     $repo = $this->mockRepo();

    //     $this->assertNotNull($repo->findById(1));

    //     $this->assertEquals(false, $repo->findById(1));
    // }

    // public function test_it_admin_repository_dropdownList()
    // {
    //     $repo = $this->mockRepo();

    //     $this->assertNotNull($repo->dropdownList());

    //     $this->assertArrayHasKey('roles', $repo->dropdownList());
    //     $this->assertArrayHasKey('date_type', $repo->dropdownList());
    //     $this->assertArrayHasKey('condition', $repo->dropdownList());
    // }
}
