<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Seo\UpdateSeoRequest;
use App\Interfaces\SeoInterface;
use Illuminate\Http\Request;

class SeoController extends Controller
{
    protected $seoInterface;

    public function __construct(SeoInterface $seoInterface)
    {
        $this->seoInterface = $seoInterface;
    }

    public function index(Request $request)
    {
        $columns    = $request->input('columns', ['*']);
        $seo        = $this->seoInterface->getDefault($columns);
        return response()->json($seo, $seo->code);
    }

    public function update(UpdateSeoRequest $request)
    {
        $data   = $request->only([
                    'title',
                    'keyword',
                    'description',
                    'code',
                    'footer'
                ]);
        $seo    = $this->seoInterface->update($data);
        return response()->json($seo, $seo->code);
    }
}
