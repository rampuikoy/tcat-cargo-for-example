<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Admin;
// use App\Models\EmailQueue;
use Tymon\JWTAuth\Facades\JWTAuth;

class SmsQueueTest extends TestCase
{
    use WithFaker;
    protected $user;
    protected $emailQueue;
    protected $url = '/api/backend/sms-queue';
    /**
     * A basic feature test example.
     *
     * @return void
     */
    protected function authenticate(string $guard = 'admins')
    {
        $this->user = factory(Admin::class)->create();
        // $this->emailQueue = EmailQueue::created([
        //     'mailable_class' => 'App\Mail\User\UserRegistered',
        //     'mailable_type ' => 'App\Models\User',
        //     'to' => 'TcatTest@example.net',
        //     'status' => 'queue'
        // ]);
        auth()->shouldUse($guard);
        $token = JWTAuth::fromUser($this->user);
        return 'Bearer ' . $token;
    }

    protected function deleteUser()
    {
        // $this->emailQueue->forceDelete();
        $this->user->forceDelete();
    }

    /** @test */
    public function GetSmsQueueSearchSuccessTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('GET', "{$this->url}/search");
        $response->assertStatus(200);
        $this->deleteUser();
    }

    /** @test */
    public function GetSmsQueueSearchFailTest()
    {
        $response = $this->json('GET', "{$this->url}/search");
        $response->assertStatus(401);
    }

    /** @test */
    public function GetSmsQueueDropdownSuccessTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('GET', "{$this->url}/dropdown");
        $response->assertStatus(200);
        $this->deleteUser();
    }

    /** @test */
    public function GetSmsQueueDropdownFailTest()
    {
        $response = $this->json('GET', "{$this->url}/dropdown");
        $response->assertStatus(401);
    }

    // /** @test */
    // public function GetEmailQueueUpdateSuccessTest()
    // {
    //     $token = $this->authenticate();
    //     $response = $this->withHeaders([
    //         'Authorization' => $token,
    //     ])->json('POST', "{$this->url}/queue-all", [
    //         'ids' => [$this->emailQueue->id]
    //     ]);
    //     $response->assertStatus(200);
    //     $this->deleteUser();
    // }

    /** @test */
    public function GetSmsQueueUpdateFailTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('POST', "{$this->url}/queue-all");
        $response->assertStatus(422);
        $this->deleteUser();
    }

    /** @test */
    public function GetSmsQueueUpdateFailAuthTest()
    {
        $response = $this->json('POST', "{$this->url}/queue-all");
        $response->assertStatus(401);
    }
}
