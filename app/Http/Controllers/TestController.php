<?php

namespace App\Http\Controllers;

use App\Traits\CacheTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class TestController extends Controller
{
    use CacheTrait;
    public function webHookTest(Request $request){

        return response()->json('no message', 200);
    }
}
