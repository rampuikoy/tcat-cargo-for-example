<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Chat\StoreBroadcastRequest;
use App\Http\Requests\Chat\UpdateBroadcastRequest;
use App\Interfaces\BroadcastInterface;
use App\Models\Chat;
use Illuminate\Http\Request;

class BroadcastController extends Controller
{
    protected $broadcastInterface,$userInterface;

    public function __construct(BroadcastInterface $broadcastInterface)
    {
        $this->broadcastInterface = $broadcastInterface;
    }
    public function index(Request $request)
    {
        $this->authorize('index', Chat::class);
        $columns    = $request->input('columns', ['*']);
        $broadcast     = $this->broadcastInterface->all($columns);
        return response()->json($broadcast, $broadcast->code);
    }

    public function show(Request $request, $code)
    {
        $this->authorize('show', Chat::class);
        $columns    = $request->input('columns', ['*']);
        $broadcast     = $this->broadcastInterface->findById($code, $columns);
        return response()->json($broadcast, $broadcast->code);
    }

    public function search(Request $request)
    {
        $this->authorize('index', Chat::class);
        $columns    = $request->input('columns', ['*']);
        $conditions = $request->only(['message', 'status', 'date_start', 'date_end']);
        $limit      = $request->input('limit', $this->broadcastInterface::DEFAULT_LIMIT);
        $sorting    = $request->input('order_by', $this->broadcastInterface::DEFAULT_SORTING);
        $ascending  = $request->input('ascending') == 1 ? 'asc' : 'desc';

        if ($request->boolean('pagination')) {
            $page   = $request->input('page', 1);
            $broadcast = $this->broadcastInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset = $request->input('page', 0);
            $broadcast = $this->broadcastInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($broadcast, $broadcast->code);
    }
    public function dropdown()
    {
        $dropdown = $this->broadcastInterface->dropdownList();
        return response()->json($dropdown, $dropdown->code);
    }

    public function store(StoreBroadcastRequest $request)
    {
        $data       = $request->only([
                        'message',
                        'file',
                        'status'
                    ]);
        $broadcast  = $this->broadcastInterface->store($data);
        return response()->json($broadcast, $broadcast->code);
    }

    public function update(UpdateBroadcastRequest $request, $id)
    {
        $data       = $request->only([
                        'message',
                        'file',
                        'status'
                    ]);
        $broadcast  = $this->broadcastInterface->updateById($id, $data);
        return response()->json($broadcast, $broadcast->code);
    }
}
