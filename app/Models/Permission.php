<?php

namespace App\Models;

class Permission extends \Spatie\Permission\Models\Permission
{

    protected $table = 'permissions';
    protected $fillable = ['name', 'guard_name', 'tag', 'description', 'status'];

    static $crudMethod = [
        'index' => true,
        'show' => true,
        'store' => true,
        'update' => true,
        'destroy' => true,
        'approve' => true,
    ];

    static $extendMethod = ['extend' => true];

    public static function getArray($name, $tag, $method, $status = 'showing', $description = null)
    {
        return [
            'name' => $name,
            'tag' => $tag,
            'status' => $status,
            'methods' => $method,
            'description' => $description,
        ];
    }

    public static function getExtendArray($name, $tag, $method, $description = null)
    {
        return [
            'name' => $name,
            'tag' => $tag,
            'description' => $description,
            'methods' => $method,
        ];
    }

    public static function defaultPermissions()
    {
        $permissions = [

            self::getArray('backend.admin', 'เจ้าหน้าที่', self::$crudMethod),

            self::getArray('backend.permission', 'สิทธิ์การเข้าถึง', self::$crudMethod),

            self::getArray('backend.role', 'ระดับเจ้าหน้าที่', self::$crudMethod, 'hidden'),

            self::getArray('backend.article', 'บทความ', self::$crudMethod),

            self::getArray('backend.chat', 'กล่องข้อความ', self::$crudMethod),

            self::getArray('backend.tracking', 'Tracking', self::$crudMethod),

            self::getArray('backend.bill', 'บิลขนส่ง', self::$crudMethod),

            self::getArray('backend.nocode', 'NoCode', self::$crudMethod),

            self::getArray('backend.user', 'ลูกค้า', self::$crudMethod),

            self::getArray('backend.user-address', 'ที่อยู่จัดส่ง (ลูกค้า)', self::$crudMethod),

            self::getArray('backend.bank-account', 'บัญชีธนาคาร (ลูกค้า)', self::$crudMethod),

            self::getArray('backend.topup', 'เติมเครดิต', self::$crudMethod),

            self::getArray('backend.withdraw', 'ถอนเครดิต', self::$crudMethod),

            self::getArray('backend.money-transfer', 'โอนเงินไปจีน', self::$crudMethod),

            self::getArray('backend.money-receive', 'รับเงินจีน', self::$crudMethod),

            self::getArray('backend.claim', 'เคลมสินค้า', self::$crudMethod),

            self::getArray('backend.payment', 'รายการชำระเงิน', self::$crudMethod),

            self::getArray('backend.trip', 'บิลทริปทัวร์', self::$crudMethod),

            self::getArray('backend.cms', 'ระบบจัดการหน้าเว็บ', self::$crudMethod),

            self::getArray('backend.oversea-shipping-method', 'การขนส่งระหว่างประเทศ', self::$crudMethod),

            self::getArray('backend.thai-shipping-area-price', 'การขนส่งภายในประเทศ (ตามระยะทาง)', self::$crudMethod),

            self::getArray('backend.thai-shipping-weight-price', 'การขนส่งภายในประเทศ (คิดตามน้ำหนัก)', self::$crudMethod),

            self::getArray('backend.thai-shipping-weight-size-price', 'การขนส่งภายในประเทศ (คิดตามขนาด/น้ำหนัก)', self::$crudMethod),

            self::getArray('backend.thai-shipping-method', 'การขนส่งในประเทศ', self::$crudMethod),

            self::getArray('backend.product', 'สินค้า', self::$crudMethod),

            self::getArray('backend.product-type', 'ประเภทสินค้า', self::$crudMethod),

            self::getArray('backend.point-order', 'แลกสินค้า', self::$crudMethod),

            self::getArray('backend.shipping-rate', 'shipping-rate', self::$crudMethod),

            self::getArray('backend.warehouse', 'โกดัง', self::$crudMethod),

            self::getArray('backend.warehouse-chinese', 'โกดังจีน', self::$crudMethod),

            self::getArray('backend.truck', 'รถบริษัท', self::$crudMethod),

            self::getArray('backend.bank-system', 'บัญชีธนาคารระบบ', self::$crudMethod),

            self::getArray('backend.exchange-rate', 'อัตราแลกเปลี่ยน', self::$crudMethod),

            self::getArray('backend.coupon', 'คูปอง', self::$crudMethod),

            self::getArray('backend.point', 'แต้มสะสม', self::$crudMethod ),

            self::getArray('backend.noti', 'ระบบแจ้งเตือน', self::$crudMethod),

            self::getArray('backend.broadcast', 'ข้อความ Broadcast', self::$crudMethod),

            self::getArray('backend.rate', 'อัตราค่าขนส่ง', self::$crudMethod),

            self::getArray('backend.credit', 'เครดิต', self::$crudMethod),

            /**
             *
             * Extends
             *
             */

            self::getExtendArray('extend.menu-create-bill', 'สิทธิพิเศษ', self::$extendMethod, 'ลูกค้า - หน้าออกบิล'),

            self::getExtendArray('extend.user-reference', 'สิทธิพิเศษ', self::$extendMethod, 'ลูกค้า - เซลล์ที่รับผิดชอบ'),

            self::getExtendArray('extend.user-rate', 'สิทธิพิเศษ', self::$extendMethod, 'ลูกค้า - กำหนดอัตราค่าขนส่ง'),

            self::getExtendArray('extend.tracking-user', 'สิทธิพิเศษ', self::$extendMethod, 'Tracking - แก้ไข (เฉพาะลูกค้า)'),

            self::getExtendArray('extend.bill-prepair-queue', 'สิทธิพิเศษ', self::$extendMethod, 'บิลขนส่ง - เพิ่มคิวเตรียมล่วงหน้า'),

            self::getExtendArray('extend.bill-pack-queue', 'สิทธิพิเศษ', self::$extendMethod, 'บิลขนส่ง - เพิ่มคิวรอแพ็ค'),

            self::getExtendArray('extend.bill-pack', 'สิทธิพิเศษ', self::$extendMethod, 'บิลขนส่ง - ยิงแพ็คสินค้า'),

            self::getExtendArray('extend.bill-out', 'สิทธิพิเศษ', self::$extendMethod, 'บิลขนส่ง - ยิงบิลออกจากโกดัง'),

            self::getExtendArray('extend.bill-droppoint-in', 'สิทธิพิเศษ', self::$extendMethod, 'บิลขนส่ง - ยิงบิลเข้าจุดรับสินค้า'),

            self::getExtendArray('extend.bill-gps', 'สิทธิพิเศษ', self::$extendMethod, 'บิลขนส่ง - รายงานตำแหน่ง GPS'),

            self::getExtendArray('extend.commission', 'สิทธิพิเศษ', self::$extendMethod, 'บิลขนส่ง - บันทึกค่าคอมฯ'),

            self::getExtendArray('extend.commission-rate', 'สิทธิพิเศษ', self::$extendMethod, 'บิลขนส่ง - กำหนดเรทค่าคอมฯ'),

            self::getExtendArray('extend.bill-backward', 'สิทธิพิเศษ', self::$extendMethod, 'บิลขนส่ง - ย้อนบิล'),

            self::getExtendArray('extend.credit', 'สิทธิพิเศษ', self::$extendMethod, 'Statement'),

            self::getExtendArray('extend.report', 'สิทธิพิเศษ', self::$extendMethod, 'รายงาน'),

            self::getExtendArray('extend.barter-card', 'สิทธิพิเศษ', self::$extendMethod, 'เติมเครดิต - อนุมัติบิล Bartercard'),

            self::getExtendArray('extend.menu-store-bill', 'สิทธิพิเศษ', self::$extendMethod, 'Menu Store Bill'),

        ];

        return $permissions;
    }

    public function scopeShowOnly($query)
    {
        return $query->where('status', 'showing');
    }

    public function pivotRoles()
    {
        return $this->belongsToMany('App\Models\Role', 'role_has_permissions', 'permission_id', 'role_id');
    }

    public function scopeSearch($query, $conditions)
    {
        return $query->where(function ($query) use ($conditions) {
            if (@$conditions['id']) {
                $query->where('id', $conditions['id']);
            }
            if (@$conditions['search']) {
                $query->where('name', 'LIKE', "%{$conditions['search']}%")
                    ->orWhere('tag', 'LIKE', "%{$conditions['search']}%")
                    ->orWhere('description', 'LIKE', "%{$conditions['search']}%");
            }
            if (@$conditions['name']) {
                $query->where('name', 'LIKE', "%{$conditions['name']}%");
            }

        });
    }

}
