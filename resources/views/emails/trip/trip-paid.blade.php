@component('mail::message')
# เรียนคุณ {{$trip->user->name}}
แจ้งอัพเดทสถานะบิล <br>
รหัสลูกค้า: {{$trip->user->code}}

@component('mail::table')
| แจ้งอัพเดทสถานะบิล   |                   |
|:-------------|--------------:|
| เลขที่บิล | {{ $trip->no }} |
| รหัสลูกค้า| {{$trip->user->code}}|
| สถานะบิล | <span style="color: red; text-align: left;">ชำระเงินแล้ว</span> |
@endcomponent
<?php
$credit = ($trip->user->credit)? $trip->user->credit->total : 0 ;
$remain_credit = $credit - $trip->total_price;
?>
<p>
  <h3>รายละเอียด: </h3>
  <?php $detail = str_replace(' ', '&nbsp;',$trip->detail) ?>
  {!! nl2br($detail) !!}
</p>
<p>
  <h3  style="color: green">ชำระเงินเรียบร้อยแล้ว</h3>
  ยอดเครดิตคงเหลือของคุณ: {{number_format($credit, 2)}} บาท<br><br>
</p>
@endcomponent

