<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PackingQueue extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_code',
        'bill_code',
        'bill_id',
        'tracking',
        'status',
        'type',
        'updated_at',
        'created_admin',
        'created_admin_id',
    ];

    public function bill()
    {
        return $this->belongsTo('App\Bill','bill_id','id');
    }
}
