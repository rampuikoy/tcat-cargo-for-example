<?php

namespace App\Models;

use App\Helpers\ScopeQueries;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FcmQueue extends Model
{
    use HasFactory;

    protected $table = 'fcm_queues';

    protected $fillable = [
        'title',
        'message',
        'user_code',
        'status',
        'fcmable_type',
        'fcmable_id',
    ];

    protected $appends = ['status_text', 'type_text'];

    public static function statusList()
    {
        $array = [
            'queue' => __('messages.queue'),
            'success' => __('messages.sended'),
            'cancel' => __('messages.cancel'),
        ];
        return $array;
    }

    public static $typeList = [
        'App\Models\Withdraw' => 'บิลถอนเครดิต',
    ];

    public function getStatusTextAttribute()
    {
        return collect(self::statusList())->get($this->status);
    }

    public function getTypeTextAttribute()
    {
        return collect(self::$typeList)->get($this->fcmable_type);
    }

    // releation
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_code', 'code');
    }

    public function fcmable()
    {
        return $this->morphTo();
    }

    // scoped query sql
    public function scopeSearch($query, $conditions)
    {
        return $query->where(function ($query) use ($conditions) {
            if (@$conditions['id']) {
                $query->where('id', $conditions['id']);
            }
            if (@$conditions['title']) {
                $query->where('title', 'LIKE', "%{$conditions['title']}%");
            }
            if (@$conditions['message']) {
                $query->where('message', 'LIKE', "%{$conditions['message']}%");
            }
            if (@$conditions['status']) {
                $query->whereStatus($conditions['status']);
            }
            if (@$conditions['user_code']) {
                $query->whereUserCode($conditions['user_code']);
            }
            $date_type = (@$conditions['date_type']) ? $conditions['date_type'] : 'created_at';
            if (@$conditions['date_start'] && @$conditions['date_end']) {
                ScopeQueries::scopeDateRange($query, $date_type, $conditions['date_start'], $conditions['date_end']);
            } else if (@$conditions['date_start']) {
                ScopeQueries::scopeDateStart($query, 'created_at', $conditions['date_start']);
            } else if (@$conditions['date_end']) {
                ScopeQueries::scopeDateEnd($query, 'created_at', $conditions['date_end']);
            }
        });
    }
}
