<?php

namespace App\Http\Requests\User;

use App\Http\Requests\FormRequest;

class SettingNotificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.user.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sms_notification'  => 'required|in:active,inactive',
            'alert_box'         => 'required|in:active,inactive',
            'alert_message'     => 'required_if:alert_box,active',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'sms_notification'  => __('form.user.sms_notification'),
            'alert_box'         => __('form.user.alert_box'),
            'alert_message'     => __('form.user.alert_message'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
