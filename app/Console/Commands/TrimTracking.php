<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Models\Tracking;
use Illuminate\Console\Command;

class TrimTracking extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'trim:tracking';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Trim space from trackings';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Tracking $tracking,User $user)
    {
        $this->tracking=$tracking;
        $this->user=$user;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('run trim space from user_code trackings...');

        $trackings = $this->tracking->select('user_code')->whereRaw('user_code LIKE  "% %"')->limit(100)->get();
        collect($trackings)
            ->unique('user_code')
            ->map(function ($track) {
                if ($track) {
                    $tmp = str_replace("\r\n", "", $track->user_code);
                    $tmp = str_replace(' ', '', $tmp);
                    $tmp = strip_tags($tmp);
                    $tmp = str_replace(chr(194) . chr(160), '', $tmp);
                    $this->user->whereCode($track->user_code)->get()->each(function ($user) use ($tmp) {
                        $user->update([
                            'code'  =>  $tmp,
                        ]);
                    });
                }
            });

        $this->info('run trim space from code trackings...');

        $trackings = $this->tracking->whereRaw('code LIKE  "% %"')->limit(100)->get();
        collect($trackings)
            ->map(function ($track){
                if ($track) {
                    $tmp = str_replace("\r\n", "", $track->code);
                    $tmp = str_replace(' ', '', $tmp);
                    $tmp = strip_tags($tmp);
                    $tmp = str_replace(chr(194) . chr(160), '', $tmp);
                    $track->update([
                        'code'  =>  $tmp,
                    ]);
                }
            });
    }
}
