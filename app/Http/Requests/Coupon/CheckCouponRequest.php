<?php

namespace App\Http\Requests\Coupon;

use Illuminate\Foundation\Http\FormRequest;

class CheckCouponRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_code' => 'required|exists:users,code',
            'thai_shipping_method_id' => 'required|exists:thai_shipping_methods,id',
            'bill' => 'required|in:shipping,money_transfer,trip,all',
        ];
    }
}
