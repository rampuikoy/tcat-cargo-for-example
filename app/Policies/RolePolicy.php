<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.role.index');
    }

    public function show(Admin $admin)
    {
        return $admin->hasAnyPermission(['backend.role.show', 'backend.role.update']);
    }

    public function store(Admin $admin)
    {
        return $admin->can('backend.role.store');
    }

    public function update(Admin $admin)
    {
        return $admin->can('backend.role.update');
    }

    public function destroy(Admin $admin)
    {
        return $admin->can('backend.role.destroy');
    }
}
