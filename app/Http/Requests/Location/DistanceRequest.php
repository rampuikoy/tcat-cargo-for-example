<?php

namespace App\Http\Requests\Location;

use App\Http\Requests\FormRequest;
// use App\Rules\Latitude;
// use App\Rules\Longitude;

class DistanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $rules = [
            'origin.latitude'       => 'required|numeric',
            'origin.longitude'      => 'required|numeric',

            'destination.district'  => 'required|string',
            'destination.amphur'    => 'required|string',
            'destination.province'  => 'required|string',
            'destination.zipcode'   => 'required',   
        ];
        return $rules;
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'origin.latitude'          => __('form.origin.latitude'),
            'origin.longitude'         => __('form.origin.longitude'),

            'destination.district'     => __('form.destination.district'),
            'destination.amphur'       => __('form.destination.amphur'),
            'destination.province'     => __('form.destination.province'),
            'destination.zipcode'           => __('form.destination.zipcode'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
