<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\FcmQueue;
use DB;

class GenTrackingFcm extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tracking:fcm';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate tracking fcm';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(FcmQueue $fcmQueue)
    {
        $this->fcmQueue = $fcmQueue;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Generate tracking fcm...');

        $date  = strtotime(date("Y-m-d") . ' -3 months');
        $users = DB::table('trackings')
                    ->select(DB::raw('user_code, COUNT(*) AS total'))
                    ->where('status', 3)
                    ->where('thai_in', '>', date('Y-m-d', $date))
                    ->whereNotNull('user_code')
                    ->groupBy('user_code')
                    ->get();

        collect($users)
            ->map(function ($user) {
                $this->fcmQueue->create([
                    'user_code' =>  $user->user_code,
                    'title'     =>  __('messages.fcm.tracking_wait_title'),
                    'message'   =>  __('messages.fcm.tracking_wait_message', [
                        'total' =>  $user->total
                    ])
                ]);
            });
    }
}
