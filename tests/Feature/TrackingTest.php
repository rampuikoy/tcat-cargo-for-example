<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Admin;
use Tymon\JWTAuth\Facades\JWTAuth;

class TrackingTest extends TestCase
{
    use WithFaker;
  protected $user;
  protected $productType;
  protected $url = '/api/backend/tracking';
  /**
   * A basic feature test example.
   *
   * @return void
   */
  protected function authenticate(string $guard = 'admins')
  {
    $this->user = factory(Admin::class)->create();
    auth()->shouldUse($guard);
    $this->productType = factory(ProductType::class)->create();
    $token = JWTAuth::fromUser($this->user);
    return 'Bearer ' . $token;
  }

  protected function deleteUser()
  {
    $this->productType->forceDelete();
    return Admin::find($this->user->id)->forceDelete();
  }

  /** @test */
  public function getDropdownSuccess(){
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', $this->url."/dropdown");
    $response->assertStatus(200);
    $this->deleteUser();
  }
}
