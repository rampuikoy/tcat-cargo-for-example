<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\UserAddress;
use Faker\Generator as Faker;
use Illuminate\Support\Arr;

$addressLists = [
    [
        'province_id'   => 77,          // บึงกาฬ
        'amphur_id'     => 999,         // เมืองบึงกาฬ,
        'district_code' => '380101',    // คำนาดี,
        'zipcode'       => '38000'
    ],
    [
        'province_id'   => 1,           // กรุงเทพ
        'amphur_id'     => 1,           // เขตพระนคร,
        'district_code' => '100105',    // ศาลเจ้าพ่อเสือ,
        'zipcode'       => '10200'
    ],
    [
        'province_id'   => 1,           // กรุงเทพ
        'amphur_id'     => 10,          // มีนบุรี,
        'district_code' => '101002',    // แสนแสบ,
        'zipcode'       => '10510'
    ],
    [
        'province_id'   => 4,           // ปทุมธานี
        'amphur_id'     => 66,          // เมืองปทุมธานี,
        'district_code' => '130101',    // บางปรอก,
        'zipcode'       => '12000'
    ],
    [
        'province_id'   => 66,           // ภูเก็ต
        'amphur_id'     => 880,          // เมืองภูเก็ต,
        'district_code' => '830101',     // ตลาดใหญ่,
        'zipcode'       => '83000'
    ]
];

$factory->define(UserAddress::class, function (Faker $faker) use ($addressLists) {

    $address = Arr::random($addressLists);
    return [
        'name'          => $faker->name,
        'tel'           => $faker->randomNumber,
        'address'       => $faker->address,
        'province_id'   => $address['province_id'],
        'amphur_id'     => $address['amphur_id'],
        'district_code' => $address['district_code'],
        'zipcode'       => $address['zipcode'],
        'user_code'     => 'SF0002',
        'latitude'      => $faker->latitude,
        'longitude'     => $faker->longitude,
    ];
});
