<?php

namespace App\Models;

use App\Helpers\ScopeQueries;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PointOrder extends Model
{
    use HasFactory;

    const PREFIX = 'PR-';

    protected $fillable = [
        'code',
        'user_code',
        'status',
        'user_remark',
        'admin_remark',
        'total',
        'address_id',
        'title',
        'tel',
        'address',
        'province',
        'amphur',
        'district',
        'province_id',
        'amphur_id',
        'district_id',
        'zipcode',
        'shipping_method_id',
        'shipping_cost',
        'tracking',
        'truck_id',
        'driver_admin',
        'driver_admin_id',
        'driver2_admin',
        'driver2_admin_id',

        'approved_at',
        'packed_at',
        'shipped_at',
        'printed_at',
        'cancelled_at',
        'confirmed_at',
        'succeed_at',

        'created_admin',
        'updated_admin',
        'approved_admin',
        'packed_admin',
        'shipped_admin',
        'printed_admin',
        'cancelled_admin',
        'confirmed_admin',
        'succeed_admin',

        'created_admin_id',
        'updated_admin_id',
        'approved_admin_id',
        'packed_admin_id',
        'shipped_admin_id',
        'printed_admin_id',
        'cancelled_admin_id',
        'confirmed_admin_id',
        'succeed_admin_id',
    ];

    protected $appends = ['no', 'status_text', 'full_address', 'counter'];

    public static function statusList()
    {
        $array = [
            'cancel' => __('messages.point_orders.status.cancel'),
            'waiting' => __('messages.point_orders.status.waiting'),
            'approved' => __('messages.point_orders.status.approved'),
            'packed' => __('messages.point_orders.status.packed'),
            'shipped' => __('messages.point_orders.status.shipped'),
            'confirmed' => __('messages.point_orders.status.confirmed'),
            'success' => __('messages.point_orders.status.success'),
        ];
        return $array;
    }

    public static function timeFormatList()
    {
        $array = [
            'created_at' => __('messages.point_orders.time_format.created_at'),
            'updated_at' => __('messages.point_orders.time_format.updated_at'),
            'approved_at' => __('messages.point_orders.time_format.approved_at'),
            'printed_at' => __('messages.point_orders.time_format.printed_at'),
            'packed_at' => __('messages.point_orders.time_format.packed_at'),
            'shipped_at' => __('messages.point_orders.time_format.shipped_at'),
            'confirmed_at' => __('messages.point_orders.time_format.confirmed_at'),
        ];
        return $array;
    }

    /**
     * Get the name for enum columns
     *
     * @return string
     */
    public function getNoAttribute()
    {
        return ($this->id) ? $this->code : null;
    }

    public function getCounterAttribute()
    {
        return count($this->products) > 0 ? count($this->products) : null;
    }

    public function getStatusTextAttribute()
    {
        return collect(self::statusList())->get($this->status);
    }

    public function getFullAddressAttribute()
    {
        return $this->address
            . ' ต.' . $this->district
            . ' อ.' . $this->amphur
            . ' จ.' . $this->province
            . ' ' . $this->zipcode;
    }
    /*
    relation
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_code', 'code');
    }
    public function shippingMethod()
    {
        return $this->belongsTo('App\Models\ThaiShippingMethod', 'shipping_method_id', 'id');
    }
    public function truck()
    {
        return $this->belongsTo('App\Models\Truck', 'truck_id', 'id');
    }
    public function address()
    {
        return $this->belongsTo('App\Models\UserAddress', 'address_id', 'id');
    }
    public function point()
    {
        return $this->morphMany('App\Models\Point', 'pointable');
    }
    public function products()
    {
        return $this->belongsToMany('App\Models\Product', 'point_order_items', 'order_id', 'product_id')->withPivot('amount', 'total', 'point');
    }
    public function credit()
    {
        return $this->morphOne('App\Models\Credit', 'creditable');
    }
    public function credits()
    {
        return $this->morphMany('App\Models\Credit', 'creditable');
    }
    public function emails()
    {
        return $this->morphMany('App\Models\EmailQueue', 'mailable');
    }
    public function sms()
    {
        return $this->morphMany('App\Models\SmsQueue', 'smsable');
    }
    public function fcm()
    {
        return $this->morphMany('App\Models\FcmQueue', 'fcmable');
    }


    // scope queires
    public function scopeSearch($query, $conditions)
    {
        return $query->where(function ($query) use ($conditions) {
            ScopeQueries::scopeUserCodeBySale($query);
            if (@$conditions['status']) {
                $query->whereStatus($conditions['status']);
            }
            if (@$conditions['shipping_method_id']) {
                $query->whereShippingMethodId($conditions['shipping_method_id']);
            }

            if (@$conditions['user_code']) {
                $query->where('user_code', 'like', '%' . $conditions['user_code'] . '%');
            }
            if (@$conditions['code']) {
                $query->where('code', 'like', '%' . $conditions['code'] . '%');
            }
            if (@$conditions['tracking']) {
                $query->where('tracking', 'like', '%' . $conditions['tracking'] . '%');
            }
            if (@$conditions['user_remark']) {
                $query->where('user_remark', 'like', '%' . $conditions['user_remark'] . '%');
            }
            if (@$conditions['admin_remark']) {
                $query->where('admin_remark', 'like', '%' . $conditions['admin_remark'] . '%');
            }

            $time_format = (@$conditions['time_format']) ? $conditions['time_format'] : 'created_at';
            if (@$conditions['date_start'] && @$conditions['date_end']) {
                ScopeQueries::scopeDateRange($query, $time_format, $conditions['date_start'], $conditions['date_end']);
            } else if (@$conditions['date_start']) {
                ScopeQueries::scopeDateStart($query, $time_format, $conditions['date_start']);
            } else if (@$conditions['date_end']) {
                ScopeQueries::scopeDateEnd($query, $time_format, $conditions['date_end']);
            }
        });
    }
}
