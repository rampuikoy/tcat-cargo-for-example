<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackingQueuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packing_queues', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('user_code');
            $table->integer('bill_id');
            $table->string('bill_code');
            $table->integer('tracking');
            $table->enum('status',['prepairing','waiting','packing','shipping','success','prepaired'])->default('waiting');
            $table->enum('type',['waiting', 'prepair'])->default('waiting');
            $table->string('created_admin')->nullable();
            $table->integer('created_admin_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packing_queues');
    }
}
