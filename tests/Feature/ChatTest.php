<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Admin;
use App\Models\Chat;
use Tymon\JWTAuth\Facades\JWTAuth;

class ChatTest extends TestCase
{
    use WithFaker;
    protected $user;
    protected $chat;
    protected $url = '/api/backend/chat';
    /**
     * A basic feature test example.
     *
     * @return void
     */
    protected function authenticate(string $guard = 'admins')
    {
        $this->user = factory(Admin::class)->create();
        $this->chat = factory(Chat::class)->create();
        auth()->shouldUse($guard);
        $token = JWTAuth::fromUser($this->user);
        return 'Bearer ' . $token;
    }

    protected function deleteUser()
    {
        $this->user->forceDelete();
        $this->chat->forceDelete();
    }

    /** @test */
    public function GetAllSuccessTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('GET', "{$this->url}");
        $response->assertStatus(200);
        $this->deleteUser();
    }

    /** @test */
    public function GetAllFailTest()
    {
        $response = $this->json('GET', "{$this->url}");
        $response->assertStatus(401);
    }

    /** @test */
    public function FindByIdSuccessTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('GET', "{$this->url}/{$this->chat->id}");
        $response->assertStatus(200);
        $this->deleteUser();
    }

    /** @test */
    public function FindByIdFailTest()
    {
        $response = $this->json('GET', "{$this->url}/1");
        $response->assertStatus(401);
    }

    /** @test */
    public function GetChatSearchSuccessTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('GET', "{$this->url}/search");
        $response->assertStatus(200);
        $this->deleteUser();
    }

    /** @test */
    public function GetChatSearchFailTest()
    {
        $response = $this->json('GET', "{$this->url}/search");
        $response->assertStatus(401);
    }

    /** @test */
    public function GetChatDropdownSuccessTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('GET', "{$this->url}/dropdown");
        $response->assertStatus(200);
        $this->deleteUser();
    }

    /** @test */
    public function GetChatDropdownFailTest()
    {
        $response = $this->json('GET', "{$this->url}/dropdown");
        $response->assertStatus(401);
    }

    /** @test */
    public function PostChatSuccessTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('POST', "{$this->url}", [
            "user_code" =>  "SF0006",
            "message" => "สวัสดี 1",
            "file" => "",
            "readed" => "0",
            "status" => "active",
            "created_type" => "admin"
        ]);
        $response->assertStatus(200);
        Chat::where('user_code', 'SF0006')->forceDelete();
        $this->deleteUser();
    }

    /** @test */
    public function PostChatFailTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('POST', "{$this->url}");
        $response->assertStatus(422);
        $this->deleteUser();
    }

    /** @test */
    public function PostChatFailAuthTest()
    {
        $response = $this->json('POST', "{$this->url}");
        $response->assertStatus(401);
    }

    /** @test */
    public function ReadAllSuccessTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('POST', "{$this->url}", [
            "user_code" =>  "SF0006",
            "message" => "สวัสดี",
            "file" => "",
            "readed" => "0",
            "status" => "active"
        ]);
        $response->assertStatus(200);
        Chat::where('user_code', 'SF0006')->forceDelete();
        $this->deleteUser();
    }

    /** @test */
    public function ReadAllFailTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('POST', "{$this->url}");
        $response->assertStatus(422);
        $this->deleteUser();
    }

    /** @test */
    public function ReadAllFailAuthTest()
    {
        $response = $this->json('POST', "{$this->url}");
        $response->assertStatus(401);
    }
}
