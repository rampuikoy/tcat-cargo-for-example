<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Statement extends Model
{
    use HasFactory;
    protected $connection = 'taobaoconew';
    protected $table = 'bank_report';

    public $selected_banks_id = [6,19,25,26,30,34,38];
    // public $selected_domains_id = [4];

    public $timestamps = false;

	protected $fillable = [
        'referent_id',
        'referent_code',
        'domain',
        'date_modify',
    ];


    public function scopeSelectedBank($query)
    {
    	return $query->whereIn('bank_id', $this->selected_banks_id);
    }
}
