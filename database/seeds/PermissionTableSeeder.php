<?php

use App\Models\Permission;
use App\Models\Role;
use App\Models\Admin;
use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Helpers\MoveSqlData;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Step 1. create permission
        $permissions           = Permission::defaultPermissions();
        Permission::withoutEvents(function () use ($permissions) {
            $insertData = [];
            foreach ($permissions as $permission) {
                $name           = $permission['name'];
                $tag            = $permission['tag'];
                $created_at     = Carbon::now();
                $status         = (array_key_exists('status', $permission)) ? $permission['status'] : 'showing';
                $methods        = (array_key_exists('methods', $permission))  ? $permission['methods'] : [];

                if(count($methods) > 0){
                    foreach($methods as $key => $method){
                        if($key != 'extend'){
                            $ability      = $name . '.' . $key;
                            $resourceName = $this->getResourceName($key);
                        }else {
                            $ability      = $name;
                            $resourceName = $permission['description'];
                        }
                        if($method && $resourceName != ""){
                            array_push($insertData, [
                                'guard_name'    => 'admins',
                                'name'          => $ability,
                                'tag'           => $tag,
                                'description'   => $resourceName,
                                'status'        => $status,
                                'created_at'    => $created_at
                            ]);
                        }
                    }
                }
            }
            return Permission::insert($insertData);
        });

        // Step 2 Create SuperAdmin and assign
        $role = Role::firstOrCreate(['name' => 'SuperAdmin', 'guard_name' => 'admins']);
        $role->syncPermissions(Permission::where('guard_name', 'admins')->get());
        $this->assignRoleAdmin($role);

        // Step 3 Create role form tcatcargo
        $move_role_data     = collect(MoveSqlData::insertRoleTable());
        $message            = $move_role_data->get('message');
        $error              = $move_role_data->get('error');
        if($error){
            $this->command->error($message);
            $this->command->warn('Please insert fresh again');
            return;
        }else{
            $this->command->info($message);
        }

        // Step 4 register role to admin
        $move_has_data     = collect(MoveSqlData::insertModelHasRoleTable());
        $message            = $move_has_data->get('message');
        $error              = $move_has_data->get('error');
        if($error){
            $this->command->error($message);
            $this->command->warn('Please insert fresh again');
            return;
        }else{
            $this->command->info($message);
        }
    }

    private function getResourceName($method)
    {
        switch ($method) {
            case 'index':
                return 'ค้นหา';
            case 'show':
                return 'เรียกดู';
            case 'store':
                return 'สร้าง';
            case 'update':
                return 'แก้ไข';
            case 'destroy':
                return 'ลบ/ยกเลิก';
            case 'approve':
                return 'อนุมัติ';
            default:
                return;
        }
    }

    private function assignRoleAdmin($role)
    {
        $admin = Admin::where('role', 'SuperAdmin')->first();
        $admin->update(['role' => $role->name]);
        $admin->assignRole($role->name);
    }
}
