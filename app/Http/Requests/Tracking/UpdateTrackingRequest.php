<?php

namespace App\Http\Requests\Tracking;

use App\Http\Requests\FormRequest;

class UpdateTrackingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.tracking.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'bill_id'      => 'exists:bills,id',
            'warehouse_id' => 'required|integer|exists:warehouses,id',
            'china_out'    => 'nullable|date_format:Y-m-d',
            'china_in'     => 'nullable|date_format:Y-m-d',
            'thai_in'      => 'required_if:warehouse_id,2,3,4,5,6,7,8,9,10,11,12,13|nullable|date_format:Y-m-d',
            'thai_out'     => 'nullable|date_format:Y-m-d',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'code'              => __('form.tracking.code'),
            'china_out'         => __('form.tracking.china_out'),
            'china_in'          => __('form.tracking.china_in'),
            'thai_in'           => __('form.tracking.thai_in'),
            'thai_out'          => __('form.tracking.thai_out')
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
