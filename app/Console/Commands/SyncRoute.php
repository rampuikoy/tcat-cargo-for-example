<?php

namespace App\Console\Commands;

use App\Models\Route;
use Carbon\Carbon;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Route as RouteList;

class SyncRoute extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:route';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert route to database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            //code...
            if ($this->confirm('Do you want update route ?')) {
                Route::truncate();
                $createData = $this->getRouteList();
                if (count($createData) > 0) {
                    Route::insert($createData);
                    $this->info('Insert data successfully.');
                } else {
                    $this->error('Empty route list');
                }
            }
        } catch (\Exception $th) {
            //throw $th;
            $this->error('Can not insert data!', $th);
        }
    }

    private function getRouteList()
    {
        return collect(RouteList::getRoutes())
            ->filter(function ($route) {
                return strpos($route->uri(), 'api') !== false;
            })->map(function ($route) {
            return [
                'guard_name'    => strpos($route->getName(), 'backend.') !== false ? 'admins' : 'users',
                'ability'       => $route->getName(),
                'method'        => implode('|', $route->methods()),
                'action'        => ltrim($route->getActionName(), '\\'),
                'created_at'    => Carbon::now()->format('Y-m-d H:i:s'),
            ];
        })->toArray();
    }
}
