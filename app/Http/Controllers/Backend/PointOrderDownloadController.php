<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Interfaces\PointOrderInterface;
use App\Interfaces\ThaiShippingMethodInterface;
use Barryvdh\DomPDF\Facade as PDF;

class PointOrderDownloadController extends Controller
{
    protected $pointOrderInterface;

    public function __construct(PointOrderInterface $pointOrderInterface, ThaiShippingMethodInterface $thaiShippingMethodInterface)
    {
        $this->pointOrderInterface = $pointOrderInterface;
        $this->thaiShippingMethodInterface = $thaiShippingMethodInterface;
    }

    public function getDataByIds($data)
    {
        $droppoint = !empty($data['thai_shipping_method_id']) ? $this->thaiShippingMethodInterface->findById($data['thai_shipping_method_id']) : null;
        $ids = explode('-', $data['bill']);
        $bills = $this->pointOrderInterface->fetchListByIds($ids);
        $sendDate = !empty($data['thai_shipping_method_id']) ?  ['bills' => $bills->results, 'droppoint' => $droppoint->results] : ['bills' => $bills->results];
        return $sendDate;
    }

    public function getDownloadDroppoint(Request $request)
    {
        $data = $request->only([
            "thai_shipping_method_id",
            "bill",
        ]);
        $setData = $this->getDataByIds($data);
        $pdf = PDF::loadView('admin.point-order.download.bill-shipping-droppoint', $setData);
        return $pdf->stream();
    }

    public function getDownloadOther(Request $request)
    {
        $data = $request->only([
            "thai_shipping_method_id",
            "bill",
        ]);
        $setData = $this->getDataByIds($data);
        $pdf = PDF::loadView('admin.point-order.download.bill-shipping-other', $setData);
        return $pdf->stream();
    }

    public function getDownloadDriver(Request $request)
    {
        $data = $request->only([
            "thai_shipping_method_id",
            "bill",
        ]);
        $setData = $this->getDataByIds($data);
        $pdf = PDF::setPaper('a4', 'landscape')->loadView('admin.point-order.download.bill-shipping-driver', $setData);
        return $pdf->stream();
    }

    public function getDownloadStickers(Request $request)
    {
        $data = $request->only(["bill"]);
        $setData = $this->getDataByIds($data);
        $pdf = PDF::loadView('admin.point-order.download.bill-multi-sticker', $setData);
        return $pdf->setPaper(array(0, 0, 297.64, 500.53), 'landscape')->stream();
    }

    public function getDownloadCovers(Request $request)
    {
        $data = $request->only(["bill"]);
        $setData = $this->getDataByIds($data);
        $pdf = PDF::loadView('admin.point-order.download.bill-multi-cover', $setData);
        return $pdf->stream();
    }

    public function getDownloadBills(Request $request)
    {
        $data = $request->only(["bill"]);
        $setData = $this->getDataByIds($data);
        $pdf = PDF::loadView('admin.point-order.download.bill-multi-pdf', $setData);
        return $pdf->stream();
    }
}
