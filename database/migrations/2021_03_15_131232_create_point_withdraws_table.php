<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePointWithdrawsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('point_withdraws', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('amount');
            $table->enum('type', ['topup', 'withdraw'])->default('withdraw');
            $table->string('user_code', 10);
            $table->text('remark')->nullable();

            $table->string('created_admin')->nullable();
            $table->string('updated_admin')->nullable();
            $table->string('approved_admin')->nullable();

            $table->integer('created_admin_id')->unsigned()->nullable();
            $table->integer('updated_admin_id')->unsigned()->nullable();
            $table->integer('approved_admin_id')->unsigned()->nullable();

            $table->datetime('approved_at')->nullable();
            $table->tinyInteger('status')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('point_withdraws');
    }
}
