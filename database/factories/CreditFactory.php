<?php

namespace Database\Factories;

use App\Models\Credit;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

class CreditFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Credit::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */

    public function definition()
    {
        $sufcreate = [
            'SF0002',
            'SF0003',
            'SF0004',
            'SF0005',
            'SF0006',
        ];
        // $suftype = [
        //     'topup',
        //     'withdraw',
        // ];
        // $randomInt = random_int(1, 500);
        // $randomFloat = random_int(100, 1000) / 10;
        return [
            'approved_at' => $this->faker->dateTimeBetween('now', '+10 days'),
            'creditable_type' => 'App\\Models\\Topup',
            'creditable_id' => 0,
            'user_code' => Arr::random($sufcreate),
            'amount' => 1000,
            'before' => 0,
            'after' => 1000,
            'type' => 'topup',
            'created_admin' => 'admin',
            'updated_admin' => 'admin',
            'approved_admin' => 'admin',
            'created_admin_id' => 1,
            'updated_admin_id' => 1,
            'approved_admin_id' => 1,
        ];
    }
}
