<?php

namespace App\Repositories;

use App\Helpers\Util;
use App\Interfaces\TopupInterface;
use App\Models\BankAccount;
use App\Models\Bill;
use App\Models\Topup;
use App\Models\TopupAutopay;
use App\Models\Trip;
use App\Models\User;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use App\Traits\UploadTrait;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class TopupRepository implements TopupInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, UploadTrait, CoreResponseTrait;

    const MINUTE_CACHE              = 1;
    const TABLE_RELETIONS_BANK      = ['systemBankAccount'];
    const TABLE_RELETIONS_CREDIT    = ['credit'];
    const TABLE_RELETIONS_FIND      = ['systemBankAccount', 'credits', 'botMatching'];

    public function __construct(Topup $topup, User $user, BankAccount $bankAccount, Bill $bill, Trip $trip, TopupAutopay $topupAutopay)
    {
        $this->topup        = $topup;
        $this->user         = $user;
        $this->bill         = $bill;
        $this->trip         = $trip;
        $this->bankAccount  = $bankAccount;
        $this->topupAutopay = $topupAutopay;
    }

    public function all(array $columns = ['*'], $sorting = TopupInterface::DEFAULT_SORTING, $ascending = TopupInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $topups     = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
            return $this->topup::select($columns)
                ->with(self::TABLE_RELETIONS_BANK)
                ->orderBy($sorting, $ascending)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.topup')]), $topups);
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = TopupInterface::DEFAULT_LIMIT, $sorting = TopupInterface::DEFAULT_SORTING, $ascending = TopupInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $topups     = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
            return $this->topup::select($columns)
                ->with(self::TABLE_RELETIONS_BANK)
                ->search($conditions)
                ->orderBy($sorting, $ascending)
                ->skip($offset)
                ->limit($limit)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.topup')]), $topups);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = TopupInterface::DEFAULT_LIMIT, $sorting = TopupInterface::DEFAULT_SORTING, $ascending = TopupInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $topups     = Cache::tags([$this->getClassPrefix(), $this->getPaginationWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
            $model = $this->topup::select($columns)
                ->with(self::TABLE_RELETIONS_BANK)
                ->search($conditions)
                ->orderBy($sorting, $ascending);
            if ($page == 1) {
                return $model->paginate($limit);
            } else {
                return $model->skip($limit * ($page - 1))->paginate($limit);
            }
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.topup')]), $topups);
    }

    public function findById($id, array $columns = ['*'])
    {
        $topup = $this->findByIdMethod($id, $columns);

        if (!$topup) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.topup'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.topup')]), $topup);
    }

    public function findByUserCode($userCode, array $columns = ['*'])
    {
        $topup = $this->findByUserCodeMethod($userCode, $columns);
        if ($topup->isEmpty()) {
            return $this->coreResponse(404, __('messages.no_data_with_user_code', ['data' => __('messages.topup'), 'user_code' => $userCode]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.topup')]), $topup);
    }

    public function dropdownList()
    {
        $key        = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $dropdown   = Cache::remember($key, $time, function () {
            return [
                'date_type'     => $this->topup::dateTypeList(),
                'method'        => $this->topup::methodList(),
                'status'        => $this->topup::statusList(),
                'condition'     => $this->topup::conditionList(),
            ];
        });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function store(array $data)
    {
        $topup = $this->storeMethod($data);
        if (@$data['bill']) {
            collect($data['bill'])
                ->where('select', true)
                ->map(function ($bill) use ($topup) {
                    $payment = app('App\Repositories\PaymentRepository')->findByIdMethod($bill['id']);
                    $payment->topup_id = $topup->id;
                    $payment->save();
                    return $payment;
                });
        }
        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.topup')]),  $topup);
    }

    public function updateById($id, array $data)
    {
        $topup      = $this->findByidMethod($id);

        if (!$topup) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.topup'), 'id' => $id]));
        }

        $topup->update($data);
        $this->clearCacheTags();
        $this->clearCacheTagById($id);
        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.topup')]), $topup);
    }

    public function approveById($id)
    {
        $topup      = $this->findByidMethod($id);

        if (!$topup) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.topup'), 'id' => $id]));
        }

        if ($topup->status != 'waiting') {
            return $this->coreResponse(404, __('messages.no_data_with_credit', ['data' => __('messages.status_credit'), 'credit' => $topup->status]));
        }

        //check image file
        if ($topup->method == 'transfer' && !isset($topup->file)) {
            return $this->coreResponse(404, __('messages.no_data_with_image', ['data' => __('messages.topup_not_image')]));
        }

        // stranr transction
        DB::beginTransaction();
        try {
            $auth           = auth()->user();
            $timestamp      = date('Y-m-d H:i:s');
            $system_bank    = $topup->systemBankAccount;

            $topup->update([
                'status'            => 'approved',
                'approved_at'       => $timestamp,
                'approved_admin'    => $auth->username,
                'approved_admin_id' => $auth->id,
                'system_bank'       => @$system_bank->bank,
                'system_account'    => str_replace('-', '', filter_var(@$system_bank->account, FILTER_SANITIZE_NUMBER_INT)),
            ]);

            $remain_credit      = app('App\Repositories\UserRepository')->findByCodeMethod($topup->user_code);
            $current_credit     = $remain_credit->total ?? 0;
            $topup->credit()->create([
                'type'              => 'topup',
                'user_code'         => $topup->user_code,
                'amount'            => $topup->amount,
                'before'            => $current_credit,
                'after'             => ($current_credit + $topup->amount),
                'approved_at'       => $timestamp,
                'approved_admin'    => $auth->username,
                'approved_admin_id' => $auth->id,
            ]);

            app('App\Repositories\CreditRepository')->clearCacheTagByUserCode($topup->user_code);
            app('App\Repositories\UserRepository')->clearCacheTagByUserCode($topup->user_code);
            // toup payment bill
            if ($topup->id) {
                $bills = app('App\Repositories\PaymentRepository')->findPaymentListMethod($topup->user_code, 'approve');
               collect($bills->toArray())
                    ->map(function ($value) {

                        // check typ bill
                        if ($value['tracking_bills'] || $value['trips'] !== null) {

                            $bill_tracking = collect($value['tracking_bills']);
                            $bill_trip     = collect($value['trips']);
                            $data          = $bill_tracking->merge($bill_trip);

                            $piad =  collect($data->all())
                                ->map(function ($bill) {
                                    $payable_type    = $bill['pivot']['payable_type'];
                                    $payable_id      = $bill['pivot']['payable_id'];
                                    $payment_id      = $bill['pivot']['payment_id'];
                                    if ($payable_type == 'App\\Models\\Bill') {
                                        $payment =  $this->paymentApprove($payable_id, $payment_id, 'tracking');
                                    } else if ($payable_type == 'App\\Models\\Trip') {
                                        $payment =  $this->paymentApprove($payable_id, $payment_id, 'trip');
                                    }
                                    return  $payment;
                                });
                        }
                        return  $piad;
                    });
            }

            $this->clearCacheTags();
            $this->clearCacheTagById($id);

            DB::commit();
            return $this->coreResponse(200, __('messages.approve_data', ['data' => __('messages.topup')]), $topup);
        } catch (\Throwable $th) {
            // rollback insert sql
            DB::rollBack();
            return $this->coreResponse(500, __('messages.not_approve_data', ['data' => __('messages.topup')]));
        }
    }

    public function cancelById($id, array $data)
    {
        $topup      = $this->findByIdMethod($id);

        if (!$topup) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.topup'), 'id' => $id]));
        }

        if ($topup->status != 'waiting') {
            return $this->coreResponse(500, __('messages.no_data_with_credit', ['data' => __('messages.status_credit'), 'credit' => $topup->status]));
        }

        // stranr transction
        DB::beginTransaction();
        try {
            $payment = app('App\Repositories\PaymentRepository')->findByTopuUpIdMethod($id);
            if ($payment) {
                collect($payment)
                    ->map(function ($pay) {
                        $payment = app('App\Repositories\PaymentRepository')->findByIdMethod($pay['id']);
                        $payment->topup_id = null;
                        $payment->save();
                        return $payment;
                    });
            }

            $auth           = auth()->user();
            $timestamp      = date('Y-m-d H:i:s');
            $topup->update([
                'status'            => 'cancel',
                'user_remark'       => Util::concatField($topup->user_remark, $data['remark']),
                'approved_admin'    => $auth->username,
                'approved_admin_id' => $auth->id,
                'approved_at'       => $timestamp,
            ]);

            $topup->credits->each(function ($credit) use ($topup, $auth, $timestamp) {
                $remain_credit      = app('App\Repositories\UserRepository')->findByCodeMethod($topup->user_code);
                $current_credit     = $remain_credit->total ?? 0;
                $topup->credit()->create([
                    'type'              => 'withdraw',
                    'user_code'         => $credit->user_code,
                    'amount'            => $credit->amount,
                    'before'            => $current_credit,
                    'after'             => $current_credit - $credit->amount,
                    'approved_admin'    => $auth->username,
                    'approved_admin_id' => $auth->id,
                    'approved_at'       => $timestamp,
                ]);
            });

            $this->clearCacheTags();
            $this->clearCacheTagById($id);

            DB::commit();
            return $this->coreResponse(200, __('messages.cancel_data', ['data' => __('messages.topup')]), $topup);
        } catch (\Throwable $th) {
            // rollback insert sql
            DB::rollBack();
            return $this->coreResponse(500, __('messages.not_cancel_data', ['data' => __('messages.topup')]));
        }
    }

    public function getCheckDuplicate(array $conditions, array $columns = ['*'])
    {

        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $topups     = Cache::tags([$this->getClassPrefix(), $this->getDuplicateWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns) {
            return $this->topup::select($columns)
                ->searchDuplicate($conditions)
                ->with(self::TABLE_RELETIONS_BANK)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.topup_duplicate')]), $topups);
    }

    public function optionCreate($code)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);
        $topup      = $this->findByUserCodeMethod($code);

        if (!$topup) {
            return $this->coreResponse(404, __('messages.no_data_with_code', ['data' => __('messages.topup'), 'id' => $code]));
        }

        $topups     = Cache::tags([$this->getClassPrefix(), $this->getOptionCreateWithClassPrefix()])->remember($key, $time, function () use ($code) {
            $banks      = $this->bankAccount::whereNull('user_code')->where('status', 'active')->get();
            $methods    = $this->topup::methodList();
            $recent     = $this->topup::whereUserCode($code)
                ->with(self::TABLE_RELETIONS_BANK)
                ->orderBy('id', 'DESC')
                ->limit(5)
                ->get();

            return [
                'methods'   => $methods,
                'recent'    => $recent,
                'banks'     => $banks,
            ];
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.topup')]), $topups);
    }

    public function optionEdit($id)
    {
        $key        = $this->getCacheKey('OPTION', $id);

        $time       = $this->getTime(self::MINUTE_CACHE);
        $topup      = $this->findByIdMethod($id);

        if (!$topup) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.topup'), 'id' => $id]));
        }

        $topups     = Cache::tags([$this->getOptionEditWithClassPrefix()])->remember($key, $time, function () use ($topup) {
            $methods            = $this->topup::methodList();
            $reasons            = $this->topup::cancelList();
            $auto_pay           = $this->topupAutopay::whereUserCode($topup->user_code)->whereStatus('waiting')->get();

            $topup_duplicate    = collect($topup)
                ->only(['date', 'time', 'amount', 'method', 'system_bank_account_id'])
                ->map(function ($value) {
                    return $value;
                });

            $duplicate          = $this->topup::with(self::TABLE_RELETIONS_BANK)->searchDuplicate($topup_duplicate)->where('id', '!=', $topup->id)->get();

            return [
                'methods'   => $methods,
                'reasons'   => $reasons,
                'auto_pay'  => $auto_pay,
                'duplicate' => $duplicate,
            ];
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.topup')]), $topups);
    }

    public function softDeleteById($id)
    {
        $topup  = $this->findByIdMethod($id);

        if (!$topup) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.topup'), 'id' => $id]));
        }
        if ($topup->status != 'waiting') {
            return $this->coreResponse(500, __('messages.no_data_with_credit', ['data' => __('messages.status_credit'), 'credit' => $topup->status]));
        }

        $topup->delete();
        $this->clearCacheTags();
        $this->clearCacheTagById($id);

        return $this->coreResponse(200, __('messages.delete_data', ['data' => __('messages.topup')]), $topup);
    }

    private function findByIdMethod($id, array $columns = ['*'])
    {
        $key        = $this->getCacheKey(null, $id);
        $time       = $this->getTime(self::MINUTE_CACHE);

        $topup      = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($id)
        ])->remember($key, $time, function () use ($id, $columns) {
            return $this->topup::select($columns)
                ->with(self::TABLE_RELETIONS_FIND)
                ->whereId($id)
                ->first();
        });

        return $topup;
    }

    private function findByUserCodeMethod($userCode, $columns = ['*'])
    {
        $key        = $this->getCacheKey(null,  $userCode);
        $time       = $this->getTime(self::MINUTE_CACHE);

        $topup      = Cache::tags([
            $this->getClassPrefix(),
            $this->getUserCodePrefix($userCode),
        ])->remember($key, $time, function () use ($userCode, $columns) {
            return $this->topup::select($columns)
                ->with(self::TABLE_RELETIONS_BANK)
                ->whereUserCode($userCode)
                ->get();
        });

        return $topup;
    }

    private function storeFile($file)
    {
        if (!$file) {
            return null;
        }
        return $this->uploadBase64($file, $this->topup::$imageFolder, '', $this->topup::$imageWidth, $this->topup::$imageHeight);
    }

    private function processTopupAutopay($bills, $topup)
    {
        collect($bills)->each(function ($value) use ($topup) {
            if ($value) {
                return;
            }
            if (strpos($value, $this->bill::PREFIX) !== false) {
                $payable_type = 'App\Models\Bill';
                $trimmed = ltrim($value, $this->bill::PREFIX . '0');
            } elseif (strpos($value, $this->trip::PREFIX) !== false) {
                $payable_type = 'App\Models\Trip';
                $trimmed = ltrim($value, $this->trip::PREFIX . '0');
            }

            $this->topupAutopay::firstOrCreate([
                'payable_type' => $payable_type,
                'payable_id' => $trimmed,
                'user_code' => $topup->user_code,
                'topup_id' => $topup->id,
            ]);
        });
    }

    public function storeMethod($data)
    {
        if (@$data['file']) {
            $data['file']   = $this->storeFile($data['file']);
        }

        $topup      = $this->topup::create($data);
        if ($topup->id && !empty($data['bill'])) {
            $this->processTopupAutopay($data['bill'], $topup);
        }
        $this->clearCacheTags();

        return  $topup;
    }

    private function paymentApprove($bill_id, $payment_id, $type)
    {
        $data = [
            'bill_id' => $bill_id,
            'credit_withdraw' => true,
            'type' => $type,
            'type_pay' => 'credit',
        ];
        $payment =   app('App\Repositories\PaymentRepository')->approve($payment_id, $data, 'topup');
        return $payment;
    }

    private function clearCacheTags()
    {
        Cache::tags([
            $this->getDuplicateWithClassPrefix(),
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix(),
        ])->flush();
    }

    private function clearCacheTagById($id)
    {
        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();
    }


}
