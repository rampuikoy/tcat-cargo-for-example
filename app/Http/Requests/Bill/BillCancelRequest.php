<?php

namespace App\Http\Requests\Bill;

use App\Http\Requests\FormRequest;

class BillCancelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.bill.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'remark' => 'required'
        ];
    }
}
