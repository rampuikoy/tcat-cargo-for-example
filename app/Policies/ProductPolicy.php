<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.product.index');
    }

    public function show(Admin $admin)
    {
        return $admin->hasAnyPermission(['backend.product.show', 'backend.product.update']);
    }

    public function store(Admin $admin)
    {
        return $admin->can('backend.product.store');
    }

    public function update(Admin $admin)
    {
        return $admin->can('backend.product.update');
    }

    public function destroy(Admin $admin)
    {
        return $admin->can('backend.product.destroy');
    }
}
