<?php

namespace App\Repositories;

use App\Interfaces\UserAddressInterface;
use App\Models\UserAddress;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use Illuminate\Support\Facades\Cache;

class UserAddressRepository implements UserAddressInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const MINUTE_CACHE          = 1;

    public function __construct(UserAddress $userAddress)
    {
        $this->userAddress      = $userAddress;
    }

    public function all(array $columns = ['*'], $sorting = UserAddressInterface::DEFAULT_SORTING, $ascending = UserAddressInterface::DEFAULT_ASCENDING)
    {
        $key = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time = $this->getTime(self::MINUTE_CACHE);

        $addresses  = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
            return $this->userAddress::select($columns)->orderBy($sorting, $ascending)->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.user_address')]), $addresses);
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = UserAddressInterface::DEFAULT_LIMIT, $sorting = UserAddressInterface::DEFAULT_SORTING, $ascending = UserAddressInterface::DEFAULT_ASCENDING)
    {
        $key = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time = $this->getTime(self::MINUTE_CACHE);

        $addresses  = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
            return $this->userAddress::with('user', 'province', 'amphur', 'district')
                ->select($columns)
                ->orderBy($sorting, $ascending)
                ->skip($offset)
                ->limit($limit)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.user_address')]), $addresses);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = UserAddressInterface::DEFAULT_LIMIT, $sorting = UserAddressInterface::DEFAULT_SORTING, $ascending = UserAddressInterface::DEFAULT_ASCENDING)
    {
        $key = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time = $this->getTime(self::MINUTE_CACHE);

        $addresses  = Cache::tags([
            $this->getClassPrefix(),
            $this->getPaginationWithClassPrefix(),
        ])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
            $model = $this->userAddress::with('user', 'province', 'amphur', 'district')
                ->select($columns)
                ->search($conditions)
                ->orderBy($sorting, $ascending);

            if ($page == 1) {
                return $model->paginate($limit);
            } else {
                return $model->skip($limit * ($page - 1))->paginate($limit);
            }
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.user_address')]), $addresses);
    }

    public function findById($id, array $columns = ['*'])
    {
        $address    =  $this->findByIdMethod($id);

        if (!$address) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.user_address'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.user_address')]), $address);
    }

    public function dropdownList()
    {
        $key = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time = $this->getTime(self::MINUTE_CACHE);

        $dropdown   = Cache::remember($key, $time, function () {
            return ['status' => $this->userAddress::statusList()];
        });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function store(array $data)
    {
        $address    = $this->userAddress::create($data);
        Cache::tags([$this->getIdWithClassPrefix($address->id)])->flush();
        Cache::tags([$this->getUserCodePrefix($address->user_code)])->flush();
        $this->clearCacheTags();
        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.user_address')]), $address->load('user', 'province', 'amphur', 'district'));
    }

    public function updateById($id, array $data)
    {
        $address    = $this->findByIdMethod($id);
        if (!$address) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.user_address'), 'id' => $id]));
        }
        $address->update($data);
        Cache::tags([$this->getIdWithClassPrefix($address->id)])->flush();
        Cache::tags([$this->getUserCodePrefix($address->user_code)])->flush();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.user_address')]), $address);
    }

    public function findByUserCodeExtend($userCode, array $columns = ['*'])
    {
        $key = $this->getCacheKey(null, $userCode);
        $time = $this->getTime(self::MINUTE_CACHE);

        $address = Cache::tags([
            $this->getClassPrefix(),
            $this->getUserCodePrefix($userCode),
        ])->remember($key, $time, function () use ($userCode, $columns) {
            return $this->userAddress::select($columns)
                ->with('province', 'amphur', 'district')
                ->whereUserCode($userCode)
                ->whereStatus('active')
                ->get();
        });
        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.user_address')]), $address);
    }

    public function softDeleteById($id)
    {
        $address = $this->findByIdMethod($id);
        if (!$address) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.user_address'), 'id' => $id]));
        }
        $address->delete();
        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();
        $this->clearCacheTags();
        return $this->coreResponse(200, __('messages.delete_data', ['data' => __('messages.user_address')]), $address);
    }

    public function findByIdMethod($id, array $columns = ['*'])
    {
        $key = $this->getCacheKey(null, $id);
        $time = $this->getTime(self::MINUTE_CACHE);

        $address    = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($id),
        ])->remember($key, $time, function () use ($id, $columns) {
            return $this->userAddress::with('user', 'province', 'amphur', 'district')
                ->select($columns)
                ->whereId($id)
                ->first();
        });
        return $address;
    }

    private function clearCacheTags()
    {
        Cache::tags([
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix()
        ])->flush();
    }
}
