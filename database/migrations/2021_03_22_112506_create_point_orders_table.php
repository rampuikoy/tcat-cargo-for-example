<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePointOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('point_orders', function (Blueprint $table) {
            $table->id();
            $table->enum('status', ['waiting', 'approved', 'packed', 'shipped', 'confirmed', 'success', 'cancel'])->default('waiting');

            $table->string('user_code', 10)->nullable();
            $table->string('code', 10)->nullable();
            $table->integer('total')->unsigned();
            $table->integer('address_id')->unsigned()->nullable();
            $table->integer('province_id')->nullable();
            $table->integer('amphur_id')->nullable();
            $table->integer('district_id')->nullable();

            $table->string('title', 100);
            $table->string('tel', 10);
            $table->string('address', 300);
            $table->string('province', 100);
            $table->string('amphur', 100);
            $table->string('district', 100);
            $table->string('zipcode', 5);

            $table->integer('shipping_method_id')->unsigned()->nullable();
            $table->integer('shipping_cost')->unsigned()->default(0);
            $table->string('tracking', 200)->nullable();

            $table->text('user_remark')->nullable();
            $table->text('admin_remark')->nullable();

            $table->integer('truck_id')->unsigned()->nullable();
            $table->integer('driver_admin_id')->unsigned()->nullable();
            $table->integer('driver2_admin_id')->unsigned()->nullable();

            $table->string('driver_admin', 20)->nullable();
            $table->string('driver2_admin', 20)->nullable();

            $table->timestamps();
            $table->timestamp('approved_at')->nullable();
            $table->timestamp('packed_at')->nullable();
            $table->timestamp('shipped_at')->nullable();
            $table->timestamp('printed_at')->nullable();
            $table->timestamp('cancelled_at')->nullable();
            $table->timestamp('confirmed_at')->nullable();
            $table->timestamp('succeed_at')->nullable();

            $table->string('created_admin', 20)->nullable();
            $table->string('updated_admin', 20)->nullable();
            $table->string('approved_admin', 20)->nullable();
            $table->string('packed_admin', 20)->nullable();
            $table->string('shipped_admin', 20)->nullable();
            $table->string('printed_admin', 20)->nullable();
            $table->string('cancelled_admin', 20)->nullable();
            $table->string('confirmed_admin', 20)->nullable();
            $table->string('succeed_admin', 20)->nullable();

            $table->integer('created_admin_id')->unsigned()->nullable();
            $table->integer('updated_admin_id')->unsigned()->nullable();
            $table->integer('approved_admin_id')->unsigned()->nullable();
            $table->integer('packed_admin_id')->unsigned()->nullable();
            $table->integer('shipped_admin_id')->unsigned()->nullable();
            $table->integer('printed_admin_id')->unsigned()->nullable();
            $table->integer('cancelled_admin_id')->unsigned()->nullable();
            $table->integer('confirmed_admin_id')->unsigned()->nullable();
            $table->integer('succeed_admin_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('point_orders');
    }
}
