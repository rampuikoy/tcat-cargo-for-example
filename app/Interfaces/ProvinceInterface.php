<?php

namespace App\Interfaces;

interface ProvinceInterface
{
    const DEFAULT_LIMIT = 25;
    const DEFAULT_SORTING = 'title_th';
    const DEFAULT_ASCENDING = 'asc';

    /**
     * fetch all province
     *
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function all(array $columns = ['*'], $sorting = ProvinceInterface::DEFAULT_SORTING, $ascending = ProvinceInterface::DEFAULT_ASCENDING);
    /**
     * find province by id
     *
     * @param $id
     * @param array $columns
     *
     * @return object|null
     */
    public function findById($id, array $columns = ['*']);

    /**
     * get province dropdown list
     *
     * @return array
     */
}
