<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClaimItems extends Model
{
    use HasFactory;

    protected $table = 'claim_items';

    protected $fillable = [
        'claim_id',
        'tracking',
        'amount',
        'price',
        'total',
        'remark',
        'type',
    ];

    public static function statusList()
    {
        $array = [
            'lose'      => __('messages.claim_items.status.lose'),
            'lost_some' => __('messages.claim_items.status.lost_some'),
            'broken'    => __('messages.claim_items.status.broken'),
        ];
        return $array;
    }

    protected $appends = ['current_type'];

    public function getCurrentTypeAttribute()
    {
        return collect(self::statusList())->get($this->type);
    }

    public function claim()
    {
        return $this->belongsTo('App\Models\Claims', 'claim_id');
    }
}
