<?php

namespace App\Http\Controllers\Backend;

use App\Models\Trip;
use Illuminate\Http\Request;
use App\Interfaces\TripInterface;
use App\Interfaces\UserInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\Trip\StoreTripRequest;
use App\Http\Requests\Trip\CancelTripRequest;
use App\Http\Requests\Trip\UpdateTripRequest;
use Barryvdh\DomPDF\Facade as PDF;

class TripController extends Controller
{
    protected $tripInterface, $userInterface;

    public function __construct(TripInterface $tripInterface, UserInterface $userInterface)
    {
        $this->tripInterface            = $tripInterface;
        $this->userInterface            = $userInterface;
    }

    public function index(Request $request)
    {
        $this->authorize('index', Trip::class);
        $columns = $request->input('columns', ['*']);
        $trip = $this->tripInterface->all($columns);
        return response()->json($trip, $trip->code);
    }

    public function show(Request $request, $id)
    {
        $this->authorize('show', Trip::class);
        $columns = $request->input('columns', ['*']);
        $trip = $this->tripInterface->findById($id, $columns);
        return response()->json($trip, $trip->code);
    }

    public function search(Request $request)
    {
        $this->authorize('index', Trip::class);
        $conditions = $request->only([
            'user_code',
            'destination',
            'status',
            'trip',
            'bill_no',
            'depart_date',
            'to_date',
            'user_remark',
            'admin_remark',
            'type',
            'condition',
            'bill'
        ]);
        $columns = $request->input('columns', ['*']);
        $limit = $request->input('limit', $this->tripInterface::DEFAULT_LIMIT);
        $sorting = $request->input('order_by', $this->tripInterface::DEFAULT_SORTING);
        $ascending = $request->input('ascending') == 1 ? 'asc' : 'desc';

        if ($request->boolean('pagination')) {
            $page = $request->input('page', 1);
            $trip = $this->tripInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset = $request->input('page', 0);
            $trip = $this->tripInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($trip, $trip->code);
    }

    public function store(StoreTripRequest  $request)
    {
        try {
            $data = $request->only([
                "destination",
                "customer",
                "depart_date",
                "return_date",
                "discount_title",
                "sub_total",
                "total",
                "user_code",
                "trip",
                "discount",
                "orders",
                "withholding",
                "detail",
                "admin_remark",
            ]);

            $trip = $this->tripInterface->store($data);
            return response()->json($trip, $trip->code);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json(['error' => true, 'code' => 500, 'message' => $th->getMessage()], 500);
        }
    }

    public function update(UpdateTripRequest $request, $id)
    {
        $data              = $request->only(['admin_remark', 'detail']);
        $trip              = $this->tripInterface->updateById($id, $data);
        return response()->json($trip, $trip->code);
    }

    public function notifyCreate(Request $request, $id)
    {
        $this->authorize('store', Trip::class);
        $data = $request->only([
            'user_code',
            'payment_type_id',
            'bill_id',
            'type'
        ]);
        $trip              = $this->tripInterface->notifyCreate($id, $data);
        return response()->json($trip, $trip->code);
    }

    public function notifyClose(Request $request, $id)
    {
        $trip              = $this->tripInterface->notifyClose($id);
        return response()->json($trip, $trip->code);
    }
    public function notifyDetail(Request $request, $id)
    {
        $trip              = $this->tripInterface->notifyDetail($id);
        return response()->json($trip, $trip->code);
    }

    public function optionEdit(Request $request, $id)
    {
        $this->authorize('show', Trip::class);
        $columns = $request->input('columns', ['*']);
        $trip = $this->tripInterface->optionEdit($id, $columns);

        if ($trip->results->user_code) {
            $user = $this->userInterface->findByCode($trip->results->user_code);
        }

        $trip->results = collect($trip->results)->merge([
            'credit' => (!$user->error) ? $user->results : [],
        ]);
        return response()->json($trip, $trip->code);
    }

    public function dropdown()
    {
        $dropdown = $this->tripInterface->dropdownList();
        return response()->json($dropdown, $dropdown->code);
    }

    public function postCancel(CancelTripRequest $request, $id)
    {
        $data              = $request->only(['admin_remark']);
        $trip              = $this->tripInterface->postCancel($id, $data);
        return response()->json($trip, $trip->code);
    }

    public function getDownload($id)
    {
        $trip = $this->tripInterface->findById($id);
        $pdf = PDF::loadView('admin.trip.pdf', ['bill' => $trip->results]);
        return $pdf->stream();
    }
}
