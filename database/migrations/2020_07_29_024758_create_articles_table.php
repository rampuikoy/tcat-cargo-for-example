<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->id();
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->enum('type', ['article', 'news', 'promotion']);
            $table->string('title', 191);
            $table->text('content');
            $table->integer('views')->default(0);
            $table->string('file_path', 191)->nullable()->default('default.png');
            $table->dateTime('published_at')->nullable();
            $table->timestamps();
            $table->integer('created_admin_id');
            $table->integer('updated_admin_id')->nullable();
            $table->string('created_admin', 64);
            $table->string('updated_admin', 64)->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
