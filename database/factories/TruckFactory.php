<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Truck;
use Faker\Generator as Faker;

$factory->define(Truck::class, function (Faker $faker) {
    $text =  random_int(1, 50).$faker->stateAbbr.$faker->numberBetween(100, 500);
    return [
        'title' =>  $text,
        'code' =>   $text,
        'remark' => $faker->text($maxNbChars = 100),
        'created_admin' => 'admin',
        'updated_admin' => 'admin',
    ];
});
