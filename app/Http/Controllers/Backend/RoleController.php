<?php

namespace App\Http\Controllers\Backend;
use App\Http\Controllers\Controller;
use App\Http\Requests\Role\StoreRoleRequest;
use App\Http\Requests\Role\UpdateRoleRequest;
use App\Interfaces\RoleInterface;
use Illuminate\Http\Request;
use App\Models\Role;

class RoleController extends Controller
{
    protected $roleInterface;

    public function __construct(RoleInterface $roleInterface)
    {
        $this->roleInterface = $roleInterface;
    }

    public function index(Request $request)
    {
        $this->authorize('index', Role::class);
        $columns    = $request->input('columns', ['*']);
        $roles      = $this->roleInterface->all($columns);
        return response()->json($roles, $roles->code);
    }

    public function show(Request $request, $id)
    {
        $this->authorize('show', Role::class);
        $columns    = $request->input('columns', ['*']);
        $role       = $this->roleInterface->findById($id, $columns);
        return response()->json($role, $role->code);
    }

    public function search(Request $request)
    {
        $this->authorize('index', Role::class);
        $conditions     = $request->only(['id', 'search']);
        $columns        = $request->input('columns', ['*']);
        $limit          = $request->input('limit', $this->roleInterface::DEFAULT_LIMIT);
        $sorting        = $request->input('order_by', $this->roleInterface::DEFAULT_SORTING);
        $ascending      = $request->input('ascending') == 1 ? 'asc' : 'desc';

        if ($request->boolean('pagination')) {
            $page       = $request->input('page', 1);
            $roles      = $this->roleInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset     = $request->input('page', 0);
            $roles      = $this->roleInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($roles, $roles->code);
    }

    public function store(StoreRoleRequest $request)
    {
        $data    = $request->only(['name', 'guard_name', 'permissions', 'remark']);
        $role    = $this->roleInterface->store($data);
        return response()->json($role, $role->code);
    }

    public function update(UpdateRoleRequest $request, $id)
    {
        $data    = $request->only(['name', 'guard_name', 'permissions', 'remark']);
        $role    = $this->roleInterface->updateById($id, $data);
        return response()->json($role, $role->code);
    }

    public function destroy($id)
    {
        $this->authorize('destroy', Role::class);
        $role   = $this->roleInterface->softDeleteById($id);
        return response()->json($role, $role->code);
    }
}
