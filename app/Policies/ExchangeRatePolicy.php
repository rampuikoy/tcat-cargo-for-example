<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class ExchangeRatePolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.exchange-rate.index');
    }
    public function show(Admin $admin)
    {
        return $admin->hasAnyPermission(['backend.exchange-rate.show', 'backend.rate.update']);
    }

    public function store(Admin $admin)
    {
        return $admin->can('backend.exchange-rate.store');
    }
    public function update(Admin $admin)
    {
        return $admin->can('backend.exchange-rate.update');
    }

    public function destroy(Admin $admin)
    {
        return $admin->can('backend.exchange-rate.destroy');
    }
}
