<?php

namespace Database\Seeders;
use App\Models\Topup;
use App\Models\TopupAutopay;
use Illuminate\Database\Seeder;

class TopupAutopayTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $topup = Topup::all();
        $topup->each(function ($topup){
            TopupAutopay::factory()->create([
                    'topup_id'            => $topup->id,
                    'user_code'           => $topup->user_code,
                ]);
        });
    }
}
