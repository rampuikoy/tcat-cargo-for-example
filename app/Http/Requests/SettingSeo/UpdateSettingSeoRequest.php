<?php

namespace App\Http\Requests\SettingSeo;

use App\Http\Requests\FormRequest;

class UpdateSettingSeoRequest extends FormRequest
{
    /**
     * Determine if the user is authoriz ed to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'keyword' => 'required',
            'description' => 'required',
            'code' => 'required',
            'footer' => 'required',
            'point_rate' => 'required|numeric',
        ];
    }
}
