<?php

namespace App\Observers;

use App\Models\Article;
use App\Observers\BaseObserver;
use App\Traits\UploadTrait;

class ArticleObserver extends BaseObserver
{  use UploadTrait;
    /**
     * Handle the article "creating" event.
     *
     * @param  \App\Models\Article  $article
     * @return void
     */
    public function creating(Article $article)
    {
        //
        $this->stampCreated($article);
    }

    /**
     * Handle the article "updating" event.
     *
     * @param  \App\Models\Article  $article
     * @return void
     */
    public function updating(Article $article)
    {
        //
        $this->stampUpdated($article);
    }

    /**
     * Handle the article "updated" event.
     *
     * @param  \App\Models\Article  $article
     * @return void
     */
    public function updated(Article $article)
    {
        if ($article->isDirty('file_path')) {
            $oldFilePath = $article->getOriginal('file_path');
            $this->removeFiles([
                $article->getFileUrlAttribute($oldFilePath),
                $article->getThumbnailUrlAttribute($oldFilePath),
            ]);
        }
    }

    /**
     * Handle the article "force deleted" event.
     *
     * @param  \App\Models\Article  $article
     * @return void
     */
    public function forceDeleted(Article $article)
    {
        //
        $this->removeFiles([
            $article->getFileUrlAttribute(),
            $article->getThumbnailUrlAttribute(),
        ]);
    }
}
