<?php

namespace App\Repositories;

use App\Interfaces\WarehouseChineseInterface;
use App\Models\WarehouseChinese;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use App\Traits\UploadTrait;
use Illuminate\Support\Facades\Cache;

class WarehouseChineseRepository implements WarehouseChineseInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, UploadTrait, CoreResponseTrait;

    const MINUTE_CACHE          = 1;

    public function __construct(WarehouseChinese $warehouse_chinese)
    {
        $this->warehouse_chinese        = $warehouse_chinese;
    }

    public function all(array $columns = ['*'], $sorting = WarehouseChineseInterface::DEFAULT_SORTING, $ascending = WarehouseChineseInterface::DEFAULT_ASCENDING)
    {
        $key = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time = $this->getTime(self::MINUTE_CACHE);
        $warehouse_chineses     = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
            return $this->warehouse_chinese::select($columns)->orderBy($sorting, $ascending)->get();
        });
        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.warehouse_chinese')]), $warehouse_chineses);
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = WarehouseChineseInterface::DEFAULT_LIMIT, $sorting = WarehouseChineseInterface::DEFAULT_SORTING, $ascending = WarehouseChineseInterface::DEFAULT_ASCENDING)
    {
        $key = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time = $this->getTime(self::MINUTE_CACHE);
        $warehouse_chineses     = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
            return $this->warehouse_chinese::select($columns)
                ->search($conditions)
                ->orderBy($sorting, $ascending)
                ->skip($offset)
                ->limit($limit)
                ->get();
        });
        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.warehouse_chinese')]), $warehouse_chineses);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = WarehouseChineseInterface::DEFAULT_LIMIT, $sorting = WarehouseChineseInterface::DEFAULT_SORTING, $ascending = WarehouseChineseInterface::DEFAULT_ASCENDING)
    {
        $key = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time = $this->getTime(self::MINUTE_CACHE);
        $warehouse_chineses     = Cache::tags([
            $this->getClassPrefix(),
            $this->getPaginationWithClassPrefix(),
        ])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
            $model = $this->warehouse_chinese::select($columns)
                ->search($conditions)
                ->orderBy($sorting, $ascending);
            if ($page == 1) {
                return $model->paginate($limit);
            } else {
                return $model->skip($limit * ($page - 1))->paginate($limit);
            }
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.warehouse_chinese')]), $warehouse_chineses);
    }

    public function findById($id, array $columns = ['*'])
    {
        $warehouse_chinese  = $this->findByIdMethod($id, $columns = ['*']);
        if (!$warehouse_chinese) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.warehouse_chinese'), 'id' => $id]));
        }
        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.warehouse_chinese')]), $warehouse_chinese);
    }

    public function dropdownList()
    {
        $key = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time = $this->getTime(self::MINUTE_CACHE);

        $dropdown   = Cache::remember($key, $time, function () {
            return [
                'status' => $this->warehouse_chinese::statusList()
            ];
        });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function store(array $data)
    {
        if (@$data['file']) {
            $data['image']    = $this->storeFile($data['file']);
        }

        $warehouse_chinese  = $this->warehouse_chinese::create($data);
        Cache::tags([$this->getIdWithClassPrefix($warehouse_chinese->id)])->flush();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.warehouse_chinese')]), $warehouse_chinese);
    }

    public function updateById($id, array $data)
    {
        $warehouse_chinese = $this->findByIdMethod($id);

        if (!$warehouse_chinese) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.warehouse_chinese'), 'id' => $id]));
        }

        $data['image']  = (@$data['file']) ? $this->storeFile($data['file']) : $warehouse_chinese->image;

        $warehouse_chinese->update($data);
        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.warehouse_chinese')]), $warehouse_chinese);
    }

    public function softDeleteById($id)
    {
        $warehouse_chinese = $this->findByIdMethod($id);

        if (!$warehouse_chinese) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.warehouse_chinese'), 'id' => $id]));
        }

        $warehouse_chinese->delete();
        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();

        return $this->coreResponse(200, __('messages.delete_data', ['data' => __('messages.warehouse_chinese')]), $warehouse_chinese);
    }

    private function storeFile($file)
    {
        if (!$file) {
            return null;
        }
        return $this->uploadBase64($file, $this->warehouse_chinese::$imageFolder, '', $this->warehouse_chinese::$imageWidth, $this->warehouse_chinese::$imageHeight);
    }

    private function clearCacheTags()
    {
        Cache::tags([
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix()
        ])->flush();
    }

    private function findByIdMethod($id, array $columns = ['*'])
    {
        $key = $this->getCacheKey(null, $id);
        $time = $this->getTime(self::MINUTE_CACHE);
        $warehouse_chinese  = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($id)
        ])->remember($key, $time, function () use ($id, $columns) {
            return $this->warehouse_chinese::select($columns)->whereId($id)->first();
        });
        return $warehouse_chinese;
    }
}
