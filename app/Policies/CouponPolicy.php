<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class CouponPolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.coupon.index');
    }

    public function show(Admin $admin)
    {
        return $admin->hasAnyPermission(['backend.coupon.show', 'backend.coupon.update']);
    }

    public function store(Admin $admin)
    {
        return $admin->can('backend.coupon.store');
    }

    public function update(Admin $admin)
    {
        return $admin->can('backend.coupon.update');
    }

    public function destroy(Admin $admin)
    {
        return $admin->can('backend.coupon.destroy');
    }
}
