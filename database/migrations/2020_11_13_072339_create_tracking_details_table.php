<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrackingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracking_details', function (Blueprint $table) {
            $table->id();
            $table->enum('product_type', ['clothing','shoes', 'bag', 'accessories', 'electronics', 'it' , 'car','home' ,'office','stationery','handyman_tool','pet','toy','furniture' , 'food_and_drug','other', 'cosmetics']);
            $table->text('product_remark')->nullable();
            $table->integer('qty');
            $table->integer('tracking_id')->unsigned();
            $table->string('created_admin')->nullable();
            $table->string('updated_admin')->nullable();
            $table->integer('created_admin_id')->nullable();
            $table->integer('updated_admin_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracking_details');
    }
}
