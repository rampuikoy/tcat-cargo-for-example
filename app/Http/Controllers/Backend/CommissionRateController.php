<?php

namespace App\Http\Controllers\Backend;

use App\Exports\CommissionExport;
use App\Helpers\Util;
use App\Http\Controllers\Controller;
use App\Http\Requests\CommissionRate\ExportCommissionRateRequest;
use App\Http\Requests\CommissionRate\StoreCommissionRateRequest;
use App\Interfaces\AdminInterface;
use App\Interfaces\BillInterface;
use App\Interfaces\CommissionRateInterface;
use App\Interfaces\ProvinceInterface;
use App\Interfaces\RateInterface;
use App\Interfaces\ThaiShippingMethodInterface;
use App\Models\CommissionRate;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Excel as ExcelExcel;

class CommissionRateController extends Controller
{
    protected $commissionInterface, $billInterface, $rateInterface, $adminInterface;

    public function __construct(CommissionRateInterface $commissionInterface, BillInterface $billInterface, RateInterface $rateInterface, AdminInterface $adminInterface, ProvinceInterface $provinceInterface, ThaiShippingMethodInterface $thaiShippingMethodInterface)
    {
        $this->commissionInterface = $commissionInterface;
        $this->billInterface = $billInterface;
        $this->rateInterface = $rateInterface;
        $this->adminInterface = $adminInterface;
        $this->provinceInterface = $provinceInterface;
        $this->thaiShippingMethodInterface = $thaiShippingMethodInterface;
    }

    public function search(Request $request)
    {
        $this->authorize('index', CommissionRate::class);

        $conditions = $request->only([
            'admin_ref_id',
            'time_format',
            'date_start',
            'date_end',
            'status',
            'rate_id',
            'thai_shipping_method_id',
            'shipping_province_id',
            'bill_no',
            'user_code',
            'user_remark',
            'admin_remark',
            'condition',
        ]);

        $columns = $request->input('columns', ['*']);
        $limit = $request->input('limit', $this->billInterface::DEFAULT_LIMIT);
        $sorting = $request->input('order_by', $this->billInterface::DEFAULT_SORTING);
        $ascending = $request->input('ascending') == 1 ? 'asc' : 'desc';

        $page = $request->input('page', 1);
        $bill = $this->billInterface->paginateListCommissionByFields($conditions, $columns, $page, $limit, $sorting, $ascending);

        return response()->json($bill, $bill->code);
    }

    public function dropdown()
    {
        $dropdown = $this->billInterface->dropdownList();
        if (!$dropdown->error) {
            $sales = $this->adminInterface->findManySale(['id', 'name', 'username']);
            $province = $this->provinceInterface->all(['id', 'title_th']);
            $rate = $this->rateInterface->all(['id', 'title']);
            $thai_shipping_method = $this->thaiShippingMethodInterface->all(['id', 'title_th']);
            $dropdown->results = collect($dropdown->results)
                ->merge([
                    'sale' => (!$sales->error) ? $sales->results : [],
                    'province' => (!$province->error) ? $province->results : [],
                    'rate' => (!$rate->error) ? $rate->results : [],
                    'thai_shipping_method' => (!$thai_shipping_method->error) ? $thai_shipping_method->results : [],
                ]);
        }
        return response()->json($dropdown, $dropdown->code);
    }

    public function store(StoreCommissionRateRequest $request)
    {
        $data = $request->only([
            'kg_car_genaral',
            'kg_car_iso',
            'kg_car_brand',
            'kg_ship_genaral',
            'kg_ship_iso',
            'kg_ship_brand',
            'cubic_car_genaral',
            'cubic_car_iso',
            'cubic_car_brand',
            'cubic_ship_genaral',
            'cubic_ship_iso',
            'cubic_ship_brand',
            'admin_remark',
            'bill_id',
            'admin_id',
        ]);
        $commission = $this->commissionInterface->store($data);
        return response()->json($commission, $commission->code);
    }

    public function exportExcel(ExportCommissionRateRequest $request)
    {
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 300);
        try {
            $conditions = $request->only([
                'admin_ref_id',
                'time_format',
                'date_start',
                'date_end',
                'status',
                'rate_id',
                'thai_shipping_method_id',
                'shipping_province_id',
                'bill_no',
                'user_code',
                'user_remark',
                'admin_remark',
                'condition',
            ]);

            $rate = $this->rateInterface->findByAdminId($request->admin_ref_id);
            if ($rate->error) {
                throw new \Exception($rate->message);
            }

            $sale = $this->adminInterface->findById($request->admin_ref_id);
            if ($sale->error) {
                throw new \Exception($sale->message);
            }

            $bill = $this->billInterface->allCommissionByCondition($conditions, false);
            if ($bill->error) {
                throw new \Exception($bill->message);
            }

            $bill_custom = $this->billInterface->allCommissionByCondition($conditions, true);
            if ($bill_custom->error) {
                throw new \Exception($bill_custom->message);
            }

            $filename = Util::generateFilename('commission-' . $sale->results->username, 'xlsx');
            $commissions = new CommissionExport();
            $commissions->setBill($bill->results);
            $commissions->setBillCustom($bill_custom->results);
            $commissions->setSale($sale->results);
            $commissions->setRate($rate->results);
            $commissions->setDateStart($request->date_start);
            $commissions->setDateEnd($request->date_end);

            return ($commissions)->download($filename, ExcelExcel::XLSX, ['X-Vapor-Base64-Encode' => 'True']);
        } catch (\Throwable $th) {
            //throw $th;
            return response()->json(['error' => true, 'code' => 500, 'message' => $th->getMessage()], 500);
        }
    }
}
