<?php

namespace App\Interfaces;

interface CouponInterface
{
    const DEFAULT_LIMIT = 25;
    const DEFAULT_SORTING = 'id';
    const DEFAULT_ASCENDING = 'desc';

    /**
     * fetch all coupon
     *
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function all(array $columns = ['*'], $sorting = CouponInterface::DEFAULT_SORTING, $ascending = CouponInterface::DEFAULT_ASCENDING);

    /**
     * list coupon
     *
     * @param array $conditions
     * @param array $columns
     * @param int $offset
     * @param int $limit
     *
     * @return \Illuminate\Support\Collection
     */
    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * pagination coupon
     *
     * @param array $conditions
     * @param array $columns
     * @param int $page
     * @param int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $perPage = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * find coupon by id
     *
     * @param $id
     * @param array $columns
     *
     * @return object|null
     */
    public function findById($id, array $columns = ['*']);

    /**
     * get coupon dropdown list
     *
     * @return dropdown
     */
    public function dropdownList();

    /**
     * create new coupon
     *
     * @param array $data
     *
     * @return object|null
     */
    public function store(array $data);

    /**
     * update coupon by id
     *
     * @param $id
     * @param array $data
     *
     * @return object
     */
    public function updateById($id, array $data);


    /**
     * soft delete coupon by id
     *
     * @param $id
     *
     * @return object|null
     */
    public function softDeleteById($id);

    /**
     * soft delete coupon by id
     *
     * @param $id
     *
     * @return object|null
     */
    public function check($code, array $data);
}
