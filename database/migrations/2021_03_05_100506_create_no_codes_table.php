<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('no_codes', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('code');
            $table->string('order_code')->nullable();
            $table->string('reference_code')->nullable();
            $table->text('zone')->nullable();

            $table->enum('type', ['clothes', 'bag', 'accessories', 'electronics', 'toy', 'other', 'cosmetics', 'spares', 'shoe'])->nullable();
            $table->enum('department', ['TCATCARGO', 'TMALL', 'TAOBAO', 'ALIBABA'])->nullable();
            $table->text('description')->nullable();

            $table->float('weight', 8, 2)->nullable();
            $table->integer('width')->nullable();
            $table->integer('height')->nullable();
            $table->integer('length')->nullable();
            $table->date('date_in')->nullable();
            $table->date('date_out')->nullable();
            $table->text('remark')->nullable();
            $table->text('system_remark')->nullable();

            $table->integer('amount')->nullable();
            $table->integer('list')->nullable();
            $table->integer('status')->default(1);

            $table->string('source')->nullable();
            $table->string('source_order', 100)->nullable();

            $table->string('created_admin')->nullable();
            $table->string('updated_admin')->nullable();
            $table->string('taken_admin')->nullable();

            $table->integer('created_admin_id')->unsigned()->nullable();
            $table->integer('updated_admin_id')->unsigned()->nullable();
            $table->integer('taken_admin_id')->unsigned()->nullable();

            $table->foreign('created_admin_id')->references('id')->on('admins')->onDelete('SET NULL');
            $table->foreign('updated_admin_id')->references('id')->on('admins')->onDelete('SET NULL');
            $table->foreign('taken_admin_id')->references('id')->on('admins')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('no_codes');
    }
}
