<!DOCTYPE html>
<html lang="th">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>ใบคุม</title>
	{!! Html::style('public/css/pdf.css') !!}
	<link rel="stylesheet" href="https://res.imemo.co/tcatcargo_alpha/misc/css/pdf.css" media="all" />
</head>
<style>
	td,
	th {
		word-wrap: break-word;
	}
</style>
<body>
	<div>
		<table style="margin-bottom: 10px;" aria-describedby="shipping-droppoint-other">
			<tr>
				<th colspan="3" scope="col" class="text-center">
					<div style="font-size: 24px;">ใบคุม&nbsp;Shipping&nbsp;/&nbsp;{!!str_replace(' ', '&nbsp;',@$droppoint->title)!!}</div>
				</th>
			</tr>
			<tr>
				<td>
					<div style="border-bottom: 1px dotted;">วันที่</div>
				</td>
				<td>
					<div style="border-bottom: 1px dotted;">รอบ</div>
				</td>
				<td>
					<div style="border-bottom: 1px dotted;">ฝ่ายจัดส่ง</div>
				</td>
			</tr>
			<tr>
				<td>
					<div style="border-bottom: 1px dotted;">คนเช็คของขึ้น</div>
				</td>
				<td>
					<div style="border-bottom: 1px dotted;">คนเช็คของลง</div>
				</td>
				<td>
					<div style="border-bottom: 1px dotted;">หมายเหตุ</div>
				</td>
			</tr>
		</table>
		<table class="bordered" aria-describedby="shipping-other">
			<tr>
				<th scope="col" width="14">
					<div>#</div>
				</th>
				<th scope="col" width="45">
					<div>@lang('admin.bill_no')</div>
				</th>
				<th scope="col" width="35">
					<div>@lang('admin.user_code')</div>
				</th>
				<th scope="col" width="12">
					<div>T</div>
				</th>
				<th scope="col" width="100">
					<div>ชื่อผู้รับ</div>
				</th>
				<th scope="col" width="20">
					<div>P</div>
				</th>
				<th scope="col" width="30">
					<div>ประเภท</div>
				</th>
				<th scope="col" width="55">
					<div>เลขอ้างอิง</div>
				</th>
				<th scope="col" width="50">
					<div>ยอดเงิน</div>
				</th>
				<th scope="col">
					<div>barcode</div>
				</th>
			</tr>

			@foreach($bills as $key => $bill)
			<tr>
				<td class="text-right" rowspan="2">
					<div>{{ $key+1 }}</div>
				</td>
				<td class="text-center" rowspan="2">
					<div>{{ $bill->no }}</div>
				</td>
				<td class="text-center">
					<div>{{ $bill->user_code }}</div>
				</td>
				<td class="text-center">
					<div>{{ $bill->counter }}</div>
				</td>
				<td>
					<div>{!!str_replace(' ', '&nbsp;',$bill->shipping_name)!!} <br>
						{!!str_replace(' ', '&nbsp;',$bill->shippingProvince->title_th)!!}
						/
						{{ $bill->shipping_zipcode }}
					</div>
				</td>
				<td>
					<div style="border-bottom: 1px dotted;height: 25px;"></div>
				</td>
				<td>
					<div style="border-bottom: 1px dotted;height: 25px;"></div>
				</td>

				<td>
					@if($bill->thai_shipping_code)
					<div style="border-bottom: 1px dotted;height: 25px;" class="text-center">{{$bill->thai_shipping_code}}</div>
					@else
					<div style="border-bottom: 1px dotted;height: 25px;"></div>
					@endif
				</td>

				<td class="text-right">
					@if($bill->thai_shipping_raw_price)
					<div style="border-bottom: 1px dotted;height: 25px;">{{number_format($bill->thai_shipping_raw_price,2)}}</div>
					@else
					<div style="border-bottom: 1px dotted;height: 25px;"></div>
					@endif
				</td>
				<td class="text-center" style="padding: 15px 5px;">
					{!!'<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($bill->no, 'C128',1.5,18) . '" alt="barcode" />'!!}
				</td>
			</tr>
			<tr>
				<td colspan="8" style="background: #eee; ">
					<div>
						@lang('admin.admin_remark'):&nbsp;{{$bill->admin_remark}}
					</div>
				</td>
			</tr>
			@endforeach
			<tr class="text-bold">
				<td colspan="8" class="text-right">
					<div>ยอดรวม</div>
				</td>
				<td class="text-right">
					<div>{{ number_format($bills->sum('thai_shipping_raw_price'),2)}}</div>
				</td>
				<td class="text-center"></td>
			</tr>
		</table>
	</div>


</body>

</html>