<?php

namespace App\Models;

use App\Helpers\ScopeQueries;
use App\Notifications\ResetPassword;
use App\Notifications\VerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject //, MustVerifyEmail

{
    use Notifiable, SoftDeletes;

    protected $guard = 'users';
    protected $guard_name = 'users';

    public static $imageFolder = 'uploads/user/';
    public static $imageWidth = 300;
    public static $imageHeight = 300;

    public static function activeList()
    {
        $array = [
            'active' => __('messages.active'),
            'inactive' => __('messages.inactive'),
        ];
        return $array;
    }

    public static function createBillList()
    {
        $array = [
            'all' => __('messages.create_bill_all'),
            'self' => __('messages.create_bill_self'),
            'admin' => __('messages.create_bill_admin'),
        ];
        return $array;
    }

    public static function conditionList()
    {
        $array = [
            'user_remark' => __('messages.conditions.only_user_remark'),
            'admin_remark' => __('messages.conditions.only_admin_remark'),
            'image' => __('messages.conditions.only_has_image'),
        ];
        return $array;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        // string
        'code',
        'name',
        'email',
        'password',
        'tel1',
        'tel2',
        'image',
        // enum
        'status',
        'create_bill',
        'withholding',
        'sms_notification',
        'alert_box',
        // string
        'tax_id',
        // text
        'alert_message',
        'fcm_token',
        'user_remark',
        'admin_remark',
        'system_remark',
        // string
        'created_admin',
        'updated_admin',
        // integer
        'created_admin_id',
        'updated_admin_id',
        'province_id',
        'admin_ref_id',
        'default_rate_id',
        'thai_shipping_method_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'fcm_token',
        'system_remark',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'image_url',
        'status_text',
        'withholding_text',
        'sms_notification_text',
        'alert_box_text',
        'create_bill_text',
    ];

    /**
     * Get the oauth providers.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function oauthProviders()
    {
        return $this->hasMany(OAuthProvider::class);
    }
    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail);
    }

    /**
     * @return int
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function generateUserCode()
    {
        $user = self::where('code', 'like', 'SF%')->orderBy('id', 'DESC')->first();

        if ($user) {
            $lastcode = $user->code;
        } else {
            $lastcode = 'SF0001';
        }

        $prefix = substr($lastcode, 1, 1);
        $number = substr($lastcode, 2) + 1;
        if ($number == 10000) {
            $number = 0001;
            $prefix_val = ord($prefix) + 1;

            return 'S' . chr($prefix_val) . sprintf('%04d', $number);
        } else {
            return 'S' . $prefix . sprintf('%04d', $number);
        }
    }

    /**
     * Get the profile photo URL attribute.
     *
     * @return string
     */
    public function getImageUrlAttribute($handle = null)
    {
        $file = $handle ?? $this->image;
        return ($file) ? env('APP_MEDIA_URL') . '/' . self::$imageFolder . $file : null;
    }
    /**
     * Get the name for enum columns
     *
     * @return string
     */
    public function getStatusTextAttribute()
    {
        return collect(self::activeList())->get($this->status);
    }
    public function getWithholdingTextAttribute()
    {
        return collect(self::activeList())->get($this->withholding);
    }
    public function getSmsNotificationTextAttribute()
    {
        return collect(self::activeList())->get($this->sms_notification);
    }
    public function getAlertBoxTextAttribute()
    {
        return collect(self::activeList())->get($this->alert_box);
    }
    public function getCreateBillTextAttribute()
    {
        return collect(self::createBillList())->get($this->create_bill);
    }

    /**
     * relation
     */
    public function addresses()
    {
        return $this->hasMany('App\Models\UserAddress', 'user_code', 'code');
    }
    public function banks()
    {
        return $this->hasMany('App\Models\BankAccount', 'user_code', 'code');
    }
    public function province()
    {
        return $this->belongsTo('App\Models\Province', 'province_id', 'id')->select('id', 'title_th');
    }
    public function sale()
    {
        return $this->belongsTo('App\Models\Admin', 'admin_ref_id', 'id')->select('id', 'name', 'lastname', 'email', 'tel');
    }
    public function rate()
    {
        return $this->belongsTo('App\Models\Rate', 'default_rate_id', 'id');
    }
    public function thaiShippingMethod()
    {
        return $this->belongsTo('App\Models\ThaiShippingMethod', 'thai_shipping_method_id', 'id');
    }
    public function emails()
    {
        return $this->morphMany('App\Models\EmailQueue', 'mailable');
    }
    public function sms()
    {
        return $this->morphMany('App\Models\SmsQueue', 'smsable');
    }
    public function chats()
    {
        return $this->hasMany('App\Models\Chat', 'user_code', 'code');
    }
    public function credit()
    {
        return $this->hasOne('App\Models\Credit', 'user_code', 'code')
            ->selectRaw('user_code,SUM(IF(type="topup",amount,-amount)) as total')
            ->groupBy('user_code');
    }
    public function point()
    {
        return $this->hasOne('App\Models\Point', 'user_code', 'code')
            ->selectRaw('user_code,SUM(IF(type="topup",amount,-amount)) as total')
            ->whereRaw('date(created_at) >= "2020-01-01"')
            ->groupBy('user_code');
    }
    /**
     * scope
     *
     */
    public function scopeSearch($query, $conditions)
    {
        return $query->where(function ($query) use ($conditions) {
            if (@$conditions['id']) {
                $query->where('id', $conditions['id']);
            }

            if (@$conditions['code']) {
                $query->where('code', 'LIKE', "%{$conditions['code']}%");
            }

            if (@$conditions['name']) {
                $query->where('name', 'LIKE', "%{$conditions['name']}%");
            }

            if (@$conditions['email']) {
                $query->where('email', 'LIKE', "%{$conditions['email']}%");
            }

            if (@$conditions['tel']) {
                $query->where('tel1', 'LIKE', "%{$conditions['tel']}%")
                    ->orWhere('tel2', 'LIKE', "%{$conditions['tel']}%");
            }

            if (@$conditions['default_rate_id']) {
                $query->where('default_rate_id', '=', $conditions['default_rate_id']);
            }

            if (@$conditions['thai_shipping_method_id']) {
                $query->where('thai_shipping_method_id', '=', $conditions['thai_shipping_method_id']);
            }

            if (@$conditions['province_id']) {
                $query->where('province_id', '=', $conditions['province_id']);
            }

            if (@$conditions['withholding']) {
                $query->where('withholding', '=', $conditions['withholding']);
            }

            if (@$conditions['tax_id']) {
                $query->where('tax_id', '=', $conditions['tax_id']);
            }

            $date_type = (@$conditions['date_type']) ? $conditions['date_type'] : 'created_at';
            if (@$conditions['date_start'] && @$conditions['date_end']) {
                ScopeQueries::scopeDateRange($query, $date_type, $conditions['date_start'], $conditions['date_end']);
            } else if (@$conditions['date_start']) {
                ScopeQueries::scopeDateStart($query, 'created_at', $conditions['date_start']);
            } else if (@$conditions['date_end']) {
                ScopeQueries::scopeDateEnd($query, 'created_at', $conditions['date_end']);
            }

            if (@$conditions['user_remark']) {
                $query->where('user_remark', 'LIKE', "%{$conditions['user_remark']}%");
            }

            if (@$conditions['admin_remark']) {
                $query->where('admin_remark', 'LIKE', "%{$conditions['admin_remark']}%");
            }

            if (@$conditions['sms_notification']) {
                $query->where('sms_notification', '=', $conditions['sms_notification']);
            }

            if (@$conditions['status']) {
                $query->where('status', '=', $conditions['status']);
            }

            if (@$conditions['condition']) {
                switch (@$conditions['condition']) {
                    case 'admin_remark':
                        $query->whereNotNull('admin_remark');
                        break;
                    case 'user_remark':
                        $query->whereNotNull('user_remark');
                        break;
                    case 'has_image':
                        $query->whereNotNull('has_image');
                        break;
                }
            }

            if (@$conditions['admin_ref_id']) {
                $query->where('admin_ref_id', '=', $conditions['admin_ref_id']);
            }
        });
    }
}
