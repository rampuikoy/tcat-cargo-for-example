<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\ProductType;
use App\Models\Admin;
use Tymon\JWTAuth\Facades\JWTAuth;

class ProductTypeTest extends TestCase
{
  use WithFaker;
  protected $user;
  protected $productType;
  protected $url = '/api/backend/product-type';
  /**
   * A basic feature test example.
   *
   * @return void
   */
  protected function authenticate(string $guard = 'admins')
  {
    $this->user = factory(Admin::class)->create();
    auth()->shouldUse($guard);
    $this->productType = factory(ProductType::class)->create();
    $token = JWTAuth::fromUser($this->user);
    return 'Bearer ' . $token;
  }

  protected function deleteUser()
  {
    $this->productType->forceDelete();
    return Admin::find($this->user->id)->forceDelete();
  }

  /** @test */
  public function GetProductTypeSearchSuccessTest()
  {
    $id = random_int(1, 200);
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', "{$this->url}/search", [
      "id" => $id,
      "user_code" => "",
      "name" => "",
      "tel" => "",
      "address" => "",
      "province_id" => "",
      "zipcode" => "",
      "status" => "",
      "user_remark" => "",
      "admin_remark" => "",
    ]);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function GetProductTypeSearchFailTest()
  {
    $response = $this->json('GET', "{$this->url}/search");
    $response->assertStatus(401);
  }

  /** @test */
  public function GetProductTypeDropdownSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', "{$this->url}/dropdown");
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function GetProductTypeDropdownFailTest()
  {
    $response = $this->json('GET', "{$this->url}/dropdown");
    $response->assertStatus(401);
  }

  /** @test */
  public function GetProductTypeAllSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', $this->url);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function GetProductTypeAllFailTest()
  {
    $response = $this->json('GET', $this->url);
    $response->assertStatus(401);
  }

  /** @test */
  public function ProductTypePostSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('POST', $this->url, [
      'title_th' => 'test3',
      'title_en' => 'test3',
      'title_cn' => 'test3',
      // 'status'   => 'inactive',
    ]);
    $response->assertStatus(200);
    ProductType::where('title_th', 'test3')->forceDelete();
    $this->deleteUser();
  }

  /** @test */
  public function ProductTypePostFailAuthTest()
  {
    $response = $this->json('POST', $this->url);
    $response->assertStatus(401);
  }

  /** @test */
  public function ProductTypePostFailTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('POST', $this->url);
    $response->assertStatus(422);
    $this->deleteUser();
  }

  /** @test */
  public function ProductTypeUpdateByIdSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('PUT', "{$this->url}/{$this->productType->id}", [
      'title_th' => 'test',
      'title_en' => 'test',
      'title_cn' => 'test',
    ]);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function ProductTypeUpdateByIdFailAuthTest()
  {
    $response = $this->json('PUT', "{$this->url}/11");
    $response->assertStatus(401);
  }

  /** @test */
  public function ProductTypeUpdateByIdFailTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' =>  $token,
    ])->json('PUT', "{$this->url}/11");
    $response->assertStatus(422);
    $this->deleteUser();
  }
  
  //TODO BUG status 400
  public function ProductTypeDeleteSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('DELETE', "{$this->url}"."/"."999");
    $response->assertStatus(200);
    $this->deleteUser();
  }

  public function ProductTypeDeleteFailTest()
  {
    $response = $this->json('DELETE', "{$this->url}/5");
    $response->assertStatus(401);
  }
}
