<?php

namespace App\Mail\Trip;

use App\Models\Trip;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;

class TripPaid extends Mailable
{
    use Queueable, SerializesModels;
    public $trip;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Trip $trip)
    {
        $this->trip    = $trip;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        try {
            $urlFront =  env('URL_FRONTEND', null);
            $urlBillEdit = $urlFront . '/trip/' . $this->trip->id;
            $data = app('App\Repositories\TripRepository')->findByIdMethod($this->trip->id);
            $file = PDF::loadView('admin.trip.pdf', ['bill' => $data, 'signUrl' => $urlBillEdit]);
            $filename = $this->trip->no . '.pdf';
            return $this->to($this->trip->user->email)
                ->subject(__('emails.trip_created', ['no' => $this->trip->no]))
                ->markdown('emails.trip.trip-paid')
            ->attachData($file->stream(),  $filename, [
                'mime' => 'application/pdf',
            ]);
        } catch (\Throwable $t) {
            Log::error($t->getMessage());
        }
    }
}
