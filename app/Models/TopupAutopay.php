<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TopupAutopay extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'user_code',
        'topup_id',
        'payable_id',
        'payable_type',
        'status',
        'created_admin',
        'updated_admin',
        'created_admin_id',
        'updated_admin_id',
    ];

    protected $appends = ['current_status'];

    public static function statusList()
    {
            $array = [
                'waiting'     => __('messages.cancel'),
                'success'     => __('messages.waiting'),
                'failed'      => __('messages.approved'),
            ];
            return $array;
    }

    public function getCurrentStatusAttribute()
    {
        return collect(self::statusList())->get($this->status);
    }

    public function Topup()
    {
        return $this->belongsTo('App\Models\Topup','topup_id');
    }
    public function payable()
    {
        return $this->morphTo();
    }

}
