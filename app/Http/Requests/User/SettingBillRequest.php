<?php

namespace App\Http\Requests\User;

use App\Http\Requests\FormRequest;

class SettingBillRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.user.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'withholding'               => 'required|in:active,inactive',
            'create_bill'               => 'required|in:all,self,admin',
            'thai_shipping_method_id'   => 'sometimes|nullable|exists:thai_shipping_methods,id',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'withholding'               => __('form.user.withholding'),
            'create_bill'               => __('form.user.create_bill'),
            'thai_shipping_method_id'   => __('form.user.thai_shipping_method_id'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
