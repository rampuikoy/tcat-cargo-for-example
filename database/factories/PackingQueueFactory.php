<?php

namespace Database\Factories;

use App\Models\PackingQueue;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

class PackingQueueFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PackingQueue::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $sufType = [
            'waiting', 'prepair'
        ];
        $sufStatus = [
            'prepairing', 'waiting', 'packing', 'shipping', 'success', 'prepaired'
        ];
        $sufUser = [
            'SF0002',
            'SF0003',
            'SF0004',
            'SF0005',
            'SF0006',
        ];
        $Bill = random_int(1, 5);
        return [
            'user_code' => Arr::random($sufUser),
            'bill_id' => $Bill,
            'bill_code' => 'SH-00000' . $Bill,
            'tracking' => random_int(1, 25),
            'status' => Arr::random($sufStatus),
            'type' => Arr::random($sufType),
        ];
    }
}
