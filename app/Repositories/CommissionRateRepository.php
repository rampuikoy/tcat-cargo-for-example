<?php

namespace App\Repositories;

use App\Interfaces\CommissionRateInterface;
use App\Models\CommissionRate;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use Illuminate\Support\Facades\Cache;


class CommissionRateRepository implements CommissionRateInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const MINUTE_CACHE          = 1;

    public function __construct(CommissionRate $commissionrate)
    {
        $this->commissionrate        = $commissionrate;
    }

    public function findById($id, array $columns = ['*'])
    {
        $commission =  $this->findByIdMethod($id, $columns = ['*']);
        if (!$commission) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.trackings.tracking'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.trackings.tracking')]), $commission);
    }


    public function store(array $data)
    {
        $commission = $this->findByBillId($data['bill_id']);
        $bill       = app('App\Repositories\BillRepository')->findByIdMethod($data['bill_id']);

        if (!is_null($commission)) {
            $commission->update($data);
            $commission->save();
            Cache::tags($this->getBillIdWithClassPrefix($data['bill_id']))->flush();
        } else {
            $commission = $this->commissionrate->create($data);
        }

        $bill->commission_rate_id = $commission->id;
        $bill->save();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.commissionrate')]),  $commission);
    }

    private function findByIdMethod($id, array $columns = ['*'])
    {
        $commission = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix($id),
            $this->getIdWithClassPrefix($id)
        ])->remember($this->getIdWithClassPrefix($id), $this->getTime(), function () use($id, $columns){
            return $this->commissionrate::select($columns)->whereId($id)->first();
        });

        return $commission;
    }

    private function findByBillId($bill_id, array $columns = ['*'])
    {
        $commission = Cache::tags([
            $this->getClassPrefix(),
            $this->getBillIdPrefix($bill_id),
            $this->getBillIdWithClassPrefix($bill_id),
        ])->remember($this->getBillIdWithClassPrefix($bill_id), $this->getTime(), function () use ($bill_id, $columns) {
            return $this->commissionrate::select($columns)->where('bill_id', $bill_id)->first();
        });

        return $commission;
    }


    private function clearCacheTags()
    {
        $keys = [
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix()
        ];
        Cache::tags($keys)->flush();
    }
}
