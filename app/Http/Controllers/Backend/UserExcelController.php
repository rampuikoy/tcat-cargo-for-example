<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserExcelRequest;
use App\Interfaces\UserExcelInterface;
use Illuminate\Http\Request;

class UserExcelController extends Controller
{
    protected $userExcelInterface;

    public function __construct(UserExcelInterface $userExcelInterface)
    {
        $this->userExcelInterface = $userExcelInterface;
    }

    public function importValidate(UserExcelRequest $request)
    {
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 300);
        $users = $this->userExcelInterface->validateExcelFile($request->file);
        return response()->json($users, $users->code);
    }

    public function createMany(Request $request)
    {
        $data      = $request->input('users', []);
        $users     = $this->userExcelInterface->createMany($data);
        return response()->json($users, $users->code);
    }
}
