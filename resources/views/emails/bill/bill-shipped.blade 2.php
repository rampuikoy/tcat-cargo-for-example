@component('mail::message')
# เรียนคุณ {{$bill->user->name}}
แจ้งอัพเดทสถานะบิลขนส่ง <br>
รหัสลูกค้า: {{$bill->user->code}} <br>
สถานะบิล:  <span style="color: #1bc5bd">จัดส่งแล้ว </span>
@component('mail::table')
| จัดส่งแล้ว   |               |
|:-------------|--------------:|
| การขนส่งภายในประเทศ      |  <span style="text-align:left;">{{ $bill->thaiShippingMethod->title }}</span> |
| ชื่อผู้รับ   ฅ{{ $bill->shipping_name }} / เบอร์โทร: {{$bill->shipping_tel}} |
|ที่อยู่จัดส่ง {{$bill->shipping_address}} |
| ต.{{@$bill->shippingDistrict->title_th}} อ.{{@$bill->shippingAmphur->title_th}} จ.{{@$bill->shippingProvince->title_th}} {{$bill->shipping_zipcode}}|
@endcomponent
@endcomponent
