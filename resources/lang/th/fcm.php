<?php

return [

    'topup_approved'            => [
        'title'     => 'เติมเครดิตสำเร็จ :no',
        'message'   => 'จำนวน :amount บาท / คงเหลือ :credit บาท',
    ],
    'topup_cancel'              => [
        'title'     => 'เติมเครดิต :no',
        'message'   => 'ถูกยกเลิก /:remark',
    ],

    'money_receive_approved'    => [
        'title'     => 'บิลรับเงินจีน :no สำเร็จ',
        'message'   => 'จำนวน :amount หยวน / คงเหลือ :credit บาท',
    ],
    'money_receive_cancel'      => [
        'title'     => 'บิลรับเงินจีน :no',
        'message'   => 'ถูกยกเลิก /:remark',
    ],

    'withdraw_approved'         => [
        'title'     => 'ถอนเครดิตสำเร็จ :no',
        'message'   => 'จำนวน :amount บาท / คงเหลือ :credit บาท',
    ],
    'bill'         => [
        'bill_new_title'        => 'บิลขนส่งใหม่ :no',
        'bill_new_message'      => ':no ยอดที่ต้องชำระ :total บาท',
        'bill_paid_title'       => 'บิลขนส่ง :no',
        'bill_paid_message'     => 'ชำระเงินเรียบร้อยแล้ว',
        'bill_shipping_title'   => 'บิลขนส่ง :no',
        'bill_shipping_message' => 'กำลังจัดส่ง',
        'bill_shipped_title'    => 'บิลขนส่ง :no',
        'bill_shipped_message'  => 'จัดส่งแล้ว',
        'bill_success_title'    => 'บิลขนส่ง :no',
        'bill_success_message'  => 'สำเร็จแล้ว/ปิดบิล',
        'bill_cancel_title'     => 'บิลขนส่ง :no ',
        'bill_cancel_message'   => 'ถูกยกเลิก /:remark',
        'bill_dropped_title'    => 'บิลขนส่ง :no',
        'bill_dropped_message'  => 'กรุณาติดต่อรับสินค้า :no จัดส่งโดย:method',
    ],

    // 'money_new_title'        => 'บิลโอนเงินจีนใหม่ :no',
    // 'money_new_message'      => 'จำนวน :money_china หยวน ยอดที่ต้องชำระ :total บาท',
    // 'money_paid_title'       => 'บิลโอนเงินจีน :no ',
    // 'money_paid_message'     => 'ชำระเงินเรียบร้อยแล้ว',
    // 'money_success_title'    => 'บิลโอนเงินจีน :no',
    // 'money_success_message'  => 'สำเร็จแล้ว/ปิดบิล',
    // 'money_cancel_title'     => 'บิลโอนเงินจีน :no',
    // 'money_cancel_message'   => 'ถูกยกเลิก /:remark',
    // 'tracking_wait_title'    => 'รอออกบิลขนส่ง',
    // 'tracking_wait_message'  => 'สินค้าของคุณถึงโกดังไทยแล้ว :totalแทร็ค (รอออกบิล)',
    // 'point_order_success_title'    => 'แลกสินค้า :no สำเร็จแล้ว',
    // 'point_order_success_message'  => 'เลขพัสดุ :tracking ค่าขนส่ง :cost บาท',
    // 'point_order_cancel_title'  => 'แลกสินค้า :no',
    // 'point_order_cancel_message'  => 'ถูกยกเลิก',
    'point_order_success' => [
        'title'    => 'แลกสินค้า :no สำเร็จแล้ว',
        'message'  => 'เลขพัสดุ :tracking ค่าขนส่ง :cost บาท',
    ],
    'point_order_cancel' => [
        'title'    => 'แลกสินค้า :no',
        'message'  => 'ถูกยกเลิก',
    ],
];
