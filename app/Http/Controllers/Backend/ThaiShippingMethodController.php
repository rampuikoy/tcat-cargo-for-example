<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Models\ThaiShippingMethod;
use App\Http\Controllers\Controller;
use App\Interfaces\ThaiShippingMethodInterface;
use App\Http\Requests\ThaiShippingMethod\UpdateThaiShippingMethodRequest;
use App\Http\Requests\ThaiShippingMethod\StoreThaiShippingMethodRequest;

class ThaiShippingMethodController extends Controller
{
    public function __construct(ThaiShippingMethodInterface $thaiShippingMethodInterface)
    {
        $this->thaiShippingMethodInterface = $thaiShippingMethodInterface;
    }

    public function index(Request $request)
    {
        $this->authorize('index', ThaiShippingMethod::class);
        $columns                    = $request->input('columns', ['*']);
        $thaiShippingMethods        = $this->thaiShippingMethodInterface->all($columns);
        return response()->json($thaiShippingMethods, $thaiShippingMethods->code);
    }

    public function show(Request $request, $id)
    {
        $this->authorize('show', ThaiShippingMethod::class);
        $columns                     = $request->input('columns', ['*']);
        $thaiShippingMethods         = $this->thaiShippingMethodInterface->findById($id, $columns);
        return response()->json($thaiShippingMethods, $thaiShippingMethods->code);
    }

    public function search(Request $request)
    {
        $this->authorize('index', ThaiShippingMethod::class);
        $conditions     = $request->only(['id', 'title', 'remark', 'type', 'status','calculate_type','access']);
        $columns        = $request->input('columns', ['*']);
        $limit          = $request->input('limit', $this->thaiShippingMethodInterface::DEFAULT_LIMIT);
        $sorting        = $request->input('sorting', $this->thaiShippingMethodInterface::DEFAULT_SORTING);
        $ascending      = $request->input('ascending') == 1 ? 'asc' : 'desc';

        if ($request->boolean('pagination')) {
            $page             = $request->input('page', 1);
            $thaiShippingMethods  = $this->thaiShippingMethodInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset           = $request->input('page', 0);
            $thaiShippingMethods  = $this->thaiShippingMethodInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($thaiShippingMethods, $thaiShippingMethods->code);
    }

    public function dropdown()
    {
        $dropdown = $this->thaiShippingMethodInterface->dropdownList();
        return response()->json($dropdown, $dropdown->code);
    }

    public function postDroppoint(){
        $droppoint = $this->thaiShippingMethodInterface->postDroppoint();
        return response()->json($droppoint, $droppoint->code);
    }

    public function store(StoreThaiShippingMethodRequest $request)
    {
        $data              = $request->only([
                                'title',
                                'address',
                                'province_id',
                                'amphur_id',
                                'zipcode',
                                'district_code',
                                'latitude',
                                'longitude',
                                'max_weight', //น้ำหนักสูงสุด
                                'max_distance', //ระยะทางสูงสุด
                                'charge', //ค่าบริการ(บาท)*
                                'shipping_rate_weight', // ค่าขนส่ง(บาท/KG)*
                                'shipping_rate_cubic', // ค่าขนส่ง(บาท/CBM)*
                                'api',
                                'calculate_type', // การคิดราคา*
                                'type',
                                'status',
                                'image',
                                'refund',
                                'access', // การเข้าถึง*
                                'tel',
                                'detail',
                                'remark',
                                'min_price' //ค่าส่งขั้นต่ำ(บาท)*
                            ]);
        $thaiShippingMethods    = $this->thaiShippingMethodInterface->store($data);
        return response()->json($thaiShippingMethods, $thaiShippingMethods->code);
    }

    public function update(UpdateThaiShippingMethodRequest $request, $id)
    {
        $data              = $request->only([
                                'title',
                                'address',
                                'province_id',
                                'amphur_id',
                                'district_code',
                                'zipcode',
                                'latitude',
                                'longitude',
                                'max_weight',
                                'max_distance',
                                'charge',
                                'shipping_rate_weight',
                                'shipping_rate_cubic',
                                'api',
                                'calculate_type',
                                'type',
                                'status',
                                'image',
                                'refund',
                                'access',
                                'tel',
                                'detail',
                                'remark',
                                'min_price'
                            ]);
        $thaiShippingMethods    = $this->thaiShippingMethodInterface->updateById($id, $data);
        return response()->json($thaiShippingMethods, $thaiShippingMethods->code);
    }

    public function destroy($id)
    {
        $this->authorize('destroy', ThaiShippingMethod::class);
        $thaiShippingMethods     = $this->thaiShippingMethodInterface->softDeleteById($id);
        return response()->json($thaiShippingMethods, $thaiShippingMethods->code);
    }

}
