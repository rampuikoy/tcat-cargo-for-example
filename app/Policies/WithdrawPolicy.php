<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class WithdrawPolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.withdraw.index');
    }
    public function show(Admin $admin)
    {
        return $admin->hasAnyPermission(['backend.withdraw.show', 'backend.withdraw.update']);
    }

    public function store(Admin $admin)
    {
        return $admin->can('backend.withdraw.store');
    }
    public function update(Admin $admin)
    {
        return $admin->can('backend.withdraw.update');
    }

    public function destroy(Admin $admin)
    {
        return $admin->can('backend.withdraw.destroy');
    }
    public function approve(Admin $admin)
    {
        return $admin->can('backend.withdraw.approve');
    }
    public function cancel(Admin $admin)
    {
        return $admin->can('backend.withdraw.destroy');
    }
}
