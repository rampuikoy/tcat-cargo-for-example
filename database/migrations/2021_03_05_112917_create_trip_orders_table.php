<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('type',20);
            $table->string('title')->nullable();
            $table->float('price', 8, 2)->default(0);
            $table->integer('amount')->unsigned();
            $table->float('total', 8, 2)->default(0);

            $table->integer('trip_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip_orders');
    }
}
