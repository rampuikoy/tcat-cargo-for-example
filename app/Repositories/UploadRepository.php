<?php

namespace App\Repositories;

use App\Interfaces\UploadInterface;
use App\Models\Bill;
use App\Models\Claims;
use App\Models\NoCode;
use App\Models\Tracking;
use App\Models\Upload;
use App\Models\Withdraw;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use App\Traits\UploadTrait;
use Illuminate\Support\Facades\Cache;

class UploadRepository implements UploadInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, UploadTrait, CoreResponseTrait;

    const MINUTE_CACHE = 1;

    public function __construct(Upload $upload, Withdraw $withdraw, Tracking $tracking, Bill $bill, Claims $claims, NoCode $nocode)
    {
        $this->upload = $upload;
        $this->withdraw = $withdraw;
        $this->tracking = $tracking;
        $this->bill = $bill;
        $this->claims = $claims;
        $this->nocode = $nocode;
    }

    public function store(array $data)
    {
        $relatable_type = $data['relatable_type'] ?? null;
        $sub_path = $data['sub_path'] ?? null;

        $folder = $this->upload::$imageFolder;
        $imageWidth = $this->upload::$imageWidth;
        $imageHeight = $this->upload::$imageHeight;

        switch ($relatable_type) {
            case 'App\Models\Withdraw':
                $folder = $this->withdraw::$imageFolder;
                $imageWidth = $this->withdraw::$imageWidth;
                $imageHeight = $this->withdraw::$imageHeight;
                break;
            case 'App\Models\Tracking':
                $folder = $this->tracking::$imageFolder;
                $imageWidth = $this->tracking::$imageWidth;
                $imageHeight = $this->tracking::$imageHeight;
                break;
            case 'App\Models\Bill':
                $sub_path = ($sub_path == 'pack' ? $this->bill::$imageSubFolderPack : ($sub_path == 'sign' ? $this->bill::$imageSubFolderSing : null));
                $folder = $this->bill::$imageFolder . $sub_path;
                $imageWidth = $this->bill::$imageWidth;
                $imageHeight = $this->bill::$imageHeight;
                break;
            case 'App\Models\Claims':
                $folder = $this->claims::$imageFolder;
                $imageWidth = $this->claims::$imageWidth;
                $imageHeight = $this->claims::$imageHeight;
                break;
            case 'App\Models\NoCode':
                $folder = $this->nocode::$imageFolder;
                $imageWidth = $this->nocode::$imageWidth;
                break;
            default:
                break;
        }

        $upload = $this->upload::create([
            'relatable_type' => $relatable_type,
            'relatable_id' => (array_key_exists('relatable_id', $data)) ? $data['relatable_id'] : null,
            'type' => (array_key_exists('type', $data)) ? $data['type'] : null,
            'file_path' => $this->uploadBase64($data['file_path'], $folder, '', $imageWidth, $imageHeight),
            'sub_path' => $sub_path,
        ]);
        Cache::tags([$this->getIdWithClassPrefix($upload->id)])->flush();
        $this->clearCacheByModule($upload->relatable_type, $upload->relatable_id);

        return $this->coreResponse(200, __('messages.upload_file'), $upload);
    }

    public function delete($id)
    {
        $upload = $this->getCacheOrFind($id);
        if (!$upload) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.file'), 'id' => $id]));
        }
        $upload->delete();
        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();
        $this->clearCacheByModule($upload->relatable_type, $upload->relatable_id);

        return $this->coreResponse(200, __('messages.delete_file'), $upload);
    }

    private function getCacheOrFind($id)
    {
        $key = $this->getCacheKey(null, $id);
        $time = $this->getTime(self::MINUTE_CACHE);

        $upload = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($id),
        ])->remember($key, $time, function () use ($id) {
            return $this->upload->find($id);
        });
        return $upload;
    }

    private function clearCacheByModule($class_name, $id)
    {
        $repo_path = '';
        switch ($class_name) {
            case 'App\Models\Withdraw':
                $repo_path = 'App\Repositories\WithdrawRepository';
                break;
            case 'App\Models\Tracking':
                $repo_path = 'App\Repositories\TrackingRepository';
                break;
            case 'App\Models\Bill':
                $repo_path = 'App\Repositories\BillRepository';
                break;
            case 'App\Models\NoCode':
                $repo_path = 'App\Repositories\NoCodeRepository';
                break;
            case 'App\Models\Claims':
                $repo_path = 'App\Repositories\ClaimsRepository';
                break;
            default:
                break;
        }
        if ($repo_path) {
            app($repo_path)->clearCacheTagById($id);
        }
    }
}
