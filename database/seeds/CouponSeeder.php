<?php

namespace Database\Seeders;

use App\Helpers\MoveSqlData;
use Illuminate\Database\Seeder;

class CouponSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Coupon::factory(10)->create();
        $move_geo_data    = collect(MoveSqlData::insertCoupon());
        $message          = $move_geo_data->get('message');
        $error            = $move_geo_data->get('error');

        if($error){
            $this->command->error($message);
            $this->command->warning('Please insert fresh again');
        }else{
            $this->command->info($message);
        }
    }
}
