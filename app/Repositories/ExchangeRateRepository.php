<?php

namespace App\Repositories;

use App\Interfaces\ExchangeRateInterface;
use App\Models\ExchangeRate;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use Illuminate\Support\Facades\Cache;

class ExchangeRateRepository implements ExchangeRateInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const MINUTE_CACHE          = 1;

    public function __construct(ExchangeRate $exchangerate)
    {
        $this->exchangerate     = $exchangerate;
    }

    public function all(array $columns = ['*'], $sorting = ExchangeRateInterface::DEFAULT_SORTING, $ascending = ExchangeRateInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);
        $exchanges  = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
            return $this->exchangerate::select($columns)->orderBy($sorting, $ascending)->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.exchange_rate')]), $exchanges);
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = ExchangeRateInterface::DEFAULT_LIMIT, $sorting = ExchangeRateInterface::DEFAULT_SORTING, $ascending = ExchangeRateInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $exchanges  = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
            return $this->exchangerate::select($columns)
                ->search($conditions)
                ->orderBy($sorting, $ascending)
                ->skip($offset)
                ->limit($limit)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.exchange_rate')]), $exchanges);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = ExchangeRateInterface::DEFAULT_LIMIT, $sorting = ExchangeRateInterface::DEFAULT_SORTING, $ascending = ExchangeRateInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $exchanges  = Cache::tags([

            $this->getClassPrefix(),
            $this->getPaginationWithClassPrefix(),

        ])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
            $model = $this->exchangerate::select($columns)
                ->search($conditions)
                ->orderBy($sorting, $ascending);

            if ($page == 1) {
                return $model->paginate($limit);
            } else {
                return $model->skip($limit * ($page - 1))->paginate($limit);
            }
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.exchange_rate')]), $exchanges);
    }

    public function findById($id, array $columns = ['*'])
    {
        $exchange = $this->findByIdMethod($id, $columns);
        if (!$exchange) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.exchange_rate'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.exchange_rate')]), $exchange);
    }

    public function store(array $data)
    {
        $exchange   = $this->exchangerate::create($data);
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.exchange_rate')]), $exchange);
    }

    public function updateById($id, array $data)
    {
        $exchange = $this->findByIdMethod($id);

        if (!$exchange) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.exchange_rate'), 'id' => $id]));
        }

        $exchange->update($data);
        Cache::tags([$this->getIdWithClassPrefix($exchange->id)])->flush();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.exchange_rate')]), $exchange);
    }

    public function softDeleteById($id)
    {
        $exchange = $this->findByIdMethod($id);

        if (!$exchange) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.exchange_rate'), 'id' => $id]));
        }

        $exchange->delete();
        Cache::tags([$this->getIdWithClassPrefix($exchange->id)])->flush();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.delete_data', ['data' => __('messages.exchange_rate')]), $exchange);
    }

    public function getRateExtend($code)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);
        $exchanges  = Cache::tags([$this->getClassPrefix()])->remember($key, $time, function () {
            return $this->exchangerate->orderBy(ExchangeRateInterface::DEFAULT_SORTING, ExchangeRateInterface::DEFAULT_ASCENDING)->first();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.exchange_rate')]), $exchanges);
    }

    private function clearCacheTags()
    {
        Cache::tags([
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix()
        ])->flush();
    }

    private function findByIdMethod($id, array $columns = ['*'])
    {
        $key        = $this->getCacheKey(null, $id);
        $time       = $this->getTime(self::MINUTE_CACHE);

        $exchangerate    = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($id)
        ])->remember($key, $time, function () use ($id, $columns) {
            return $this->exchangerate::select($columns)->whereId($id)->first();
        });

        return $exchangerate;
    }
}
