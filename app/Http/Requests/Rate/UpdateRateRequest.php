<?php

namespace App\Http\Requests\Rate;

use App\Http\Requests\FormRequest;

class UpdateRateRequest extends FormRequest
{
    public function authorize()
    {
        return $this->user('admins')->can('backend.rate.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'                 => 'required|max:50',
            'status'                => 'required|in:active,inactive',
            'admin_remark'          => 'required',
            'user_code'             => 'required_if:type,user|required_if:type,sale_only_user|nullable|exists:users,code',
            'type'                  => 'required|in:general,user,sale,sale_only_user',
            'admin_id'              => 'required_if:type,sale|required_if:type,sale_only_user|nullable|exists:admins,id',

            'kg_car_genaral'        => 'required|numeric',
            'kg_car_iso'            => 'required|numeric',
            'kg_car_brand'          => 'required|numeric',

            'kg_ship_genaral'       => 'required|numeric',
            'kg_ship_iso'           => 'required|numeric',
            'kg_ship_brand'         => 'required|numeric',

            'kg_plane_genaral'      => 'required|numeric',
            'kg_plane_iso'          => 'required|numeric',
            'kg_plane_brand'        => 'required|numeric',

            'cubic_car_genaral'     => 'required|numeric',
            'cubic_car_iso'         => 'required|numeric',
            'cubic_car_brand'       => 'required|numeric',

            'cubic_ship_genaral'    => 'required|numeric',
            'cubic_ship_iso'        => 'required|numeric',
            'cubic_ship_brand'      => 'required|numeric',

            'cubic_plane_genaral'   => 'required|numeric',
            'cubic_plane_iso'       => 'required|numeric',
            'cubic_plane_brand'     => 'required|numeric',

        ];
    }
}
