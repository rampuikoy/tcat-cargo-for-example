<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Commission\StoreCommissionRequest;
use App\Interfaces\AdminInterface;
use App\Interfaces\CommissionInterface;
use App\Models\Commission;
use Illuminate\Http\Request;

class CommissionController extends Controller
{
    protected $commissionInterface, $adminInterface;

    public function __construct(CommissionInterface $commissionInterface, AdminInterface $adminInterface)
    {
        $this->commissionInterface  = $commissionInterface;
        $this->adminInterface       = $adminInterface;
    }

    public function search(Request $request)
    {
        $this->authorize('index', Commission::class);
        $conditions     = $request->only([
                            'no',
                            'admin_ref_id',
                            'time_format',
                            'date_start',
                            'date_end',
                        ]);
        $columns        = $request->input('columns', ['*']);
        $limit          = $request->input('limit', $this->commissionInterface::DEFAULT_LIMIT);
        $sorting        = $request->input('order_by', $this->commissionInterface::DEFAULT_SORTING);
        $ascending      = $request->input('ascending') == 1 ? 'asc' : 'desc';

        if ($request->boolean('pagination')) {
            $page       = $request->input('page', 1);
            $commission = $this->commissionInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset     = $request->input('page', 0);
            $commission = $this->commissionInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($commission, $commission->code);
    }

    public function dropdown()
    {
        $dropdown   = $this->commissionInterface->dropdownList();
        if (!$dropdown->error) {
            $sales              = $this->adminInterface->findManySale(['id', 'name', 'username']);
            $dropdown->results  = collect($dropdown->results)->merge(['sale' => (!$sales->error) ? $sales->results : []]);
        }
        return response()->json($dropdown, $dropdown->code);
    }

    public function show(Request $request, $id)
    {
        $this->authorize('show', Commission::class);
        $columns = $request->input('columns', ['*']);
        $commission = $this->commissionInterface->findById($id, $columns);
        return response()->json($commission, $commission->code);
    }

    public function store(StoreCommissionRequest $request)
    {
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 300);

        $data           = $request->only(['file', 'admin_id', 'remark']);
        $commission     = $this->commissionInterface->store($data);

        return response()->json($commission, $commission->code);
    }
}
