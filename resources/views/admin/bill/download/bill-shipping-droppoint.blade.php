<!DOCTYPE html>
<html lang="th">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>ใบคุม</title>
	{!! Html::style('public/css/pdf.css') !!}
	<link rel="stylesheet" href="https://res.imemo.co/tcatcargo_alpha/misc/css/pdf.css" media="all" />
</head>

<body>
	<div>
		<table style="margin-bottom: 10px;" aria-describedby="shipping-droppoint">
			<tr>
				<th colspan="3" scope="col" class="text-center">
					<div style="font-size: 24px;">ใบคุม&nbsp;Shipping&nbsp;/&nbsp;{!!str_replace(' ', '&nbsp;',@$droppoint->title)!!}</div>
				</th>
			</tr>
			<tr>
				<td>
					<div style="border-bottom: 1px dotted;">วันที่</div>
				</td>
				<td>
					<div style="border-bottom: 1px dotted;">รอบ</div>
				</td>
				<td>
					<div style="border-bottom: 1px dotted;">ฝ่ายจัดส่ง</div>
				</td>
			</tr>
			<tr>
				<td>
					<div style="border-bottom: 1px dotted;">คนเช็คของขึ้น</div>
				</td>
				<td>
					<div style="border-bottom: 1px dotted;">คนเช็คของลง</div>
				</td>
				<td>
					<div style="border-bottom: 1px dotted;">หมายเหตุ</div>
				</td>
			</tr>
		</table>
		<table class="bordered" aria-describedby="shipping-detail">
			<thead>
				<tr>
					<th scope="col" width="10">
						<div>#</div>
					</th>
					<th scope="col" width="50">
						<div>@lang('admin.bill_no')</div>
					</th>
					<th scope="col" width="27">
						<div>@lang('admin.user_code')</div>
					</th>
					<th scope="col" width="15">
						<div>T</div>
					</th>
					<th scope="col" width="18">
						<div>P</div>
					</th>
					<th scope="col" width="25">
						<div>เช็คของขึ้น</div>
					</th>
					<th scope="col" width="25">
						<div>เช็คของลง</div>
					</th>
					<th scope="col" width="150">
						<div>@lang('admin.remark')</div>
					</th>
					<th scope="col">
						<div>barcode</div>
					</th>
				</tr>
			</thead>
			@foreach($bills as $key => $bill)
			<tr>
				<td class="text-right">
					<div>{{ $key + 1 }}</div>
				</td>
				<td class="text-center">
					<div>{{ $bill->no }}</div>
				</td>
				<td class="text-center">
					<div>{{ $bill->user_code }}</div>
				</td>
				<td class="text-center">
					<div>{{ $bill->counter }}</div>
				</td>
				<td>
					<div style="border-bottom: 1px dotted;height: 25px;"></div>
				</td>
				<td>
					<div style="border-bottom: 1px dotted;height: 25px;"></div>
				</td>
				<td>
					<div style="border-bottom: 1px dotted;height: 25px;"></div>
				</td>
				<td>
					<div>{{ $bill->admin_remark }}</div>
				</td>
				<td class="text-center" style="padding: 10px 10px;">
					{!!'<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($bill->no, 'C128',1.5,18) . '" alt="barcode" />'!!}
				</td>
			</tr>
			@endforeach
		</table>
	</div>


</body>

</html>