<?php

namespace App\Repositories;

use App\Interfaces\CreditInterface;
use App\Models\Credit;
use App\Traits\CacheTrait;
use Illuminate\Support\Facades\Cache;
use App\Traits\CoreResponseTrait;

class CreditRepository implements CreditInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const MINUTE_CACHE          = 1;
    const TABLE_RELETIONS       = [];

    public function __construct(Credit $credit)
    {
        $this->credit        = $credit;
    }

    public function all(array $columns = ['*'], $sorting = CreditInterface::DEFAULT_SORTING, $ascending = CreditInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $credits    = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
                        return $this->credit::select($columns)->orderBy($sorting, $ascending)->get();
                    });
        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.Credit')]), $credits);
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = CreditInterface::DEFAULT_LIMIT, $sorting = CreditInterface::DEFAULT_SORTING, $ascending = CreditInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $credits    = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
                        return $this->credit::select($columns)
                            ->search($conditions)
                            ->orderBy($sorting, $ascending)
                            ->skip($offset)
                            ->limit($limit)
                            ->get();
                    });
        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.Credit')]), $credits);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = CreditInterface::DEFAULT_LIMIT, $sorting = CreditInterface::DEFAULT_SORTING, $ascending = CreditInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $credits    = Cache::tags([$this->getClassPrefix(), $this->getPaginationWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
                        $model = $this->credit::select($columns)
                            ->search($conditions)
                            ->orderBy($sorting, $ascending);

                        if ($page == 1) {
                            return $model->paginate($limit);
                        } else {
                            return $model->skip($limit * ($page - 1))->paginate($limit);
                        }
                    });
        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.Credit')]), $credits);
    }

    public function findById($id, array $columns = ['*'])
    {
        $credit    = $this->findByIdMethod($id);

        if(!$credit){
            $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.Credit'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.Credit')]), $credit);
    }

    public function dropdownList()
    {
        $key        = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $dropdown   = Cache::remember($key, $time, function () {
            return [
                'type'      => $this->credit::typeList(),
                'condition' => $this->credit::conditionList()
            ];
        });
        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function store(array $data)
    {
        $credit  = $this->credit::create($data);
        $this->clearCacheTags();
        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.Credit')]), $credit);
    }

    public function updateById($id, array $data)
    {
        $credit    = $this->findByIdMethod($id);

        if (!$credit) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.Credit'), 'id' => $id]));
        }

        $credit->update($data);
        $this->clearCacheTags();
        $this->clearCacheTagById($credit->id);
        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.Credit')]), $credit);
    }


    private function findByIdMethod($id, array $columns = ['*'])
    {
        $key        = $this->getCacheKey(null, $id);
        $time       = $this->getTime(self::MINUTE_CACHE);

        $article    = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($id)
        ])->remember($key, $time, function () use ($id, $columns) {
            return $this->credit::select($columns)->whereId($id)->first();
        });

        return $article;
    }

    public function clearCacheTags()
    {
        Cache::tags([
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix()
        ])->flush();
    }

    public function clearCacheTagById($id){
        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();
    }

    public function clearCacheTagByUserCode($userCode){
        Cache::tags([
            $this->getUserCodePrefix($userCode),
            $this->getUserCodeWithClassPrefix($userCode)
        ])->flush();
    }
}
