<?php

namespace App\Http\Requests\Tracking;

use App\Http\Requests\FormRequest;

class PutZoneInModalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.tracking.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'warehouse_zone' => 'required',
            'reference_code' => 'exists:users,code',
            'ids'           => 'required|array',
            'ids.*'         => 'required|integer',
            'warehouse_id' => 'required|exists:warehouses,id',
            'shipping_method_id' => 'exists:shipping_methods,id',
            'product_type_id' => 'exists:product_types,id'
        ];
    }
}
