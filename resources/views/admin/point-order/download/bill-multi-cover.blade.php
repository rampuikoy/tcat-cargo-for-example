<!DOCTYPE html>
<html lang="th">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>ใบปะหน้า</title>
	{!! Html::style('public/css/pdf.css') !!}
	<link rel="stylesheet" href="https://res.imemo.co/tcatcargo_alpha/misc/css/pdf.css" media="all" />
  <style>
    html {
      margin: 30px 20px 30px 10px;
      ;
    }

    table.cover td {
      border: 1px solid #000;
    }

    tbody {
      font-size: 20px;
    }
  </style>
</head>

<body>
  <div>
    <table aria-describedby="multi-cover">
      <tbody>
        @foreach($bills as $key => $bill)
        <tr>
          <th scope="col"></th>
          <td style="padding-bottom: 30px;">
            <table class="cover" aria-describedby="cover">
              <tr>
                <th scope="col"></th>
              </tr>
              <tr style=" margin-left: 0px;">
                @include('admin.bill.download.partials.bill-qr2')
              </tr>
              <tr style="line-height: 13px;font-size: 18px;">
                <td width="65%" style="padding: 5px;">
                  <div>@lang('admin.user_code'):&nbsp;{{$bill->user_code}}&nbsp;({{$bill->user->name}})&nbsp;<br>
                    @lang('admin.thai_shipping_method'):&nbsp;{{ $bill->shippingMethod->title}}<br>
                    @lang('admin.thai_shipping_detail'):&nbsp;{{ $bill->thai_shipping_detail}}
                  </div>
                </td>
                <td>
                  <div style="font-size:80px;text-align:center;margin-top:-10px;line-height:50px;">
                    <strong>{{$bill->user_code}}</strong>
                  </div>
                  <div style="font-size:30px;text-align:center;">
                    <strong>@lang('admin.product_list')&nbsp;:&nbsp;{{$bill->counter}}</strong>
                  </div>
                </td>
              </tr>
              <tr>
                <td colspan="2">
                  <div style="font-size:50px;text-align:left;margin-top:-25px;">
                    <strong>{{ $bill->shippingMethod->title}}</strong>
                  </div>
                </td>
              </tr>
              <tr>
                <td colspan="2">
                  <div style="line-height: 17px;padding: 8px 5px 8px 5px;font-size: 26px;">
                    ผู้รับ:&nbsp;{{$bill->title}}&nbsp;/&nbsp;โทร&nbsp;{{$bill->tel}} <br>
                    {!!str_replace(' ', '&nbsp;',$bill->address)!!}
                    ต.{{$bill->district}}&nbsp;อ.{{$bill->amphur}}&nbsp;จ.{{$bill->province}}&nbsp;{{$bill->zipcode}}
                  </div>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</body>

</html>