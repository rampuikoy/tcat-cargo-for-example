<?php

namespace Database\Factories;

use App\Models\coupon;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

class CouponFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = coupon::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $sufUser = [
            'SF0002',
            'SF0003',
            'SF0004',
            'SF0005',
            'SF0006',
        ];
        return [
            'title' => $this->faker->lexify('???????'),
            'code' => $this->faker->numerify($string = '######'),
            'user_code' => Arr::random($sufUser),
            'type' => 'life_time',
            'price' => random_int(0, 1000),
            'start_date' => Carbon::now(),
            'end_date' => $this->faker->dateTimeBetween('+0 days', '+1 month'),
        ];
    }
}
