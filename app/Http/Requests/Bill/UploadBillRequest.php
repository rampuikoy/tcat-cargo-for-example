<?php

namespace App\Http\Requests\Bill;

use App\Http\Requests\FormRequest;
use App\Rules\Base64Image;
class UploadBillRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.bill.store');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file_path'     =>  ['required', new Base64Image],
            'sub_path'      =>  'required|in:pack,sign',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'file_path'    => __('form.bill_update.file_path'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
