<?php

namespace App\Interfaces;

interface UserInterface
{
    const DEFAULT_LIMIT = 25;
    const DEFAULT_SORTING = 'id';
    const DEFAULT_SORTING_POINT = 'code';
    const DEFAULT_ASCENDING = 'desc';

    /**
     * fetch all user
     *
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function all(array $columns = ['*'], $sorting = UserInterface::DEFAULT_SORTING, $ascending = UserInterface::DEFAULT_ASCENDING);

    /**
     * list user
     *
     * @param array $conditions
     * @param array $columns
     * @param int $offset
     * @param int $limit
     *
     * @return \Illuminate\Support\Collection
     */
    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * list user
     *
     * @param array $conditions
     * @param array $columns
     * @param int $offset
     * @param int $limit
     *
     * @return \Illuminate\Support\Collection
     */
    public function fetchListByFieldsPoints(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * pagination user
     *
     * @param array $conditions
     * @param array $columns
     * @param int $page
     * @param int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $perPage = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * find user by code
     * pagination user
     *
     * @param array $conditions
     * @param array $columns
     * @param int $page
     * @param int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateListByFieldsPoints(array $conditions, array $columns = ['*'], $page = 1, $perPage = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * find user by id
     *
     * @param $code
     * @param array $columns
     *
     * @return object|null
     */
    public function findByCode($code, array $columns = ['*']);

    /**
     * find user by email
     *
     * @param $email
     * @param array $columns
     *
     * @return object|null
     */
    public function findByEmail($email, array $columns = ['*']);

    /**
     * find many user by user_code or email
     *
     * @param $slug
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function findByUserCodeOrEmail($slug, array $columns = ['*']);

    /**
     * get user dropdown list
     *
     * @return object
     */
    public function dropdownList();

    /**
     * create new user
     *
     * @param array $data
     *
     * @return object|null
     */
    public function store(array $data);

    /**
     * update user by id
     *
     * @param $id
     * @param array $data
     *
     * @return object|null
     */
    public function updateById($id, array $data);

    /**
     * soft delete user by id
     *
     * @param $id
     *
     * @return object|null
     */
    public function softDeleteById($id);

    /**
     * Reset password by id
     *
     * @param $id
     * @param $data
     *
     * @return object|null
     */
    public function ResetPassword($id, $newPassword);

    /**
     * update firebase token
     *
     * @param $id
     * @param $token
     *
     * @return object|null
     */
    public function updateFcmTokenById($id, $token);
}
