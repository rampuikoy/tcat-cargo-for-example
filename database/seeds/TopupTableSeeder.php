<?php

namespace Database\Seeders;

use App\Models\BankAccount;
use App\Models\User;
use App\Models\Topup;
use Illuminate\Database\Seeder;


class TopupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::whereIn('code', ['SF0002', 'SF0003', 'SF0004','SF0005'])->limit(5)->get();
        $bank_company = BankAccount::whereNull('user_code')->first();
        $users->each(function ($user) use ($bank_company) {
            $topup = Topup::factory()->create([
                    'user_code'                 => $user->code,
                    'system_bank_account_id'    => @$bank_company->id,
                    'system_bank'               => @$bank_company->bank,
                    'system_account'            => @$bank_company->account,
                ]);

            $total = $topup->user->credit->total ?? 0;

            if ($topup->status === 'approved'){
                // withdraw reletion create credit
                $topup->credit()->create([
                    'type'              => 'topup',
                    'user_code'         => $topup->user_code,
                    'amount'            => $topup->amount,
                    'before'            => $total,
                    'after'             => $total + $topup->amount,
                    'created_admin'     => $topup->created_admin,
                    'created_admin_id'  => $topup->created_admin_id,
                    'updated_admin'     => $topup->updated_admin,
                    'updated_admin_id'  => $topup->updated_admin_id,
                    'approved_at'       => $topup->approved_at,
                    'approved_admin'    => $topup->approved_admin,
                    'approved_admin_id' => $topup->approved_admin_id,
                ]);
            }
        });
    }
}
