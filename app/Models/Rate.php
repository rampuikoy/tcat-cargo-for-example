<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rate extends Model
{
  use SoftDeletes;

  protected $table      = 'rates';

  protected $fillable   = [
    'title',
    'status',
    'admin_remark',
    'user_code',

    'kg_car_genaral',
    'kg_car_iso',
    'kg_car_brand',

    'kg_ship_genaral',
    'kg_ship_iso',
    'kg_ship_brand',

    'kg_plane_genaral',
    'kg_plane_iso',
    'kg_plane_brand',

    'cubic_car_genaral',
    'cubic_car_iso',
    'cubic_car_brand',

    'cubic_ship_genaral',
    'cubic_ship_iso',
    'cubic_ship_brand',

    'cubic_plane_genaral',
    'cubic_plane_iso',
    'cubic_plane_brand',

    'created_admin',
    'updated_admin',
    'created_admin_id',
    'updated_admin_id',

    'default_rate',
    'type',
    'admin_id',
  ];

  protected $appends = ['status_text', 'type_text'];

  public static function typeList()
  {
    return [
      'general'             => __('messages.rate_type.general'),
      'user'                => __('messages.rate_type.user'),
      'sale'                => __('messages.rate_type.sale'),
      'sale_only_user'      => __('messages.rate_type.sale_only_user'),
    ];
  }

  public static function statusList()
  {
    return [
      'active'              => __('messages.active'),
      'inactive'            => __('messages.inactive'),
    ];
  }

  public static function conditionList()
  {
    return [
      'only_remark'           => __('messages.conditions.only_remark'),
      'only_has_user_code'    => __('messages.conditions.only_has_user_code'),
      'only_not_user_code'    => __('messages.conditions.only_not_user_code')
    ];
  }


  public function getStatusTextAttribute()
  {
    return collect(self::statusList())->get($this->status);
  }

  public function getTypeTextAttribute()
  {
    return collect(self::typeList())->get($this->type);
  }

  // scope query
  public function scopeSearch($query, $conditions)
  {
    return $query->where(function ($query) use ($conditions) {
      if (@$conditions['id']) {
        $query->where('id', $conditions['id']);
      }
      if (@$conditions['search']) {
        $query->where('title', 'LIKE', "%{$conditions['search']}%")
          ->orWhere('user_code', 'LIKE', "%{$conditions['search']}%")
          ->orWhere('admin_remark', 'LIKE', "%{$conditions['search']}%")
          ->orWhere('type', 'LIKE', "%{$conditions['search']}%");
      }
      if (@$conditions['title']) {
        $query->where('title', 'LIKE', "%{$conditions['title']}%");
      }

      if (@$conditions['admin_remark']) {
        $query->where('admin_remark', 'LIKE', "%{$conditions['admin_remark']}%");
      }

      if (@$conditions['user_code']) {
        $query->where('user_code', 'LIKE', "%{$conditions['user_code']}%");
      }

      if (@$conditions['type']) {
        $query->where('type', '=', $conditions['type']);
      }

      if (@$conditions['status']) {
        $query->where('status', '=', $conditions['status']);
      }

      if (@$conditions['condition']) {
        switch (@$conditions['condition']) {
          case 'only_remark':
            $query->whereNotNull('admin_remark');
            break;
          case 'only_has_user_code':
            $query->whereNotNull('user_code');
            break;
          case 'only_not_user_code':
            $query->whereNull('user_code');
            break;
        }
      }
    });
  }

  // relations
  public function user()
  {
    return $this->belongsTo('App\Models\User', 'user_code', 'code');
  }
}
