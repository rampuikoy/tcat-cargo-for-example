<?php

use App\Models\BankAccount;
use Illuminate\Database\Seeder;

class BankAccountTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(BankAccount::class, 15)->create();
    }
}
