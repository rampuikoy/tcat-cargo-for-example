<?php

namespace App\Providers;

use App\Models\Admin;
use App\Models\Article;
use App\Models\BankAccount;
use App\Models\Bill;
use App\Models\Chat;
use App\Models\Commission;
use App\Models\CommissionRate;
use App\Models\Credit;
use App\Models\ExchangeRate;
use App\Models\NoCode;
use App\Models\PaymentType;
use App\Models\Permission;
use App\Models\ProductType;
use App\Models\Rate; //
use App\Models\Role;
use App\Models\ShippingMethod;
use App\Models\ThaiShippingAreaPrice;
use App\Models\ThaiShippingWeightPrice;
use App\Models\ThaiShippingWeightSizePrice;
use App\Models\Topup;
use App\Models\Tracking;
use App\Models\Truck;
use App\Models\User;
use App\Models\UserAddress;
use App\Models\WarehouseChinese;
use App\Models\Withdraw;
use App\Models\Claims;
use App\Models\Point;
use App\Models\PointWithdraw;
use App\Models\Trip;
use App\Models\Coupon;
use App\Models\PointOrder;
use App\Models\Product;
use App\Models\ThaiShippingMethod;
use App\Policies\AdminPolicy;
use App\Policies\ArticlePolicy;
use App\Policies\BankAccountPolicy;
use App\Policies\BillPolicy;
use App\Policies\ChatPolicy;
use App\Policies\CommissionPolicy;
use App\Policies\CommissionRatePolicy;
use App\Policies\CreditPolicy;
use App\Policies\ExchangeRatePolicy;
use App\Policies\ExtendPolicy;
use App\Policies\NoCodePolicy;
use App\Policies\NotificationPolicy;
use App\Policies\PaymentPolicy;
use App\Policies\PermissionPolicy;
use App\Policies\ProductTypePolicy;
use App\Policies\RatePolicy;
use App\Policies\RolePolicy;
use App\Policies\ShippingMethodPolicy;
use App\Policies\ThaiShippingAreaPricePolicy;
use App\Policies\ThaiShippingWeightPricePolicy;
use App\Policies\ThaiShippingWeightSizePricePolicy;
use App\Policies\TopupPolicy;
use App\Policies\TrackingPolicy;
use App\Policies\TruckPolicy;
use App\Policies\UserAddressPolicy;
use App\Policies\UserPolicy;
use App\Policies\WarehouseChinesePolicy;
use App\Policies\WithdrawPolicy;
use App\Policies\ClaimPolicy;
use App\Policies\PointPolicy;
use App\Policies\PointWithdrawPolicy;
use App\Policies\TripPolicy;
use App\Policies\CouponPolicy;
use App\Policies\PointOrderPolicy;
use App\Policies\ProductPolicy;
use App\Policies\ThaiShippingMethodPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Admin::class => AdminPolicy::class,
        Article::class => ArticlePolicy::class,
        Permission::class => PermissionPolicy::class,
        Role::class => RolePolicy::class,
        BankAccount::class => BankAccountPolicy::class,
        ProductType::class => ProductTypePolicy::class,
        UserAddress::class => UserAddressPolicy::class,
        User::class => UserPolicy::class,
        ShippingMethod::class => ShippingMethodPolicy::class,
        WarehouseChinese::class => WarehouseChinesePolicy::class,
        Rate::class => RatePolicy::class,
        NotificationPolicy::class => NotificationPolicy::class,
        ExchangeRate::class => ExchangeRatePolicy::class,
        ExtendPolicy::class => ExtendPolicy::class,
        Chat::class => ChatPolicy::class,
        Truck::class => TruckPolicy::class,
        Credit::class => CreditPolicy::class,
        Withdraw::class => WithdrawPolicy::class,
        Tracking::class => TrackingPolicy::class,
        Topup::class => TopupPolicy::class,
        Bill::class => BillPolicy::class,
        ThaiShippingAreaPrice::class => ThaiShippingAreaPricePolicy::class,
        ThaiShippingWeightPrice::class => ThaiShippingWeightPricePolicy::class,
        ThaiShippingWeightSizePrice::class => ThaiShippingWeightSizePricePolicy::class,
        PaymentType::class => PaymentPolicy::class,
        CommissionRate::class => CommissionRatePolicy::class,
        Commission::class => CommissionPolicy::class,
        Claims::class => ClaimPolicy::class,
        NoCode::class => NoCodePolicy::class,
        Trip::class => TripPolicy::class,
        Point::class => PointPolicy::class,
        PointWithdraw::class => PointWithdrawPolicy::class,
        Coupon::class => CouponPolicy::class,
        Product::class => ProductPolicy::class,
        PointOrder::class => PointOrderPolicy::class,
        ThaiShippingMethod::class => ThaiShippingMethodPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
