<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BillTracking extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $fillable = [
        	'bill_id',
            'tracking_id',
        ];
}
