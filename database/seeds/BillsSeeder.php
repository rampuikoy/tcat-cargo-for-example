<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Helpers\MoveSqlData;

class BillsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $move_geo_data    = collect(MoveSqlData::insertBills());
        $message          = $move_geo_data->get('message');
        $error            = $move_geo_data->get('error');

        if ($error) {
            $this->command->error($message);
            $this->command->warning('Please insert fresh again');
            return;
        } else {
            $this->command->info($message);
        }
    }
}
