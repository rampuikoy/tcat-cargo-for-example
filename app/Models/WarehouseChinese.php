<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WarehouseChinese extends Model
{
    use HasFactory,SoftDeletes;


    public static $imageFolder  = 'uploads/warehouse/';
    public static $imageWidth   = 300;
    public static $imageHeight  = 300;

    protected $fillable         = [
                                    'image',
                                    'title_th',
                                    'title_en',
                                    'title_cn',
                                    'latitude',
                                    'longitude',
                                    'info_th',
                                    'info_en',
                                    'info_cn',
                                    'remark',
                                    'status',
                                    'created_admin',
                                    'updated_admin',
                                    'created_admin_id',
                                    'updated_admin_id',
                                ];

    protected $appends          = ['status_text','image_url'];


    public static function statusList()
    {
        $array = [
            'active'    => __('messages.active'),
            'inactive'  => __('messages.inactive'),
        ];
        return $array;
    }

    public function getImageUrlAttribute($handle = null)
    {
        $file = $handle ?? $this->image;
        return ($file) ?  env('APP_MEDIA_URL') . '/' . self::$imageFolder . $file : null;
    }

    public function getStatusTextAttribute()
    {
        return collect(self::statusList())->get($this->status);
    }

    public function scopeSearch($query, $conditions)
    {
        return  $query->where(function ($query) use ($conditions) {
            if (@$conditions['id']) {
                $query->where('id', $conditions['id']);
            }
            if (@$conditions['search']) {
                $query->where('title_th', 'LIKE', "%{$conditions['search']}%")
                    ->orWhere('title_en', 'LIKE', "%{$conditions['search']}%")
                    ->orWhere('title_cn', 'LIKE', "%{$conditions['search']}%")
                    ->orWhere('status', 'LIKE', "%{$conditions['search']}%");
            }
            if (@$conditions['title_th']) {
                $query->where('title_th', 'LIKE', "%{$conditions['title_th']}%");
            }
            if (@$conditions['status']) {
                $query->where('status', 'LIKE', "%{$conditions['status']}%");
            }
        });
    }
}
