<?php

namespace App\Http\Controllers\Backend;

use App\Models\WarehouseChinese;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\WarehouseChineseInterface;
use App\Http\Requests\WarehouseChinese\StoreWarehouseChineseRequest;
use App\Http\Requests\WarehouseChinese\UpdateWarehouseChineseRequest;

class WarehouseChineseController extends Controller
{
    protected $warehouseChineseInterface;

    public function __construct(WarehouseChineseInterface $warehouseChineseInterface)
    {
        $this->warehouseChineseInterface = $warehouseChineseInterface;
    }
    public function index(Request $request)
    {
        $this->authorize('index', WarehouseChinese::class);
        $columns        = $request->input('columns', ['*']);
        $warehouseChineses     = $this->warehouseChineseInterface->all($columns);
        return response()->json($warehouseChineses, $warehouseChineses->code);
    }


    public function show(Request $request, $id)
    {
        $this->authorize('show', WarehouseChinese::class);
        $columns        = $request->input('columns', ['*']);
        $warehouseChinese      = $this->warehouseChineseInterface->findById($id, $columns);
        return response()->json($warehouseChinese, $warehouseChinese->code);
    }

    public function search(Request $request)
    {
        $this->authorize('index', WarehouseChinese::class);
        $conditions     = $request->only(['id', 'title_th', 'title_en', 'title_cn', 'status','search']);
        $columns        = $request->input('columns', ['*']);
        $limit          = $request->input('limit', $this->warehouseChineseInterface::DEFAULT_LIMIT);
        $sorting        = $request->input('order_by', $this->warehouseChineseInterface::DEFAULT_SORTING);
        $ascending      = $request->input('ascending') == 1 ? 'asc' : 'desc';

        if ($request->boolean('pagination')) {
            $page         = $request->input('page', 1);
            $warehouseChineses   = $this->warehouseChineseInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset       = $request->input('page', 0);
            $warehouseChineses   = $this->warehouseChineseInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($warehouseChineses, $warehouseChineses->code);
    }

    public function dropdown()
    {
        $dropdown   = $this->warehouseChineseInterface->dropdownList();
        return response()->json($dropdown, $dropdown->code);
    }

    public function store(StoreWarehouseChineseRequest $request)
    {
        $data       = $request->only([
                        'file',
                        'title_th',
                        'title_en',
                        'title_cn',
                        'latitude',
                        'longitude',
                        'info_th',
                        'info_en',
                        'info_cn',
                        'remark',
                        'status',
                    ]);
        $warehouseChinese   = $this->warehouseChineseInterface->store($data);
        return response()->json($warehouseChinese, $warehouseChinese->code);
    }

    public function update(UpdateWarehouseChineseRequest $request, $id)
    {
        $data       = $request->only([
                        'file',
                        'title_th',
                        'title_en',
                        'title_cn',
                        'latitude',
                        'longitude',
                        'info_th',
                        'info_en',
                        'info_cn',
                        'remark',
                        'status',
                    ]);
        $warehouseChinese  = $this->warehouseChineseInterface->updateById($id, $data);
        return response()->json($warehouseChinese, $warehouseChinese->code);
    }

    public function destroy($id)
    {
        $this->authorize('destroy', WarehouseChinese::class);
        $warehouseChinese  = $this->warehouseChineseInterface->softDeleteById($id);
        return response()->json($warehouseChinese, $warehouseChinese->code);
    }
}
