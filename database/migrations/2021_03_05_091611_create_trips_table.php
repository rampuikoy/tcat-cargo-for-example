<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('title')->nullable();
            $table->string('destination')->nullable();
            $table->integer('customer')->unsigned();
            $table->date('depart_date');
            $table->date('return_date');
            $table->text('detail')->nullable();
            $table->float('sub_total', 8, 2);
            $table->string('discount_title')->nullable();
            $table->float('discount', 8, 2)->default(0);
            $table->float('withholding', 8, 2)->default(0);
            $table->float('total', 8, 2);

            $table->text('user_remark')->nullable();
            $table->text('admin_remark')->nullable();
            $table->boolean('status')->default(1);

            $table->string('created_admin', 20);
            $table->string('updated_admin', 20)->nullable();

            $table->string('user_code', 100)->nullable();
            $table->integer('created_admin_id')->unsigned()->nullable();
            $table->integer('updated_admin_id')->unsigned()->nullable();

            $table->foreign('user_code')
                    ->references('code')->on('users')->onDelete('set null');
            $table->foreign('created_admin_id')
                    ->references('id')->on('admins')->onDelete('set null');
            $table->foreign('updated_admin_id')
                    ->references('id')->on('admins')->onDelete('set null');
            $table->enum('bill', ['normal', 'company'])->default('normal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips');
    }
}
