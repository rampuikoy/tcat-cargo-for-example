<?php

namespace App\Http\Controllers\Backend\Notification;

use App\Http\Controllers\Controller;
use App\Http\Requests\NotiQueue\UpdateManyQueueRequest;
use App\Interfaces\SmsQueueInterface;
use App\Policies\NotificationPolicy;
use Illuminate\Http\Request;

class SmsQueueController extends Controller
{

    protected $smsQueueInterface;

    public function __construct(SmsQueueInterface $smsQueueInterface)
    {
        $this->smsQueueInterface = $smsQueueInterface;
    }

    public function search(Request $request)
    {
        $this->authorize('index', NotificationPolicy::class);
        $conditions     = $request->only(['id', 'user_code', 'status', 'msisdn', 'message', 'send', 'date_start', 'date_end']);
        $columns        = $request->input('columns', ['*']);
        $limit          = $request->input('limit', $this->smsQueueInterface::DEFAULT_LIMIT);
        $sorting        = $request->input('order_by', $this->smsQueueInterface::DEFAULT_SORTING);
        $ascending      = $request->input('ascending') == 1 ? 'asc' : 'desc';
        $offset         = $request->input('page', 0);

        if ($request->boolean('pagination')) {
            $page = $request->input('page', 1);
            $smsQueue = $this->smsQueueInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset = $request->input('page', 0);
            $smsQueue = $this->smsQueueInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }

        return response()->json($smsQueue, $smsQueue->code);
    }

    public function dropdown()
    {
        $dropdown = $this->smsQueueInterface->dropdownList();
        return response()->json($dropdown, $dropdown->code);
    }

    public function updateManyQueue(UpdateManyQueueRequest $request)
    {
        $data       = $request->input('ids', []);
        $smsQueue   = $this->smsQueueInterface->updateManyQueue($data);
        return response()->json($smsQueue, $smsQueue->code);
    }
}
