<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class TopupPolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.topup.index');
    }
    public function show(Admin $admin)
    {
        return $admin->hasAnyPermission(['backend.topup.show', 'backend.topup.update']);
    }

    public function store(Admin $admin)
    {
        return $admin->can('backend.topup.store');
    }
    public function update(Admin $admin)
    {
        return $admin->can('backend.topup.update');
    }

    public function destroy(Admin $admin)
    {
        return $admin->can('backend.topup.destroy');
    }
    public function approve(Admin $admin)
    {
        return $admin->can('backend.topup.approve');
    }
}
