<?php

namespace App\Repositories;

use App\Interfaces\PointInterface;
use App\Models\Point;
use App\Traits\CacheTrait;
use Illuminate\Support\Facades\Cache;
use App\Traits\CoreResponseTrait;

class PointRepository implements PointInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const MINUTE_CACHE          = 1;

    public function __construct(Point $point)
    {
        $this->point        = $point;
    }

    public function all(array $columns = ['*'], $sorting = PointInterface::DEFAULT_SORTING, $ascending = PointInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $points     = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
                        return $this->point::select($columns)->orderBy($sorting, $ascending)->get();
                    });
        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.point')]), $points);
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = PointInterface::DEFAULT_LIMIT, $sorting = PointInterface::DEFAULT_SORTING, $ascending = PointInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $points     = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
                        return $this->point::select($columns)
                            ->search($conditions)
                            ->orderBy($sorting, $ascending)
                            ->skip($offset)
                            ->limit($limit)
                            ->get();
                    });
        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.point')]), $points);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = PointInterface::DEFAULT_LIMIT, $sorting = PointInterface::DEFAULT_SORTING, $ascending = PointInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $points     = Cache::tags([$this->getClassPrefix(), $this->getPaginationWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
                        $model = $this->point::select($columns)
                            ->search($conditions)
                            ->orderBy($sorting, $ascending);
                        if ($page == 1) {
                            return $model->paginate($limit);
                        } else {
                            return $model->skip($limit * ($page - 1))->paginate($limit);
                        }
                    });
        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.point')]), $points);
    }

    public function dropdownList()
    {
        $key        = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $dropdown   = Cache::remember($key, $time, function () {
                        return [
                            'type' => $this->point::typeList()
                        ];
                    });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function clearCacheTags()
    {
        Cache::tags([
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix()
        ])->flush();
    }
}
