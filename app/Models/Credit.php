<?php

namespace App\Models;

use App\Helpers\ScopeQueries;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Credit extends Model
{
    use HasFactory;

    protected $table = 'credits';

    protected $fillable = [
        'approved_at',
        'creditable_type',
        'creditable_id',
        'user_code',
        'amount',
        'before',
        'after',
        'type',
        'created_admin',
        'updated_admin',
        'approved_admin',
        'created_admin_id',
        'updated_admin_id',
        'approved_admin_id',
    ];

    protected $appends = ['description', 'current_type', 'bill_no'];

    public static function typeList()
    {
        $array = [
            'topup'     => 'เติมเงิน',
            'withdraw'  => 'ถอนเงิน',
        ];
        return $array;
    }

    public static function descriptionList()
    {
        $array = [
            'bill_withdraw'             => 'ชำระบิล',
            'bill_topup'                => 'คืนเงิน',
            'bill_refund_addon'         => 'คืนเงิน',
            'money_transfer'            => 'ชำระบิล',
            'money_transfer_refund'     => 'คืนเงิน',
            'withdraw_cancel'           => 'คืนเงิน',
            'withdraw_success'          => 'ถอนเครดิต',
            'money_receive_success'     => 'รับเงินจีน',
            'App\Models\Trip'           => 'ชำระบิล',
            'App\Models\Topup'          => 'เติมเครดิต',
            'App\Models\Zmemo'          => 'ยอดจาก Zmemo',
            'App\Models\PointOrder'     => 'แลกสินค้า',
            'App\Models\MoneyTransfer'  => 'ชำระบิลโอนเงินจีน',
        ];
        return $array;
    }

    public static function conditionList()
    {
        $array = [
            'transport_bill'            => __('messages.conditions.only_transport_bill'),
            'topup_credit'              => __('messages.conditions.only_topup_credit'),
            'withdraw_credit'           => __('messages.conditions.only_withdraw_credit'),
            'receive_chinese_money'     => __('messages.conditions.only_receive_chinese_money'),
            'transfer_money_to_china'   => __('messages.conditions.only_transfer_money_to_china'),
            'china_trip_tour'           => __('messages.conditions.only_china_trip_tour'),
            'peak_from_zmemo'           => __('messages.conditions.only_peak_from_zmemo'),
            'exchange_products'         => __('messages.conditions.only_exchange_products'),
        ];
        return $array;
    }

    public function getCurrentTypeAttribute()
    {
        $type = $this->typeList();
        return $type[$this->type] ?? [];
    }

    public function getBillNoAttribute()
    {
        if (!$this->id) {
            return null;
        }
        $instance = new $this->creditable_type;
        return $instance::PREFIX . str_pad($this->creditable_id, 6, "0", STR_PAD_LEFT);
    }

    public function getDescriptionAttribute()
    {
        if (!$this->creditable_type) {
            return null;
        }
        $description        = null;
        $description_lists  = $this->descriptionList();

        switch ($this->creditable_type) {
            case 'App\Models\Bill':
                if ($this->type == 'topup') {
                    $description    = ($this->created_admin == 'system')
                                        ? $description_lists['bill_topup']
                                        : $description_lists['bill_refund_addon'];
                } else if ($this->type == 'withdraw') {
                    $description    = $description_lists['bill_withdraw'];
                }
                break;
            case 'App\Models\MoneyTransfer':
                $description    = ($this->type == 'topup')
                                    ? $description_lists['money_transfer_refund']
                                    : $description_lists['money_transfer'];
                break;
            case 'App\Models\Withdraw':
                $description    = ($this->type == 'topup')
                                    ? $description_lists['withdraw_cancel']
                                    : $description_lists['withdraw_success'];
                break;
            case 'App\Models\MoneyReceive':
                $description    = $description_lists['money_receive_success'];
                break;
            default:
                $description    = $description_lists[$this->creditable_type];
                break;
        }
        return $description;
    }

    // relation

    // scope query
    public function scopeSearch($query, $conditions)
    {
        return $query->where(function ($query) use ($conditions) {
            if (@$conditions['id']) {
                $query->where('id', $conditions['id']);
            }

            if (@$conditions['user_code']) {
                $query->where('user_code', 'LIKE', "%{$conditions['user_code']}%");
            }

            if (@$conditions['baht']) {
                $query->where('amount', 'LIKE', "%{$conditions['baht']}%")
                    ->orWhere('before', 'LIKE', "%{$conditions['baht']}%")
                    ->orWhere('after', 'LIKE', "%{$conditions['baht']}%");
            }

            if (@$conditions['type']) {
                $query->where('type', '=', $conditions['type']);
            }

            $date_type = (@$conditions['date_type']) ? $conditions['date_type'] : 'created_at';
            if (@$conditions['date_start'] && @$conditions['date_end']) {
                ScopeQueries::scopeDateRange($query, $date_type, $conditions['date_start'], $conditions['date_end']);
            } else if (@$conditions['date_start']) {
                ScopeQueries::scopeDateStart($query, 'created_at', $conditions['date_start']);
            } else if (@$conditions['date_end']) {
                ScopeQueries::scopeDateEnd($query, 'created_at', $conditions['date_end']);
            }

            if (@$conditions['condition']) {
                switch (@$conditions['condition']) {
                    case 'admin_remark':
                        $query->whereNotNull('admin_remark');
                        break;
                    case 'user_remark':
                        $query->whereNotNull('user_remark');
                        break;
                    case 'has_image':
                        $query->whereNotNull('has_image');
                        break;
                }
            }
        });
    }
}
