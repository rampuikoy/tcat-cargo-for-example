<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Interfaces\PointInterface;
use App\Models\Point;

class PointController extends Controller
{
    protected $pointInterface;

    public function __construct(PointInterface $pointInterface)
    {
        $this->pointInterface = $pointInterface;
    }
    public function index(Request $request)
    {
        $this->authorize('index', Point::class);
        $columns    = $request->input('columns', ['*']);
        $point      = $this->pointInterface->all($columns);
        return response()->json($point, $point->code);
    }

    public function search(Request $request)
    {
        $this->authorize('index', Point::class);
        $conditions   = $request->only(['user_code', 'amount', 'type', 'date_start', 'date_end']);
        $columns      = $request->input('columns', ['*']);
        $limit        = $request->input('limit', $this->pointInterface::DEFAULT_LIMIT);
        $sorting      = $request->input('order_by', $this->pointInterface::DEFAULT_SORTING);
        $ascending    = $request->input('ascending') == 1 ? 'asc' : 'desc';
        if ($request->boolean('pagination')) {
            $page       = $request->input('page', 1);
            $point      = $this->pointInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset     = $request->input('page', 0);
            $point  = $this->pointInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($point, $point->code);
    }

    public function dropdown()
    {
        $dropdown = $this->pointInterface->dropdownList();
        return response()->json($dropdown, $dropdown->code);
    }
}
