<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BankMap extends Model
{
    use HasFactory;
    protected $table = 'bank_map';
    public $timestamps = false;
}
