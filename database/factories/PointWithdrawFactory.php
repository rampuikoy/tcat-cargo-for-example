<?php

namespace Database\Factories;

use App\Models\Admin;
use App\Models\PointWithdraw;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

class PointWithdrawFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PointWithdraw::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $admin                  = Admin::select('id', 'username', 'role')->whereRole('SuperAdmin')->first();
        $suf_type               = ['topup', 'withdraw'];
        $suf_user               = ['SF0002', 'SF0003', 'SF0004', 'SF0005'];

        return [
            //
            'amount'            => $this->faker->numberBetween(100, 1000),
            'type'              => Arr::random($suf_type),
            'user_code'         => Arr::random($suf_user),
            'remark'            => 'REMARK : ' . $this->faker->sentence(10),

            'created_admin'     => $admin->username,
            'created_admin_id'  => $admin->id,
            'updated_admin'     => $admin->username,
            'updated_admin_id'  => $admin->id,

            'status'            => $this->faker->numberBetween(1, 3),
        ];
    }
}
