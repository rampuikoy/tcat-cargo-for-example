<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admins', function (Blueprint $table) {
            // tcatcargo columns
            $table->string('username', 64)->unique()->nullable();
            $table->string('tel', 20)->nullable();
            $table->enum('department', ['tdar','cargo','taobao','tmall','alibaba','express','china'])->default('tdar');
            $table->integer('droppoint_id')->nullable();
            $table->enum('branch', ['headquarter', 'branch'])->default('headquarter');
            $table->enum('type', ['staff', 'admin', 'accountant', 'warehouse', 'sale', 'driver'])->default('staff');
            $table->enum('status', ['active', 'inactive'])->default('active');
            $table->enum('regex_user', ['active', 'inactive'])->default('inactive');
            $table->string('pattern_user', 64)->nullable();
            $table->text('remark')->nullable();
            $table->integer('created_admin_id')->unsigned()->nullable();
            $table->integer('updated_admin_id')->unsigned()->nullable();
            $table->string('created_admin', 64)->nullable();
            $table->string('updated_admin', 64)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admins', function (Blueprint $table) {
            $table->dropColumn('username');
            $table->dropColumn('tel');
            $table->dropColumn('department');
            $table->dropColumn('droppoint_id');
            $table->dropColumn('branch');
            $table->dropColumn('type');
            $table->dropColumn('status');
            $table->dropColumn('regex_user');
            $table->dropColumn('pattern_user');
            $table->dropColumn('remark');
            $table->dropColumn('created_admin_id');
            $table->dropColumn('updated_admin_id');
            $table->dropColumn('created_admin');
            $table->dropColumn('updated_admin');
        });
    }
}
