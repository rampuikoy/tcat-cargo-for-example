<?php

namespace Database\Factories;
use App\Models\Admin;
use App\Models\TopupAutopay;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

class TopupAutopayFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TopupAutopay::class;

    const SUF_TYPE = ['App\Models\Bill', 'App\Models\Trip'];

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $admin      = Admin::whereRole('SuperAdmin')->first();
        $topup_autopay = [
            'payable_type'  => Arr::random(self::SUF_TYPE),
            'payable_id'    => $this->faker->numberBetween($min = 1000, $max = 9000),

            'created_admin'     => $admin->name,
            'updated_admin'     => $admin->name,
            'created_admin_id'  => $admin->id,
            'updated_admin_id'  => $admin->id,
        ];

        return $topup_autopay;
    }
}
