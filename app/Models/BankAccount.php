<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BankAccount extends Model
{
    use softDeletes;

    protected $table = 'bank_accounts';

    protected $fillable = [
        'title',
        'account',
        'branch',
        'bank',
        'status',
        'user_code',
        'user_remark',
        'admin_remark',
        'created_admin',
        'updated_admin',
        'created_admin_id',
        'updated_admin_id',
    ];

    protected $appends = ['status_text', 'bank_name'];

    public static function bankList()
    {
        return [
            'KBANK' => 'ธ.กสิกรไทย',
            'SCB' => 'ธ.ไทยพานิชย์',
            'KTB' => 'ธ.กรุงไทย',
            'BBL' => 'ธ.กรุงเทพ',
            'BAY' => 'ธ.กรุงศรีฯ',
            'TMB' => 'ธ.ทหารไทย',
            'NBANK' => 'ธ.ธนชาติ',
            'GSB' => 'ธ.ออมสิน',
            'GHB' => 'ธ.อาคารสงเคราะห์',
            'SCIB' => 'ธ.นครหลวงไทย',
            'CITI' => 'ธ.ซิตี้แบงค์',
        ];
    }

    public static function statusList()
    {
        $array = [
            'active' => __('messages.active'),
            'inactive' => __('messages.inactive'),
        ];
        return $array;
    }

    public function getStatusTextAttribute()
    {
        return collect(self::statusList())->get($this->status);
    }

    public function getBankNameAttribute()
    {
        return collect(self::bankList())->get($this->bank);
    }

    // releations
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_code', 'code');
    }

    // scoped query
    public function scopeSearch($query, $conditions)
    {
        return $query->where(function ($query) use ($conditions) {
            
            if (@$conditions['id']) {
                $query->where('id', $conditions['id']);
            }

            if (@$conditions['search']) {
                $query->where('title', 'LIKE', "%{$conditions['search']}%")
                    ->orWhere('user_code', 'LIKE', "%{$conditions['search']}%")
                    ->orWhere('account', 'LIKE', "%{$conditions['search']}%")
                    ->orWhere('branch', 'LIKE', "%{$conditions['search']}%")
                    ->orWhere('user_remark', 'LIKE', "%{$conditions['search']}%")
                    ->orWhere('admin_remark', 'LIKE', "%{$conditions['search']}%")
                    ->orWhere('bank', 'LIKE', "%{$conditions['search']}%");
            }
            if (@$conditions['title']) {
                $query->where('title', 'LIKE', "%{$conditions['title']}%");
            }
            if (@$conditions['account']) {
                $query->where('account', 'LIKE', "%{$conditions['account']}%");
            }
            if (@$conditions['user_code']) {
                $query->where('user_code', 'LIKE', "%{$conditions['user_code']}%");
            }
            if (@$conditions['bank']) {
                $query->where('bank', $conditions['bank']);
            }
            if (@$conditions['branch']) {
                $query->where('branch', 'LIKE', "%{$conditions['branch']}%");
            }
            if (@$conditions['status']) {
                $query->where('status', '=', $conditions['status']);
            }
            if (@$conditions['user_remark']) {
                $query->where('user_remark', 'LIKE', "%{$conditions['user_remark']}%");
            }
            if (@$conditions['admin_remark']) {
                $query->where('admin_remark', 'LIKE', "%{$conditions['admin_remark']}%");
            }

        });
    }
}
