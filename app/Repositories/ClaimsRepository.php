<?php

namespace App\Repositories;

use App\Helpers\Util;
use App\Interfaces\ClaimsInterface;
use App\Models\Claims;
use App\Models\ClaimItems;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;


class ClaimsRepository implements ClaimsInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const MINUTE_CACHE          = 1;
    const TABLE_RELETIONS       = ['images', 'items'];

    public function __construct(Claims $claims, ClaimItems $claimItems)
    {
        $this->claims       = $claims;
        $this->claimItems = $claimItems;
    }

    public function all(array $columns = ['*'], $sorting = ClaimsInterface::DEFAULT_SORTING, $ascending = ClaimsInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $claimss     = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])
            ->remember($key, $time, function () use ($columns, $sorting, $ascending) {
                return $this->claims::select($columns)->orderBy($sorting, $ascending)->get();
            });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.claims')]), $claimss);
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = claimsInterface::DEFAULT_LIMIT, $sorting = claimsInterface::DEFAULT_SORTING, $ascending = claimsInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $claimss     = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
            return $this->claims::select($columns)
                ->search($conditions)
                ->orderBy($sorting, $ascending)
                ->skip($offset)
                ->limit($limit)
                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.claims')]), $claimss);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = claimsInterface::DEFAULT_LIMIT, $sorting = claimsInterface::DEFAULT_SORTING, $ascending = claimsInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $claimss     = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
            $model = $this->claims::select($columns)
                ->search($conditions)
                ->orderBy($sorting, $ascending);

            if ($page == 1) {
                return $model->paginate($limit);
            } else {
                return $model->skip($limit * ($page - 1))->paginate($limit);
            }
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.claims')]), $claimss);
    }

    public function findById($id, array $columns = ['*'])
    {
        $claims = $this->findByIdMethod($id, $columns);

        if (!$claims) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.claims'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.claims')]), $claims);
    }

    public function dropdownList()
    {
        $key        = $this->getCacheKey(null, $this->getDropDownWithClassPrefix());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $dropdown   = Cache::remember($key, $time, function () {
            return [
                'shipping' => $this->claims::shippingList(),
                'type' => $this->claims::typeList(),
                'status' => $this->claims::statusList(),
                'status_item' => $this->claimItems::statusList(),
            ];
        });

        return $this->coreResponse(200, __('messages.fetch_data', ['data' => __('messages.dropdown')]), $dropdown);
    }

    public function store(array $data)
    {
        // stranr transction
        DB::beginTransaction();
        try {
            $claims = $this->claims::create($data);
            foreach ($data['items'] as $item) {
                $claimItem = array_merge($item, ['claim_id' => $claims->id]);
                app('App\Repositories\ClaimItemsRepository')->store($claimItem);
            }
            $this->clearCacheTags();
            DB::commit();
            return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.claims')]), $claims);
        } catch (\Throwable $th) {
            DB::rollBack();
            return $this->coreResponse(500, $th->getMessage() ?? __('messages.not_update_data', ['data' => __('messages.bill')]), $th->getTrace());
        }
    }

    public function approveById($id, array $data)
    {
        $claims = $this->findByIdMethod($id);
        $admin  = auth()->user();
        if (!$claims) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.claims'), 'id' => $id]));
        }
        if ($claims->status == 'waiting') {
            $claims->status = 'approved';
            $claims->approved_admin = $admin->username;
            $claims->approved_admin_id = $admin->id;
            $claims->claimed_admin = $admin->username;
            $claims->claimed_admin_id = $admin->id;
            $claims->approved_at = date('Y-m-d H:i:s');
            $claims->update($data);
        }
        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.claims')]), $claims);
    }

    public function cancelById($id, array $data)
    {
        $claims = $this->findByIdMethod($id);

        if (!$claims) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.claims'), 'id' => $id]));
        }
        if ($claims->status == 'waiting') {
            $claims->status = 'cancel';
            $claims->remark = Util::concatField($claims->remark, !empty($data['remark']) ?  $data['remark'] : null);
            $claims->update($data);
        }
        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();
        $this->clearCacheTags();

        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.claims')]), $claims);
    }

    private function clearCacheTags()
    {
        Cache::tags([
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix()
        ])
            ->flush();
    }

    public function findByIdMethod($id, array $columns = ['*'])
    {
        $key        = $this->getCacheKey(null, $id);
        $time       = $this->getTime(self::MINUTE_CACHE);

        $claims      = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($id)
        ])
            ->remember($key, $time, function () use ($id, $columns) {
                return $this->claims::with(self::TABLE_RELETIONS)->select($columns)->whereId($id)->first();
            });

        return $claims;
    }

    public function clearCacheTagById($id)
    {
        Cache::tags([$this->getFindByIdWithClassPrefix($id)])->flush();
    }
}
