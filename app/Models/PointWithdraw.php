<?php

namespace App\Models;

use App\Helpers\ScopeQueries;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PointWithdraw extends Model
{
    use HasFactory;

    const PREFIX = 'WP-';

    protected $fillable = [
        'status',
        'user_code',
        'amount',
        'type',
        'remark',
    ];

    protected $appends = ['no', 'type_text', 'status_text'];

    public static function typeList()
    {
        $array = [
            'topup'      => __('messages.points.type.topup'),
            'withdraw'   => __('messages.points.type.withdraw'),
        ];
        return $array;
    }

    public static function statusList()
    {
        $array = [
            [
                'id' => 1,
                'name' => __('messages.point_withdraws.status.1'),
            ],
            [
                'id' => 2,
                'name' => __('messages.point_withdraws.status.2'),
            ],
            [
                'id' => 3,
                'name' => __('messages.point_withdraws.status.3'),
            ],
            [
                'id' => 4,
                'name' => __('messages.point_withdraws.status.4'),
            ],
        ];
        return $array;
    }

    public function getNoAttribute()
    {
        return ($this->id) ? self::PREFIX . str_pad($this->id, 6, "0", STR_PAD_LEFT) : null;
    }

    public function getTypeTextAttribute()
    {
        return collect(self::typeList())->get($this->type);
    }

    public function getStatusTextAttribute()
    {
        $status = collect(self::statusList())->firstWhere('id', $this->status);
        return $status['name'] ?? null;
    }

    public function point()
    {
        return $this->morphMany('App\Models\Point', 'pointable');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_code', 'code');
    }

    // scope sql query
    public function scopeSearch($query, $conditions)
    {
        return $query->where(function ($query) use ($conditions) {
            ScopeQueries::scopeUserCodeBySale($query);

            if (@$conditions['type']) {
                $query->whereType($conditions['type']);
            }
            if (@$conditions['status']) {
                $query->whereStatus($conditions['status']);
            }
            if (@$conditions['user_code']) {
                $query->where('user_code', 'like', '%' . $conditions['user_code'] . '%');
            }
            if (@$conditions['amount']) {
                $query->whereAmount($conditions['amount']);
            }

            $time_format = 'created_at';
            if (@$conditions['date_start'] && @$conditions['date_end']) {
                ScopeQueries::scopeDateRange($query, $time_format, $conditions['date_start'], $conditions['date_end']);
            } else if (@$conditions['date_start']) {
                ScopeQueries::scopeDateStart($query, $time_format, $conditions['date_start']);
            } else if (@$conditions['date_end']) {
                ScopeQueries::scopeDateEnd($query, $time_format, $conditions['date_end']);
            }
        })->whereRaw('date(created_at) >= "2020-01-01"');
    }
}
