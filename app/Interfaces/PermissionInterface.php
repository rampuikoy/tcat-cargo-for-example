<?php

namespace App\Interfaces;

interface PermissionInterface
{
    const DEFAULT_LIMIT = 25;
    const DEFAULT_SORTING = 'id';
    const DEFAULT_ASCENDING = 'desc';

    /**
     * fetch all permission
     *
     * @param array $columns
     *
     * @return \Illuminate\Support\Collection
     */
    public function all(array $columns = ['*'], $sorting = PermissionInterface::DEFAULT_SORTING, $ascending = PermissionInterface::DEFAULT_ASCENDING);

        /**
     * list permissions
     *
     * @param array $conditions
     * @param array $columns
     * @param int $offset
     * @param int $limit
     *
     * @return \Illuminate\Support\Collection
     */
    public function fetchListByFields(array $conditions, array $columns = ['*'], $offset = 0, $limit = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * pagination permissions
     *
     * @param array $conditions
     * @param array $columns
     * @param int $page
     * @param int $perPage
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $perPage = self::DEFAULT_LIMIT, $sorting = self::DEFAULT_SORTING, $ascending = self::DEFAULT_ASCENDING);

    /**
     * find permission by id
     *
     * @param $id
     * @param array $columns
     *
     * @return object|null
     */
    public function findById($id, array $columns = ['*']);

    /**
     * get permission dropdown list
     *
     * @return object
     */
    public function dropdownList();

    /**
     * create new permission
     *
     * @param array $data
     *
     * @return object|null
     */
    public function store(array $data);

    /**
     * update permission by id
     *
     * @param $id
     * @param array $data
     *
     * @return object|null
     */
    public function updateById($id, array $data);

    /**
     * delete permission by id
     *
     * @param $id
     *
     * @return object|null
     */
    public function deleteById($id);

    /**
     * delete many permission by id
     *
     * @param $id
     *
     * @return object|null
     */
    public function deleteMany(array $id);

}
