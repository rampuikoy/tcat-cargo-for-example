<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Carbon\Carbon;
use App\Models\Article;
use Faker\Generator as Faker;
use Illuminate\Support\Arr;

$suftype = [
    'article',
    'news',
    'promotion',
];
$factory->define(Article::class, function (Faker $faker)use ($suftype) {
    return [
        'type' =>  Arr::random($suftype),
        'title' =>  $faker->text($maxNbChars = 30),
        'content' =>  $faker->text($maxNbChars = 300),
        'views' => random_int(1, 20),
        'published_at' => Carbon::now(),
        'created_admin_id'=>  random_int(1, 200),
        'created_admin'=>  'Admin'
    ];
});
