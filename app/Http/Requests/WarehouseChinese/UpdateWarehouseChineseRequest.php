<?php

namespace App\Http\Requests\WarehouseChinese;

use App\Http\Requests\FormRequest;
use App\Rules\Base64Image;
use App\Rules\Latitude;
use App\Rules\Longitude;

class UpdateWarehouseChineseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.warehouse-chinese.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file'          => ['sometimes','nullable', new Base64Image],
            'title_th'      => 'required|string',
            'title_en'      => 'required|string',
            'title_cn'      => 'required|string',
            'latitude'      => ['required', new Latitude],
            'longitude'     => ['required', new Longitude],
            'info_th'       => 'required|string',
            'info_en'       => 'required|string',
            'info_cn'       => 'required|string',
            'status'        => 'required|in:active,inactive',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'title_th'      => __('form.warehouse_chinese.title_th'),
            'title_en'      => __('form.warehouse_chinese.title_en'),
            'title_cn'      => __('form.warehouse_chinese.title_cn'),
            'info_th'       => __('form.warehouse_chinese.info_th'),
            'info_en'       => __('form.warehouse_chinese.info_en'),
            'info_cn'       => __('form.warehouse_chinese.info_cn'),
            'status'        => __('form.warehouse_chinese.status'),
            'latitude'      => __('form.warehouse_chinese.latitude'),
            'longitude'     => __('form.warehouse_chinese.longitude'),
            'file'          => __('form.warehouse_chinese.file'),
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
