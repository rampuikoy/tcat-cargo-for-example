<?php

namespace App\Policies;

use App\Models\Admin;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductTypePolicy
{
    use HandlesAuthorization;

    public function index(Admin $admin)
    {
        return $admin->can('backend.product-type.index');
    }
    
    public function show(Admin $admin)
    {
        return $admin->hasAnyPermission(['backend.product-type.show', 'backend.product-type.update']);
    }

    public function store(Admin $admin)
    {
        return $admin->can('backend.product-type.store');
    }

    public function update(Admin $admin)
    {
        return $admin->can('backend.product-type.update');
    }

    public function destroy(Admin $admin)
    {
        return $admin->can('backend.product-type.destroy');
    }

}
