<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'          => 'ต้องยอมรับ :attribute',
    'active_url'        => ':attribute ไม่ใช่ URL ที่ถูกต้อง',
    'after'             => ':attribute ต้องเป็นวันที่หลังจาก: วันที่',
    'after_or_equal'    => ':attribute ต้องเป็นวันที่หลังหรือเท่ากับ: วันที่',
    'alpha'             => ':attribute สามารถมีได้เฉพาะตัวอักษร',
    'alpha_dash'        => ':attribute ต้องประกอบด้วยตัวอักษรตัวเลขขีดกลางและขีดล่างเท่านั้น',
    'alpha_num'         => ':attribute ต้องประกอบด้วยตัวอักษรและตัวเลขเท่านั้น',
    'array'             => ':attribute ต้องเป็นอาร์เรย์',
    'before'            => ':attribute ต้องเป็นวันที่ก่อน: วันที่',
    'before_or_equal'   => ':attribute ต้องเป็นวันที่ก่อนหน้าหรือเท่ากับวันที่',
    'between'           => [
                            'numeric'   => ':attribute ต้องอยู่ระหว่าง :min และ :max',
                            'file'      => ':attribute ต้องอยู่ระหว่าง :min และ :max กิโลไบต์',
                            'string'    => ':attribute ต้องอยู่ระหว่าง :min และ :max ตัวอักษร',
                            'array'     => ':attribute ต้องมีจำนวนระหว่าง :min และ :max รายการ',
                        ],
    'boolean'           => 'ฟิลด์ :attribute ต้องเป็นจริงหรือเท็จ',
    'confirmed'         => ':attribute การยืนยันไม่ตรงกัน',
    'date'              => ':attribute ไม่ใช่วันที่ที่ถูกต้อง',
    'date_equals'       => ':attribute ต้องเป็นวันที่เท่ากับวันที่',
    'date_format'       => ':attribute รูปแบบวันที่ไม่ตรงกับ :format',
    'different'         => ':attribute และ :other ต้องแตกต่างกัน',
    'digits'            => ':attribute ต้องเป็น :digits หลัก',
    'digits_between'    => ':attribute ต้องอยู่ระหว่าง :min และ :max หลัก',
    'dimensions'        => ':attribute มีขนาดของภาพที่ไม่ถูกต้อง',
    'distinct'          => 'ฟิลด์ :attribute มีค่าที่ซ้ำกัน',
    'email'             => ':attribute ต้องเป็นที่อยู่อีเมลที่ถูกต้อง',
    'exists'            => ':attribute ไม่มีค่านี้ในระบบ',
    'file'              => ':attribute ต้องเป็นไฟล์',
    'filled'            => ':attribute ต้องมีค่า',

    'gt'                => [
                            'numeric'    => ':attribute ต้องมากกว่า :value',
                            'file'       => ':attribute ต้องมากกว่า :value กิโลไบต์',
                            'string'     => ':attribute ต้องมากกว่า :value ตัวอักษร',
                            'array'      => ':attribute ต้องมากกว่า :value รายการ',
                        ],

    'gte'               => [
                            'numeric'    => ':attribute ต้องมากกว่าหรือเท่ากับ :value',
                            'file'       => ':attribute ต้องมากกว่าหรือเท่ากับ :value กิโลไบต์',
                            'string'     => ':attribute ต้องมากกว่าหรือเท่ากับ :value ตัวอักษร',
                            'array'      => ':attribute ต้องมากกว่าหรือเท่ากับ :value รายการ',
                        ],
                        
    'image'             => ':attribute ต้องเป็นรูปภาพ',
    'in'                => ':attribute ที่เลือกไม่ถูกต้อง.',
    'in_array'          => ':attribute ไม่มีค่าที่ตรงกับ :other',
    'integer'           => ':attribute ต้องเป็นจำนวนเต็ม',
    'ip'                => ':attribute ต้องเป็นที่อยู่ IP ที่ถูกต้อง',
    'ipv4'              => ':attribute ต้องเป็นที่อยู่ IPv4 ที่ถูกต้อง',
    'ipv6'              => ':attribute ต้องเป็นที่อยู่ IPv6 ที่ถูกต้อง',
    'json'              => ':attribute ต้องเป็นรูปแบบ JSON เท่านั้น',

    'lt' => [
        'numeric'       => ':attribute ต้องน้อยกว่า :value',
        'file'          => ':attribute ต้องน้อยกว่า :value กิโลไบต์',
        'string'        => ':attribute ต้องน้อยกว่า :value ตัวอักษร',
        'array'         => ':attribute ต้องน้อยกว่า :value รายการ',
    ],
    'lte' => [
        'numeric'       => ':attribute ต้องน้อยกว่าหรือเท่ากับ :value',
        'file'          => ':attribute ต้องน้อยกว่าหรือเท่ากับ :value กิโลไบต์',
        'string'        => ':attribute ต้องน้อยกว่าหรือเท่ากับ :value ตัวอักษร',
        'array'         => ':attribute ต้องน้อยกว่าหรือเท่ากับ :value รายการ',
    ],
    'max'               => [
                            'numeric'   => ':attribute ต้องมีค่าไม่มากกว่า :max',
                            'file'      => ':attribute ต้องมีค่าไม่มากกว่า :max กิโลไบต์',
                            'string'    => ':attribute ต้องมีค่าไม่มากกว่า :max ตัวอักษร',
                            'array'     => ':attribute ต้องมีค่าไม่มากกว่า :max รายการ',
                        ],

    'mimes'             => 'The :attribute must be a file of type: :values.',
    'mimetypes'         => 'The :attribute must be a file of type: :values.',

    'min'               => [
                            'numeric'   => ':attribute ต้องมีค่าอย่างน้อย :min',
                            'file'      => ':attribute ต้องมีค่าอย่างน้อย :min กิโลไบต์',
                            'string'    => ':attribute ต้องมีค่าอย่างน้อย :min ตัวอักษร',
                            'array'     => ':attribute ต้องมีค่าอย่างน้อย :min รายการ',
                          ],

    'not_in'                => ':attribute ไม่ถูกต้อง',
    'not_regex'             => ':attribute รูปแบบไม่ถูกต้อง',
    'numeric'               => ':attribute ต้องเป็นตัวเลข',
    'present'               => ':attribute ต้องมีฟิลด์',
    'regex'                 => ':attribute รูปแบบไม่ถูกต้อง',
    'required'              => ':attribute จำเป็นต้องระบุ',
    'required_if'           => ':attribute จำเป็นต้องระบุเมื่อ :other เท่ากับ :value',
    'required_unless'       => ':attribute จำเป็นต้องละเว้นเมื่อ :other เท่ากับ :values',
    'required_with'         => ':attribute จำเป็นต้องระบุเมื่อ :values มีค่า',
    'required_with_all'     => ':attribute จำเป็นต้องระบุเมื่อ :values มีค่า',
    'required_without'      => ':attribute จำเป็นต้องละเว้นเมื่อ :values มีค่า',
    'required_without_all'  => ':attribute จำเป็นต้องละเว้นเมื่อ :values มีค่า',

    'same'                  => ':attribute และ :other ต้องตรงกัน',
    'size'                  => [
                                'numeric'   => ':attribute ต้องมีขนาด :size',
                                'file'      => ':attribute ต้องมีขนาด :size กิโลไบต์',
                                'string'    => ':attribute ต้องมีขนาด :size อักษร',
                                'array'     => ':attribute ต้องมีขนาด :size รายการ',
                            ],

    'starts_with'           => ':attribute ต้องขึ้นต้นด้วยค่าใดค่าหนึ่งต่อไปนี้: :values',
    'string'                => ':attribute ต้องมีค่าเป็นตัวอักษร',
    'timezone'              => ':attribute ต้องอยู่ในรูปแบบ timezone',
    'unique'                => ':attribute ถูกนำมาใช้งานแล้ว',
    'uploaded'              => ':attribute ไม่สามารถอัปโหลดได้',
    'url'                   => ':attribute ต้องอยู่ในรูปแบบ ลิงค์',
    'uuid'                  => ':attribute ต้องอยู่ในรูปแบบ UUID',
    'match_old_password'    => ':attribute รหัสผ่านเดิมไม่ถูกต้อง',
    'base64'                => ':attribute ไม่ตรงกับรูปแบบ  (Base64)',
    'base64_image'          => ':attribute ไม่ตรงกับรูปแบบ  (png, jpg, jpeg)',
    'date_formats'          => ':attribute ไม่ตรงกับรูปแบบ Y-m-d, Y-m-d H:i:s',
    'phone'                 => ':attribute ไม่ตรงกับรูปแบบ 0x-xxx-xxxx, 0xx-xxx-xxxx',
    "latitude"              => ':attribute ไม่ตรงกับรูปแบบ ละติจูด',
    "longitude"             => ':attribute ไม่ตรงกับรูปแบบ ลองจิจูด',
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
