@component('mail::message')
# เรียนคุณ {{$withdraw->user->name}}
แจ้งสถานะบิลถอนเครดิต <br>
รหัสลูกค้า: {{$withdraw->user->code}}

@component('mail::table')
| ข้อมูลบิล      |               |
|:-------------|--------------:|
| เลขที่บิล      | {{ $withdraw->no }} |
| ยอดที่ถอน     | {{number_format($withdraw->amount, 2)}} |
| สถานะบิล      | <span style="color: #1bc5bd">สำเร็จ</span> |
@if(isset($withdraw->userBankAccount))
| ธนาคาร        | {{$withdraw->userBankAccount->bank_name}} |
| ชื่อบัญชี        | {{$withdraw->userBankAccount->title}} |
| เลขบัญชี       | {{$withdraw->userBankAccount->account}} |
@endif
@endcomponent
@if(isset($withdraw->credit))
# ยอดเครดิตคงเหลือของคุณ <span style="color: #1bc5bd">{{number_format($withdraw->credit->after, 2)}}</span>
@endif
@endcomponent
