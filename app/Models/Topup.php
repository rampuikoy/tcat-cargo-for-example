<?php

namespace App\Models;

use App\Helpers\ScopeQueries;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Topup extends Model
{
    use SoftDeletes, HasFactory;
    const PREFIX = 'TU-';
    public static $imageFolder = 'uploads/topup/';
    public static $imageWidth = 300;
    public static $imageHeight = 300;

    protected $fillable = [
        'approved_at',
        'method',
        'code',
        'date',
        'time',
        'file',
        'amount',
        'user_code',
        'system_bank_account_id',
        'system_bank',
        'system_account',
        'status',
        'user_remark',
        'admin_remark',
        'system_remark',
        'duplicate',
        'created_admin',
        'updated_admin',
        'approved_admin',
        'created_admin_id',
        'updated_admin_id',
        'approved_admin_id',
        'bot',
    ];

    protected $appends = [
        'status_text',
        'method_text',
        'no',
        'amount_text',
        'image_url',
    ];

    public static function statusList()
    {
        $array = [
            'cancel' => __('messages.cancel'),
            'waiting' => __('messages.waiting'),
            'approved' => __('messages.approved'),
        ];
        return $array;
    }

    public static function conditionList()
    {
        $array = [
            'only_user_remark' => __('messages.conditions.only_user_remark'),
            'only_admin_remark' => __('messages.conditions.only_admin_remark'),
            'only_has_image' => __('messages.conditions.only_has_image'),
            'only_not_image' => __('messages.conditions.only_not_image'),
            'only_has_duplicate' => __('messages.conditions.only_has_duplicate'),
            'only_has_match' => __('messages.conditions.only_has_match'),
            'only_not_match' => __('messages.conditions.only_not_match'),
            'only_has_bot' => __('messages.conditions.only_has_bot'),
            'only_not_bot' => __('messages.conditions.only_not_bot'),
            'only_not_match_same' => __('messages.conditions.only_not_match_same'),
        ];
        return $array;
    }

    public static function dateTypeList()
    {
        $array = [
            'created_at' => __('messages.created_at'),
            'updated_at' => __('messages.updated_at'),
            'approved_at' => __('messages.approved_at'),
            'date' => __('messages.date_transfer'),
        ];
        return $array;
    }

    public static function methodList()
    {
        $array = [
            'transfer' => __('messages.topups.transfer'),
            'cash' => __('messages.topups.cash'),
            'credit' => __('messages.topups.credit'),
            'cheque' => __('messages.topups.cheque'),
            'bt_card' => __('messages.topups.bt_card'),
            'other' => __('messages.topups.other'),
        ];
        return $array;
    }

    public static function methodUserList()
    {
        $array = [
            'transfer' => __('messages.topups.transfer'),
            'cheque' => __('messages.topups.cheque'),
            'other' => __('messages.topups.other'),
        ];
        return $array;
    }

    public static function cancelList()
    {
        $array = [
            __('messages.topup_cancel.repeat'),
            __('messages.topup_cancel.slip_not_match'),
            __('messages.topup_cancel.not_found_on_time'),
            __('messages.topup_cancel.transfer_out_account'),
            __('messages.topup_cancel.forgot_attach_slip'),
            __('messages.topup_cancel.test_system'),
        ];
        return $array;
    }

    public function setSystemBankAccountIdAttribute($value)
    {
        $this->attributes['system_bank_account_id'] = ($value) ? $value : null;
    }
    
    public function setDateAttribute($value)
    {
        $this->attributes['date'] = ($value) ? $value : null;
    }

    public function setTimeAttribute($value)
    {
        $this->attributes['time'] = ($value) ? $value : null;
    }
    
    public static function getMethodOptions()
    {
        return self::methodList();
    }

    public function getAmountTextAttribute()
    {
        return ($this->amount) ? number_format($this->amount, 2) : null;
    }

    public function getNoAttribute()
    {
        return ($this->id) ? self::PREFIX . str_pad($this->id, 6, "0", STR_PAD_LEFT) : null;
    }

    public function getImageUrlAttribute($handle = null)
    {
        $file = $handle ?? $this->file;
        return ($file) ? env('APP_MEDIA_URL') . '/' . self::$imageFolder . $file : null;
    }

    public function getStatusTextAttribute()
    {
        return collect(self::statusList())->get($this->status);
    }

    public function getMethodTextAttribute()
    {
        return collect(self::methodList())->get($this->method);
    }

    /**
     * relation
     */

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_code', 'code');
    }

    public function systemBankAccount()
    {
        return $this->belongsTo('App\Models\BankAccount', 'system_bank_account_id');
    }

    public function credit()
    {
        return $this->morphOne('App\Models\Credit', 'creditable');
    }

    public function credits()
    {
        return $this->morphMany('App\Models\Credit', 'creditable');
    }

    public function autoPay()
    {
        return $this->hasMany('App\Models\TopupAutopay', 'topup_id');
    }

    public function emails()
    {
        return $this->morphMany('App\Models\EmailQueue', 'mailable');
    }

    public function fcm()
    {
        return $this->morphMany('App\Models\FcmQueue', 'fcmable');
    }

    public function sms()
    {
        return $this->morphMany('App\Models\SmsQueue', 'smsable');
    }

    public function externalBank()
    {
        return $this->hasOne('App\Models\BankMap', 'bank_id', 'system_bank_account_id');
    }

    public function botMatching()
    {
        return $this->hasOne('App\Models\Statement', 'id', 'bank_report_id');
    }

    public function scopeSearchDuplicate($query, $conditions)
    {
        return $query->where(function ($query) use ($conditions) {
            if (@$conditions['user_code']) {
                $query->where('user_code', $conditions['user_code']);
            }
            if (@$conditions['amount']) {
                $query->where('amount', $conditions['amount']);
            }
            if (@$conditions['method']) {
                $query->where('method', $conditions['method']);
            }
            if (@$conditions['method'] == 'transfer') {
                $query->where('system_bank_account_id', $conditions['system_bank_account_id']);
            }
            if (@$conditions['date'] && @$conditions['time']) {
                $date_time = $conditions['date'] . ' ' . $conditions['time'] . ':00';
                $data = 'CONCAT(date," ",time,":00") BETWEEN DATE_SUB("' . $date_time . '", INTERVAL 30 MINUTE) AND DATE_ADD("' . $date_time . '", INTERVAL 30 MINUTE)';
                $query->whereRaw($data);
            }
        });
    }
    // scoped query sql
    public function scopeSearch($query, $conditions)
    {
        return $query->where(function ($query) use ($conditions) {
            if (@$conditions['id']) {
                $trimmed = ltrim($conditions['id'], Topup::PREFIX . '0');
                $query->where('id', 'LIKE', '%' . $trimmed . '%');
            }
            if (@$conditions['system_bank_account_id']) {
                $query->where('system_bank_account_id', 'LIKE', "%{$conditions['system_bank_account_id']}%");
            }
            if (@$conditions['user_code']) {
                $query->where('user_code', 'LIKE', "%{$conditions['user_code']}%");
            }
            if (@$conditions['method']) {
                $query->where('method', $conditions['method']);
            }
            if (@$conditions['status']) {
                $query->where('status', $conditions['status']);
            }
            if (@$conditions['amount']) {
                $query->where('amount', $conditions['amount']);
            }
            if (@$conditions['time']) {
                $query->where('time', 'LIKE', "%{$conditions['time']}%");
            }
            if (@$conditions['remark']) {
                $query->where('user_remark', 'like', '%' . $conditions['remark'] . '%');
                $query->orWhere('admin_remark', 'like', '%' . $conditions['remark'] . '%');
            }

            $date_type = (@$conditions['date_type']) ? $conditions['date_type'] : 'created_at';
            if (@$conditions['date_start'] && @$conditions['date_end']) {
                ScopeQueries::scopeDateRange($query, $date_type, $conditions['date_start'], $conditions['date_end']);
            } else if (@$conditions['date_start']) {
                ScopeQueries::scopeDateStart($query, 'created_at', $conditions['date_start']);
            } else if (@$conditions['date_end']) {
                ScopeQueries::scopeDateEnd($query, 'created_at', $conditions['date_end']);
            }

            if (@$conditions['condition']) {
                switch (@$conditions['condition']) {
                    case 'only_user_remark':
                        $query->whereRaw('user_remark != "" AND user_remark IS NOT NULL');
                        break;
                    case 'only_admin_remark':
                        $query->whereRaw('admin_remark != "" AND admin_remark IS NOT NULL');
                        break;
                    case 'only_has_image':
                        $query->whereNotNull('file');
                        break;
                    case 'only_not_image':
                        $query->whereNull('file');
                        break;
                    case 'only_has_duplicate':
                        $query->where('duplicate', 1);
                        break;
                    case 'only_has_match':
                        $query->where('bank_report_id', '!=', 0);
                        break;
                    case 'only_not_match':
                        $query->where('bank_report_id', 0);
                        break;
                    case 'only_not_match_same':
                        $query->whereNotNull('match_id');
                        $query->whereRaw('match_id != bank_report_id');
                        break;
                    case 'only_has_bot':
                        $query->where('approved_admin', 'BOT');
                        break;
                    case 'only_not_bot':
                        $query->where('approved_admin', '!=', 'BOT');
                        break;
                }
            }
        });
    }
}
