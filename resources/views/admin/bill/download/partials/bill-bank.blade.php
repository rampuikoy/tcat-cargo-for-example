<table class="bank" style="border:1px solid #bbb;" aria-describedby="bill-bank">
	<thead>
		<tr>
			<th colspan="5" scope="col" class="text-center">
				<div>บัญชีธนาคารสำหรับชำระเงิน</div>
			</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td width="3%"></td>
			<td class="text-left" width="15%">
				<div>ธนาคารกรุงเทพ</div>
			</td>
			<td class="text-center" width="5%">
				<div>(BBL)</div>
			</td>
			<td class="text-center" width="28%">
				<div>@lang('admin.title_account'):&nbsp;บจก.&nbsp;ทีซีเอที&nbsp;จำกัด</div>
			</td>
			<td class="text-left" width="20%">
				<div>@lang('admin.account_no'):&nbsp;924-0-14969-1</div>
			</td>
		</tr>
		<tr>
			<td></td>
			<td class="text-left">
				<div>ธนาคารกสิกรไทย</div>
			</td>
			<td class="text-center">
				<div>(KBANK)</div>
			</td>
			<td class="text-center">
				<div>@lang('admin.title_account'):&nbsp;บจก.&nbsp;ทีซีเอที&nbsp;จำกัด</div>
			</td>
			<td class="text-left">
				<div>@lang('admin.account_no'):&nbsp;033-3-94465-0</div>
			</td>
		</tr>
		<tr>
			<td colspan="5" style="height:5px;"></td>
		</tr>
	</tbody>
</table>