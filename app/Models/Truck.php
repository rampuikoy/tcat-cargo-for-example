<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Truck extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title',
        'code',
        'remark',
        'status',
        'created_admin',
        'updated_admin',
        'created_admin_id',
        'updated_admin_id',
    ];

    protected $appends = ['status_text'];
    
    public static function statusList()
    {
        $array = [
            'active'    => __('messages.active'),
            'inactive'  => __('messages.inactive'),
        ];
        return $array;
    }

    public function getStatusTextAttribute()
    {
        return collect(self::statusList())->get($this->status);
    }

    public function scopeSearch($query, $conditions)
    {
        return $query->where(function ($query) use ($conditions) {
            if (@$conditions['id']) {
                $query->where('id', $conditions['id']);
            }
            if (@$conditions['search']) {
                $query->where('title', 'LIKE', "%{$conditions['search']}%")
                    ->orWhere('code', 'LIKE', "%{$conditions['search']}%");
            }
            if (@$conditions['title']) {
                $query->where('title', 'LIKE', "%{$conditions['title']}%");
            }
            if (@$conditions['code']) {
                $query->where('code', 'LIKE', "%{$conditions['code']}%");
            }
            if (@$conditions['status']) {
                $query->where('status',$conditions['status']);
            }
        });
    }
}
