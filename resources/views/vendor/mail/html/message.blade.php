@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
{{ config('app.name') }}
@endcomponent
@endslot

{{-- Body --}}
{{ $slot }}

{{-- Subcopy --}}
@isset($subcopy)
@slot('subcopy')
@component('mail::subcopy')
{{ $subcopy }}
@endcomponent
@endslot
@endisset

@component('mail::table')
| ติดต่อฝ่ายบริการลูกค้า       |               |
|:------------- |--------------:|
| เว็บไซต์        | <a href="https://www.tcatcargo.com">www.tcatcargo.com</a> |
| Facebook      | <a href="https://www.facebook.com/tcatcargo">www.facebook.com/tcatcargo</a> |
| Line          | <a href="https://line.me/ti/p/@tcatcargo">@tcatcargo</a> |
| ฝ่ายบริการลูกค้า  | 080-003-1122 |
<!-- | โกดังหลักสินค้า  | 080-002-3636 |
| ฝ่ายโอนเงินไปจีน | 080-003-2828 |
| ฝ่ายไกด์-ทริป | 088-333-7979 |
| ฝ่ายลูกค้าปิดตู้ | 085-977-6543, 085-722-7222 | -->
@endcomponent

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
© {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.')
@endcomponent
@endslot
@endcomponent
