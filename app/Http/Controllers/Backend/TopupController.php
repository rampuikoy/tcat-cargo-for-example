<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Topup\CancelTopupRequest;
use App\Http\Requests\Topup\StoreTopupRequest;
use App\Http\Requests\Topup\UpdateTopupRequest;
use App\Interfaces\BankCompanyInterface;
use App\Interfaces\TopupInterface;
use App\Models\Topup;
use Illuminate\Http\Request;

class TopupController extends Controller
{
    protected $topupInterface, $bankCompanyInterface;

    public function __construct(TopupInterface $topupInterface, BankCompanyInterface $bankCompanyInterface)
    {
        $this->topupInterface           = $topupInterface;
        $this->bankCompanyInterface     = $bankCompanyInterface;
    }

    public function index(Request $request)
    {
        $this->authorize('index', Topup::class);
        $columns    = $request->input('columns', ['*']);
        $topup      = $this->topupInterface->all($columns);
        return response()->json($topup, $topup->code);
    }

    public function show(Request $request, $id)
    {
        $this->authorize('index', Topup::class);
        $columns    = $request->input('columns', ['*']);
        $topup      = $this->topupInterface->findById($id, $columns);
        return response()->json($topup, $topup->code);
    }

    public function search(Request $request)
    {
        $this->authorize('index', Withdraw::class);
        $conditions     = $request->only(['id', 'system_bank_account_id', 'user_code', 'status', 'amount', 'method', 'time', 'remark', 'date_start', 'date_end', 'date_type', 'condition']);
        $columns        = $request->input('columns', ['*']);
        $limit          = $request->input('limit', $this->topupInterface::DEFAULT_LIMIT);
        $sorting        = $request->input('order_by', $this->topupInterface::DEFAULT_SORTING);
        $ascending      = $request->input('ascending') == 1 ? 'asc' : 'desc';

        if ($request->boolean('pagination')) {
            $page       = $request->input('page', 1);
            $topup      = $this->topupInterface->paginateListByFields($conditions, $columns, $page, $limit, $sorting, $ascending);
        } else {
            $offset     = $request->input('page', 0);
            $topup      = $this->topupInterface->fetchListByFields($conditions, $columns, $offset, $limit, $sorting, $ascending);
        }
        return response()->json($topup, $topup->code);
    }

    public function dropdown()
    {
        $dropdown   = $this->topupInterface->dropdownList();
        if (!$dropdown->error) {
            $bankCompany        = $this->bankCompanyInterface->all(['id', 'account', 'title', 'branch', 'bank']);
            $collection         = collect($dropdown->results)->merge(['bank_company'   => (!$bankCompany->error) ? $bankCompany->results : []]);
            $dropdown->results = $collection;
        }
        return response()->json($dropdown, $dropdown->code);
    }

    public function store(StoreTopupRequest $request)
    {
        ini_set('memory_limit', '512M');
        ini_set('max_execution_time', 300);

        $data   = $request->only([
            'user_code',
            'method',
            'system_bank_account_id',
            'amount',
            'date',
            'time',
            'file',
            'admin_remark',
            'user_remark',
            'bill'
        ]);
        $topup  = $this->topupInterface->store($data);
        return response()->json($topup, $topup->code);
    }

    public function update(UpdateTopupRequest $request, $id)
    {
        $data   = $request->only([
            'date',
            'time',
            'method',
            'system_bank_account_id',
            'admin_remark',
        ]);
        $topup  = $this->topupInterface->updateById($id, $data);
        return response()->json($topup, $topup->code);
    }

    public function approve($id)
    {
        $this->authorize('approve', Topup::class);
        $topup  = $this->topupInterface->approveById($id);
        return response()->json($topup, $topup->code);
    }

    public function cancel(CancelTopupRequest $request, $id)
    {
        $this->authorize('destroy', Topup::class);
        $data   = $request->only(['remark']);
        $topup  = $this->topupInterface->cancelById($id, $data);
        return response()->json($topup, $topup->code);
    }

    public function checkDuplicate(Request $request)
    {
        $this->authorize('index', Topup::class);
        $conditions     = $request->only([
            'user_code',
            'method',
            'system_bank_account_id',
            'amount',
            'date',
            'time',
        ]);
        $topup          = $this->topupInterface->getCheckDuplicate($conditions);
        return response()->json($topup, $topup->code);
    }

    public function optionCreate($code)
    {
        $this->authorize('store', Topup::class);
        $topup  = $this->topupInterface->optionCreate($code);
        return response()->json($topup, $topup->code);
    }

    public function optionEdit($id)
    {
        $this->authorize('update', Topup::class);
        $options    = $this->topupInterface->optionEdit($id);

        if (!$options->error) {
            $bankCompany        = $this->bankCompanyInterface->all(['id', 'account', 'title', 'branch', 'bank']);
            $collection         = collect($options->results)->merge([
                'bank_company'   => (!$bankCompany->error) ? $bankCompany->results : [],
            ]);
            $options->results = $collection;
        }
        return response()->json($options, $options->code);
    }

    public function destroy($id)
    {
        $this->authorize('destroy', Topup::class);
        $topup     = $this->topupInterface->softDeleteById($id);
        return response()->json($topup, $topup->code);
    }
}
