<?php

namespace App\Http\Requests\NoCode;

use App\Http\Requests\FormRequest;

class ZoneInNoCodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user('admins')->can('backend.nocode.store') || $this->user('admins')->can('backend.nocode.update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code'          => 'required|string',
            'zone'          => 'sometimes|nullable|string',
            'tcat'          => 'required|boolean',
            'taobao'        => 'required|boolean',
            'tmall'         => 'required|boolean',
        ];
    }

    public function attributes()
    {
        return [
            'code'              => __('form.no_code.code'),
            'zone'              => __('form.no_code.zone'),
            'tcat'              => __('form.no_code.option.tcat'),
            'taobao'            => __('form.no_code.option.taobao'),
            'tmall'             => __('form.no_code.option.tmall'),
        ];
    }
}
