<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ThaiShippingWeightSizePrice extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'title',
        'max_size',
        'max_weight',
        'min_weight',
        'price',
        'thai_shipping_method_id',
    ];
}
