<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Admin;
use Tymon\JWTAuth\Facades\JWTAuth;

class ReportTest extends TestCase
{
    use WithFaker;
    protected $user;
    protected $url = '/api/backend/norole';
    /**
     * A basic feature test example.
     *
     * @return void
     */
    protected function authenticate(string $guard = 'admins')
    {
        $this->user = factory(Admin::class)->create();
        auth()->shouldUse($guard);
        $token = JWTAuth::fromUser($this->user);
        return 'Bearer ' . $token;
    }

    protected function deleteUser()
    {
        $this->user->forceDelete();
    }

    /** @test */
    public function GetReportSuccessTest()
    {
        $token = $this->authenticate();
        $response = $this->withHeaders([
            'Authorization' => $token,
        ])->json('GET', "{$this->url}/user/find/test");
        $response->assertStatus(200);
        $this->deleteUser();
    }

    /** @test */
    public function GetReportFailTest()
    {
        $response = $this->json('GET', "{$this->url}/user/find/test");
        $response->assertStatus(401);
    }
}
