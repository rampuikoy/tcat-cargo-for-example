<?php

namespace Database\Factories;

use App\Models\Admin;
use App\Models\NoCode;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Arr;

class NoCodeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = NoCode::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $admin              = Admin::whereRole('SuperAdmin')->first();
        $suf_order          = [$this->faker->numerify('##########'), null];
        $suf_ref            = [$this->faker->numerify('##########'), null];
        $suf_type           = ['clothes', 'bag', 'accessories', 'electronics', 'toy', 'other', 'cosmetics', 'spares', 'shoe'];
        $suf_dep            = ['TCATCARGO', 'TMALL', 'TAOBAO', 'ALIBABA', null];
        $suf_source         = ['TAOBAO / TCAT-MALL', 'TCAT-CARGO / TAOBAO / TCAT-MALL', null];
        $suf_source_order   = ['BA180973', 'KB181633', 'SC2317', null];

        return [
            //
            'code'              => $this->faker->numerify('##########'),
            'order_code'        => Arr::random($suf_order),
            'reference_code'    => Arr::random($suf_ref),
            'zone'              => $this->faker->text(10),
            'weight'            => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 10, $max = 50),
            'width'             => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 10, $max = 50),
            'height'            => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 10, $max = 50),
            'length'            => $this->faker->randomFloat($nbMaxDecimals = 2, $min = 10, $max = 50),
            'date_in'           => $this->faker->dateTime(),
            'date_out'          => $this->faker->dateTime(),
            'amount'            => $this->faker->numberBetween(1, 10),
            'list'              => $this->faker->numberBetween(1, 10),
            'type'              => Arr::random($suf_type),
            'department'        => Arr::random($suf_dep),
            'description'       => $this->faker->sentence(6),
            'status'            => $this->faker->numberBetween(0, 3),
            'source'            => Arr::random($suf_source),
            'source_order'      => Arr::random($suf_source_order),
            'remark'            => 'REMARK : ' . $this->faker->sentence(10),
            'system_remark'     => 'SYSTEM REMARK : ' . $this->faker->sentence(10),
            'taken_admin_id'    => $admin->id,
            'taken_admin'       => $admin->username,
        ];
    }
}
