<?php

namespace App\Repositories;

use App\Interfaces\ThaiShippingAreaPriceInterface;
use App\Models\ThaiShippingAreaPrice;
use App\Traits\CacheTrait;
use App\Traits\CoreResponseTrait;
use Illuminate\Support\Facades\Cache;


class ThaiShippingAreaPriceRepository implements ThaiShippingAreaPriceInterface
{
    // Use ResponseAPI Trait in this repository
    use CacheTrait, CoreResponseTrait;

    const MINUTE_CACHE          = 1;
    const TABLE_RELETIONS       = [];

    public function __construct(ThaiShippingAreaPrice $thaiShippingAreaPrice)
    {
        $this->thaiShippingAreaPrice        = $thaiShippingAreaPrice;
    }

    public function all(array $columns = ['*'], $sorting = ThaiShippingAreaPriceInterface::DEFAULT_SORTING, $ascending = ThaiShippingAreaPriceInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $thaiShippingAreaPrices     = Cache::tags([$this->getClassPrefix(), $this->getAllWithClassPrefix()])->remember($key, $time, function () use ($columns, $sorting, $ascending) {
                                        return $this->thaiShippingAreaPrice::with('province')
                                        ->select($columns)
                                        ->orderBy($sorting, $ascending)
                                        ->get();
                                    });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.thai_shipping_area_price')]), $thaiShippingAreaPrices);
    }

    public function fetchListByFields(array $conditions = [], array $columns = ['*'], $offset = 0, $limit = ThaiShippingAreaPriceInterface::DEFAULT_LIMIT, $sorting = ThaiShippingAreaPriceInterface::DEFAULT_SORTING, $ascending = ThaiShippingAreaPriceInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $thaiShippingAreaPrices     = Cache::tags([$this->getClassPrefix(), $this->getFetchWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $offset, $limit, $sorting, $ascending) {
            return $this->thaiShippingAreaPrice::with('province')
                                                ->select($columns)
                                                ->where(function ($query) use ($conditions) {
                                                    if (@$conditions['thai_shipping_method_id']) {
                                                        $query->whereThaiShippingMethodId($conditions['thai_shipping_method_id']);
                                                    }
                                                })
                                                ->orderBy($sorting, $ascending)
                                                ->skip($offset)
                                                ->limit($limit)
                                                ->get();
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.thai_shipping_area_price')]), $thaiShippingAreaPrices);
    }

    public function paginateListByFields(array $conditions, array $columns = ['*'], $page = 1, $limit = ThaiShippingAreaPriceInterface::DEFAULT_LIMIT, $sorting = ThaiShippingAreaPriceInterface::DEFAULT_SORTING, $ascending = ThaiShippingAreaPriceInterface::DEFAULT_ASCENDING)
    {
        $key        = $this->getCacheKey($this->getUrlWithClassPrefix(), $this->getCurrentUrl());
        $time       = $this->getTime(self::MINUTE_CACHE);

        $thaiShippingAreaPrices     = Cache::tags([$this->getClassPrefix(), $this->getPaginationWithClassPrefix()])->remember($key, $time, function () use ($conditions, $columns, $page, $limit, $sorting, $ascending) {
            $model = $this->thaiShippingAreaPrice::with('province')
                                                ->select($columns)
                                                ->where(function ($query) use ($conditions) {
                                                    if (@$conditions['thai_shipping_method_id']) {
                                                        $query->whereThaiShippingMethodId($conditions['thai_shipping_method_id']);
                                                    }
                                                })
                                                ->orderBy($sorting, $ascending);

            if ($page == 1) {
                return $model->paginate($limit);
            } else {
                return $model->skip($limit * ($page - 1))->paginate($limit);
            }
        });

        return $this->coreResponse(200, __('messages.all_data', ['data' => __('messages.thai_shipping_area_price')]), $thaiShippingAreaPrices);
    }

    public function findById($id, array $columns = ['*'])
    {
        $thaiShippingAreaPrice = $this->findByIdMethod($id);

        if (!$thaiShippingAreaPrice) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.thai_shipping_area_price'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.thai_shipping_area_price')]), $thaiShippingAreaPrice);
    }

    public function findManyByShippingId($id, array $columns = ['*'])
    {
        $thaiShippingAreaPrice = $this->findManyByShippingIdMethod($id);

        if ($thaiShippingAreaPrice->count() === 0) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.thai_shipping_area_price'), 'id' => $id]));
        }

        return $this->coreResponse(200, __('messages.find_data', ['data' => __('messages.thai_shipping_area_price')]), $thaiShippingAreaPrice);
    }

    public function store(array $data)
    {
        $thaiShippingAreaPrice  = $this->thaiShippingAreaPrice::create([
            'province_id'               => $data['province_id'],
            'price_a'                   => $data['price_a'],
            'price_b'                   => $data['price_b'],
            'price_c'                   => $data['price_c'],
            'price_d'                   => $data['price_d'],
            'price_e'                   => $data['price_e'],
            'thai_shipping_method_id'   => $data['thai_shipping_method_id'],
        ]);

        $this->clearCacheTags();
        return $this->coreResponse(200, __('messages.create_data', ['data' => __('messages.thai_shipping_area_price')]), $thaiShippingAreaPrice);
    }

    public function updateById($id, array $data)
    {
        $thaiShippingAreaPrice      = $this->findByIdMethod($id);

        if (!$thaiShippingAreaPrice) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.thai_shipping_area_price'), 'id' => $id]));
        }

        $thaiShippingAreaPrice->update([
            'province_id'               => $data['province_id'],
            'price_a'                   => $data['price_a'],
            'price_b'                   => $data['price_b'],
            'price_c'                   => $data['price_c'],
            'price_d'                   => $data['price_d'],
            'price_e'                   => $data['price_e'],
        ]);
        $this->clearCacheTags();
        $this->clearCacheTagById($id);
        return $this->coreResponse(200, __('messages.update_data', ['data' => __('messages.thai_shipping_area_price')]), $thaiShippingAreaPrice);
    }

    public function softDeleteById($id)
    {
        $thaiShippingAreaPrice      = $this->findByIdMethod($id);

        if (!$thaiShippingAreaPrice) {
            return $this->coreResponse(404, __('messages.no_data_with_id', ['data' => __('messages.thai_shipping_area_price'), 'id' => $id]));
        }

        $thaiShippingAreaPrice->delete();
        $this->clearCacheTags();
        $this->clearCacheTagById($id);
        return $this->coreResponse(200, __('messages.delete_data', ['data' => __('messages.thai_shipping_area_price')]), $thaiShippingAreaPrice);
    }

    private function findByIdMethod($id, array $columns = ['*'])
    {
        $key    = $this->getCacheKey(null,  $id);
        $time   = $this->getTime(self::MINUTE_CACHE);

        $thaiShippingAreaPrice  = Cache::tags([
            $this->getClassPrefix(),
            $this->getFindByIdWithClassPrefix(),
            $this->getIdWithClassPrefix($id)
        ])->remember($key, $time, function () use ($id, $columns) {
            return $this->thaiShippingAreaPrice::with('province')->select($columns)->whereId($id)->first();
        });

        return $thaiShippingAreaPrice;
    }

    private function findManyByShippingIdMethod($id, array $columns = ['*'])
    {
        $key    = $this->getCacheKey('SHIPPING_METHOD', $id);
        $time   = $this->getTime(self::MINUTE_CACHE);
        
        $thaiShippingAreaPrice  = Cache::tags([
                                        $this->getClassPrefix(),
                                        $this->getIdWithClassPrefix($id),
                                        $this->getFindByShippingMethodIdWithClassPrefix($id),
                                    ])->remember($key, $time, function () use ($id, $columns) {
                                        return $this->thaiShippingAreaPrice::with('province')->select($columns)->whereThaiShippingMethodId($id)->get();
                                    });
        return $thaiShippingAreaPrice;
    }

    private function clearCacheTags()
    {
        $keys = [
            $this->getPaginationWithClassPrefix(),
            $this->getFetchWithClassPrefix(),
        ];
        Cache::tags($keys)->flush();
    }

    private function clearCacheTagById($id)
    {
        Cache::tags([$this->getIdWithClassPrefix($id)])->flush();
    }
}
