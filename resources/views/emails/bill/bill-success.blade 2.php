@component('mail::message')
# เรียนคุณ {{$bill->user->name}}
แจ้งสรุปบิลขนส่ง <br>
เลขที่บิล: <a href="#" target="_blank">{{$bill->no}}</a> <br>
รหัสลูกค้า: {{$bill->user->code}} <br>
สถานะบิล:  <span style="color: #1bc5bd">สำเร็จ/ปิดบิล</span>
<br><br><br>
ลูกค้าสามารถตรวจสอบข้อมูลสรุปบิล จากเอกสารที่แนบมา <br>
ขอขอบคุณที่ใช้บริการ TCAT CARGO ค่ะ
@endcomponent
