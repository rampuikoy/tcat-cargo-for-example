<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\ShippingMethod;
use App\Models\Admin;
use Tymon\JWTAuth\Facades\JWTAuth;

class ShippingMethodTest extends TestCase
{
  use WithFaker;
  protected $user;
  protected $shippingMethod;
  protected $url = '/api/backend/shipping-method';
  /**
   * A basic feature test example.
   *
   * @return void
   */
  protected function authenticate(string $guard = 'admins')
  {
    $this->user = factory(Admin::class)->create();
    auth()->shouldUse($guard);
    $this->shippingMethod = factory(ShippingMethod::class)->create();
    $token = JWTAuth::fromUser($this->user);
    return 'Bearer ' . $token;
  }

  protected function deleteUser()
  {
    $this->shippingMethod->forceDelete();
    $this->user->forceDelete();
  }

  /** @test */
  public function GetShippingMethodSearchSuccessTest()
  {
    $id = random_int(1, 200);
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', "{$this->url}/search", [
      "id" => $id,
      "search" => "",
      "title_th" => ""
    ]);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function GetShippingMethodSearchFailTest()
  {
    $response = $this->json('GET', "{$this->url}/search");
    $response->assertStatus(401);
  }

  /** @test */
  public function GetShippingMethodDropdownSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', "{$this->url}/dropdown");
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function GetShippingMethodDropdownFailTest()
  {
    $response = $this->json('GET', "{$this->url}/dropdown");
    $response->assertStatus(401);
  }

  /** @test */
  public function GetShippingMethodAllSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('GET', $this->url);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function GetShippingMethodAllFailTest()
  {
    $response = $this->json('GET', $this->url);
    $response->assertStatus(401);
  }

  /** @test */
  public function ShippingMethodPostSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('POST', $this->url, [
      'title_th' => 'test3',
      'title_en' => 'test3',
      'title_cn' => 'test3',
      // 'status'   => 'inactive',
    ]);
    $response->assertStatus(200);
    ShippingMethod::where('title_th', 'test3')->forceDelete();
    $this->deleteUser();
  }

  /** @test */
  public function ShippingMethodPostFailAuthTest()
  {
    $response = $this->json('POST', $this->url);
    $response->assertStatus(401);
  }

  /** @test */
  public function ShippingMethodPostFailTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('POST', $this->url);
    $response->assertStatus(422);
    $this->deleteUser();
  }

  /** @test */
  public function ShippingMethodUpdateByIdSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('PUT', "{$this->url}/{$this->shippingMethod->id}", [
      'title_th' => 'test',
      'title_en' => 'test',
      'title_cn' => 'test',
    ]);
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function ShippingMethodUpdateByIdFailAuthTest()
  {
    $response = $this->json('PUT', "{$this->url}/11");
    $response->assertStatus(401);
  }

  /** @test */
  public function ShippingMethodUpdateByIdFailTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' =>  $token,
    ])->json('PUT', "{$this->url}/{$this->shippingMethod->id}");
    $response->assertStatus(422);
    $this->deleteUser();
  }

  /** @test */
  public function ShippingMethodDeleteSuccessTest()
  {
    $token = $this->authenticate();
    $response = $this->withHeaders([
      'Authorization' => $token,
    ])->json('DELETE', "{$this->url}/{$this->shippingMethod->id}");
    $response->assertStatus(200);
    $this->deleteUser();
  }

  /** @test */
  public function ShippingMethodDeleteFailTest()
  {
    $response = $this->json('DELETE', "{$this->url}/5");
    $response->assertStatus(401);
  }
}
