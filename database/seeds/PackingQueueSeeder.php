<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\PackingQueue;

class PackingQueueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PackingQueue::factory(10)->create();
    }
}
