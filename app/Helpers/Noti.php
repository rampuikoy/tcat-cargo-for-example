<?php

namespace App\Helpers;

use Carbon\Carbon;
use App\Models\Bill;
use App\Models\Chat;
use App\Models\Topup;
use App\Models\Withdraw;

class Noti
{
    public static function counterNoti()
    {
        $bill      = Bill::whereStatus('0')->count();
        $chat      = Chat::whereCreatedType('user')->whereReaded(0)->count();
        $topup     = Topup::whereStatus('waiting')->count();
        $withdraw  = Withdraw::whereStatus('waiting')->count();

        return [
            'bill' => $bill,
            'chat' => $chat,
            'topup' => $topup,
            'withdraw' => $withdraw
        ];
    }
}
